﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using System.Web.Security;

namespace Bestprintbuy.Admin.Web
{
    public partial class Login : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session.RemoveAll();
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string userName = txtUserName.Text;
            string password = txtPassword.Text;

            UserModel.UserDataTable user = UserService.AuthenticateUser(userName, MD5Encryption.Encrypt(password), ConnString);
            if (user != null && user.Rows.Count > 0)
            {
                UserModel.UserRow userRow = (UserModel.UserRow)user.Rows[0];
                UserIdentity identity = new UserIdentity();
                identity.canChangePasswd = userRow.permit_pw_change;
                identity.UserPK = userRow.pkey_user;
                identity.Name = userRow.login_id;
                identity.AllowCustomerCommunication = userRow.can_communicate_with_customer;
                //identity.UserRole = userRow.role;
                Session["UserName"] = identity.Name;
                UserPrincipal principal = new UserPrincipal(identity, "roles");
                Session[Session.SessionID] = principal;
                Response.Redirect("~/Account/DashBoard.aspx");
            }
            else
            {
                AlertScript("Invalid Credentials");
                txtPassword.Focus();
            }
        }
    }
}