﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using Bestprintbuy.Admin.Web.Common;

namespace Bestprintbuy.Admin.Web
{
    public class Global : System.Web.HttpApplication
    {
        private static List<string> lstSecurityScreens = new List<string>();
        protected void Application_Start(object sender, EventArgs e)
        {
            try
            {

                //Bestprintbuy.Admin.Utilities.FileLogger.WriteEntry("Loading configs");
                WebConfiguration.InitializeAppConfig(HttpRuntime.AppDomainAppVirtualPath);
                //Bestprintbuy.Admin.Utilities.FileLogger.WriteEntry("Loading app configs");
                WebConfiguration.InitializeScreenMaps(HttpRuntime.AppDomainAppVirtualPath);

            }
            catch (Exception ex) { Bestprintbuy.Admin.Utilities.FileLogger.WriteEntry("Loading configs error:" + ex.Message); }
            try
            {
                PopulatePages();
            }
            catch { }
        }

        private void PopulatePages()
        {
            lstSecurityScreens.Add("grouplist.aspx");
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            bool compress = false;
            try
            {
                compress = (System.Configuration.ConfigurationManager.AppSettings["Compress"].ToString().ToLower() == "yes");
            }
            catch { }

            //if (compress)
            //{
            //    HttpContext incoming = HttpContext.Current;
            //    string oldpath = incoming.Request.Path.ToLower();
            //    incoming.Response.Filter = new System.IO.Compression.GZipStream(incoming.Response.Filter, System.IO.Compression.CompressionMode.Compress);
            //    HttpContext.Current.Response.AppendHeader("Content-encoding", "gzip");
            //    HttpContext.Current.Response.Cache.VaryByHeaders["Accept-encoding"] = true;
            //}
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {
            lstSecurityScreens.Clear();
        }
    }
}