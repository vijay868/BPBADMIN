using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Bestprintbuy.Admin.DataModel;

namespace Bestprintbuy.Admin.Web.Common
{
    public class LoggedUser
    {
        private string userName;
        private int userID;
        private string name;
        private string emailID;
        private bool isSuperUser;

        private LoggedUser() { 
        
        }

        internal LoggedUser(string userName, int userID, string name, string emailID, bool isSuperUser) {
            this.userName = userName;
            this.userID = userID;
            this.emailID = emailID;
            this.name = name;
            this.isSuperUser = isSuperUser;
        }

        internal string UserName
        {
            get { return userName; }
        }

        internal string Name
        {
            get { return name; }
        }

        internal string EmailID
        {
            get { return emailID; }
        }

        internal bool ISSuperUser
        {
            get { return isSuperUser; }
        }

        internal int UserID
        {
            get { return userID; }
        }
    }
}
