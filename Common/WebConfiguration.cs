﻿using System;
using System.Runtime.Remoting;
using System.Collections;
using System.Diagnostics;
using System.Configuration;
using System.Xml;
using System.Collections.Specialized;

namespace Bestprintbuy.Admin.Web.Common
{
    /// <summary>
    /// Summary description for WebConfiguration.
    /// </summary>
    public class WebConfiguration : IConfigurationSectionHandler
    {
        //
        // Constant values for all of the SystemFramework standard settings
        //
        private const String LOGGING_ENABLED = "WebConfiguration.WebLogger.Enabled";
        private const String LOGGING_FILE = "WebConfiguration.WebLogger.File";
        private const String LOGGING_LEVEL = "WebConfiguration.WebLogger.Level";
        private const String LOGGING_SWITCHNAME = "WebConfiguration.WebLogger.SwitchName";
        private const String LOGGING_SWITCHDESCRIPTION = "WebConfiguration.WebLogger.SwitchDescription";
        private const String REMOTING_CHANNEL = "host.ppsol.services.channel";
        private const String REMOTING_PORT = "host.ppsol.services.port";
        private const String REMOTING_HOST_NAME = "host.ppsol.services.hostname";
        private const String FILE_UPLOAD_SIZE = "file.upload.size";
        private const String PAGEFLEX_EDIT_HOST_URL = "PageFlex.Edit.Host.URL";

        private const String LOGIN_ADMIN_URL = "ppsol.login.admin.URL";
        private const String LOGIN_SUPPLIER_URL = "ppsol.login.supplier.URL";
        private const String LOGIN_CUSTOMER_URL = "ppsol.login.customer.URL";
        private const String HOME_ADMIN_URL = "ppsol.home.admin.URL";
        private const String FAIL_URL = "ppsol.fail.URL";
        private const String HOME_SUPPLIER_URL = "ppsol.home.supplier.URL";
        private const String HOME_CUSTOMER_URL = "ppsol.home.customer.URL";
        private const String SUPPORTED_IMAGE_TYPES = "ppsol.supported.image.types";
        private const String ACCESS_ERROR_URL = "ppsol.Error.URL";
        private const String BPB_SUPPORT_MAILID = "bpb.Support.EMail";
        private const String BPB_IS_PRODUCTION = "bpb.IsProduction";
        private const String BPB_IGNORE_VERISIGN = "bpb.IgnoreVerisign";
        private const String BPB_IGNORE_ORDERSTATUSMAIL = "bpb.IgnoreOrderStatusMails";
        private const String BPB_UPS_TRACKURL = "bpb.UPSTrackingURL";
        private const String BPB_USPS_TRACKURL = "bpb.USPSTrackingURL";
        private const String BPB_FEDEX_TRACKURL = "bpb.FedexTrackingURL";

        private const String BPB_REDIRECT_CAMPAIGNMAIL = "bpb.RedirectCampaignMails";
        private const String BPB_XSELL_NOTEPADS = "bpb.xSellNotePads";
        private const String BPB_PROFILE_CMYK = "BPB.Profile.CMYK";
        private const String BPB_PROFILE_RGB = "BPB.Profile.RGB";
        private const String BPB_PROFILE_TOCMYK = "BPB.Profile.TOCMYKProfile";
        private const String BPB_IS_JPG_FROM_CMYKPDF = "BPB.IsJPGFromCMYKPDF";
        private const String BPB_PDFLIB_LICENSE_KEY = "BPB.PDFLibLicenseKey";
        private const String BPB_CUSTOMERCARE_MAILID = "bpb.customercare.email";
        private const String BPB_CATALOG_REQUEST_MAILID = "bpb.catalogrequest.email";
        private const String BPB_STATE_TAX = "bpb.statetax.MA";
        private const String BPB_STATE_TAX_LABEL = "bpb.statetax.Label";
        private const String BPB_UPLOADDESIGNPROCESSCOST = "bpb.statetax.UploadDesignProcessCost";

        private const String BPB_DM_PCSTANDARD_FIRSTCLASS = "bpb.DM.PCSTDFirstClass";
        private const String BPB_DM_PCOVE_FIRSTCLASS = "bpb.DM.PCOVEFirstClass";
        private const String BPB_DM_PCOVE_STANDARDBULK = "bpb.DM.PCOVEStandardBulk";

        private const String DESIGN_UPLOAD_RESOLUTION = "design.upload.resolution";
        private const String PPSOL_PHOTO_DIMENSIONS = "ppsol.photo.dimensions";

        private const String BPB_PDFSAVEFOLDER = "PPSOL.PDFSaveFolder";
        private const String BPB_PDFSAVEFOLDER_SERVER = "PPSOL.PDFSaveFolderServer";

        private const String BPB_ADMIN_USER_INFO_URL = "BPB.CustomerPortalAUIURL";
        private const String BPB_CUSTOMER_PROOFS_MAILID = "bpb.customerproofs.email";



        //
        // Static member variables. These contain the application settings
        //   from Config.Web, or the default values.
        //
        private static bool loggingEnabled;
        private static String loggingFile;
        private static TraceLevel loggingLevel;
        private static String loggingSwitchName;
        private static String loggingSwitchDescription;
        private static int port = -1;
        private static String channel = null;
        private static String hostname = null;
        private static int uploadSize = 0;
        private static String dotEditURL = "http://localhost/.Edit";
        private static string supportedImageTypes = "";
        private static int designResolution = 300;
        private static string strPhotoDimensions = "84X120";  //width X height



        private static String loginAdminUrl = "";
        private static String loginSupplierUrl = "";
        private static String loginCustomerUrl = "";
        private static String adminHomeUrl = "";
        private static String supplierHomeUrl = "";
        private static String customerHomeUrl = "";
        private static String failUrl = "";
        private static String accessErrorUrl = "";
        private static String ppsolSupportEmail = "";
        private static String customerCareEmail = "";
        private static String catalogrequestEmail = "";
        private static String customerProofsEmail = "";
        private static bool isProduction = true;
        private static bool ignoreVerisign = true;
        private static bool ignoreOrderStatusMails = false;
        private static string xSellNotePads = "Both";
        private static double stateTax = 0.0;
        private static string stateTaxLabel = "";
        private static string pdfSaveFolder = "";
        private static string pdfSaveFolderServer = "";
        private static decimal uploadDesignProcessCost = 0.0m;
        private static bool redirectCampaignMailToAdmin = false;

        private static decimal dmOVEStandardBulkCost = 0.30m;
        private static decimal dmStandardFirstClassCost = 0.26m;
        private static decimal dmOVEFirstClassCost = 0.41m;

        private static string bpbCMYKProfile = "";
        private static string bpbToCMYKprofile = "";
        private static string bpbRGBProfile = "";
        private static string bpbPDFLibLicenseKey = "";
        private static bool isJPGFromCMYKPDF = true;
        private static string adminUIURL = "";

        private static string upstrackingUrl = "";
        private static string uspstrackingurl = "";
        private static string fedextrackingurl = "";


        private static Hashtable adminLoginScreens = new Hashtable();
        private static Hashtable ppsolScreens = new Hashtable();
        private static Hashtable supplierScreens = new Hashtable();
        private static Hashtable portalScreens = new Hashtable();
        //private static Hashtable designTemplates = new Hashtable();

        private static Hashtable imageDirs = new Hashtable();

        private static Hashtable productVariants = new Hashtable();
        private static Hashtable displayVariants = new Hashtable();
        private static Hashtable productTypeKeys = new Hashtable();
        private static Hashtable productColorKeys = new Hashtable();
        private static Hashtable variantDataTypes = new Hashtable();
        private static Hashtable shipmentTypes = new Hashtable();
        private static Hashtable mailContentHtmls = new Hashtable();
        //Modification Start(Sujani)
        private static Hashtable dcDefaultimagestobeignored = new Hashtable();
        private static Hashtable dcColorDesignations = new Hashtable();
        //Modification End.

        private static NameValueCollection BCProfile = new NameValueCollection();
        private static NameValueCollection PCProfile = new NameValueCollection();
        private static NameValueCollection ALProfile = new NameValueCollection();
        private static NameValueCollection LHProfile = new NameValueCollection();
        private static NameValueCollection TemplateCategory = new NameValueCollection();

        private static Hashtable melissaDataInfo = new Hashtable();
        private static Hashtable usDataLeadsInfo = new Hashtable();

        private static Hashtable paymentInfo = new Hashtable();

        //
        // Constant values for all of the default settings.
        //
        private const bool LOGGING_ENABLED_DEFAULT = true;
        private const String LOGGING_FILE_DEFAULT = "ApplicationTrace.txt";
        private const TraceLevel LOGGING_LEVEL_DEFAULT = TraceLevel.Verbose;
        private const String LOGGING_SWITCHNAME_DEFAULT = "ApplicationTraceSwitch";
        private const String LOGGING_SWITCHDESCRIPTION_DEFAULT = "Application error and logging information";


        private static bool initConfigSection = false;



        public WebConfiguration()
        {

        }

        public static void InitializeAppConfig(String configpath)
        {
            initConfigSection = true;


            try
            {
                //RemotingConfiguration.Configure(configpath);
                System.Configuration.ConfigurationSettings.GetConfig("ConfigSection/WebConfiguration");
            }
            catch (Exception ex)
            {
                Bestprintbuy.Admin.Utilities.FileLogger.WriteException(ex);
                //Console.WriteLine("Message : " + ex.Message);
                //Console.ReadLine();
            }

        }

        public static void InitializeScreenMaps(String configpath)
        {
            initConfigSection = false;
            try
            {
                NameValueCollection nvc = null;
                nvc = (NameValueCollection)System.Configuration.ConfigurationSettings.GetConfig("ConfigSection/AdminLoginScreens");
                UpdateDataModel(ref adminLoginScreens, ref nvc);
                nvc = null;
                nvc = (NameValueCollection)System.Configuration.ConfigurationSettings.GetConfig("ConfigSection/AdminScreens");
                UpdateDataModel(ref ppsolScreens, ref nvc);
                nvc = null;
                nvc = (NameValueCollection)System.Configuration.ConfigurationSettings.GetConfig("ConfigSection/SupplierScreens");
                UpdateDataModel(ref supplierScreens, ref nvc);
                nvc = null;
                nvc = (NameValueCollection)System.Configuration.ConfigurationSettings.GetConfig("ConfigSection/PortalScreens");
                UpdateDataModel(ref portalScreens, ref nvc);
                nvc = null;
                //nvc= (NameValueCollection) System.Configuration.ConfigurationSettings.GetConfig("ConfigSection/DesignTemplates");
                //UpdateDataModel(ref designTemplates,ref nvc);
                //nvc = null;

                nvc = (NameValueCollection)System.Configuration.ConfigurationSettings.GetConfig("ConfigSection/ProductVariants");
                UpdateDataModel(ref productVariants, ref nvc);
                nvc = null;
                nvc = (NameValueCollection)System.Configuration.ConfigurationSettings.GetConfig("ConfigSection/DisplayVariants");
                UpdateDataModel(ref displayVariants, ref nvc);
                nvc = null;
                nvc = (NameValueCollection)System.Configuration.ConfigurationSettings.GetConfig("ConfigSection/ProductTypeKeys");
                UpdateDataModel(ref productTypeKeys, ref nvc);
                nvc = null;

                nvc = (NameValueCollection)System.Configuration.ConfigurationSettings.GetConfig("ConfigSection/ImageDirs");
                UpdateDataModel(ref imageDirs, ref nvc);
                nvc = null;

                nvc = (NameValueCollection)System.Configuration.ConfigurationSettings.GetConfig("ConfigSection/PaymentInfo");
                UpdateDataModel(ref paymentInfo, ref nvc);
                nvc = null;

                nvc = (NameValueCollection)System.Configuration.ConfigurationSettings.GetConfig("ConfigSection/MelissaDataInfo");
                UpdateDataModel(ref melissaDataInfo, ref nvc);
                nvc = null;

                nvc = (NameValueCollection)System.Configuration.ConfigurationSettings.GetConfig("ConfigSection/USDataLeads");
                UpdateDataModel(ref usDataLeadsInfo, ref nvc);
                nvc = null;


                nvc = (NameValueCollection)System.Configuration.ConfigurationSettings.GetConfig("ConfigSection/ProductColorKeys");
                UpdateDataModel(ref productColorKeys, ref nvc);
                nvc = null;

                nvc = (NameValueCollection)System.Configuration.ConfigurationSettings.GetConfig("ConfigSection/VariantDataTypes");
                UpdateDataModel(ref variantDataTypes, ref nvc);
                nvc = null;

                nvc = (NameValueCollection)System.Configuration.ConfigurationSettings.GetConfig("ConfigSection/ShipmentTypes");
                UpdateDataModel(ref shipmentTypes, ref nvc);
                nvc = null;



                nvc = (NameValueCollection)System.Configuration.ConfigurationSettings.GetConfig("ConfigSection/MailContentHTMLs");
                UpdateDataModel(ref mailContentHtmls, ref nvc);
                nvc = null;

                nvc = (NameValueCollection)System.Configuration.ConfigurationSettings.GetConfig("ConfigSection/DCDefaultImagesToBeIgnored");
                UpdateDataModel(ref dcDefaultimagestobeignored, ref nvc);
                nvc = null;

                nvc = (NameValueCollection)System.Configuration.ConfigurationSettings.GetConfig("ConfigSection/DCColorDesignations");
                UpdateDataModel(ref dcColorDesignations, ref nvc);
                nvc = null;

                BCProfile = (NameValueCollection)System.Configuration.ConfigurationSettings.GetConfig("ConfigSection/BCProfile");

                PCProfile = (NameValueCollection)System.Configuration.ConfigurationSettings.GetConfig("ConfigSection/PCProfile");

                ALProfile = (NameValueCollection)System.Configuration.ConfigurationSettings.GetConfig("ConfigSection/ALProfile");

                LHProfile = (NameValueCollection)System.Configuration.ConfigurationSettings.GetConfig("ConfigSection/LHProfile");

                TemplateCategory = (NameValueCollection)System.Configuration.ConfigurationSettings.GetConfig("ConfigSection/TemplateCategory");

            }
            catch (Exception ex)
            {
                Bestprintbuy.Admin.Utilities.FileLogger.WriteException(ex);
                //Console.WriteLine("Message : " + ex.Message);
                //Console.ReadLine();
            }
        }

        private static void UpdateDataModel(ref Hashtable table, ref NameValueCollection nvc)
        {
            if (nvc != null)
            {
                foreach (string key in nvc.AllKeys)
                {
                    table.Add(key, nvc.Get(key));
                }
            }
        }




        public Object Create(Object parent, object configContext, XmlNode section)
        {

            NameValueCollection settings = null;

            try
            {
                NameValueSectionHandler baseHandler = new NameValueSectionHandler();
                settings = (NameValueCollection)baseHandler.Create(parent, configContext, section);
            }
            catch
            {
                settings = null;
            }

            if (settings == null && initConfigSection)
            {
                loggingEnabled = LOGGING_ENABLED_DEFAULT;
                loggingFile = LOGGING_FILE_DEFAULT;
                loggingLevel = LOGGING_LEVEL_DEFAULT;
                loggingSwitchName = LOGGING_SWITCHNAME_DEFAULT;
                loggingSwitchDescription = LOGGING_SWITCHDESCRIPTION_DEFAULT;

            }
            else if (initConfigSection)
            {
                loggingEnabled = ReadSetting(settings, LOGGING_ENABLED, LOGGING_ENABLED_DEFAULT);
                loggingFile = ReadSetting(settings, LOGGING_FILE, LOGGING_FILE_DEFAULT);
                loggingLevel = ReadSetting(settings, LOGGING_LEVEL, LOGGING_LEVEL_DEFAULT);
                loggingSwitchName = ReadSetting(settings, LOGGING_SWITCHNAME, LOGGING_SWITCHNAME_DEFAULT);
                loggingSwitchDescription = ReadSetting(settings, LOGGING_SWITCHDESCRIPTION, LOGGING_SWITCHDESCRIPTION_DEFAULT);
                port = ReadSetting(settings, REMOTING_PORT, 8085);
                channel = ReadSetting(settings, REMOTING_CHANNEL, "tcp");
                hostname = ReadSetting(settings, REMOTING_HOST_NAME, "localhost");
                uploadSize = ReadSetting(settings, FILE_UPLOAD_SIZE, 0);
                dotEditURL = ReadSetting(settings, PAGEFLEX_EDIT_HOST_URL, "http://localhost/.Edit");

                loginAdminUrl = ReadSetting(settings, LOGIN_ADMIN_URL, "");
                loginSupplierUrl = ReadSetting(settings, LOGIN_SUPPLIER_URL, "");
                loginCustomerUrl = ReadSetting(settings, LOGIN_CUSTOMER_URL, "");
                adminHomeUrl = ReadSetting(settings, HOME_ADMIN_URL, "");
                supplierHomeUrl = ReadSetting(settings, HOME_SUPPLIER_URL, "");
                customerHomeUrl = ReadSetting(settings, HOME_CUSTOMER_URL, "");
                failUrl = ReadSetting(settings, FAIL_URL, "");
                supportedImageTypes = ReadSetting(settings, SUPPORTED_IMAGE_TYPES, "jpeg,gif,tiff,eps,pdf");
                designResolution = ReadSetting(settings, DESIGN_UPLOAD_RESOLUTION, 300);
                strPhotoDimensions = ReadSetting(settings, PPSOL_PHOTO_DIMENSIONS, strPhotoDimensions);
                accessErrorUrl = ReadSetting(settings, ACCESS_ERROR_URL, "/AccessError.aspx");
                ppsolSupportEmail = ReadSetting(settings, BPB_SUPPORT_MAILID, "support@bestprintbuy.com");
                isProduction = Convert.ToBoolean(ReadSetting(settings, BPB_IS_PRODUCTION, "True"));
                ignoreVerisign = Convert.ToBoolean(ReadSetting(settings, BPB_IGNORE_VERISIGN, "True"));
                ignoreOrderStatusMails = Convert.ToBoolean(ReadSetting(settings, BPB_IGNORE_ORDERSTATUSMAIL, "False"));
                redirectCampaignMailToAdmin = Convert.ToBoolean(ReadSetting(settings, BPB_REDIRECT_CAMPAIGNMAIL, "False"));

                upstrackingUrl = Convert.ToString(ReadSetting(settings, BPB_UPS_TRACKURL, "#"));
                uspstrackingurl = Convert.ToString(ReadSetting(settings, BPB_USPS_TRACKURL, "#"));
                fedextrackingurl = Convert.ToString(ReadSetting(settings, BPB_FEDEX_TRACKURL, "#"));
                xSellNotePads = Convert.ToString(ReadSetting(settings, BPB_XSELL_NOTEPADS, "Both"));
                customerCareEmail = ReadSetting(settings, BPB_CUSTOMERCARE_MAILID, "customercare@bestprintbuy.com");
                catalogrequestEmail = ReadSetting(settings, BPB_CATALOG_REQUEST_MAILID, "customercare@bestprintbuy.com");
                stateTax = Convert.ToDouble(ReadSetting(settings, BPB_STATE_TAX, "0.0"));
                stateTaxLabel = ReadSetting(settings, BPB_STATE_TAX_LABEL, "State Tax");
                uploadDesignProcessCost = Convert.ToDecimal(ReadSetting(settings, BPB_UPLOADDESIGNPROCESSCOST, "0"));

                dmStandardFirstClassCost = Convert.ToDecimal(ReadSetting(settings, BPB_DM_PCSTANDARD_FIRSTCLASS, "0.28"));
                dmOVEFirstClassCost = Convert.ToDecimal(ReadSetting(settings, BPB_DM_PCOVE_FIRSTCLASS, "0.43"));
                dmOVEStandardBulkCost = Convert.ToDecimal(ReadSetting(settings, BPB_DM_PCOVE_STANDARDBULK, "0.32"));

                pdfSaveFolder = ReadSetting(settings, BPB_PDFSAVEFOLDER, "");
                pdfSaveFolderServer = ReadSetting(settings, BPB_PDFSAVEFOLDER_SERVER, "");

                bpbCMYKProfile = ReadSetting(settings, BPB_PROFILE_CMYK, "");
                bpbToCMYKprofile = ReadSetting(settings, BPB_PROFILE_TOCMYK, "");
                bpbRGBProfile = ReadSetting(settings, BPB_PROFILE_RGB, "");
                bpbPDFLibLicenseKey = ReadSetting(settings, BPB_PDFLIB_LICENSE_KEY, "0");
                isJPGFromCMYKPDF = Convert.ToBoolean(ReadSetting(settings, BPB_IS_JPG_FROM_CMYKPDF, "True"));
                adminUIURL = ReadSetting(settings, BPB_ADMIN_USER_INFO_URL, "");
                customerProofsEmail = ReadSetting(settings, BPB_CUSTOMER_PROOFS_MAILID, "proofs@bestprintbuy.com");
            }

            return settings;
        }

        /// <summary>
        ///     String version of ReadSetting.
        ///     <remarks>
        ///         Reads a setting from a hashtable and converts it to the correct
        ///         type. One of these functions is provided for each type
        ///         expected in the hash table. These are public so that other
        ///         classes don't have to duplicate them to read settings from
        ///         a hash table.
        ///     </remarks>
        ///     <param name="settings">The Hashtable to read from.</param>
        ///     <param name="key">A key for the value in the Hashtable.</param>
        ///     <param name="defaultValue">The default value if the item is not found.</param>
        ///     <retvalue>
        ///		    <para>value: from the hash table</para>
        ///         <para>
        ///             default: if the item is not in the table or cannot be case to the expected type.
        ///		    </para>
        ///	    </retvalue>
        /// </summary>
        public static String ReadSetting(NameValueCollection settings, String key, String defaultValue)
        {
            try
            {
                Object setting = settings[key];

                return (setting == null) ? defaultValue : (String)setting;
            }
            catch
            {
                return defaultValue;
            }
        }

        /// <summary>
        ///     Boolean version of ReadSetting.
        ///     <remarks>
        ///         Reads a setting from a hashtable and converts it to the correct
        ///         type. One of these functions is provided for each type
        ///         expected in the hash table. These are public so that other
        ///         classes don't have to duplicate them to read settings from
        ///         a hash table.
        ///     </remarks>
        ///     <param name="settings">The Hashtable to read from.</param>
        ///     <param name="key">A key for the value in the Hashtable.</param>
        ///     <param name="defaultValue">The default value if the item is not found.</param>
        ///     <retvalue>
        ///		    <para>value: from the hash table</para>
        ///         <para>
        ///             default: if the item is not in the table or cannot be case to the expected type.
        ///		    </para>
        ///	    </retvalue>
        /// </summary>
        public static bool ReadSetting(NameValueCollection settings, String key, bool defaultValue)
        {
            try
            {
                Object setting = settings[key];

                return (setting == null) ? defaultValue : Convert.ToBoolean((String)setting);
            }
            catch
            {
                return defaultValue;
            }
        }

        /// <summary>
        ///     int version of ReadSetting.
        ///     <remarks>
        ///         Reads a setting from a hashtable and converts it to the correct
        ///         type. One of these functions is provided for each type
        ///         expected in the hash table. These are public so that other
        ///         classes don't have to duplicate them to read settings from
        ///         a hash table.
        ///     </remarks>
        ///     <param name="settings">The Hashtable to read from.</param>
        ///     <param name="key">A key for the value in the Hashtable.</param>
        ///     <param name="defaultValue">The default value if the item is not found.</param>
        ///     <retvalue>
        ///		    <para>value: from the hash table</para>
        ///         <para>
        ///             default: if the item is not in the table or cannot be case to the expected type.
        ///		    </para>
        ///	    </retvalue>
        /// </summary>
        public static int ReadSetting(NameValueCollection settings, String key, int defaultValue)
        {
            try
            {
                Object setting = settings[key];

                return (setting == null) ? defaultValue : Convert.ToInt32((String)setting);
            }
            catch
            {
                return defaultValue;
            }
        }

        /// <summary>
        ///     TraceLevel version of ReadSetting.
        ///     <remarks>
        ///         Reads a setting from a hashtable and converts it to the correct
        ///         type. One of these functions is provided for each type
        ///         expected in the hash table. These are public so that other
        ///         classes don't have to duplicate them to read settings from
        ///         a hash table.
        ///     </remarks>
        ///     <param name="settings">The Hashtable to read from.</param>
        ///     <param name="key">A key for the value in the Hashtable.</param>
        ///     <param name="defaultValue">The default value if the item is not found.</param>
        ///     <retvalue>
        ///		    <para>value: from the hash table</para>
        ///         <para>
        ///             default: if the item is not in the table or cannot be case to the expected type.
        ///		    </para>
        ///	    </retvalue>
        /// </summary>
        public static TraceLevel ReadSetting(NameValueCollection settings, String key, TraceLevel defaultValue)
        {
            try
            {
                Object setting = settings[key];

                return (setting == null) ? defaultValue : (TraceLevel)Convert.ToInt32((String)setting);
            }
            catch
            {
                return defaultValue;
            }
        }

        /// Application properties.
        /// <value>
        ///     Property TracingEnabled is used to get the configuration setting, defaulting to False on error.
        /// </value>
        public static bool LoggingEnabled
        {
            get
            {
                return loggingEnabled;
            }
        }

        /// <value>
        ///     Property TracingTraceFile is used to get the full path name to the file that contains trace 
        ///     settings, defaults to ApplicationTrace.txt.
        /// </value>
        public static String LoggingFile
        {
            get
            {
                return loggingFile;
            }
        }

        /// <value>
        ///     Property TracingTraceFile is used to get the highest logging level that should be written to 
        ///     the logging file, defaults to TraceLevel.Verbose (however, TracingEnabled defaults
        ///     to False, so you have to turn it on explicitly).
        /// </value>
        public static TraceLevel LoggingLevel
        {
            get
            {
                return loggingLevel;
            }
        }

        /// <value>
        ///   Property TracingSwitchName is used to get the trace switch name, defaults to ApplicationTraceSwitch.
        /// </value>
        public static String LoggingSwitchName
        {
            get
            {
                return loggingSwitchName;
            }
        }

        /// <value>
        ///   Property TracingSwitchDescription is used to get the trace settings file, defaults to 
        ///     "Application error and logging information".
        /// </value>
        public static String LoggingSwitchDescription
        {
            get
            {
                return loggingSwitchDescription;
            }
        }

        public static int Port
        {
            get
            {
                return port;
            }
        }

        public static String Hostname
        {
            get
            {
                return hostname;
            }
        }

        public static String Channel
        {
            get
            {
                return channel;
            }
        }

        public static int UploadSize
        {
            get
            {
                return uploadSize;
            }
        }

        public static String DotEditURL
        {
            get
            {
                return dotEditURL;
            }
        }

        public static String AdminLoginUrl
        {
            get
            {
                return loginAdminUrl;
            }
        }

        public static String SupplierLoginUrl
        {
            get
            {
                return loginSupplierUrl;
            }
        }

        public static String CustomerLoginUrl
        {
            get
            {
                return loginCustomerUrl;
            }
        }

        public static String CustomerHomeUrl
        {
            get
            {
                return customerHomeUrl;
            }
        }

        public static String AdminHomeUrl
        {
            get
            {
                return adminHomeUrl;
            }
        }

        public static String SupplierHomeUrl
        {
            get
            {
                return supplierHomeUrl;
            }
        }



        public static String FailUrl
        {
            get
            {
                return failUrl;
            }
        }

        public static Hashtable AdminLoginScreens
        {
            get
            {
                return adminLoginScreens;
            }
        }

        public static Hashtable PPSOLScreens
        {
            get
            {
                return ppsolScreens;
            }
        }
        public static Hashtable SupplierScreens
        {
            get
            {
                return supplierScreens;
            }
        }
        public static Hashtable PortalScreens
        {
            get
            {
                return portalScreens;
            }
        }

        /*public static Hashtable DesignTemplates
        {
            get
            {
                return designTemplates;
            }
        }*/

        /*public static String GetDesingTemplate(String designMode)
        {
            Object template = designTemplates[designMode];
            if (template != null)
            {
                return Convert.ToString(template);
            }
            return null;
        }*/

        public static Hashtable DCDefaultimagestobeignored
        {
            get
            {
                return dcDefaultimagestobeignored;
            }
        }


        public static Hashtable DCColorDesignations
        {
            get
            {
                return dcColorDesignations;
            }
        }

        public static Hashtable ProductVariants
        {
            get
            {
                return productVariants;
            }
        }

        public static String GetProductVariant(String variantName)
        {
            Object variants = productVariants[variantName];
            if (variants != null)
            {
                return Convert.ToString(variants);
            }
            return null;
        }

        public static bool ISDCIgnoreDefaultImage(String ImageName)
        {

            Object Image = DCDefaultimagestobeignored[ImageName];
            if (Image != null)
            {
                return true;
            }
            return false;
        }

        public static bool ISColorDesignation(String ImageName)
        {

            Object Image = DCColorDesignations[ImageName];
            if (Image != null)
            {
                return true;
            }
            return false;
        }

        public static Hashtable DisplayVariants
        {
            get
            {
                return displayVariants;
            }
        }

        public static String GetDisplayVariant(String cartType)
        {
            Object variants = displayVariants[cartType];
            if (variants != null)
            {
                return Convert.ToString(variants);
            }
            return null;
        }

        public static Hashtable ProductTypeKeys
        {
            get
            {
                return productTypeKeys;
            }
        }

        public static String GetProductTypeKey(String productType)
        {
            Object typeKey = productTypeKeys[productType];
            if (typeKey != null)
            {
                return Convert.ToString(typeKey);
            }
            return null;
        }

        public static Hashtable ProductColorKeys
        {
            get
            {
                return productColorKeys;
            }
        }

        public static String GetProductColorKey(String productColor)
        {
            Object colorKey = productColorKeys[productColor];
            if (colorKey != null)
            {
                return Convert.ToString(colorKey);
            }
            return null;
        }

        public static Hashtable VariantDataTypes
        {
            get
            {
                return variantDataTypes;
            }
        }

        public static Hashtable ShipmentTypes
        {
            get
            {
                return shipmentTypes;
            }
        }

        public static String GetVariantDataType(String variantKey)
        {
            Object variantDataType = variantDataTypes[variantKey];
            if (variantDataType != null)
            {
                return Convert.ToString(variantDataType);
            }
            return null;
        }

        public static String GetShipmentTypeType(String shipmentKey)
        {
            Object shipmentType = ShipmentTypes[shipmentKey];
            if (shipmentType != null)
            {
                return Convert.ToString(shipmentType);
            }
            return null;
        }

        public static Hashtable MailContentHtmls
        {
            get
            {
                return mailContentHtmls;
            }
        }

        public static String GetEmailHTMLLoc(String htimlKey)
        {
            Object emailHtmlLoc = mailContentHtmls[htimlKey];
            if (emailHtmlLoc != null)
            {
                return Convert.ToString(emailHtmlLoc);
            }
            return null;
        }


        public static Hashtable ImageDirs
        {
            get
            {
                return imageDirs;
            }
        }


        public static string SupportedImageTypes
        {
            get
            {
                return supportedImageTypes;
            }
        }


        public static string paymentUser
        {
            get
            {
                return Convert.ToString(paymentInfo["user"]);
            }
        }

        public static string MelissaDataFiles
        {
            get
            {
                return Convert.ToString(melissaDataInfo["MelissaDataFiles"]);
            }
        }

        public static string MelissaLicenseString
        {
            get
            {
                return Convert.ToString(melissaDataInfo["LicenseString"]);
            }
        }

        public static string paymentPassword
        {
            get
            {
                return Convert.ToString(paymentInfo["password"]);
            }
        }

        public static string paymentPartner
        {
            get
            {
                return Convert.ToString(paymentInfo["partner"]);
            }
        }

        public static string paymentHost
        {
            get
            {
                string host = Convert.ToString(paymentInfo["hostAddress"]);
                if (host == string.Empty)
                    host = Convert.ToString(paymentInfo["testHostAddress"]);
                return host;
            }
        }

        public static int paymentPort
        {
            get
            {
                return Convert.ToInt32(paymentInfo["hostPort"]);
            }
        }

        public static int paymentTimeOut
        {
            get
            {
                return Convert.ToInt32(paymentInfo["timeout"]);
            }
        }

        public static string paymentVendor
        {
            get
            {
                return Convert.ToString(paymentInfo["vendor"]);
            }
        }

        public static string paymentProxyHost
        {
            get
            {
                return Convert.ToString(paymentInfo["proxyAddress"]);
            }
        }

        public static string GetPaymentError(int errorcode)
        {
            string result = (string)paymentInfo[errorcode.ToString()];
            if (result == null)
            {
                if (errorcode > 0)
                    result = (string)paymentInfo["MoreThanZero"];
                else
                    result = (string)paymentInfo["LessThanZero"];
            }
            return result;
        }

        public static string AccessErrorUrl
        {
            get
            {
                return accessErrorUrl;
            }
        }

        public static string BPBSupportEmailID
        {
            get
            {
                return ppsolSupportEmail;
            }
        }

        public static string BPBCustomerCareEmail
        {
            get
            {
                return customerCareEmail;
            }
        }

        public static string BPBCatalogRequestEmail
        {
            get
            {
                return catalogrequestEmail;
            }
        }
        public static string BPBCustomerProofsEmail
        {
            get
            {
                return customerProofsEmail;
            }
        }

        public static string StateTaxLabel
        {
            get
            {
                return stateTaxLabel;
            }
        }

        public static decimal UploadDesignProcessCost
        {
            get
            {
                return uploadDesignProcessCost;
            }
        }

        public static decimal DMStandardFirstClassCost
        {
            get
            {
                return dmStandardFirstClassCost;
            }
        }

        public static decimal DMOVEFirstClassCost
        {
            get
            {
                return dmOVEFirstClassCost;
            }
        }

        public static decimal DMOVEStandardBulkCost
        {
            get
            {
                return dmOVEStandardBulkCost;
            }
        }

        public static bool ISProductionRelease
        {
            get
            {
                return isProduction;
            }
        }

        public static bool ISIgnoreVerisign
        {
            get
            {
                return ignoreVerisign;
            }
        }

        public static string UPSTrackUrl
        {
            get
            {
                return upstrackingUrl;
            }
        }

        public static string USPSTrackUrl
        {
            get
            {
                return uspstrackingurl;
            }
        }
        public static string FEDEXTrackUrl
        {
            get
            {
                return fedextrackingurl;
            }
        }


        public static bool ISIgnoreOrderStatusMail
        {
            get
            {
                return ignoreOrderStatusMails;
            }
        }

        public static bool RedirectCampaignMailToAdmin
        {
            get
            {
                return redirectCampaignMailToAdmin;
            }
        }

        public static bool ISXSellSmallNotepds
        {
            get
            {
                return xSellNotePads.ToLower() == "both" || xSellNotePads.ToLower() == "small";
            }
        }

        public static bool ISXSellLargeNotepds
        {
            get
            {
                return xSellNotePads.ToLower() == "both" || xSellNotePads.ToLower() == "large";
            }
        }

        public static double StateTax
        {
            get
            {
                return stateTax;
            }
        }

        public static int paymentProxyPort
        {
            get
            {
                return Convert.ToInt32(paymentInfo["proxyPort"]);
            }
        }

        public static string paymentProxyLogid
        {
            get
            {
                return Convert.ToString(paymentInfo["proxyLogid"]);
            }
        }

        public static string paymentProxyPassword
        {
            get
            {
                return Convert.ToString(paymentInfo["proxyPassword"]);
            }
        }


        public static int DesignResolution
        {
            get
            {
                return designResolution;
            }
        }

        public static string PhotoDimensions
        {
            get
            {
                return strPhotoDimensions;
            }
        }

        public static NameValueCollection BCProfileMap
        {
            get
            {
                return BCProfile;
            }
        }
        public static NameValueCollection PCProfileMap
        {
            get
            {
                return PCProfile;
            }
        }
        public static NameValueCollection ALProfileMap
        {
            get
            {
                return ALProfile;
            }
        }
        public static NameValueCollection LHProfileMap
        {
            get
            {
                return LHProfile;
            }
        }
        public static NameValueCollection TemplateCategoryMap
        {
            get
            {
                return TemplateCategory;
            }
        }
        public static NameValueCollection GetProfileMap(string mapFor)
        {
            if ("BC".Equals(mapFor)) return BCProfileMap;
            else if ("PC".Equals(mapFor)) return PCProfileMap;
            else if ("AL".Equals(mapFor)) return ALProfileMap;
            else if ("LH".Equals(mapFor)) return LHProfileMap;
            else return new NameValueCollection();

        }

        public static String PDFSavedFolder
        {
            get
            {
                return pdfSaveFolder;
            }
        }

        public static String PDFSavedFolderServer
        {
            get
            {
                return pdfSaveFolderServer;
            }
        }

        public static String BPBCMYKProfile
        {
            get
            {
                return bpbCMYKProfile;
            }
        }

        public static String BPBT0CMYKprofile
        {
            get
            {
                return bpbToCMYKprofile;
            }
        }

        public static String BPBRGBProfile
        {
            get
            {
                return bpbRGBProfile;
            }
        }

        public static String BPBPDFLibLicenseKey
        {
            get
            {
                return bpbPDFLibLicenseKey;
            }
        }

        public static String AdminUserInterfaceUrl
        {
            get
            {
                return adminUIURL;
            }
        }

        public static bool IsJPGFromCMYKPDF
        {
            get
            {
                return isJPGFromCMYKPDF;
            }
        }

        public static double GetApplicableStateTax(string stateAbbr)
        {
            if ("bpb.statetax." + stateAbbr == BPB_STATE_TAX)
            {
                return StateTax;
            }
            return 0.0;
        }

        #region US Leads Information

        public static String USLeadsTargetURL
        {
            get
            {
                return Convert.ToString(usDataLeadsInfo["USLeadsTargetURL"]);
            }
        }
        public static String USLeadsWebServiceURL
        {
            get
            {
                return Convert.ToString(usDataLeadsInfo["USLeadsWebServiceURL"]);
            }
        }
        public static String USLeadsUserID
        {
            get
            {
                return Convert.ToString(usDataLeadsInfo["USLeadsUserID"]);
            }
        }
        public static String USLeadsPassword
        {
            get
            {
                return Convert.ToString(usDataLeadsInfo["USLeadsPassword"]);
            }
        }
        public static String USLeadsClientID
        {
            get
            {
                return Convert.ToString(usDataLeadsInfo["USLeadsClientID"]);
            }
        }
        public static String USLeadsDataSource
        {
            get
            {
                return Convert.ToString(usDataLeadsInfo["USLeadsDataSource"]);
            }
        }
        public static String USLeadsOrderMailTo
        {
            get
            {
                return Convert.ToString(usDataLeadsInfo["USLeadsOrderMailTo"]);
            }
        }

        #endregion

    }
}