﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace Bestprintbuy.Admin.Web.Common
{
    public class MD5Encryption
    {
        public static string Encrypt(string input)
        {
            return FormsAuthentication.HashPasswordForStoringInConfigFile(input, "md5");
        }
    }
}