﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading;
using System.ComponentModel;
using Bestprintbuy.Admin.Utilities;
using Bestprintbuy.Admin.DataModel;

namespace Bestprintbuy.Admin.Web.Common
{
    public class MailSender
    {
        private string fromEmailId = "";

        private string toEmailId = "";

        private string mailMessage = "";

        private string mailSubject = "";

        private string format = "HTML";

        private bool ccSelf = false;
        private string attachement = "";
        private string mailServer = "";
        private ReportsModel emailData;
        bool enableSSL = false;

        public MailSender(string fromEmailId, string toEmailId,
            string mailMessage, string mailSubject,
            string format, bool ccSelf)
        {
            this.fromEmailId = fromEmailId;
            this.toEmailId = toEmailId;
            this.mailMessage = mailMessage;
            this.mailSubject = mailSubject;
            this.format = format;
            this.ccSelf = ccSelf;
        }

        public MailSender(string fromEmailId, string toEmailId,
             string mailMessage, string mailSubject, string mailServer,
             string format, bool ccSelf, string attachmentLoc)
        {
            this.fromEmailId = fromEmailId;
            this.toEmailId = toEmailId;
            this.mailMessage = mailMessage;
            this.mailSubject = mailSubject;
            this.mailServer = mailServer;
            this.format = format;
            this.ccSelf = ccSelf;
            this.attachement = attachmentLoc;
        }

        public MailSender(ReportsModel emailData)
        {
            // TODO: Complete member initialization
            this.emailData = emailData;
        }

        //static bool mailSent = false;
        private static void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
        {
            // Get the unique identifier for this asynchronous operation.
            String token = (string)e.UserState;

            if (e.Cancelled)
            {
                //Console.WriteLine("[{0}] Send canceled.", token);
            }
            if (e.Error != null)
            {
                //Console.WriteLine("[{0}] {1}", token, e.Error.ToString());
            }
            else
            {
                //Console.WriteLine("Message sent.");
            }
            //mailSent = true;
        }


        internal void SendEmail()
        {
            try
            {
                SmtpClient client = new SmtpClient();
                MailAddress from = new MailAddress(fromEmailId, fromEmailId, Encoding.UTF8);

                //TODO : Need to modify once testing is over
                MailAddress to = new MailAddress(toEmailId);

                MailMessage mailmessage = new MailMessage(from, to);
                mailmessage.Body = mailMessage;

                if (ccSelf) mailmessage.CC.Add(fromEmailId);
                mailmessage.IsBodyHtml = format == "HTML";
                mailmessage.BodyEncoding = System.Text.Encoding.UTF8;

                mailmessage.Subject = mailSubject;
                mailmessage.SubjectEncoding = System.Text.Encoding.UTF8;


                client.Send(mailmessage);
                mailmessage.Dispose();
            }
            catch (Exception ex)
            {
                //throw ex;                
                FileLogger.WriteException(ex);
            }

        }

        internal void SendEmailCampaign()
        {
            bool ccSelf = false;
            try
            {
                ccSelf = Convert.ToBoolean(ConfigurationManager.AppSettings["CopyEmailCampaignToAdmin"]);
            }
            catch
            {

            }
            foreach (ReportsModel.EmailListRow emailRow in emailData.EmailList.Rows)
            {
                try
                {
                    SmtpClient client = new SmtpClient();
                    MailAddress from = new MailAddress(emailRow.from_email, emailRow.from_name, Encoding.UTF8);

                    //TODO : Need to modify once testing is over
                    MailAddress to = new MailAddress(emailRow.email_id);
                    MailMessage mailmessage = new MailMessage(from, to);
                    if (ccSelf) mailmessage.CC.Add(emailRow.from_email);
                    mailmessage.Body = emailRow.message;
                    mailmessage.IsBodyHtml = true;
                    mailmessage.BodyEncoding = System.Text.Encoding.UTF8;
                    mailmessage.Subject = emailRow.subject;
                    mailmessage.SubjectEncoding = System.Text.Encoding.UTF8;
                    client.EnableSsl = enableSSL;
                    client.Send(mailmessage);
                    mailmessage.Dispose();
                }
                catch (Exception ex)
                {
                    //throw ex;                
                    //ExceptionManager.Publish(ex);
                }
            }
        }
    }
}