﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading;
using System.Configuration;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Utilities;
using Bestprintbuy.Admin.Services;

namespace Bestprintbuy.Admin.Web.Common
{
    public class EmailProcessor
    {
        public static void SendMCARegistrationEmail(string msgBody, CustomerModel.CustomersRow custrow)
        {
            try
            {
                string UserName = "";

                if (!custrow.Iscus_firstnameNull())
                    UserName = UserName + custrow.cus_firstname;

                if (!custrow.Iscus_lastnameNull())
                    UserName = UserName + " " + custrow.cus_lastname;

                string emailID = custrow.email;
                string password = custrow.actualpassword;

                if (UserName.Trim() == "")
                    UserName = UserName + ",";
                else
                    UserName = emailID + ",";


                msgBody = string.Format(msgBody, UserName, emailID, password, DateTime.Now.ToString("MM/dd/yyyy hh:mm tt"));

                msgBody = msgBody.Replace("&#123;", "{");
                msgBody = msgBody.Replace("&#125;", "}");

                msgBody = msgBody.Replace("##kiosk##", "");
                msgBody = msgBody.Replace("##ecid##", "");
                msgBody = msgBody.Replace("##eckey##", "");

                string fromEmail = WebConfiguration.BPBCustomerCareEmail;

                bool sendMail = false;
                try
                {
                    sendMail = (ConfigurationManager.AppSettings["SendMails"].ToString().ToLower() == "yes");
                }
                catch { }

                if (sendMail)
                {
                    MailSender ms = new MailSender(fromEmail, emailID, msgBody,
                    "New User Registration Bestprintbuy.com", "HTML", false);
                    //MailSender ms = new MailSender(fromEmail, "sreedhara@inforaise.com", msgBody,
                    //"New User Registration Bestprintbuy.com", "HTML", false);
                    Thread mailThread = new Thread(new ThreadStart(ms.SendEmail));
                    mailThread.Start();
                }
                else
                {
                    FileLogger.WriteEntry("Mail sending restricted");
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                //throw ex;
            }
        }


        public static void SendCampaingnEmails(string fromEmail, List<string> Toemails, List<string> msgBody, string subject)
        {
            bool sendMail = false;
            try
            {
                sendMail = (ConfigurationManager.AppSettings["SendMails"].ToString().ToLower() == "yes");
            }
            catch { }

            if (sendMail)
            {
                try
                {
                    for (int i = 0; i < Toemails.Count; i++)
                    {
                        MailSender ms = new MailSender(fromEmail, Toemails[i], msgBody[i], subject, "HTML", false);
                        //MailSender ms = new MailSender(fromEmail, "sreedhara@inforaise.com", msgBody[i], subject, "HTML", false);
                        Thread mailThread = new Thread(new ThreadStart(ms.SendEmail));
                        mailThread.Start();
                    }
                }
                catch (Exception ex)
                {
                    FileLogger.WriteException(ex);
                    //throw ex;
                }
            }
            else
            {
                FileLogger.WriteEntry("Mail sending restricted");
            }
        }





        public static void InitiateCancelMails(string FromMailId, System.Collections.IList PODetailKeys, int productTypeKey, int userKey, string mailContent, string connString)
        {
            bool sendMail = false;
            try
            {
                sendMail = (ConfigurationManager.AppSettings["SendMails"].ToString().ToLower() == "yes");
            }
            catch { }

            if (sendMail)
            {

                try
                {
                    SupplierPOModel poModel = new SupplierPOModel();
                    string errMessages = string.Empty;

                    poModel = SupplierPOService.GetCancelMailOrders(userKey, productTypeKey, connString);
                    if (mailContent == string.Empty)
                    {
                        mailContent = GetDefaultCancelContent();
                    }

                    if (PODetailKeys == null)
                    {
                        PODetailKeys = new System.Collections.ArrayList();
                    }

                    if (poModel.CustOrders.Rows.Count > 0)
                    {
                        int fkeyecattribute = 0;
                        string strEcAttribute = string.Empty;
                        foreach (SupplierPOModel.CustOrdersRow mailRow in poModel.CustOrders.Rows)
                        {
                            fkeyecattribute = -1;
                            strEcAttribute = string.Empty;
                            //Not strEcAttribute = String.Empty
                            if (!mailRow.Isfkey_ec_attributeNull())
                            {
                                fkeyecattribute = mailRow.fkey_ec_attribute;
                                strEcAttribute = GetExclusiveCustomer(fkeyecattribute);
                            }


                            if (PODetailKeys.Contains(Convert.ToInt32(mailRow.pkey_cust_po_dtl)) == true)
                            {
                                bool isMailSent = true;
                                try
                                {
                                    string orderstatus = "Cancelled";
                                    string subject = "Your order has been {0} - Order Number {1}";
                                    subject = string.Format(subject, orderstatus, mailRow.ordernumber);
                                    string message = string.Empty;

                                    message = mailContent;
                                    message = string.Format(message, orderstatus, mailRow.ordernumber, mailRow.customer_name, DateTime.Now.ToString("MM/dd/yyyy hh:mm tt"));
                                    message = message.Replace("&#123;", "{");
                                    message = message.Replace("&#125;", "}");
                                    message = message.Replace("##kiosk##", "/" + strEcAttribute);
                                    message = message.Replace("##ecid##", strEcAttribute);
                                    message = message.Replace("##eckey##", (fkeyecattribute > 0 ? Convert.ToString(fkeyecattribute) : ""));

                                    MailSender ms = new MailSender(FromMailId, mailRow.customer_email_id, message, subject, "HTML", false);
                                    //MailSender ms = new MailSender(FromMailId, "sreedhara@inforaise.com", message, subject, "HTML", false);
                                    Thread mailThread = new Thread(new ThreadStart(ms.SendEmail));
                                    mailThread.Start();

                                }
                                catch (Exception ex)
                                {
                                    isMailSent = false;
                                }
                                if (isMailSent == true || WebConfiguration.ISIgnoreOrderStatusMail)
                                {
                                    mailRow.email_date = System.DateTime.Now;
                                }
                                else
                                {
                                    errMessages = errMessages + "Unable to send mail to: " + mailRow.customer_email_id;
                                    errMessages = errMessages + ". PO Number: " + mailRow.ordernumber;
                                    errMessages = errMessages + ". PO Detail Ref Key: " + mailRow.pkey_cust_po_dtl + "\\n";
                                }
                            }
                        }
                        SupplierPOService.UpdateOrdersWithMailSentStatus(userKey, poModel, errMessages, connString);
                    }

                }
                catch (Exception ex)
                {
                    FileLogger.WriteException(ex);
                }
            }
            else
            {
                FileLogger.WriteEntry("Mail sending restricted");
            }
        }

        public static void InitiateMails(string FromMailId, System.Collections.IList PODetailKeys, int productTypeKey, int userKey, string mailContent, string connString)
        {
            bool sendMail = false;
            string errMessages = "";
            try
            {
                sendMail = (ConfigurationManager.AppSettings["SendMails"].ToString().ToLower() == "yes");
            }
            catch { }

            if (sendMail)
            {

                try
                {
                    SupplierPOModel poModel = new SupplierPOModel();
                    

                    poModel = SupplierPOService.GetMailOrders(userKey, productTypeKey, connString);
                    if (mailContent == string.Empty)
                    {
                        mailContent = GetDefaultContent();
                    }

                    if (PODetailKeys == null)
                    {
                        PODetailKeys = new System.Collections.ArrayList();
                    }

                    if (poModel.CustOrders.Rows.Count > 0)
                    {
                        int fkeyecattribute = 0;
                        string strEcAttribute = string.Empty;
                        foreach (SupplierPOModel.CustOrdersRow mailRow in poModel.CustOrders.Rows)
                        {
                            fkeyecattribute = -1;
                            strEcAttribute = string.Empty;
                            //Not strEcAttribute = String.Empty
                            if (!mailRow.Isfkey_ec_attributeNull())
                            {
                                fkeyecattribute = mailRow.fkey_ec_attribute;
                                strEcAttribute = GetExclusiveCustomer(fkeyecattribute);
                            }


                            if (PODetailKeys.Contains(Convert.ToInt32(mailRow.pkey_cust_po_dtl)) == true)
                            {
                                bool isMailSent = true;
                                try
                                {
                                    string orderstatus = "Shipped";
                                    string shipTracking = String.Empty;
                                    if (!mailRow.Iscarrier_service_nameNull())
                                    {
                                        //orderstatus = orderstatus & " & Shipped"
                                        if (!string.IsNullOrEmpty(mailRow.carrier_service_name))
                                        {
                                            shipTracking = "Shipping Company: <B>" + Convert.ToString(mailRow.carrier_service_name) + "</B><BR>";
                                        }
                                    }
                                    if (!mailRow.Istracking_numberNull())
                                    {
                                        if (!string.IsNullOrEmpty(mailRow.tracking_number.Trim()))
                                        {
                                            if (!string.IsNullOrEmpty(mailRow.tracking_url.Trim()))
                                            {
                                                shipTracking = shipTracking + "Tracking Number: <a href='" + Convert.ToString(mailRow.tracking_url) + "' target=_blank><B>" + Convert.ToString(mailRow.tracking_number) + "</B></a>";
                                            }
                                            else
                                            {
                                                shipTracking = shipTracking + "Tracking Number: <B>" + Convert.ToString(mailRow.tracking_number) + "</B>";
                                            }
                                        }
                                    }

                                    string subject = "Your order has been {0} - Order Number {1}";
                                    subject = string.Format(subject, orderstatus, mailRow.ordernumber);
                                    string message = string.Empty;

                                    message = mailContent;
                                    message = String.Format(message, orderstatus, mailRow.ordernumber, mailRow.customer_name, shipTracking, DateTime.Now.ToString("MM/dd/yyyy hh:mm tt"));
                                    message = message.Replace("&#123;", "{");
                                    message = message.Replace("&#125;", "}");
                                    message = message.Replace("##kiosk##", "/" + strEcAttribute);
                                    message = message.Replace("##ecid##", strEcAttribute);
                                    message = message.Replace("##eckey##", (fkeyecattribute > 0 ? Convert.ToString(fkeyecattribute) : ""));

                                    MailSender ms = new MailSender(FromMailId, mailRow.customer_email_id, message, subject, "HTML", false);
                                    //MailSender ms = new MailSender(FromMailId, "sreedhara@inforaise.com", message, subject, "HTML", false);
                                    Thread mailThread = new Thread(new ThreadStart(ms.SendEmail));
                                    mailThread.Start();

                                }
                                catch (Exception ex)
                                {
                                    isMailSent = false;
                                }
                                if (isMailSent == true || WebConfiguration.ISIgnoreOrderStatusMail)
                                {
                                    mailRow.email_date = System.DateTime.Now;
                                }
                                else
                                {
                                    errMessages = errMessages + "Unable to send mail to: " + mailRow.customer_email_id;
                                    errMessages = errMessages + ". PO Number: " + mailRow.ordernumber;
                                    errMessages = errMessages + ". PO Detail Ref Key: " + mailRow.pkey_cust_po_dtl + "\\n";
                                }
                            }
                        }
                        SupplierPOService.UpdateOrdersWithMailSentStatus(userKey, poModel, errMessages, connString);
                    }

                }
                catch (Exception ex)
                {
                    FileLogger.WriteException(ex);
                    throw ex;
                }
            }
            else
            {
                FileLogger.WriteEntry("Mail sending restricted");
                throw new Exception("Mail sending restricted, Please update config settings");
            }

            if (errMessages != "")
            {
                throw new Exception(errMessages);
            }
        }

        public static void InitiateSendOrderModificationMails(string FromMailId, System.Collections.IList PODetailKeys, int productTypeKey, int userKey, string mailContent, string connString)
        {
            bool sendMail = false;
            try
            {
                sendMail = (ConfigurationManager.AppSettings["SendMails"].ToString().ToLower() == "yes");
            }
            catch { }

            if (sendMail)
            {
                try
                {
                    SupplierPOModel poModel = new SupplierPOModel();
                    string errMessages = string.Empty;

                    poModel = SupplierPOService.GetModificationRequiredMailOrders(userKey, productTypeKey, connString);
                    if (mailContent == string.Empty)
                    {
                        mailContent = GetDefaultSendOrderModificationContent();
                    }

                    if (PODetailKeys == null)
                    {
                        PODetailKeys = new System.Collections.ArrayList();
                    }

                    if (poModel.CustOrders.Rows.Count > 0)
                    {
                        int fkeyecattribute = 0;
                        string strEcAttribute = string.Empty;
                        foreach (SupplierPOModel.CustOrdersRow mailRow in poModel.CustOrders.Rows)
                        {
                            fkeyecattribute = -1;
                            strEcAttribute = string.Empty;
                            //Not strEcAttribute = String.Empty
                            if (!mailRow.Isfkey_ec_attributeNull())
                            {
                                fkeyecattribute = mailRow.fkey_ec_attribute;
                                strEcAttribute = GetExclusiveCustomer(fkeyecattribute);
                            }


                            if (PODetailKeys.Contains(Convert.ToInt32(mailRow.pkey_cust_po_dtl)) == true)
                            {
                                bool isMailSent = true;
                                try
                                {
                                    //string orderstatus = "Cancelled";
                                    string subject = "Your order requires modification - Order Number {0}";
                                    subject = string.Format(subject, mailRow.ordernumber);
                                    string message = string.Empty;

                                    message = mailContent;
                                    message = string.Format(message, mailRow.ordernumber, mailRow.customer_name, DateTime.Now.ToString("MM/dd/yyyy hh:mm tt"));
                                    message = message.Replace("&#123;", "{");
                                    message = message.Replace("&#125;", "}");
                                    message = message.Replace("##kiosk##", "/" + strEcAttribute);
                                    message = message.Replace("##ecid##", strEcAttribute);
                                    message = message.Replace("##eckey##", (fkeyecattribute > 0 ? Convert.ToString(fkeyecattribute) : ""));

                                    MailSender ms = new MailSender(FromMailId, mailRow.customer_email_id, message, subject, "HTML", false);
                                    Thread mailThread = new Thread(new ThreadStart(ms.SendEmail));
                                    mailThread.Start();

                                }
                                catch (Exception ex)
                                {
                                    isMailSent = false;
                                }
                                if (isMailSent == true || WebConfiguration.ISIgnoreOrderStatusMail)
                                {
                                    mailRow.email_date = System.DateTime.Now;
                                }
                                else
                                {
                                    errMessages = errMessages + "Unable to send mail to: " + mailRow.customer_email_id;
                                    errMessages = errMessages + ". PO Number: " + mailRow.ordernumber;
                                    errMessages = errMessages + ". PO Detail Ref Key: " + mailRow.pkey_cust_po_dtl + "\\n";
                                }
                            }
                        }
                        SupplierPOService.UpdateOrdersWithMailSentStatus(userKey, poModel, errMessages, connString);
                    }

                }
                catch (Exception ex)
                {
                    FileLogger.WriteException(ex);
                }
            }
            else
            {
                FileLogger.WriteEntry("Mail sending restricted");
            }
        }

        public static string GetExclusiveCustomer(int ecKey)
        {
            try
            {
                string ecID = string.Empty;
                foreach (string strKey in WebConfiguration.ProductColorKeys.Keys)
                {
                    if (strKey.IndexOf("ExclusiveCustomer") >= 0 & Convert.ToInt32(WebConfiguration.ProductColorKeys[strKey]) == ecKey)
                    {
                        ecID = strKey.Replace("ExclusiveCustomer ", "");
                        break; // TODO: might not be correct. Was : Exit For
                    }
                }
                return ecID;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }


        private static string GetDefaultContent()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder("");
            sb.Append("<html><head><title>Order Fulfillment.</title>");
            sb.Append("<style>");
            sb.Append(".bodytext &#123; font-family: verdana, arial, Helvetica; font-size:10px; color:#000000; &#125;");
            sb.Append(".head &#123; font-family: verdana, arial, Helvetica;  font-size:12px;  color:#000000; &#125;");
            sb.Append("</style></head>");
            sb.Append("<body>");
            sb.Append("<TABLE cellSpacing='1' cellPadding='0' width='100%' border='0' bgcolor='#000000' >");
            sb.Append("<TBODY><TR><TD>");
            sb.Append("<TABLE cellSpacing='0' cellPadding='10' width='100%' border='0' bgcolor='#ffffff' >");
            sb.Append("<TBODY><TR><TD>");
            sb.Append("\t<TABLE cellSpacing='10' cellPadding='0' width='100%' border='0' >");
            sb.Append("\t<TBODY><tr><td align='center' class='head'><B>Your order has been {0} - Order Number {1}</B></td></tr>");
            sb.Append("<tr><td class='head' align=right>Date:{4}</td></tr>");
            sb.Append("\t<tr><td align='center' class='head'></td></tr>");
            sb.Append("\t<tr><td align='left' class='head'>Dear Mr/Mrs {2},<BR>Thank you for your order. ");
            sb.Append("\tYour order number <B>{1}</B> has been {0}.</td></tr>");
            sb.Append("\t<tr><td align='left' class='head'>{3}</td></tr>        ");
            sb.Append("\t<tr><td align='left' class='head'>If you have any questions regarding your order,");
            sb.Append("\tplease reply directly to this message or send an e-mail to <a href='mailto:Customercare@Bestprintbuy.com'>\tCustomercare@Bestprintbuy.com</a></td></tr>");
            sb.Append("\t<tr><td align='left' class='head'>Thank you for ordering from Bestprintbuy.com the user friendly one stop shop for an assortment of business card themes and designs.</td></tr>");
            sb.Append("\t<tr><td align='left' class='head'>Please take some time to check back for a whole range of new products as we update our site.</td></tr>");
            sb.Append("\t<tr><td align='left' class='head'></td></tr>");
            //sb.Append("	<tr><td align='left' class='head'>###Disclaimer###</td></tr>")
            sb.Append("</TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></body></html>");
            return sb.ToString();
        }

        public static string GetDefaultCancelContent()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder("");
            sb.Append("<html><head><title>Order Cancellation.</title>");
            sb.Append("<style>");
            sb.Append(".bodytext &#123; font-family: verdana, arial, Helvetica; font-size:10px; color:#000000; &#125;");
            sb.Append(".head &#123; font-family: verdana, arial, Helvetica;  font-size:12px;  color:#000000; &#125;");
            sb.Append("</style></head>");
            sb.Append("<body>");
            sb.Append("<TABLE cellSpacing='1' cellPadding='0' width='100%' border='0' bgcolor='#000000' >");
            sb.Append("<TBODY><TR><TD>");
            sb.Append("<TABLE cellSpacing='0' cellPadding='10' width='100%' border='0' bgcolor='#ffffff' >");
            sb.Append("<TBODY><TR><TD>");
            sb.Append("\t<TABLE cellSpacing='10' cellPadding='0' width='100%' border='0' >");
            sb.Append("\t<TBODY><tr><td align='center' class='head'><B>Your order has been {0} - Order Number {1}</B></td></tr>");
            sb.Append("<tr><td class='head' align=right>Date:{3}</td></tr>");
            sb.Append("\t<tr><td align='center' class='head'></td></tr>");
            sb.Append("\t<tr><td align='left' class='head'>Dear Mr/Mrs {2},<BR>");
            sb.Append("\tYour order number <B>{1}</B> has been {0}.</td></tr>");
            sb.Append("\t<tr><td align='left' class='head'>If you have any questions regarding your order,");
            sb.Append("\tplease reply directly to this message or send an e-mail to <a href='mailto:Customercare@Bestprintbuy.com'>\tCustomercare@Bestprintbuy.com</a></td></tr>");
            sb.Append("\t<tr><td align='left' class='head'>Please take some time to check back for a whole range of new products as we update our site.</td></tr>");
            sb.Append("\t<tr><td align='left' class='head'></td></tr>");
            //sb.Append("	<tr><td align='left' class='head'>###Disclaimer###</td></tr>")
            sb.Append("</TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></body></html>");
            return sb.ToString();
        }

        public static string GetDefaultSendOrderModificationContent()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder("");
            sb.Append("<html><head><title>Order Modification.</title>");
            sb.Append("<style>");
            sb.Append(".bodytext &#123; font-family: verdana, arial, Helvetica; font-size:10px; color:#000000; &#125;");
            sb.Append(".head &#123; font-family: verdana, arial, Helvetica;  font-size:12px;  color:#000000; &#125;");
            sb.Append("</style></head>");
            sb.Append("<body>");
            sb.Append("<TABLE cellSpacing='1' cellPadding='0' width='100%' border='0' bgcolor='#000000' >");
            sb.Append("<TBODY><TR><TD>");
            sb.Append("<TABLE cellSpacing='0' cellPadding='10' width='100%' border='0' bgcolor='#ffffff' >");
            sb.Append("<TBODY><TR><TD>");
            sb.Append("\t<TABLE cellSpacing='10' cellPadding='0' width='100%' border='0' >");
            sb.Append("\t<TBODY><tr><td align='center' class='head'><B>Your order requires modification - Order Number {0}</B></td></tr>");
            sb.Append("<tr><td class='head' align=right>Date:{2}</td></tr>");
            sb.Append("\t<tr><td align='center' class='head'></td></tr>");
            sb.Append("\t<tr><td align='left' class='head'>Dear Mr/Mrs {1},<BR>");
            sb.Append("\tThe uploaded designs does not meet the print requirements.  We request you to modify / re-upload the designs for order <b>{0}</b>.</td></tr>");
            sb.Append("\t<tr><td align='left' class='head'>If you have any questions regarding your order,");
            sb.Append("\tplease reply directly to this message or send an e-mail to <a href='mailto:Customercare@Bestprintbuy.com'>\tCustomercare@Bestprintbuy.com</a></td></tr>");
            sb.Append("\t<tr><td align='left' class='head'>Please take some time to check back for a whole range of new products as we update our site.</td></tr>");
            sb.Append("\t<tr><td align='left' class='head'></td></tr>");
            //sb.Append("	<tr><td align='left' class='head'>###Disclaimer###</td></tr>")
            sb.Append("</TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></body></html>");
            return sb.ToString();
        }

        public static void SendMail(string fromEmail, string Toemails, string msgBody, string subject)
        {
            bool sendMail = false;
            try
            {
                sendMail = (ConfigurationManager.AppSettings["SendMails"].ToString().ToLower() == "yes");
            }
            catch { }

            if (sendMail)
            {
                try
                {
                    MailSender ms = new MailSender(fromEmail, Toemails, msgBody, subject, "HTML", false);
                    //MailSender ms = new MailSender(fromEmail, "sreedhara@inforaise.com", msgBody[i], subject, "HTML", false);
                    Thread mailThread = new Thread(new ThreadStart(ms.SendEmail));
                    mailThread.Start();
                }
                catch (Exception ex)
                {
                    FileLogger.WriteException(ex);
                    //throw ex;
                }
            }
            else
            {
                FileLogger.WriteEntry("Mail sending restricted");
            }
        }
    }
}