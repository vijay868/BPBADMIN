using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.IO;
using System.Net;
using System.Collections;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;

namespace Bestprintbuy.Admin.Web.Common
{
    public class SprocketProcessor
    {
        private static string orderStatusURL = "http://www.sprocketexpress.com/webapi/order_status.php";
        private static string ordersURL = "http://www.sprocketexpress.com/webapi/order.php";
        private static string inventoryURL = "http://www.sprocketexpress.com/webapi/inventory.php";
        private static string trackingURL = "http://www.sprocketexpress.com/webapi/tracking.php";

        private static string SPROCKET_USER = "BPB";
        private static string SPROCKET_APIKEY = "BreadPaulBig91";


        public static void ReadInventoryAndUpdate(string dbConnString)
        {
            try
            {
                Hashtable ht = new Hashtable();
                ht.Add("action", "get");
                ht.Add("api_username", SPROCKET_USER);
                ht.Add("api_password", SPROCKET_APIKEY);
                PHPSerializer serializer = new PHPSerializer();
                string strParams = serializer.Serialize(ht);
                Hashtable data = (Hashtable)GetData(inventoryURL, string.Format("data={0}&data_format=php_serialize", System.Web.HttpUtility.UrlEncode(strParams)));
                if (data != null)
                {
                    CatalogRequestModel cData = new CatalogRequestModel();
                    
                    foreach (Hashtable item in data.Values)
                    {
                        CatalogRequestModel.InventoryRow row = cData.Inventory.NewInventoryRow();

                        row.DESCRIPTION = Convert.ToString(item["description"]);
                        row.SKU_NUMBER = Convert.ToString(item["item"]);
                        row.LOW = Convert.ToChar(item["low"]);
                        row.ON_ORDER = Convert.ToString(item["on_order"]);
                        row.REORDER_POINT = Convert.ToInt32(item["reorder_point"]);
                        row.UNITS = Convert.ToInt32(item["units"]);
                        row.BACK_ORDER = Convert.ToInt32(item["back_order"]);
                        cData.Inventory.Rows.Add(row);
                    }
                    ReportService.UpdateCatReqInventory(cData, dbConnString);
                }
            }
            catch (Exception ex) {
                throw(ex);
            }
        }

        public static void ReadStatusAndUpdate(string dbConnString)
        {
            try
            {
                Hashtable ht = new Hashtable();
                ht.Add("action", "get");
                ht.Add("api_username", SPROCKET_USER);
                ht.Add("api_password", SPROCKET_APIKEY);
                PHPSerializer serializer = new PHPSerializer();
                string strParams = serializer.Serialize(ht);
                Hashtable data = (Hashtable)GetData(orderStatusURL, string.Format("data={0}&data_format=php_serialize", System.Web.HttpUtility.UrlEncode(strParams)));
                if (data != null)
                {
                    CatalogRequestModel cData = new CatalogRequestModel();

                    foreach (Hashtable item in data)
                    {
                        CatalogRequestModel.CatalogRequestsRow row = cData.CatalogRequests.NewCatalogRequestsRow();

                        row.CR_MM_ORDERID = Convert.ToString(item["order_number"]);
                        row.ORDER_STATUS = Convert.ToString(item["order_status"]);
                        row.CR_STATUS_DATE = Convert.ToDateTime(item["order_status_date"]);
                        cData.CatalogRequests.Rows.Add(row);
                    }
                    ReportService.UpdateOrderStatus(cData, dbConnString);
                }
            }
            catch (Exception ex)
            {
                throw(ex);
            }
        }

        public static void ReadTrackingStatusAndUpdate(string dbConnString)
        {
            try
            {
                Hashtable ht = new Hashtable();
                ht.Add("action", "get");
                ht.Add("api_username", SPROCKET_USER);
                ht.Add("api_password", SPROCKET_APIKEY);
                PHPSerializer serializer = new PHPSerializer();
                string strParams = serializer.Serialize(ht);
                Hashtable data = (Hashtable)GetData(trackingURL, string.Format("data={0}&data_format=php_serialize", System.Web.HttpUtility.UrlEncode(strParams)));
                if (data != null)
                {
                    CatalogRequestModel cData = new CatalogRequestModel();

                    foreach (Hashtable item in data.Values)
                    {
                        CatalogRequestModel.CatalogRequestsRow row = cData.CatalogRequests.NewCatalogRequestsRow();

                        row.CR_MM_ORDERID = Convert.ToString(item["order_number"]);
                        row.CR_SHIP_DATE = Convert.ToDateTime(item["ship_date"]);
                        row.CARRIER_CODE = Convert.ToString(item["ship_method_code"]);
                        row.CARRIER_NAME = Convert.ToString(item["ship_method_name"]);
                        row.TRACKING_NUMER = Convert.ToString(item["tracking_number"]);

                        cData.CatalogRequests.Rows.Add(row);

                    }
                    ReportService.UpdateOrderShipStatus(cData, dbConnString);
                }
            }
            catch (Exception ex)
            {
                throw(ex);
            }
        }

        public static Hashtable PostPrePressOrderToSprocket(TransactionsModel.SprocketOrdersRow row)
        {
            try
            {
                Hashtable htOrder = new Hashtable();
                Hashtable htOrderItem = new Hashtable();
                ArrayList alItems = new ArrayList();



                htOrder.Add("order_number", row.po_number.TrimStart('0')); //Pkey
                htOrder.Add("order_date", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                htOrder.Add("carrier_code", row.ship_price_option);
                htOrder.Add("po_number", row.po_dtl_number);

                htOrder.Add("billing_title", "");
                htOrder.Add("billing_first_name", "Tiru");
                htOrder.Add("billing_last_name", "Chaparala");
                htOrder.Add("billing_company", "BESTPRINTBUY");
                htOrder.Add("billing_addr1", "1506 Providence Highway");
                htOrder.Add("billing_addr2", "STE 29");
                htOrder.Add("billing_city", "Norwood");
                htOrder.Add("billing_state_code", "MA");
                //htOrder.Add("billing_state_name", "Massachusetts");
                htOrder.Add("billing_zip_code", "02062");
                htOrder.Add("billing_country_iso2", "US");

                //htOrder.Add("billing_country_name", "United States");
                htOrder.Add("billing_email", "printing@bestprintbuy.com");
                htOrder.Add("billing_phone", "866-763-8720");
                htOrder.Add("billing_phone_extension", "");

                htOrder.Add("shipping_title", row.Isship_titleNull() ? "" : row.ship_title);
                htOrder.Add("shipping_first_name", row.ship_first_name);
                htOrder.Add("shipping_last_name", row.ship_last_name);
                htOrder.Add("shipping_company", row.ship_company_name);
                htOrder.Add("shipping_addr1", row.shipment_address1);
                htOrder.Add("shipping_addr2", row.shipment_address2);
                htOrder.Add("shipping_city", row.city);
                htOrder.Add("shipping_state_code", row.state_abbr);
                //htOrder.Add("shipping_state_name", "Massachusetts");
                htOrder.Add("shipping_zip_code", row.zipcode);
                htOrder.Add("shipping_country_iso2", "US");
                //htOrder.Add("shipping_country_name", "United States");

                htOrder.Add("shipping_email", row.email_id);
                htOrder.Add("shipping_phone", row.ship_phone);
                htOrder.Add("shipping_phone_extension", "");

                htOrder.Add("order_memo1", "");
                htOrder.Add("order_memo2", "");
                htOrder.Add("order_memo3", "");
                htOrder.Add("internal_comment", "");
                htOrder.Add("use_ship_amount", false);
                htOrder.Add("shipping", "0.00");
                htOrder.Add("use_cod", false);
                htOrder.Add("cod_fee", "0.00");
                htOrder.Add("use_prices", false);

                htOrderItem = new Hashtable();
                htOrderItem.Add("product_name", row.description);
                htOrderItem.Add("product_sku", row.skunumber);
                htOrderItem.Add("quantity", row.quantity);
                htOrderItem.Add("price", "0");
                htOrderItem.Add("discount", "0");

                alItems.Add(htOrderItem);
                htOrder.Add("order_items", alItems);

                Hashtable htOrders = new Hashtable();
                htOrders.Add("1", htOrder);

                Hashtable htAction = new Hashtable();
                htAction.Add("action", "add");
                htAction.Add("api_username", SPROCKET_USER);
                htAction.Add("api_password", SPROCKET_APIKEY);
                htAction.Add("orders", htOrders);

                PHPSerializer serializer = new PHPSerializer();
                string postdata = serializer.Serialize(htAction);
                return (Hashtable)GetData(ordersURL, string.Format("data={0}&data_format=php_serialize", System.Web.HttpUtility.UrlEncode(postdata)));

            }
            catch (Exception exception)
            {
                throw(exception);
            }
            return null;
        }

        public static Hashtable PostOrderToSprocket(CatalogRequestModel.CatalogRequestsRow row)
        {
            try
            {
                Hashtable htOrder = new Hashtable();
                Hashtable htOrderItem = new Hashtable();
                ArrayList alItems = new ArrayList();

                

                htOrder.Add("order_number", row.CR_MM_ORDERID.TrimStart('0')); //Pkey
                htOrder.Add("order_date", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                htOrder.Add("carrier_code", row.Shipping_Type);
                htOrder.Add("po_number", row.Order_No);

                htOrder.Add("billing_title", "");
                htOrder.Add("billing_first_name", "Tiru");
                htOrder.Add("billing_last_name", "Chaparala");
                htOrder.Add("billing_company", "BESTPRINTBUY");
                htOrder.Add("billing_addr1", "1506 Providence Highway");
                htOrder.Add("billing_addr2", "STE 29");
                htOrder.Add("billing_city", "Norwood");
                htOrder.Add("billing_state_code", "MA");
                //htOrder.Add("billing_state_name", "Massachusetts");
                htOrder.Add("billing_zip_code", "02062");
                htOrder.Add("billing_country_iso2", "US");
                
                //htOrder.Add("billing_country_name", "United States");
                htOrder.Add("billing_email", "printing@bestprintbuy.com");
                htOrder.Add("billing_phone", "866-763-8720");
                htOrder.Add("billing_phone_extension", "");

                htOrder.Add("shipping_title", "");
                htOrder.Add("shipping_first_name", row.First_Name);
                htOrder.Add("shipping_last_name", row.Last_Name);
                htOrder.Add("shipping_company", row.Company_Name);
                htOrder.Add("shipping_addr1", row.Address_1);
                htOrder.Add("shipping_addr2", "");
                htOrder.Add("shipping_city", row.City);
                htOrder.Add("shipping_state_code", row.State);
                //htOrder.Add("shipping_state_name", "Massachusetts");
                htOrder.Add("shipping_zip_code", row.Zip);
                htOrder.Add("shipping_country_iso2", "US");
                //htOrder.Add("shipping_country_name", "United States");

                htOrder.Add("shipping_email", row.FromEmail);
                htOrder.Add("shipping_phone", row.Phone);
                htOrder.Add("shipping_phone_extension", "");

                htOrder.Add("order_memo1", "");
                htOrder.Add("order_memo2", "");
                htOrder.Add("order_memo3", "");
                htOrder.Add("internal_comment", "");
                htOrder.Add("use_ship_amount", false);
                htOrder.Add("shipping", "0.00");
                htOrder.Add("use_cod", false);
                htOrder.Add("cod_fee", "0.00");
                htOrder.Add("use_prices", false);

                htOrderItem = new Hashtable();
                htOrderItem.Add("product_name", row.PRODDESC);
                htOrderItem.Add("product_sku", row.SKU_Number);
                htOrderItem.Add("quantity", "1");
                htOrderItem.Add("price", "0");
                htOrderItem.Add("discount", "0");

                alItems.Add(htOrderItem);
                htOrder.Add("order_items", alItems);

                Hashtable htOrders = new Hashtable();
                htOrders.Add("1", htOrder);

                Hashtable htAction = new Hashtable();
                htAction.Add("action", "add");
                htAction.Add("api_username", SPROCKET_USER);
                htAction.Add("api_password", SPROCKET_APIKEY);
                htAction.Add("orders", htOrders);

                PHPSerializer serializer = new PHPSerializer();
                string postdata = serializer.Serialize(htAction);
                return (Hashtable)GetData(ordersURL, string.Format("data={0}&data_format=php_serialize", System.Web.HttpUtility.UrlEncode(postdata)));

            }
            catch (Exception exception)
            {
                throw(exception);
            }
            return null;
        }


        private static object GetData(string url, string param)
        {
            using (WebClient wc = new WebClient())
            {
                PHPSerializer serializer = new PHPSerializer();
                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                string HtmlResult = wc.UploadString(url, param);
                return serializer.Deserialize(HtmlResult);
            }
            return null;
        }
    }
}
