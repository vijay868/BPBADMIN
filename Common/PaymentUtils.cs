using System;
using System.Collections.Specialized;
using System.Collections;
using System.Text;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.Common
{
	/// <summary>
	/// Summary description for PaymentsUtils.
	/// </summary>
	public class PaymentUtils
	{
		private PFProCOMLib.PNComClass processor = null;

		private string txnType = "S";

		private NameValueCollection nvcResponse = new NameValueCollection();

		private string autherizationPNREF = "";

        public PaymentUtils()
		{
			processor = new PFProCOMLib.PNComClass();
		}

		private int GetContext()
		{
			return processor.CreateContext(HostAddress,HostPort,TxnTimeOut,string.Empty,0,string.Empty,string.Empty);
		}

		// the method is for transferring the amount that has been order by
		// customer for purchasing a product on-line. 
		// means that when customer trying for placeing an on line order has 
		// to furnish the details as given below in order to process the payment
		// using "VeriSign"

		// below are the detail that are need for processing the payment.
		// 1) expiration date of the card
		// 2) card number 
		// 3) amount of the product.

		// The below call returns the status for the transfer of funds.
		// it he status is "true" then the amount set as a paramter been
		// authrized and transferred to merchant. If in any case transaction
		// is failed the amount is rolled back by payment server itself. 
		// but, on all the transaction the payment processor turns the 
		// PNREF number which then be used by our system to do a follow-up with
		// payment service provider.

		// so, the programer when using the method called one must check
		// the status than grab the PNREF number for later use.

		public bool TransferAmountToMerchant(string ExpirationDate,String cardNo,
            double amount,string street,string zip) 
		{
			txnType = "S";
			autherizationPNREF = "";
			if(!HasValidateParams())
				ExceptionHandler.HandleException(ProjectType.Web ,new Exception("Please check card number. If it's Ok, please contact BestPrintBuy.com customer case section."));
			//AppErrors.throwException(null,new Exception("MSG020_1"));
			return ValidateCard(GetContext(),ExpirationDate,cardNo,amount,street,zip); 
		}

		public bool TransferAmountToCardHolder(string ExpirationDate,double amount,
            string orgid) 
		{
			
			txnType = "V";
			autherizationPNREF = orgid;
			if(!HasValidateParams())
                ExceptionHandler.HandleException(ProjectType.Web, new Exception("Please check card number. If it's Ok, please contact BestPrintBuy.com customer case section."));
			//AppErrors.throwException(null,new Exception("MSG020_1"));
			return ValidateCard(GetContext(),ExpirationDate,"",amount,"",""); 
		}



		public bool ValidateCard(int context,string ExpirationDate,String cardNo,double amount,string street,string zip) 
		{
			int paymentContext = context;
			try
			{
				
				// This transaction is done using
				// the transaction type as Autherization 
				// this call will autherize the amount that
				// is passed to payment server in
				// parameter list.

				string ParmList = GetParametersList(ExpirationDate,cardNo,amount,street,zip);
				UpdateResponse(processor.SubmitTransaction(paymentContext,ParmList,ParmList.Length)); 

                /*
                 * Commented below code to Stop the Address Valication on Payment Process
                 * 
                 * 
                if((StatusCode ==0) && (IsValidAddress == false))
                {
                    txnType = "V";
                    autherizationPNREF = PNREF;
					ParmList = GetParametersList(ExpirationDate,cardNo,amount,"","");
                    string voidTrainsaction = processor.SubmitTransaction(paymentContext,ParmList,ParmList.Length);
                    AppErrors.throwException(null,new Exception(WebConfiguration.GetPaymentError(-2)));
                }
                else 
                * Commented on [12-01-2005]
                */
                if(Status == false)
                {
                    ExceptionHandler.HandleException(ProjectType.Web, new Exception(WebConfiguration.GetPaymentError(StatusCode)));
                }
				processor.DestroyContext(paymentContext);

			}
			catch(Exception ape)
			{
				processor.DestroyContext(paymentContext);
				throw ape;


			}
			return Status;
		}


		private string GetParametersList(string ExpirationDate,String cardNo,
            double amount,string street,string zip)
		{
            if(txnType != "V")
            {
                // checking card number.
                if(cardNo == string.Empty || cardNo == null)
                    ExceptionHandler.HandleException(ProjectType.Web, new Exception("Please check card number. If it's Ok, please contact BestPrintBuy.com customer care section."));
                 //AppErrors.throwException(null,new Exception("MSG020_1"));
                
            
                // checking the street.
                if(street == string.Empty || street == null )
                    ExceptionHandler.HandleException(ProjectType.Web, new Exception("Please check card number. If it's Ok, please contact BestPrintBuy.com customer case section."));
                //AppErrors.throwException(null,new Exception("MSG020_1"));

                // checking the zip
                if(zip == string.Empty ||zip == null )
                    ExceptionHandler.HandleException(ProjectType.Web, new Exception("Please check card number. If it's Ok, please contact BestPrintBuy.com customer case section."));
                //AppErrors.throwException(null,new Exception("MSG020_1"));           
            

            }
            else
            {
                if(autherizationPNREF == string.Empty || autherizationPNREF == null)
                    ExceptionHandler.HandleException(ProjectType.Web, new Exception("Please check card number. If it's Ok, please contact BestPrintBuy.com customer case section."));
                //AppErrors.throwException(null,new Exception("MSG020_1"));
            }
            string validator = "TRXTYPE={0}&TENDER={1}&PARTNER={2}&VENDOR={3}&PWD={4}&"+
                                "USER={5}&ACCT={6}&EXPDATE={7}&AMT={8}";
            
			StringBuilder sb = new StringBuilder();
            sb.AppendFormat(validator,
                    TxnType.Trim(),Tender.Trim(),
                    Partner.Trim(),Vendor.Trim(),
                    Password.Trim(),User.Trim(),
                    cardNo.Trim(),ExpirationDate,
                    amount);

            if(street != string.Empty && street != null)
            {
                sb.AppendFormat("&STREET={0}",street);
            }
            if(zip != string.Empty && zip != null)
            {
                sb.AppendFormat("&ZIP={0}",zip);
            }
            
			if(autherizationPNREF != string.Empty)
			   sb.AppendFormat("&ORIGID={0}",autherizationPNREF);

			return sb.ToString();
		}

		
		private bool HasValidateParams()
		{
			if(TxnType == null || TxnType == string.Empty )
				return false;
			if(Tender == null || Tender == string.Empty )
				return false;
			if(Partner == null || Partner == string.Empty )
				return false;
			if(Vendor == null || Vendor == string.Empty )
				return false;

			if(Password == null || Password == string.Empty )
				return false;

			if(User  == null || User == string.Empty )
				return false;
			
			return true;

		}

		private void UpdateResponse(string response)
		{
            
            FileLogger.WriteEntry("Error in Verisign Processor :\n[" + response + "]");
			if(response == null)
                ExceptionHandler.HandleException(ProjectType.Web, new Exception("Timeout occured on Payment server. Please try again."));
			//AppErrors.throwException(null,new Exception("MSG019_0"));

			string[] responsePairs = response.Split('&');
			foreach(string pair in responsePairs)
			{
				string[] keyvalue = pair.Split(new char[]{'='});
				string key = keyvalue[0];
				string result = keyvalue[1];
				nvcResponse.Add(key,result);
			}
		}

		public int HostPort
		{
			get
			{
				return WebConfiguration.paymentPort;
			}
		}

		public string HostAddress
		{
			get
			{
				return WebConfiguration.paymentHost;
			}
		}

		public int TxnTimeOut
		{
			get
			{
				return WebConfiguration.paymentTimeOut;;
			}
		}

		public string TxnType
		{
			get
			{
				return txnType;
			}
		}

		public string Tender
		{
			get
			{
				return "C";;
			}
		}
						 
		public string Partner
		{
			get
			{
				return WebConfiguration.paymentPartner;;
			}
		}

		
		public string Vendor
		{
			get
			{
				return WebConfiguration.paymentVendor;;
			}
		}

		public string User
		{
			get
			{
				return WebConfiguration.paymentUser;;
			}
		}

		public string Password
		{
			get
			{
				return WebConfiguration.paymentPassword;;
			}
		}

		public string PNREF
		{
			get
			{
				return Responses["PNREF"];
			}
		}

		public string AutherizationPNREF
		{
			get
			{
				return autherizationPNREF;
			}
		}
		

		public bool Status
		{
			get
			{
				return StatusCode == 0;
			}
		}

        public int StatusCode
        {
            get
            {
                return Convert.ToInt32(Responses["RESULT"]);
            }
        }

		public NameValueCollection Responses
		{
			get
			{
				return nvcResponse;
			}
		}

        public bool IsValidAddress
        {
            get
            {
                return Responses["AVSADDR"]== "Y" & Responses["AVSZIP"] == "Y" ;
            }
        }

        public bool IsInternationAddress
        {
            get
            {
                return Responses["IAVS"] == "Y";
            }
        }


//				public static void Main()
//				{
//					PaymentUtils payment = new PaymentUtils();
//		
//					ArrayList array = new ArrayList();
//					
//					array.Add("411111111111111");
//					array.Add("4012888888881881");
//					array.Add("4222222222222");
//		
//					array.Add("5555555555554444");
//					array.Add("5105105105105100");
//		
//					array.Add("378282246310005");
//					array.Add("371449635398431");
//		
//					foreach(Object obj in array)
//					{
//						string noOfCard = Convert.ToString(obj);
//						
//						string date = 0104;													    
//						for (int i = 1;i<=10;i++)
//						{
//							double amount = 100.00d * Convert.ToDouble(i) + Convert.ToDouble(i);
//		
//							bool myStatus = payment.TransferAmountToMerchant(date,noOfCard,amount);
//		
//							Console.WriteLine(" The card no {0} transaction status {1} for an amount of {2} and PNREF Number : {3}", noOfCard, myStatus, amount ,payment.PNREF);
//						}
//		
//						for (int i = 1;i<=50;i++)
//						{
//							int amount = 1000  + i;
//		
//							bool myStatus = payment.ValidateCard(DateTime.Now,noOfCard,Convert.ToDouble(amount));
//		
//							Console.WriteLine(" The card no {0} transaction status {1} for an amount of {2} and PNREF Number : {3}", noOfCard, myStatus, amount ,payment.PNREF);
//						}
//		
//						for (int i = 1;i<=20;i++)
//						{
//							int amount = 2000  + i;
//		
//							bool myStatus = payment.ValidateCard(DateTime.Now,noOfCard,Convert.ToDouble(amount));
//		
//							Console.WriteLine(" The card no {0} transaction status {1} for an amount of {2} and PNREF Number : {3}", noOfCard, myStatus, amount ,payment.PNREF);
//						}
//		
//						Console.Read();
//					}
//				}
		
	}
}

