﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Security;
using System.Security.Principal;

namespace Bestprintbuy.Admin.Web.Common
{
    /// <summary>
    /// Summary description for UserPrincipal.
    /// </summary>
    public class UserPrincipal : IPrincipal
    {
        String role;
        UserIdentity identity;
        String screen;
        Hashtable screenRights = new Hashtable();
        public UserPrincipal()
        {
            identity = null;
            role = null;
        }

        public UserPrincipal(UserIdentity userIdentity, String screen)
        {
            this.screen = screen;
            this.identity = userIdentity;
            this.role = null;
        }

        public UserPrincipal(UserIdentity userIdentity, String screen, string role)
        {
            this.screen = screen;
            this.identity = userIdentity;
            this.role = role;
        }

        public UserPrincipal(UserIdentity userIdentity, String screen, NameValueCollection nvc)
        {
            this.identity = userIdentity;
            screenRights.Add(screen, nvc);
        }

        public bool IsInRole(string role)
        {
            NameValueCollection nvc = null;
            string[] roles = role.Split(',');
            bool rights = false;
            if (roles.Length == 2)
            {
                nvc = (NameValueCollection)screenRights[roles[0]];

                if (nvc != null)
                {
                    rights = Convert.ToBoolean(nvc[roles[1]]);
                }
            }

            return rights;
        }

        /// <summary>
        /// Gets the CustomIdentity of the user represented by the current CustomPrincipal.
        /// </summary>
        public IIdentity Identity
        {
            get { return identity; }
            set { identity = (UserIdentity)value; }
        }

        public UserIdentity userIdentity
        {
            get
            {
                return identity;
            }
            set
            {
                identity = value;
            }
        }
    }
}