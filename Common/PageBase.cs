﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Security;
using System.Configuration;
using System.Data;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Utilities;
using System.Web.UI.WebControls;


namespace Bestprintbuy.Admin.Web.Common
{
    public class PageBase : System.Web.UI.Page
    {
        private string connectionString = "";
        private string mCustomerPortalURL = "";
        private string screenName = "";
        private string functionalName = "";
        private string strTimeStamp = "";
        private bool new_session = false;
        protected UserModel accessModel = new UserModel();
        protected int RowCounter = 0;
        protected int RecordCounter = 0;
        public PageBase()
        {
            this.connectionString = ConfigurationManager.AppSettings["BestPrintBuyDBConn"];
            this.mCustomerPortalURL = ConfigurationManager.AppSettings["BPBCustomerBaseURL"];

            /*if (Page.Session.IsNewSession)
                new_session = true;
            string screenName = Page.GetType().AssemblyQualifiedName;
            string[] screenNames = screenName.Split(",".ToCharArray());
            if (screenNames.Length > 1)
            {
                screenNames = screenNames[0].Split("._".ToCharArray());
                if (screenNames.Length > 2)
                {
                    screenName = screenNames[1];
                }
            }
            string FailUrl = WebConfiguration.FailUrl;
            string functionalName = "list";
            //Dim EditKeys As String = Request.Item("EditKeys")
            //Dim DeleteKeys As String = Request.Item("DeleteKeys")
            // Dim targetAction As String = Request.Item("TargetAction")

            strTimeStamp = "timeStamp=" + System.DateTime.Now.Ticks.ToString();

            if (Request.QueryString["IsEdit"] != null)
                functionalName = "edit";
            else if (Request.QueryString["IsDelete"] != null)
                functionalName = "delete";
            else if (Request.QueryString["IsAdd"] != null)
                functionalName = "add";
            else if (Request.QueryString["IsView"] != null)
                functionalName = "view";
            screenName = GetScreenName(screenName);
            if (new_session == true)
            {
                if (IsSupplier(screenName))
                {
                    Response.Redirect("../Supplier/SupplierSessionExpired.aspx");
                    Response.End();
                }
                else
                {
                    Response.Redirect("../Admin/AdminSessionExpired.aspx");
                    Response.End();
                }
            }

            string sessionId = Session.SessionID;
            Object objPrincipal = Session[sessionId];
            if (objPrincipal != null)
            {
                UserPrincipal principal = (UserPrincipal)objPrincipal;
                Context.User = principal;
            }

            if (Request.IsAuthenticated)
            {
                OnAuthenticate(screenName);
            }*/

            //OnAutherize(screenName, functionalName);

            /*string pageName = Page.ToString().ToLower();
            if (pageName.IndexOf("login") == -1 && Identity != null)
            {
                PopulateAccessRights(Identity.UserPK);
            }*/
        }

        //private void OnAuthenticate(string screenName)
        //{
        //    throw new NotImplementedException();
        //}

        //private void OnAutherize(string screenName, string functionalName)
        //{
        //    throw new NotImplementedException();
        //}

        private string strPageMessage = string.Empty;
        public string PageMessage
        {
            get { return strPageMessage; }
            set { strPageMessage = value; }
        }

        protected string ConnString
        {
            get { return connectionString; }
        }

        protected string customerPotralUrl
        {
            get { return mCustomerPortalURL; }
        }
        protected string UrlBase
        {
            get { return "http://" + UrlSuffix + "/"; }
        }

        protected string ProdDesignCenterUrlBase
        {
            get { return ConfigurationManager.AppSettings["BPBCustomerBaseURL"].ToString() + "/DesignCenter"; }
        }


        protected string UrlSuffix
        {
            get
            {
                string strUrlSuffix;
                strUrlSuffix = System.Web.HttpContext.Current.Request.Url.Host;
                strUrlSuffix = strUrlSuffix + System.Web.HttpContext.Current.Request.ApplicationPath;
                if (strUrlSuffix.LastIndexOf("/") == strUrlSuffix.Length - 1)
                {
                    strUrlSuffix = strUrlSuffix.Substring(0, strUrlSuffix.Length - 1);
                }
                return strUrlSuffix;
            }
        }

        protected string SecureUrlBase
        {
            get
            {
                return ("https://") + UrlSuffix;
            }
        }

        public bool IsDBNull(object obj)
        {
            return System.Convert.IsDBNull(obj);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreRender(EventArgs e)
        {
            try
            {
                SetPageMessge();
                base.OnPreRender(e);
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);
                CheckAuthentication();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
            }
        }

        /// <summary>
        /// Checks whether user is authenticated or not
        /// </summary>
        private void CheckAuthentication()
        {
            string pageName = Page.ToString().ToLower();
            if (pageName.IndexOf("asp.login") == -1 && Identity == null)
            {
                if (this.Master == null)  // popup page
                {
                    Response.Redirect("~/Login.aspx?sessiontimeoutfrompopup=true");
                }
                else
                {
                    Response.Redirect("~/Login.aspx");
                }
            }
            else if (pageName.IndexOf("asp.login") == -1)
            {
                PopulateAccessRights(Identity.UserPK);
            }
            // TODO : need to redirect to appropriate login screen (ex:suppliar)
        }


        /// <summary>
        /// This method declares the page message variable
        /// in the javascript ands sets the page message
        /// </summary>
        public void SetPageMessge()
        {
            ClientScriptManager cs = base.ClientScript;
            cs.RegisterHiddenField("PageMessage", PageMessage);
            PageMessage = string.Empty;
        }


        protected void AddToCookie(string key, string value)
        {
            try
            {
                HttpCookie cookie = this.Request.Cookies["BestPrintBuy.com"];
                if (cookie == null)
                    cookie = new HttpCookie("BestPrintBuy.com");
                cookie.Values[key] = value;
                DateTime dtNow = DateTime.Now;
                TimeSpan tsMinute = new TimeSpan(60, 0, 0, 0);
                cookie.Expires = dtNow + tsMinute;
                Response.Cookies.Add(cookie);
            }
            catch { }
        }

        protected void RemoveFromCookie(string key)
        {
            try
            {
                HttpCookie cookie = this.Request.Cookies["BestPrintBuy.com"];
                cookie.Values.Remove(key);
                DateTime dtNow = DateTime.Now;
                TimeSpan tsMinute = new TimeSpan(60, 0, 0, 0);
                cookie.Expires = dtNow + tsMinute;
                Response.Cookies.Add(cookie);
            }
            catch { }
        }

        protected string GetCookie(string key)
        {
            string value = string.Empty;
            try
            {
                HttpCookie cookie = this.Request.Cookies["BestPrintBuy.com"];
                if (cookie != null)
                {
                    if (cookie.Values[key] != null)
                    {
                        value = cookie.Values[key];
                    }
                }
            }
            catch { }
            return value;
        }

        internal string GetIpAddress()
        {
            try
            {
                string ip = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                if (ip == null || ip == string.Empty)
                {
                    ip = Request.ServerVariables["REMOTE_ADDR"];
                }
                return ip;
            }
            catch
            {
                return "";
            }
        }

        public UserIdentity Identity
        {
            get
            {
                try
                {
                    string sessionId = Session.SessionID;
                    object objPrincipal = Session[sessionId];
                    if (objPrincipal != null)
                    {
                        UserPrincipal principal = (UserPrincipal)objPrincipal;
                        return principal.userIdentity;
                    }
                    return null;
                }
                catch { return null; }
            }
        }

        protected void PopulateAccessRights(int pkeyuser)
        {
            if (!Identity.IsSuperUser)
            {
                accessModel = UserService.GetUserRoles(Identity.UserPK, ConnString);
            }
        }

        public bool HasAccess(string p_screenName, string p_functionaName)
        {

            if (Identity.IsSuperUser)
                return true;

            string l_screenName = GetScreenName(p_screenName);
            string l_filter = "Screen_Name='" + l_screenName + "'";
            if (p_functionaName == "list")
                l_filter += " and (listaccess = 'Yes' or listaccess = 'yes')";
            else if (p_functionaName == "view")
                l_filter += " and (viewaccess = 'Yes' or viewaccess = 'yes')";
            else if (p_functionaName == "delete")
                l_filter += " and (deleteaccess = 'Yes' or deleteaccess = 'yes')";
            else if (p_functionaName == "edit")
                l_filter += " and (editaccess = 'Yes' or editaccess = 'yes')";
            else if (p_functionaName == "add")
                l_filter += " and (addaccess = 'Yes' or addaccess = 'yes')";
            DataRow[] dr = accessModel.UserAccess.Select(l_filter);


            if (dr.Length == 1)
                return true;
            return false;
        }

        private string GetScreenName(string functionScreenName)
        {
            if (WebConfiguration.AdminLoginScreens.ContainsKey(functionScreenName))
                return WebConfiguration.AdminLoginScreens[functionScreenName].ToString();
            else if (WebConfiguration.PPSOLScreens.ContainsKey(functionScreenName))
                return WebConfiguration.PPSOLScreens[functionScreenName].ToString();
            else if (WebConfiguration.SupplierScreens.ContainsKey(functionScreenName))
                return WebConfiguration.SupplierScreens[functionScreenName].ToString();
            else if (WebConfiguration.PortalScreens.ContainsKey(functionScreenName))
                return WebConfiguration.PortalScreens[functionScreenName].ToString();
            return "";
        }

        private bool IsSuperAdmin(string screenName)
        {
            return WebConfiguration.AdminLoginScreens.ContainsValue(screenName);
        }

        private bool IsAdmin(string screenName)
        {
            return WebConfiguration.PPSOLScreens.ContainsValue(screenName);
        }

        private bool IsSupplier(string screenName)
        {
            return WebConfiguration.SupplierScreens.ContainsValue(screenName);
        }

        private bool IsCustomer(string screenName)
        {
            return WebConfiguration.PortalScreens.ContainsValue(screenName);
        }

        protected void Logout()
        {
            Session.RemoveAll();
        }

        /// <summary>
        /// To Display alert messages
        /// </summary>
        /// <param name="message"></param>
        public void AlertScript(string message)
        {
            string ss = string.Format("alert('{0}');", message.Replace("'", "\\'").Replace("\r\n", "\\n"));
            ScriptManager.RegisterStartupScript(this, this.GetType(), "jscript", ss, true);
        }

        /// <summary>
        /// To Display alert messages
        /// </summary>
        /// <param name="message"></param>
        public void RunScript(string message)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "jscript", message, true);
        }

        public string ConvertToMoneyFormat(object obj)
        {
            return CurrencyUtils.ToMoneyFormat(obj);
        }

        public string ReadMailContent(string strMailFor)
        {
            try
            {
                string strFile;
                string strContent;
                strFile = WebConfiguration.GetEmailHTMLLoc(strMailFor);
                System.IO.StreamReader file = new System.IO.StreamReader(strFile);
                strContent = file.ReadToEnd();
                file.Close();
                file = null;
                return strContent;
            }
            catch
            {
                return "";
            }
        }


        public int GetRowCount()
        {
            if (RowCounter == 0)
                RowCounter = 1;
            else
                RowCounter = RowCounter + 1;
            return RowCounter;
        }

        public int GetRecordCount(int pageNo)
        {
            if (RecordCounter == 0)
            {
                int page_number = Convert.ToInt32(pageNo);
                if (page_number > 0)
                    RecordCounter = ((page_number - 1) * 10) + RecordCounter + 1;
                else if (page_number == 0)
                    RecordCounter = 1;
            }
            else
            {
                RecordCounter = RecordCounter + 1;
            }
            RowCounter = RecordCounter;
            return RecordCounter;
        }


        public int GetRecordCountForPageSize(int PageSize, int PageNo)
        {
            if (RecordCounter == 0)
            {
                int page_number = PageNo;
                if (page_number > 0)
                {
                    RecordCounter = ((page_number - 1) * PageSize) + RecordCounter + 1;
                }
                else if (page_number == 0)
                {
                    RecordCounter = 1;
                }
            }
            else
            {
                RecordCounter = RecordCounter + 1;
            }
            RowCounter = RecordCounter;
            return RecordCounter;
        }


        public int GetRowIndex()
        {
            return RowCounter;
        }

        public int GetMasterCatKey(String strID)
        {
            switch (strID)
            {
                case "sup":
                    //Supplier Qualifiers  - attributes for supplier
                    return 7;
                case "mer":
                    //Merchant Accounts Qualifiers  - attributes for Merchant Accounts - not in use
                    return 8;
                case "pat":
                    //Co-Branded Business Partners/B2B Affiliate Qualifiers 
                    return 9;
                case "afl":
                    //Co-Branded Business Partners/B2B Affiliate Qualifiers 
                    return 9;
                case "bur":
                    //Business Category Register Qualifiers
                    return 6;
                case "ship":
                    //Freight Classification and Delivery
                    return 13;
                default:
                    //Business Category Register Qualifiers
                    return 6;
            }
        }

        public string GetSelectedText(object obj)
        {
            if (obj == null)
                return "";
            string templateUrl = obj.ToString();
            string dialogcomment = obj.ToString().Replace("\\", "\\\\").Replace("'", "\\'").Replace("\"", "&quot;").Replace("\r\n", "<br>");
            try
            {
                if (templateUrl.Length <= 20)
                    return templateUrl;
                else
                {
                    return templateUrl.Substring(0, 20) + string.Format("<a href=\"#\" class=\"bold topDir\" title=\"{0}\">...</a>", dialogcomment);
                }
            }
            catch
            {
                return "";
            }
        }

        /// <summary>
        /// Returns items from dropdown list
        /// </summary>
        /// <param name="ddl"></param>
        /// <returns></returns>
        internal List<KeyValuePair<string, string>> GetItems(DropDownList ddl)
        {
            List<KeyValuePair<string, string>> lst = new List<KeyValuePair<string, string>>();
            foreach (ListItem item in ddl.Items)
            {
                lst.Add(new KeyValuePair<string, string>(item.Text, item.Value));
            }
            return lst;
        }

        /// <summary>
        /// Bind dropdown list
        /// </summary>
        /// <param name="ddl"></param>
        /// <param name="dataSource"></param>
        /// <param name="defaultItemText"></param>
        internal void BindDropdown(DropDownList ddl, List<KeyValuePair<string, string>> dataSource, string defaultItemText)
        {
            BindDropdown(ddl, dataSource, "key", "value", defaultItemText);
        }

        /// <summary>
        /// Bind dropdown list
        /// </summary>
        /// <param name="ddl"></param>
        /// <param name="dataSource"></param>
        /// <param name="textField"></param>
        /// <param name="valueField"></param>
        /// <param name="defaultItemText"></param>
        internal void BindDropdown(DropDownList ddl, List<KeyValuePair<string, string>> dataSource, string textField, string valueField, string defaultItemText)
        {
            ddl.DataSource = dataSource;
            ddl.DataValueField = valueField;
            ddl.DataTextField = textField;
            ddl.DataBind();
            if (defaultItemText != "")
                ddl.Items.Insert(0, new ListItem(defaultItemText, ""));
        }

        public void SortGridView(GridView gv, GridViewSortEventArgs e)
        {
            if (gv.Attributes["SortExpression"] != null && gv.Attributes["SortExpression"] == e.SortExpression.ToString())
            {
                if (gv.Attributes["SortDirection"].ToString() == ConvertSortDirectionToString(SortDirection.Ascending))
                {
                    gv.Attributes["SortDirection"] = ConvertSortDirectionToString(SortDirection.Descending);
                }
                else
                {
                    gv.Attributes["SortDirection"] = ConvertSortDirectionToString(SortDirection.Ascending);
                }
            }
            else
            {
                gv.Attributes["SortDirection"] = ConvertSortDirectionToString(SortDirection.Ascending);
                gv.Attributes["SortExpression"] = e.SortExpression.ToString();
            }
        }

        private string ConvertSortDirectionToString(SortDirection sortDirecition)
        {
            if (sortDirecition == SortDirection.Descending)
                return "DESC";
            return "ASC";
        }

        public string GetExclusiveCustomerByECKey(int ecKey)
        {
            try
            {
                string strKey = string.Empty;
                string ecID = string.Empty;
                foreach (string strKey_loopVariable in WebConfiguration.ProductColorKeys.Keys)
                {
                    strKey = strKey_loopVariable;
                    if (strKey.IndexOf("ExclusiveCustomer") >= 0 & Convert.ToInt32(WebConfiguration.ProductColorKeys[strKey]) == ecKey)
                    {
                        ecID = strKey.Replace("ExclusiveCustomer ", "");
                        break; // TODO: might not be correct. Was : Exit For
                    }
                }
                return ecID;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }
    }
}