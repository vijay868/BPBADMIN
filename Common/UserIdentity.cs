﻿using System;
using System.Security;
using System.Security.Principal;
using System.Collections.Specialized;

using Bestprintbuy.Admin.DataModel;

namespace Bestprintbuy.Admin.Web.Common
{
   /// <summary>
	/// Represents the Identity of a User. 
	/// Stores the details of a User. 
	/// Implements the IIDentity interface.
	/// </summary>
    [Serializable]
    public class UserIdentity : IIdentity
    {
        # region private variables

        private String userId;
        private int userPK;
        private bool login;
        private bool isSuperUser;
        private string fullName;
        private string userEmail;
        private int fkeyregister;
        private string role;
        private bool change_passwd;
        private bool can_communicate;
        private string exclusiveCustomer = string.Empty;
        private int exclusiveCustomerKey = -1;
        private bool allowCustomerCommunication = false;


        #endregion

        # region constructors
        /// <summary>
        /// The default constructor initializes any fields to their default values.
        /// </summary>
        public UserIdentity()
        {
            this.userPK = 0;
            this.userId = String.Empty;
            this.login = false;
            this.isSuperUser = false;
            this.fullName = String.Empty;
            this.userEmail = String.Empty;
            this.role = String.Empty;
            this.fkeyregister = -1;
        }

        /// <summary>
        /// Initializes a new instance of the CustomIdentity class 
        /// with the passed parameters
        /// </summary>
        /// <param name="uId">User ID of the user</param>
        /// <param name="upk">Primary Key of the User record in User table</param>
        /// <param name="islogin">Flag that indicates whether the user has been authenticated</param>
        /// <param name="isAdmin">Flag that indicates whether the user is an Administrator</param>
        /// <param name="userName">Full name of the User</param>
        /// <param name="email">Email of the User</param>
        public UserIdentity(
                    string uId,
                    int upk,
                    bool islogin,
                    bool isAdmin,
                    string userName,
                    string email,
                    string uRoles,
                    int fkeyregister)
        {
            this.userPK = upk;
            this.userId = uId;
            this.login = islogin;
            this.isSuperUser = isAdmin;
            this.fullName = userName;
            this.userEmail = email;
            this.role = uRoles;
            this.fkeyregister = fkeyregister;
        }

        #endregion

        # region properties
        // Properties
        /// <summary>
        /// Gets the Authentication Type
        /// </summary>
        public string AuthenticationType
        {
            get { return "Custom"; }
        }

        /// <summary>
        /// Indicates whether the User is authenticated
        /// </summary>
        public bool IsAuthenticated
        {
            get { return login; }
            set { login = value; }
        }

        /// <summary>
        /// Gets or sets the UserID of the User
        /// </summary>
        public string Name
        {
            get { return userId; }
            set { userId = value; }
        }

        /// <summary>
        /// Gets or sets the Primary Key for the User record
        /// </summary>
        public int UserPK
        {
            get { return userPK; }
            set { userPK = value; }
        }

        /// <summary>
        /// Indicates whether the User is an Administrator
        /// </summary>
        public bool IsSuperUser
        {
            //get { return "ADMIN".Equals(this.role.ToUpper()) && "Admin".Equals(this.userId); }
            get { return "admin".Equals(this.userId.ToLower()); }
            set { isSuperUser = value; }
        }

        /// <summary>
        /// Gets or sets the Full Name of the User
        /// </summary>
        public string UserFullName
        {
            get { return fullName; }
            set { fullName = value; }
        }

        /// <summary>
        /// Gets or sets the Email of the User
        /// </summary>
        public string UserEmail
        {
            get { return userEmail; }
            set { userEmail = value; }
        }


        public string UserRole
        {
            get { return role; }
            set { role = value; }
        }

        public int RegistryPkey
        {
            get { return fkeyregister; }
        }

        public bool isSupplier
        {
            get { return "SUPPLIER".Equals(this.role.ToUpper()); }
        }

        public bool isPartner
        {
            get { return "PARTNERS".Equals(this.role.ToUpper()); }
        }

        public bool isMerchant
        {
            get { return "MERCHANT ACCOUNTS".Equals(this.role.ToUpper()); }
        }

        public bool isCUstomer
        {
            get { return "USER".Equals(this.role.ToUpper()); }
        }

        public bool isWareHouseUser
        {
            get { return "WARE HOUSE USER".Equals(this.role.ToUpper()); }
        }

        public bool isSMUser
        {
            get { return "SALES & MARKETING USER".Equals(this.role.ToUpper()); }
        }

        public bool canChangePasswd
        {
            get { return change_passwd; }
            set { change_passwd = value; }
        }

        public bool canCommWithCustomer
        {
            get { return can_communicate; }
            set { can_communicate = value; }
        }

        public string ExclusiveCustomerOf
        {
            get { return exclusiveCustomer; }
            set { exclusiveCustomer = value; }
        }

        public int ExclusiveCustomerKey
        {
            get { return exclusiveCustomerKey; }
            set { exclusiveCustomerKey = value; }
        }

        public bool AllowCustomerCommunication
        {
            get { return allowCustomerCommunication; }
            set { allowCustomerCommunication = value; }
        }


        #endregion
    }
}