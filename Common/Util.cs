using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

using Bestprintbuy.Admin.DataModel;

namespace Bestprintbuy.Admin.Web.Common
{
    public class Util
    {
        public static decimal CMoney(string money, decimal defaultValue)
        {
            string numberPart = "";
            money = money.Trim();
            if (money.Length > 0)
            {
                bool foundDollarSign = false;
                for (int i = 0; i < money.Length; i++)
                {
                    string ch = money.Substring(i, 1);
                    switch (ch)
                    {
                        case "$":
                            if (foundDollarSign)
                            {
                                foundDollarSign = false;
                            }
                            break;
                        default:
                            numberPart += ch;
                            break;
                    }
                }
            }
            return CDecimal(numberPart, defaultValue);
        }

        public static decimal CDecimal(object obj, decimal defaultValue)
        {
            decimal returnValue = defaultValue;
            try
            {
                returnValue = Convert.ToDecimal(obj);
            }
            catch { }
            return returnValue;
        }

        public static Int64 CInt64(object obj)
        {
            return CInt64(obj, int.MinValue);
        }

        public static Int64 CInt64(object obj, Int64 defaultValue)
        {
            Int64 returnValue = defaultValue;
            try
            {
                returnValue = Convert.ToInt64(obj);
            }
            catch { }
            return returnValue;
        }

        public static bool CBool(object obj)
        {
            return CBool(obj, false);
        }

        public static bool CBool(object obj, bool defaultValue)
        {
            bool returnValue = defaultValue;
            try
            {
                returnValue = Convert.ToBoolean(obj);
            }
            catch { }
            return returnValue;
        }

        public static double CDouble(object obj)
        {
            return CDouble(obj, double.MinValue);
        }

        public static double CDouble(object obj, double defaultValue)
        {
            double returnValue = defaultValue;
            try
            {
                returnValue = Convert.ToDouble(obj);
            }
            catch { }

            return returnValue;
        }

        public static DateTime CDate(object obj)
        {
            return CDate(obj, DateTime.MinValue);
        }

        public static DateTime CDate(object obj, DateTime defaultValue)
        {
            DateTime returnValue = defaultValue;
            if (obj != null)
            {
                try
                {
                    returnValue = Convert.ToDateTime(obj);
                }
                catch { }
            }
            return returnValue;
        }

        public static string CString(object obj)
        {
            return CString(obj, "");
        }

        public static string CString(object obj, bool useDefaultValueOnBlankString)
        {
            return CString(obj, "", useDefaultValueOnBlankString);
        }

        public static string CString(
            object obj,
            string defaultValue,
            bool useDefaultValueOnBlankString)
        {
            string returnValue = defaultValue;
            if (obj != null)
            {
                try
                {
                    returnValue = Convert.ToString(obj);
                    if (useDefaultValueOnBlankString)
                    {
                        if (returnValue.Length == 0)
                        {
                            returnValue = defaultValue;
                        }
                    }
                }
                catch { }
            }
            return returnValue;
        }

        public static string CString(object obj, string defaultValue)
        {
            return CString(obj, defaultValue, true);
        }

        internal static void SaveFile(string l_strCertificateFilename, string l_StrContents)
        {
            FileInfo info = new FileInfo(l_strCertificateFilename);
            StreamWriter SWriter;
            if (info.Exists)
            {
                info.Delete();
            }
            SWriter = info.CreateText();
            SWriter.Write(l_StrContents);

            SWriter.Close();

            info.Refresh();
            info = null;
        }

        internal static void CheckDirExistance(string p_folderName)
        {
            DirectoryInfo l_TempDir = new DirectoryInfo(p_folderName);
            if (!l_TempDir.Exists)
            {
                l_TempDir.Create();
            }
            else
            {
                l_TempDir.Refresh();
            }
            l_TempDir = null;

        }

        internal static string ReadMailContent(int mailindex)
        {
            try
            {
                String strFile, strContent;
                strFile = ConfigurationManager.AppSettings["EmailCampaignTemplates"];
                StreamReader file;
                file = new StreamReader(String.Format(strFile, mailindex));
                strContent = file.ReadToEnd();
                file.Close();
                return strContent;
            }
            catch (FileNotFoundException)
            {
                return string.Empty;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        internal static string GetEmail()
        {
            string email = "nagasreem@inforaise.com";
            Random rndNo = new Random();
            int l_iIndex;

            l_iIndex = rndNo.Next(0, 6);
            switch (l_iIndex)
            {
                case 0:
                    return "nagasreem@inforaise.com";
                case 1:
                    return "test02@bestprintbuy.com";
                case 3:
                    return "test01@bestprintbuy.com";
                case 2:
                    return "sanjeevabrol@yahoo.com";
                case 4:
                    return "test01@bestprintbuy.com";
                case 5:
                    return "sabrol@bestprintbuy.com";
                case 6:
                    return "test02@bestprintbuy.com";
            }
            return email;
        }

        internal static string GetECHome(int fkey_ec_attribute)
        {
            switch (fkey_ec_attribute)
            {
                case 170: return "/keller-williams/index.htm";
                case 172: return "/best-realty/index.htm";
                case 173: return "/exit-realty/index.htm";
                case 174: return "/realty-executives/index.htm";
                case 175: return "/prudential/index.htm";
                case 176: return "/century-21/index.htm";
                case 177: return "/era/index.htm";
                case 178: return "/remax/index.htm";
                case 179: return "/assist-2-sell/index.htm";
                case 204: return "/realty-world/index.htm";
                case 205: return "/weichert/index.htm";
                case 206: return "/independent-realtor/index.htm";
                case 209: return "/coldwell-banker/index.htm";
                case 216: return "/downing-frye-realty/index.htm";
                case 217: return "/nextre/index.htm";
                case 218: return "/howard-hanna/index.htm";
                case 219: return "/realty-one-group/index.htm";
                case 220: return "/homesmart/index.htm";
                case 221: return "/long-and-foster/index.htm";
                case 222: return "/united-country/index.htm";
                case 223: return "/windermere/index.htm";
                case 224: return "/tarbell-realtors/index.htm";
                case 225: return "/first-team/index.htm";
                case 226: return "/real-living/index.htm";
                case 227: return "/jack-conway-realtor/index.htm";
                case 228: return "/avalar/index.htm";
                case 229: return "/realty-associates/index.htm";
                case 230: return "/john-l-scott/index.htm";
                case 231: return "/better-homes-and-gardens/index.htm";
                default: return "";
            }
        }
    }
}
