﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.Account
{
    public partial class CustomerInvoiceList : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCustomerInvoiceList();
            }
        }

        private void BindCustomerInvoiceList()
        {
            try
            {
                CustomerInvoicesModel model = new CustomerInvoicesModel();
                model = CustomerInvoicesService.GetList(model, ConnString);
                lblTotalRecords.Text = string.Format(" Total Number of Invoices : {0}", model.Invoices.Rows.Count);
                InvoiceListGrid.DataSource = model.Invoices;
                InvoiceListGrid.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected int GetRecordCountForPage()
        {
            return GetRecordCountForPageSize(InvoiceListGrid.PageSize, InvoiceListGrid.PageIndex + 1);
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            try
            {
                btnReset.Visible = true;
                CustomerInvoicesModel model = new CustomerInvoicesModel();
                string fromDate = txtFromDate.Text == "" ? "-1" : txtFromDate.Text;
                string todate = txtToDate.Text == "" ? "-1" : txtToDate.Text + " 23:59";
                model = CustomerInvoicesService.GetSearchList(model, GetSearchCondition(), fromDate, todate, -1, ConnString);

                InvoiceListGrid.DataSource = model.Invoices;
                InvoiceListGrid.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            BindCustomerInvoiceList();
            btnReset.Visible = false;
            txtFromDate.Text = "";
            txtToDate.Text = "";
            txtValue.Text = "";
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbtn = sender as LinkButton;
                CustomerInvoicesService.DeleteCustomerInvoices(lbtn.CommandArgument, false, ConnString);
                BindCustomerInvoiceList();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                if (ex.Message.IndexOf("FK_PPS_CUST_INVOICE") > -1)
                    AlertScript("Cannot delete customer invoice");
                else
                    AlertScript(ex.Message);
            }
        }

        /// <summary>
        /// GetSearchCondition is used to retirve the 
        /// Search critera and pattern fron the request and 
        /// form the named value collection and then
        /// form the Saerch string and return
        /// for eg. "PPS_CUSTOMERS.CUTOMER_ID LIKE 'nagasree%' "
        /// </summary>
        /// <returns>String holding the search condition</returns>
        public String GetSearchCondition()
        {
            //Variable holding the current session object
            System.Web.SessionState.HttpSessionState currsess = System.Web.HttpContext.Current.Session;

            //Variable holding the parameters - values from the request in a named value collection
            NameValueCollection requestCollections = System.Web.HttpContext.Current.Request.Params;

            //Get the Critera , pattern, operator from the request
            String criteria = ddlFilterFor.SelectedValue;
            String roperator = ddlOperator.SelectedValue;
            String pattern = txtValue.Text.Trim();
            String strFromDate = txtFromDate.Text;
            String strToDate = txtToDate.Text;

            //Check if pattern exists
            if ("".Equals(pattern)) return "-1";
            //if ("".Equals(strFromDate)) return "-1";
            //if ("".Equals(strToDate)) return "-1";

            //Create a new instance of named value collection
            NameValueCollection nvc = new NameValueCollection();

            //Add the Critera , pattern, operator to the nvc
            nvc.Add("criteria", criteria);
            nvc.Add("operator", roperator);
            nvc.Add("pattern", pattern);
            nvc.Add("txtFromDate", strFromDate);
            nvc.Add("txtToDate", strToDate);
            currsess["combovalues"] = nvc;

            //Invoke FormSearchString on validator set the
            //search condition and pattern on to the session and return the condition
            String searchfor = FormSearchString(nvc);
            currsess["search_condition"] = searchfor;
            currsess["pattern_value"] = pattern;
            currsess["txtFromDate"] = strFromDate;
            currsess["txtToDate"] = strToDate;
            return searchfor;
        }

        private String FormSearchString(NameValueCollection nvc)
        {
            String sOperator = nvc["operator"];
            sOperator = sOperator != null ? sOperator.Trim() : sOperator;
            string pattern = nvc["pattern"];
            StringBuilder sb = new StringBuilder();


            if (sOperator.IndexOf("LIKE") < 0)
            {
                pattern = pattern.Replace("'", "''");
                sb.Append(nvc["criteria"]).Append(" ").Append(sOperator).Append(" ").Append("'").Append(pattern).Append("'");
            }
            else if (sOperator != null && (sOperator.ToLower()).IndexOf("in") > 0)
            {
                sb.Append(nvc["criteria"]).Append("  ").Append(sOperator).Append(" (").Append(pattern).Append(")");
            }
            else
            {
                sOperator = sOperator.Replace("LIKE", pattern);
                sOperator = sOperator.Replace("'", "''");
                sb.Append(nvc["criteria"]).Append(" LIKE '").Append(sOperator).Append("'");
            }
            return sb.ToString();
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/Dashboard.aspx");
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("CustomerInvoiceDetail.aspx");
        }

        protected void InvoiceListGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }
    }
}