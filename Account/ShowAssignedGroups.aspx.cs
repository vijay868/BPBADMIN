﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;

namespace Bestprintbuy.Admin.Web.Account
{
    public partial class ShowAssignedGroups : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ViewState["userkey"] = Convert.ToInt32(Request.QueryString["userkey"]);
            if (!Page.IsPostBack)
            {
                GetData();
            }
        }

        private void GetData()
        {
            try
            {
                int userkey = Convert.ToInt32(ViewState["userkey"]);
                UserModel UserDetails = UserService.GetAssignedGroupsData(userkey, ConnString);

                rptrAssignedGroups.DataSource = UserDetails.group;
                rptrAssignedGroups.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }
    }
}