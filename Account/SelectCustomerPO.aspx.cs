﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.Account
{
    public partial class SelectCustomerPO : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCustomerPOs();
            }
        }

        private void BindCustomerPOs()
        {
            try
            {
                CustomerInvoicesModel model = new CustomerInvoicesModel();

                int fkeysupplier = Convert.ToInt32(Request.QueryString["fkeysupplier"] == null ? "-1" : Request.QueryString["fkeysupplier"]);
                int ListKeys = Convert.ToInt32(Request.QueryString["ListKeys"] == null ? "-1" : Request.QueryString["ListKeys"]);

                if (Request.QueryString["fkeysupplier"] != null)
                    model = CustomerInvoicesService.GetSupplierPendingPOList(model, ListKeys, fkeysupplier, ConnString);
                else
                    model = CustomerInvoicesService.GetPendingPOList(model, ListKeys, ConnString);

                rptrPOList.DataSource = model.CustomerPO;
                rptrPOList.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (RepeaterItem item in rptrPOList.Items)
                {
                    HiddenField hdnpkey_cust_po_master = (HiddenField)item.FindControl("hdnpkey_cust_po_master");
                    HiddenField hdnpo_number = (HiddenField)item.FindControl("hdnpo_number");
                    HiddenField hdnpo_date = (HiddenField)item.FindControl("hdnpo_date");

                    CheckBox chkbox = (CheckBox)item.FindControl("chkSelect");
                    if (chkbox.Checked == true)
                    {
                        string script = string.Format("SetDataAndClose('{0}','{1}','{2}');", hdnpkey_cust_po_master.Value, hdnpo_number.Value, hdnpo_date.Value);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "jscript", script, true);
                    }
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }
    }
}