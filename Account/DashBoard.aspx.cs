﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using System.Configuration;
using System.Data;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.Account
{
    public partial class DashBoard : System.Web.UI.Page
    {
        public string m_timeStamp = DateTime.Now.Ticks.ToString();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected UserModel accessModel = null;
        public bool HasAccess(string p_screenName, string p_functionaName)
        {
            try
            {
                UserIdentity identity = (new PageBase()).Identity;
                if (identity == null)
                    return false;
                if (identity.IsSuperUser)
                    return true;

                if (accessModel == null)
                    accessModel = UserService.GetUserRoles(identity.UserPK, ConfigurationManager.AppSettings["BestPrintBuyDBConn"]);
                string l_screenName = GetScreenName(p_screenName);
                string l_filter = "Screen_Name='" + l_screenName + "'";
                if (p_functionaName == "list")
                    l_filter += " and (listaccess = 'Yes' or listaccess = 'yes')";
                else if (p_functionaName == "view")
                    l_filter += " and (viewaccess = 'Yes' or viewaccess = 'yes')";
                else if (p_functionaName == "delete")
                    l_filter += " and (deleteaccess = 'Yes' or deleteaccess = 'yes')";
                else if (p_functionaName == "edit")
                    l_filter += " and (editaccess = 'Yes' or editaccess = 'yes')";
                else if (p_functionaName == "add")
                    l_filter += " and (addaccess = 'Yes' or addaccess = 'yes')";
                DataRow[] dr = accessModel.UserAccess.Select(l_filter);

                if (dr.Length == 1)
                    return true;
                
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
            }
            return false;
        }

        private string GetScreenName(string functionScreenName)
        {
            if (WebConfiguration.AdminLoginScreens.ContainsKey(functionScreenName))
                return WebConfiguration.AdminLoginScreens[functionScreenName].ToString();
            else if (WebConfiguration.PPSOLScreens.ContainsKey(functionScreenName))
                return WebConfiguration.PPSOLScreens[functionScreenName].ToString();
            else if (WebConfiguration.SupplierScreens.ContainsKey(functionScreenName))
                return WebConfiguration.SupplierScreens[functionScreenName].ToString();
            else if (WebConfiguration.PortalScreens.ContainsKey(functionScreenName))
                return WebConfiguration.PortalScreens[functionScreenName].ToString();
            return "";
        }
    }
}