﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="PaymentList.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Account.PaymentList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <script type="text/ecmascript">

         $().ready(function () {
             $("#frmPayments").validate();
         });
         function validate_twoDates() {
             var dateStart = $("#ContentPlaceHolder1_txtFromDate").val();
             var dateEnd = $("#ContentPlaceHolder1_txtToDate").val();
             if (Date.parse(dateStart) > Date.parse(dateEnd)) {
                 alert("Invalid date range!");
                 return false;
             }
         }
         $(function () {
             $("#ContentPlaceHolder1_txtFromDate").datepicker();
             $("#ContentPlaceHolder1_txtToDate").datepicker();
         });

         function validateSearch() {
             var dateStart = $("#ContentPlaceHolder1_txtFromDate").val();
             var dateEnd = $("#ContentPlaceHolder1_txtToDate").val();
             var txtValue = $("#ContentPlaceHolder1_txtValue").val();
             if (dateStart == "" && dateEnd == "" && txtValue == "") {
                 alert("Please define the criteria for applying search.");
                 return false;
             }
             if (Date.parse(dateStart) > Date.parse(dateEnd)) {
                 alert("Invalid date range!");
                 return false;
             }
             return true;
         }

         function PaymentView(pkeyPament) {
             url = "PaymentDetailsView.aspx?pkeyPayment=" + pkeyPament;
             $.fancybox({
                 'width': '50%',
                 'autoScale': true,
                 'transitionIn': 'fade',
                 'transitionOut': 'fade',
                 'href': url,
                 'type': 'iframe'
             });
            
             return false;
         }
    </script>
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Accounts</li>
            <li>Payments</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Payments</h2>
        <form id="frmPayments" name="frmPayments" runat="server">
            <table cellpadding="0" cellspacing="0" border="0" width="100%" class="accounts-table">

                <tbody>
                    <tr>
                        <td width="10%">
                            From Date
                        </td>
                          <td width="20%">
                                 <asp:TextBox name="txtFromDate" ID="txtFromDate" runat="server" Width="220px" CssClass="text-box date"></asp:TextBox>                   
                            </td>
                        <td width="5%">
                              To Date  
                        </td>
                         <td colspan="3" style="text-align:left;padding-left:3%">
                            <asp:TextBox name="txtToDate" ID="txtToDate"  runat="server" Width="220px"  CssClass="text-box date"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td width="10%">
                            Filter List For</td>
                        <td>
                            <asp:DropDownList name="ddlFilter" ID="ddlFilter" CssClass="slect-box" runat="server" Width="243px">
                                <asp:ListItem Value="PPS_BUSINESS_REGISTER.BUSINESS_ID">Supplier Code
                                </asp:ListItem>
                                <asp:ListItem Value="PPS_BR_PAYMENT_MASTER.PAYMENT_NUMBER">Payment Number
                                </asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:DropDownList name="ddloperator" ID="ddloperator" CssClass="slect-box" Width="80px" runat="server">
                                <asp:ListItem Value="=">Equals
                                </asp:ListItem>
                                <asp:ListItem Value="&lt;&gt;">Not Equals
                                </asp:ListItem>
                                <asp:ListItem Value="LIKE%">Starts With
                                </asp:ListItem>
                                <asp:ListItem Value="%LIKE">Ends with
                                </asp:ListItem>
                                <asp:ListItem Value="%LIKE%">Contains
                                </asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td style="padding-left:3%" width="25%">
                            <asp:TextBox runat="server" ID="txtValue" Width="220px"  CssClass="text-box" />
                        </td>
                        <td width="3%">
                            <asp:Button runat="server" ID="btnGo" CssClass="button rounded" Text="GO" OnClick="btnGo_Click" OnClientClick="if(!validateSearch()) return false;"></asp:Button>
                        </td>
                        <td width="3%">
                            <asp:Button runat="server" ID="btnReset" CssClass="button rounded" Text="Reset" Visible="false" OnClick="btnReset_Click" UseSubmitBehavior="false"></asp:Button>
                        </td>
                        <td width="20%"></td>
                    </tr>

                </tbody>
            </table>           
            <div class="clr"></div>

            <asp:GridView ID="PaymentGridGrid" runat="server" AutoGenerateColumns="false" EnableViewState="true"
                ForeColor="Black" Width="100%" PageSize="20" AllowPaging="true" AllowSorting="true" OnSorting="PaymentGridGrid_Sorting" OnPageIndexChanging="PaymentGridGrid_PageIndexChanging"
                class="table" name="PaymentGridGrid">
                <PagerStyle HorizontalAlign="Right" CssClass="dtPage"></PagerStyle>
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />
                <Columns>
                    <asp:TemplateField HeaderText="#SL" ItemStyle-Width="5%">
                        <HeaderStyle Width="5%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <span><%# GetRecordCountForPage()%></span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Payment Number" ItemStyle-Width="9%" SortExpression="payment_number">
                        <HeaderStyle Width="9%" ForeColor="White" />
                        <ItemTemplate>
                            <a href="#" onclick='return PaymentView("<%# Eval("pkey_payment_master")%>")'><%#Eval("payment_number") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Payment Date" ItemStyle-Width="10%" SortExpression="payment_date">
                        <HeaderStyle Width="10%" ForeColor="White" />
                        <ItemTemplate>
                            <%#Eval("payment_date","{0:MM/dd/yyyy}") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Supplier Code" ItemStyle-Width="9%" SortExpression="supplier_code">
                        <HeaderStyle Width="9%" ForeColor="White" />
                        <ItemTemplate>
                            <%#Eval("supplier_code") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Amount" ItemStyle-Width="9%" SortExpression="amount">
                        <HeaderStyle Width="9%" ForeColor="White" />
                        <ItemTemplate>
                            <%#Eval("amount") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
             <asp:Label ID="lblTotalRecords" runat="server" Font-Bold="true"></asp:Label>
            <div class="btn_lst">
                <div class="lft">
                    <asp:Button runat="server" ID="btnAdd" Text="Add a Payment" CssClass="button rounded" OnClick="btnAdd_Click" UseSubmitBehavior="false"></asp:Button>
                </div>
                <div class="rgt">
                    <asp:Button runat="server" ID="btnClose" Text="Close" CssClass="button rounded" OnClick="btnClose_Click" UseSubmitBehavior="false"></asp:Button>
                </div>
            </div>

        </form>

    </div>
</asp:Content>
