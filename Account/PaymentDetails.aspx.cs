﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;


namespace Bestprintbuy.Admin.Web.Account
{
    public partial class PaymentDetails : PageBase
    {
        protected int recordCount = 0;
        protected int paymentType = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetUniqueRefNo();
                BindPendingInvoceList();
            }
        }

        private void BindPendingInvoceList()
        {
            try
            {
                divAdvance.Visible = false;
                divInvoice.Visible = false;
                hdntotalOutStands.Value = "0";
                hdnpkey_customer.Value = "-1";
                if (rbtnAdvance.Checked)
                {
                    paymentType = 1;
                    divAdvance.Visible = true;
                }
                else if (rbtnOnAccount.Checked)
                {
                    paymentType = 2;
                    divAdvance.Visible = true;
                }
                else
                    divInvoice.Visible = true;
                int supplierkey = Convert.ToInt32(hdnSupplierkey.Value == "" ? "-2" : hdnSupplierkey.Value);
                PaymentModel model = PaymentService.GetPaymentData(-2, "-1", supplierkey, paymentType, ConnString);
                recordCount = model.PendingInvoices.Rows.Count;
                if (model.PendingInvoices.Rows.Count > 0)
                {
                    rptrInvoiceList.DataSource = model.PendingInvoices;
                    rptrInvoiceList.DataBind();
                    PaymentModel.PendingInvoicesRow row1 = model.PendingInvoices[0];
                    hdnpkey_cust_po_master.Value = row1.Isfkey_br_po_masterNull() ? "-2" : row1.fkey_br_po_master.ToString();
                    hdnInvoiceKey.Value = row1.Ispkey_br_invoice_masterNull() ? "-2" : row1.pkey_br_invoice_master.ToString();
                    hdntotalOutStands.Value = row1.Istotal_amountNull() ? "0.00" : row1.total_amount.ToString();
                    lblTotalAmt.Text = row1.Istotal_amountNull() ? "0.00" : row1.total_amount.ToString();
                    lblTotalBalance.Text = row1.Istotal_balanceNull() ? "0.00" : row1.total_balance.ToString();
                    lblTotalPaidAmt.Text = row1.Istotal_paidNull() ? "0.00" : row1.total_paid.ToString();
                }
                if (model.Supplier_Payment_Info.Rows.Count > 0)
                {
                    PaymentModel.Supplier_Payment_InfoRow Supplier_Payment_InfoRow = (PaymentModel.Supplier_Payment_InfoRow)model.Supplier_Payment_Info.Rows[0];
                    txtPayName.Text = Supplier_Payment_InfoRow.Iscontact_nameNull() ? "" : Supplier_Payment_InfoRow.contact_name;
                    lblAccountnumber.Text = Supplier_Payment_InfoRow.Isaccount_numberNull() ? "" : Supplier_Payment_InfoRow.account_number;
                    lblBankname.Text = Supplier_Payment_InfoRow.Isbank_nameNull() ? "" : Supplier_Payment_InfoRow.bank_name;
                    lblRoutingnumber.Text = Supplier_Payment_InfoRow.Isrouting_numberNull() ? "" : Supplier_Payment_InfoRow.routing_number;
                    ddlPaymentMode.SelectedValue = Supplier_Payment_InfoRow.Ispayment_modeNull() ? "CA" : Supplier_Payment_InfoRow.payment_mode;
                    hdnpkey_customer.Value = Supplier_Payment_InfoRow.Isfkey_payee_contactNull() ? "-1" : Supplier_Payment_InfoRow.fkey_payee_contact.ToString();
                }
                else {
                    txtPayName.Text = txtSupplier.Text;
                }
                if (model.Advance_payments.Rows.Count > 0)
                {
                    PaymentModel.Advance_paymentsRow row = model.Advance_payments[0];
                    lblAdvancePatyment.Text = row.Istotal_advance_paymentNull() ? "0.0" : row.total_advance_payment.ToString();
                    lblTotalInvoice.Text = row.Istotal_invoice_amountNull() ? "0.00" : row.total_invoice_amount.ToString();
                    lblTotalPaid.Text = row.Istotal_paid_amountNull() ? "0.00" : row.total_paid_amount.ToString();
                    lblTotalOutstanding.Text = row.Istotal_outstanding_amountNull() ? "0.00" : row.total_outstanding_amount.ToString();
                    hdntotalOutStands.Value = row.Istotal_advance_paymentNull() ? "0.00" : row.total_advance_payment.ToString();
                }
                else
                {
                    lblAdvancePatyment.Text = "0.0";
                    lblTotalInvoice.Text = "0.00";
                    lblTotalPaid.Text = "0.00";
                    lblTotalOutstanding.Text = "0.00";
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        private void GetUniqueRefNo()
        {
            try
            {
                lblPaymentNumbr.Text = CustomerReceiptsService.GetUniqueRefNo("Payments", ConnString);
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                PaymentModel model = new PaymentModel();
                PaymentModel.PaymentsRow row = (PaymentModel.PaymentsRow)model.Payments.NewPaymentsRow();
                PaymentModel.PaymentDetailsRow detailrow = (PaymentModel.PaymentDetailsRow)model.PaymentDetails.NewPaymentDetailsRow();

                int payment_type = 0;
                if (rbtnAdvance.Checked)
                    payment_type = 1;
                else if (rbtnOnAccount.Checked)
                    payment_type = 2;

                row.pkey_payment_master = -2;
                row.payment_number = lblPaymentNumbr.Text;
                row.payment_date = Convert.ToDateTime(txtPaymentDate.Text);
                row.payment_mode = ddlPaymentMode.SelectedValue;
                row.amount = Convert.ToDecimal(txtPaymentAmount.Text == "" ? "0" : txtPaymentAmount.Text);
                row.fkey_supplier = hdnSupplierkey.Value != "" ? Convert.ToInt32(hdnSupplierkey.Value) : -1;
                row.payment_type = payment_type;
                row.instrument_number = txtInstrumentNumber.Text;
                row.fkey_br_payment_info = Convert.ToInt32(hdnpkey_cust_po_master.Value == "" ? "-1" : hdnpkey_cust_po_master.Value);
                row.fkey_user = Identity.UserPK;

                model.Payments.AddPaymentsRow(row);

                //detailrow.fkey_payment_master = -2;
                //detailrow.fkey_invoice_master = Convert.ToInt32(hdnInvoiceKey.Value == "" ? "-1" : hdnInvoiceKey.Value); ;
                //detailrow.amount = Convert.ToDecimal(txtPaymentAmount.Text == "" ? "0" : txtPaymentAmount.Text);
                //detailrow.fkey_user = Identity.UserPK;

                //model.PaymentDetails.AddPaymentDetailsRow(detailrow);
                if (payment_type == 0)
                {
                    foreach (String key in Request.Form.AllKeys)
                    {
                        if (key.StartsWith("txtPayAmount"))
                        {
                            string invoiceKeystr = key.Trim();
                            int startIndex = "txtPayAmount".Length;
                            invoiceKeystr = invoiceKeystr.Substring(startIndex);
                            int invoiceKey = -1;
                            try
                            {
                                invoiceKey = Convert.ToInt32(invoiceKeystr);
                            }
                            catch (Exception) { }
                            double amount = Convert.ToDouble(txtPaymentAmount.Text == "" ? "-1.00" : txtPaymentAmount.Text);
                            if (amount > 0.00 && invoiceKey > 0)
                            {
                                detailrow.amount = Convert.ToDecimal(amount);
                                detailrow.fkey_invoice_master = Convert.ToInt32(hdnInvoiceKey.Value == "" ? "-1" : hdnInvoiceKey.Value);
                                detailrow.fkey_user = Identity.UserPK;
                                model.PaymentDetails.AddPaymentDetailsRow(detailrow);
                            }
                        }
                    }
                }
                else if (payment_type == 1)
                {
                    if (Convert.ToDouble(row["amount"]) > 0.00)
                    {
                        detailrow.amount = Convert.ToDecimal(row["amount"]);
                        detailrow.fkey_invoice_master = -3;
                        detailrow.fkey_user = Identity.UserPK;
                        model.PaymentDetails.AddPaymentDetailsRow(detailrow);
                    }
                }

                int Key = PaymentService.Update(model, ConnString);
                Response.Redirect("PaymentList.aspx");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void rbtnAdvance_CheckedChanged(object sender, EventArgs e)
        {
            BindPendingInvoceList();
        }

        protected void lbtnSupplier_Click(object sender, EventArgs e)
        {
            BindPendingInvoceList();
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("PaymentList.aspx");
        }
    }
}
