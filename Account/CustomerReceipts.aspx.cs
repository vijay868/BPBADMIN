﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using System.Data;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.Account
{
    public partial class CustomerReceipts : PageBase
    {
        protected CustomerStatsModel model = new CustomerStatsModel();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindReceipts();
            }
        }
        #region Private Methods

        /// <summary>
        /// 
        /// </summary>
        private void BindReceipts()
        {
            try
            {
                CustomerStatsModel.CustomerReceiptsDataTable customers = CustomerReceiptsService.GetList(txtFromDate.Text, txtToDate.Text, ddlFilter.SelectedValue, ConnString);

                if (CustomereceiptGrid.Attributes["SortExpression"] != null)
                    customers.DefaultView.Sort= CustomereceiptGrid.Attributes["SortExpression"] + " " + CustomereceiptGrid.Attributes["SortDirection"];
                lblTotalRecords.Text = string.Format("Total Number of Reciepts :{0} ", customers.Rows.Count);
                    CustomereceiptGrid.DataSource = customers;
                    CustomereceiptGrid.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        #endregion

        #region Events

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/Dashboard.aspx");
        }

        protected int GetRecordCountForPage()
        {
            return GetRecordCountForPageSize(CustomereceiptGrid.PageSize, CustomereceiptGrid.PageIndex + 1);
        }

        protected void CustomereceiptGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CustomereceiptGrid.PageIndex = e.NewPageIndex;

            BindReceipts();
        }
        protected void CustomereceiptGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            SortGridView(CustomereceiptGrid, e);
            CustomereceiptGrid.PageIndex = 0;
            BindReceipts();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
            
        {

            Response.Redirect("~/Account/CustomerReceiptsDetail.aspx");

        }
 
        protected void btnGo_Click(object sender, EventArgs e)
        {
            BindReceipts();
            btnReset.Visible = true;
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            btnReset.Visible = false;
            txtFromDate.Text = "";
            txtToDate.Text = "";
            BindReceipts();
        }
        #endregion


    }

}