﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="GroupDetails.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Account.GroupDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function SetGlobalAccess(chk, name) {
            var isChecked = chk.checked;
            if (name == "List") {
                if (!isChecked) {   // List unchecked 
                    name = name + ",View,Add,Edit,Delete";
                    //$("#ContentPlaceHolder1_chkGlbList").attr('checked', false);
                    $("#ContentPlaceHolder1_chkGlbView").attr('checked', false);
                    $("#ContentPlaceHolder1_chkGlbAdd").attr('checked', false);
                    $("#ContentPlaceHolder1_chkGlbEdit").attr('checked', false);
                    $("#ContentPlaceHolder1_chkGlbDelete").attr('checked', false);
                    SetAccessToItems(name, isChecked);
                }
                else {
                    SetAccessToItems(name, isChecked);
                }
            }
            else if (name == "View") {
                if (!isChecked) {
                    name = name + ",Add,Edit";
                    //$("#ContentPlaceHolder1_chkGlbView").attr('checked', false);
                    $("#ContentPlaceHolder1_chkGlbAdd").attr('checked', false);
                    $("#ContentPlaceHolder1_chkGlbEdit").attr('checked', false);
                    SetAccessToItems(name, isChecked);
                }
                else {
                    if (!$("#ContentPlaceHolder1_chkGlbList").is(':checked')) {
                        $("#ContentPlaceHolder1_chkGlbView").attr('checked', false);
                    }
                    SetAccessToItems(name, isChecked);
                }
            }
            else {
                SetAccessToItems(name, isChecked);
            }
        }

        function SetAccessToItems(names, isChecked) {
            // Loops all items in names and selects approriate checkbox
            $.each(names.split(','), function (key, name) {
                //alert(name);
                $('.Access_' + name + ' input:checkbox').each(function () {
                    var id = this.id;
                    // ContentPlaceHolder1_rptrGruopDetails_chkList_0
                    if (name == "List")
                        this.checked = isChecked;
                    else if (name == "View") {
                        if (isChecked) {
                            if ($("#" + id.replace("View", "List")).is(':checked'))
                                this.checked = true;
                            else
                                this.checked = false;
                        }
                        else {
                            this.checked = false;
                        }
                    }
                    else if (name == "Add") {
                        if (isChecked) {
                            if ($("#" + id.replace("Add", "View")).is(':checked'))
                                this.checked = true;
                            else
                                this.checked = false;
                        }
                        else {
                            this.checked = false;
                        }
                    }
                    else if (name == "Edit") {
                        if (isChecked) {
                            if ($("#" + id.replace("Edit", "View")).is(':checked'))
                                this.checked = true;
                            else
                                this.checked = false;
                        }
                        else {
                            this.checked = false;
                        }
                    }
                    else if (name == "Delete") {
                        if (isChecked) {
                            if ($("#" + id.replace("Delete", "List")).is(':checked'))
                                this.checked = true;
                            else
                                this.checked = false;
                        }
                        else {
                            this.checked = false;
                        }
                    }
                });
            });
        }

        function SetAccess(chk, name) {
            var isChecked = !chk.checked;
            var id = chk.id;
            if (name == "List") {
                if (isChecked) {
                    $("#" + id.replace("List", "View")).attr('checked', false);
                    $("#" + id.replace("List", "Add")).attr('checked', false);
                    $("#" + id.replace("List", "Edit")).attr('checked', false);
                    $("#" + id.replace("List", "Delete")).attr('checked', false);
                }
            }
            else if (name == "View") {
                if (isChecked) {
                    $("#" + id.replace("View", "Add")).attr('checked', false);
                    $("#" + id.replace("View", "Edit")).attr('checked', false);
                }
                else if (!$("#" + id.replace("View", "List")).is(':checked')) {
                    return false;
                }
            }
            else if (name == "Add") {
                if (!$("#" + id.replace("Add", "View")).is(':checked')) {
                    return false;
                }
            }
            else if (name == "Edit") {
                if (!$("#" + id.replace("Edit", "View")).is(':checked')) {
                    return false;
                }
            }
            else if (name == "Delete") {
                if (!$("#" + id.replace("Delete", "List")).is(':checked')) {
                    return false;
                }
            }
            return true;
        }

        function ClosePopup() {
            $("#divAssignedUsers").dialog("close");
        }

        function deleteGroup() {
            if ('<%=HasAccess("GroupList", "delete")%>' != 'False') {
                return confirm('This action results in removal of the Group Role(s) and associated user(s) will be detached from the Group, 	Are you sure?');
            }
            else {
                alert('User does not have access.');
                return false;
            }
        }
        function saveGroup() {

            return true;
        }
        function assignedUsers() {
            if ('<%=HasAccess("ShowAssignedUsers", "list")%>' == 'False') {
                alert('User does not have access.');
                return false;
            }
            else {
                ShowAssignedUsersPopup();
                return false;
            }
        }
        function ShowAssignedUsersPopup() {
            url = "ShowAssignedUsers.aspx?groupID=" + $("#ContentPlaceHolder1_hdnGroupId").val();
            $.fancybox({
                'width': '50%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': url,
                'type': 'iframe'
            });
            return false;
        }
    </script>
    <script>
        $().ready(function () {
            // validate the comment form when it is submitted
            $("#frmGroupDetails").validate();
        });
    </script>

    <div class="clr"></div>
    <!-- Container Starts -->
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="DashBoard.aspx">Home</a></li>
            <li class="liColor">Security</li>
            <li><a href="GroupList.aspx">Group List</a></li>
            <li>Group Details</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Group Details</h2>
        <form runat="server" name="frmGroupDetails" id="frmGroupDetails">

            <table width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="35%" style="vertical-align: top;">
                            <table width="100%" class="accounts-table">
                                <tbody>
                                    <tr>
                                        <td width="15%">
                                            <label>Group ID*</label></td>
                                        <td width="20%">
                                            <asp:TextBox CssClass="text-box" name="txtGroupID" ID="txtGroupID" runat="server" required></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Group Name*</label></td>
                                        <td>
                                            <asp:TextBox CssClass="text-box" name="txtGroupName" ID="txtGroupName" runat="server" required></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align:top">
                                            <label>Comments</label></td>
                                        <td>
                                            <asp:TextBox style="resize:none;" CssClass="text-box" name="txtComments" ID="txtComments" runat="server" Rows="8" Width="295px" TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td style="vertical-align: top;">
                            <table width="100%" class="accounts-table">
                                <tbody>
                                    <tr>
                                        <td>
                                            <label>Function/Process</label>
                                        </td>
                                        <td>
                                            <asp:DropDownList name="ddlBindfuncNames" ID="ddlBindfuncNames" CssClass="slect-box" runat="server" OnSelectedIndexChanged="ddlBindfuncNames_SelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div style="padding-left: 15%">
                                                <div class="clr"></div>
                                                <table class="tabled" width="91%">
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 50%;">Functional Screen</th>
                                                            <th>List<div class="center">
                                                                <asp:CheckBox ID="chkGlbList" runat="server" onclick="SetGlobalAccess(this, 'List')" />
                                                            </div>
                                                            </th>
                                                            <th>View<div class="center">
                                                                <asp:CheckBox ID="chkGlbView" runat="server" onclick="SetGlobalAccess(this, 'View')" />
                                                            </div>
                                                            </th>
                                                            <th>Add<div class="center">
                                                                <asp:CheckBox ID="chkGlbAdd" runat="server" onclick="SetGlobalAccess(this, 'Add')" />
                                                            </div>
                                                            </th>
                                                            <th>Edit<div class="center">
                                                                <asp:CheckBox ID="chkGlbEdit" runat="server" onclick="SetGlobalAccess(this, 'Edit')" />
                                                            </div>
                                                            </th>
                                                            <th>Delete<div class="center">
                                                                <asp:CheckBox ID="chkGlbDelete" runat="server" onclick="SetGlobalAccess(this, 'Delete')" />
                                                            </div>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <asp:Repeater ID="rptrGruopDetails" runat="server">
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblName" runat="server" Text='<%#Eval("screen_name") %>' />
                                                                        <asp:HiddenField ID="hdnAccessID" runat="server" Value='<%#Eval("pkey_access") %>' />
                                                                        <asp:HiddenField ID="hdnScreenKey" runat="server" Value='<%#Eval("fkey_screen") %>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkList" runat="server" Checked='<%#Eval("access_list") %>' CssClass="Access_List" onclick="if(!SetAccess(this, 'List')) return false;" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkView" runat="server" Checked='<%#Eval("access_view") %>' CssClass="Access_View" onclick="if(!SetAccess(this, 'View')) return false;" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkAdd" runat="server" Checked='<%#Eval("access_add") %>' CssClass="Access_Add" onclick="if(!SetAccess(this, 'Add')) return false;" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkEdit" runat="server" Checked='<%#Eval("access_edit") %>' CssClass="Access_Edit" onclick="if(!SetAccess(this, 'Edit')) return false;" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkDelete" runat="server" Checked='<%#Eval("access_delete") %>' CssClass="Access_Delete" onclick="if(!SetAccess(this, 'Delete')) return false;" />
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>

            <div class="btn_lst">
                <div class="lft">
                    <asp:Button ID="btnShowAssignedUsrs" Text="Show Assigned Users" CssClass="button rounded" Visible="false" runat="server" OnClientClick="return assignedUsers();"></asp:Button>
                    <asp:HiddenField ID="hdnGroupId" runat="server" />
                </div>
                <div class="rgt">
                    <asp:Button ID="btnClose" Text="Close" CssClass="button rounded" runat="server" OnClick="btnClose_Click" UseSubmitBehavior="false" type="button"></asp:Button>
                    <asp:Button ID="btnSave" Text="Save" CssClass="button rounded" runat="server" OnClick="btnSave_Click" OnClientClick="return saveGroup();"></asp:Button>
                    <asp:Button ID="btnDelete" Text="Delete" CssClass="button rounded" runat="server" Visible="false" OnClick="btnDelete_Click" OnClientClick="return deleteGroup();"></asp:Button>
                </div>
            </div>
        </form>
    </div>
</asp:Content>
