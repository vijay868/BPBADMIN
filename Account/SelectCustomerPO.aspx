﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SelectCustomerPO.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Account.SelectCustomerPO" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Best Print Buy</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" media="all" href="../css/bestprintbuy_style.css" />
    <link rel="stylesheet" media="all" href="../css/jquery.fancybox.css" />
    <link rel="stylesheet" href="../css/jquery-ui.css" />
    <script src="../Scripts/jquery.js"></script>
    <script src="../Scripts/jquery-ui.js"></script>
    <script src="../Scripts/common.js"></script>
    <script src="../Scripts/ddsmoothmenu.js"></script>
    <script src="../Scripts/plugin/fancybox/jquery.fancybox.js"></script>
    <script src="../Scripts/jquery.validate.js"></script>
    <!--[if lt IE 9]>
			<script src="../Scripts/html5.js"></script>
		<![endif]-->
    <!--[if lte IE 7]> <html class="ie7"> <![endif]-->
    <!--[if IE 8]>     <html class="ie8"> <![endif]-->
    <!--[if IE 9]>     <html class="ie9"> <![endif]-->
    <!--[if IE 10]>     <html class="ie10"> <![endif]-->
    <!--[if !IE]><!-->
    <!--<![endif]-->
    <script type="text/javascript">
        function customerSelect(obj) {
            var grid = obj.parentNode.parentNode.parentNode.parentNode;
            var inputs = grid.getElementsByTagName("input");
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].type == "checkbox") {
                    if (obj.checked && inputs[i] != obj && inputs[i].checked) {
                        inputs[i].checked = false;
                    }
                }
            }
        }

        function SetDataAndClose(pkey_cust_po_master, po_number, po_date) {
            window.top.SetCustomerPOData(pkey_cust_po_master, po_number, po_date);
            return false;
        }

        function checkSelect() {
            var ischecked = false;
            $('.select input:checkbox').each(function () {
                if (this.checked) {
                    ischecked = true;
                    return true;
                }
            });

            if (!ischecked) {
                alert("Please select atleast one product.");
                return false;
            }
            return true;
        }
    </script>
</head>
<body id="Body" runat="server">
    <!-- Wrapper starts -->
    <div id="Div1" class="popup-wrapper" runat="server">
        <div class="popup-title">
            Orders List
        </div>
        <!-- Header Ends -->
        <div class="popup-contnt">
            <!-- Menu starts -->
            <form id="Form1" runat="server">
                <table class="tablep">
                    <thead>
                        <tr>
                            <th>Order ID</th>
                            <th>Order Date</th>
                            <th>PO Value</th>
                            <th>Status</th>
                            <th>Select</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rptrPOList" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <%#Eval("po_number") %>
                                    </td>
                                    <td>
                                        <%#Eval("po_date") %>
                                    </td>
                                    <td>
                                        <%#Eval("povalue") %>
                                    </td>
                                    <td>
                                        <%#Eval("po_status") %>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkSelect" runat="server" onclick="customerSelect(this);" CssClass="select"/>
                                        <asp:HiddenField ID="hdnpkey_cust_po_master" runat="server" Value='<%#Eval("pkey_cust_po_master") %>' />
                                        <asp:HiddenField ID="hdnpo_number" runat="server" Value='<%#Eval("po_number") %>' />
                                        <asp:HiddenField ID="hdnpo_date" runat="server" Value='<%#Eval("po_date") %>' />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
                <table>
                    <tr>
                        <td style="float: right;">
                            <asp:Button runat="server" ID="btnSelect" CssClass="button rounded" Text="Select" OnClick="btnSelect_Click" OnClientClick="if(!checkSelect()) return false;"></asp:Button>
                            <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClientClick="parent.$.fancybox.close(); return false;"></asp:Button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</body>
</html>
