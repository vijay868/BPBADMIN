﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="CustomerPayment.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Account.CustomerPayment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
        $().ready(function () {
            $("#frmCustomerPayment").validate();
        });

        function CheckValidDate() {
            var m_cartCost = ($("#ContentPlaceHolder1_txtAmount").val() == 'undefined' ? 0 : $("#ContentPlaceHolder1_txtAmount").val()) * 1.0;
            if (m_cartCost > 0) {
                var Exp_month = '<%= DateTime.Now.Month%>';
                var Exp_year = '<%= DateTime.Now.Year.ToString().Substring(2,2)%>';
                if (($("#ContentPlaceHolder1_ddlMonth").val() < (Exp_month * 1) && $("#ContentPlaceHolder1_ddlYear").val() <= (1 * Exp_year)) || ($("#ContentPlaceHolder1_ddlYear").val() < (1 * Exp_year))) {
                    alert('Valid upto date cannot be prior to the current date.');
                    $("#ContentPlaceHolder1_ddlMonth").focus();
                    return false;
                }
            }
            if (validateCardNumber() == false) {
                $("#ContentPlaceHolder1_txtCreditcardNumber").focus();
                return false;
            }
            return true;
        }

        function validateCardNumber() {
            var txtCardNo = $("#ContentPlaceHolder1_txtCreditcardNumber").val();
            var option = $("#ContentPlaceHolder1_ddlCreditCardtype option:selected").val();
            if (option == 637) {
                if ((!(txtCardNo.substring(0, 2) == 34) && !(txtCardNo.substring(0, 2) == 37))) {
                    alert("Invalid Credit Card Number");
                    return false;
                }
            }

            if (option == 267) {
                if (!((txtCardNo.substring(0, 2) > 50) && (txtCardNo.substring(0, 2) < 56))) {
                    alert("Invalid Credit Card Number");
                    return false;
                }
            }

            if (option == 268) {
                if (!(txtCardNo.substring(0, 1) == 4)) {
                    alert("Invalid Credit Card Number");
                    return false;
                }
            }
            return true;
        }

        function FetchCustomer() {
            __doPostBack("ctl00$ContentPlaceHolder1$lbtnFetchCustomer", "");
        }
    </script>
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor" >Accounts</li>
            <li><a href="CustomerPayments.aspx">Customer Payments</a></li>
            <li>Customer Information</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Customer Information</h2>
        <form id="frmCustomerPayment" name="frmCustomerPayment" runat="server">
            <table cellpadding="0" cellspacing="0" border="0" width="100%" class="accounts-table">
                <thead>
                    <tr>
                        <th colspan="4" width="100%">Customer Information</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="15%">
                            <label>Login By*</label></td>
                        <td width="20%">
                            <asp:DropDownList name="ddlLoginby" ID="ddlLoginby" CssClass="slect-box" runat="server" required>
                                <asp:ListItem Value="1">Email Address</asp:ListItem>
                                <asp:ListItem Value="2">Purchase Order Number</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td width="15%" style="vertical-align:top">
                            <label>Notes</label>
                        </td>
                        <td width="35%" rowspan="2">
                            <asp:TextBox CssClass="text-box" name="txtNote" ID="txtNote" runat="server" TextMode="MultiLine" style="resize:none" Rows="5" Width="295px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Email Addr/Po Number*</label></td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txt" ID="txtEmailAddress" onblur="FetchCustomer();" runat="server" required></asp:TextBox>
                            <asp:LinkButton ID="lbtnFetchCustomer" OnClick="lbtnFetchCustomer_Click" runat="server"></asp:LinkButton>
                        </td>
                    </tr>
                </tbody>
            </table>

            <table cellpadding="0" cellspacing="0" border="0" width="100%" class="accounts-table">
                <thead>
                    <tr>
                        <th colspan="4" width="100%">Contact Information</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="15%">
                            <label>First Name*</label></td>
                        <td width="20%">
                            <asp:TextBox CssClass="text-box" name="txtFirstName" ID="txtFirstName" runat="server" required></asp:TextBox>
                        </td>
                        <td width="15%">
                            <label>Country*</label></td>
                        <td width="35%">
                            <asp:DropDownList name="ddlCountry" ID="ddlCountry" CssClass="slect-box" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" required>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Last Name*</label></td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtLastName" ID="txtLastName" runat="server" required></asp:TextBox>
                        </td>
                        <td>
                            <label>State*</label></td>
                        <td>
                            <asp:DropDownList name="ddlState" ID="ddlState" CssClass="slect-box" runat="server" required>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Address 1*</label></td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtAddress1" ID="txtAddress1" runat="server" required></asp:TextBox>
                        </td>
                        <td>
                            <label>Zip*</label></td>
                        <td>
                            <asp:TextBox CssClass="text-box digits" name="txtZip" ID="txtZip" runat="server" required></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Address 2</label></td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtAddress2" ID="txtAddress2" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <label>Phone</label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtPhone" ID="txtPhone" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>City*</label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtCity" ID="txtCity" runat="server" required></asp:TextBox>
                        </td>
                    </tr>
                </tbody>
            </table>

            <table cellpadding="0" cellspacing="0" border="0" width="100%" class="accounts-table">
                <thead>
                    <tr>
                        <th colspan="4" width="100%">Payment Details</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="15%">
                            <label>Credit Card type*</label></td>
                        <td width="20%">
                            <asp:DropDownList name="ddlCreditCardtype" ID="ddlCreditCardtype" CssClass="slect-box" runat="server" required>
                            </asp:DropDownList>
                        </td>
                        <td width="15%">
                            <label>Valid Upto*</label></td>
                        <td width="35%">
                            <asp:DropDownList name="ddlValidUpto" ID="ddlMonth" CssClass="slect-box" runat="server" style="width:37%" required>
                               <asp:ListItem Value="">--</asp:ListItem>
                                <asp:ListItem Value="01">01</asp:ListItem>
                                <asp:ListItem Value="02">02</asp:ListItem>
                                <asp:ListItem Value="03">03</asp:ListItem>
                                <asp:ListItem Value="04">04</asp:ListItem>
                                <asp:ListItem Value="05">05</asp:ListItem>
                                <asp:ListItem Value="06">06</asp:ListItem>
                                <asp:ListItem Value="07">07</asp:ListItem>
                                <asp:ListItem Value="08">08</asp:ListItem>
                                <asp:ListItem Value="09">09</asp:ListItem>
                                <asp:ListItem Value="10">10</asp:ListItem>
                                <asp:ListItem Value="11">11</asp:ListItem>
                                <asp:ListItem Value="12">12</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList name="ddlYear" ID="ddlYear" CssClass="slect-box" runat="server" style="width:38%" requried>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Creditcard Number*</label></td>
                        <td>
                            <asp:TextBox CssClass="text-box digits" name="txtCreditcardNumber" ID="txtCreditcardNumber" runat="server" required></asp:TextBox>
                        </td>
                        <td>
                            <label>Name on the Card*</label></td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtNameontheCard" ID="txtNameontheCard" runat="server" required></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Amount*</label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box number" name="txtAmount" ID="txtAmount" runat="server" required></asp:TextBox>
                        </td>
                        <td>
                            <label><span id="spnRef" runat="server">PN Ref#</span></label>
                        </td>
                        <td>
                            <asp:Label ID="lblPNRef" runat="server" />
                        </td>
                    </tr>
                </tbody>
            </table>

            <table cellpadding="0" cellspacing="0" border="0" width="100%" class="accounts-table mbot100">
                <tbody>
                    <tr>
                        <td colspan="2"></td>
                        <td colspan="2" class="rgt" align="right">
                            <asp:Button runat="server" CssClass="button rounded" ID="btnSave" Text="Make Payment" OnClick="btnSave_Click" OnClientClick="return CheckValidDate();" Visible="false"></asp:Button>
                            <asp:Button runat="server" CssClass="button rounded close-btn" ID="btnClose" Text="Close" UseSubmitBehavior="false" OnClick="btnClose_Click"></asp:Button>
                        </td>
                    </tr>
                </tbody>
            </table>
            <!-- table Ends -->
        </form>
    </div>
</asp:Content>

