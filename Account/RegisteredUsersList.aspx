﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="RegisteredUsersList.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Account.RegisteredUsersList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="clr"></div>
    <!-- Container Starts -->
    <div class="contant_hldr" style="padding-bottom: 15px">
        <ul class="breadcrumbs">
            <li><a href="DashBoard.aspx">Home</a></li>
            <li class="liColor">Customer Service</li>
            <li>User Statistics</li>
        </ul>
        <h2>Logged Customers</h2>
        <form class="form" runat="server" name="reguserlist" id="reguserlist">
            <div class="form_sep_left" style="float: left;">
                <table class="accounts-table" style="width: 40%;">
                    <tr>
                        <td>Logged Users</td>
                        <td>
                            <asp:Label ID="lblLoggedUsers" runat="server" Font-Size="12px" Font-Bold="true"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>Anonymous Users</td>
                        <td>
                            <asp:Label ID="lblAnonymousUsers" runat="server" Font-Size="12px" Font-Bold="true"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>Total Users</td>
                        <td>
                            <asp:Label ID="lblTotalUsers" runat="server" Font-Size="12px" Font-Bold="true"></asp:Label></td>
                    </tr>
                </table>
            </div>           
            <div class="clr"></div>
            <div class="table_align">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#SL</th>
                            <th>Customer Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>IP Address</th>
                            <th>Login Time</th>
                            <th>Admin</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rptrLoggedUserslist" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <%#Container.ItemIndex + 1 %>
                                    </td>
                                    <td>
                                        <%#Eval("custname") %>
                                    </td>
                                    <td>
                                        <%#Eval("email") %>
                                    </td>
                                    <td>
                                        <%#Eval("phone") %>
                                    </td>
                                    <td>
                                        <%#Eval("ipaddress") %>
                                    </td>
                                    <td>
                                        <%#Eval("logintime") %>
                                    </td>
                                    <td>
                                        <%#Eval("isadmin") %>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
            <div class="ttl"><b>Total Number of Customer Credits: </b><%=LoggedUsers%></div>
            <div class="btn_lst">
                <div class="rgt">
                    <asp:Button runat="server" CssClass="button rounded" ID="btnClose" Text="Close" OnClick="btnClose_Click"></asp:Button>
                </div>
            </div>
        </form>
    </div>
</asp:Content>
