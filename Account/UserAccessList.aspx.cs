﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;

namespace Bestprintbuy.Admin.Web.Account
{
    public partial class UserAccessList : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ViewState["userkey"] = Convert.ToInt32(Request.QueryString["userkey"]);
            if (!Page.IsPostBack)
            {
                GetData();
            }
        }

        private void GetData()
        {
            try
            {
                int userkey = Convert.ToInt32(ViewState["userkey"]);
                UserModel UserDetails = UserService.GetUserAccessData(userkey, ConnString);

                rptrUserAccesslist.DataSource = UserDetails.group;
                rptrUserAccesslist.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }
    }
}