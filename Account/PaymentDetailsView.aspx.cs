﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.Account
{
    public partial class PaymentDetailsView : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCustomerReceiptView();
            }
        }

        private void BindCustomerReceiptView()
        {
            try
            {
                int receiptkey = Convert.ToInt32(Request.QueryString["pkeyPayment"]);
                PaymentModel model = PaymentService.GetViewPayment(receiptkey, ConnString);

                if (model.Payments.Rows.Count > 0)
                {
                    PaymentModel.PaymentsRow row = model.Payments[0];
                    lblPaymentNumbr.Text = row.payment_number;
                    lblPaymentDate.Text = row.payment_date.ToString("MM/dd/yyyy") + "{MM/DD/YYYY}";
                    lblModeOfPayment.Text = row.payment_mode;
                    lblInstrumentNumber.Text = row.instrument_number;
                    lblReceiptAmount.Text = row.amount.ToString();

                    if (row.payment_type == 1 || row.payment_type == 2)
                        tdAdvance.Visible = true;
                    else if (row.payment_type == 0)
                        divInvoice.Visible = true;

                    if (row.payment_type == 0)
                        rbtnInvoice.Checked = true;
                    else if (row.payment_type == 2)
                        rbtnOnacoount.Checked = true;
                    else if (row.payment_type == 1)
                        rbtnAdvance.Checked = true;
                }
                if (model.Supplier_Payment_Info.Rows.Count > 0)
                {
                    PaymentModel.Supplier_Payment_InfoRow row = model.Supplier_Payment_Info[0];
                    lblSupplier.Text = row.contact_name;
                    lblPayTo.Text = row.contact_name;
                }
                if (model.Advance_payments.Rows.Count > 0)
                {
                    PaymentModel.Advance_paymentsRow row = model.Advance_payments[0];
                    lblTotalAdvanceReceipts.Text = row.Istotal_advance_paymentNull() ? "0.0" : row.total_advance_payment.ToString();
                    lblTotalInvoiced.Text = row.Istotal_invoice_amountNull() ? "0.00" : row.total_invoice_amount.ToString();
                    lblOtherRctAmount.Text = row.Istotal_paid_amountNull() ? "0.00" : row.total_paid_amount.ToString();
                    lblTotalOutstanding.Text = row.Istotal_outstanding_amountNull() ? "0.00" : row.total_outstanding_amount.ToString();
                }
                if (model.PendingInvoices.Rows.Count > 0)
                {
                    // rptrInvoiceList.da                   
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }
    }
}