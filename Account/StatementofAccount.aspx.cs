﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.Account
{
    public partial class StatementofAccount : PageBase
    {
        protected Double balance = 0.00;
        protected Double totalDebit = 0.00;
        protected Double totalCredit = 0.00;
        protected Boolean rowexists = false;
        protected int pkey_register = -1;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                pkey_register = Convert.ToInt32(Request.QueryString["pkey_register"] == null ? "-1" : Request.QueryString["pkey_register"]);
                hdnSupplierkey.Value = pkey_register.ToString();
                BindStatementofAccount();
            }
        }

        private void BindStatementofAccount()
        {
            try
            {
                CustomerInvoicesModel model = new CustomerInvoicesModel();
                model = CustomerInvoicesService.UpdateStatementOfAccounts(model, pkey_register, ConnString);
                if (StatementofAccountGrid.Attributes["SortExpression"] != null)
                    model.BusinessTransactions.DefaultView.Sort = StatementofAccountGrid.Attributes["SortExpression"] + " " + StatementofAccountGrid.Attributes["SortDirection"];
                StatementofAccountGrid.DataSource = model.BusinessTransactions;
                StatementofAccountGrid.DataBind();
                lblCount.Text = "0";
                if (model.BusinessTransactions.Rows.Count > 0)
                {
                    rowexists = true;
                    lblCount.Text = model.BusinessTransactions.Rows.Count.ToString();
                }
                if (model.Business.Rows.Count > 0)
                {
                    CustomerInvoicesModel.BusinessRow row = (CustomerInvoicesModel.BusinessRow)model.Business.Rows[0];
                    lblSupplier.Text = row.Isbusiness_nameNull() ? "" : row.business_name;
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void StatementofAccountGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            SortGridView(StatementofAccountGrid, e);
            StatementofAccountGrid.PageIndex = 0;
            BindStatementofAccount();
        }

        protected void StatementofAccountGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            StatementofAccountGrid.PageIndex = e.NewPageIndex;
            BindStatementofAccount();
        }

        protected int GetRecordCountForPage()
        {
            return GetRecordCountForPageSize(StatementofAccountGrid.PageSize, StatementofAccountGrid.PageIndex + 1);
        }

        public string GetBalance(object obj, object obj1)
        {
            try
            {
                double credit = 0;
                double debit = 0;
                credit = Convert.ToDouble(obj.ToString() == "" ? "0.00" : obj.ToString());
                debit = Convert.ToDouble(obj1.ToString() == "" ? "0.00" : obj1.ToString());
                totalCredit = totalCredit + credit;
                totalDebit = totalDebit + debit;
                balance = balance + (credit - debit);
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
            return balance.ToString();
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/DashBoard.aspx");
        }

        protected void lbtnSupplier_Click(object sender, EventArgs e)
        {
            pkey_register = hdnSupplierkey.Value != "" ? Convert.ToInt32(hdnSupplierkey.Value) : -1;
            BindStatementofAccount();
        }
    }
}