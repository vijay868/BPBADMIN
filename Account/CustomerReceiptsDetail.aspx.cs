﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
namespace Bestprintbuy.Admin.Web.Account
{
    public partial class CustomerReceiptsDetail : PageBase
    {
        protected int is_advance = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetUniqueRefNo();
                BindPendingInvoceList();
            }
        }

        private void BindPendingInvoceList()
        {
            try
            {
                divAdvance.Visible = false;
                divInvoice.Visible = false;
                hdntotalOutStands.Value = "0";
                if (rbtnAgainstInvoice.Checked)
                {
                    is_advance = 1;
                    divInvoice.Visible = true;
                }
                else if (rbtnOnAccount.Checked)
                {
                    is_advance = 2;
                    divAdvance.Visible = true;
                }
                else
                    divAdvance.Visible = true;
                int pkeyCustomer = Convert.ToInt32(hdnpkey_customer.Value == "" ? "-1" : hdnpkey_customer.Value);
                CustomerStatsModel model = CustomerReceiptsService.GetPendingInvoceList(pkeyCustomer, is_advance, ConnString);

                if (model.CustomerInfo.Rows.Count > 0)
                {
                    CustomerStatsModel.CustomerInfoRow CustomerInfoRow = model.CustomerInfo[0];
                    lblCustEmail.Text = CustomerInfoRow.Iscustomer_idNull() ? "" : CustomerInfoRow.customer_id;
                    lblCustName.Text = CustomerInfoRow.Iscustomer_nameNull() ? "" : CustomerInfoRow.customer_name;
                }

                if (model.PendingInvoices.Rows.Count > 0)
                {
                    rptrInvoiceList.DataSource = model.PendingInvoices;
                    rptrInvoiceList.DataBind();
                    CustomerStatsModel.PendingInvoicesRow row1 = model.PendingInvoices[0];
                    hdnpkey_cust_po_master.Value = row1.Ispkey_cust_po_masterNull() ? "-2" : row1.pkey_cust_po_master.ToString();
                    hdnInvoiceKey.Value = row1.Ispkey_cust_invoice_masterNull() ? "-2" : row1.pkey_cust_invoice_master.ToString();
                    hdntotalOutStands.Value = row1.Istotal_amountNull() ? "0.00" : row1.total_amount.ToString();
                }
                if (model.Advance_Receipts.Rows.Count > 0)
                {
                    CustomerStatsModel.Advance_ReceiptsRow row = model.Advance_Receipts[0];
                    lblAdvancePatyment.Text = row.Istotal_advance_paymentNull() ? "0.0" : row.total_advance_payment.ToString();
                    lblTotalInvoice.Text = row.Istotal_invoice_amountNull() ? "0.00" : row.total_invoice_amount.ToString();
                    lblTotalPaid.Text = row.Istotal_paid_amountNull() ? "0.00" : row.total_paid_amount.ToString();
                    lblTotalOutstanding.Text = row.Istotal_outstanding_amountNull() ? "0.00" : row.total_outstanding_amount.ToString();
                    hdntotalOutStands.Value = row.Istotal_advance_paymentNull() ? "0.00" : row.total_advance_payment.ToString();
                }
                else
                {
                    lblAdvancePatyment.Text = "0.0";
                    lblTotalInvoice.Text = "0.00";
                    lblTotalPaid.Text = "0.00";
                    lblTotalOutstanding.Text = "0.00";
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        private void GetUniqueRefNo()
        {
            try
            {
                lblReceiptNumber.Text = CustomerReceiptsService.GetUniqueRefNo("CustomerReceipts", ConnString);
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void lbtnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                BindPendingInvoceList();
                CustomerModel.CustomersDataTable customers = CustomerReceiptsService.GetCustomerList("-1", Convert.ToInt32(hdnpkey_customer.Value), ConnString);
                foreach (CustomerModel.CustomersRow row in customers.Rows)
                {
                    lblCustomerId.Text = row.customer_id;
                    lblFirstName.Text = row.cus_firstname;
                    lblLastName.Text = row.cus_lastname;
                    lblTitle.Text = row.title;
                    lblCompanyName.Text = row.company_name;
                    lblAddress1.Text = row.address1;
                    lblAddress2.Text = row.address2;
                    lblCity.Text = row.city;
                    lblState.Text = row.state;
                    lblZip.Text = row.zip;
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                CustomerStatsModel model = new CustomerStatsModel();
                CustomerStatsModel.CustomerReceiptsRow row = (CustomerStatsModel.CustomerReceiptsRow)model.CustomerReceipts.NewCustomerReceiptsRow();
                CustomerStatsModel.ReceiptDetailsRow receiptrow = (CustomerStatsModel.ReceiptDetailsRow)model.ReceiptDetails.NewReceiptDetailsRow();

                int receipt_type = 1;
                if (rbtnAdvance.Checked)
                    receipt_type = 0;
                else if (rbtnOnAccount.Checked)
                    receipt_type = 2;

                row.pkey_cust_manual_receipt = -2;
                row.receipt_number = lblReceiptNumber.Text;
                row.receipt_date = Convert.ToDateTime(txtReceiptDate.Text);
                row.payment_mode = ddlPaymentMode.SelectedValue;
                row.receipt_amount = Convert.ToDecimal(txtReceiptAmount.Text == "" ? "0.00" : txtReceiptAmount.Text);
                row.fkey_customer = Convert.ToInt32(hdnpkey_customer.Value);
                row.is_advance = rbtnAdvance.Checked ? 1 : 0;
                row.is_invoice = rbtnAgainstInvoice.Checked ? 1 : 0;
                row.is_account = rbtnOnAccount.Checked ? 1 : 0;
                row.receipt_type = receipt_type;
                row.instrument_no = txtInstrumentNumber.Text;
                row.fkey_cust_po_master = Convert.ToInt32(hdnpkey_cust_po_master.Value == "" ? "-2" : hdnpkey_cust_po_master.Value);
                row.fkey_user = Identity.UserPK;
                row.fkey_invoice = Convert.ToInt32(hdnInvoiceKey.Value == "" ? "-1" : hdnInvoiceKey.Value);
                row.is_automatic = 0;

                model.CustomerReceipts.AddCustomerReceiptsRow(row);

                if (receipt_type == 1)
                {
                    foreach (String key in Request.Form.AllKeys)
                    {
                        if (key.StartsWith("txtPayAmount"))
                        {
                            string invoiceKeystr = key.Trim();
                            int startIndex = "txtPayAmount".Length;
                            invoiceKeystr = invoiceKeystr.Substring(startIndex);
                            int invoiceKey = -1;
                            try
                            {
                                invoiceKey = Convert.ToInt32(invoiceKeystr);
                            }
                            catch (Exception) { }
                            double amount = Convert.ToDouble(txtReceiptAmount.Text == "" ? "-1.00" : txtReceiptAmount.Text);
                            if (amount > 0.00 && invoiceKey > 0)
                            {
                                receiptrow.amount = Convert.ToDecimal(amount);
                                receiptrow.fkey_invoice = Convert.ToInt32(hdnInvoiceKey.Value == "" ? "-1" : hdnInvoiceKey.Value);
                                receiptrow.fkey_cust_manual_receipt = -2;
                                receiptrow.fkey_user = Identity.UserPK;
                                model.ReceiptDetails.AddReceiptDetailsRow(receiptrow);
                            }
                        }
                    }
                }
                else if (receipt_type == 0)
                {
                    if (Convert.ToDouble(row["receipt_amount"]) > 0.00)
                    {
                        receiptrow.amount = Convert.ToDecimal(row["receipt_amount"]);
                        receiptrow.fkey_invoice = -3;
                        receiptrow.fkey_cust_manual_receipt = -2;
                        receiptrow.fkey_user = Identity.UserPK;
                        model.ReceiptDetails.AddReceiptDetailsRow(receiptrow);
                    }
                }

                int Key = CustomerReceiptsService.Update(model, ConnString);
                Response.Redirect("CustomerReceipts.aspx");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void rbtnAdvance_CheckedChanged(object sender, EventArgs e)
        {
            BindPendingInvoceList();
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("CustomerReceipts.aspx");
        }
    }
}