﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using System.Collections.Specialized;
using System.Text;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.Account
{
    public partial class CustomerList : PageBase
    {
        protected string searchstring = "-1";
        protected int pkey_customer = -1;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCustomerList();
            }
        }

        private void BindCustomerList()
        {
            try
            {
                CustomerModel.CustomersDataTable customers = UserService.GetCustomerListBysearch(pkey_customer, searchstring, ConnString);
                CustomerlistGrid.DataSource = customers;
                CustomerlistGrid.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected int GetRecordCountForPage()
        {
            return GetRecordCountForPageSize(CustomerlistGrid.PageSize, CustomerlistGrid.PageIndex + 1);
        }

        protected void CustomerlistGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CustomerlistGrid.PageIndex = e.NewPageIndex;
            searchstring = GetSearchCondition();
            BindCustomerList();
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            try
            {
                searchstring = GetSearchCondition();
                BindCustomerList();
                btnReset.Visible = true;
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (GridViewRow item in CustomerlistGrid.Rows)
                {

                    HiddenField hdnpkey_customer = (HiddenField)item.FindControl("hdnpkey_customer");
                    HiddenField hdncustomer_id = (HiddenField)item.FindControl("hdncustomer_id");
                    HiddenField hdncus_firstname = (HiddenField)item.FindControl("hdncus_firstname");
                    HiddenField hdncus_lastname = (HiddenField)item.FindControl("hdncus_lastname");
                    HiddenField hdnaddress1 = (HiddenField)item.FindControl("hdnaddress1");
                    HiddenField hdnaddress2 = (HiddenField)item.FindControl("hdnaddress2");
                    HiddenField hdncity = (HiddenField)item.FindControl("hdncity");
                    HiddenField hdnstate = (HiddenField)item.FindControl("hdnstate");
                    HiddenField hdnpostalcode = (HiddenField)item.FindControl("hdnpostalcode");
                    string cus_name = hdncus_firstname.Value + " " + hdncus_lastname.Value;

                    CheckBox chkbox = (CheckBox)item.FindControl("chkSelect");
                    if (chkbox.Checked == true)
                    {
                        string script = string.Format("SetDataAndClose('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}');", hdnpkey_customer.Value, hdncustomer_id.Value,
                                                                                                                            cus_name, hdnaddress1.Value,
                                                                                                                            hdnaddress2.Value, hdncity.Value,
                                                                                                                            hdnstate.Value, hdnpostalcode.Value);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "jscript", script, true);
                    }
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            btnReset.Visible = false;
            ddlOperator.SelectedIndex = 0;
            ddlFilterFor.SelectedIndex = 0;
            txtPattern.Text = "";
            BindCustomerList();
        }

        public String GetSearchCondition()
        {
            //Variable holding the current session object
            System.Web.SessionState.HttpSessionState currsess = System.Web.HttpContext.Current.Session;

            //Variable holding the parameters - values from the request in a named value collection
            NameValueCollection requestCollections = System.Web.HttpContext.Current.Request.Params;

            //Get the Critera , pattern, operator from the request
            String criteria = ddlFilterFor.SelectedValue;
            String roperator = ddlOperator.SelectedValue;
            String pattern = txtPattern.Text.Trim();

            //Check if pattern exists
            if ("".Equals(pattern)) return "-1";

            //Create a new instance of named value collection
            NameValueCollection nvc = new NameValueCollection();

            //Add the Critera , pattern, operator to the nvc
            nvc.Add("criteria", criteria);
            nvc.Add("operator", roperator);
            nvc.Add("pattern", pattern);
            currsess["combovalues"] = nvc;

            String searchfor = FormSearchString(nvc);
            currsess["search_condition"] = searchfor;
            currsess["pattern_value"] = pattern;

            return searchfor;
        }

        private String FormSearchString(NameValueCollection nvc)
        {
            String sOperator = nvc["operator"];
            sOperator = sOperator != null ? sOperator.Trim() : sOperator;
            string pattern = nvc["pattern"];
            StringBuilder sb = new StringBuilder();


            if (sOperator.IndexOf("LIKE") < 0)
            {
                pattern = pattern.Replace("'", "''");
                sb.Append(nvc["criteria"]).Append(" ").Append(sOperator).Append(" ").Append("'").Append(pattern).Append("'");
            }
            else if (sOperator != null && (sOperator.ToLower()).IndexOf("in") > 0)
            {
                sb.Append(nvc["criteria"]).Append("  ").Append(sOperator).Append(" (").Append(pattern).Append(")");
            }
            else
            {
                sOperator = sOperator.Replace("LIKE", pattern);
                sOperator = sOperator.Replace("'", "''");
                sb.Append(nvc["criteria"]).Append(" LIKE '").Append(sOperator).Append("'");
            }
            return sb.ToString();
        }
    }
}