﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomerList.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Account.CustomerList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Best Print Buy</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" media="all" href="../css/bestprintbuy_style.css" />
    <link rel="stylesheet" media="all" href="../css/jquery.fancybox.css" />
    <link rel="stylesheet" media="all" href="../css/Style.css" />
    <script src="../Scripts/jquery.js"></script>
    <script src="../Scripts/common.js"></script>
    <script src="../Scripts/ddsmoothmenu.js"></script>
    <script src="../Scripts/plugin/fancybox/jquery.fancybox.js"></script>
    <!--[if lt IE 9]>
			<script src="../Scripts/html5.js"></script>
		<![endif]-->
    <!--[if lte IE 7]> <html class="ie7"> <![endif]-->
    <!--[if IE 8]>     <html class="ie8"> <![endif]-->
    <!--[if IE 9]>     <html class="ie9"> <![endif]-->
    <!--[if IE 10]>     <html class="ie10"> <![endif]-->
    <!--[if !IE]><!-->
    <!--<![endif]-->
    <script type="text/javascript">
        function customerSelect(obj) {
            var grid = obj.parentNode.parentNode.parentNode.parentNode;
            var inputs = grid.getElementsByTagName("input");
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].type == "checkbox") {
                    if (obj.checked && inputs[i] != obj && inputs[i].checked) {
                        inputs[i].checked = false;
                    }
                }
            }
        }

        function SetDataAndClose(pkey_customer, customer_id, cus_name, address1, address2, city, state, postalcode) {
            window.top.SetCustomerData(pkey_customer, customer_id, cus_name, address1, address2, city, state, postalcode);
            return false;
        }

        function checkSelect() {
            var ischecked = false;
            $('.select input:checkbox').each(function () {
                if (this.checked) {
                    ischecked = true;
                    return true;
                }
            });

            if (!ischecked) {
                alert("Please select atleast one customer.");
                return false;
            }
            return true;
        }
    </script>
</head>
<body id="Body" runat="server">
    <div id="Div1" class="popup-wrapper" runat="server">
        <div class="popup-title">
            Customers List
        </div>
        <!-- Header Ends -->
        <div class="popup-contnt">
            <!-- Menu starts -->
            <form id="Form1" runat="server">
                <table class="accounts-table">
                    <tbody>
                        <tr>
                            <td width="15%">Filter List for </td>
                            <td width="15%">
                                <asp:DropDownList name="ddlFilterFor" ID="ddlFilterFor" runat="server" CssClass="slect-box" Width="100%">
                                    <asp:ListItem Value="pps_customers.email_id">Email Address</asp:ListItem>
                                    <asp:ListItem Value="pps_customers.first_name">First Name</asp:ListItem>
                                    <asp:ListItem Value="pps_customers.last_name">Last Name</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td width="1%"></td>
                            <td width="15%">
                                <asp:DropDownList name="ddlOperator" ID="ddlOperator" runat="server" CssClass="slect-box" Width="100%">
                                    <asp:ListItem Value="=">Equals</asp:ListItem>
                                    <asp:ListItem Value="<>">Not Equals</asp:ListItem>
                                    <asp:ListItem Value="LIKE%">Starts With</asp:ListItem>
                                    <asp:ListItem Value="%LIKE">Ends with</asp:ListItem>
                                    <asp:ListItem Value="%LIKE%">Contains</asp:ListItem>
                                </asp:DropDownList></td>
                            <td width="1%"></td>
                            <td width="15%">
                                <asp:TextBox name="txtPattern" CssClass="text" ID="txtPattern" runat="server" Width="100%"></asp:TextBox>
                            </td>
                            <td width="3%"></td>
                            <td width="5%">
                                <asp:Button runat="server" ID="btnGo" CssClass="button rounded" Text="GO" OnClick="btnGo_Click"></asp:Button>
                            </td>
                            <td width="1%"></td>
                            <td width="5%">
                                <asp:Button runat="server" ID="btnReset" CssClass="button rounded" Text="Reset" OnClick="btnReset_Click" Visible="false"></asp:Button>
                            </td>
                            <td width="35%"></td>
                        </tr>
                    </tbody>
                </table>            
            <div class="clr"></div>
                <asp:GridView ID="CustomerlistGrid" runat="server" AutoGenerateColumns="false" EnableViewState="true"
                    ForeColor="Black" Width="100%" PageSize="20" AllowPaging="true"
                    class="tablep" name="CustomerlistGrid" OnPageIndexChanging="CustomerlistGrid_PageIndexChanging">
                    <PagerStyle HorizontalAlign="Right" CssClass="dtPage"></PagerStyle>
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />
                    <Columns>
                        <asp:TemplateField HeaderText="#SL">
                            <HeaderStyle Width="6%" ForeColor="White"></HeaderStyle>
                            <ItemTemplate>
                                <span><%# GetRecordCountForPage()%></span>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Email Address">
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container,"DataItem.customer_id") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="First Name">
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container,"DataItem.cus_firstname") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Last Name">
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container,"DataItem.cus_lastname") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Address">
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container,"DataItem.address1") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="City">
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container,"DataItem.city") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="State">
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container,"DataItem.state") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Postal Code">
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container,"DataItem.postalcode") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Select">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server" onclick="customerSelect(this);" CssClass="select"/>
                                <asp:HiddenField ID="hdnpkey_customer" runat="server" Value='<%#Eval("pkey_customer") %>' />
                                <asp:HiddenField ID="hdncustomer_id" runat="server" Value='<%#Eval("customer_id") %>' />
                                <asp:HiddenField ID="hdncus_firstname" runat="server" Value='<%#Eval("cus_firstname") %>' />
                                <asp:HiddenField ID="hdncus_lastname" runat="server" Value='<%#Eval("cus_lastname") %>' />
                                <asp:HiddenField ID="hdnaddress1" runat="server" Value='<%#Eval("address1") %>' />
                                <asp:HiddenField ID="hdnaddress2" runat="server" Value='<%#Eval("address2") %>' />
                                <asp:HiddenField ID="hdncity" runat="server" Value='<%#Eval("city") %>' />
                                <asp:HiddenField ID="hdnstate" runat="server" Value='<%#Eval("state") %>' />
                                <asp:HiddenField ID="hdnpostalcode" runat="server" Value='<%#Eval("postalcode") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>

                <table>
                    <tbody>
                        <tr>
                            <td align="right">
                                <asp:Button runat="server" ID="btnSelect" CssClass="button rounded" Text="Select" OnClientClick="if(!checkSelect()) return false;" OnClick="btnSelect_Click"></asp:Button>
                                <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClientClick="parent.$.fancybox.close(); return false;"></asp:Button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
</body>
</html>
