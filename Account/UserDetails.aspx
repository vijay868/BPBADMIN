﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="UserDetails.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Account.UserDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        $().ready(function () {
            $("#frmUserDetails").validate();
        });

        function moveProductOptions(option, moveAll) {
            fromObject = document.getElementById("ContentPlaceHolder1_lbAvailableGroups");
            toObject = document.getElementById("ContentPlaceHolder1_lbAssignedGroups");

            if (option == "right") {
                from = eval(fromObject);
                to = eval(toObject);
            }
            else {
                from = eval(toObject);
                to = eval(fromObject);
            }

            if (from && to) {
                var fromSize = from.options.length;
                var toSize = to.options.length;
                var fromArray = new Array();
                var fromSelectedArray = new Array();
                var fromSelectedIndex = 0;
                var fromIndex = 0;
                for (i = fromSize - 1; i > -1; i--) {
                    var isSelected = false
                    if (from.options[i].selected)
                        isSelected = from.options[i].selected;
                    if ((isSelected && isSelected == true) || moveAll == true) {
                        fromSelectedArray[fromSelectedIndex] = new Array(from.options[i].value, from.options[i].text);
                        from.options[i] = null;
                        fromSelectedIndex++;
                    }
                    else {
                        fromArray[fromIndex] = new Array(from.options[i].value, from.options[i].text);
                        from.options[i] = null;
                        fromIndex++;
                    }
                }
                fromSize = fromArray.length;
                for (i = 0; i < fromSize; i++) {
                    from.options[i] = new Option(fromArray[fromSize - 1 - i][1], fromArray[fromSize - 1 - i][0]);
                }
                var newToSize = fromSelectedArray.length;
                var strData = "";
                for (i = 0; i < newToSize; i++) {
                    toSize = to.options.length;
                    to.options[toSize] = new Option(fromSelectedArray[i][1], fromSelectedArray[i][0]);
                    strData = strData + to.options[toSize].value;
                    if (i != newToSize - 1) strData = strData + ",";
                }
            }
        }

        function selectMemberRoles() {
            if (document.getElementById('ContentPlaceHolder1_lbAssignedGroups')) {
                var leng = document.getElementById('ContentPlaceHolder1_lbAssignedGroups').options.length;
                for (i = leng - 1; i > -1; i--) {
                    document.getElementById('ContentPlaceHolder1_lbAssignedGroups').options[i].selected = true;
                }
            }
        }

        function SetRequirePassword() {
            if ('<%=hdnUserId.Value%>' != -2) {
                $('#ContentPlaceHolder1_txtConfirmPassword').removeAttr('required');
                $('#ContentPlaceHolder1_txtPassword').removeAttr('required');
            }
        }

        function saveUser() {
            if ('<%=HasAccess("UserList", hdnUserId.Value != "-2" ? "edit" : "add")%>' == 'False') {
                alert('User does not have access.');
                return false;
            }
            else {
                selectMemberRoles();
                function validateCheckBox() {
                    if ($("#ContentPlaceHolder1_chkIsartist").is(':checked')) {
                        if ($("#ContentPlaceHolder1_ddlSelectArtist").val() == "-1") {
                            alert("Please select Artist and Try");
                            return false;
                        }
                    }
                    return true;
                }
            }
        }

        function ShowSelectPopup() {
            if ($("#ContentPlaceHolder1_ddlUsertype").val() == "" || $("#ContentPlaceHolder1_ddlUsertype").val() == "-1") {
                alert("Please select the user type and try.");
                return false;
            }
            else {
                //var pkey = "pkey=" + frm.txtFkeyRegister.value;
                url = "../BusinessEntites/SelectBusinessEntity.aspx?bid=usr&cboType=" + $("#ContentPlaceHolder1_ddlUsertype").val();
                $.fancybox({
                    'width': '70%',
                    'autoScale': true,
                    'transitionIn': 'fade',
                    'transitionOut': 'fade',
                    'href': url,
                    'type': 'iframe'
                });
                return false;
            }
        }

        function ShowRegDetailsPopup() {
            url = "../BusinessEntites/BusinessRegisterPopUp.aspx?bid=usr&pkey=" + $("#ContentPlaceHolder1_txtFkeyRegister").val();
            $.fancybox({
                'width': '60%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': url,
                'type': 'iframe'
            });
            return false;
        }

        function SetSupplierData(parentID, Lastname) {
            $("#ContentPlaceHolder1_txtLastName").val(Lastname);
            $("#ContentPlaceHolder1_txtFkeyRegister").val(parentID);
            __doPostBack("ctl00$ContentPlaceHolder1$lbtnSupplier", "");
        }

        function ShowUserAccessListPopup() {
            url = "UserAccessList.aspx?userkey=" + $("#ContentPlaceHolder1_hdnUserId").val();
            $.fancybox({
                'width': '70%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': url,
                'type': 'iframe'
            });
            return false;
        }


        function ShowAssignedGroupsPopup() {
            url = "ShowAssignedGroups.aspx?userkey=" + $("#ContentPlaceHolder1_hdnUserId").val();
            $.fancybox({
                'width': '70%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': url,
                'type': 'iframe'
            });
            return false;
        }
        </script>
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Security</li>
            <li><a href="UserList.aspx">User List</a></li>
            <li>User Details</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>User Details</h2>
        <form id="frmUserDetails" name="frmUserDetails" runat="server">
            <table width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="7%">User Type </td>
                        <td width="20%">
                            <asp:DropDownList name="ddlUsertype" ID="ddlUsertype" CssClass="slect-box" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td width="15%">
                            <asp:Button runat="server" ID="btnSelecrUser" CssClass="button rounded" Text="Select User" Enabled="false" UseSubmitBehavior="false" OnClientClick="return ShowSelectPopup();"></asp:Button>
                            <asp:LinkButton ID="lbtnSelecrUser" runat="server" OnClick="lbtnSelecrUser_Click" />
                        </td>
                        <td width="5%">
                            <asp:Button runat="server" ID="btnRegDetails" CssClass="button rounded" Text="Reg. Details" Visible="false" UseSubmitBehavior="false" OnClientClick="return ShowRegDetailsPopup();"></asp:Button>
                        </td>
                        <td width="5%">
                            <asp:Button runat="server" CssClass="button rounded" ID="btnUserAccessList" Text="User Access List" OnClientClick="return ShowUserAccessListPopup();" Enabled="false" UseSubmitBehavior="false"></asp:Button>
                        </td>
                        <td width="25%"></td>
                    </tr>
                </tbody>
            </table>
            <table cellpadding="0" cellspacing="0" border="0" width="100%" class="accounts-table">
                <thead>
                    <tr>
                        <th colspan="4" width="100%">Customer Information</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="15%">
                            <label>First Name*</label></td>
                        <td width="20%">
                            <asp:TextBox CssClass="text-box" name="txtFirstName" ID="txtFirstName" runat="server" required></asp:TextBox>
                        </td>
                        <td width="15%">
                            <label>Login ID* </label>
                        </td>
                        <td width="35%">
                            <asp:TextBox CssClass="text-box" name="txtLoginID" ID="txtLoginID" runat="server" required></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Last Name*</label></td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtLastName" ID="txtLastName" runat="server" required></asp:TextBox>
                        </td>
                        <td>
                            <label>Password* </label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtPassword" ID="txtPassword" runat="server" TextMode="Password" required></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">
                            <label>Comments</label></td>
                        <td>
                            <asp:TextBox style="resize:none;" CssClass="text-box" name="txtComments" ID="txtComments" runat="server" Width="295px" Rows="6" TextMode="MultiLine"></asp:TextBox>
                        </td>
                        <td style="vertical-align: top;">
                            <label>Confirm Password* </label>
                        </td>
                        <td style="vertical-align: top;">
                            <asp:TextBox CssClass="text-box" name="txtConfirmPassword" ID="txtConfirmPassword" runat="server" TextMode="Password" equalTo="#ContentPlaceHolder1_txtPassword" required></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <asp:CheckBox ID="chkUsrCngPwd" CssClass="check-box" runat="server" /><asp:Label ID="Label5" runat="server" Text="   User can change password"></asp:Label>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <asp:CheckBox ID="chkActive" CssClass="check-box" runat="server" /><asp:Label ID="Label1" runat="server" Text="   Active"></asp:Label>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
            <table width="100%" class="accounts-table">
                <thead>
                    <tr>
                        <th colspan="4" width="100%">Access Rights Information</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="15%">
                            <label></label>
                        </td>
                        <td width="50%">
                            <div style="width: 40%; float: left">
                                <label style="padding-left: 0px;"><b>Available Groups</b></label>
                                <asp:ListBox ID="lbAvailableGroups" CssClass="list-box" runat="server" SelectionMode="Multiple"></asp:ListBox>
                            </div>
                            <div style="width: 10%; float: left; padding: 40px 10px 0 10px;">
                                <button id="to2" type="button" class="button rounded width53" onclick="javascript:moveProductOptions('right',false)">
                                    &nbsp;&gt;&nbsp;</button><br />
                                <button id="allTo2" type="button" class="button rounded width53" onclick="javascript:moveProductOptions('right',true)">
                                    &nbsp;&gt;&gt;&nbsp;</button><br />
                                <button id="to1" type="button" class="button rounded width53" onclick="javascript:moveProductOptions('left',false)">
                                    &nbsp;&lt;&nbsp;</button><br />
                                <button id="allTo1" type="button" class="button rounded width53" onclick="javascript:moveProductOptions('left',true)">
                                    &nbsp;&lt;&lt;&nbsp;</button>
                            </div>
                            <div style="width: 35%; float: left; padding-right: 10px;">
                                <label style="padding-left: 0px;"><b>Member of</b></label>
                                <asp:ListBox ID="lbAssignedGroups" runat="server" CssClass="list-box" SelectionMode="Multiple"></asp:ListBox>
                            </div>
                        </td>
                        <td width="35%"></td>
                    </tr>
                </tbody>
            </table>
            <table width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="15%">
                            <label></label>
                        </td>
                        <td width="20%">
                            <asp:CheckBox ID="chkIsartist" CssClass="check-box" runat="server" /><asp:Label ID="Label2" runat="server" Text="   Is Artist"></asp:Label>
                        </td>
                        <td width="60%">
                            <div style="width:60%">
                            <label style="padding:0px 0px 0px 0px;width:75px">Select Artist</label>
                            <asp:DropDownList name="ddlSelectArtist" ID="ddlSelectArtist" CssClass="slect-box" runat="server" Width="54%">
                            </asp:DropDownList>
                                </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label></label>
                        </td>
                        <td>
                            <asp:CheckBox ID="chkisWHProd" CssClass="check-box" runat="server" /><asp:Label ID="Label11" runat="server" Text="   Is WareHouse Product"></asp:Label>
                        </td>
                        <td>
                            <label></label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label></label>
                        </td>
                        <td>
                            <asp:CheckBox ID="chkIsSalesMkt" CssClass="check-box" runat="server" /><asp:Label ID="Label3" runat="server" Text="   Is Sales & Marketing"></asp:Label>
                        </td>
                        <td>
                            <label></label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label></label>
                        </td>
                        <td>
                            <asp:CheckBox ID="chkCanCumminicate" CssClass="check-box" runat="server" /><asp:Label ID="Label4" runat="server" Text="   Can communicate with customer"></asp:Label>
                        </td>
                        <td>
                            <label></label>
                        </td>
                    </tr>
                </tbody>
            </table>
         <%--   <table width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="15%" class="lft">
                            <asp:Button runat="server" ID="btnAssignedGroups" Text="Assigned Groups" CssClass="button rounded" OnClientClick="return ShowAssignedGroupsPopup();" Visible="false" UseSubmitBehavior="false"></asp:Button>
                            <asp:HiddenField ID="txtFkeyRegister" runat="server" />
                            <asp:HiddenField ID="hdnUserId" runat="server" />
                        </td>
                          <td width="17%">&nbsp;</td>
                        <td width="15%">&nbsp;</td>
                        <td width="35%" style="float:right; padding-right:15%">
                            <asp:Button runat="server" ID="btnSave" Text="Save" OnClick="btnSave_Click" CssClass="button rounded" OnClientClick="return saveUser();"></asp:Button>
                            <asp:Button runat="server" ID="btnClose" Text="Close" OnClick="btnClose_Click" CssClass="button rounded" UseSubmitBehavior="false"></asp:Button>
                        </td>
                    </tr>
                </tbody>
            </table>--%>
                 <div class="btn_lst">
                     <div class="lft">
                            <asp:Button runat="server" ID="btnAssignedGroups" Text="Assigned Groups" CssClass="button rounded" OnClientClick="return ShowAssignedGroupsPopup();" Visible="false" UseSubmitBehavior="false"></asp:Button>
                            <asp:HiddenField ID="txtFkeyRegister" runat="server" />
                            <asp:HiddenField ID="hdnUserId" runat="server" />

                     </div>
                <div class="rgt" style="padding-right:7%">
                    <asp:Button runat="server" ID="btnClose" Text="Close" OnClick="btnClose_Click" CssClass="button rounded" UseSubmitBehavior="false"></asp:Button>
                    <asp:Button runat="server" ID="btnSave" Text="Save" OnClick="btnSave_Click" CssClass="button rounded" OnClientClick="return saveUser();"></asp:Button>
                    
                </div>
            </div>
        </form>
        
        <script>
            SetRequirePassword();
        </script>
    </div>
</asp:Content>
