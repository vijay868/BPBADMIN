﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using System.Text;

namespace Bestprintbuy.Admin.Web.Account
{
    public partial class UserList : PageBase
    {
        protected int serialNo;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadData();
            }
        }

        private void LoadData()
        {
            try
            {
                int key = -2;
                UserModel.UserListDataTable UserList = UserService.GetUserList(key, ConnString);
                if (UserlistGrid.Attributes["SortExpression"] != null)
                    UserList.DefaultView.Sort = UserlistGrid.Attributes["SortExpression"] + " " + UserlistGrid.Attributes["SortDirection"];
                lblTotalRecords.Text = string.Format(" Total Number of Users : {0}", UserList.Rows.Count);
                UserlistGrid.DataSource = UserList.DefaultView;
                serialNo = UserlistGrid.PageIndex * UserlistGrid.PageSize;
                UserlistGrid.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnAddNewUser_Click(object sender, EventArgs e)
        {
            Response.Redirect("UserDetails.aspx?userID=-2");
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("DashBoard.aspx");
        }

        protected void UserlistGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            UserlistGrid.PageIndex = e.NewPageIndex;
            LoadData();
        }

        protected void UserlistGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            SortGridView(UserlistGrid, e);
            UserlistGrid.PageIndex = 0;
            LoadData();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbtn = (LinkButton)sender;
                int userID = Convert.ToInt32(lbtn.CommandArgument);
                UserService.DeleteUser(userID, ConnString);
                LoadData();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void UserlistGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Cells[0].Text = Convert.ToString(++serialNo);
            }
        }
    }
}