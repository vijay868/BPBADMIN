﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="CustomerReceiptsDetail.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Account.CustomerReceiptsDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">

        function CustomerList() {
            url = "CustomerList.aspx";
            $.fancybox({
                'width': '70%',
                'height': '100%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': url,
                'type': 'iframe'
            });
            return false;
        }


        function SetCustomerData(pkey_customer, customer_id, cus_name, address1, address2, city, state, postalcode) {
            $("#ContentPlaceHolder1_hdnpkey_customer").val(pkey_customer);
            $("#ContentPlaceHolder1_hdncustomer_id").val(customer_id);
            $("#ContentPlaceHolder1_hdncus_name").val(cus_name);
            $("#ContentPlaceHolder1_hdnaddress1").val(address1);
            $("#ContentPlaceHolder1_hdnaddress2").val(address2);
            $("#ContentPlaceHolder1_hdncity").val(city);
            $("#ContentPlaceHolder1_hdnstate").val(state);
            $("#ContentPlaceHolder1_hdnpostalcode").val(postalcode);

            $("#ContentPlaceHolder1_lblCustEmail").text(customer_id);
            $("#ContentPlaceHolder1_lblCustName").text(cus_name);
            __doPostBack("ctl00$ContentPlaceHolder1$lbtnUpdate", "");
        }

        function CustomerPreview() {
            if ($("#ContentPlaceHolder1_lblCustomerId").text() == "") {
                alert("Please select the Customer.");
                return false;
            }
            $.fancybox({
                'width': '100%',
                'height': '100%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': '#divCustomerPreview',
                'type': 'inline'
            });
            return false;
        }

        var totalBalance = 0.00;
        var adjAmount = 0.00;

        function validateCustomer() {
            var sOption = '<%=is_advance%>';
            var key = '<%=hdnpkey_customer.Value%>';
            if (key == '') {
                alert("Customer is required, Please select a value and try again.")
                return false;
            }
            if (sOption == "1" && !checkInvoiceAmount()) return false;
            else if (sOption == "2" && !checkOnAccountAmount()) return false;
            return true;
        }

        function checkOnAccountAmount() {
            var outstandingInvoices = 0.00;
            var totalBalance = 0.00;
            var adjAmount = 0.00;
            var totalOutStands = $("#ContentPlaceHolder1_hdntotalOutStands").val();
            var outstandingInvoices = 0.00;
            if (totalOutStands && !isNaN(totalOutStands))
                outstandingInvoices = parseFloat(totalOutStands);
            var pmttempAmount = '';
            if (eval($("#ContentPlaceHolder1_txtReceiptAmount").val()))
                pmttempAmount = eval($("#ContentPlaceHolder1_txtReceiptAmount").val());
            var pmtAmount = 0.00;

            if (!isNaN(pmttempAmount)) {
                pmtAmount = parseFloat(pmttempAmount);
            }
            if (pmtAmount != outstandingInvoices) {
                alert("Amount should be exactly equal to the entered receipt amount.");
                return false;
            }
            return true;
        }

        function checkInvoiceAmount() {
            var pmttempAmount = '';
            if (eval($("#ContentPlaceHolder1_txtReceiptAmount").val()))
                pmttempAmount = eval($("#ContentPlaceHolder1_txtReceiptAmount").val());
            var pmtAmount = 0.00;

            if (!isNaN(pmttempAmount)) {
                pmtAmount = parseFloat(pmttempAmount);
            }
            if ((pmtAmount - totalBalance) > 0.01) {
                alert("Amount should be exactly equal to the entered receipt amount.");
                return false;
            }
            else if ((pmtAmount - totalBalance) < 0.00) {
                alert("Amount should be exactly equal to the entered receipt amount.");
                return false;
            }
            return true;
        }

        function UpdateTotalBalance(amount) {
            var textName = amount.name;
            var payAmount = 0.00;
            var tempAmount = amount.value;
            try {
                tempAmount = tempAmount.trim();
            } catch (eee) { }
            if (tempAmount && '' != tempAmount && !isNaN(tempAmount)) {
                payAmount = parseFloat(tempAmount);
            }

            var splitArray = textName.split("txtPayAmount")
            var sizeArray = splitArray.length;
            var invoiceMaster = 0;
            var balanceAmt = 0.00;
            if (sizeArray > 0)
                invoiceMaster = splitArray[sizeArray - 1];
            if (invoiceMaster && '' != invoiceMaster && !isNaN(invoiceMaster))
                invoiceMaster = parseInt(invoiceMaster);


            if (invoiceMaster > 0) {
                var tempBalAmount = $(".classBalanceAmount" + invoiceMaster).val();

                if (!isNaN(tempBalAmount))
                    balanceAmt = parseFloat(tempBalAmount);

                if (payAmount != 0.00 && payAmount > balanceAmt) {
                    alert("Entered amount in pay amount column is more than the Invoce Balance.");
                    if (adjAmount == 0 || adjAmount == 0.00)
                        amount.value = '';
                    else
                        amount.value = adjAmount;
                    amount.focus();
                }
                else if (payAmount != 0.00) {
                    var tempTotalBalance = totalBalance;
                    totalBalance = totalBalance - (adjAmount - payAmount);
                    adjAmount = 0.00;
                }
                else if (payAmount == 0.00 || payAmount == 0) {
                    totalBalance = totalBalance - adjAmount;
                    if (totalBalance < 0.01) totalBalance = 0.00;
                    adjAmount = 0.00;

                }
            }
            else {
                alert("invoice key lost");
            }
            try {
                amount.value = amount.value.trim();
            }
            catch (eee) { }
            amount.value = Math.round(amount.value, 2);

        }

        function UpdateAdjAmount(amount) {
            if (amount) {
                var tempAmount = amount.value;
                try {
                    tempAmount = tempAmount.trim();
                }
                catch (eee) { }

                if (tempAmount && '' != tempAmount && !isNaN(tempAmount))
                    adjAmount = parseFloat(tempAmount);
            }
            if (amount.value == '0.00') this.select();
        }

        $(function () {
            $("#ContentPlaceHolder1_txtReceiptDate").datepicker();
            $("#frmReceiptDetail").validate();
        });
    </script>
    <style type="text/css">
        #divCustomerPreview {
            overflow: hidden;
        }
    </style>

    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Accounts</li>
            <li><a href="CustomerReceipts.aspx">Customer Receipts</a></li>
            <li>Manual Receipt</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Manual Receipt</h2>
        <form class="" runat="server" id="frmReceiptDetail">

            <table class="accounts-table" width="100%">
                <tbody>
                    <tr>
                        <td width="8%"></td>
                        <td width="15%">
                            <asp:Button ID="btnCustomer" Text="Customer" CssClass="button rounded" runat="server" UseSubmitBehavior="false" OnClientClick="return CustomerPreview();" />
                            <asp:LinkButton ID="lbtnUpdate" runat="server" OnClick="lbtnUpdate_Click" />
                            <asp:Button ID="btnCustomerDetail" Text="..." CssClass="button rounded" Font-Bold="true" runat="server" UseSubmitBehavior="false" OnClientClick="return CustomerList();" />
                        </td>

                        <td>
                            <asp:Label ID="lblCustEmail" runat="server" /><br />
                            <asp:Label ID="lblCustName" runat="server" />
                        </td>

                    </tr>

                </tbody>
            </table>

            <table class="accounts-table" width="100%">
                <tbody>
                    <tr>
                        <td width="15%">
                            <label>Receipt Number*</label></td>
                        <td width="20%">
                            <asp:Label runat="server" ID="lblReceiptNumber" /></td>
                        <td class="a_cnt" colspan="3" width="40%">&nbsp;&nbsp;&nbsp;&nbsp;<asp:RadioButton runat="server" ID="rbtnAdvance" CssClass="check-box" GroupName="advance" OnCheckedChanged="rbtnAdvance_CheckedChanged" AutoPostBack="true" />Advance
                        &nbsp;&nbsp;&nbsp;&nbsp;<asp:RadioButton CssClass="check-box" runat="server" ID="rbtnAgainstInvoice" GroupName="advance" Checked="true" OnCheckedChanged="rbtnAdvance_CheckedChanged" AutoPostBack="true" />Against Invoice 
                         &nbsp;&nbsp;&nbsp;&nbsp; 
                            <asp:RadioButton runat="server" ID="rbtnOnAccount" CssClass="check-box" GroupName="advance" OnCheckedChanged="rbtnAdvance_CheckedChanged" AutoPostBack="true" />On Account
                            
                        </td>

                    </tr>
                    <tr>
                        <td width="15%">
                            <label>Receipt Date*</label></td>
                        <td width="20%">
                            <asp:TextBox name="txtReceiptDate" ID="txtReceiptDate" CssClass="text-box date" runat="server" required></asp:TextBox></td>
                        <td width="15%">
                            <label>Instrument Number</label>
                        </td>
                        <td width="35%">
                            <asp:TextBox name="txtInstrumentNumber" ID="txtInstrumentNumber" CssClass="text-box" runat="server">
                            </asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>
                            <label>Mode Of Payment</label></td>
                        <td>
                            <asp:DropDownList name="ddlPaymentMode" ID="ddlPaymentMode" CssClass="slect-box" runat="server">
                                <asp:ListItem Value="CA" Selected="True">Cash</asp:ListItem>
                                <asp:ListItem Value="CH">Check</asp:ListItem>
                                <asp:ListItem Value="FT">Funds Transfer</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <label>Receipt Amount($)*</label>
                        </td>
                        <td>
                            <asp:TextBox name="txtReceiptAmount" CssClass="text-box" ID="txtReceiptAmount" required runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td colspan="4"></td>
                    </tr>
                    <tr>
                        <td width="15%"></td>
                        <div id="divAdvance" runat="server" visible="false">
                            <td colspan="2" style="border: 1px solid #ff6a00">
                                <table width="100%" id="amountstable">
                                    <tbody>
                                        <tr>
                                            <td width="50%">
                                                <label>Total Advance Payments</label>
                                            </td>
                                            <td width="35%">
                                                <asp:Label ID="lblAdvancePatyment" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>Total Invoiced</label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblTotalInvoice" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>Total Paid</label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblTotalPaid" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>Total Outstanding</label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblTotalOutstanding" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </div>
                        <td></td>
                    </tr>
                </tbody>
            </table>
            <div class="box" id="divInvoice" runat="server" style="padding-top: 4%;">
                <div class="header">
                    <h2 style="padding-top: 13%; padding-right: 70%">Pending Invoices / Orders</h2>
                </div>
                <div class="content">
                    <div class="clr"></div>
                    <table class="table" width="100%" style="padding-bottom: 100px">
                        <thead>
                            <tr>
                                <th width="1%"></th>
                                <th width="25%">Invoice # / Date</th>
                                <th width="25%">Order Number / Date</th>
                                <th width="10%">Amount</th>
                                <th width="10%">Balance</th>
                                <th width="10%">Paid</th>
                                <th>Pay</th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater ID="rptrInvoiceList" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <%#Eval("invoice_number") %><br />
                                            <%#Eval("invoice_date") %>
                                        </td>
                                        <td>
                                            <%#Eval("po_number") %><br />
                                            <%#Eval("po_date") %>
                                        </td>
                                        <td>
                                            <%#Eval("invoice_amount") %>
                                        </td>
                                        <td>
                                            <%#Eval("balance_amount") %>
                                            <input class='classBalanceAmount<%# DataBinder.Eval(Container, "DataItem.pkey_cust_invoice_master") %>' id='txtBalanceAmount<%# DataBinder.Eval(Container, "DataItem.pkey_cust_invoice_master") %>' value='<%# DataBinder.Eval(Container, "DataItem.balance_amount") %>' type="hidden">
                                        </td>
                                        <td>
                                            <%#Eval("paid_amount") %>
                                        </td>
                                        <td>
                                            <input class="textbox" name='txtPayAmount<%# DataBinder.Eval(Container, "DataItem.pkey_cust_invoice_master") %>' type="text" value='' dir="rtl" onblur="UpdateTotalBalance(this);" onfocus="UpdateAdjAmount(this)">$
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="btn_lst">
                <div class="rgt" style="padding-top: 2%">
                    <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" UseSubmitBehavior="false" OnClick="btnClose_Click"></asp:Button>
                    <asp:Button runat="server" ID="btnSave" CssClass="button rounded" Text="Save" OnClick="btnSave_Click" OnClientClick="return validateCustomer();"></asp:Button>

                    <asp:HiddenField ID="hdnpkey_customer" runat="server" />
                    <asp:HiddenField ID="hdnpkey_cust_po_master" runat="server" />
                    <asp:HiddenField ID="hdnInvoiceKey" runat="server" />
                    <asp:HiddenField ID="hdntotalOutStands" runat="server" />
                </div>
            </div>
        </form>

    </div>
    <div id="divCustomerPreview" title="Customer Details" style="display: none;">
        <div id="Div1" class="popup-wrapper" runat="server">
            <div class="popup-title">
                Customer Details
            </div>
            <!-- Header Ends -->
            <div class="popup-contnt">
                <table>
                    <tr>
                        <td width="60%">
                            <label for="lblCustomerId">Customer Id</label>
                        </td>
                        <td>
                            <asp:Label ID="lblCustomerId" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="lblFirstName">First Name</label>
                        </td>
                        <td>
                            <asp:Label ID="lblFirstName" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="lblLastName">Last Name</label>
                        </td>
                        <td>
                            <asp:Label ID="lblLastName" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="lblTitle">Title</label>
                        </td>
                        <td>
                            <asp:Label ID="lblTitle" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="lblCompanyName">Company Name</label>
                        </td>
                        <td>
                            <asp:Label ID="lblCompanyName" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="lblAddress1">Address1</label>
                        </td>
                        <td>
                            <asp:Label ID="lblAddress1" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="lblAddress2">Address2</label>
                        </td>
                        <td>
                            <asp:Label ID="lblAddress2" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="lblCity">City</label>
                        </td>
                        <td>
                            <asp:Label ID="lblCity" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="lblState">State</label>
                        </td>
                        <td>
                            <asp:Label ID="lblState" runat="server"></asp:Label>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="lblZip">Zip</label>
                        </td>
                        <td>
                            <asp:Label ID="lblZip" runat="server"></asp:Label>

                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
