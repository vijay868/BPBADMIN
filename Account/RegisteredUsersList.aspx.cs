﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;

namespace Bestprintbuy.Admin.Web.Account
{

    public partial class RegisteredUsersList : PageBase
    {
        protected string LoggedUsers = "0";
        protected string AnonymousUsers = "0";
        protected string TotalUsers = "0";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadData();
            }
        }

        private void LoadData()
        {
            try
            {
                CustomerStatsModel RegisteredUsersDetails = UserService.GetCustomerStats(ConnString);
                rptrLoggedUserslist.DataSource = RegisteredUsersDetails.LoggedCustomers;
                rptrLoggedUserslist.DataBind();

                LoggedUsers = RegisteredUsersDetails.LoggedCustomers.Rows.Count.ToString();
                //AnonymousUsers = RegisteredUsersDetails.GuestCustomers.Rows.Count.ToString();
                //TotalUsers = Convert.ToString(RegisteredUsersDetails.GuestCustomers.Rows.Count + RegisteredUsersDetails.LoggedCustomers.Rows.Count);

                lblLoggedUsers.Text = RegisteredUsersDetails.LoggedCustomers.Rows.Count.ToString();
                lblAnonymousUsers.Text = RegisteredUsersDetails.GuestCustomers.Rows.Count.ToString();
                lblTotalUsers.Text = Convert.ToString(RegisteredUsersDetails.GuestCustomers.Rows.Count + RegisteredUsersDetails.LoggedCustomers.Rows.Count);
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("DashBoard.aspx");
        }
    }

}