﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.Account
{
    public partial class ACustomerInvoiceView : PageBase
    {
        CustomerInvoicesModel model = new CustomerInvoicesModel();
        protected decimal m_shipPromoDiscount = 0;
        protected decimal m_productPromoDiscount = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCustomerInvoiceView();
            }
        }

        private void BindCustomerInvoiceView()
        {
            try
            {
                int pkeyCustInvoice = Convert.ToInt32(Request.QueryString["pkeyCustInvoice"] != null ? Request.QueryString["pkeyCustInvoice"] : "-1");
                model = CustomerInvoicesService.GetCustomerInvoice(pkeyCustInvoice, ConnString);
                rptrProductList.DataSource = model.InvocieDetails;
                rptrProductList.DataBind();

                CustomerInvoicesModel.InvoicesRow row = (CustomerInvoicesModel.InvoicesRow)model.Invoices.Rows[0];

                if (model.InvocieDetails.Rows.Count > 0)
                {
                    for (int i = 0; i < model.InvocieDetails.Rows.Count; i++)
                    {
                        if (model.InvocieDetails.Rows[i]["promo_shipment_discount"] != null)
                            m_shipPromoDiscount = m_shipPromoDiscount + Convert.ToDecimal(model.InvocieDetails.Rows[i]["promo_shipment_discount"]);
                        lblPromotionalshipment.Text = m_shipPromoDiscount.ToString();

                        if (model.InvocieDetails.Rows[i]["promo_product_discount"] != null)
                            m_productPromoDiscount = m_productPromoDiscount + Convert.ToDecimal(model.InvocieDetails.Rows[i]["promo_product_discount"]);
                        lblPromotionaldiscount.Text = m_productPromoDiscount.ToString();
                    }
                }
                if (model.Invoices.Rows.Count > 0)
                {
                    lblCustEmail.Text = row.Iscustomer_idNull() ? "" : row.customer_id;
                    lblName.Text = row.Iscustomer_nameNull() ? "" : row.customer_name;
                    lblAdd1.Text = row.Isaddress1Null() ? "" : row.address1;
                    lblAdd2.Text = row.Isaddress2Null() ? "" : row.address2;
                    lblSate.Text = (row.IscityNull() ? "" : row.city + ", ") + (row.Isstate_nameNull() ? "" : row.state_name + ", ") + (row.IszipcodeNull() ? "" : row.zipcode);
                    lblSate.Text = lblSate.Text.TrimEnd(' ').TrimEnd(',');

                    lblShippingCost.Text = row.Isadd_on1Null() ? "" : row.add_on1.ToString();
                    lblTotal.Text = row.IstotalNull() ? "" : row.total.ToString();

                    lblinvoicenumber.Text = row.invoice_number;
                    lblInvoiceDate.Text = row.invoice_date.ToShortDateString();
                    lblordernumber.Text = row.Isorder_numberNull() ? "" : row.order_number;
                    lblorderdate.Text = row.Isorder_dateNull() ? "" : row.order_date.ToShortDateString();

                    lblDCNumber.Text = row.dc_number;
                    lblDCDate.Text = row.dc_date.ToShortDateString();
                    lblFreightAgency.Text = row.IsfrieghtNull() ? "" : row.frieght;
                    lblTrackingNumber.Text = row.Istracking_numberNull() ? "" : row.tracking_number;
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected string GetValue(string key, bool bNumeric)
        {
            string returnAmount = "";
            try
            {
                if (model.Invoices.Count > 0)
                {
                    if (model.Invoices.Rows[0][key] != null)
                    {
                        if (bNumeric)
                        {
                            if (model.Invoices.Rows[0][key] != null)
                            {
                                returnAmount = model.Invoices.Rows[0][key].ToString();
                            }
                        }
                        else
                        {
                            returnAmount = model.Invoices.Rows[0][key].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
            return returnAmount;
        }
    }
}