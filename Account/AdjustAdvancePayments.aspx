﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="AdjustAdvancePayments.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Account.AdjustAdvancePayments" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        var totalBalance = 0.00;
        var adjAmount = 0.00;
        function checkInvoice() {
            var sOption = '<%=ipaymentType%>';
            var key = $("#ContentPlaceHolder1_txtSupplier").val();  // window.document.frmStatementofAccount.txtSupplier.value;
            if (key == '') {
                alert("Amount should be exactly equal to the entered payment amount.")
                return false;
            }

            if (sOption == "0" && !checkInvoiceAmount()) return false;
            else if (sOption == "2" && !checkOnAccountAmount()) return false;
            return true;

        }

        function checkOnAccountAmount() {
            return true;
        }

        function checkInvoiceAmount() {
            var pmttempAmount = '';
            if (eval($("#ContentPlaceHolder1_txtPaymentAmount").val()))
                pmttempAmount = $("#ContentPlaceHolder1_txtPaymentAmount").val();
            var pmtAmount = 0.00;

            if (!isNaN(pmttempAmount)) {
                pmtAmount = parseFloat(pmttempAmount);
            }

            if (pmtAmount != totalBalance && (pmtAmount - totalBalance) < 0.01) {
                alert("Amount should be exactly equal to the entered payment amount.");	// selected invoice total amount is more than the available advance amount. please select invoices amounts so that the cumulative will be less than the avaliable advance."
                return false;
            }
            else if (totalBalance <= 0.00) {
                alert("Amount should be exactly equal to the entered payment amount.");
                return false;
            }
            return true;

        }

        function UpdateTotalBalance(amount) {
            var textName = amount.name;
            var payAmount = 0.00;
            var tempAmount = amount.value;
            try {
                tempAmount = tempAmount.trim();
            } catch (eee) {}
            if (tempAmount && '' != tempAmount && !isNaN(tempAmount)) {
                payAmount = parseFloat(tempAmount);
            }
            var splitArray = textName.split("txtPayAmount")
            var sizeArray = splitArray.length;
            var invoiceMaster = 0;
            var balanceAmt = 0.00;
            if (sizeArray > 0)
                invoiceMaster = splitArray[sizeArray - 1];
            if (invoiceMaster && '' != invoiceMaster && !isNaN(invoiceMaster))
                invoiceMaster = parseInt(invoiceMaster);


            if (invoiceMaster > 0) {
                //var textBalance = "document.frmPaymentDetail.txtBalanceAmount" + invoiceMaster;
                //var tempBalAmount = eval(textBalance).value.trim();
                var tempBalAmount = $(".classBalanceAmount" + invoiceMaster).val();
                if (!isNaN(tempBalAmount))
                    balanceAmt = parseFloat(tempBalAmount);

                if (payAmount != 0.00 && payAmount > balanceAmt) {
                    alert("Entered amount in pay amount column is more than the Invoce Balance.");
                    if (adjAmount == 0 || adjAmount == 0.00)
                        amount.value = '';
                    else
                        amount.value = adjAmount;
                    amount.focus();
                }
                else if (payAmount != 0.00) {
                    var tempTotalBalance = totalBalance;
                    totalBalance = totalBalance - (adjAmount - payAmount);
                    adjAmount = 0.00;
                }
                else if (payAmount == 0.00 || payAmount == 0) {
                    totalBalance = totalBalance - adjAmount;
                    if (totalBalance < 0.01) totalBalance = 0.00;
                    adjAmount = 0.00;

                }
            }
            else {
                alert("invoice key lost");
            }
            
            try {
                amount.value = amount.value.trim();
            } catch (e) {

            }
            amount.value = Math.round(amount.value, 2);

        }

        function UpdateAdjAmount(amount) {
            if (amount) {
                var tempAmount = amount.value;
                try {
                    tempAmount = tempAmount.trim();
                } catch (eee) {}
                if (tempAmount && '' != tempAmount && !isNaN(tempAmount))
                    adjAmount = parseFloat(tempAmount);
            }
            if (amount.value == '0.00') this.select();
        }
    </script>
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Accounts</li>
            <li><a href="StatementofAccount.aspx">Statement of Account</a></li>
            <li>Adjust Advance</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Adjust Advance</h2>

        <form id="frmStatementofAccount" name="frmStatementofAccount" runat="server">
            <table width="100%" class="accounts-table">
                <tr>
                    <td width="15%">
                        <label>Supplier Code</label>
                    </td>
                    <td width="20%">
                        <%= GetRowData("supplier_code")%>&nbsp;
                            <% if (SupplierKey != -1)
                               {%>
                        <%= GetPaymentInfo("routing_number") %>&nbsp;
                                <%= GetPaymentInfo("bank_name") %>&nbsp;
                                <%= GetPaymentInfo("account_number") %>
                        <%}%>
                    </td>
                    <td width="15%">
                        <label>Payee Name</label>
                    </td>
                    <td width="35%">
                        <%=GetRowData("contact_name")%>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Advance Amount</label>
                    </td>
                    <td>
                        <%=GetCurrencyData("amount")%>
                        <asp:HiddenField ID="txtPaymentAmount" runat="server" />
                        <asp:HiddenField ID="txtSupplier" runat="server" />
                        <asp:HiddenField ID="txtPaymentInfoKey" runat="server" />
                        <asp:HiddenField ID="fkeyregister" Value='<%# Request.QueryString["fkeyregister"]%>' runat="server" />
                    </td>
                    <td>
                        <label></label>
                    </td>
                    <td>
                        <asp:RadioButton ID="rbtnAgainstInvoice" runat="server" GroupName="adjust" AutoPostBack="true" OnCheckedChanged="rbtnAgainstInvoice_CheckedChanged" /><asp:Label ID="Label1" Text="  Against Invoice" runat="server" />
                        <asp:RadioButton ID="rbtnOnAccount" runat="server" GroupName="adjust" AutoPostBack="true" OnCheckedChanged="rbtnOnAccount_CheckedChanged" /><asp:Label ID="Label2" Text="   On Account" runat="server" />
                    </td>
                </tr>
                <tr>
                    <div id="trAgainstInvoice" runat="server" visible="false">
                        <td colspan="4">
                    <h2>Pending Invoices</h2>
                            <asp:GridView ID="PendingInvoiceGrid" runat="server" AutoGenerateColumns="false" ShowHeader="true" ShowHeaderWhenEmpty="true"
                                ForeColor="Black" Width="100%" PageSize="20" AllowPaging="true" OnPageIndexChanging="PendingInvoiceGrid_PageIndexChanging"
                                class="tabled" name="PendingInvoiceGrid">
                                <PagerStyle HorizontalAlign="Right" CssClass="dtPage"></PagerStyle>
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />
                                <Columns>
                                    <asp:TemplateField HeaderText="#SL">
                                        <HeaderStyle ForeColor="White"></HeaderStyle>
                                        <ItemTemplate>
                                            <span><%# GetRecordCountForPage()%></span>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Invoice#">
                                        <HeaderStyle Width="20%" ForeColor="White" />
                                        <ItemTemplate>
                                            <%#Eval("invoice_number") %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date">
                                        <HeaderStyle Width="15%" ForeColor="White" />
                                        <ItemTemplate>
                                            <%#Eval("invoice_date") %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amount">
                                        <HeaderStyle Width="15%" ForeColor="White" />
                                        <ItemTemplate>
                                            <%#Eval("invoice_amount") %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Balance">
                                        <HeaderStyle Width="15%" ForeColor="White" />
                                        <ItemTemplate>
                                            <%#Eval("balance_amount") %>                                       
                                            <input class='classBalanceAmount<%# DataBinder.Eval(Container, "DataItem.pkey_cust_invoice_master") %>' id='txtBalanceAmount<%# DataBinder.Eval(Container, "DataItem.pkey_br_invoice_master") %>' value ='<%# DataBinder.Eval(Container, "DataItem.balance_amount") %>' type="hidden" >
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Paid">
                                        <HeaderStyle Width="15%" ForeColor="White" />
                                        <ItemTemplate>
                                            <%#Eval("paid_amount") %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Pay">
                                        <HeaderStyle Width="15%" ForeColor="White" />
                                        <ItemTemplate>
                                            <input class="textbox" name='txtPayAmount<%# DataBinder.Eval(Container, "DataItem.pkey_br_invoice_master") %>' type="text" value='<%# DataBinder.Eval(Container, "DataItem.pay_amount") %>' onblur="UpdateTotalBalance(this)" onfocus="UpdateAdjAmount(this)">
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <div class="ttl" runat="server">
                                <b>Total Number of Invoices: </b><%=recordCount%>
                                </br>
                                <b>Totals</b><%= GetInvoiceData("total_amount",true) %>&nbsp;<%= GetInvoiceData("total_balance",true) %>&nbsp;<%= GetInvoiceData("total_paid",true) %>
                            </div>
                        </td>
                    </div>
                </tr>
                <tr>
                    <td width="15%"></td>
                    <div id="trOnAccount" runat="server" visible="false">
                        <td colspan="2" style="border: 1px solid #ff6a00">
                            <table width="100%" id="amountstable">
                                <tbody>
                                    <tr>
                                        <td width="50%">
                                            <label>Total Advance Payments</label>
                                        </td>
                                        <td width="35%">
                                            <%= GetAdvancePayments("TOTAL_ADVANCE_PAYMENT") %>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Total Invoiced</label>
                                        </td>
                                        <td>
                                            <%= GetAdvancePayments("TOTAL_INVOICE_AMOUNT")%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Total Paid</label>
                                        </td>
                                        <td>
                                            <%= GetAdvancePayments("TOTAL_PAID_AMOUNT")%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Total Outstanding against Invoices</label>
                                        </td>
                                        <td>
                                            <%= GetAdvancePayments("TOTAL_OUTSTANDING_AGAINST_INVOICES")%>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </div>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style="float: right;">
                        <asp:Button runat="server" ID="btnSave" Text="Save" OnClientClick="return checkInvoice();" OnClick="btnSave_Click" CssClass="button rounded" ></asp:Button>
                        <asp:Button runat="server" ID="btnClose" Text="Close" OnClick="btnClose_Click" CssClass="button rounded" UseSubmitBehavior="false"></asp:Button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</asp:Content>
