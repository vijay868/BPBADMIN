﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.Account
{
    public partial class CustomerInvoiceDetail : PageBase
    {
        protected decimal totalshipprice = 0;
        protected decimal m_shipPromoDiscount = 0;
        protected decimal m_productPromoDiscount = 0;
        protected int strRec = 0;
        //CustomerInvoicesModel model = new CustomerInvoicesModel();
        CustomerInvoicesModel l_InvoiceModel = new CustomerInvoicesModel();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                l_InvoiceModel = CustomerInvoicesService.GetData(-1, ConnString);

                UpdatePromotionalDiscounts();
                lblShippingPrice.Text = GetTotalValue("shipping_price", true);
                hdnAddOn1.Value = totalshipprice.ToString();
                lblPromoDiscount.Text = m_productPromoDiscount.ToString();
                lblShipDiscount.Text = m_shipPromoDiscount.ToString();
                lblTotal.Text = GetTotalValue("amount", false);
            }
        }

        protected void lbtnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                CustomerModel.CustomersDataTable customers = CustomerReceiptsService.GetCustomerList("-1", Convert.ToInt32(hdnpkey_customer.Value), ConnString);
                foreach (CustomerModel.CustomersRow row in customers.Rows)
                {
                    lblCustomerId.Text = row.customer_id;
                    lblFirstName.Text = row.cus_firstname;
                    lblLastName.Text = row.cus_lastname;
                    lblTitle.Text = row.title;
                    lblCompanyName.Text = row.company_name;
                    lblAddress1.Text = row.address1;
                    lblAddress2.Text = row.address2;
                    lblCity.Text = row.city;
                    lblState.Text = row.state;
                    lblZip.Text = row.zip;

                    lblCustEmail.Text = row.email;
                    lblName.Text = row.cus_firstname + " " + row.cus_lastname;
                    lblAdd1.Text = row.address1;
                    lblAdd2.Text = row.address2;
                    lblSate.Text = (row.IscityNull() ? "" : row.city + ", ") + (row.IsstateNull() ? "" : row.state + ", ") + row.zip;
                    lblSate.Text = lblSate.Text.TrimEnd(' ').TrimEnd(',');
                    hdnpkey_customer.Value = row.pkey_customer.ToString();
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }

        }

        protected void lbtnUpdate1_Click(object sender, EventArgs e)
        {
            try
            {
                //variable holding the details count
                int dtlCount = 0;
                //Loop Counter variable
                int iCounter = 0;

                int pkeyCustInvoice = -2;//base.GetValue(request,"EditKeys",-2);
                int pkeyCustPO = Convert.ToInt32(hdnpkey_cust_po_master.Value == "" ? "-1" : hdnpkey_cust_po_master.Value);//base.GetValue(request,"fkeyCustPOMaster",-1);	
                int pkeyCustomer = Convert.ToInt32(hdnpkey_customer.Value == "" ? "-1" : hdnpkey_customer.Value);//base.GetValue(request,"fkeyCustomer",-1);	
                int pkeyProduct = Convert.ToInt32(hdnpkey_product.Value == "" ? "-1" : hdnpkey_product.Value);//base.GetValue(request,"txtNewProduct",-1);
                int oldPkeyCustomer = Convert.ToInt32(hdnpkey_customer.Value == "" ? "-1" : hdnpkey_customer.Value); //base.GetValue(request, "oldFkeyCustomer", -1);
                int pkeySupplier = -1;//base.GetValue(request,"fkeysupplier",-1);	

                CustomerInvoicesModel dt = new CustomerInvoicesModel(); ;
                if (ViewState["invDetail"] != null)
                {
                    dt = ViewState["invDetail"] as CustomerInvoicesModel;
                }


                l_InvoiceModel = CustomerInvoicesService.GetCustomerInvoice(pkeyCustInvoice, pkeyCustPO, pkeyCustomer, pkeyProduct, pkeySupplier, ConnString);

                if (dt.Invoices.Rows.Count == 1)
                {
                    CustomerInvoicesModel.InvoicesRow requestRow = (CustomerInvoicesModel.InvoicesRow)dt.Invoices.Rows[0];

                    if (l_InvoiceModel.Invoices.Rows.Count == 1)
                        l_InvoiceModel.Invoices.RemoveInvoicesRow((CustomerInvoicesModel.InvoicesRow)l_InvoiceModel.Invoices.Rows[0]);

                    CustomerInvoicesModel.InvoicesRow newRow = l_InvoiceModel.Invoices.NewInvoicesRow();
                    newRow.ItemArray = requestRow.ItemArray;
                    l_InvoiceModel.Invoices.AddInvoicesRow(newRow);
                }

                dtlCount = dt.InvocieDetails.Rows.Count;

                if (pkeyCustPO == -1 && pkeyCustomer == oldPkeyCustomer)
                {
                    for (iCounter = 0; iCounter < dtlCount; iCounter++)
                    {
                        CustomerInvoicesModel.InvocieDetailsRow requestRow = (CustomerInvoicesModel.InvocieDetailsRow)dt.InvocieDetails.Rows[iCounter];

                        CustomerInvoicesModel.InvocieDetailsRow newRow = l_InvoiceModel.InvocieDetails.NewInvocieDetailsRow();
                        newRow.ItemArray = requestRow.ItemArray;
                        l_InvoiceModel.InvocieDetails.AddInvocieDetailsRow(newRow);
                    }
                }
                ViewState["invDetail"] = l_InvoiceModel;
                rptrProductList.DataSource = l_InvoiceModel.InvocieDetails;
                rptrProductList.DataBind();
                strRec = rptrProductList.Items.Count;
                UpdatePromotionalDiscounts();
                lblShippingPrice.Text = GetTotalValue("shipping_price", true);
                hdnAddOn1.Value = totalshipprice.ToString();
                lblPromoDiscount.Text = m_productPromoDiscount.ToString();
                lblShipDiscount.Text = m_shipPromoDiscount.ToString();
                lblTotal.Text = GetTotalValue("amount", false);
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbtn = sender as LinkButton;
                string strPkey_Product = lbtn.CommandArgument;
                CustomerInvoicesModel custInvData = ViewState["invDetail"] as CustomerInvoicesModel;
                CustomerInvoicesModel.InvoicesRow invoicerow = (CustomerInvoicesModel.InvoicesRow)custInvData.Invoices.NewInvoicesRow();

                int l_iRowCount = custInvData.InvocieDetails.Rows.Count;

                string colName = "fkey_cust_po_dtl";
                System.Data.DataRow[] drs;
                bool deletionApplied = false;
                if (invoicerow.Isfkey_cust_po_masterNull())
                {
                    colName = "pkey_product";
                }

                drs = custInvData.InvocieDetails.Select(colName + "=" + strPkey_Product);
                foreach (System.Data.DataRow dr in drs)
                {
                    custInvData.InvocieDetails.RemoveInvocieDetailsRow((CustomerInvoicesModel.InvocieDetailsRow)dr);
                    deletionApplied = true;
                }

                if (colName.Equals("fkey_cust_po_dtl") && custInvData.InvocieDetails.Rows.Count == 0 && deletionApplied)
                {
                    custInvData.Invoices.Rows[0][custInvData.Invoices.fkey_cust_po_masterColumn.ColumnName] = Convert.DBNull;
                }

                ViewState["invDetail"] = custInvData;
                rptrProductList.DataSource = custInvData.InvocieDetails;
                rptrProductList.DataBind();
                strRec = rptrProductList.Items.Count;
                UpdatePromotionalDiscounts();
                lblShippingPrice.Text = GetTotalValue("shipping_price", true);
                hdnAddOn1.Value = totalshipprice.ToString();
                lblPromoDiscount.Text = m_productPromoDiscount.ToString();
                lblShipDiscount.Text = m_shipPromoDiscount.ToString();
                lblTotal.Text = GetTotalValue("amount", false);
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                CustomerInvoicesModel custInvData = new CustomerInvoicesModel();

                CustomerInvoicesModel.InvoicesRow invoicerow = (CustomerInvoicesModel.InvoicesRow)custInvData.Invoices.NewInvoicesRow();

                invoicerow.pkey_customer_invoice_master = -2;
                invoicerow.fkey_customer = Convert.ToInt32(hdnpkey_customer.Value == "" ? "-1" : hdnpkey_customer.Value); ;
                invoicerow.fkey_cust_po_master = -1;// Convert.ToInt32(hdnpkey_cust_po_master.Value == "" ? "-1" : hdnpkey_cust_po_master.Value);
                invoicerow.fkey_br_frieght = -1;// Convert.ToInt32(hdnpkey_cust_po_master.Value == "" ? "-1" : hdnpkey_cust_po_master.Value);

                invoicerow.invoice_number = txtInvoiceNumber.Text;
                invoicerow.invoice_date = Convert.ToDateTime(txtInvoiceDate.Text);
                invoicerow.order_number = txtOrderNumber.Text;
                if (txtOrderDate.Text != "")
                    invoicerow.order_date = Convert.ToDateTime(txtOrderDate.Text);
                invoicerow.dc_date = Convert.ToDateTime(txtDCDate.Text);
                invoicerow.dc_number = txtDCNumber.Text;
                invoicerow.tracking_number = txtTrackingNo.Text;

                invoicerow.add_on1 = Convert.ToDecimal(hdnAddOn1.Value == "" ? "0" : hdnAddOn1.Value);
                invoicerow.add_on2 = Convert.ToDecimal(hdnAddOn2.Value == "" ? "0" : hdnAddOn2.Value);
                invoicerow.discount = Convert.ToSingle(lblPromoDiscount.Text == "" ? "0" : lblPromoDiscount.Text);
                invoicerow.fkey_supplier = -1;
                invoicerow.frieght = "";
                invoicerow.fkey_user = Identity.UserPK;
                invoicerow.total = 0;
                custInvData.Invoices.Rows.Add(invoicerow);

                foreach (RepeaterItem item in rptrProductList.Items)
                {
                    CustomerInvoicesModel.InvocieDetailsRow detailrow = (CustomerInvoicesModel.InvocieDetailsRow)custInvData.InvocieDetails.NewInvocieDetailsRow();
                    HiddenField hdnProductid = (HiddenField)item.FindControl("hdnProductid");
                    HiddenField hdnProductname = (HiddenField)item.FindControl("hdnProductname");
                    HiddenField hdnquantity = (HiddenField)item.FindControl("hdnquantity");
                    HiddenField hdnprice = (HiddenField)item.FindControl("hdnprice");
                    HiddenField hdnamount = (HiddenField)item.FindControl("hdnamount");
                    HiddenField hdnfkey_cust_po_dtl = (HiddenField)item.FindControl("hdnfkey_cust_po_dtl");

                    detailrow.pkey_cust_invoice_dtl = -2;
                    detailrow.pkey_product = Convert.ToInt32(hdnpkey_product.Value == "" ? "-1" : hdnpkey_product.Value);
                    detailrow.fkey_cust_po_dtl = Convert.ToInt32(hdnfkey_cust_po_dtl.Value == "" ? "-1" : hdnfkey_cust_po_dtl.Value); ;
                    detailrow.product_id = hdnProductid.Value;
                    detailrow.product_name = hdnProductname.Value;
                    detailrow.quantity = Convert.ToInt32(hdnquantity.Value == "" ? "0" : hdnquantity.Value);
                    detailrow.price = Convert.ToDecimal(hdnprice.Value == "" ? "0" : hdnprice.Value);
                    detailrow.amount = Convert.ToDecimal(hdnamount.Value == "" ? "0" : hdnamount.Value);
                    detailrow.fkey_user = Identity.UserPK;
                    custInvData.InvocieDetails.Rows.Add(detailrow);
                }

                int key = CustomerInvoicesService.Update(custInvData, ConnString);
                if (key > 0)
                    AlertScript("Customer Invoice details saved successfully.");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                if (ex.Message.ToLower().IndexOf("duplicate") > -1)
                    AlertScript("Entered Invoice Number already Exists. Please re-enter.");
                else
                    AlertScript("Customer Invoice details save failed .try again.");
            }
        }

        protected string GetTotalValue()
        {
            double dblInvAmt = 0.0;
            Int32 l_iCounter = default(Int32);
            double pricePerUnit = 0;
            double qty = 0;
            Int64 discount = default(Int64);
            try
            {
                if (l_InvoiceModel.Invoices.Rows.Count > 0)
                {
                    CustomerInvoicesModel.InvoicesRow invoicerow = (CustomerInvoicesModel.InvoicesRow)l_InvoiceModel.Invoices.Rows[0];
                    CustomerInvoicesModel.InvocieDetailsRow invoiceDetailrow = (CustomerInvoicesModel.InvocieDetailsRow)l_InvoiceModel.InvocieDetails.Rows[0];

                    if (!invoicerow.Isadd_on1Null())
                    {
                        dblInvAmt = dblInvAmt + Convert.ToDouble(invoicerow.add_on1);
                    }

                    if (!invoicerow.Isadd_on2Null())
                    {
                        dblInvAmt = dblInvAmt + Convert.ToDouble(invoicerow.add_on2);
                    }

                    if (l_InvoiceModel.InvocieDetails.Rows.Count > 0)
                    {
                        for (l_iCounter = 0; l_iCounter <= l_InvoiceModel.InvocieDetails.Rows.Count - 1; l_iCounter++)
                        {
                            qty = Convert.ToDouble(invoiceDetailrow.quantity);
                            pricePerUnit = Convert.ToDouble(invoiceDetailrow.price);
                            dblInvAmt = dblInvAmt + (qty * pricePerUnit);
                        }
                    }

                    if (!invoicerow.IsdiscountNull())
                    {
                        discount = Convert.ToInt64(invoicerow.discount);
                        dblInvAmt = dblInvAmt - (dblInvAmt * discount / 100);
                    }
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                //AlertScript(ex.Message);
            }
            return Convert.ToString(dblInvAmt);
        }

        protected string GetTotalValue(string key, bool isship)
        {
            string returnamount = "";
            try
            {
                //CustomerInvoicesModel.InvoicesRow invoicerow = (CustomerInvoicesModel.InvoicesRow)model.Invoices.Rows[0];
                if (ViewState["invDetail"] != null)
                    l_InvoiceModel = ViewState["invDetail"] as CustomerInvoicesModel;
                decimal totalamount = 0;

                Int32 i = default(Int32);
                if (l_InvoiceModel.InvocieDetails.Rows.Count > 0)
                {
                    CustomerInvoicesModel.InvocieDetailsRow invoiceDetailrow = (CustomerInvoicesModel.InvocieDetailsRow)l_InvoiceModel.InvocieDetails.Rows[0];
                    if (!isship)
                    {
                        for (i = 0; i <= l_InvoiceModel.InvocieDetails.Rows.Count - 1; i++)
                        {
                            if (!invoiceDetailrow.IsamountNull())
                            {
                                totalamount = totalamount + Convert.ToDecimal(l_InvoiceModel.InvocieDetails.Rows[i][key]);
                            }
                        }
                        totalamount = totalamount + totalshipprice;
                        totalamount = totalamount - (m_shipPromoDiscount + m_productPromoDiscount);
                        returnamount = ConvertToMoneyFormat(totalamount).ToString();
                    }
                    else
                    {
                        for (i = 0; i <= l_InvoiceModel.InvocieDetails.Rows.Count - 1; i++)
                        {
                            if (!invoiceDetailrow.Isshipping_priceNull())
                            {
                                totalshipprice = totalshipprice + Convert.ToDecimal(l_InvoiceModel.InvocieDetails.Rows[i][key]);
                            }
                        }
                        returnamount = ConvertToMoneyFormat(totalshipprice).ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                //AlertScript(ex.Message);
                return "";
            }
            return returnamount;
        }

        protected void UpdatePromotionalDiscounts()
        {
            try
            {
                Int32 i = default(Int32);
                CustomerInvoicesModel.InvocieDetailsRow invoiceDetailrow = (CustomerInvoicesModel.InvocieDetailsRow)l_InvoiceModel.InvocieDetails.Rows[0];

                if (l_InvoiceModel.InvocieDetails.Rows.Count > 0)
                {
                    for (i = 0; i <= l_InvoiceModel.InvocieDetails.Rows.Count - 1; i++)
                    {
                        if (!invoiceDetailrow.Ispromo_shipment_discountNull())
                        {
                            trSHipDiscout.Visible = true;
                            m_shipPromoDiscount = m_shipPromoDiscount + Convert.ToDecimal(l_InvoiceModel.InvocieDetails.Rows[i]["promo_shipment_discount"]);
                        }
                    }

                    for (i = 0; i <= l_InvoiceModel.InvocieDetails.Rows.Count - 1; i++)
                    {
                        if (!invoiceDetailrow.Ispromo_product_discountNull())
                        {
                            trPromDiscout.Visible = true;
                            m_productPromoDiscount = m_productPromoDiscount + Convert.ToDecimal(l_InvoiceModel.InvocieDetails.Rows[i]["promo_product_discount"]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                //AlertScript(ex.Message);
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("CustomerInvoiceList.aspx");
        }
    }
}