﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShowAssignedUsers.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Account.ShowAssignedUsers" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Best Print Buy</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" media="all" href="../css/bestprintbuy_style.css" />
    <link rel="stylesheet" media="all" href="../css/jquery.fancybox.css" />
    <script src="../Scripts/jquery.js"></script>
    <script src="../Scripts/common.js"></script>
    <script src="../Scripts/ddsmoothmenu.js"></script>
    <script src="../Scripts/plugin/fancybox/jquery.fancybox.js"></script>
    <!--[if lt IE 9]>
			<script src="../Scripts/html5.js"></script>
		<![endif]-->
    <!--[if lte IE 7]> <html class="ie7"> <![endif]-->
    <!--[if IE 8]>     <html class="ie8"> <![endif]-->
    <!--[if IE 9]>     <html class="ie9"> <![endif]-->
    <!--[if IE 10]>     <html class="ie10"> <![endif]-->
    <!--[if !IE]><!-->
    <!--<![endif]-->
</head>
<body id="Body" runat="server">
    <!-- Wrapper starts -->
    <div id="Div1" class="popup-wrapper" runat="server">
        <div class="popup-title">
            Show Assigned Users
        </div>
        <!-- Header Ends -->
        <div class="popup-contnt">
            <!-- Menu starts -->
            <form id="Form1" runat="server">
                <table class="tablep">
                    <thead>
                        <tr>
                            <th width="3%"></th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Login ID</th>
                            <th>Groups</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rptrAssignedUsers" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td></td>
                                    <td>
                                        <%#Eval("first_name") %>
                                    </td>
                                    <td>
                                        <%#Eval("last_name") %>
                                    </td>
                                    <td>
                                        <%#Eval("login_id") %>
                                    </td>
                                    <td>
                                        <%#Eval("groups") %>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
                <table>
                    <tr>
                        <td style="float: right;">
                            <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClientClick="parent.$.fancybox.close(); return false;"></asp:Button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</body>
</html>
