﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.Account
{
    public partial class CustomerPayment : PageBase
    {
        CustomerModel model = new CustomerModel();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCustomerPaymentDetails();
                if (Convert.ToBoolean(Request.QueryString["isChangeCustomer"] == null ? false : Convert.ToBoolean(Request.QueryString["isChangeCustomer"])))
                {
                    btnSave.Visible = true;
                    spnRef.Visible = false;
                }
            }
        }

        private void BindCustomerPaymentDetails()
        {
            try
            {
                int pkey_cust_admin_receipts = Convert.ToInt32(Request.QueryString["pkeyReceipt"]);
                model = CustomerReceiptsService.GetCustAdminReceipt(model, pkey_cust_admin_receipts, ConnString);
                ViewState["model"] = model;
                for (int year = 2000; year <= 2060; year++)
                {
                    ddlYear.Items.Add(new ListItem(Convert.ToString(year), year.ToString().Substring(2, 2)));
                }
                if (model.Countries.Rows.Count > 0)
                {
                    ddlCountry.DataSource = model.Countries;
                    ddlCountry.DataTextField = "country_name";
                    ddlCountry.DataValueField = "pkey_country";
                    ddlCountry.DataBind();
                    ddlCountry.Items.Insert(0, new ListItem("No Option", ""));
                }
                BindSates();
                if (model.CreditCardTypes.Rows.Count > 0)
                {
                    ddlCreditCardtype.DataSource = model.CreditCardTypes;
                    ddlCreditCardtype.DataTextField = "ct_name";
                    ddlCreditCardtype.DataValueField = "pkey_category_type";
                    ddlCreditCardtype.DataBind();
                    ddlCreditCardtype.Items.Insert(0, new ListItem("No Option", ""));
                }

                if (model.CAdminReceipts.Rows.Count > 0)
                {
                    CustomerModel.CAdminReceiptsRow row = model.CAdminReceipts[0];
                    //ddlLoginby.SelectedValue = row.
                    if (ddlLoginby.SelectedValue == "1")
                        txtEmailAddress.Text = row.email_id;
                    else
                        txtEmailAddress.Text = row.po_number;
                    txtNote.Text = row.notes;

                    txtFirstName.Text = row.first_name;
                    txtLastName.Text = row.last_name;
                    txtAddress1.Text = row.address1;
                    txtAddress2.Text = row.address2;
                    txtCity.Text = row.city;
                    ddlCountry.SelectedValue = row.fkey_country == -1 ? "" : row.fkey_country.ToString();
                    ddlState.SelectedValue = row.fkey_state == -1 ? "" : row.fkey_state.ToString();
                    txtZip.Text = row.zipcode;
                    txtPhone.Text = row.phone;

                    ddlCreditCardtype.SelectedValue = row.fkey_credit_card_type == -1 ? "" : row.fkey_credit_card_type.ToString();
                    //txtCreditcardNumber.Text = row.credit_card_name
                    txtAmount.Text = row.amount.ToString();
                    ddlMonth.SelectedValue = row.valid_month;
                    ddlYear.SelectedValue = row.valid_year;
                    txtNameontheCard.Text = row.credit_card_name;
                    lblPNRef.Text = row.pnref_number;
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        private void BindSates()
        {
            try
            {
                CustomerModel model = (CustomerModel)ViewState["model"];
                if (model.States.Rows.Count > 0)
                {
                    ddlState.DataSource = model.States;
                    ddlState.DataTextField = "state_name";
                    ddlState.DataValueField = "pkey_state";
                    ddlState.DataBind();
                    ddlState.Items.Insert(0, new ListItem("No Option", ""));
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindSates();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string emailid = txtEmailAddress.Text;
                int loginOption = Convert.ToInt32(ddlLoginby.SelectedValue != "-1" ? ddlLoginby.SelectedValue : "1");
                int fkey_admin_user = Identity.UserPK;
                model = CustomerReceiptsService.GetCustAdminReceipt(model, -2, ConnString);
                CustomerModel cdata = new CustomerModel();
                //CustomerModel cdata = CustomerReceiptsService.AdminValidateCuatomerIdentity(emailid, -1, loginOption, "", fkey_admin_user, "", 1, ConnString);
                if (cdata.Customers.Rows.Count <= 0)
                {
                    model = GetCustAdminReceiptsDataModel(model);
                    //ExceptionHandler.HandleException(ProjectType.Web, new Exception("Invalid Email / PO Number. Please re-enter."));
                }
                // model = GetCustAdminReceipts(false);
                model = CheckPaymentValidity(model);

                int key = CustomerReceiptsService.UpdateCustAdminReceipt(model, ConnString);
                AlertScript("Successfully saved");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        private CustomerModel CheckPaymentValidity(CustomerModel custData)
        {
            string card_number = txtCreditcardNumber.Text;
            string month = ddlMonth.SelectedValue;
            string year = ddlYear.SelectedValue;

            string zipCode = string.Empty;
            string streetAddress = string.Empty;

            CustomerModel.CAdminReceiptsRow dr = (CustomerModel.CAdminReceiptsRow)custData.CAdminReceipts.Rows[0];
            zipCode = dr.zipcode;
            streetAddress = dr.address1;

            double amount = 0;

            //Get the Purchase order amount from the cart
            if (Convert.IsDBNull(custData.CAdminReceipts.Rows[0][custData.CAdminReceipts.amountColumn.ColumnName]))
            {
                amount = 0.00;
            }
            else
            {
                amount = Convert.ToDouble(custData.CAdminReceipts.Rows[0][custData.CAdminReceipts.amountColumn.ColumnName]);
            }

            double paymentAmount = amount;
            //variable holding the pnref number
            string pnrefNum = "ssssss";

            //Create a new instance of PaymentUtils object
            PaymentWebUtils pay_util = new PaymentWebUtils();

            //variable holding the validity status falg
            bool status = false;
            Double l_paymentAmount = Convert.ToDouble(string.Format("{0:n2}", paymentAmount));
            try
            {
                if (WebConfiguration.ISIgnoreVerisign || l_paymentAmount == 0.00)
                {
                    status = true;
                }
                else
                {
                    paymentAmount = Convert.ToDouble(string.Format("{0:n2}", paymentAmount));
                    status = pay_util.TransferAmountToMerchant(month + year, card_number, paymentAmount, streetAddress, zipCode);
                    pnrefNum = pay_util.PNREF;
                }

                custData.CAdminReceipts.Rows[0][custData.CAdminReceipts.pnref_numberColumn.ColumnName] = pnrefNum;
                //System.Web.HttpContext.Current.Session["custData"] = custData;

                //In case of invalid staus set the error on the model and throw the exception
                //if (!status)
                //{
                //    custData.CAdminReceipts.Rows[0].SetColumnError(custData.CAdminReceipts.pkey_cust_admin_receiptsColumn, "MSG0014_39_1");
                //    custData.CAdminReceipts.pkey_cust_admin_receiptsColumn.ExtendedProperties.Add("MSG0014_39_1", "MSG0014_39_1");
                //    //AppErrors.throwBusinessException(custData, new Exception("Payments transaction is rejected by the Payment processing server."), null);
                //}
            }
            //catch (AppErrors ex)
            //{
            //    //throw exception
            //    ex.Data = custData;
            //    throw ex;
            //}
            catch (Exception ex)
            {
                pnrefNum = pay_util.PNREF;
                if (status)
                    pay_util.TransferAmountToCardHolder(month + year, paymentAmount, pnrefNum);
                // when audit trail is doing we need to capture this roled back
                // PNREF number for future auditing purpose.
                FileLogger.WriteEntry("Error in Verisign Processor:" + ex.ToString());

                throw ex;
            }

            // return the cart data
            return custData;
        }

        private CustomerModel GetCustAdminReceiptsDataModel(CustomerModel model)
        {
            CustomerModel.CAdminReceiptsRow row = model.CAdminReceipts.NewCAdminReceiptsRow();
            row.pkey_cust_admin_receipts = -2;
            row.email_id = txtEmailAddress.Text;
            row.po_number = txtEmailAddress.Text;
            row.receipt_option = Convert.ToInt32(ddlLoginby.SelectedValue);
            row.first_name = txtFirstName.Text;
            row.last_name = txtLastName.Text;
            row.address1 = txtAddress1.Text;
            row.address2 = txtAddress2.Text;
            row.city = txtCity.Text;
            row.fkey_state = Convert.ToInt32(ddlState.SelectedValue == "" ? "-1" : ddlState.SelectedValue);
            row.zipcode = txtZip.Text;
            row.phone = txtPhone.Text;
            row.fkey_country = Convert.ToInt32(ddlCountry.SelectedValue == "" ? "-1" : ddlCountry.SelectedValue);
            row.fkey_credit_card_type = Convert.ToInt32(ddlCreditCardtype.SelectedValue == "" ? "-1" : ddlCreditCardtype.SelectedValue);
            row.credit_card_name = txtCreditcardNumber.Text;
            row.valid_month = ddlMonth.SelectedValue;
            row.valid_year = ddlYear.SelectedValue;
            row.amount = Convert.ToDecimal(txtAmount.Text == "" ? "0" : txtAmount.Text);
            row.notes = txtNote.Text;
            row.change_user = Identity.UserPK;
            row.title = "";
            row.pnref_number = lblPNRef.Text;
            model.CAdminReceipts.Rows.Add(row);
            return model;
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/CustomerPayments.aspx");
        }

        protected void lbtnFetchCustomer_Click(object sender, EventArgs e)
        {
            try
            {
                string emailid = txtEmailAddress.Text;
                int loginOption = Convert.ToInt32(ddlLoginby.SelectedValue != "-1" ? ddlLoginby.SelectedValue : "1");
                int fkey_admin_user = Identity.UserPK;
                model = CustomerReceiptsService.GetCustAdminReceipt(model, -2, ConnString);
                CustomerModel cdata = CustomerReceiptsService.AdminValidateCuatomerIdentity(emailid, -1, loginOption, "", fkey_admin_user, "", 1, ConnString);
                if (cdata.Customers.Rows.Count <= 0)
                {
                    model = GetCustAdminReceiptsDataModel(model);
                    AlertScript("Invalid Email / PO Number. Please re-enter.");
                    return;
                }
                if (cdata.Customers.Rows.Count > 0)
                    model = CustomerReceiptsService.GetCustAdminReceiptBillInfo(Convert.ToInt32(cdata.Customers.Rows[0]["pkey_customer"]), ConnString);
                if (model.CAdminReceipts.Rows.Count > 0)
                {
                    model.CAdminReceipts.Rows[0]["receipt_option"] = loginOption;
                    model.CAdminReceipts.Rows[0]["po_number"] = loginOption == 2 ? emailid : "";
                }
                if (model.CAdminReceipts.Rows.Count > 0)
                {
                    CustomerModel.CAdminReceiptsRow row = model.CAdminReceipts[0];
                    if (ddlLoginby.SelectedValue == "1")
                        txtEmailAddress.Text = row.email_id;
                    else
                        txtEmailAddress.Text = row.po_number;
                    txtNote.Text = row.IsnotesNull() ? "" : row.notes;

                    txtFirstName.Text = row.Isfirst_nameNull() ? "" : row.first_name;
                    txtLastName.Text = row.Islast_nameNull() ? "" : row.last_name;
                    txtAddress1.Text = row.Isaddress1Null() ? "" : row.address1;
                    txtAddress2.Text = row.Isaddress2Null() ? "" : row.address2;
                    txtCity.Text = row.IscityNull() ? "" : row.city;
                    ddlCountry.SelectedValue = row.Isfkey_countryNull() ? "" : row.fkey_country.ToString();
                    ddlState.SelectedValue = row.Isfkey_stateNull() ? "" : row.fkey_state.ToString();
                    txtZip.Text = row.IszipcodeNull() ? "" : row.zipcode;
                    txtPhone.Text = row.IsphoneNull() ? "" : row.phone;

                    ddlCreditCardtype.SelectedValue = row.Isfkey_credit_card_typeNull() ? "" : row.fkey_credit_card_type.ToString();
                    txtAmount.Text = row.IsamountNull() ? "" : row.amount.ToString();
                    ddlMonth.SelectedValue = row.Isvalid_monthNull() ? "" : row.valid_month;
                    ddlYear.SelectedValue = row.valid_year;
                    txtNameontheCard.Text = row.Iscredit_card_nameNull() ? "" : row.credit_card_name;
                    lblPNRef.Text = row.Ispnref_numberNull() ? "" : row.pnref_number;
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }
    }
}