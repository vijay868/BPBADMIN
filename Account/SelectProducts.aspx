﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SelectProducts.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Account.SelectProducts" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Best Print Buy</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" media="all" href="../css/bestprintbuy_style.css" />
    <link rel="stylesheet" media="all" href="../css/jquery.fancybox.css" />
    <link rel="stylesheet" media="all" href="../css/Style.css" />
    <script src="../Scripts/jquery.js"></script>
    <script src="../Scripts/common.js"></script>
    <script src="../Scripts/ddsmoothmenu.js"></script>
    <script src="../Scripts/plugin/fancybox/jquery.fancybox.js"></script>
    <!--[if lt IE 9]>
			<script src="../Scripts/html5.js"></script>
		<![endif]-->
    <!--[if lte IE 7]> <html class="ie7"> <![endif]-->
    <!--[if IE 8]>     <html class="ie8"> <![endif]-->
    <!--[if IE 9]>     <html class="ie9"> <![endif]-->
    <!--[if IE 10]>     <html class="ie10"> <![endif]-->
    <!--[if !IE]><!-->
    <!--<![endif]-->
    <script type="text/javascript">
        function customerSelect(obj) {
            var grid = obj.parentNode.parentNode.parentNode.parentNode;
            var inputs = grid.getElementsByTagName("input");
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].type == "checkbox") {
                    if (obj.checked && inputs[i] != obj && inputs[i].checked) {
                        inputs[i].checked = false;
                    }
                }
            }
        }

        function SetDataAndClose(pkey_product, product_name) {
            window.top.SetProductData(pkey_product, product_name);
            return false;
        }

        //function SelectCheck() {
        //    alert('asdasdfasdfasd');
        //    $(".chkSelect").click(function () {
        //        if ($(this).is(":checked")) {
        //            return true;
        //        } else {
        //            alert('sdfgsdfg');
        //            return false;
        //        }
        //    });
        //}

        function checkSelect() {
            var ischecked = false;
            $('.select input:checkbox').each(function () {
                if (this.checked) {
                    ischecked = true;
                    return true;
                }
            });

            if (!ischecked) {
                alert("Please select atleast one product.");
                return false;
            }
            return true;
        }
    </script>
</head>
<body id="Body" runat="server">
    <div id="Div1" class="popup-wrapper" runat="server">
        <div class="popup-title">
            Products List
        </div>
        <!-- Header Ends -->
        <div class="popup-contnt">
            <!-- Menu starts -->
            <form id="Form1" runat="server">           
            <div class="clr"></div>
                <asp:GridView ID="POListGrid" runat="server" AutoGenerateColumns="false" EnableViewState="true"
                    ForeColor="Black" Width="100%" PageSize="20" AllowPaging="true"
                    class="tablep" name="POListGrid" OnPageIndexChanging="POListGrid_PageIndexChanging">
                    <PagerStyle HorizontalAlign="Right" CssClass="dtPage"></PagerStyle>
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />
                    <Columns>
                        <asp:TemplateField HeaderText="#SL">
                            <HeaderStyle Width="6%" ForeColor="White"></HeaderStyle>
                            <ItemTemplate>
                                <span><%# GetRecordCountForPage()%></span>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Product Id">
                            <HeaderStyle Width="30%" ForeColor="White"></HeaderStyle>
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container,"DataItem.product_id") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%-- <asp:TemplateField HeaderText="Product Name">
                           <HeaderStyle Width="6%" ForeColor="White"></HeaderStyle>
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container,"DataItem.product_name") %>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:TemplateField HeaderText="Product description">
                            <HeaderStyle Width="27%" ForeColor="White"></HeaderStyle>
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container,"DataItem.comments") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Active">
                            <HeaderStyle Width="6%" ForeColor="White"></HeaderStyle>
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container,"DataItem.is_active") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Current Orders">
                            <HeaderStyle Width="10%" ForeColor="White"></HeaderStyle>
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container,"DataItem.order_value") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Suppliers">
                            <HeaderStyle Width="6%" ForeColor="White"></HeaderStyle>
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container,"DataItem.suppliers") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Select">
                            <HeaderStyle Width="5%" ForeColor="White"></HeaderStyle>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server" onclick="customerSelect(this);" CssClass="select" />
                                <asp:HiddenField ID="hdnpkey_product" runat="server" Value='<%#Eval("pkey_product") %>' />
                                <asp:HiddenField ID="hdnproduct_name" runat="server" Value='<%#Eval("product_name") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>

                <table>
                    <tbody>
                        <tr>
                            <td align="right">
                                <asp:Button runat="server" ID="btnSelect" CssClass="button rounded" Text="Select" OnClick="btnSelect_Click" OnClientClick="if(!checkSelect()) return false;"></asp:Button>
                                <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClientClick="parent.$.fancybox.close(); return false;"></asp:Button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
</body>
</html>
