﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.Account
{
    public partial class CustomerReceiptView : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCustomerReceiptView();
            }
        }

        private void BindCustomerReceiptView()
        {
            try
            {
                int receiptkey = Convert.ToInt32(Request.QueryString["receiptKey"]);
                CustomerStatsModel model = CustomerReceiptsService.GetReceiptViewData(receiptkey, ConnString);

                if (model.CustomerReceipts.Rows.Count > 0)
                {
                    CustomerStatsModel.CustomerReceiptsRow row = model.CustomerReceipts[0];
                    lblReceiptNumbr.Text = row.receipt_number;
                    lblReceiptDate.Text = row.receipt_date.ToString("MM/dd/yyyy") + "{MM/DD/YYYY}";
                    lblModeOfPayment.Text = row.payment_mode;
                    lblInstrumentNumber.Text = row.instrument_no;
                    lblReceiptAmount.Text = row.receipt_amount.ToString();
                    lblReceiptType.Text = row.receipttype;
                    lblRctAmount.Text = row.receipt_amount.ToString();
                    if (row.receipt_type == 0 || row.receipt_type == 2)
                        tdAdvance.Visible = true;
                    else if (row.receipt_type == 1)
                        divInvoice.Visible = true;
                }
                if (model.CustomerInfo.Rows.Count > 0)
                {
                    CustomerStatsModel.CustomerInfoRow row = model.CustomerInfo[0];
                    lblCustomer.Text = row.customer_name;
                }
                if (model.Advance_Receipts.Rows.Count > 0)
                {
                    CustomerStatsModel.Advance_ReceiptsRow row = model.Advance_Receipts[0];
                    lblTotalAdvanceReceipts.Text = row.Istotal_advance_paymentNull() ? "0.0" : row.total_advance_payment.ToString();
                    lblTotalInvoiced.Text = row.Istotal_invoice_amountNull() ? "0.00" : row.total_invoice_amount.ToString();
                    lblOtherRctAmount.Text = row.Istotal_paid_amountNull() ? "0.00" : row.total_paid_amount.ToString();
                    lblTotalOutstanding.Text = row.Istotal_outstanding_amountNull() ? "0.00" : row.total_outstanding_amount.ToString();
                }
                if (model.PendingInvoices.Rows.Count > 0)
                {
                    // rptrInvoiceList.da                   
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }
    }
}