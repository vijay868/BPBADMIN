﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.Account
{
    public partial class CustomerPayments : PageBase
    {
        protected int recordCount = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCustomerPaymentList();
            }
        }

        private void BindCustomerPaymentList()
        {
            try
            {
                CustomerModel model = new CustomerModel();
                model = CustomerReceiptsService.GetCustAdminReceipts(model, "", ConnString);
                if (CustomerPaymentListGrid.Attributes["SortExpression"] != null)
                    model.CAdminReceipts.DefaultView.Sort = CustomerPaymentListGrid.Attributes["SortExpression"] + " " + CustomerPaymentListGrid.Attributes["SortDirection"];
                recordCount = model.CAdminReceipts.Rows.Count;
                CustomerPaymentListGrid.DataSource = model.CAdminReceipts.DefaultView;
                CustomerPaymentListGrid.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void CustomerPaymentListGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            SortGridView(CustomerPaymentListGrid, e);
            CustomerPaymentListGrid.PageIndex = 0;
            BindCustomerPaymentList();
        }

        protected void CustomerPaymentListGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CustomerPaymentListGrid.PageIndex = e.NewPageIndex;
            BindCustomerPaymentList();
        }

        protected int GetRecordCountForPage()
        {
            return GetRecordCountForPageSize(CustomerPaymentListGrid.PageSize, CustomerPaymentListGrid.PageIndex + 1);
        }

        protected void btnChangeCustomer_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Account/CustomerPayment.aspx?isChangeCustomer=" + true);
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/DashBoard.aspx");
        }
    }
}