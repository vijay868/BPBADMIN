﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;

namespace Bestprintbuy.Admin.Web.Account
{
    public partial class UserDetails : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ViewState["userID"] = Request.QueryString["userID"];
                hdnUserId.Value = Request.QueryString["userID"];
                //txtFkeyRegister.Value = Request.QueryString["userID"];
                GetData();
            }
        }

        private void GetData()
        {
            try
            {
                int userID = Convert.ToInt32(Request.QueryString["userID"]);
                int Categorykey = 665;
                UserModel UserDetails = UserService.GetUserDetails(userID, Categorykey, ConnString);

                ddlUsertype.DataSource = UserDetails.UserTypes;
                ddlUsertype.DataTextField = "usertype";
                ddlUsertype.DataValueField = "pkey_category_type";
                ddlUsertype.DataBind();
                ddlUsertype.Items.Insert(0, new ListItem("No Option", "-1"));

                ddlSelectArtist.DataSource = UserDetails.catTypes;
                ddlSelectArtist.DataTextField = "ct_name";
                ddlSelectArtist.DataValueField = "pkey_category_type";
                ddlSelectArtist.DataBind();
                ddlSelectArtist.Items.Insert(0, new ListItem("No Option", "-1"));

                lbAvailableGroups.DataSource = UserDetails.AvailableGroups;
                lbAvailableGroups.DataTextField = "group_name";
                lbAvailableGroups.DataValueField = "pkey_group";
                lbAvailableGroups.SelectionMode = ListSelectionMode.Multiple;
                lbAvailableGroups.DataBind();

                if (UserDetails.User.Rows.Count > 0)
                {
                    UserModel.UserRow userRow = (UserModel.UserRow)UserDetails.User.Rows[0];
                    txtFirstName.Text = userRow.first_name;
                    txtLastName.Text = userRow.last_name;
                    txtLoginID.Text = userRow.login_id;
                    txtComments.Text = userRow.comments;
                    chkActive.Checked = userRow.is_active;
                    chkUsrCngPwd.Checked = userRow.permit_pw_change;
                    chkIsartist.Checked = userRow.is_artist;
                    chkisWHProd.Checked = userRow.is_wh_user;
                    chkIsSalesMkt.Checked = userRow.is_sm_user;
                    ddlUsertype.SelectedValue = userRow.fkey_usertype.ToString() == "" ? "-1" : userRow.fkey_usertype.ToString();
                    ddlSelectArtist.SelectedValue = userRow.fkey_artist_type.ToString() == "" ? "-1" : userRow.fkey_artist_type.ToString();
                    chkCanCumminicate.Checked = userRow.can_communicate_with_customer;
                    txtFkeyRegister.Value = userRow.fkey_register;
                    btnSelecrUser.Enabled = false;
                    btnUserAccessList.Enabled = true;
                    btnAssignedGroups.Visible = true;

                    if (UserDetails.AssignedGroups != null)
                    {
                        lbAssignedGroups.DataSource = UserDetails.AssignedGroups;
                        lbAssignedGroups.DataTextField = "group_name";
                        lbAssignedGroups.DataValueField = "fkey_group";
                        lbAssignedGroups.SelectionMode = ListSelectionMode.Multiple;
                        lbAssignedGroups.DataBind();
                    }

                    if (txtFkeyRegister.Value != "")
                    {
                        btnRegDetails.Visible = true;
                    }
                    else
                    {
                        btnRegDetails.Visible = false;
                    }
                }
                else
                {
                    btnSelecrUser.Enabled = true;
                    btnUserAccessList.Enabled = false;
                    btnAssignedGroups.Visible = false;
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int userID = Convert.ToInt32(ViewState["userID"]);
                UserModel UserDetails = new UserModel();
                UserModel.UserRow user = UserDetails.User.NewUserRow();
                user.pkey_user = userID;
                user.first_name = txtFirstName.Text.Trim();
                user.last_name = txtLastName.Text.Trim();
                user.login_id = txtLoginID.Text.Trim();
                user.password = txtPassword.Text.Trim() == "" ? "" : MD5Encryption.Encrypt(txtPassword.Text.Trim());
                user.comments = txtComments.Text.Trim();
                user.fkey_user = Identity.UserPK;
                user.permit_pw_change = chkUsrCngPwd.Checked;
                user.is_active = chkActive.Checked;
                user.is_artist = chkIsartist.Checked;
                user.is_sm_user = chkIsSalesMkt.Checked;
                user.is_wh_user = chkisWHProd.Checked;
                user.fkey_artist_type = ddlSelectArtist.SelectedValue;
                user.can_communicate_with_customer = chkCanCumminicate.Checked;
                //if (userID != -2)
                user.fkey_register = Convert.ToString(txtFkeyRegister.Value);
                // else
                //     user.fkey_register = "";
                user.fkey_usertype = ddlUsertype.SelectedValue;
                UserDetails.User.AddUserRow(user);

                string AssignedGroups = "";
                if (Request["ctl00$ContentPlaceHolder1$lbAssignedGroups"] != null)
                    AssignedGroups = Request["ctl00$ContentPlaceHolder1$lbAssignedGroups"].ToString();

                if (AssignedGroups != "")
                {
                    foreach (string item in AssignedGroups.Split(','))
                    {
                        UserModel.AssignedGroupsRow assignedGroupsRow = UserDetails.AssignedGroups.NewAssignedGroupsRow();
                        assignedGroupsRow.fkey_group = Convert.ToInt32(item);
                        assignedGroupsRow.fkey_user = userID;
                        assignedGroupsRow.fkey_loggeduser = Identity.UserPK;
                        assignedGroupsRow.pkeyusergroup_xref = -2;
                        UserDetails.AssignedGroups.AddAssignedGroupsRow(assignedGroupsRow);
                    }
                }

                userID = UserService.UpdateUser(UserDetails, ConnString);
                ViewState["GroupID"] = userID;
                Response.Redirect("UserList.aspx");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                if (ex.Message.Contains("duplicate"))
                {                    
                    AlertScript("This Login Id already exists, Please enter another Id and try.");
                }
                else
                {
                    AlertScript(ex.Message);
                }
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("UserList.aspx");
        }

        protected void btnUserAccessList_Click(object sender, EventArgs e)
        {
            int userID = Convert.ToInt32(ViewState["userID"]);
            Response.Redirect("UserAccessList.aspx?userkey=" + userID);
        }

        protected void btnAssignedGroups_Click(object sender, EventArgs e)
        {
            int userID = Convert.ToInt32(ViewState["userID"]);
            Response.Redirect("ShowAssignedGroups.aspx?userkey=" + userID);
        }

        protected void lbtnSelecrUser_Click(object sender, EventArgs e)
        {
            GetData();
        }
    }
}