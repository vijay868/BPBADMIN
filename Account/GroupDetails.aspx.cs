﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;

namespace Bestprintbuy.Admin.Web.Account
{
    public partial class GroupDetails : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ViewState["GroupID"] = Convert.ToInt32(Request.QueryString["groupID"]);
                hdnGroupId.Value = Request.QueryString["groupID"];
                BindFunctionNames();
                GetData();
            }
        }

        private void BindFunctionNames()
        {
            try
            {
                ddlBindfuncNames.DataSource = UserService.GetFunctionalItems(ConnString);
                ddlBindfuncNames.DataTextField = "functional_name";
                ddlBindfuncNames.DataValueField = "functional_name";
                ddlBindfuncNames.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        private void GetData()
        {
            try
            {
                int groupID = Convert.ToInt32(ViewState["GroupID"]);
                string funcationProcess = ddlBindfuncNames.SelectedValue;
                UserModel GroupsDetails = UserService.GetGroupDetails(groupID, funcationProcess, ConnString);
                rptrGruopDetails.DataSource = GroupsDetails.Access;
                rptrGruopDetails.DataBind();
                if (GroupsDetails.group.Rows.Count > 0)
                {
                    UserModel.groupRow groupRow = (UserModel.groupRow)GroupsDetails.group.Rows[0];
                    txtGroupID.Text = groupRow.group_id;
                    txtGroupName.Text = groupRow.group_name;
                    txtComments.Text = groupRow.comments;
                    btnShowAssignedUsrs.Visible = true;
                    btnDelete.Visible = true;
                }
                chkGlbList.Checked = false;
                chkGlbView.Checked = false;
                chkGlbAdd.Checked = false;
                chkGlbEdit.Checked = false;
                chkGlbDelete.Checked = false;
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int groupID = Convert.ToInt32(ViewState["GroupID"]);
                UserService.DeleteGroup(groupID, ConnString);
                Response.Redirect("GroupList.aspx");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int groupID = Convert.ToInt32(ViewState["GroupID"]);
                UserModel GroupsDetails = new UserModel();
                UserModel.groupRow group = GroupsDetails.group.NewgroupRow();
                group.pkey_group = groupID;
                group.group_id = txtGroupID.Text.Trim();
                group.group_name = txtGroupName.Text.Trim();
                group.comments = txtComments.Text;
                group.fkey_user = Identity.UserPK;
                GroupsDetails.group.AddgroupRow(group);

                foreach (RepeaterItem item in rptrGruopDetails.Items)
                {
                    UserModel.AccessRow row = GroupsDetails.Access.NewAccessRow();
                    HiddenField hdnAccessID = (HiddenField)item.FindControl("hdnAccessID");
                    HiddenField hdnScreenKey = (HiddenField)item.FindControl("hdnScreenKey");
                    Label lblName = (Label)item.FindControl("lblName");
                    row.pkey_access = -2;
                    if (hdnAccessID.Value != "" && hdnAccessID.Value != "0")
                        row.pkey_access = Convert.ToInt32(hdnAccessID.Value);

                    row.screen_name = lblName.Text;

                    row.access_view = ((CheckBox)item.FindControl("chkView")).Checked;
                    row.access_list = ((CheckBox)item.FindControl("chkList")).Checked;
                    row.access_add = ((CheckBox)item.FindControl("chkAdd")).Checked;
                    row.access_edit = ((CheckBox)item.FindControl("chkEdit")).Checked;
                    row.access_delete = ((CheckBox)item.FindControl("chkDelete")).Checked;
                    row.fkey_screen = Convert.ToInt32(hdnScreenKey.Value);
                    row.fkey_user = Identity.UserPK;
                    GroupsDetails.Access.AddAccessRow(row);
                }

                groupID = UserService.UpdateGroup(GroupsDetails, ConnString);
                ViewState["GroupID"] = groupID;
                btnShowAssignedUsrs.Visible = true;
                btnDelete.Visible = true;
                AlertScript("Group Datails Successfully saved.");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                if (ex.Message.Contains("duplicate"))
                {
                    AlertScript("Group Id already exists, please try changing the Group Id.");
                }
                else
                {
                    AlertScript(ex.Message);
                }
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("GroupList.aspx");
        }

        protected void ddlBindfuncNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetData();
        }
    }
}