﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="GroupList.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Account.GroupList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function deleteGroup() {
            if ('<%=HasAccess("GroupList", "delete")%>' != 'False') {
               return confirm('This action results in removal of the Group Role(s) and associated user(s) will be detached from the Group, 	Are you sure?');
           }
           else {
               alert('User does not have access.');
               return false;
           }
       }

       function addGroup() {
           if ('<%=HasAccess("GroupList", "add")%>' == 'False') {
                    alert('User does not have access.');
                    return false;
                }
                return true;
            }
    </script>
    <div class="clr"></div>
    <!-- Container Starts -->
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="DashBoard.aspx">Home</a></li>
            <li class="liColor">Security</li>
            <li>Group List</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Group List</h2>
        <form id="frmGroupList" name="frmGroupList" runat="server">            
            <div class="clr"></div>
            <asp:GridView ID="GrouplistGrid" runat="server" AutoGenerateColumns="false" EnableViewState="true"
                ForeColor="Black" Width="100%" PageSize="20" AllowPaging="true" AllowSorting="true"
                class="table" name="GrouplistGrid" OnPageIndexChanging="GrouplistGrid_PageIndexChanging" OnSorting="GrouplistGrid_Sorting">
                <PagerStyle HorizontalAlign="Right" CssClass="dtPage"></PagerStyle>
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />
                <Columns>
                    <asp:TemplateField HeaderText="#SL">
                            <ItemTemplate>
                                <span><%# GetRecordCountForPage()%></span>
                            </ItemTemplate>
                        </asp:TemplateField>
                    <asp:TemplateField HeaderText="Group ID" SortExpression="group_id">
                        <HeaderStyle Width="15%" ForeColor="White" />
                        <ItemTemplate>
                            <a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("GroupDetails", "view")%>','GroupDetails.aspx?groupID=<%# DataBinder.Eval(Container,"DataItem.pkey_group") %>')" title="Edit"><%# DataBinder.Eval(Container,"DataItem.group_id") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Group Name" SortExpression="group_name">
                        <HeaderStyle Width="15%" ForeColor="White" />
                        <ItemTemplate>
                            <a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("GroupDetails", "view")%>','GroupDetails.aspx?groupID=<%# DataBinder.Eval(Container,"DataItem.pkey_group") %>')" title="Edit"><%# DataBinder.Eval(Container,"DataItem.group_name") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Users">
                        <HeaderStyle Width="8%" />
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container,"DataItem.users") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Comments">
                        <HeaderStyle Width="47%" />
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container,"DataItem.comments") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Delete">
                        <HeaderStyle Width="7%" />
                        <ItemTemplate>
                            <asp:LinkButton ID="btnDelete" runat="server" CommandName="Delete" CausesValidation="true"
                                ToolTip="Delete" CommandArgument='<%#Bind("pkey_group")%>' OnClientClick="return deleteGroup();"
                                OnClick="btnDelete_Click"><img src="../images/trashcan.png" /></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
             <asp:Label ID="lblTotalRecords" runat="server" Font-Bold="true"></asp:Label>
            <!-- table Ends -->
            <div class="btn_lst">
                <asp:Button ID="btnAddGrouprole" Text="Add a New Group Role" CssClass="button rounded" runat="server" OnClick="btnAddGrouprole_Click" OnClientClick="return addGroup();"></asp:Button>
                <div class="rgt">
                    <asp:Button ID="btnClose" Text="Close" CssClass="button rounded" runat="server" OnClick="btnClose_Click"></asp:Button>
                </div>
            </div>
        </form>
    </div>
</asp:Content>
