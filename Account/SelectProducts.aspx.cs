﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.Account
{
    public partial class SelectProducts : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindProducts();
            }
        }

        private void BindProducts()
        {
            try
            {
                ProductModel model = new ProductModel();
                model = ProductService.GetProduct(-1, -1, ConnString);

                POListGrid.DataSource = model.Products;
                POListGrid.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected int GetRecordCountForPage()
        {
            return GetRecordCountForPageSize(POListGrid.PageSize, POListGrid.PageIndex + 1);
        }

        protected void POListGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            POListGrid.PageIndex = e.NewPageIndex;
            BindProducts();
        }

        protected void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (GridViewRow item in POListGrid.Rows)
                {
                    HiddenField hdnpkey_product = (HiddenField)item.FindControl("hdnpkey_product");
                    HiddenField hdnproduct_name = (HiddenField)item.FindControl("hdnproduct_name");

                    CheckBox chkbox = (CheckBox)item.FindControl("chkSelect");
                    if (chkbox.Checked == true)
                    {
                        string script = string.Format("SetDataAndClose('{0}','{1}');", hdnpkey_product.Value, hdnproduct_name.Value);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "jscript", script, true);
                    }
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }
    }
}