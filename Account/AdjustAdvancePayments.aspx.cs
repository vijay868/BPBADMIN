﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.Account
{
    public partial class AdjustAdvancePayments : PageBase
    {
        protected bool rowExits = false;
        PaymentModel model = new PaymentModel();
        protected bool PayToExists = false;
        protected bool advanceRow = false;
        protected bool PayExists = false;
        protected int ipaymentType = 0;
        protected int SupplierKey = -1;
        protected int recordCount = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            SupplierKey = Convert.ToInt32(Request.QueryString["fkeyregister"] == null ? "-1" : Request.QueryString["fkeyregister"]);
            txtSupplier.Value = SupplierKey.ToString();
            ipaymentType = Convert.ToInt32(Request.QueryString["paymentType"] == null ? "0" : Request.QueryString["paymentType"]);
            if (!Page.IsPostBack)
            {
                LoadData();
            }
        }

        private void LoadData()
        {
            try
            {
                model = PaymentService.GetAdvances(SupplierKey, ipaymentType, ConnString);

                PendingInvoiceGrid.DataSource = null;
                PendingInvoiceGrid.DataBind();

                if (model.PendingInvoices.Rows.Count > 0)
                {
                    rowExits = true;
                    PendingInvoiceGrid.DataSource = model.PendingInvoices;
                    PendingInvoiceGrid.DataBind();
                }
                if (model.Payments.Rows.Count > 0)
                {
                    PayExists = true;
                    txtPaymentAmount.Value = model.Payments.Rows[0]["amount"].ToString();
                }
                if (model.Advance_payments.Rows.Count > 0)
                {
                    advanceRow = true;
                }
                if (SupplierKey != -1)
                {
                    if (model.Supplier_Payment_Info.Rows.Count > 0)
                    {
                        PayToExists = true;
                        txtPaymentInfoKey.Value = model.Supplier_Payment_Info.Rows[0]["PKEY_BR_PAYMENT_INFO"].ToString();
                    }
                }
                if (ipaymentType == 0)
                {
                    trAgainstInvoice.Visible = true;
                    trOnAccount.Visible = false;
                    rbtnAgainstInvoice.Checked = true;
                    ipaymentType = 0;
                }
                else
                {
                    trOnAccount.Visible = true;
                    trAgainstInvoice.Visible = false;
                    rbtnOnAccount.Checked = true;
                    ipaymentType = 2;
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected string GetInvoiceData(string key, bool bFormat = false)
        {
            try
            {
                string strValue = "0.00";
                if (rowExits)
                {
                    if (bFormat)
                    {
                        if (model.PendingInvoices.Rows[0][key] != null)
                            strValue = string.Format(model.PendingInvoices.Rows[0][key].ToString(), "##,##0.00");
                        if (strValue == "")
                            strValue = "0.00";
                        return strValue;
                    }
                    else
                        return Convert.ToString(model.PendingInvoices.Rows[0][key]);
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
            return "";
        }

        protected string GetPaymentInfo(string key)
        {
            try
            {
                if (PayToExists)
                {
                    if (model.Supplier_Payment_Info.Rows[0][key] != null)
                    {
                        return Convert.ToString(model.Supplier_Payment_Info.Rows[0][key]);
                    }
                    else
                        return "";
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
            return "";
        }

        protected string GetAdvancePayments(string key)
        {
            string strValue = "0.00";
            try
            {
                if (advanceRow)
                {
                    if (model.Advance_payments.Rows[0][key] != null)
                    {
                        strValue = string.Format(model.Advance_payments.Rows[0][key].ToString(), "##,##0.00");
                        if (strValue == "")
                            strValue = "0.00";
                    }
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
            }
            return strValue;
        }

        protected int GetRecordCountForPage()
        {
            return GetRecordCountForPageSize(PendingInvoiceGrid.PageSize, PendingInvoiceGrid.PageIndex + 1);
        }

        protected void PendingInvoiceGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            PendingInvoiceGrid.PageIndex = e.NewPageIndex;
        }

        protected string GetPaymentTypeStatus(int p_paymentType)
        {
            if (p_paymentType == ipaymentType)
                return "checked";
            else
                return "";
        }

        protected string GetRowData(string p_key)
        {
            try
            {
                if (PayExists)
                {
                    if (model.Payments.Rows[0][p_key] != null)
                        return model.Payments.Rows[0][p_key].ToString();
                    else
                        return "";
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
            }
            return "";
        }

        protected string GetCurrencyData(string p_key)
        {
            try
            {
                if (PayExists)
                {
                    string obj = model.Payments.Rows[0][p_key].ToString();
                    if (obj != null)
                        obj = string.Format(obj, "##,##0.00");
                    else
                        obj = "0.00";
                    return obj;
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
            return "";
        }

        protected void rbtnAgainstInvoice_CheckedChanged(object sender, EventArgs e)
        {
            ipaymentType = 0;
            LoadData();
        }

        protected void rbtnOnAccount_CheckedChanged(object sender, EventArgs e)
        {
            ipaymentType = 2;
            LoadData();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                PaymentModel model = new PaymentModel();
                PaymentModel.PaymentsRow row = (PaymentModel.PaymentsRow)model.Payments.NewPaymentsRow();
                PaymentModel.PaymentDetailsRow detailrow = (PaymentModel.PaymentDetailsRow)model.PaymentDetails.NewPaymentDetailsRow();

                row.pkey_payment_master = -2;
                row.amount = Convert.ToDecimal(txtPaymentAmount.Value == "" ? "0" : txtPaymentAmount.Value);
                row.fkey_supplier = SupplierKey;
                row.payment_type = rbtnAgainstInvoice.Checked ? 0 : 2;
                row.fkey_br_payment_info = Convert.ToInt32(txtPaymentInfoKey.Value == "" ? "-1" : txtPaymentInfoKey.Value);
                row.fkey_user = Identity.UserPK;

                model.Payments.AddPaymentsRow(row);
                detailrow.fkey_payment_master = -2;
                //detailrow.fkey_invoice_master = Convert.ToInt32(hdnInvoiceKey.Value == "" ? "-1" : hdnInvoiceKey.Value);
                detailrow.amount = Convert.ToDecimal(txtPaymentAmount.Value == "" ? "-1.00" : txtPaymentAmount.Value);
                detailrow.fkey_user = Identity.UserPK;

                model.PaymentDetails.AddPaymentDetailsRow(detailrow);

                int key = PaymentService.UpdateAdvances(model, ConnString);
                Response.Redirect("StatementofAccount.aspx?pkey_register=" + SupplierKey);
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("StatementofAccount.aspx?pkey_register=" + SupplierKey);
        }
    }
}