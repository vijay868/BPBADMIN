﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;

namespace Bestprintbuy.Admin.Web.Account
{
    public partial class ChangePassword : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblLoginID.Text = Identity.Name;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string password = MD5Encryption.Encrypt(txtOldPassword.Text);
                string newpasswoed = MD5Encryption.Encrypt(txtNewPassword.Text);

                UserService.UpdatePassword(Identity.UserPK, password, newpasswoed, ConnString);
                Response.Redirect("DashBoard.aspx");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("DashBoard.aspx");
        }
    }
}