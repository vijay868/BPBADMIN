﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using System.Text;


namespace Bestprintbuy.Admin.Web.Account
{
    public partial class GroupList : PageBase
    {
        protected int serialNo;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadData();
            }
        }

        private void LoadData()
        {
            try
            {
                UserModel.groupDataTable groups = UserService.GetGroupList(ConnString);
                if (GrouplistGrid.Attributes["SortExpression"] != null)
                    groups.DefaultView.Sort = GrouplistGrid.Attributes["SortExpression"] + " " + GrouplistGrid.Attributes["SortDirection"];
                lblTotalRecords.Text = string.Format(" Total Number of Groups : {0}", groups.Rows.Count);
                GrouplistGrid.DataSource = groups.DefaultView;
                GrouplistGrid.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnAddGrouprole_Click(object sender, EventArgs e)
        {
            Response.Redirect("GroupDetails.aspx?groupID=-2");
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("DashBoard.aspx");
        }

        protected void GrouplistGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            SortGridView(GrouplistGrid, e);
            GrouplistGrid.PageIndex = 0;
            LoadData();
        }

        protected void GrouplistGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GrouplistGrid.PageIndex = e.NewPageIndex;
            LoadData();
        }

        protected int GetRecordCountForPage()
        {
            return GetRecordCountForPageSize(GrouplistGrid.PageSize, GrouplistGrid.PageIndex + 1);
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbtn = (LinkButton)sender;
                int groupID = Convert.ToInt32(lbtn.CommandArgument);
                UserService.DeleteGroup(groupID, ConnString);
                LoadData();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }
    }
}