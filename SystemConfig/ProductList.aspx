﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="ProductList.aspx.cs" Inherits="Bestprintbuy.Admin.Web.SystemConfig.ProductList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function addProduct() {
            $("#ContentPlaceHolder1_txtPattern").removeAttr("required");
            if ('<%=HasAccess("ProductList", "add")%>' == 'False') {
                alert('User does not have access.');
                return false;
            }
            return true;
        }

        function OpenSupplierDialog(productkey, parentID) {
            var url = "DefineSupplierPrices.aspx?productkey=" + productkey + "&parentID=" + parentID;
            $.fancybox({
                'width': '70%',
                'height': '40%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': url,
                'type': 'iframe'
            });
            return false;
        }

        function SetSupplierData(count, parentID) {
            var text = "+Add Suppliers";
            if (count > 0)
                text = "Suppliers(" + count + ")";
            $("#" + parentID).text(text);
            parent.$.fancybox.close();
        }

    </script>
    <script>
        $().ready(function () {
            $("#frmProductsList").validate();
        });
</script>
    <div class="clr"></div>
    <!-- Container Starts -->
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
             <li class="liColor">System Configuration</li>
            <li>Products List</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Products List</h2>
        <form id="frmProductsList" name="frmProductsList" runat="server" class="form">
            <table cellpadding="0" cellspacing="0" border="0" width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="7%">Filter List for </td>
                        <td width="15%">
                            <asp:DropDownList name="ddlFilterFor" ID="ddlFilterFor" runat="server">
                                <asp:ListItem Value="PRODUCT_ID">Product Id</asp:ListItem>
                                <asp:ListItem Value="PRODUCT_NAME">Product Name</asp:ListItem>
                                <asp:ListItem Value="SUM(PC.AMOUNT)/(CASE COUNT(distinct(PKEY_PRODUCT_BR_XREF)) WHEN 0 THEN 1 ELSE COUNT(distinct(PKEY_PRODUCT_BR_XREF)) END )">Order Value</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td width="1%"></td>
                        <td width="15%">
                            <asp:DropDownList name="ddlOperator" ID="ddlOperator" runat="server">
                                <asp:ListItem Value="=">Equals</asp:ListItem>
                                <asp:ListItem Value="<>">Not Equals</asp:ListItem>
                                <asp:ListItem Value="LIKE%">Starts With</asp:ListItem>
                                <asp:ListItem Value="%LIKE">Ends with</asp:ListItem>
                                <asp:ListItem Value="%LIKE%">Contains</asp:ListItem>
                            </asp:DropDownList></td>
                        <td width="1%"></td>
                        <td width="15%">
                            <asp:TextBox name="txtPattern" CssClass="text" ID="txtPattern" runat="server" required></asp:TextBox>
                        </td>
                        <td width="5%">
                            <asp:Button runat="server" ID="btnGo" CssClass="button rounded" Text="GO" OnClick="btnGo_Click"></asp:Button>
                        </td>
                        <td width="5%">
                            <asp:Button runat="server" ID="btnReset" CssClass="button rounded" Text="Reset" OnClick="btnReset_Click" Visible="false" UseSubmitBehavior="false"></asp:Button>
                        </td>
                        <td width="35%"></td>
                    </tr>
                </tbody>
            </table>           
            <div class="clr"></div>
            <asp:GridView ID="ProductlistGrid" runat="server" AutoGenerateColumns="false" EnableViewState="true"
                ForeColor="Black" Width="100%" PageSize="20" AllowPaging="true" AllowSorting="true"
                class="table" name="ProductlistGrid" OnPageIndexChanging="ProductlistGrid_PageIndexChanging" OnSorting="ProductlistGrid_Sorting">
                <PagerStyle HorizontalAlign="Right" CssClass="dtPage"></PagerStyle>
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />
                <Columns>
                    <asp:TemplateField HeaderText="#SL">
                        <HeaderStyle Width="6%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <span><%# GetRecordCountForPage()%></span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Product ID" SortExpression="product_id">
                        <HeaderStyle ForeColor="White" Width="25%"/>
                        <ItemTemplate>
                            <a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("ProductDetail", "view")%>','ProductDetails.aspx?productKey=<%# DataBinder.Eval(Container,"DataItem.PKEY_PRODUCT") %>')" data-gravity="s" title="Edit"><%# DataBinder.Eval(Container,"DataItem.product_id") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Product Name" SortExpression="product_name">
                        <HeaderStyle ForeColor="White" Width="25%"/>
                        <ItemTemplate>
                            <a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("ProductDetail", "view")%>','ProductDetails.aspx?productKey=<%# DataBinder.Eval(Container,"DataItem.PKEY_PRODUCT") %>')" data-gravity="s" title="Edit"><%# DataBinder.Eval(Container,"DataItem.product_name") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Product Description">
                        <ItemTemplate>
                            <%# GetSelectedText(DataBinder.Eval(Container,"DataItem.comments")) %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Active">
                        <HeaderStyle Width="10%"/>
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container,"DataItem.is_active") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Order Value" SortExpression="order_value">
                        <HeaderStyle ForeColor="White" Width="10%"/>
                        <ItemTemplate>
                            <%# ConvertToMoneyFormat(DataBinder.Eval(Container,"DataItem.order_value")) %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Suppliers">
                        <HeaderStyle ForeColor="White" Width="10%"/>
                        <ItemTemplate>
                            <a href="#" onclick="OpenSupplierDialog('<%# DataBinder.Eval(Container,"DataItem.PKEY_PRODUCT") %>', this.id);" id="aSupplier<%# Container.DataItemIndex%>" class="open-add-client-dialog" title="Edit"><%#Convert.ToInt32(DataBinder.Eval(Container,"DataItem.suppliers")) > 0 ? "Suppliers("+DataBinder.Eval(Container,"DataItem.suppliers")+")" : "+Add Suppliers" %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <div class="ttl"><b>Total Number of Products: </b><%=recordCount%></div>
             <div class="btn_lst">
                 <div class="lft">
                      <asp:Button ID="btnAddnewProduct" Text="Add New Product" CssClass="button rounded" runat="server" OnClientClick="return addProduct();" OnClick="btnAddnewProduct_Click"></asp:Button>
                 </div>
                <div class="rgt">
                    <asp:Button ID="btnClose" Text="Close" CssClass="button rounded" runat="server" OnClick="btnClose_Click" UseSubmitBehavior="false"></asp:Button>
                </div>
            </div>
        </form>
    </div>
</asp:Content>
