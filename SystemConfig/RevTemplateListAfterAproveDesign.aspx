﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="RevTemplateListAfterAproveDesign.aspx.cs" Inherits="Bestprintbuy.Admin.Web.SystemConfig.RevTemplateListAfterAproveDesign" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        var mCustomerPortalURL = "<%= mCustomerPortalURL%>";
    </script>
    <div class="clr"></div>
    <!-- Container Starts -->
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
              <li class="liColor">System Configuration</li>
            <li><a href="RevTemplateList.aspx">Reverse Templates</a></li>
            <li>Business Card Template For Selection After Approve Design</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Business Card Template For Selection After Approve Design</h2>
        <form id="frmRevTemplateListAfterAproveDesign" name="frmRevTemplateListAfterAproveDesign" runat="server">
            <table width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="15%">
                            <label>Kiosk</label></td>
                        <td width="20%">
                            <asp:DropDownList name="ddlKiosk" ID="ddlKiosk" CssClass="slect-box" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlKiosk_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td width="15%">
                            <label>Template Type</label>
                        </td>
                        <td width="35%">
                            <asp:DropDownList name="ddlTemplateType" ID="ddlTemplateType" CssClass="slect-box" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlTemplateType_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4"></td>
                    </tr>
                </tbody>
            </table>
            <table wid="100%" class="accounts-table">
                <tr>
                    <td width="10%">&nbsp;</td>
                    <td width="80%" colspan="2">
                        <asp:DataList runat="server" ID="dtlRevTemplates" RepeatColumns="5" RepeatDirection="Horizontal">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td style="padding: 15px 8px 0px 8px;">
                                            <img src="<%# mCustomerPortalURL+Eval("thumbnail_url") %>" id="imgThumbnailUrl" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0px 8px 0px 8px;">
                                            <asp:CheckBox runat="server" ID="chkTemplate" CssClass="check-box" Checked='<%# Eval("enable_ad_selection").ToString().Trim() == "True" ? true : false %>'></asp:CheckBox>&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblTemplateName" runat="server" Text='<%#Eval("template_id")%>'></asp:Label>
                                            <asp:HiddenField ID="hdnFkeyTemplateLayout" Value='<%#Eval("fkey_template_layout")%>' runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0px 8px 5px 8px;">
                                            <asp:TextBox ID="txtdisplay" runat="server" Text='<%#Eval("display_text")%>' CssClass="text-box" Width="80%"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:DataList>
                    </td>
                    <td width="10%">&nbsp;
                    </td>
                </tr>
            </table>
            <table width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="50%">&nbsp;</td>
                        <td width="35%" style="float: right;">
                            <asp:Button runat="server" CssClass="button rounded" ID="btnSave" Text="Save" OnClick="btnSave_Click"></asp:Button>
                            <asp:Button runat="server" CssClass="button rounded" ID="btnClose" Text="Close" OnClick="btnClose_Click"></asp:Button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
</asp:Content>
