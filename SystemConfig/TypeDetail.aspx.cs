﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.SystemConfig
{
    public partial class TypeDetail : PageBase
    {
        protected int pkeyType;
        protected int pkeyCategory;
        protected int pkeyMaster;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                GetData();
            }
        }

        private void GetData()
        {
            try
            {
                pkeyType = Convert.ToInt32(Request.QueryString["pkeytype"]);
                pkeyCategory = Convert.ToInt32(Request.QueryString["pkeyCategory"] == "" ? "-1" : Request.QueryString["pkeyCategory"]);
                pkeyMaster = Convert.ToInt32(Request.QueryString["pkeyMaster"] == "" ? "-1" : Request.QueryString["pkeyMaster"]);
                TypeModel TypeData = SupportTypesService.GetTypesList(pkeyType, pkeyMaster, pkeyCategory, ConnString);

                var catType = from collect in TypeData.MasterCategory
                              where collect.pkey_master_category == pkeyMaster
                              select collect;

                var catType1 = from collect in TypeData.Categories
                               where collect.pkey_category_type == pkeyCategory
                               select collect;

                if (catType.Count() == 1)
                {
                    lblMasterCategory.Text = catType.ToList()[0].mc_name;    // TypeData.MasterCategory.Rows[0]["mc_name"].ToString();
                }

                if (pkeyCategory != -1)
                {
                    lblCategory.Text = catType1.ToList()[0].ct_name;
                }
                else
                {
                    lblCategory.Text = TypeData.Categories.Rows[0]["ct_name"].ToString();
                }

                ddlParentType.DataSource = TypeData.ParentTypes;
                ddlParentType.DataTextField = "ct_name";
                ddlParentType.DataValueField = "pkey_category_type";
                ddlParentType.DataBind();

                if (TypeData.Types.Rows.Count > 0)
                {
                    TypeModel.TypesRow Row = (TypeModel.TypesRow)TypeData.Types.Rows[0];
                    txtTypeID.Text = Row.ct_abbr;
                    txtTypeName.Text = Row.ct_name;
                    if (!Row.IscommentsNull())
                        txtComments.Text = Row.comments;
                    else
                        txtComments.Text = "";
                    chkBackOrdered.Checked = Row.is_backordered;
                    ddlParentType.SelectedValue = TypeData.Types.Rows[0]["fkey_parent_type"].ToString();
                    chkRealestste.Checked = Row.is_realestate;
                    chkParentType.Checked = Row.has_parent_type;
                }

                if (pkeyMaster == 14)
                {
                    pnlProductSelection.Visible = true;
                }

                if (TypeData.AvailableProductTypes != null)
                {
                    var availableProducts = from avalilabeproducts in TypeData.AvailableProductTypes
                                            where avalilabeproducts.isSelected == 0
                                            select avalilabeproducts;

                    lbAvailableProductTypes.DataSource = availableProducts;
                    lbAvailableProductTypes.DataTextField = "ct_name";
                    lbAvailableProductTypes.DataValueField = "pkey_product_type_xref";
                    lbAvailableProductTypes.SelectionMode = ListSelectionMode.Multiple;
                    lbAvailableProductTypes.DataBind();

                    var assignedProduct = from assignedproducts in TypeData.AvailableProductTypes
                                          where assignedproducts.isSelected == 1
                                          select assignedproducts;

                    lbAssignedProductTypes.DataSource = assignedProduct;
                    lbAssignedProductTypes.DataTextField = "ct_name";
                    lbAssignedProductTypes.DataValueField = "pkey_product_type_xref";
                    lbAssignedProductTypes.SelectionMode = ListSelectionMode.Multiple;
                    lbAssignedProductTypes.DataBind();

                    //foreach (TypeModel.AvailableProductTypesRow row in TypeData.AvailableProductTypes)
                    //{
                    //    if (row.isSelected == 1)
                    //    {
                    //        lbProducts.Items.FindByValue(row.pkey_product_type_xref.ToString()).Selected = true;
                    //    }
                    //}
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int pkeytype = Convert.ToInt32(Request.QueryString["pkeytype"]);
                int pkeyCategory = Convert.ToInt32(Request.QueryString["pkeyCategory"] == "" ? "-1" : Request.QueryString["pkeyCategory"]);
                int pkeyMaster = Convert.ToInt32(Request.QueryString["pkeyMaster"] == "" ? "-1" : Request.QueryString["pkeyMaster"]);
                TypeModel types = new TypeModel();
                TypeModel.TypesRow Row = types.Types.NewTypesRow();
                Row.pkey_category_type = pkeytype;
                Row.ct_abbr = txtTypeID.Text;
                Row.ct_name = txtTypeName.Text;
                Row.comments = txtComments.Text;
                Row.fkey_master_category = pkeyMaster;
                Row.fkey_self = pkeyCategory;
                Row.fkey_user = Identity.UserPK;
                Row.fkey_parent_type = ddlParentType.SelectedValue;
                Row.has_parent_type = chkParentType.Checked;
                Row.is_backordered = chkBackOrdered.Checked;
                Row.is_realestate = chkRealestste.Checked;
                types.Types.AddTypesRow(Row);

                if (Convert.ToInt32(Row["fkey_master_category"]) == 14)
                {
                    TypeModel.AvailableProductTypesRow avilTemplateRow;

                    string ApplicableProductTypes = "";
                    if (Request["ctl00$ContentPlaceHolder1$lbAssignedProductTypes"] != null)
                        ApplicableProductTypes = Request["ctl00$ContentPlaceHolder1$lbAssignedProductTypes"].ToString();

                    //if (ApplicableProductTypes != "")
                    //{
                    //    foreach (string item in ApplicableProductTypes.Split(','))
                    //    {
                    //        templateKeys += item.ToString() + ",";
                    //    }
                    //}

                    ApplicableProductTypes = ApplicableProductTypes.TrimEnd(',');
                    String[] applicableTypes = ApplicableProductTypes.Split(',');

                    for (int i = 0; i < applicableTypes.Length && !"".Equals(applicableTypes); i++)
                    {
                        avilTemplateRow = types.AvailableProductTypes.NewAvailableProductTypesRow();

                        avilTemplateRow.pkey_product_type_xref = Convert.ToString(applicableTypes[i]);
                        avilTemplateRow.ct_name = "";
                        avilTemplateRow.fkey_user = Identity.UserPK;
                        types.AvailableProductTypes.Rows.Add(avilTemplateRow);
                    }
                }
                pkeytype = SupportTypesService.Update(types, ConnString);
                AlertScript("Successfully Saved");
                GetData();
            }
            catch (Exception ex)
            {
                if (ex.Message.ToLower().IndexOf("duplicate")>-1)
                {
                    FileLogger.WriteException(ex);
                    AlertScript("Type ID must be unique. Please re-enter Type ID.");
                }
                else
                {
                    FileLogger.WriteException(ex);
                    AlertScript("Save/Update failed. try again.");
                }
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                pkeyType = Convert.ToInt32(Request.QueryString["pkeytype"]);
                pkeyCategory = Convert.ToInt32(Request.QueryString["pkeyCategory"] == "" ? "-1" : Request.QueryString["pkeyCategory"]);
                pkeyMaster = Convert.ToInt32(Request.QueryString["pkeyMaster"] == "" ? "-1" : Request.QueryString["pkeyMaster"]);
                string url = string.Format("TypeList.aspx?pkeytype={0}&pkeyMaster={1}&pkeyCategory={2}", pkeyType, pkeyMaster, pkeyCategory);
                Response.Redirect(url);
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }
    }
}