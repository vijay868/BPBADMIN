﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.SystemConfig
{
    public partial class DefineSupplierPrices : PageBase
    {
        protected int suppliersCount = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                hdnParentID.Value = Request.QueryString["parentID"];
                GetData();
            }
        }

        private void GetData()
        {
            try
            {
                int productKey = Convert.ToInt32(Request.QueryString["productkey"]);
                SupplierPriceModel suppliersPriceData = SupplierPriceServise.GetList(productKey, false, ConnString);
                suppliersCount = suppliersPriceData.SuppliersPrice.Rows.Count;

                rptrSupplierTypes.DataSource = suppliersPriceData.SuppliersPrice;
                rptrSupplierTypes.DataBind();

                foreach (RepeaterItem item in rptrSupplierTypes.Items)
                {
                    HiddenField hdncompeIndex = (HiddenField)item.FindControl("hdncompeIndex");
                    DropDownList ddl = (DropDownList)item.FindControl("ddlcompeIndex");
                    for (int index = 0; index < 1000; index++)
                    {
                        ddl.Items.Add(new ListItem(index.ToString(), index.ToString()));
                        if (hdncompeIndex.Value == index.ToString())
                        {
                            ddl.SelectedValue = index.ToString();
                        }
                    }
                }

                hdnSupplierCount.Value = suppliersPriceData.SuppliersPrice.Select("pkey_supplier_price > 0").Length.ToString();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int productKey = Convert.ToInt32(Request.QueryString["productkey"]);
                SupplierPriceModel suppliersDetails = new SupplierPriceModel();
                foreach (RepeaterItem item in rptrSupplierTypes.Items)
                {
                    HiddenField hdnSelect = (HiddenField)item.FindControl("hdnSelect");
                    HiddenField hdnSupplier = (HiddenField)item.FindControl("hdnSupplier");
                    CheckBox chk = (CheckBox)item.FindControl("chkSelect");
                    DropDownList ddl = (DropDownList)item.FindControl("ddlcompeIndex");
                    TextBox txtPrice = (TextBox)item.FindControl("txtPrice");
                    TextBox txtCapacityPerDay = (TextBox)item.FindControl("txtCapacityPerDay");
                    SupplierPriceModel.SuppliersPriceRow row = (SupplierPriceModel.SuppliersPriceRow)suppliersDetails.SuppliersPrice.NewSuppliersPriceRow();
                    if (chk.Checked)
                    {
                        row.pkey_supplier_price = hdnSelect.Value != "" ? hdnSelect.Value : "-2";
                        row.price = Convert.ToDouble(txtPrice.Text.ToString().Replace("$", ""));
                        row.max_capacity_per_day = Convert.ToInt32(txtCapacityPerDay.Text);
                        row.fkey_user = Identity.UserPK;
                        row.competitive_index = Convert.ToInt32(ddl.SelectedValue);
                        row.fkey_supplier = Convert.ToInt32(hdnSupplier.Value);
                        row.fkey_product = productKey;

                        suppliersDetails.SuppliersPrice.AddSuppliersPriceRow(row);
                    }
                }
                suppliersDetails = SupplierPriceServise.Update(suppliersDetails, ConnString);
                GetData();
                AlertScript("Save/Update Successfully completed.");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript("Save/Update failed. try again.");
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbtn = (LinkButton)sender;
                int supplierPkey = Convert.ToInt32(lbtn.CommandArgument);
                SupplierPriceServise.DeleteSupplier(supplierPkey, ConnString);
                GetData();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }
    }
}