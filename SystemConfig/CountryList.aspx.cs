﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.SystemConfig
{

    public partial class CountryList : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadData();
            }
        }

        private void LoadData()
        {
            try
            {
                int countrykey = -1;
                CountryModel country_Data = CountryService.GetList(countrykey, ConnString);
                lblTotalRecords.Text = string.Format("Total Number of Countries :{0}", country_Data.Countries.Rows.Count);
                CountryGrid.DataSource = country_Data.Countries;
                CountryGrid.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected int GetRecordCountForPage()
        {
            return GetRecordCountForPageSize(CountryGrid.PageSize, CountryGrid.PageIndex + 1);
        }

        protected void CountryGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CountryGrid.PageIndex = e.NewPageIndex;
            LoadData();
        }
        protected void btnAddCountry_Click(object sender, EventArgs e)
        {
            Response.Redirect("CountryDetail.aspx?pkeyCountry=-2");
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/DashBoard.aspx");
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbtn = (LinkButton)sender;
                int countrykey = Convert.ToInt32(lbtn.CommandArgument);
                CountryService.Delete(countrykey, ConnString);
                Response.Redirect("CountryList.aspx");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                if (ex.Message.ToLower().Contains("fk_pps_states_pps_countries"))
                {
                    AlertScript("Cannot delete country as country has states.");
                }
                else
                    AlertScript(ex.Message);
            }
        }

        protected void CountryGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }
    }
}