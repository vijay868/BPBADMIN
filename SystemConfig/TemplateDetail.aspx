﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="TemplateDetail.aspx.cs" Inherits="Bestprintbuy.Admin.Web.SystemConfig.TemplateDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        $().ready(function () {
            $("#fmTemplateDetails").validate();
        });

        function moveProductOptions(option, moveAll) {
            fromObject = document.getElementById("ContentPlaceHolder1_lbAvailableProductTypes");
            toObject = document.getElementById("ContentPlaceHolder1_lbAssignedProductTypes");

            if (option == "right") {
                from = eval(fromObject);
                to = eval(toObject);
            }
            else {
                from = eval(toObject);
                to = eval(fromObject);
            }

            if (from && to) {
                var fromSize = from.options.length;
                var toSize = to.options.length;
                var fromArray = new Array();
                var fromSelectedArray = new Array();
                var fromSelectedIndex = 0;
                var fromIndex = 0;
                for (i = fromSize - 1; i > -1; i--) {
                    var isSelected = false
                    if (from.options[i].selected)
                        isSelected = from.options[i].selected;
                    if ((isSelected && isSelected == true) || moveAll == true) {
                        fromSelectedArray[fromSelectedIndex] = new Array(from.options[i].value, from.options[i].text);
                        from.options[i] = null;
                        fromSelectedIndex++;
                    }
                    else {
                        fromArray[fromIndex] = new Array(from.options[i].value, from.options[i].text);
                        from.options[i] = null;
                        fromIndex++;
                    }
                }
                fromSize = fromArray.length;
                for (i = 0; i < fromSize; i++) {
                    from.options[i] = new Option(fromArray[fromSize - 1 - i][1], fromArray[fromSize - 1 - i][0]);
                }
                var newToSize = fromSelectedArray.length;
                var strData = "";
                for (i = 0; i < newToSize; i++) {
                    toSize = to.options.length;
                    to.options[toSize] = new Option(fromSelectedArray[i][1], fromSelectedArray[i][0]);
                    strData = strData + to.options[toSize].value;
                    if (i != newToSize - 1) strData = strData + ",";
                }
            }
        }

        function selectMemberRoles() {
            if (document.getElementById('ContentPlaceHolder1_lbAssignedProductTypes')) {
                var leng = document.getElementById('ContentPlaceHolder1_lbAssignedProductTypes').options.length;
                for (i = leng - 1; i > -1; i--) {
                    document.getElementById('ContentPlaceHolder1_lbAssignedProductTypes').options[i].selected = true;
                }
            }
        }

        function OpenProductDialog() {
            var promoKey = "<%= pkeyTemplate%>";
            var productIds = "";
            var templateOption = "";
            if (promoKey != "-2") {
                productIds = "<%= fkey_product_type%>";
            } else {
                productIds = $("#ContentPlaceHolder1_ddlProductType option:selected").val();
                templateOption = $("#ContentPlaceHolder1_ddlProductCategory option:selected").val();
            }

            var url = "ProductSelect.aspx?promoKey=" + promoKey + "&producttypekeys=" + productIds + "&templateOption=" + templateOption + "&productIds=" + $("#ContentPlaceHolder1_hdnProductIds").val();
            $.fancybox({
                'width': '80%',
                'height': '80%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': url,
                'type': 'iframe'
            });
            return false;
        }

        function SetProductData(ProductIds, ProductNames, ProductCat) {
            $("#ContentPlaceHolder1_hdnProductIds").val(ProductIds);
            $("#ContentPlaceHolder1_hdnProductCat").val(ProductCat);
            $("#ContentPlaceHolder1_hdnProductNames").val(ProductNames.split(";:;").join("^"));
            $("#ContentPlaceHolder1_txtAssignedProducts").val(ProductNames.split(";:;").join("\n"));
            parent.$.fancybox.close();
        }

        function deleteTemplate() {
            if ('<%=HasAccess("TemplateList", "delete")%>' != 'False') {
                 return confirm('This action results in removal of the Template.       Do you want to continue?');
             }
             else {
                 alert('User does not have access.');
                 return false;
             }
         }
         function saveTemplate() {
             if ('<%=HasAccess("TemplateList", pkeyTemplate != -2 ? "edit" :"add")%>' == 'False') {
                 alert('User does not have access.');
                 return false;
             }
             selectMemberRoles();
             if (document.getElementById('ContentPlaceHolder1_lbAssignedProductTypes')) {
                 var leng = document.getElementById('ContentPlaceHolder1_lbAssignedProductTypes').options.length;
                 if (leng == 0) {
                     alert("Please select a Template Type.");
                     return false;
                 }
             }
             return true;
         }
    </script>

    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
             <li class="liColor">System Configuration</li>
            <li><a href="TemplateList.aspx">Template List</a></li>
            <li>Template Details</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Template Details</h2>
        <form id="fmTemplateDetails" name="fmTemplateDetails" runat="server">
            <table width="100%" class="accounts-table">
                <thead>
                    <tr>
                        <th colspan="4" width="100%">Promotion Detail</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="15%">
                            <label>Template Name</label></td>
                        <td width="20%">
                            <asp:TextBox CssClass="text-box" name="txtTemplateName" ID="txtTemplateName" runat="server"></asp:TextBox>
                        </td>
                        <td width="15%">&nbsp;
                        </td>
                        <td width="35%">&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Product Category*</label></td>
                        <td>
                           <asp:Label ID="lblProductCategory" runat="server" Visible="false" Font-Size="14px"></asp:Label>
                                <asp:HiddenField ID="hdnProdCat" runat="server" />
                            <asp:DropDownList name="ddlProductCategory" ID="ddlProductCategory" Visible="false" CssClass="slect-box" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlProductCategory_SelectedIndexChanged" required>
                            </asp:DropDownList>
                        </td>
                        <td> <label>Product Type*</label></td>
                        <td>
                            <asp:Label ID="lblProductType" runat="server" Visible="false" Font-Size="14px"></asp:Label>
                                <asp:HiddenField ID="hdnProdType" runat="server" />
                            <asp:DropDownList name="ddlProductType" ID="ddlProductType" Visible="false" CssClass="slect-box" required runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlProductType_SelectedIndexChanged">                                
                            </asp:DropDownList>
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <label>Card Orientation </label>
                        </td>
                        <td>
                            <asp:DropDownList name="ddlCardOrient" ID="ddlCardOrient" CssClass="slect-box" runat="server">
                                <asp:ListItem Value="0" Selected>LandScape</asp:ListItem>
								<asp:ListItem Value="1">Portrait</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                         <td><label>By Theme*</label></td>
                         <td>
                             <asp:DropDownList name="ddlByTheme" ID="ddlByTheme" CssClass="slect-box" required runat="server">
                                 <asp:ListItem Text="No Option" Value="" />
                            </asp:DropDownList>
                         </td>
                    </tr>
                    <tr>
                        <td>
                            <label>By Category* </label>
                        </td>
                        <td>
                             <asp:DropDownList name="ddlByCategory" ID="ddlByCategory" CssClass="slect-box" required runat="server">
                                 <asp:ListItem Text="No Option" Value="" />
                            </asp:DropDownList>
                        </td>
                        <td>
                            <label>Additional Qualifier</label>
                        </td>
                        <td>
                          <asp:DropDownList name="ddlAddQuailifier" ID="ddlAddQuailifier" CssClass="slect-box" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Exclusive Customer </label>
                        </td>
                        <td>
                            <asp:DropDownList name="ddlExclusiveCustomer" ID="ddlExclusiveCustomer" CssClass="slect-box" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <label>Template Identity*</label>
                        </td>
                        <td>
                            <asp:HiddenField ID="hdnTemplatesCategory" runat="server" />
                            <asp:DropDownList name="ddlTemplateIdentity" ID="ddlTemplateIdentity" CssClass="slect-box" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlTemplateIdentity_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </tbody>
            </table>

            <table width="100%" class="accounts-table">
                <thead>
                    <tr>
                        <th colspan="4" width="100%">Apply Eligible Template Types</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="15%"> <label></label></td>
                        <td width="50%">
                            <div style="width: 40%; float: left">
                                 <label style="padding-left:0px;"><b>Available Template Types</b></label>
                                <asp:ListBox ID="lbAvailableProductTypes" CssClass="list-box" runat="server" SelectionMode="Multiple"></asp:ListBox>
                            </div>
                           <div style="width: 10%; float: left; padding:40px 10px 0 10px;">
                                <button id="to2" type="button" class="button rounded width53" onclick="javascript:moveProductOptions('right',false)">
                                    &nbsp;&gt;&nbsp;</button><br />
                                <button id="allTo2" type="button" class="button rounded width53" onclick="javascript:moveProductOptions('right',true)">
                                    &nbsp;&gt;&gt;&nbsp;</button><br />
                                <button id="to1" type="button" class="button rounded width53" onclick="javascript:moveProductOptions('left',false)">
                                    &nbsp;&lt;&nbsp;</button><br />
                                <button id="allTo1" type="button" class="button rounded width53" onclick="javascript:moveProductOptions('left',true)">
                                    &nbsp;&lt;&lt;&nbsp;</button>
                            </div>
                            <div style="width: 35%; float: left; padding-right: 10px;">
                                 <label style="padding-left:0px;"><b>Eligible Template Types</b></label>
                                <asp:ListBox ID="lbAssignedProductTypes" runat="server" CssClass="list-box" SelectionMode="Multiple"></asp:ListBox>
                            </div>
                        </td>
                         <td width="35%"></td>
                    </tr>
                </tbody>
            </table>

            <table width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="15%"> <label></label></td>
                        <td width="35%">
                            <asp:CheckBox ID="chkAlltimeFavourites" CssClass="check-box" runat="server" /><asp:Label ID="Label1" runat="server" Text="   Include in All Time Favourites."></asp:Label>
                        </td>
                        <td width="35%"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:CheckBox ID="chkTempeligble" CssClass="check-box" runat="server" /><asp:Label ID="Label11" runat="server" Text="   Template Eligble for Free."></asp:Label>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:CheckBox ID="chkHasDesig" CssClass="check-box" runat="server" /><asp:Label ID="Label2" runat="server" Text="   Has Designation Logo."></asp:Label>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top;">
                            <label>Assigned Products</label></td>
                        <td>
                            <asp:HiddenField ID="hdnProductIds" runat="server" />
                            <asp:HiddenField ID="hdnProductNames" runat="server" />
                            <asp:HiddenField ID="hdnProductCat" runat="server" />
                            <asp:TextBox name="txtAssignedProducts" ID="txtAssignedProducts" CssClass="text-box" runat="server" ReadOnly="true" TextMode="MultiLine" Style="overflow-y: auto;resize:none" Rows="5" Width="450px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Button ID="btnSelectProductTypes" runat="server" CssClass="button rounded" Text="Select Products" UseSubmitBehavior="false" OnClientClick="OpenProductDialog(); return false;"></asp:Button>
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <label>Template Location*</label></td>
                        <td>
                            <asp:TextBox name="txtTemplateLocation" ID="txtTemplateLocation" Width="450px" CssClass="text-box" runat="server" required></asp:TextBox>
                        </td>
                        <td>
                            
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <label>Thumb Nail*</label></td>
                        <td>
                            <asp:TextBox name="txtThumbnailUrl" ID="txtThumbnailUrl" CssClass="text-box" Width="450px" runat="server" required></asp:TextBox>
                        </td>
                        <td style="float:right;">
                              <asp:Button runat="server" ID="btnDelete" Text="Delete" CssClass="button rounded" Visible="false"  OnClientClick="return deleteTemplate();" OnClick="btnDelete_Click"></asp:Button>
                            <asp:Button runat="server" CssClass="button rounded" ID="btnSave" Text="Save" OnClientClick="return saveTemplate();" OnClick="btnSave_Click"></asp:Button>
                            <asp:Button runat="server" CssClass="button rounded" ID="btnClose" Text="Close" UseSubmitBehavior="false" OnClick="btnClose_Click"></asp:Button>                         
                        </td>
                    </tr>
                </tbody>
            </table>

        </form>
    </div>
</asp:Content>
