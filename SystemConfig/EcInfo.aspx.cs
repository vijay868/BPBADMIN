﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.SystemConfig
{
    public partial class EcInfo : PageBase
    {
        protected ExclusiveCustomerModel ECattributeDetails = new ExclusiveCustomerModel();
        protected string strDirLoc = "";
        protected int fkeyECAttribute = -1;
        protected int pkeyMessage = -2;
        protected int ECkioskKey;
        protected void Page_Load(object sender, EventArgs e)
        {
            ViewState["ECattributeKey"] = Request.QueryString["ECattributeKey"];
            if (!Page.IsPostBack)
            {
                GetData();
            }
        }

        private void GetData()
        {
            try
            {
                int ECattributeKey = Convert.ToInt32(ViewState["ECattributeKey"]);
                ExclusiveCustomerModel ECattributeDetails = ExclusiveCustomerService.GETECInfo(ECattributeKey, ConnString);
                ViewState["ECattributeDetails"] = ECattributeDetails;
                if (ECattributeDetails.ExclusiveCustomerInfo.Rows.Count > 0)
                {
                    ExclusiveCustomerModel.ExclusiveCustomerInfoRow row = (ExclusiveCustomerModel.ExclusiveCustomerInfoRow)ECattributeDetails.ExclusiveCustomerInfo.Rows[0];
                    fkeyECAttribute = row.fkey_ec_attribute;
                    ViewState["pkeyMessage"] = row.pkey_exc_customer;
                    pkeyMessage = Convert.ToInt32(ViewState["pkeyMessage"]);
                    txtCompanyName.Text = row.company_name;
                    txtCompanyID.Text = row.company_id;
                    txtKioskID.Text = row.kiosk_id;
                    lblImageDir.Text = GetDirectoryLoc(row.dir_loc, true, "ec");
                    lblLogoLoc.Text = Server.MapPath("..") + row.logo_loc;
                    lblBannerLoc.Text = Server.MapPath("..") + row.banner_loc;
                    lblMastheadLoc.Text = Server.MapPath("..") + row.masthead_loc;
                    hdnDirloc.Value = GetDirectoryLoc(row.dir_loc, false, "ec");
                    hdnbannerLoc.Value = row.banner_loc == "" ? "" : row.banner_loc;
                    hdnmastLoc.Value = row.masthead_loc == "" ? "" : row.masthead_loc;
                    hdnLogoLoc.Value = row.logo_loc == "" ? "" : row.logo_loc;
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        private string GetDirectoryLoc(string key, bool isFullPath, string fixedLoc)
        {
            ExclusiveCustomerModel ECattributeDetails = (ExclusiveCustomerModel)ViewState["ECattributeDetails"];
            try
            {
                if (ECattributeDetails.ExclusiveCustomerInfo.Rows.Count > 0)
                    strDirLoc = Convert.ToString(ECattributeDetails.ExclusiveCustomerInfo.Rows[0]["dir_loc"]);
                else
                    if (fixedLoc == string.Empty)
                        strDirLoc = "/images/" + fixedLoc;
                    else
                        strDirLoc = "/images/" + ECattributeDetails.AttributeInfo.Rows[0]["attribute_code"].ToString();
                if (isFullPath)
                    strDirLoc = UrlBase + strDirLoc;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strDirLoc;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            ExclusiveCustomerModel model = new ExclusiveCustomerModel();
            ExclusiveCustomerModel.ExclusiveCustomerInfoRow row = (ExclusiveCustomerModel.ExclusiveCustomerInfoRow)model.ExclusiveCustomerInfo.NewExclusiveCustomerInfoRow();
            row.pkey_exc_customer = Convert.ToInt32(ViewState["pkeyMessage"]);
            row.fkey_ec_attribute = Convert.ToInt32(ViewState["ECattributeKey"]);
            row.company_name = txtCompanyName.Text == "" ? "" : txtCompanyName.Text;
            row.company_id = txtCompanyID.Text == "" ? "" : txtCompanyID.Text;
            row.kiosk_id = txtKioskID.Text == "" ? "" : txtKioskID.Text;
            row.dir_loc = hdnDirloc.Value;
            row.logo_loc = hdnLogoLoc.Value;
            row.masthead_loc = hdnmastLoc.Value;
            row.banner_loc = hdnbannerLoc.Value;
            row.fkey_messages = hdnMsg.Value == "" ? "-1" : hdnMsg.Value;
            model.ExclusiveCustomerInfo.AddExclusiveCustomerInfoRow(row);
            try
            {
                pkeyMessage = ExclusiveCustomerService.UPDATEExclusiveCustomer(model, ConnString);
                GetData();
                //SaveBannerFile();
                //SaveMastheadFile();
                Response.Redirect("ECInfoList.aspx");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        //private void SaveMastheadFile()
        //{
        //    try{
        //        ExclusiveCustomerModel ECattributeDetails = (ExclusiveCustomerModel)ViewState["ECattributeDetails"];
        //        if(mastheadImgFile.PostedFile != null &&  mastheadImgFile.PostedFile.ContentLength > 0 )
        //        {
        //            string strDirectory = Server.MapPath("..") + GetDirectoryLoc("dir_loc", false, "ec");

        //            string strPath = Server.MapPath("..") + ECattributeDetails.ExclusiveCustomerInfo.Rows[0]["masthead_loc"];

        //            if(Directory.Exists(strDirectory))
        //            { 
        //                Directory.CreateDirectory(strDirectory);
        //            }

        //            strPath = strPath.Replace("\" , "/");
        //            if(File.Exists(strPath))
        //                File.Delete(strPath);

        //            mastheadImgFile.PostedFile.SaveAs(strPath);          
        //        }
        //    }
        //    catch(Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("ECInfoList.aspx");
        }
    }
}