﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="VariantDetails.aspx.cs" Inherits="Bestprintbuy.Admin.Web.SystemConfig.VariantDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function deleteVariant() {
            if ('<%=HasAccess("VariantList", "delete")%>' != 'False') {
                return confirm('All attached attributes will be deleted if the products are not defined, 	Are you sure?');
            }
            else {
                alert('User does not have access.');
                return false;
            }
        }
        function saveVariant() {
            selectMemberRoles();
            if ('<%=HasAccess("VariantList", variantkey != -2 ? "edit" : "add")%>' == 'False') {
                alert('User does not have access.');
                return false;
            }
            return true;
        }

        $().ready(function () {
            $("#frmVariantsDetails").validate();
        });

        function moveProductOptions(option, moveAll) {
            fromObject = document.getElementById("ContentPlaceHolder1_lbAvailableProductTypes");
            toObject = document.getElementById("ContentPlaceHolder1_lbApplicableProductTypes");

            if (option == "right") {
                from = eval(fromObject);
                to = eval(toObject);
            }
            else {
                from = eval(toObject);
                to = eval(fromObject);
            }

            if (from && to) {
                var fromSize = from.options.length;
                var toSize = to.options.length;
                var fromArray = new Array();
                var fromSelectedArray = new Array();
                var fromSelectedIndex = 0;
                var fromIndex = 0;
                for (i = fromSize - 1; i > -1; i--) {
                    var isSelected = false
                    if (from.options[i].selected)
                        isSelected = from.options[i].selected;
                    if ((isSelected && isSelected == true) || moveAll == true) {
                        fromSelectedArray[fromSelectedIndex] = new Array(from.options[i].value, from.options[i].text);
                        from.options[i] = null;
                        fromSelectedIndex++;
                    }
                    else {
                        fromArray[fromIndex] = new Array(from.options[i].value, from.options[i].text);
                        from.options[i] = null;
                        fromIndex++;
                    }
                }
                fromSize = fromArray.length;
                for (i = 0; i < fromSize; i++) {
                    from.options[i] = new Option(fromArray[fromSize - 1 - i][1], fromArray[fromSize - 1 - i][0]);
                }
                var newToSize = fromSelectedArray.length;
                var strData = "";
                for (i = 0; i < newToSize; i++) {
                    toSize = to.options.length;
                    to.options[toSize] = new Option(fromSelectedArray[i][1], fromSelectedArray[i][0]);
                    strData = strData + to.options[toSize].value;
                    if (i != newToSize - 1) strData = strData + ",";
                }
            }
        }

        function selectMemberRoles() {
            if (document.getElementById('ContentPlaceHolder1_lbApplicableProductTypes')) {
                var leng = document.getElementById('ContentPlaceHolder1_lbApplicableProductTypes').options.length;
                for (i = leng - 1; i > -1; i--) {
                    document.getElementById('ContentPlaceHolder1_lbApplicableProductTypes').options[i].selected = true;
                }
            }
        }
    </script>

    <div class="clr"></div>
    <!-- Container Starts -->
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">System Configuration</li>
            <li><a href="VariantList.aspx">Variant List</a></li>
            <li>Variant Details</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Variant Details</h2>
        <form runat="server" name="frmVariantsDetails" id="frmVariantsDetails">
            <table width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="15%" style="vertical-align: top;">
                            <label>Variant Code*</label>
                        </td>
                        <td width="20%" style="vertical-align: top;">
                            <asp:TextBox CssClass="text-box" name="txtVariantCode" ID="txtVariantCode" runat="server" required></asp:TextBox>
                        </td>
                        <td width="15%" style="vertical-align: top;" rowspan="3">
                            <label>Comments</label>
                        </td>
                        <td width="35%" style="vertical-align: top;" rowspan="3">
                            <asp:TextBox CssClass="text-box" name="txtComments" ID="txtComments" runat="server" Rows="5" Width="295px" style="resize:none;" TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">
                            <label>Variant Name*</label>
                        </td>
                        <td style="vertical-align: top;">
                            <asp:TextBox CssClass="text-box" name="txtVariantName" ID="txtVariantName" runat="server" required></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">
                            <label>Category*</label>
                        </td>
                        <td style="vertical-align: top;">
                            <asp:DropDownList name="ddlCategory" ID="ddlCategory" runat="server" CssClass="slect-box" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table width="100%" class="accounts-table">
                <thead>
                    <tr>
                        <th colspan="4" width="100%">Access Rights Information</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="15%">
                            <label></label>
                        </td>
                        <td width="50%">
                            <div style="width: 40%; float: left">
                                <label style="padding-left: 0px;"><b>Available Product Types</b></label>
                                <asp:ListBox ID="lbAvailableProductTypes" CssClass="list-box" runat="server" SelectionMode="Multiple"></asp:ListBox>
                            </div>
                            <div style="width: 10%; float: left; padding: 40px 10px 0 10px;">
                                <button id="to2" type="button" class="button rounded width53" onclick="javascript:moveProductOptions('right',false)">
                                    &nbsp;&gt;&nbsp;</button><br />
                                <button id="allTo2" type="button" class="button rounded width53" onclick="javascript:moveProductOptions('right',true)">
                                    &nbsp;&gt;&gt;&nbsp;</button><br />
                                <button id="to1" type="button" class="button rounded width53" onclick="javascript:moveProductOptions('left',false)">
                                    &nbsp;&lt;&nbsp;</button><br />
                                <button id="allTo1" type="button" class="button rounded width53" onclick="javascript:moveProductOptions('left',true)">
                                    &nbsp;&lt;&lt;&nbsp;</button>
                            </div>
                            <div style="width: 35%; float: left; padding-right: 10px;">
                                <label style="padding-left: 0px;"><b>Applicable Product Types</b></label>
                                <asp:ListBox ID="lbApplicableProductTypes" runat="server" CssClass="list-box" SelectionMode="Multiple"></asp:ListBox>
                            </div>
                        </td>
                        <td width="35%"></td>
                    </tr>
                </tbody>
            </table>

          <%--  <table width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="15%" style="float: left;">
                            <asp:Button runat="server" ID="btnDelete" Text="Delete" Visible="false" CssClass="button rounded" OnClientClick="if(!deleteVariant()) return false;" UseSubmitBehavior="false" OnClick="btnDelete_Click"></asp:Button>

                        </td>
                        <td width="17%">&nbsp;</td>
                        <td width="15%">&nbsp;</td>
                        <td width="35%" style="float: right;">
                            <asp:Button runat="server" ID="btnSave" Text="Save" OnClick="btnSave_Click" CssClass="button rounded" OnClientClick="return saveVariant();"></asp:Button>
                            <asp:Button runat="server" ID="btnClose" Text="Close" OnClick="btnClose_Click" CssClass="button rounded" UseSubmitBehavior="false"></asp:Button>
                        </td>
                    </tr>
                </tbody>
            </table>--%>
                 <div class="btn_lst" style="padding-top:20px">
                <div class="rgt" style="padding-right:8%">
                    <asp:Button runat="server" ID="btnClose" Text="Close" OnClick="btnClose_Click" CssClass="button rounded" UseSubmitBehavior="false"></asp:Button>
                    <asp:Button runat="server" ID="btnSave" Text="Save" OnClick="btnSave_Click" CssClass="button rounded" OnClientClick="return saveVariant();"></asp:Button>
                </div>
                 <div class="lft">
                     <asp:Button runat="server" ID="btnDelete" Text="Delete" Visible="false" CssClass="button rounded" OnClientClick="if(!deleteVariant()) return false;" UseSubmitBehavior="false" OnClick="btnDelete_Click"></asp:Button>
                </div>
            </div>
        </form>
    </div>
</asp:Content>
