﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.SystemConfig
{
    public partial class CategoryList : PageBase
    {
        protected int CategoryKey = -1;
        protected int masterKey = -1;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                GetMasterCategories();
                LoadData();
            }
        }

        private void LoadData()
        {
            try
            {
                if (Request.QueryString["mastercatKey"] != null && Request.QueryString["mastercatKey"] != "")
                    masterKey = Convert.ToInt32(Request.QueryString["mastercatKey"]);
                else
                    masterKey = Convert.ToInt32(ddlMasterCategory.SelectedValue);
                TypeModel model = SupportTypesService.GetData(CategoryKey, masterKey, ConnString);
                if (CategoryGrid.Attributes["SortExpression"] != null)
                    model.Category.DefaultView.Sort = CategoryGrid.Attributes["SortExpression"] + " " + CategoryGrid.Attributes["SortDirection"];
                lblTotalRecords.Text = string.Format(" Total Number of Categories: {0}", model.Category.Rows.Count);
                CategoryGrid.DataSource = model.Category;
                CategoryGrid.DataBind();
                ddlMasterCategory.SelectedValue = masterKey.ToString();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        private void GetMasterCategories()
        {
            try
            {
                ddlMasterCategory.DataSource = SupportTypesService.GetMasterCategories(ConnString);
                ddlMasterCategory.DataTextField = "mc_name";
                ddlMasterCategory.DataValueField = "pkey_master_category";
                ddlMasterCategory.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void ddlMasterCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                masterKey = Convert.ToInt32(ddlMasterCategory.SelectedValue);
                TypeModel model = SupportTypesService.GetData(CategoryKey, masterKey, ConnString);
                lblTotalRecords.Text = string.Format("Total Number of Categories: {0}", model.Category.Rows.Count);
                CategoryGrid.DataSource = model.Category;
                CategoryGrid.DataBind();
                ddlMasterCategory.SelectedValue = masterKey.ToString();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected int GetRecordCountForPage()
        {
            return GetRecordCountForPageSize(CategoryGrid.PageSize, CategoryGrid.PageIndex + 1);
        }

        protected void CategoryGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CategoryGrid.PageIndex = e.NewPageIndex;
            LoadData();
        }

        protected void CategoryGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            SortGridView(CategoryGrid, e);
            CategoryGrid.PageIndex = 0;
            LoadData();
        }


        protected void btnAdd_Click(object sender, EventArgs e)
        {
            masterKey = Convert.ToInt32(ddlMasterCategory.SelectedValue);
            Response.Redirect("CategoryDetails.aspx?categoryKey=-2&masterKey=" + masterKey);
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            bool flag = Convert.ToBoolean(Request.QueryString["edit"]);
            if (flag)
                Response.Redirect("MasterCategoryList.aspx");
            else
                Response.Redirect("../Account/DashBoard.aspx");
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {

            LinkButton lbtn = (LinkButton)sender;
            int pkeyCategory = Convert.ToInt32(lbtn.CommandArgument);
            masterKey = Convert.ToInt32(ddlMasterCategory.SelectedValue);
            try
            {
                bool success = SupportTypesService.daleteCategory(pkeyCategory, ConnString);
                if (success)
                {
                    Response.Redirect("CategoryList.aspx?mastercatKey=" + masterKey + "&edit=true");
                }
                else
                {
                    AlertScript("Delete Failed. Please try again");
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }
    }
}