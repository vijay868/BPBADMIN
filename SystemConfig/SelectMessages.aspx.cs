﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;

namespace Bestprintbuy.Admin.Web.SystemConfig
{
    public partial class SelectMessages : PageBase
    {
        protected CustMessageModel model = new CustMessageModel();
        protected string fields = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                GetData();
            }
        }

        private void GetData()
        {
            try
            {
                int fkey_ec_attribute = Convert.ToInt32(Request.QueryString["fkey_ec_attribute"]);
                model = ExclusiveCustomerService.getMessageList(-1, fkey_ec_attribute, 1, ConnString);
                rptrMessages.DataSource = model.CustMessageMaster;
                rptrMessages.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                string msgID = "";
                foreach (RepeaterItem item in rptrMessages.Items)
                {
                    HiddenField txtFiled1 = (HiddenField)item.FindControl("txtFiled1");
                    CheckBox chkbox = (CheckBox)item.FindControl("chkSelect");
                    fields = Request.QueryString["fields"];
                    if (chkbox.Checked == true)
                    {
                        msgID += ((msgID == "") ? "" : ",") + txtFiled1.Value;
                    }
                }
                string script = string.Format("SetDataAndClose('{0}','{1}','{2}');", false.ToString().ToLower(), msgID, fields);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "jscript", script, true);
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }
    }
}