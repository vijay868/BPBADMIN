﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.SystemConfig
{
    public partial class AttributeDetails : PageBase
    {
        protected int attributekey;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ViewState["attributeKey"] = Convert.ToInt32(Request.QueryString["attributeKey"]);
                ViewState["variantKey"] = Convert.ToInt32(Request.QueryString["variantKey"]);
                attributekey = Convert.ToInt32(ViewState["attributeKey"]);
                //GetVariantsData();
                GetData();
            }
        }

        private void GetData()
        {
            try
            {
                int variantKey = Convert.ToInt32(ViewState["variantKey"]);
                int attributeKey = Convert.ToInt32(ViewState["attributeKey"]);
                ProductModel attributeDetails = ProductService.GetAttributeList(attributeKey, variantKey, ConnString);
                ProductModel variants = ProductService.GetVariantList(ConnString);
                if (attributeDetails.Attributes.Rows.Count > 0)
                {
                    ProductModel.AttributesRow attributeRow = (ProductModel.AttributesRow)attributeDetails.Attributes.Rows[0];
                    txtAttributeCode.Text = attributeRow.attribute_code;
                    txtAttributeName.Text = attributeRow.attribute_value;
                    txtComments.Text = attributeRow.comments;
                    foreach (ProductModel.VariantsRow row in variants.Variants)
                    {
                        if (row.pkey_variant == variantKey)
                            lblVariants.Text = row.variant_name;
                    }
                    lblVariants.Visible = true;
                    btnAdd.Visible = true;
                    btnDelete.Visible = true;
                }
                else
                {
                    ddlCategory.Visible = true;
                    GetVariantsData();
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }            
        }

        private void GetVariantsData()
        {
            try
            {
                ProductModel variants = ProductService.GetVariantList(ConnString);
                ddlCategory.DataSource = variants.Variants;
                ddlCategory.DataTextField = "variant_name";
                ddlCategory.DataValueField = "pkey_variant";
                ddlCategory.DataBind();
            }
            catch (Exception ex)
            {
                AlertScript(ex.Message);
                FileLogger.WriteException(ex);
            }           
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            ddlCategory.Visible = true;
            lblVariants.Visible = false;
            txtAttributeCode.Text = "";
            txtAttributeName.Text = "";
            txtComments.Text = "";
            Response.Redirect("AttributeDetails.aspx?attributeKey=-2");
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int attributeKey = Convert.ToInt32(Request.QueryString["attributeKey"]);
                ProductService.DeleteAttribute(attributeKey, ConnString);
                Response.Redirect("AttributeList.aspx");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                if (ex.Message.ToLower().IndexOf("fk_pps_pro_price_variant_xref_pps_product_variants") > -1)
                {
                    AlertScript("Cannot delete record");
                }
                else
                    AlertScript(ex.Message);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int attributeKey = Convert.ToInt32(ViewState["attributeKey"]);
                int variantKey = Convert.ToInt32(ViewState["variantKey"]);
                ProductModel attributeDetail = new ProductModel();
                ProductModel.AttributesRow attribute = attributeDetail.Attributes.NewAttributesRow();
                attribute.pkey_attribute = attributeKey;
                attribute.attribute_code = txtAttributeCode.Text;
                attribute.attribute_value = txtAttributeName.Text;
                attribute.comments = txtComments.Text;
                if (attributeKey != -2)
                {
                    attribute.fkey_variant = variantKey;
                }
                else
                {
                    attribute.fkey_variant = Convert.ToInt32(ddlCategory.SelectedValue);
                }
                attribute.fkey_user = Identity.UserPK;
                attributeDetail.Attributes.AddAttributesRow(attribute);
                attributeKey = ProductService.UpdateAttribute(attributeDetail, ConnString);
                ViewState["attributeKey"] = attributeKey;
                AlertScript("The attribute added is saved, Please continue.");
                GetData();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                if (ex.Message.ToLower().IndexOf("duplicate") > -1)
                    AlertScript("An Attribute already exists for selected variant, please check");
                else
                    AlertScript("Save/Update failed. try again.");
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("AttributeList.aspx");
        }
    }
}