﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.SystemConfig
{
    public partial class CustMessagesList : PageBase
    {
        protected int recordCount = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadData();
            }
        }

        private void LoadData()
        {
            try
            {
                int catKey = Convert.ToInt32(WebConfiguration.ProductTypeKeys["Templates By Business"]);
                CustMessageModel model = ProductService.GetMsgData(-1, catKey, ConnString);
                if (MessagesListGrid.Attributes["SortExpression"] != null)
                    model.CustMessageMaster.DefaultView.Sort = MessagesListGrid.Attributes["SortExpression"] + " " + MessagesListGrid.Attributes["SortDirection"];
                recordCount = model.CustMessageMaster.Rows.Count;
                MessagesListGrid.DataSource = model.CustMessageMaster.DefaultView;
                MessagesListGrid.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void MessagesListGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            SortGridView(MessagesListGrid, e);
            MessagesListGrid.PageIndex = 0;
            LoadData();
        }

        protected void MessagesListGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            MessagesListGrid.PageIndex = e.NewPageIndex;
            LoadData();
        }

        protected int GetRecordCountForPage()
        {
            return GetRecordCountForPageSize(MessagesListGrid.PageSize, MessagesListGrid.PageIndex + 1);
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("CustMessageDetail.aspx?Pkey=-2");
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/DashBoard.aspx");
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbtn = (LinkButton)sender;
                int key = Convert.ToInt32(lbtn.CommandArgument);
                ProductService.DeleteMessage(key, ConnString);
                LoadData();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }
    }
}