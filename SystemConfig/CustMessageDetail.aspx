﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="CustMessageDetail.aspx.cs" Inherits="Bestprintbuy.Admin.Web.SystemConfig.CustMessageDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
        function moveMatchProductOptions(option, moveAll) {
            fromObject = document.getElementById("ContentPlaceHolder1_lbAvailableMatchProducts");
            toObject = document.getElementById("ContentPlaceHolder1_lbAssignedMatchProducts");

            if (option == "right") {
                from = eval(fromObject);
                to = eval(toObject);
            }
            else {
                from = eval(toObject);
                to = eval(fromObject);
            }

            if (from && to) {
                var fromSize = from.options.length;
                var toSize = to.options.length;
                var fromArray = new Array();
                var fromSelectedArray = new Array();
                var fromSelectedIndex = 0;
                var fromIndex = 0;
                for (i = fromSize - 1; i > -1; i--) {
                    var isSelected = false
                    if (from.options[i].selected)
                        isSelected = from.options[i].selected;
                    if ((isSelected && isSelected == true) || moveAll == true) {
                        fromSelectedArray[fromSelectedIndex] = new Array(from.options[i].value, from.options[i].text);
                        from.options[i] = null;
                        fromSelectedIndex++;
                    }
                    else {
                        fromArray[fromIndex] = new Array(from.options[i].value, from.options[i].text);
                        from.options[i] = null;
                        fromIndex++;
                    }
                }
                fromSize = fromArray.length;
                for (i = 0; i < fromSize; i++) {
                    from.options[i] = new Option(fromArray[fromSize - 1 - i][1], fromArray[fromSize - 1 - i][0]);
                }
                var newToSize = fromSelectedArray.length;
                var strData = "";
                for (i = 0; i < newToSize; i++) {
                    toSize = to.options.length;
                    to.options[toSize] = new Option(fromSelectedArray[i][1], fromSelectedArray[i][0]);
                    strData = strData + to.options[toSize].value;
                    if (i != newToSize - 1) strData = strData + ",";
                }
            }
        }

        $().ready(function () {
            $("#frmCustMessageDetail").validate();
        });

        function selectMemberRoles() {
            if (document.getElementById('ContentPlaceHolder1_lbAssignedMatchProducts')) {
                var leng = document.getElementById('ContentPlaceHolder1_lbAssignedMatchProducts').options.length;
                for (i = leng - 1; i > -1; i--) {
                    document.getElementById('ContentPlaceHolder1_lbAssignedMatchProducts').options[i].selected = true;
                }
            }
            return true;
        }

    </script>
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">System Configuration</li>
            <li><a href="CustMessagesList.aspx">Messages List</a></li>
            <li>Message Details</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Message Details</h2>
        <form id="frmCustMessageDetail" name="frmCustMessageDetail" runat="server">
            <table cellpadding="0" cellspacing="0" border="0" width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="15%">
                            <label>Message ID*</label></td>
                        <td width="20%">
                            <asp:TextBox CssClass="text-box" name="txtMessageID" ID="txtMessageID" runat="server" required></asp:TextBox>
                        </td>
                        <td width="15%" style="vertical-align: top" rowspan="3">
                            <label>Message*</label>
                        </td>
                        <td width="35%" rowspan="3">
                            <asp:TextBox CssClass="text-box" name="txtMessage" ID="txtMessage" runat="server" style="resize:none;" TextMode="MultiLine" Rows="8" Width="295px" required></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Category</label></td>
                        <td>
                            <asp:DropDownList name="ddlCategory" ID="ddlCategory" CssClass="slect-box" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label></label>
                        </td>
                        <td>
                            <asp:CheckBox ID="chkIsExclusive" runat="server" CssClass="check-box" /><asp:Label ID="Label1" Text="   Is Message applicable for KIOSK / Exclusive Customer" runat="server" />
                        </td>
                    </tr>
                </tbody>
            </table>
            <table width="100%" class="accounts-table">
                <thead>
                    <tr>
                        <th colspan="4" width="100%">Define Matching Products for Messages</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="15%">
                            <label></label>
                        </td>
                        <td width="50%">
                            <div style="width: 40%; float: left">
                                <label style="padding-left: 0px;"><b>Available Product Types </b></label>
                                <asp:ListBox ID="lbAvailableMatchProducts" CssClass="list-box" runat="server" SelectionMode="Multiple"></asp:ListBox>
                            </div>
                            <div style="width: 10%; float: left; padding: 40px 10px 0 10px;">
                                <button id="to2" type="button" class="button rounded width53" onclick="javascript:moveMatchProductOptions('right',false)">
                                    &nbsp;&gt;&nbsp;</button><br />
                                <button id="allTo2" type="button" class="button rounded width53" onclick="javascript:moveMatchProductOptions('right',true)">
                                    &nbsp;&gt;&gt;&nbsp;</button><br />
                                <button id="to1" type="button" class="button rounded width53" onclick="javascript:moveMatchProductOptions('left',false)">
                                    &nbsp;&lt;&nbsp;</button><br />
                                <button id="allTo1" type="button" class="button rounded width53" onclick="javascript:moveMatchProductOptions('left',true)">
                                    &nbsp;&lt;&lt;&nbsp;</button>
                            </div>
                            <div style="width: 35%; float: left; padding-right: 10px;">
                                <label style="padding-left: 0px;"><b>Applicable Product Types </b></label>
                                <asp:ListBox ID="lbAssignedMatchProducts" runat="server" CssClass="list-box" SelectionMode="Multiple"></asp:ListBox>
                            </div>
                        </td>
                         <td width="35%"></td>
                    </tr>
                    <tr>
                        <td colspan="4" class="rgt" align="right">
                            <asp:Button runat="server" ID="btnSave" CssClass="button rounded" Text="Save" OnClientClick="return selectMemberRoles();" OnClick="btnSave_Click"></asp:Button>
                            <asp:Button runat="server" CssClass="button rounded close-btn" ID="btnClose" Text="Close" UseSubmitBehavior="false" OnClick="btnClose_Click"></asp:Button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
</asp:Content>
