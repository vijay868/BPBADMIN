﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="RevTemplateDetail.aspx.cs" Inherits="Bestprintbuy.Admin.Web.SystemConfig.RevTemplateDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function saveTemplate() {
            if ('<%=HasAccess("TemplateList",  pkey_reverse_templates != -2 ? "edit" :"add")%>' == 'False') {
                alert('User does not have access.');
                return false;
            }
            return true;
        }

        $().ready(function () {
            $("#frmRevTemplateDetails").validate();
        });
    </script>
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
             <li class="liColor">System Configuration</li>
            <li><a href="RevTemplateList.aspx">Reverse Template List</a></li>
            <li>Reverse Template Details</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Reverse Template Details</h2>
        <form id="frmRevTemplateDetails" name="frmRevTemplateDetails" runat="server">
            <table cellpadding="0" cellspacing="0" border="0" width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="15%">
                            <label>Template Name*</label></td>
                        <td width="20%">
                            <asp:TextBox CssClass="text-box" name="txtTemplateName" ID="txtTemplateName" runat="server" required></asp:TextBox>
                        </td>
                        <td width="15%" style="vertical-align: top">
                            <label>Product Category*</label>
                        </td>
                        <td width="35%">
                            <asp:Label ID="lblProductCategory" Visible="false" runat="server" />
                            <asp:HiddenField ID="cboProdCategory" runat="server" />
                            <asp:DropDownList name="ddlProductCategory" ID="ddlProductCategory" CssClass="slect-box" Visible="false" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlProductCategory_SelectedIndexChanged" required>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Product Type*</label></td>
                        <td>
                            <asp:Label ID="lblProductType" Visible="false" runat="server" />
                            <asp:HiddenField ID="cboProductType" runat="server" />
                            <asp:DropDownList name="ddlProductType" ID="ddlProductType" CssClass="slect-box" Visible="false" runat="server" required>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <label>Template Type</label>
                        </td>
                        <td>
                            <asp:DropDownList name="ddlTemplateType" ID="ddlTemplateType" CssClass="slect-box" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Back Color</label>
                        </td>
                        <td>
                            <asp:DropDownList name="ddlBackColor" ID="ddlBackColor" CssClass="slect-box" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <label>Paper Type*</label>
                        </td>
                        <td>
                            <asp:DropDownList name="ddlPaperType" ID="ddlPaperType" CssClass="slect-box" runat="server" required>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Paper Size*</label>
                        </td>
                        <td>
                            <asp:DropDownList name="ddlPaperSize" ID="ddlPaperSize" CssClass="slect-box" runat="server" required>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <label>Card Format</label>
                        </td>
                        <td>
                            <asp:DropDownList name="ddlCardFormat" ID="ddlCardFormat" CssClass="slect-box" runat="server">
                                <asp:ListItem Value="-1">No Format</asp:ListItem>
                                <asp:ListItem Value="1">Format 1</asp:ListItem>
                                <asp:ListItem Value="2">Format 2</asp:ListItem>
                                <asp:ListItem Value="3">Format 3</asp:ListItem>
                                <asp:ListItem Value="4">Format 4</asp:ListItem>
                                <asp:ListItem Value="5">Format 5</asp:ListItem>
                                <asp:ListItem Value="6">Format 6</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label></label>
                        </td>
                        <td>
                            <asp:CheckBox ID="chkAlltimeFavourites" runat="server" CssClass="check-box" /><asp:Label Text="     Include in All Time Favourites." runat="server" />
                        </td>
                        <td>
                            <label></label>
                        </td>
                        <td>
                            <asp:CheckBox ID="chkhasBackText" runat="server" CssClass="check-box" /><asp:Label Text="   Back with customizable text." runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label></label>
                        </td>
                        <td>
                            <asp:CheckBox ID="chkTempeligble" runat="server" CssClass="check-box" /><asp:Label Text="   Template Eligble for Free." runat="server" />
                            <asp:HiddenField ID="chkIsCustomisable" runat="server" />
                        </td>
                        <td>
                            <label>Template Location*</label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtTemplateLocation" ID="txtTemplateLocation" runat="server" required></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Thumb Nail*</label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtThumbnailUrl" ID="txtThumbnailUrl" runat="server" required></asp:TextBox>
                        </td>
                        <td>

                        </td>
                        <td class="rgt" align="right">
                            <asp:Button runat="server" ID="btnSave" CssClass="button rounded" Text="Save" OnClientClick="return saveTemplate();" OnClick="btnSave_Click"></asp:Button>
                            <asp:Button runat="server" CssClass="button rounded close-btn" ID="btnClose" Text="Close" UseSubmitBehavior="false" OnClick="btnClose_Click"></asp:Button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
</asp:Content>
