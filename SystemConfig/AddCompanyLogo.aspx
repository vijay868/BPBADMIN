﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="AddCompanyLogo.aspx.cs" Inherits="Bestprintbuy.Admin.Web.SystemConfig.AddCompanyLogo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        $(function () {
            $("#AddCompanyLogo").validate();
        });
    </script>
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">System Configuration</li>
            <li><a href="KiosksCompanyList.aspx">Kiosks Company List</a></li>
            <li>Add Company Logo</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Add Company Logo</h2>
        <form id="AddCompanyLogo" name="AddCompanyLogo" runat="server">
            <table cellpadding="0" cellspacing="0" border="0" width="100%" class="accounts-table">

                <tbody>
                    <tr>
                        <td width="15%">
                            <label>Company Name*</label></td>
                        <td width="20%">
                            <asp:TextBox name="txtCompanyName" ID="txtCompanyName" CssClass="text-box" runat="server" required></asp:TextBox>
                        </td>
                        <td width="15%">
                            <label>State </label>
                        </td>
                        <td width="35%">
                            <asp:DropDownList name="ddlState" ID="ddlState" CssClass="slect-box" runat="server">
                                <asp:ListItem Value=""></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Company ID*</label></td>
                        <td>
                            <asp:TextBox name="txtCompanyID" ID="txtCompanyID" CssClass="text-box" runat="server" required></asp:TextBox>
                        </td>
                        <td>
                            <label>Kiosk </label>
                        </td>
                        <td>
                            <asp:DropDownList name="ddlkiosks" ID="ddlkiosks" CssClass="slect-box" runat="server">
                                <asp:ListItem Value="173">EXIT</asp:ListItem>
                                <asp:ListItem Value="175">PRU</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Logo Url*</label></td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtLogoUrl" ID="txtLogoUrl" runat="server" required></asp:TextBox>
                        </td>
                        <td>
                            <label></label>
                        </td>
                        <td>
                            <div style="float:right; padding-right:24%;">
                            <asp:Button runat="server" ID="btnSave" CssClass="button rounded" Text="Save" OnClick="btnSave_Click"></asp:Button>
                            <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClick="btnClose_Click" UseSubmitBehavior="false"></asp:Button>
                        </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
</asp:Content>
