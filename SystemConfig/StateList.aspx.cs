﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.SystemConfig
{
    public partial class StateList : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                GetStates();
                LoadData();
            }
        }

        private void GetStates()
        {
            try
            {
                int countrykey = -1;
                CountryModel country = CountryService.GetList(countrykey, ConnString);
                ddlState.DataSource = country.Countries;
                ddlState.DataTextField = "country_name";
                ddlState.DataValueField = "pkey_country";
                ddlState.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        private void LoadData()
        {
            try
            {
                int statekey = -1;
                int countrykey = Convert.ToInt32(ddlState.SelectedValue);
                CountryModel states_Data = CountryService.GetData(statekey, countrykey, ConnString);
                if (StatesGrid.Attributes["SortExpression"] != null)
                    states_Data.States.DefaultView.Sort = StatesGrid.Attributes["SortExpression"] + " " + StatesGrid.Attributes["SortDirection"];
                lblTotalRecords.Text = string.Format("Total Number of States :{0} ", states_Data.States.Rows.Count);
                StatesGrid.DataSource = states_Data.States;
                StatesGrid.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnAddState_Click(object sender, EventArgs e)
        {
            Response.Redirect("StateDetails.aspx?pkeyState=-2&countrykey=" + ddlState.SelectedValue);
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/DashBoard.aspx");
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbtn = (LinkButton)sender;
                int pkeyState = Convert.ToInt32(lbtn.CommandArgument);
                CountryService.DeleteState(pkeyState, ConnString);
                Response.Redirect("StateList.aspx");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        protected int GetRecordCountForPage()
        {
            return GetRecordCountForPageSize(StatesGrid.PageSize, StatesGrid.PageIndex + 1);
        }

        protected void StatesGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                StatesGrid.PageIndex = e.NewPageIndex;
                LoadData();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void StatesGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                SortGridView(StatesGrid, e);
                StatesGrid.PageIndex = 0;
                LoadData();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }
        protected void btnStateAbbr_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbtn = (LinkButton)sender;
                int pkeytype = Convert.ToInt32(lbtn.CommandArgument);
                Response.Redirect("StateDetails.aspx?pkeyState=" + pkeytype + "&countrykey=" + ddlState.SelectedValue);
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnStateName_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbtn = (LinkButton)sender;
                int pkeytype = Convert.ToInt32(lbtn.CommandArgument);
                Response.Redirect("StateDetails.aspx?pkeyState=" + pkeytype + "&countrykey=" + ddlState.SelectedValue);
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }
    }
}