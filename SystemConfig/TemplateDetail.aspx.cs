﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace Bestprintbuy.Admin.Web.SystemConfig
{
    public partial class TemplateDetail : PageBase
    {
        protected int pkeyTemplate = 0;
        protected int fkey_product_type = -1;
        protected int themeCategoryKey = Convert.ToInt32(WebConfiguration.ProductTypeKeys["Templates By Theme"]);
        protected int byCategoryCatKey = Convert.ToInt32(WebConfiguration.ProductTypeKeys["Templates By Category"]);
        protected string templateTypes = "-2";
        protected string category = "-2";
        protected int ExclusiveCustomerKey = Convert.ToInt32(WebConfiguration.ProductVariants["ExclusiveCustomer"]);

        protected void Page_Load(object sender, EventArgs e)
        {
            pkeyTemplate = Convert.ToInt32(Request.QueryString["pkeyTemplate"]);
            fkey_product_type = Convert.ToInt32(Request.QueryString["fkeyProductType"]);
            if (!Page.IsPostBack)
            {
                GetData();
            }
        }

        private void GetData()
        {
            try
            {
                pkeyTemplate = Convert.ToInt32(Request.QueryString["pkeyTemplate"]);

                if (pkeyTemplate == -2)
                {
                    category = ddlTemplateIdentity.SelectedValue == "" ? "270" : ddlTemplateIdentity.SelectedValue;
                    fkey_product_type = Convert.ToInt32(ddlProductType.SelectedValue == "" ? "-1" : ddlProductType.SelectedValue);
                }

                ProductTemplateModel templateDetails = TemplateService.GetTemplateDetails(pkeyTemplate, templateTypes, category, themeCategoryKey, byCategoryCatKey, ExclusiveCustomerKey, fkey_product_type, ConnString);

                lbAvailableProductTypes.DataSource = templateDetails.AvailableTemplateTypes;
                lbAvailableProductTypes.DataTextField = "type_name";
                lbAvailableProductTypes.DataValueField = "pkey_type";
                lbAvailableProductTypes.SelectionMode = ListSelectionMode.Multiple;
                lbAvailableProductTypes.DataBind();

                lbAssignedProductTypes.DataSource = templateDetails.ApplicableTemplateTypes;
                lbAssignedProductTypes.DataTextField = "type_name";
                lbAssignedProductTypes.DataValueField = "pkey_type";
                lbAssignedProductTypes.SelectionMode = ListSelectionMode.Multiple;
                lbAssignedProductTypes.DataBind();

                if (templateDetails.ProductAttributes.Rows.Count > 0)
                {
                    ddlExclusiveCustomer.DataSource = templateDetails.ProductAttributes;
                    ddlExclusiveCustomer.DataTextField = "attribute_value";
                    ddlExclusiveCustomer.DataValueField = "pkey_attribute";
                    ddlExclusiveCustomer.DataBind();
                    ddlExclusiveCustomer.Items.Insert(0, new ListItem("No Option", "-1"));
                }
                if (templateDetails.TemplateQualifier.Rows.Count > 0)
                {
                    ddlAddQuailifier.DataSource = templateDetails.TemplateQualifier;
                    ddlAddQuailifier.DataTextField = "ct_name";
                    ddlAddQuailifier.DataValueField = "pkey_category_type";
                    ddlAddQuailifier.DataBind();
                    ddlAddQuailifier.Items.Insert(0, new ListItem("No Option", "-1"));
                }
                if (templateDetails.TemplateCategories.Rows.Count > 0)
                {
                    ddlTemplateIdentity.DataSource = templateDetails.TemplateCategories;
                    ddlTemplateIdentity.DataTextField = "ct_name";
                    ddlTemplateIdentity.DataValueField = "pkey_category_type";
                    ddlTemplateIdentity.DataBind();
                }
                if (templateDetails.ThemeTypes.Rows.Count > 0)
                {
                    var catType = from collect in templateDetails.ThemeTypes
                                  where collect.fkey_self == byCategoryCatKey
                                  select collect;
                    ddlByCategory.DataSource = catType.ToList();
                    ddlByCategory.DataTextField = "ct_name";
                    ddlByCategory.DataValueField = "pkey_category_type";
                    ddlByCategory.DataBind();
                    ddlByCategory.Items.Insert(0, new ListItem("No Option", "-1"));
                }
                if (templateDetails.ThemeTypes.Rows.Count > 0)
                {
                    var catType = from collect in templateDetails.ThemeTypes
                                  where collect.fkey_self == themeCategoryKey
                                  select collect;
                    ddlByTheme.DataSource = catType.ToList();
                    ddlByTheme.DataTextField = "ct_name";
                    ddlByTheme.DataValueField = "pkey_category_type";
                    ddlByTheme.DataBind();
                    ddlByTheme.Items.Insert(0, new ListItem("No Option", "-1"));
                }

                if (templateDetails.TemplateProducts.Rows.Count > 0)
                {
                    string ProdType = "";
                    string Prodids = "";
                    string ProdCat = "";
                    string ProdNames = "";
                    foreach (ProductTemplateModel.TemplateProductsRow row in templateDetails.TemplateProducts)
                    {
                        ProdType += row.product_id + "\r\n";
                        Prodids += row.pkey_product.ToString() + ",";
                        ProdCat += row.pkey_template_product_xref.ToString() + ",";
                        ProdNames += row.product_id + "^";
                    }
                    txtAssignedProducts.Text = ProdType;
                    hdnProductIds.Value = Prodids.TrimEnd(',');
                    hdnProductCat.Value = ProdCat.TrimEnd(',');
                    hdnProductNames.Value = ProdNames.TrimEnd('^');
                }

                if (templateDetails.Templates.Rows.Count > 0)
                {
                    ProductTemplateModel.TemplatesRow TemplateRow = (ProductTemplateModel.TemplatesRow)templateDetails.Templates.Rows[0];
                    txtTemplateName.Text = TemplateRow.template_name;
                    lblProductCategory.Text = TemplateRow.product_category_name;
                    lblProductType.Text = TemplateRow.product_category_type;
                    hdnProdType.Value = TemplateRow.fkey_product_type.ToString();
                    hdnProdCat.Value = TemplateRow.fkey_product_category.ToString();
                    hdnTemplatesCategory.Value = TemplateRow.fkey_product_type.ToString();
                    lblProductType.Visible = true;
                    lblProductCategory.Visible = true;
                    btnDelete.Visible = true;

                    txtTemplateLocation.Text = TemplateRow.template_url;
                    txtThumbnailUrl.Text = TemplateRow.thumbnail_url;
                    ddlByCategory.SelectedValue = TemplateRow.Isfkey_by_category_typeNull() ? "-1" : TemplateRow.fkey_by_category_type.ToString();
                    ddlByTheme.SelectedValue = TemplateRow.Isfkey_theme_typeNull() ? "-1" : TemplateRow.fkey_theme_type.ToString();
                    ddlAddQuailifier.SelectedValue = TemplateRow.Isfkey_template_qualifierNull() ? "-1" : TemplateRow.fkey_template_qualifier.ToString();
                    ddlExclusiveCustomer.SelectedValue = TemplateRow.Isfkey_ec_attributeNull() ? "-1" : TemplateRow.fkey_ec_attribute.ToString();
                    ddlTemplateIdentity.SelectedValue = TemplateRow.Isproduct_category_typeNull() ? "-1" : TemplateRow.product_category_type.ToString();
                    chkHasDesig.Checked = TemplateRow.has_desgination_logos;
                    chkTempeligble.Checked = TemplateRow.is_free_eligible;
                    chkAlltimeFavourites.Checked = TemplateRow.is_favourite;
                    btnDelete.Visible = true;
                }
                else
                {
                    ddlProductType.Visible = true;
                    ddlProductCategory.Visible = true;
                    if (templateDetails.ProductCategories.Rows.Count > 0)
                    {
                        ddlProductCategory.DataSource = templateDetails.ProductCategories;
                        ddlProductCategory.DataTextField = "ct_name";
                        ddlProductCategory.DataValueField = "pkey_category_type";
                        ddlProductCategory.DataBind();
                    }
                    if (templateDetails.ProductTypes.Rows.Count > 0)
                    {
                        ViewState["ProductTypes"] = templateDetails.ProductTypes;
                        ddlProductType.DataSource = templateDetails.ProductTypes;
                        ddlProductType.DataTextField = "ct_name";
                        ddlProductType.DataValueField = "pkey_category_type";
                        ddlProductType.DataBind();
                    }
                    ddlByCategory.Items.Insert(0, new ListItem("No Option", "-1"));
                    ddlByTheme.Items.Insert(0, new ListItem("No Option", "-1"));
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("TemplateList.aspx");
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int pkeyTemplate = Convert.ToInt32(Request.QueryString["pkeyTemplate"]);
                ProductTemplateModel TemplatesDetails = new ProductTemplateModel();
                ProductTemplateModel.TemplatesRow templateRow = TemplatesDetails.Templates.NewTemplatesRow();
                templateRow.pkey_template_master = pkeyTemplate;
                if (pkeyTemplate != -2)
                {
                    templateRow.fkey_product_type = Convert.ToInt32(hdnProdType.Value);
                    templateRow.fkey_product_category = Convert.ToInt32(hdnProdCat.Value);
                }
                else
                {

                    templateRow.fkey_product_type = Convert.ToInt32(ddlProductType.SelectedValue);
                    templateRow.fkey_product_category = Convert.ToInt32(ddlProductCategory.SelectedValue);
                }
                templateRow.is_favourite = chkAlltimeFavourites.Checked;
                templateRow.is_free_eligible = chkTempeligble.Checked;
                templateRow.has_desgination_logos = chkHasDesig.Checked;
                templateRow.template_name = txtTemplateName.Text;
                templateRow.template_url = txtTemplateLocation.Text;
                templateRow.thumbnail_url = txtTemplateLocation.Text; //txtThumbnailUrl.Text;
                templateRow.fkey_user = Identity.UserPK;
                templateRow.foreground_color = -1;   //To do
                templateRow.background_color = -1;
                templateRow.fkey_theme_type = Convert.ToInt32(ddlByTheme.SelectedValue);
                templateRow.fkey_by_category_type = Convert.ToInt32(ddlByCategory.SelectedValue);
                templateRow.fkey_template_qualifier = Convert.ToInt32(ddlAddQuailifier.SelectedValue);
                templateRow.fkey_ec_attribute = Convert.ToInt32(ddlExclusiveCustomer.SelectedValue);
                templateRow.card_orientation = Convert.ToInt32(ddlCardOrient.SelectedValue);
                templateRow.product_category_type = ddlTemplateIdentity.SelectedValue;
                templateRow.product_category_name = ddlTemplateIdentity.SelectedItem.Text;
                TemplatesDetails.Templates.AddTemplatesRow(templateRow);

                string AssignedProductTypes = "";
                if (Request["ctl00$ContentPlaceHolder1$lbAssignedProductTypes"] != null)
                    AssignedProductTypes = Request["ctl00$ContentPlaceHolder1$lbAssignedProductTypes"].ToString();

                if (AssignedProductTypes != "")
                {
                    foreach (string item in AssignedProductTypes.Split(','))
                    {
                        ProductTemplateModel.ApplicableTemplateTypesRow applicableTemplateTypesRow = TemplatesDetails.ApplicableTemplateTypes.NewApplicableTemplateTypesRow();
                        applicableTemplateTypesRow.pkey_type = Convert.ToInt32(item);
                        TemplatesDetails.ApplicableTemplateTypes.AddApplicableTemplateTypesRow(applicableTemplateTypesRow);
                    }
                }

                if (hdnProductIds.Value != "")
                {
                    string[] productIds = hdnProductIds.Value.Split(",".ToCharArray());
                    string[] productCat = hdnProductCat.Value.Split(",".ToCharArray());
                    string[] productNames = hdnProductNames.Value.Split("^".ToCharArray());
                    for (int i = 0; i < productIds.Length; i++)
                    {
                        ProductTemplateModel.TemplateProductsRow templateProductRow = TemplatesDetails.TemplateProducts.NewTemplateProductsRow();
                        templateProductRow.pkey_product = Convert.ToInt32(productIds[i]);
                        templateProductRow.pkey_template_product_xref = Convert.ToInt32(productCat[i]);
                        templateProductRow.product_id = productNames[i];

                        TemplatesDetails.TemplateProducts.AddTemplateProductsRow(templateProductRow);
                    }
                }

                pkeyTemplate = TemplateService.UpdateTemplates(TemplatesDetails, ConnString);
                ViewState["pkeyTemplate"] = pkeyTemplate;
                Response.Redirect("TemplateList.aspx");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript("Template Details Save/Update failed. try again.");
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                pkeyTemplate = Convert.ToInt32(Request.QueryString["pkeyTemplate"]);
                TemplateService.DeleteTemplate(pkeyTemplate, false, ConnString);
                Response.Redirect("TemplateList.aspx");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void ddlProductType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ddlByTheme.Items.Clear();
                ddlByCategory.Items.Clear();
                templateTypes = "-2";  // ddlProductType.SelectedValue.ToString();
                category = ddlTemplateIdentity.SelectedValue == "" ? "270" : ddlTemplateIdentity.SelectedValue;
                fkey_product_type = Convert.ToInt32(ddlProductType.SelectedValue == "" ? "-1" : ddlProductType.SelectedValue);

                ProductTemplateModel templateDetails = TemplateService.GetTemplateDetails(pkeyTemplate, templateTypes, category, themeCategoryKey, byCategoryCatKey, ExclusiveCustomerKey, fkey_product_type, ConnString);

                if (templateDetails.ThemeTypes.Rows.Count > 0)
                {
                    var catType = from collect in templateDetails.ThemeTypes
                                  where collect.fkey_self == byCategoryCatKey
                                  select collect;
                    ddlByCategory.DataSource = catType.ToList();
                    ddlByCategory.DataTextField = "ct_name";
                    ddlByCategory.DataValueField = "pkey_category_type";
                    ddlByCategory.DataBind();
                }
                if (templateDetails.ThemeTypes.Rows.Count > 0)
                {
                    var catType = from collect in templateDetails.ThemeTypes
                                  where collect.fkey_self == themeCategoryKey
                                  select collect;
                    ddlByTheme.DataSource = catType.ToList();
                    ddlByTheme.DataTextField = "ct_name";
                    ddlByTheme.DataValueField = "pkey_category_type";
                    ddlByTheme.DataBind();
                }
                lbAvailableProductTypes.DataSource = templateDetails.AvailableTemplateTypes;
                lbAvailableProductTypes.DataTextField = "type_name";
                lbAvailableProductTypes.DataValueField = "pkey_type";
                lbAvailableProductTypes.SelectionMode = ListSelectionMode.Multiple;
                lbAvailableProductTypes.DataBind();

                lbAssignedProductTypes.DataSource = templateDetails.ApplicableTemplateTypes;
                lbAssignedProductTypes.DataTextField = "type_name";
                lbAssignedProductTypes.DataValueField = "pkey_type";
                lbAssignedProductTypes.SelectionMode = ListSelectionMode.Multiple;
                lbAssignedProductTypes.DataBind();

                if (templateDetails.TemplateCategories.Rows.Count > 0)
                {
                    ddlTemplateIdentity.DataSource = templateDetails.TemplateCategories;
                    ddlTemplateIdentity.DataTextField = "ct_name";
                    ddlTemplateIdentity.DataValueField = "pkey_category_type";
                    ddlTemplateIdentity.DataBind();
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void ddlProductCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ProductTemplateModel.ProductTypesDataTable Types = (ProductTemplateModel.ProductTypesDataTable)ViewState["ProductTypes"];
                ddlProductType.DataTextField = "ct_name";
                ddlProductType.DataValueField = "pkey_category_type";
                ddlProductType.DataSource = Types.Select("fkey_self = " + ddlProductCategory.SelectedValue);
                ddlProductType.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void ddlTemplateIdentity_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ddlByTheme.Items.Clear();
                ddlByCategory.Items.Clear();
                templateTypes = "-2";  // ddlProductType.SelectedValue.ToString();
                category = ddlTemplateIdentity.SelectedValue == "" ? "-2" : ddlTemplateIdentity.SelectedValue;
                fkey_product_type = Convert.ToInt32(ddlProductType.SelectedValue == "" ? "-1" : ddlProductType.SelectedValue);

                ProductTemplateModel templateDetails = TemplateService.GetTemplateDetails(pkeyTemplate, templateTypes, category, themeCategoryKey, byCategoryCatKey, ExclusiveCustomerKey, fkey_product_type, ConnString);

                if (templateDetails.ThemeTypes.Rows.Count > 0)
                {
                    var catType = from collect in templateDetails.ThemeTypes
                                  where collect.fkey_self == byCategoryCatKey
                                  select collect;
                    ddlByCategory.DataSource = catType.ToList();
                    ddlByCategory.DataTextField = "ct_name";
                    ddlByCategory.DataValueField = "pkey_category_type";
                    ddlByCategory.DataBind();
                }
                if (templateDetails.ThemeTypes.Rows.Count > 0)
                {
                    var catType = from collect in templateDetails.ThemeTypes
                                  where collect.fkey_self == themeCategoryKey
                                  select collect;
                    ddlByTheme.DataSource = catType.ToList();
                    ddlByTheme.DataTextField = "ct_name";
                    ddlByTheme.DataValueField = "pkey_category_type";
                    ddlByTheme.DataBind();
                }
                lbAvailableProductTypes.DataSource = templateDetails.AvailableTemplateTypes;
                lbAvailableProductTypes.DataTextField = "type_name";
                lbAvailableProductTypes.DataValueField = "pkey_type";
                lbAvailableProductTypes.SelectionMode = ListSelectionMode.Multiple;
                lbAvailableProductTypes.DataBind();

                lbAssignedProductTypes.DataSource = templateDetails.ApplicableTemplateTypes;
                lbAssignedProductTypes.DataTextField = "type_name";
                lbAssignedProductTypes.DataValueField = "pkey_type";
                lbAssignedProductTypes.SelectionMode = ListSelectionMode.Multiple;
                lbAssignedProductTypes.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }
    }
}