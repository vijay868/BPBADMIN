﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Collections;

namespace Bestprintbuy.Admin.Web.SystemConfig
{
    public partial class RevTemplateList : PageBase
    {
        protected int recordCount = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                GetReverseTemplateList(ConnString);
            }
        }

        private void GetReverseTemplateList(string ConnString)
        {
            try
            {
                ReverseTemplateModel revTemplateList = ProductService.GetReverseTemplateList(ConnString);
                if (RevTemplateListGrid.Attributes["SortExpression"] != null)
                    revTemplateList.ReverseTemplate.DefaultView.Sort = RevTemplateListGrid.Attributes["SortExpression"] + " " + RevTemplateListGrid.Attributes["SortDirection"];
                recordCount = revTemplateList.ReverseTemplate.Rows.Count;
                RevTemplateListGrid.DataSource = revTemplateList.ReverseTemplate.DefaultView;
                RevTemplateListGrid.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void RevTemplateListGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                SortGridView(RevTemplateListGrid, e);
                RevTemplateListGrid.PageIndex = 0;
                GetReverseTemplateList(ConnString);
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void RevTemplateListGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                RevTemplateListGrid.PageIndex = e.NewPageIndex;
                GetReverseTemplateList(ConnString);
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected int GetRecordCountForPage()
        {
            return GetRecordCountForPageSize(RevTemplateListGrid.PageSize, RevTemplateListGrid.PageIndex + 1);
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("RevTemplateDetail.aspx?fkeyRevTemplate=-2");
        }

        protected void btnAddBackTemp_Click(object sender, EventArgs e)
        {
            Response.Redirect("RevTemplateListAfterAproveDesign.aspx");
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/DashBoard.aspx");
        }
    }
}