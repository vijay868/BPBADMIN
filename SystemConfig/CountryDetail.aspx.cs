﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.SystemConfig
{
    public partial class CountryDetail : PageBase
    {
        protected int pkeycountry;
        protected void Page_Load(object sender, EventArgs e)
        {
            pkeycountry = Convert.ToInt32(Request.QueryString["pkeyCountry"]);
            if (!Page.IsPostBack)
            {
                GetData();
            }
        }

        private void GetData()
        {
            try
                {                
                int pkeyCountry = Convert.ToInt32(Request.QueryString["pkeyCountry"]);
                CountryModel country = CountryService.GetList(pkeyCountry, ConnString);
                if (country.Countries.Rows.Count > 0)
                {
                    CountryModel.CountriesRow countryRow = (CountryModel.CountriesRow)country.Countries.Rows[0];
                    txtCountryID.Text = countryRow.country_abbr;
                    txtCountryName.Text = countryRow.country_name;
                    chkIsDefault.Checked = countryRow.is_default == "False" ? false : true;
                }
                if (pkeyCountry == -2)
                {
                    chkIsDefault.Checked = true;
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int pkeyCountry = Convert.ToInt32(Request.QueryString["pkeyCountry"]);
                CountryModel countries = new CountryModel();
                CountryModel.CountriesRow Row = countries.Countries.NewCountriesRow();
                Row.pkey_country = pkeyCountry;
                Row.country_abbr = txtCountryID.Text;
                Row.country_name = txtCountryName.Text;
                Row.is_default = chkIsDefault.Checked.ToString();
                Row.fkey_user = Identity.UserPK;
                countries.Countries.AddCountriesRow(Row);
                pkeyCountry = CountryService.UpdateModel(countries, ConnString);
                Response.Redirect("CountryList.aspx");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                if (ex.Message.IndexOf("COUNTRY_NAME_UNIQUE") > -1)
                    AlertScript("A country with the entered name already exists, Please enter another and try.");
                else
                    AlertScript("Save/Update failed. try again.");
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("CountryList.aspx");
        }
    }
}