﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.SystemConfig
{
    public partial class CustMessageDetail : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ViewState["Pkey"] = Convert.ToInt32(Request.QueryString["Pkey"]);
                GetData();
            }
        }

        private void GetData()
        {
            try
            {
                int catKey = Convert.ToInt32(WebConfiguration.ProductTypeKeys["Templates By Business"]);
                CustMessageModel model = ProductService.GetMsgData(Convert.ToInt32(ViewState["Pkey"]), catKey, ConnString);

                ddlCategory.Items.Clear();
                ddlCategory.DataSource = model.Categories;
                ddlCategory.DataTextField = "ct_name";
                ddlCategory.DataValueField = "pkey_category_type";
                ddlCategory.DataBind();

                var availableList = (from AvailableProductTypes in model.AvailableProductTypes
                                     where AvailableProductTypes.isSelected == false
                                     select AvailableProductTypes).ToList();

                lbAvailableMatchProducts.DataSource = availableList;
                lbAvailableMatchProducts.DataTextField = "ct_name";
                lbAvailableMatchProducts.DataValueField = "pkey_category_type";
                lbAvailableMatchProducts.SelectionMode = ListSelectionMode.Multiple;
                lbAvailableMatchProducts.DataBind();

                var assignedList = (from AssignedProductTypes in model.AvailableProductTypes
                                    where AssignedProductTypes.isSelected == true
                                    select AssignedProductTypes).ToList();

                lbAssignedMatchProducts.DataSource = assignedList;
                lbAssignedMatchProducts.DataTextField = "ct_name";
                lbAssignedMatchProducts.DataValueField = "pkey_category_type";
                lbAssignedMatchProducts.SelectionMode = ListSelectionMode.Multiple;
                lbAssignedMatchProducts.DataBind();

                if (model.CustMessageMaster.Rows.Count > 0)
                {
                    CustMessageModel.CustMessageMasterRow row = (CustMessageModel.CustMessageMasterRow)model.CustMessageMaster.Rows[0];
                    txtMessageID.Text = row.MSG_ID;
                    txtMessage.Text = row.MESSAGE;
                    chkIsExclusive.Checked = row.IsKiosk;
                    if (!row.IsFKEY_TEMPLATE_TYPENull())
                        ddlCategory.SelectedValue = row.FKEY_TEMPLATE_TYPE.ToString();
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            CustMessageModel model = new CustMessageModel();
            CustMessageModel.CustMessageMasterRow row = model.CustMessageMaster.NewCustMessageMasterRow();
            row.PKEY_MESSAGE_MASTER = Convert.ToInt32(ViewState["Pkey"]);
            row.MSG_ID = txtMessageID.Text;
            row.MESSAGE = txtMessage.Text;
            row.IsKiosk = chkIsExclusive.Checked;
            if (Convert.ToInt32(ddlCategory.SelectedValue) > 0)
                row.FKEY_TEMPLATE_TYPE = Convert.ToInt32(ddlCategory.SelectedValue);


            string AssignedMatchingProducts = "";
            if (Request["ctl00$ContentPlaceHolder1$lbAssignedMatchProducts"] != null)
                AssignedMatchingProducts = Request["ctl00$ContentPlaceHolder1$lbAssignedMatchProducts"].ToString();

            AssignedMatchingProducts = AssignedMatchingProducts.TrimEnd(',');
            if (AssignedMatchingProducts.Trim() != "")
                row.Template_Type = AssignedMatchingProducts;

            model.CustMessageMaster.AddCustMessageMasterRow(row);
            try{
                int key = ProductService.UpdateMessageData(model, ConnString);
                Response.Redirect("CustMessagesList.aspx");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript("There is some problem. Please try again");
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("CustMessagesList.aspx");
        }
    }
}