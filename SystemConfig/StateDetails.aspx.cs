﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
namespace Bestprintbuy.Admin.Web.SystemConfig
{
    public partial class StateDetails : PageBase
    {
        protected int pkeystate;
        protected void Page_Load(object sender, EventArgs e)
        {
            pkeystate = Convert.ToInt32(Request.QueryString["pkeyState"]);
            if (!Page.IsPostBack)
            {
                GetData();
            }
        }

        private void GetData()
        {
            try
            {
                int pkeyState = Convert.ToInt32(Request.QueryString["pkeyState"]);
                int countrykey = Convert.ToInt32(Request.QueryString["countrykey"]);
                CountryModel state_Data = CountryService.GetData(pkeyState, countrykey, ConnString);
                if (state_Data.States.Rows.Count > 0)
                {
                    CountryModel.StatesRow stateRow = (CountryModel.StatesRow)state_Data.States.Rows[0];
                    txtStateID.Text = stateRow.state_abbr;
                    txtStateName.Text = stateRow.state_name;
                }
                if (state_Data.Countries.Rows.Count > 0)
                {
                    CountryModel.CountriesRow countryRow = (CountryModel.CountriesRow)state_Data.Countries.Rows[0];
                    lblCountry.Text = countryRow.country_name;
                    hdnCountryKey.Value = countryRow.pkey_country.ToString();
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int pkeyState = Convert.ToInt32(Request.QueryString["pkeyState"]);
                CountryModel states = new CountryModel();
                CountryModel.StatesRow Row = states.States.NewStatesRow();
                Row.pkey_state = pkeyState;
                Row.state_abbr = txtStateID.Text;
                Row.state_name = txtStateName.Text;
                Row.fkey_country = Convert.ToInt32(hdnCountryKey.Value);
                Row.fkey_user = Identity.UserPK;
                states.States.AddStatesRow(Row);
                pkeyState = CountryService.UpdateState(states, ConnString);
                Response.Redirect("StateList.aspx");
            }
            catch (Exception ex)
            {
                if (ex.Message.IndexOf("STATE_ABBR_UNIQUE") > -1)
                {
                    FileLogger.WriteException(ex);
                    AlertScript("A state with the entered abbreviation already exists, Please enter another and try.");
                }
                else if (ex.Message.IndexOf("STATE_NAME_UNIQUE") > -1)
                {
                    FileLogger.WriteException(ex);
                    AlertScript("A state with the entered name already exists, Please enter another and try again.");
                }
                else
                {
                    FileLogger.WriteException(ex);
                    AlertScript("Save/Update failed. try again.");
                }
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("StateList.aspx");
        }
    }
}