﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="KiosksCompanyList.aspx.cs" Inherits="Bestprintbuy.Admin.Web.SystemConfig.KiosksCompanyList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      
    <div class="contant_hldr">
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
             <li class="liColor">System Configuration</li>
            <li>Kiosks Company List</li>
        </ul>
        <h2>Kiosks Company List</h2>
        <form class="" runat="server" id="frmKioskCompanyList">
            <div class="form_sep_left">
            <table class="accounts-table" >
                <tbody>
                    <tr>
                        <td width="10%" class="a_cnt">Kiosks</td>
                        <td  width="15%">
                             <asp:DropDownList name="ddlkiosks" ID="ddlkiosks" CssClass="slect-box" Width="300px" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlkiosks_SelectedIndexChanged">
                        <asp:ListItem Value="173" Selected="True">EXIT</asp:ListItem>
                        <asp:ListItem Value="175">PRU</asp:ListItem>
                    </asp:DropDownList>
                        </td>
                        <td width="10%" class="a_cnt">States</td>
                        <td width="15%">
                             <asp:DropDownList name="ddlStates" ID="ddlStates" CssClass="slect-box" Width="300px"  runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlStates_SelectedIndexChanged">
                        <asp:ListItem Value=""></asp:ListItem>
                    </asp:DropDownList>
                         </td>
                        <td width="45%">
                        </td>

                    </tr>
                </tbody>
            </table>
    </div>           
            <div class="clr"></div>

            <asp:GridView ID="kioskcompanyGrid" runat="server" AutoGenerateColumns="false" EnableViewState="true"
                ForeColor="Black" Width="100%" PageSize="20" AllowPaging="true" OnPageIndexChanging="kioskcompanyGrid_PageIndexChanging"
                class="table" name="kioskcompanyGrid">
                <PagerStyle HorizontalAlign="Right" CssClass="dtPage"></PagerStyle>
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />
                <Columns>
                    <asp:TemplateField HeaderText="#SL" >
                        <HeaderStyle Width="10%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <span><%# GetRecordCountForPage()%></span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Company Name">
                        <HeaderStyle Width="50%" ForeColor="White" />
                        <ItemTemplate>
                             <%#Eval("ec_company_name") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Is Active">
                        <HeaderStyle ForeColor="White" />
                        <ItemTemplate>
                    <asp:CheckBox ID="chkIsactive" runat="server" Checked='<%# Convert.ToBoolean(Eval("is_active")) ? true : false %>' />
                                                <asp:HiddenField ID="hdnpkeyxref" runat="server" Value='<%#Eval("pkey_ec_comp_logo_state_xref") %>' />
                                                <asp:HiddenField ID="hdnPkeyecCompanyLogo" runat="server" Value='<%#Eval("pkey_ec_comp_logo") %>' />
                                                <asp:HiddenField ID="hdnecCompanyName" runat="server" Value='<%#Eval("ec_company_name") %>' />
                                                <asp:HiddenField ID="hdnMultilineLogo" runat="server" Value='<%#Eval("ismultilinelogo") %>' />
                                                <asp:HiddenField ID="hdnLogoUrl" runat="server" Value='<%#Eval("logo_url") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                  
                    
                </Columns>
            </asp:GridView>
            <asp:Label ID="lblTotalRecords" runat="server" Font-Bold="true"></asp:Label>
            <div class="btn_lst" style="padding-top:20px">
              <div class="lft">
                            <asp:Button runat="server" ID="btnAddCompanyLogo" CssClass="button rounded" Text="Add Company Logo" OnClick="btnAddCompanyLogo_Click"></asp:Button>
                            <asp:Button runat="server" ID="btnUploadLogoImages" CssClass="button rounded" Text="Upload Logo Images" Visible="false" OnClick="btnUploadLogoImages_Click" ></asp:Button>
                        </div>
                        <div class="rgt">
                            <asp:Button runat="server" ID="btnClose" CssClass="button rounded"  Text="Close" OnClick="btnClose_Click"></asp:Button>
                            <asp:Button runat="server" ID="btnUpdate" CssClass="button rounded" Text="Update" OnClick="btnUpdate_Click"></asp:Button>
                            
                        </div>
            </div>
        </form>
    </div>
</asp:Content>
