﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="CategoryList.aspx.cs" Inherits="Bestprintbuy.Admin.Web.SystemConfig.CategoryList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function deleteCategory() {
            if ('<%=HasAccess("CategoryList", "delete")%>' != 'False') {
                 return confirm('Results in deletion of selected record(s). Do you want to continue?');
             }
             else {
                 alert('User does not have access.');
                 return false;
             }
         }
         function addCategory() {
             if ('<%=HasAccess("CategoryList", "add")%>' == 'False') {
                alert('User does not have access.');
                return false;
            }
            return true;
        }
    </script>

    <div class="contant_hldr">
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">System Configuration</li>
            <li>Category List</li>
        </ul>
        <h2>Category List</h2>
        <form class="form" runat="server" id="frmCategoryList">
            <table class="accounts-table">
                <tbody>
                    <tr>
                       <td width="20%">
                            <asp:DropDownList name="ddlMasterCategory" ID="ddlMasterCategory" CssClass="search" data-placeholder="Choose a Name" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlMasterCategory_SelectedIndexChanged">
                                <asp:ListItem Value=""></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td width="80%"></td>
                    </tr>
                </tbody>
            </table>           
            <div class="clr"></div>

            <asp:GridView ID="CategoryGrid" runat="server" AutoGenerateColumns="false" EnableViewState="true"
                ForeColor="Black" Width="100%" PageSize="20" AllowPaging="true" AllowSorting="true" OnPageIndexChanging="CategoryGrid_PageIndexChanging"
                class="table" name="CategoryGrid" OnSorting="CategoryGrid_Sorting">
                <PagerStyle HorizontalAlign="Right" CssClass="dtPage"></PagerStyle>
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />
                <Columns>
                    <asp:TemplateField HeaderText="#SL" ItemStyle-Width="5%">
                        <HeaderStyle Width="5%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <span><%# GetRecordCountForPage()%></span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Category ID" ItemStyle-Width="9%" SortExpression="category_id"> 
                        <HeaderStyle Width="9%" ForeColor="White" />
                        <ItemTemplate>
                            <a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("CategoryDetails", "view")%>','CategoryDetails.aspx?categoryKey=<%#Eval("pkey_category") %>&masterKey=<%#Eval("fkey_master") %>')" data-gravity="s" title="Edit"><%#Eval("category_id") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Category Name" ItemStyle-Width="10%" SortExpression="category_name">
                        <HeaderStyle Width="10%" ForeColor="White" />
                        <ItemTemplate>
                            <a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("CategoryDetails", "view")%>','CategoryDetails.aspx?categoryKey=<%#Eval("pkey_category") %>&masterKey=<%#Eval("fkey_master") %>')" data-gravity="s" title="Edit"><%#Eval("category_name") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="# of Screens" ItemStyle-Width="10%">
                        <HeaderStyle Width="10%" ForeColor="White" />
                        <ItemTemplate>
                            <%#Eval("screens") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Delete" ItemStyle-Width="12%">
                        <HeaderStyle Width="12%" ForeColor="White" />
                        <ItemTemplate>
                            <asp:LinkButton ID="btnDelete" runat="server" CommandName="Delete" CausesValidation="true"
                                ToolTip="Delete" CommandArgument='<%#Bind("pkey_category")%>' OnClientClick="return deleteCategory();"
                                OnClick="btnDelete_Click"><img src="../images/trashcan.png" /></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <asp:Label ID="lblTotalRecords" runat="server" Font-Bold="true"></asp:Label>
            <div class="btn_lst" style="padding-top:20px">
                <div class="rgt">
                    <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClick="btnClose_Click"></asp:Button>
                </div>
                <div class="lft">
                    <asp:Button runat="server" ID="btnAdd" CssClass="button rounded" Text="Add Category" OnClientClick="return addCategory();" OnClick="btnAdd_Click"></asp:Button>
                </div>
            </div>
        </form>
    </div>
</asp:Content>
