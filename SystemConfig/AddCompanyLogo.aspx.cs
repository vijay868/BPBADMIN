﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.SystemConfig
{
    public partial class AddCompanyLogo : PageBase
    {
        protected int stateKey = -1;
        protected int kioskKey = 173;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                GetStatesData();
            }
        }

        private void GetStatesData()
        {
            try
            {
                kioskKey = Convert.ToInt32(Request.QueryString["KioskKey"]);
                ExclusiveCustomerModel states = ExclusiveCustomerService.GetECStatesCOMBO(kioskKey, ConnString);
                ddlState.DataSource = states.States;
                ddlState.DataTextField = "state_name";
                ddlState.DataValueField = "pkey_state";
                ddlState.DataBind();
                ddlState.SelectedValue = Request.QueryString["stateid"];
                ddlkiosks.SelectedValue = Request.QueryString["KioskKey"];
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
            
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string Companyname = txtCompanyName.Text;
            string CompanyID = txtCompanyID.Text;
            int PkeyState = Convert.ToInt32(ddlState.SelectedValue);
            int KioskId = Convert.ToInt32(ddlkiosks.SelectedValue);
            string LogoUrl = txtLogoUrl.Text;
            string SateName = ddlState.SelectedItem.Text;
            try
            {
                ExclusiveCustomerService.InsertCompanyLogo(Companyname, CompanyID, PkeyState, KioskId, LogoUrl, SateName, ConnString);
                Response.Redirect("KiosksCompanyList.aspx");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("KiosksCompanyList.aspx");
        }
    }
}