﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.SystemConfig
{
    public partial class TemplateList : PageBase
    {
        protected int recordCount = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                GetProductCategory();
                GetTemplateCategory();
                LoadTemplate();
            }
        }

        private void GetTemplateCategory()
        {
            try
            {
                ddlTemplateCategory.DataSource = TemplateService.GetTemplateCategories(ConnString);
                ddlTemplateCategory.DataTextField = "ct_name";
                ddlTemplateCategory.DataValueField = "pkey_category_type";
                ddlTemplateCategory.DataBind();
                ddlTemplateCategory.Items.Insert(0, new ListItem("No Option", "-1"));
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        private void GetProductCategory()
        {
            try
            {
                ddlProductCategory.DataSource = TemplateService.GetProductCategories(ConnString);
                ddlProductCategory.DataTextField = "ct_name";
                ddlProductCategory.DataValueField = "pkey_category_type";
                ddlProductCategory.DataBind();
                ddlProductCategory.Items.Insert(0, new ListItem("No Option", "-1"));
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        private void LoadTemplate()
        {
            try
            {
                int pkeyProcductCategory = Convert.ToInt32(ddlProductCategory.SelectedValue);
                int pkeyTemplateType = -1; // Convert.ToInt32(ddlTemplateCategory.SelectedValue);
                ProductTemplateModel templates = TemplateService.GetTemplateList(pkeyProcductCategory, pkeyTemplateType, ConnString);

                if (TemplateListGrid.Attributes["SortExpression"] != null)
                    templates.Templates.DefaultView.Sort = TemplateListGrid.Attributes["SortExpression"] + " " + TemplateListGrid.Attributes["SortDirection"];
                recordCount = templates.Templates.Rows.Count;

                TemplateListGrid.DataSource = templates.Templates.DefaultView;
                TemplateListGrid.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void TemplateListGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                TemplateListGrid.PageIndex = e.NewPageIndex;
                LoadTemplate();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void CustomerslistGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                SortGridView(TemplateListGrid, e);
                TemplateListGrid.PageIndex = 0;
                LoadTemplate();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected int GetRecordCountForPage()
        {
            return GetRecordCountForPageSize(TemplateListGrid.PageSize, TemplateListGrid.PageIndex + 1);
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("TemplateDetail.aspx?pkeyTemplate=-2&fkeyProductType=-1");
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/DashBoard.aspx");
        }

        protected void ddlProductCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadTemplate();
        }

        protected void ddlTemplateCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ProductTemplateModel.TemplateTypesDataTable templateTypes = TemplateService.GetTemplateTypes(ConnString);
                if (templateTypes.Rows.Count > 0)
                {
                    var catType = from collect in templateTypes
                                  where collect.fkey_self == Convert.ToInt32(ddlTemplateCategory.SelectedValue)
                                  select collect;

                    ddlTemplateType.DataSource = catType.ToList();
                    ddlTemplateType.DataTextField = "ct_name";
                    ddlTemplateType.DataValueField = "pkey_category_type";
                    ddlTemplateType.DataBind();
                    ddlTemplateType.Items.Insert(0, new ListItem("No Option", "-1"));
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void ddlTemplateType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int pkeyProcductCategory = Convert.ToInt32(ddlProductCategory.SelectedValue);
                int pkeyTemplateType = -1;
                ProductTemplateModel templates = TemplateService.GetTemplateList(pkeyProcductCategory, pkeyTemplateType, ConnString);
                string searchStr = ddlTemplateCategory.SelectedItem + " - " + ddlTemplateType.SelectedItem;
                var list1 = from collect in templates.Templates
                            where collect.template_category_type == searchStr
                            select collect;
                recordCount = list1.ToList().Count;
                TemplateListGrid.PageIndex = 0;
                TemplateListGrid.DataSource = list1.ToList();
                TemplateListGrid.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }
    }
}