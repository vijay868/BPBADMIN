﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace Bestprintbuy.Admin.Web.SystemConfig
{
    public partial class FreePromoDetails : PageBase
    {
        protected int promotionKey = 0;
        protected int fkeyProductType = 0;
        protected int fkeyProductCat = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            ViewState["promoKey"] = Convert.ToInt32(Request.QueryString["promoKey"]);
            promotionKey = Convert.ToInt32(ViewState["promoKey"]);
            if (!Page.IsPostBack)
            {
                GetData();
            }
        }

        private void GetData()
        {
            try
            {
                int promoKey = Convert.ToInt32(ViewState["promoKey"]);
                PromotionModel promoDetails = PromoService.GetPromoDetails(promoKey, 1, "", ConnString);

                lbAvailablePromotions.DataSource = promoDetails.AvailableFreePromos;
                lbAvailablePromotions.DataTextField = "promo_code";
                lbAvailablePromotions.DataValueField = "pkey_promotion";
                lbAvailablePromotions.SelectionMode = ListSelectionMode.Multiple;
                lbAvailablePromotions.DataBind();

                lbAvailableShipments.DataSource = promoDetails.AvailableShipments;
                lbAvailableShipments.DataTextField = "shipment_description";
                lbAvailableShipments.DataValueField = "shipment_on";
                lbAvailableShipments.SelectionMode = ListSelectionMode.Multiple;
                lbAvailableShipments.DataBind();

                lbAssignedPromotions.DataSource = promoDetails.AssignedFreePromos;
                lbAssignedPromotions.DataTextField = "promo_code";
                lbAssignedPromotions.DataValueField = "fkey_promotion";
                lbAssignedPromotions.SelectionMode = ListSelectionMode.Multiple;
                lbAssignedPromotions.DataBind();

                lbAssignedShipments.DataSource = promoDetails.AssignedShipments;
                lbAssignedShipments.DataTextField = "shipment_description";
                lbAssignedShipments.DataValueField = "shipment_on";
                lbAssignedShipments.SelectionMode = ListSelectionMode.Multiple;
                lbAssignedShipments.DataBind();

                if (promoDetails.Promotions.Rows.Count > 0)
                {
                    PromotionModel.PromotionsRow promotionRow = (PromotionModel.PromotionsRow)promoDetails.Promotions.Rows[0];
                    txtPromoCode.Text = promotionRow.promo_code;
                    txtDescription.Text = promotionRow.description;
                    txtProductDiscount.Text = promotionRow.discount.ToString().Replace(".00", "") == "" ? "" : promotionRow.discount.ToString().Replace(".00", "");
                    txtShipmentDiscount.Text = promotionRow.ship_discount.ToString().Replace(".00", "") == "" ? "" : promotionRow.ship_discount.ToString().Replace(".00", "");
                    txtFromDate.Text = Convert.ToDateTime(promotionRow.start_date).ToString("MM/dd/yyyy");
                    txtToDate.Text = Convert.ToDateTime(promotionRow.end_date).ToString("MM/dd/yyyy");
                    chkActive.Checked = promotionRow.is_active;
                    chkNewCustomers.Checked = promotionRow.check_new_customer;
                    chkSingleIteminCart.Checked = promotionRow.apply_to_single_item;
                    ddlOfferedOn.SelectedValue = promotionRow.rule_type.Trim();

                    if (promotionRow.template_option == 0)
                        rbtnNone.Checked = true;
                    else if (promotionRow.template_option == 1)
                        rbtnParentCategories.Checked = true;
                    else if (promotionRow.template_option == 2)
                        rbtnTemplateTypes.Checked = true;

                    if (promotionRow.ship_discount_type)
                    {
                        rbtnShipPercentage.Checked = false;
                        rbtnShipFixed.Checked = true;
                    }
                    else
                    {
                        rbtnShipPercentage.Checked = true;
                        rbtnShipFixed.Checked = false;
                    }

                    if (promotionRow.discount_type)
                    {
                        rbtnProdPercentage.Checked = false;
                        rbtnProdFixed.Checked = true;
                    }
                    else
                    {
                        rbtnProdPercentage.Checked = true;
                        rbtnProdFixed.Checked = false;
                    }
                    btnDelete.Visible = true;
                    lblProdCat.Visible = true;
                    lblProdType.Visible = true;
                }
                else
                {
                    ddlOfferedOn.SelectedValue = "D";
                    rbtnProdPercentage.Checked = true;
                    rbtnShipPercentage.Checked = true;
                    rbtnNone.Checked = true;
                    chkActive.Checked = true;
                    ddlCategory.Visible = true;
                    ddlProdTypes.Visible = true;
                }

                if (promoDetails.AssignedProductTypes.Rows.Count > 0)
                {
                    PromotionModel.AssignedProductTypesRow ProductTypesRow = (PromotionModel.AssignedProductTypesRow)promoDetails.AssignedProductTypes.Rows[0];
                    lblProdType.Text = ProductTypesRow.ct_name;
                    lblProdCat.Text = ProductTypesRow.productCat;
                    fkeyProductType = ProductTypesRow.fkey_product_type;
                }

                if (promoDetails.ProductCategories.Rows.Count > 0)
                {
                    ddlCategory.DataSource = promoDetails.ProductCategories;
                    ddlCategory.DataTextField = "ct_name";
                    ddlCategory.DataValueField = "pkey_category_type";
                    ddlCategory.DataBind();
                }

                if (promoDetails.ProductTypes.Rows.Count > 0)
                {
                    ViewState["ProductTypes"] = promoDetails.ProductTypes;
                    var catType = from collect in promoDetails.ProductTypes
                                  where collect.fkey_self == Convert.ToInt32(ddlCategory.SelectedValue)
                                  select collect;
                    ddlProdTypes.DataSource = catType.ToList();
                    ddlProdTypes.DataTextField = "ct_name";
                    ddlProdTypes.DataValueField = "pkey_category_type";
                    ddlProdTypes.DataBind();
                }

                if (promoDetails.ProductPromo.Rows.Count > 0)
                {
                    string ProdType = "";
                    string Prodids = "";
                    string ProdCat = "";
                    string ProdNames = "";
                    foreach (PromotionModel.ProductPromoRow row in promoDetails.ProductPromo)
                    {
                        Prodids += row.fkey_product.ToString() + ",";
                        ProdType += row.product + "\r\n";
                        ProdCat += row.pkey_product_promotion_xref.ToString() + ",";
                        ProdNames += row.product + "^";
                    }
                    txtAssignedProducts.Text = ProdType;
                    hdnProductIds.Value = Prodids.TrimEnd(',');
                    hdnProductCat.Value = ProdCat.TrimEnd(',');
                    hdnProductNames.Value = ProdNames.TrimEnd('^');
                }

                if (promoDetails.TemplatePromo.Rows.Count > 0)
                {
                    string temptype = "";
                    string tempids = "";
                    string tempCat = "";
                    string tempNames = "";
                    foreach (PromotionModel.TemplatePromoRow row in promoDetails.TemplatePromo)
                    {
                        tempids += row.pkey_category_type.ToString() + ",";
                        temptype += row.type_name + "\r\n";
                        tempCat += row.pkey_promo_template_cat.ToString() + ",";
                        tempNames += row.type_name + "^";
                    }
                    txtTemplateTypes.Text = temptype;
                    hdnTemplateTypeIds.Value = tempids.TrimEnd(',');
                    hdnTemplateTypeCat.Value = tempCat.TrimEnd(',');
                    hdnTemplateTypeNames.Value = tempNames.TrimEnd('^');
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
            

        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int promoKey = Convert.ToInt32(ViewState["promoKey"]);
                PromoService.DeletePromotion(promoKey, ConnString);
                Response.Redirect("FreePromoList.aspx");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int promoKey = Convert.ToInt32(ViewState["promoKey"]);
                PromotionModel PromotionDetails = new PromotionModel();
                PromotionModel.PromotionsRow promotionRow = PromotionDetails.Promotions.NewPromotionsRow();
                promotionRow.pkey_promotion = promoKey;
                promotionRow.promo_code = txtPromoCode.Text;
                promotionRow.description = txtDescription.Text;
                promotionRow.discount = txtProductDiscount.Text;
                promotionRow.ship_discount = txtShipmentDiscount.Text;
                promotionRow.start_date = Convert.ToDateTime(txtFromDate.Text);
                promotionRow.end_date = Convert.ToDateTime(txtToDate.Text);
                promotionRow.is_active = chkActive.Checked;
                promotionRow.apply_on_agent_package = false;
                promotionRow.check_new_customer = chkNewCustomers.Checked;
                promotionRow.enabled_for_ads = false;
                promotionRow.rule_type = ddlOfferedOn.SelectedValue;
                promotionRow.fkey_user = Identity.UserPK;
                promotionRow.is_free_promo = true;
                promotionRow.apply_to_single_item = chkSingleIteminCart.Checked;

                if (rbtnNone.Checked)
                    promotionRow.template_option = 0;
                else if (rbtnParentCategories.Checked)
                    promotionRow.template_option = 1;
                else if (rbtnTemplateTypes.Checked)
                    promotionRow.template_option = 2;

                if (rbtnShipFixed.Checked)
                    promotionRow.ship_discount_type = true;
                else
                    promotionRow.ship_discount_type = false;

                if (rbtnProdFixed.Checked)
                    promotionRow.discount_type = true;
                else
                    promotionRow.discount_type = false;

                PromotionDetails.Promotions.AddPromotionsRow(promotionRow);

                if (PromotionDetails.AssignedProductTypes != null)
                {
                    PromotionModel.AssignedProductTypesRow AssignedProductTypesRow = PromotionDetails.AssignedProductTypes.NewAssignedProductTypesRow();
                    AssignedProductTypesRow.fkey_category_type = Convert.ToInt32(ddlCategory.SelectedValue);
                    AssignedProductTypesRow.fkey_product_type = Convert.ToInt32(ddlProdTypes.SelectedValue);

                    PromotionDetails.AssignedProductTypes.AddAssignedProductTypesRow(AssignedProductTypesRow);
                }

                string AssignedShipments = "";
                if (Request["ctl00$ContentPlaceHolder1$lbAssignedShipments"] != null)
                    AssignedShipments = Request["ctl00$ContentPlaceHolder1$lbAssignedShipments"].ToString();

                if (AssignedShipments != "")
                {
                    foreach (string item in AssignedShipments.Split(','))
                    {
                        PromotionModel.AssignedShipmentsRow assignedShipmentRow = PromotionDetails.AssignedShipments.NewAssignedShipmentsRow();
                        assignedShipmentRow.shipment_on = item;
                        PromotionDetails.AssignedShipments.AddAssignedShipmentsRow(assignedShipmentRow);

                    }
                }

                string AssignedPromotions = "";
                if (Request["ctl00$ContentPlaceHolder1$lbAssignedPromotions"] != null)
                    AssignedPromotions = Request["ctl00$ContentPlaceHolder1$lbAssignedPromotions"].ToString();

                if (AssignedPromotions != "")
                {
                    foreach (string item in AssignedPromotions.Split(','))
                    {
                        PromotionModel.AssignedFreePromosRow assignedFreePromosRow = PromotionDetails.AssignedFreePromos.NewAssignedFreePromosRow();
                        //assignedFreePromosRow.promo_code = item;
                        assignedFreePromosRow.fkey_promotion = Convert.ToInt32(item);
                        PromotionDetails.AssignedFreePromos.AddAssignedFreePromosRow(assignedFreePromosRow);                        
                    }
                }

                if (hdnTemplateTypeIds.Value != "")
                {
                    string[] templatetypeIds = hdnTemplateTypeIds.Value.Split(",".ToCharArray());
                    string[] templatetypeCat = hdnTemplateTypeCat.Value.Split(",".ToCharArray());
                    string[] templatetypeNames = hdnTemplateTypeNames.Value.Split("^".ToCharArray());
                    for (int i = 0; i < templatetypeIds.Length; i++)
                    {
                        PromotionModel.TemplatePromoRow TemplatePromoRow = PromotionDetails.TemplatePromo.NewTemplatePromoRow();
                        TemplatePromoRow.pkey_category_type = Convert.ToInt32(templatetypeIds[i]);
                        TemplatePromoRow.pkey_promo_template_cat = Convert.ToInt32(templatetypeCat[i]);
                        TemplatePromoRow.type_name = templatetypeNames[i];
                        TemplatePromoRow.fkey_promotion = promoKey;

                        PromotionDetails.TemplatePromo.AddTemplatePromoRow(TemplatePromoRow);
                    }
                }

                if (hdnProductIds.Value != "")
                {
                    string[] productIds = hdnProductIds.Value.Split(",".ToCharArray());
                    string[] productCat = hdnProductCat.Value.Split(",".ToCharArray());
                    string[] productNames = hdnProductNames.Value.Split("^".ToCharArray());
                    for (int i = 0; i < productIds.Length; i++)
                    {
                        PromotionModel.ProductPromoRow ProductPromoRow = PromotionDetails.ProductPromo.NewProductPromoRow();
                        ProductPromoRow.fkey_product = Convert.ToInt32(productIds[i]);
                        ProductPromoRow.pkey_product_promotion_xref = Convert.ToInt32(productCat[i]);
                        ProductPromoRow.product = productNames[i];
                        ProductPromoRow.fkey_promotion = promoKey;

                        PromotionDetails.ProductPromo.AddProductPromoRow(ProductPromoRow);
                    }
                }

                promoKey = PromoService.UpdatePromoData(PromotionDetails, ConnString);
                ViewState["promoKey"] = promoKey;

                AlertScript("Promotion data saved");
                GetData();
            }
            catch (Exception ex)
            {
                if (ex.Message.ToLower().IndexOf("duplicate") > -1)
                    AlertScript("Free promo code must be unique, please re-enter");
                else
                    AlertScript(ex.Message);
                FileLogger.WriteException(ex);
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("FreePromoList.aspx");
        }

        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                PromotionModel.ProductTypesDataTable Types = (PromotionModel.ProductTypesDataTable)ViewState["ProductTypes"];
                ddlProdTypes.DataTextField = "ct_name";
                ddlProdTypes.DataValueField = "pkey_category_type";
                ddlProdTypes.DataSource = Types.Select("fkey_self = " + ddlCategory.SelectedValue);
                ddlProdTypes.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
           
        }


    }
}