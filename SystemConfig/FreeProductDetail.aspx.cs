﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace Bestprintbuy.Admin.Web.SystemConfig
{
    public partial class FreeProductDetail : PageBase
    {
        protected int productKey;
        protected void Page_Load(object sender, EventArgs e)
        {
            ViewState["pKey"] = Convert.ToInt32(Request.QueryString["pKey"]);
            productKey = Convert.ToInt32(ViewState["pKey"]);
            if (!Page.IsPostBack)
            {
                GetData();
            }
        }

        private void GetData()
        {
            try
            {
                int pKey = Convert.ToInt32(ViewState["pKey"]);
                if (pKey > 0)
                    btnDelete.Visible = true;
                int quantityVariantKey = Convert.ToInt32(WebConfiguration.ProductVariants["Quantity"]);
                FreeProductsModel model = ProductService.GetFreeProductByID(pKey, quantityVariantKey, ConnString);

                if (model.ProductCategories != null)
                {
                    ddlProductCat.DataTextField = "ct_name";
                    ddlProductCat.DataValueField = "pkey_category_type";
                    ddlProductCat.DataSource = model.ProductCategories;
                    ddlProductCat.DataBind();
                }

                if (model.PaperAttributes != null)
                {
                    ddlPaperType.DataTextField = "attribute_value";
                    ddlPaperType.DataValueField = "pkey_attribute";
                    ddlPaperType.DataSource = model.PaperAttributes;
                    ddlPaperType.DataBind();
                }

                if (model.SizeAttributes != null)
                {
                    ddlSize.DataTextField = "attribute_value";
                    ddlSize.DataValueField = "pkey_attribute";
                    ddlSize.DataSource = model.SizeAttributes;
                    ddlSize.DataBind();
                }

                if (model.ReverseColorAttributes != null)
                {
                    ddlReverseColor.DataTextField = "attribute_value";
                    ddlReverseColor.DataValueField = "pkey_attribute";
                    ddlReverseColor.DataSource = model.ReverseColorAttributes;
                    ddlReverseColor.DataBind();
                }

                if (model.ForeColorAttributes != null)
                {
                    ddlForeColor.DataTextField = "attribute_value";
                    ddlForeColor.DataValueField = "pkey_attribute";
                    ddlForeColor.DataSource = model.ForeColorAttributes;
                    ddlForeColor.DataBind();
                }

                if (model.FreeProducts.Rows.Count > 0)
                {
                    FreeProductsModel.FreeProductsRow row = (FreeProductsModel.FreeProductsRow)model.FreeProducts.Rows[0];
                    txtProductID.Text = row.product_id;
                    txtProductName.Text = row.product_name;
                    ddlProductCat.SelectedValue = row.fkey_product_category.ToString();
                    //ddlProductType.SelectedValue = row.fkey_product_type.ToString();
                    txtFromDate.Text = row.valid_from.ToString("MM/dd/yyyy");
                    txtToDate.Text = row.valid_to.ToString("MM/dd/yyyy");
                    txtQuatity.Text = row.quantity.ToString();
                    chkActive.Checked = row.is_active;

                    txtShippingCost.Text = row.shipping_cost.ToString();
                    txtShippingDescr.Text = row.ship_description;
                    txtThumbnailUrl.Text = row.thumbnail_url;

                    if (model.ProductAttributes.Rows.Count > 0)
                    {
                        ddlForeColor.SelectedValue = GetAttributeKey("FrontColor", model.ProductAttributes);
                        ddlReverseColor.SelectedValue = GetAttributeKey("BackColor", model.ProductAttributes);
                        ddlPaperType.SelectedValue = GetAttributeKey("PaperType", model.ProductAttributes);
                        ddlSize.SelectedValue = GetAttributeKey("Size", model.ProductAttributes);
                    }
                }
                else
                {
                    chkActive.Checked = true;
                }

                if (model.ProductTypes != null)
                {
                    ViewState["ProductTypes"] = model.ProductTypes;
                    ddlProductType.DataTextField = "ct_name";
                    ddlProductType.DataValueField = "pkey_category_type";
                    ddlProductType.DataSource = model.ProductTypes.Select("fkey_self = " + ddlProductCat.SelectedValue);
                    ddlProductType.DataBind();
                }

                if (model.FreeProducts.Rows.Count > 0)
                {
                    FreeProductsModel.FreeProductsRow row = (FreeProductsModel.FreeProductsRow)model.FreeProducts.Rows[0];
                    ddlProductType.SelectedValue = row.fkey_product_type.ToString();
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int pKey = Convert.ToInt32(ViewState["pKey"]);
                ProductService.DeleteFreeProduct(pKey, ConnString);
                Response.Redirect("FreeProductsList.aspx");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            int pKey = Convert.ToInt32(ViewState["pKey"]);
            FreeProductsModel model = new FreeProductsModel();
            FreeProductsModel.FreeProductsRow row = model.FreeProducts.NewFreeProductsRow();
            row.pkey_free_product = pKey;
            row.fkey_product_type = Convert.ToInt32(ddlProductType.SelectedValue);
            row.valid_from = Convert.ToDateTime(txtFromDate.Text);
            row.valid_to = Convert.ToDateTime(txtToDate.Text);
            row.quantity = Convert.ToInt32(txtQuatity.Text);
            row.is_active = chkActive.Checked;
            row.fkey_fore_color_attr = Convert.ToInt32(ddlForeColor.SelectedValue);
            row.fkey_rev_color_attr = Convert.ToInt32(ddlReverseColor.SelectedValue);
            row.fkey_paper_type_attr = Convert.ToInt32(ddlPaperType.SelectedValue);
            row.shipping_cost = Convert.ToDecimal(txtShippingCost.Text);
            row.thumbnail_url = txtThumbnailUrl.Text;
            row.fkey_user = Identity.UserPK;
            row.ship_description = txtShippingDescr.Text;
            row.product_id = txtProductID.Text;
            row.product_name = txtProductName.Text;
            row.fkey_size_attr = Convert.ToInt32(ddlSize.SelectedValue);

            try
            {
                pKey = ProductService.UpdateFreeProduct(row, ConnString);
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                if (ex.Message.IndexOf("IX_PPS_PRODUCTS") > 0)
                {
                    AlertScript("Product Id must be unique. Please re-enter Product Id");
                }
                else
                {
                    AlertScript(ex.Message);
                }
                return;
            }
            Response.Redirect("FreeProductsList.aspx");
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("FreeProductsList.aspx");
        }

        protected void ddlProductCat_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                FreeProductsModel.ProductTypesDataTable productTypes = (FreeProductsModel.ProductTypesDataTable)ViewState["ProductTypes"];
                ddlProductType.DataTextField = "ct_name";
                ddlProductType.DataValueField = "pkey_category_type";
                ddlProductType.DataSource = productTypes.Select("fkey_self = " + ddlProductCat.SelectedValue);
                ddlProductType.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        private string GetAttributeKey(string variant_name, FreeProductsModel.ProductAttributesDataTable table)
        {
            string variant_key = Convert.ToString(WebConfiguration.ProductVariants[variant_name]);
            DataRow[] drs = table.Select("fkey_variant=" + variant_key);
            if (drs.Length > 0)
                return drs[0]["fkey_attribute"].ToString();
            return "";
        }
    }
}