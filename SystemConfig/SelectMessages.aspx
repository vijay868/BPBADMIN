﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SelectMessages.aspx.cs" Inherits="Bestprintbuy.Admin.Web.SystemConfig.SelectMessages" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Best Print Buy</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" media="all" href="../css/bestprintbuy_style.css" />
    <link rel="stylesheet" media="all" href="../css/jquery.fancybox.css" />
    <link rel="stylesheet" href="../css/jquery-ui.css" />
    <script src="../Scripts/jquery.js"></script>
    <script src="../Scripts/jquery-ui.js"></script>
    <script src="../Scripts/common.js"></script>
    <script src="../Scripts/ddsmoothmenu.js"></script>
    <script src="../Scripts/plugin/fancybox/jquery.fancybox.js"></script>
    <script src="../Scripts/jquery.validate.js"></script>
    <!--[if lt IE 9]>
			<script src="../Scripts/html5.js"></script>
		<![endif]-->
    <!--[if lte IE 7]> <html class="ie7"> <![endif]-->
    <!--[if IE 8]>     <html class="ie8"> <![endif]-->
    <!--[if IE 9]>     <html class="ie9"> <![endif]-->
    <!--[if IE 10]>     <html class="ie10"> <![endif]-->
    <!--[if !IE]><!-->
    <!--<![endif]-->
    <script type="text/javascript">

        function SetDataAndClose(isNodata, msgID, fields) {
            window.top.SetMessageTypeData(msgID, fields);
            return false;
        }

        function checkSelect() {
            var ischecked = false;
            $('.select input:checkbox').each(function () {
                if (this.checked) {
                    ischecked = true;
                    return true;
                }
            });

            if (!ischecked) {
                alert("Please select atleast one message.");
                return false;
            }
            return true;
        }
    </script>
</head>
<body id="Body" runat="server">
    <div id="Div1" class="popup-wrapper" runat="server">
        <div class="popup-title">
            Messages
        </div>
        <!-- Header Ends -->
        <div class="popup-contnt">
            <!-- Menu starts -->
            <form id="Form1" runat="server">
                <table class="tablep">
                    <thead>
                        <tr>
                            <th width="7%">#SL</th>
                            <th>Message</th>
                            <th width="10%">Select</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rptrMessages" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td><%# Container.ItemIndex+1 %></td>
                                    <td>
                                        <%#Eval("MESSAGE") %>
                                    </td>
                                    <td>
                                        <asp:HiddenField ID="hdnFields" runat="server" />
                                        <asp:HiddenField ID="txtFiled1" runat="server" Value='<%#Eval("pkey_message") %>' />
                                        <asp:CheckBox ID="chkSelect" runat="server" Checked='<%#Eval("isSeleced").ToString() != "" ? true : false %>' CssClass="select"/>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
                <table>
                    <tr>
                        <td style="float: right;">
                            <asp:Button runat="server" ID="btnSelect" CssClass="button rounded" Text="Select" OnClick="btnSelect_Click" OnClientClick="if(!checkSelect()) return false;"></asp:Button>
                            <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClientClick="window.top.ClosePopup(); return false;"></asp:Button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</body>
</html>
