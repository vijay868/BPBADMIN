﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="FreePromoDetails.aspx.cs" Inherits="Bestprintbuy.Admin.Web.SystemConfig.FreePromoDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        $().ready(function () {
            $("#frmFreePromoDetails").validate();
        });

        function moveProductOptions(option, moveAll) {
            fromObject = document.getElementById("ContentPlaceHolder1_lbAvailablePromotions");
            toObject = document.getElementById("ContentPlaceHolder1_lbAssignedPromotions");

            if (option == "right") {
                from = eval(fromObject);
                to = eval(toObject);
            }
            else {
                from = eval(toObject);
                to = eval(fromObject);
            }

            if (from && to) {
                var fromSize = from.options.length;
                var toSize = to.options.length;
                var fromArray = new Array();
                var fromSelectedArray = new Array();
                var fromSelectedIndex = 0;
                var fromIndex = 0;
                for (i = fromSize - 1; i > -1; i--) {
                    var isSelected = false
                    if (from.options[i].selected)
                        isSelected = from.options[i].selected;
                    if ((isSelected && isSelected == true) || moveAll == true) {
                        fromSelectedArray[fromSelectedIndex] = new Array(from.options[i].value, from.options[i].text);
                        from.options[i] = null;
                        fromSelectedIndex++;
                    }
                    else {
                        fromArray[fromIndex] = new Array(from.options[i].value, from.options[i].text);
                        from.options[i] = null;
                        fromIndex++;
                    }
                }
                fromSize = fromArray.length;
                for (i = 0; i < fromSize; i++) {
                    from.options[i] = new Option(fromArray[fromSize - 1 - i][1], fromArray[fromSize - 1 - i][0]);
                }
                var newToSize = fromSelectedArray.length;
                var strData = "";
                for (i = 0; i < newToSize; i++) {
                    toSize = to.options.length;
                    to.options[toSize] = new Option(fromSelectedArray[i][1], fromSelectedArray[i][0]);
                    strData = strData + to.options[toSize].value;
                    if (i != newToSize - 1) strData = strData + ",";
                }
            }
        }

        function moveShipmentsOptions(option, moveAll) {
            fromObject = document.getElementById("ContentPlaceHolder1_lbAvailableShipments");
            toObject = document.getElementById("ContentPlaceHolder1_lbAssignedShipments");

            if (option == "right") {
                from = eval(fromObject);
                to = eval(toObject);
            }
            else {
                from = eval(toObject);
                to = eval(fromObject);
            }

            if (from && to) {
                var fromSize = from.options.length;
                var toSize = to.options.length;
                var fromArray = new Array();
                var fromSelectedArray = new Array();
                var fromSelectedIndex = 0;
                var fromIndex = 0;
                for (i = fromSize - 1; i > -1; i--) {
                    var isSelected = false
                    if (from.options[i].selected)
                        isSelected = from.options[i].selected;
                    if ((isSelected && isSelected == true) || moveAll == true) {
                        fromSelectedArray[fromSelectedIndex] = new Array(from.options[i].value, from.options[i].text);
                        from.options[i] = null;
                        fromSelectedIndex++;
                    }
                    else {
                        fromArray[fromIndex] = new Array(from.options[i].value, from.options[i].text);
                        from.options[i] = null;
                        fromIndex++;
                    }
                }
                fromSize = fromArray.length;
                for (i = 0; i < fromSize; i++) {
                    from.options[i] = new Option(fromArray[fromSize - 1 - i][1], fromArray[fromSize - 1 - i][0]);
                }
                var newToSize = fromSelectedArray.length;
                var strData = "";
                for (i = 0; i < newToSize; i++) {
                    toSize = to.options.length;
                    to.options[toSize] = new Option(fromSelectedArray[i][1], fromSelectedArray[i][0]);
                    strData = strData + to.options[toSize].value;
                    if (i != newToSize - 1) strData = strData + ",";
                }
            }
        }

        function selectMemberRoles() {
            if (document.getElementById('ContentPlaceHolder1_lbAssignedPromotions')) {
                var leng = document.getElementById('ContentPlaceHolder1_lbAssignedPromotions').options.length;
                for (i = leng - 1; i > -1; i--) {
                    document.getElementById('ContentPlaceHolder1_lbAssignedPromotions').options[i].selected = true;
                }
            }

            if (document.getElementById('ContentPlaceHolder1_lbAssignedShipments')) {
                var leng = document.getElementById('ContentPlaceHolder1_lbAssignedShipments').options.length;
                for (j = leng - 1; j > -1; j--) {
                    document.getElementById('ContentPlaceHolder1_lbAssignedShipments').options[j].selected = true;
                }
            }
        }
    </script>
    <script type="text/javascript">
        function SetShipDiscount() {
            var option = $("#ContentPlaceHolder1_ddlOfferedOn option:selected").val();
            if (option == "B") {
                $('#ContentPlaceHolder1_txtShipmentDiscount').attr('required', '');
                $('#ContentPlaceHolder1_txtProductDiscount').attr('required', '');
            }
            else if (option == "S") {
                $('#ContentPlaceHolder1_txtShipmentDiscount').attr('required', '');
                $('#ContentPlaceHolder1_txtProductDiscount').removeAttr('required');
            }
            else {
                $('#ContentPlaceHolder1_txtShipmentDiscount').removeAttr('required');
                $('#ContentPlaceHolder1_txtProductDiscount').attr('required', '');
            }
        }

        function OpenTemplateDialog() {
            var templateOption = 0;
            if ($("#ContentPlaceHolder1_rbtnNone").is(':checked')) {
                alert("Template selection is not applicable for the selected option.");
                return false;
            }
            else if ($("#ContentPlaceHolder1_rbtnParentCategories").is(':checked')) {
                templateOption = 1;
            }
            else if ($("#ContentPlaceHolder1_rbtnTemplateTypes").is(':checked')) {
                templateOption = 2;
            }
            var promoKey = "<%= promotionKey%>";
            var productIds = "";
            if (promoKey != "-2") {
                productIds = "<%= fkeyProductType%>";
            } else {
                productIds = $("#ContentPlaceHolder1_ddlProdTypes option:selected").val();
            }

            var url = "../Customers/TemplateTypeSelect.aspx?promoKey=" + promoKey + "&producttypekeys=" + productIds + "&templateOption=" + templateOption + "&templateIds=" + $("#ContentPlaceHolder1_hdnTemplateTypeIds").val();

            $.fancybox({
                'width': '60%',
                'height': '80%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': url,
                'type': 'iframe'
            });
            return false;
        }

        function OpenProductDialog() {
            var promoKey = "<%= promotionKey%>";
            var productIds = "";
            var templateOption = "";
            if (promoKey != "-2") {
                productIds = "<%= fkeyProductType%>";
                templateOption = "<%= fkeyProductCat%>";
            } else {
                productIds = $("#ContentPlaceHolder1_ddlProdTypes option:selected").val();
                templateOption = $("#ContentPlaceHolder1_ddlCategory option:selected").val();
            }
            var url = "ProductSelect.aspx?promoKey=" + promoKey + "&producttypekeys=" + productIds + "&templateOption=" + templateOption + "&productIds=" + $("#ContentPlaceHolder1_hdnProductIds").val();
            $.fancybox({
                'width': '80%',
                'height': '80%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': url,
                'type': 'iframe'
            });
            return false;
        }

        function SetTemplateTypeData(teplateTypeIDs, templateTypeNames, teplateTypeCat) {
            $("#ContentPlaceHolder1_hdnTemplateTypeIds").val(teplateTypeIDs);
            $("#ContentPlaceHolder1_hdnTemplateTypeCat").val(teplateTypeCat);
            $("#ContentPlaceHolder1_hdnTemplateTypeNames").val(templateTypeNames.split(";:;").join("^"));
            $("#ContentPlaceHolder1_txtTemplateTypes").val(templateTypeNames.split(";:;").join("\n"));
            parent.$.fancybox.close();
        }

        function SetProductData(ProductIds, ProductNames, ProductCat) {
            $("#ContentPlaceHolder1_hdnProductIds").val(ProductIds);
            $("#ContentPlaceHolder1_hdnProductCat").val(ProductCat);
            $("#ContentPlaceHolder1_hdnProductNames").val(ProductNames.split(";:;").join("^"));
            $("#ContentPlaceHolder1_txtAssignedProducts").val(ProductNames.split(";:;").join("\n"));
            parent.$.fancybox.close();
        }

        function UpdateTemplateOptions() {
            $("#ContentPlaceHolder1_hdnTemplateTypeIds").val("");
            $("#ContentPlaceHolder1_txtTemplateTypes").val("");
        }
    </script>
    <script type="text/javascript">
        function deletePromotion() {
            if ('<%=HasAccess("FreeProductsList", "delete")%>' != 'False') {
                return confirm('This action will result in deleting the promotion. Do you want to continue?');
            }
            else {
                alert('User does not have access.');
                return false;
            }
        }
        function savePromotion() {
            if ('<%=HasAccess("FreeProductsList", promotionKey != -2 ? "edit" :"add")%>' == 'False') {
                alert('User does not have access.');
                return false;
            }
            else {
                selectMemberRoles();
                var dateStart = $("#ContentPlaceHolder1_txtFromDate").val();
                var dateEnd = $("#ContentPlaceHolder1_txtToDate").val();
                var option = $("#ContentPlaceHolder1_ddlOfferedOn option:selected").val();
                if (Date.parse(dateStart) > Date.parse(dateEnd)) {
                    alert("Invalid date range!");
                    return false;
                }
                if (($("#ContentPlaceHolder1_lbAssignedShipments option:selected").val() == undefined ||
                 $("#ContentPlaceHolder1_lbAssignedShipments option:selected").val() == "") && (option == "S" || option == "B")) {
                    alert("Atleast one Shipment must be selected.");
                    return false;
                }
                if ($("#ContentPlaceHolder1_rbtnProdPercentage").is(':checked') && $("#ContentPlaceHolder1_txtProductDiscount").val() > 100) {
                    alert('Product Discount Amount cannot exceed 100.');
                    return false;
                }
                if ($("#ContentPlaceHolder1_rbtnShipPercentage").is(':checked') && $("#ContentPlaceHolder1_txtShipmentDiscount").val() > 100) {
                    alert('Shipment Discount Amount cannot exceed 100.');
                    return false;
                }

                if (option == "D" && $('#ContentPlaceHolder1_txtShipmentDiscount').val() != "") {
                    alert('Shipment Discount Amount is not applicable for the selected Offer.');
                    return false;
                }
                if (option == "S" && $('#ContentPlaceHolder1_txtProductDiscount').val() != "") {
                    alert('Product Discount Amount is not applicable for the selected Offer.');
                    return false;
                }
            }
            return true;
        }
    </script>
    <script>
        $(function () {
            $("#ContentPlaceHolder1_txtFromDate").datepicker();
            $("#ContentPlaceHolder1_txtToDate").datepicker();
        });

    </script>
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
             <li class="liColor">System Configuration</li>
             <li><a href="FreePromoList.aspx">Free Promotions List</a></li>
            <li>Free Promotion Detail</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Free Promotion Details</h2>
        <form id="frmFreePromoDetails" name="frmFreePromoDetails" runat="server">
            <table width="100%" class="accounts-table">
                <thead>
                    <tr>
                        <th colspan="4" width="100%">Promotion Detail</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="15%">
                            <label>Promo Code*</label></td>
                        <td width="20%">
                            <asp:TextBox CssClass="text-box" name="txtPromoCode" ID="txtPromoCode" runat="server" required></asp:TextBox>
                        </td>
                        <td width="15%">&nbsp;
                        </td>
                        <td width="35%">&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Description*</label></td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtDescription" ID="txtDescription" runat="server" required></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <label>Offerred On* </label>
                        </td>
                        <td>
                            <asp:DropDownList name="ddlOfferedOn" ID="ddlOfferedOn" CssClass="slect-box" runat="server" onchange="SetShipDiscount(); return false;" required>
                                <asp:ListItem Value="D">Only Product Cost</asp:ListItem>
                                <asp:ListItem Value="S">Only Shipping</asp:ListItem>
                                <asp:ListItem Value="B">Product & Shipping</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <label>Product Discount Type*</label></td>
                        <td>
                            <asp:RadioButton ID="rbtnProdPercentage" runat="server" GroupName="productDiscounttype" /><asp:Label ID="Label3" runat="server" Text="  Percentage"></asp:Label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:RadioButton ID="rbtnProdFixed" runat="server" GroupName="productDiscounttype" /><asp:Label ID="Label4" runat="server" Text="  Fixed"></asp:Label>
                        </td>
                        <td>
                            <label>Shipment Discount Type*</label></td>
                        <td>
                            <asp:RadioButton ID="rbtnShipPercentage" runat="server" GroupName="ShipmentDiscounttype" /><asp:Label ID="Label1" runat="server" Text="  Percentage"></asp:Label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:RadioButton ID="rbtnShipFixed" runat="server" GroupName="ShipmentDiscounttype" /><asp:Label ID="Label2" runat="server" Text="  Fixed"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Product Discount* </label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box number" name="txtProductDiscount" ID="txtProductDiscount" runat="server" required></asp:TextBox>
                        </td>
                        <td>
                            <label>Shipment Discount*</label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box number" name="txtShipmentDiscount" ID="txtShipmentDiscount" runat="server" required></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>From Date* </label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box date" name="txtFromDate" ID="txtFromDate" runat="server" required></asp:TextBox>
                        </td>
                        <td>
                            <label>To Date*</label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box date" name="txtToDate" ID="txtToDate" runat="server" required></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Product Category* </label>
                        </td>
                        <td>
                            <asp:Label ID="lblProdCat" runat="server" Font-Size="14px" Visible="false"></asp:Label>
                            <asp:DropDownList name="ddlCategory" ID="ddlCategory" Visible="false" CssClass="slect-box" runat="server" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <label>Product Type*</label>
                        </td>
                        <td>
                            <asp:Label ID="lblProdType" runat="server" Visible="false" Font-Size="14px"></asp:Label>
                            <asp:DropDownList name="ddlProdTypes" ID="ddlProdTypes" Visible="false" CssClass="slect-box" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td colspan="3">
                            <asp:CheckBox ID="chkActive" CssClass="check-box" runat="server" /><asp:Label ID="Label5" runat="server" Text="   Active"></asp:Label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:CheckBox ID="chkNewCustomers" CssClass="check-box" runat="server" /><asp:Label ID="Label6" runat="server" Text="   Applicable for new customers only"></asp:Label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:CheckBox ID="chkSingleIteminCart" CssClass="check-box" runat="server" /><asp:Label ID="Label9" runat="server" Text="   Apply to single item in cart"></asp:Label>
                        </td>
                    </tr>
                </tbody>
            </table>

            <table width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="15%" style="vertical-align:top">
                            <label>Assigned Products</label></td>
                        <td width="35%">
                            <asp:HiddenField ID="hdnProductIds" runat="server" />
                            <asp:HiddenField ID="hdnProductNames" runat="server" />
                            <asp:HiddenField ID="hdnProductCat" runat="server" />
                            <asp:TextBox name="txtAssignedProducts" ID="txtAssignedProducts" CssClass="text-box" runat="server" ReadOnly="true" TextMode="MultiLine" Style="overflow-y: auto;resize:none" Rows="5" Columns="80"></asp:TextBox>
                        </td>
                        <td width="35%">
                            <asp:Button ID="btnSelectProductTypes" runat="server" CssClass="button rounded" Text="Select Products" UseSubmitBehavior="false" OnClientClick="OpenProductDialog(); return false;"></asp:Button>
                        </td>
                    </tr>
                    <tr>
                        <td width="15%">
                            <label>Template Options*</label></td>
                        <td width="35%">
                            <asp:RadioButton ID="rbtnNone" runat="server" value="0" GroupName="templateOptions" onclick="UpdateTemplateOptions();" /><asp:Label ID="Label7" runat="server" Text="    None"></asp:Label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                           <asp:RadioButton ID="rbtnParentCategories" value="1" runat="server" GroupName="templateOptions" /><asp:Label ID="Label8" runat="server" Text="   Parent Categories"></asp:Label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:RadioButton ID="rbtnTemplateTypes" value="2" runat="server" GroupName="templateOptions" /><asp:Label ID="Label10" runat="server" Text="    Template Types"></asp:Label>
                        </td>
                        <td width="35%"></td>
                    </tr>
                    <tr>
                        <td width="15%" style="vertical-align:top">
                            <label>Template Types</label></td>
                        <td width="35%">
                            <asp:HiddenField ID="hdnTemplateTypeIds" runat="server" />
                            <asp:HiddenField ID="hdnTemplateTypeNames" runat="server" />
                            <asp:HiddenField ID="hdnTemplateTypeCat" runat="server" />
                            <asp:TextBox name="txtTemplateTypes" ID="txtTemplateTypes" CssClass="text-box" runat="server" ReadOnly="true" TextMode="MultiLine" Style="overflow-y: auto;resize:none" Rows="5" Columns="80"></asp:TextBox>
                        </td>
                        <td width="35%">
                            <asp:Button ID="btnTemplatyeTypes" runat="server" CssClass="button rounded" Text="Select Template Types" UseSubmitBehavior="false" OnClientClick="OpenTemplateDialog(); return false;"></asp:Button>
                        </td>
                    </tr>
                </tbody>
            </table>

            <table width="100%" class="accounts-table">
                <thead>
                    <tr>
                        <th colspan="4" width="100%">Apply Shipping Options</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="15%">
                            <label></label>
                        </td>
                        <td width="50%">
                            <div style="width: 40%; float: left">
                                <label style="padding-left: 0px;"><b>Available Shipment</b></label>
                                <asp:ListBox ID="lbAvailableShipments" runat="server" CssClass="list-box" SelectionMode="Multiple"></asp:ListBox>
                            </div>
                            <div style="width: 10%; float: left; padding: 40px 10px 0 10px;">
                                <button id="Button1" type="button" class="button rounded width53" onclick="javascript:moveShipmentsOptions('right',false)">
                                    &nbsp;&gt;&nbsp;</button><br />
                                <button id="Button2" type="button" class="button rounded width53" onclick="javascript:moveShipmentsOptions('right',true)">
                                    &nbsp;&gt;&gt;&nbsp;</button><br />
                                <button id="Button3" type="button" class="button rounded width53" onclick="javascript:moveShipmentsOptions('left',false)">
                                    &nbsp;&lt;&nbsp;</button><br />
                                <button id="Button4" type="button" class="button rounded width53" onclick="javascript:moveShipmentsOptions('left',true)">
                                    &nbsp;&lt;&lt;&nbsp;</button>
                            </div>
                            <div style="width: 35%; float: left; padding-right: 10px;">
                                <label style="padding-left: 0px;"><b>Eligible Shipment</b></label>
                                <asp:ListBox ID="lbAssignedShipments" runat="server" CssClass="list-box" SelectionMode="Multiple"></asp:ListBox>
                            </div>
                        </td>
                        <td width="35%"></td>
                    </tr>
                </tbody>
            </table>

            <table width="100%" class="accounts-table">
                <thead>
                    <tr>
                        <th colspan="4" width="100%">Promotion Group</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="15%">
                            <label></label>
                        </td>
                        <td width="50%">
                            <div style="width: 40%; float: left">
                                <label style="padding-left: 0px;"><b>Available Promotions</b></label>
                                <asp:ListBox ID="lbAvailablePromotions" CssClass="list-box" runat="server" SelectionMode="Multiple"></asp:ListBox>
                            </div>
                            <div style="width: 10%; float: left; padding: 40px 10px 0 10px;">
                                <button id="to2" type="button" class="button rounded width53" onclick="javascript:moveProductOptions('right',false)">
                                    &nbsp;&gt;&nbsp;</button><br />
                                <button id="allTo2" type="button" class="button rounded width53" onclick="javascript:moveProductOptions('right',true)">
                                    &nbsp;&gt;&gt;&nbsp;</button><br />
                                <button id="to1" type="button" class="button rounded width53" onclick="javascript:moveProductOptions('left',false)">
                                    &nbsp;&lt;&nbsp;</button><br />
                                <button id="allTo1" type="button" class="button rounded width53" onclick="javascript:moveProductOptions('left',true)">
                                    &nbsp;&lt;&lt;&nbsp;</button>
                            </div>
                            <div style="width: 35%; float: left; padding-right: 10px;">
                                <label style="padding-left: 0px;"><b>Eligible Promotions</b></label>
                                <asp:ListBox ID="lbAssignedPromotions" runat="server" CssClass="list-box" SelectionMode="Multiple"></asp:ListBox>
                            </div>
                        </td>
                        <td width="35%"></td>
                    </tr>
                </tbody>
            </table>



            <table width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="15%">&nbsp;</td>
                        <td width="17%">&nbsp;</td>
                        <td width="15%">&nbsp;</td>
                        <td width="35%" style="float: right;">
                            <asp:Button runat="server" ID="btnDelete" Text="Delete" CssClass="button rounded" Visible="false" OnClientClick="return deletePromotion();" OnClick="btnDelete_Click"></asp:Button>
                            <asp:Button runat="server" CssClass="button rounded" ID="btnSave" Text="Save" OnClientClick="return savePromotion();" OnClick="btnSave_Click"></asp:Button>
                            <asp:Button runat="server" CssClass="button rounded" ID="btnClose" Text="Close" UseSubmitBehavior="false" OnClick="btnClose_Click"></asp:Button>
                        </td>
                    </tr>
                </tbody>
            </table>
            <!-- table Ends -->
        </form>
        <script>
            SetShipDiscount();
        </script>

    </div>
</asp:Content>
