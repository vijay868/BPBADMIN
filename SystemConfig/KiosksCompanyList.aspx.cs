﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.SystemConfig
{
    public partial class KiosksCompanyList : PageBase
    {
        protected int stateKey = -1;
        protected int kioskKey = 173;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                GetStatesData();
                LoadData();
            }
        }

        private void GetStatesData()
        {
            try
            {
                kioskKey = Convert.ToInt32(ddlkiosks.SelectedValue == "" ? "173" : ddlkiosks.SelectedValue);
                ExclusiveCustomerModel states = ExclusiveCustomerService.GetECStatesCOMBO(kioskKey, ConnString);
                ddlStates.DataSource = states.States;
                ddlStates.DataTextField = "state_name";
                ddlStates.DataValueField = "pkey_state";
                ddlStates.DataBind();
                ddlStates.SelectedValue = states.States.Rows[0]["pkey_state"].ToString();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        private void LoadData()
        {
            try
            {
                stateKey = Convert.ToInt32(ddlStates.SelectedValue);
                kioskKey = Convert.ToInt32(ddlkiosks.SelectedValue);
                ExclusiveCustomerModel companyList = ExclusiveCustomerService.GetCompanysLogoList(kioskKey, stateKey, ConnString);
                lblTotalRecords.Text = string.Format("Total Items in Cart:{0} ", companyList.ECCompanyLogos.Count);
                kioskcompanyGrid.DataSource = companyList.ECCompanyLogos;
                kioskcompanyGrid.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }


        protected int GetRecordCountForPage()
        {
            return GetRecordCountForPageSize(kioskcompanyGrid.PageSize, kioskcompanyGrid.PageIndex + 1);
        }

        protected void kioskcompanyGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            kioskcompanyGrid.PageIndex = e.NewPageIndex;
            LoadData();
        }
        protected void ddlkiosks_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        protected void ddlStates_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        protected void btnAddCompanyLogo_Click(object sender, EventArgs e)
        {
            try
            {
                kioskKey = Convert.ToInt32(ddlkiosks.SelectedValue);
                stateKey = Convert.ToInt32(ddlStates.SelectedValue);
                Response.Redirect("AddCompanyLogo.aspx?stateid=" + stateKey + "&KioskKey=" + kioskKey);
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnUploadLogoImages_Click(object sender, EventArgs e)
        {

        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                kioskKey = Convert.ToInt32(ddlkiosks.SelectedValue);
                stateKey = Convert.ToInt32(ddlStates.SelectedValue);
                ExclusiveCustomerModel model = new ExclusiveCustomerModel();
                foreach (GridViewRow row in kioskcompanyGrid.Rows)
                {
                    ExclusiveCustomerModel.ECCompanyLogosRow ECCompanyRow = model.ECCompanyLogos.NewECCompanyLogosRow();
                    CheckBox chk = (CheckBox)row.FindControl("chkIsactive");
                    HiddenField hdnpkeyxref = (HiddenField)row.FindControl("hdnpkeyxref");
                    HiddenField hdnPkeyecCompanyLogo = (HiddenField)row.FindControl("hdnPkeyecCompanyLogo");
                    HiddenField hdnecCompanyName = (HiddenField)row.FindControl("hdnecCompanyName");
                    HiddenField hdnMultilineLogo = (HiddenField)row.FindControl("hdnMultilineLogo");
                    HiddenField hdnLogoUrl = (HiddenField)row.FindControl("hdnLogoUrl");

                    ECCompanyRow.pkey_ec_comp_logo_state_xref = Convert.ToInt32(hdnpkeyxref.Value);
                    ECCompanyRow.pkey_ec_comp_logo = Convert.ToInt32(hdnPkeyecCompanyLogo.Value);
                    ECCompanyRow.ec_company_name = hdnecCompanyName.Value;
                    ECCompanyRow.is_active = chk.Checked;
                    ECCompanyRow.ismultilinelogo = Convert.ToBoolean(hdnMultilineLogo.Value);
                    ECCompanyRow.fkey_ec_attribute = kioskKey;
                    ECCompanyRow.logo_url = hdnLogoUrl.Value;

                    model.ECCompanyLogos.AddECCompanyLogosRow(ECCompanyRow);
                }

                ExclusiveCustomerModel companyList = ExclusiveCustomerService.UpdateCompanysLogoStatus(kioskKey, stateKey, model, ConnString);
                kioskcompanyGrid.DataSource = companyList.ECCompanyLogos;
                kioskcompanyGrid.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/DashBoard.aspx");
        }

        public string Gettable(object obj)
        {
            string sb = "";
            return sb = string.Format("<input name=\"ctl00$ContentPlaceHolder1$uploadLogo{0}\" type =\"file\" ID=\"ContentPlaceHolder1_uploadLogo{0}\"/>", obj.ToString());
        }

        protected void btnStartUpload_Click(object sender, EventArgs e)
        {

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }
    }
}