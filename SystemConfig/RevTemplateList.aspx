﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="RevTemplateList.aspx.cs" Inherits="Bestprintbuy.Admin.Web.SystemConfig.RevTemplateList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <script type="text/javascript">
         function addTemplate() {
             if ('<%=HasAccess("TemplateList", "add")%>' == 'False') {
                 alert('User does not have access.');
                 return false;
             }
             return true;
         }
    </script>
<div class="contant_hldr">
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
             <li class="liColor">System Configuration</li>
            <li>Reverse Template List</li>
        </ul>
        <h2>Reverse Template List</h2>
        <form class="" runat="server" id="frmRevTemplateList">           
            <div class="clr"></div>           
            <asp:GridView ID="RevTemplateListGrid" runat="server" AutoGenerateColumns="false" EnableViewCountry="true" AllowSorting="true"
                ForeColor="Black" Width="100%" PageSize="20" AllowPaging="true" OnPageIndexChanging="RevTemplateListGrid_PageIndexChanging"
                class="table" name="RevTemplateListGrid" OnSorting="RevTemplateListGrid_Sorting">
                <PagerStyle HorizontalAlign="Right" CssClass="dtPage"></PagerStyle>
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />
                <Columns>
                    <asp:TemplateField HeaderText="#SL">
                        <HeaderStyle ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <span><%# GetRecordCountForPage()%></span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Template Name" SortExpression="template_id">
                        <HeaderStyle Width="15%" ForeColor="White" />
                        <ItemTemplate>
                            <a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("TemplateDetail", "view")%>','RevTemplateDetail.aspx?fkeyRevTemplate=<%#Eval("pkey_reverse_template") %>')"  data-gravity="s" title="Edit"><%#Eval("template_id") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Product Type" SortExpression="product_type_name">
                        <HeaderStyle Width="15%" ForeColor="White" />
                        <ItemTemplate>
                          <a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("TemplateDetail", "view")%>','RevTemplateDetail.aspx?fkeyRevTemplate=<%#Eval("pkey_reverse_template") %>')"  data-gravity="s" title="Edit"><%#Eval("product_type_name") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Paper Type" SortExpression="paper_attribute_value">
                        <HeaderStyle Width="35%" ForeColor="White" />
                        <ItemTemplate>
                          <%#Eval("paper_attribute_value") %>
                        </ItemTemplate>
                    </asp:TemplateField> 
                    <asp:TemplateField HeaderText="Colour">
                        <HeaderStyle Width="15%" ForeColor="White" />
                        <ItemTemplate>
                          <%#Eval("color_attribute_value") %>
                        </ItemTemplate>
                    </asp:TemplateField>   
                     <asp:TemplateField HeaderText="Template Url">
                        <HeaderStyle Width="15%" ForeColor="White" />
                        <ItemTemplate>
                          <%# GetSelectedText(Eval("template_url")) %>
                        </ItemTemplate>
                    </asp:TemplateField>                 
                </Columns>
            </asp:GridView>
            <div class="ttl"><b>Total Number of Attributes: </b><%=recordCount%></div>
            <div class="btn_lst">
                <div class="lft">
                    <asp:Button runat="server" ID="btnAdd" CssClass="button rounded" Text="Add" OnClientClick="return addTemplate();" OnClick="btnAdd_Click"></asp:Button>
                    <asp:Button runat="server" ID="btnAddBackTemp" CssClass="button rounded" Text="BC Templates For Selection After Approve Design" OnClick="btnAddBackTemp_Click"></asp:Button>
                </div>
                <div class="rgt">
                    <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClick="btnClose_Click"></asp:Button>
                </div>
            </div>
        </form>
    </div>
</asp:Content>

