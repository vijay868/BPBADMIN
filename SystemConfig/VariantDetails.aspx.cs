﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;

namespace Bestprintbuy.Admin.Web.SystemConfig
{
    public partial class VariantDetails : PageBase
    {
        protected int variantkey;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ViewState["variantKey"] = Convert.ToInt32(Request.QueryString["variantKey"]);
                variantkey = Convert.ToInt32(ViewState["variantKey"]);
                BindCategories();
                GetData();
            }
        }

        private void BindCategories()
        {
            try
            {
                ddlCategory.DataSource = ProductService.GetProductCategories(ConnString);
                ddlCategory.DataTextField = "ct_name";
                ddlCategory.DataValueField = "pkey_category_type";
                ddlCategory.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        private void GetData()
        {
            try
            {
                int variantKey = Convert.ToInt32(ViewState["variantKey"]);
                string category = ddlCategory.SelectedValue;
                string productTypes = "-2";
                ProductModel variantDetails = ProductService.GetVariantDetails(variantKey, productTypes, category, ConnString);

                lbAvailableProductTypes.DataSource = variantDetails.AvailableProductTypes;
                lbAvailableProductTypes.DataTextField = "type_name";
                lbAvailableProductTypes.DataValueField = "pkey_type";
                lbAvailableProductTypes.SelectionMode = ListSelectionMode.Multiple;
                lbAvailableProductTypes.DataBind();

                if (variantDetails.Variants.Rows.Count > 0)
                {
                    ProductModel.VariantsRow variantRow = (ProductModel.VariantsRow)variantDetails.Variants.Rows[0];
                    txtVariantCode.Text = variantRow.variant_code;
                    txtVariantName.Text = variantRow.variant_name;
                    txtComments.Text = variantRow.comments;
                    btnDelete.Visible = true;
                }

                if (variantDetails.ApplicableProductTypes != null)
                {
                    lbApplicableProductTypes.DataSource = variantDetails.ApplicableProductTypes;
                    lbApplicableProductTypes.DataTextField = "type_name";
                    lbApplicableProductTypes.DataValueField = "pkey_type";
                    lbApplicableProductTypes.SelectionMode = ListSelectionMode.Multiple;
                    lbApplicableProductTypes.DataBind();
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int variantKey = Convert.ToInt32(ViewState["variantKey"]);
                ProductModel VariantDetail = new ProductModel();
                ProductModel.VariantsRow variant = VariantDetail.Variants.NewVariantsRow();
                variant.pkey_variant = variantKey;
                variant.variant_code = txtVariantCode.Text;
                variant.variant_name = txtVariantName.Text;
                variant.comments = txtComments.Text;
                variant.fkey_user = Identity.UserPK;
                VariantDetail.Variants.AddVariantsRow(variant);

                string ApplicableProductTypes = "";
                if (Request["ctl00$ContentPlaceHolder1$lbApplicableProductTypes"] != null)
                    ApplicableProductTypes = Request["ctl00$ContentPlaceHolder1$lbApplicableProductTypes"].ToString();

                if (ApplicableProductTypes != "")
                {
                    foreach (string item in ApplicableProductTypes.Split(','))
                    {
                        ProductModel.ApplicableProductTypesRow assignedProductsRow = VariantDetail.ApplicableProductTypes.NewApplicableProductTypesRow();
                        assignedProductsRow.pkey_type = Convert.ToInt32(item == "" ? "-1" : item);
                        assignedProductsRow.fkey_user = Identity.UserPK;
                        //assignedProductsRow.pkey_variant_type_xref = -2;
                        assignedProductsRow.fkey_variant = variantKey;
                        VariantDetail.ApplicableProductTypes.AddApplicableProductTypesRow(assignedProductsRow);
                    }
                }

                variantKey = ProductService.UpdateVariant(VariantDetail, ConnString);
                ViewState["variantKey"] = variantKey;
                AlertScript("Variant Details Saved Successfully.");
                GetData();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript("Variant Details Save failed. try again.");
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("VariantList.aspx");
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int variantKey = Convert.ToInt32(ViewState["variantKey"]);
                ProductService.DeleteVariant(variantKey, ConnString);
                Response.Redirect("VariantList.aspx");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetData();
        }
    }
}