﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.SystemConfig
{
    public partial class ECInfoList : PageBase
    {
        protected int recordCount = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadData();
            }
        }

        private void LoadData()
        {
            try
            {
            int variantKey = Convert.ToInt32(WebConfiguration.ProductVariants["ExclusiveCustomer"]);
            int attributeKey = -1;
            ProductModel attributeList = ProductService.GetAttributeList(attributeKey, variantKey, ConnString);
            if (ECInfoListGrid.Attributes["SortExpression"] != null)
                attributeList.Attributes.DefaultView.Sort = ECInfoListGrid.Attributes["SortExpression"] + " " + ECInfoListGrid.Attributes["SortDirection"];
            recordCount = attributeList.Attributes.Rows.Count;
            ECInfoListGrid.DataSource = attributeList.Attributes;
            ECInfoListGrid.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void ECInfoListGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            SortGridView(ECInfoListGrid, e);
            ECInfoListGrid.PageIndex = 0;
            LoadData();
        }

        protected void ECInfoListGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ECInfoListGrid.PageIndex = e.NewPageIndex;
            LoadData();
        }

        protected int GetRecordCountForPage()
        {
            return GetRecordCountForPageSize(ECInfoListGrid.PageSize, ECInfoListGrid.PageIndex + 1);
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/DashBoard.aspx");
        }
    }
}