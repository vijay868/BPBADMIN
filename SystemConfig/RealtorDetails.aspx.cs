﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.SystemConfig
{
    public partial class RealtorDetails : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ViewState["Pkey"] = Convert.ToInt32(Request.QueryString["Pkey"]);
                GetData();
            }
        }

        private void GetData()
        {
            try
            {
                int ExclusiveCustomerKey = Convert.ToInt32(WebConfiguration.ProductVariants["ExclusiveCustomer"]);
                RealtorsModel model = ProductService.GetRealtorsData(Convert.ToInt32(ViewState["Pkey"]), ExclusiveCustomerKey, ConnString);

                ddlCustomer.Items.Clear();
                ddlCustomer.DataSource = model.PRODUCT_ATTRIBUTES;
                ddlCustomer.DataTextField = "attribute_value";
                ddlCustomer.DataValueField = "pkey_attribute";
                ddlCustomer.DataBind();
                ddlCustomer.Items.Insert(0, new ListItem("No Option", "-1"));

                if (model.Realtors.Rows.Count > 0)
                {
                    RealtorsModel.RealtorsRow row = (RealtorsModel.RealtorsRow)model.Realtors.Rows[0];
                    txtRealtorID.Text = row.realtor_id;
                    txtRealtorName.Text = row.realtor_name;
                    chkActive.Checked = row.IS_ACTIVE;
                    chkExclusiveCustomer.Checked = row.IS_EXCLUSIVE_CUSTOMER;
                    if (!row.IsFKEY_EC_ATTRIBUTENull())
                        ddlCustomer.SelectedValue = row.FKEY_EC_ATTRIBUTE.ToString();
                }
                else
                {
                    chkActive.Checked = true;
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                RealtorsModel model = new RealtorsModel();
                RealtorsModel.RealtorsRow row = model.Realtors.NewRealtorsRow();
                row.pkey_realtor = Convert.ToInt32(ViewState["Pkey"]);
                row.realtor_id = txtRealtorID.Text;
                row.realtor_name = txtRealtorName.Text;
                row.IS_ACTIVE = chkActive.Checked;
                row.IS_EXCLUSIVE_CUSTOMER = chkExclusiveCustomer.Checked;
                if (Convert.ToInt32(ddlCustomer.SelectedValue) > 0)
                    row.FKEY_EC_ATTRIBUTE = Convert.ToInt32(ddlCustomer.SelectedValue);
                model.Realtors.AddRealtorsRow(row);
                int key = ProductService.UpdateRealtor(model, ConnString);
                if (key > 0)
                    Response.Redirect("RealtorsList.aspx");
                else
                    AlertScript("There is some problem. Please try again");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("RealtorsList.aspx");
        }
    }
}