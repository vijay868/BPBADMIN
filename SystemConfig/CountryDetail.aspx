﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="CountryDetail.aspx.cs" Inherits="Bestprintbuy.Admin.Web.SystemConfig.CountryDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function saveCountry() {
            if ('<%=HasAccess("VariantList", pkeycountry != -2 ? "edit" : "add")%>' == 'False') {
                 alert('User does not have access.');
                 return false;
             }
             return true;
         }
         $().ready(function () {
             $("#frmCountryDetail").validate();
         });
    </script>
    <div class="contant_hldr">
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">System Configuration</li>
            <li><a href="CountryList.aspx">Country Lists</a></li>
            <li>Country Details</li>
        </ul>
        <h2>Country Details</h2>
        <form class="" runat="server" id="frmCountryDetail">
            <table cellpadding="0" cellspacing="0" border="0" class="accounts-table">
                <tr>
                    <td width="15%">
                        <label>Country ID*</label></td>
                    <td width="20%">
                        <asp:TextBox name="txtCountryID" CssClass="text-box" ID="txtCountryID" MaxLength="3" runat="server" required></asp:TextBox></td>
                    <td width="50%"></td>
                </tr>
                <tr>
                    <td>
                        <label>Country Name*</label></td>
                    <td>
                        <asp:TextBox name="txtCountryName" CssClass="text-box" ID="txtCountryName" runat="server" required></asp:TextBox></td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <label>Is Default</label></td>
                    <td>
                        <asp:CheckBox ID="chkIsDefault" runat="server" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td style="text-align: right;">
                        <asp:Button ID="btnSave" CssClass="button rounded" runat="server" OnClientClick="return saveCountry();" Text="Save" OnClick="btnSave_Click" />
                        <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClick="btnClose_Click" UseSubmitBehavior="false"></asp:Button>
                    </td>
                    <td></td>
                </tr>
            </table>
        </form>
    </div>
</asp:Content>
