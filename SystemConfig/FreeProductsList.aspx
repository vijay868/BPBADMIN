﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="FreeProductsList.aspx.cs" Inherits="Bestprintbuy.Admin.Web.SystemConfig.FreeProductsList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function addProduct() {
            if ('<%=HasAccess("FreeProductsList", "add")%>' == 'False') {
                 alert('User does not have access.');
                 return false;
             }
             return true;
         }
         function OpenSupplierDialog(freeproductkey, parentID) {
             var url = "../SystemConfig/DefineSupplierPrices.aspx?productkey=" + freeproductkey + "&parentID=" + parentID;
             $.fancybox({
                 'width': '70%',
                 'height': '40%',
                 'autoScale': true,
                 'transitionIn': 'fade',
                 'transitionOut': 'fade',
                 'href': url,
                 'type': 'iframe'
             });
             return false;
         }

         function SetSupplierData(count, parentID) {
             var text = "+Add Suppliers";
             if (count > 0)
                 text = "Suppliers(" + count + ")";
             $("#" + parentID).text(text);
             parent.$.fancybox.close();
         }
    </script>
    <div class="clr"></div>
    <!-- Container Starts -->
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">System Configuration</li>
            <li>Free Products List</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Free Products List</h2>
        <form id="fmFreeProductsList" name="fmFreeProductsList" runat="server" class="form">            
            <div class="clr"></div>           
            <asp:GridView ID="FreeProductslistGrid" runat="server" AutoGenerateColumns="false" EnableViewState="true"
                ForeColor="Black" Width="100%" PageSize="20" AllowPaging="true" AllowSorting="true"
                class="table" name="FreeProductslistGrid" OnPageIndexChanging="FreeProductslistGrid_PageIndexChanging" OnSorting="FreeProductslistGrid_Sorting">
                <PagerStyle HorizontalAlign="Right" CssClass="dtPage"></PagerStyle>
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />
                <Columns>
                    <asp:TemplateField HeaderText="#SL">
                        <HeaderStyle Width="6%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <span><%# GetRecordCountForPage()%></span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Product ID">
                        <ItemTemplate>
                            <a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("FreeProductDetail", "view")%>','FreeProductDetail.aspx?pKey=<%# DataBinder.Eval(Container,"DataItem.pkey_free_product") %>')" data-gravity="s" title="Edit"><%# DataBinder.Eval(Container,"DataItem.product_id") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>                    
                    <asp:TemplateField HeaderText="Product Description">
                        <ItemTemplate>
                            <%# GetSelectedText(DataBinder.Eval(Container,"DataItem.product_description")) %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Valid From">
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container,"DataItem.valid_from","{0:MM/dd/yyyy}") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Valid To">
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container,"DataItem.valid_to","{0:MM/dd/yyyy}") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Quantity">
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container,"DataItem.quantity") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Active">
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container,"DataItem.is_active").ToString() == "True" ? "Active" : "Inactive" %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Suppliers">
                        <ItemTemplate>
                            <a href="#" onclick="OpenSupplierDialog('<%# DataBinder.Eval(Container,"DataItem.pkey_free_product") %>', this.id);" id="aSupplier<%# Container.DataItemIndex%>" title="Edit"><%#Convert.ToInt32(DataBinder.Eval(Container,"DataItem.suppliers")) > 0 ? "Suppliers("+DataBinder.Eval(Container,"DataItem.suppliers")+")" : "+Add Suppliers" %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <div class="ttl"><b>Total Number of Products: </b><%=recordCount%></div>
             <div class="btn_lst">
                 <div class="lft">
                      <asp:Button ID="btnAdd" Text="Add Free Product" CssClass="button rounded" runat="server" OnClientClick="return addProduct();" OnClick="btnAdd_Click"></asp:Button>
                 </div>
                <div class="rgt">
                    <asp:Button ID="btnClose" Text="Close" CssClass="button rounded" runat="server" OnClick="btnClose_Click"></asp:Button>
                </div>
            </div>
        </form>
    </div>
</asp:Content>
