﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.SystemConfig
{
    public partial class TypeList : PageBase
    {
        protected int pkeyMasterCategory = -1;
        protected int pkeyCategory = -1;
        protected int pkey_type = -1;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                GetMasterCategories();
                ddlMasterCategory.SelectedValue = Request.QueryString["pkeyMaster"] == null ? "14" : Request.QueryString["pkeyMaster"];
                GetCategories();
                LoadData();
            }
        }

        private void GetCategories()
        {
            try
            {
                var catType = from collect in SupportTypesService.GetCategories(ConnString)
                              where collect.fkey_master_category == Convert.ToInt32(ddlMasterCategory.SelectedValue)
                              select collect;
                ddlCategory.DataSource = catType.ToList();
                ddlCategory.DataTextField = "ct_name";
                ddlCategory.DataValueField = "pkey_category_type";
                ddlCategory.DataBind();
                ddlCategory.Items.Insert(0, new ListItem("No Option", "-1"));
                ddlCategory.SelectedValue = Request.QueryString["pkeyCategory"] == null ? "-1" : Request.QueryString["pkeyCategory"];
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        private void LoadData()
        {
            try
            {
                //pkey_type = Convert.ToInt32(Request.QueryString["pkeytype"] == null ? "-1" : Request.QueryString["pkeytype"]);
                pkeyMasterCategory = Convert.ToInt32(ddlMasterCategory.SelectedValue);
                pkeyCategory = Convert.ToInt32(ddlCategory.SelectedValue == "" ? "-1" : ddlCategory.SelectedValue);
                TypeModel TypesList = SupportTypesService.GetTypesList(pkey_type, pkeyMasterCategory, pkeyCategory, ConnString);
                lblTotalRecords.Text = string.Format(" Total Number of Types: {0}", TypesList.Types.Rows.Count);
                if (TypesList.Types != null)
                {
                    TypesGrid.DataSource = TypesList.Types;
                    TypesGrid.DataBind();
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        private void GetMasterCategories()
        {
            try
            {
                ddlMasterCategory.DataSource = SupportTypesService.GetMasterCategories(ConnString);
                ddlMasterCategory.DataTextField = "mc_name";
                ddlMasterCategory.DataValueField = "pkey_master_category";
                ddlMasterCategory.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected int GetRecordCountForPage()
        {
            return GetRecordCountForPageSize(TypesGrid.PageSize, TypesGrid.PageIndex + 1);
        }

        protected void TypesGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                TypesGrid.PageIndex = e.NewPageIndex;
                LoadData();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("TypeDetail.aspx?pkeytype=-2&pkeyMaster=" + ddlMasterCategory.SelectedValue + "&pkeyCategory=" + ddlCategory.SelectedValue);
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/DashBoard.aspx");
        }

        protected void ddlMasterCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetCategories();
        }

        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbtn = (LinkButton)sender;
                int pkeytype = Convert.ToInt32(lbtn.CommandArgument);
                SupportTypesService.Delete(pkeytype, ConnString);
                Response.Redirect("TypeList.aspx");
            }
            catch (Exception ex)
            {
                if (ex.Message.IndexOf("FK_PPS_PRODUCT_TEMPLATE_TYPE_XREF_PPS_CATEGORIES_TYPES") > -1)
                {
                    FileLogger.WriteException(ex);
                    AlertScript("Cannot delete record");
                }
                else
                {
                    FileLogger.WriteException(ex);
                    AlertScript(ex.Message);
                }
            }
        }

        protected void btnCtAbbr_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbtn = (LinkButton)sender;
                int pkeytype = Convert.ToInt32(lbtn.CommandArgument);
                Response.Redirect("TypeDetail.aspx?pkeytype=" + pkeytype + "&pkeyMaster=" + ddlMasterCategory.SelectedValue + "&pkeyCategory=" + ddlCategory.SelectedValue);
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnCtName_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbtn = (LinkButton)sender;
                int pkeytype = Convert.ToInt32(lbtn.CommandArgument);
                Response.Redirect("TypeDetail.aspx?pkeytype=" + pkeytype + "&pkeyMaster=" + ddlMasterCategory.SelectedValue + "&pkeyCategory=" + ddlCategory.SelectedValue);
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void TypesGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }
    }
}