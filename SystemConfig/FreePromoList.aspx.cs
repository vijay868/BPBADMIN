﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Collections.Specialized;
using System.Text;

namespace Bestprintbuy.Admin.Web.SystemConfig
{
    public partial class FreePromoList : PageBase
    {
        protected int serialNo;
        protected string searchfor = string.Empty;
        protected int recordCount = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadData();
            }
        }

        //private  promotionsList = null;
        private void LoadData()
        {
            try
            {
                PromotionModel promotionsList = PromoService.GetListFor_free_promo(searchfor, ConnString);
                ViewState["Plist"] = promotionsList;

                if (FreePromotionslistGrid.Attributes["SortExpression"] != null)
                    promotionsList.Promotions.DefaultView.Sort = FreePromotionslistGrid.Attributes["SortExpression"] + " " + FreePromotionslistGrid.Attributes["SortDirection"];
                recordCount = promotionsList.Promotions.Rows.Count;
                FreePromotionslistGrid.DataSource = promotionsList.Promotions.DefaultView;
                FreePromotionslistGrid.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void FreePromotionslistGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            SortGridView(FreePromotionslistGrid, e);
            FreePromotionslistGrid.PageIndex = 0;
            searchfor = GetSearchCondition();
            LoadData();
        }

        protected void FreePromotionslistGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            FreePromotionslistGrid.PageIndex = e.NewPageIndex;
            searchfor = GetSearchCondition();
            LoadData();
        }

        protected int GetRecordCountForPage()
        {
            return GetRecordCountForPageSize(FreePromotionslistGrid.PageSize, FreePromotionslistGrid.PageIndex + 1);
        }

        protected void btnAddPromotion_Click(object sender, EventArgs e)
        {
            Response.Redirect("FreePromoDetails.aspx?promoKey=-2");
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/DashBoard.aspx");
        }

        public string GetApplicability(object pkey_promotion)
        {
            PromotionModel promotionsList = (PromotionModel)ViewState["Plist"]; //PromoService.GetPromotionList(ConnString);
            string products = "";
            try
            {

                if (promotionsList != null && promotionsList.AssignedProductTypes != null)
                {
                    DataRow[] rows = promotionsList.AssignedProductTypes.Select("fkey_promo = " + pkey_promotion.ToString());
                    if (rows != null)
                        for (int i = 0; i < rows.Length; i++)
                        {
                            PromotionModel.AssignedProductTypesRow row = (PromotionModel.AssignedProductTypesRow)rows[i];
                            products += row.ct_name;
                            if (rows.Length - 1 != 0)
                                products += "<br/><div style=\"border-bottom:0px solid #eb018b;\"></div>";
                        }
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
            return products;
        }

        public string GetShipment(object pkey_promotion)
        {
            PromotionModel promotionsList = (PromotionModel)ViewState["Plist"]; //PromoService.GetPromotionList(ConnString);
            string products = "";
            try
            {

                if (promotionsList != null && promotionsList.AssignedShipments != null)
                {
                    DataRow[] rows = promotionsList.AssignedShipments.Select("fkey_promo = " + pkey_promotion.ToString());
                    if (rows != null)
                        for (int i = 0; i < rows.Length; i++)
                        {
                            PromotionModel.AssignedShipmentsRow row = (PromotionModel.AssignedShipmentsRow)rows[i];
                            products += row.shipment_description;
                            if (rows.Length - 1 != 0)
                                products += "<br/><div style=\"border-bottom:0px solid #eb018b;\"></div>";
                        }
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
            return products;
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            try
            {
                FreePromotionslistGrid.PageIndex = 0;
                searchfor = GetSearchCondition();
                LoadData();
                btnReset.Visible = true;
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                FreePromotionslistGrid.PageIndex = 0;
                btnReset.Visible = false;
                ddlOperator.SelectedIndex = 0;
                ddlFilterFor.SelectedIndex = 0;
                txtPattern.Text = "";
                LoadData();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        public String GetSearchCondition()
        {
            //Variable holding the current session object
            System.Web.SessionState.HttpSessionState currsess = System.Web.HttpContext.Current.Session;

            //Variable holding the parameters - values from the request in a named value collection
            NameValueCollection requestCollections = System.Web.HttpContext.Current.Request.Params;

            //Get the Critera , pattern, operator from the request
            String filterOption = ddlStatus.SelectedValue;
            String criteria = ddlFilterFor.SelectedValue;
            String roperator = ddlOperator.SelectedValue;
            String pattern = txtPattern.Text.Trim();

            //Check if pattern exists
            if ("".Equals(pattern)) return "";

            //Create a new instance of named value collection
            NameValueCollection nvc = new NameValueCollection();

            //Add the Critera , pattern, operator to the nvc
            nvc.Add("filterOption", filterOption);
            nvc.Add("criteria", criteria);
            nvc.Add("operator", roperator);
            nvc.Add("pattern", pattern);
            currsess["combovalues"] = nvc;

            String searchfor = FormSearchString(nvc);
            currsess["search_condition"] = searchfor;
            currsess["pattern_value"] = pattern;

            return searchfor;
        }

        private String FormSearchString(NameValueCollection nvc)
        {
            String sOperator = nvc["operator"];
            sOperator = sOperator != null ? sOperator.Trim() : sOperator;
            string pattern = nvc["pattern"];
            string filterOption = nvc["filterOption"];
            StringBuilder sb = new StringBuilder();


            if (sOperator.IndexOf("LIKE") < 0)
            {
                pattern = pattern.Replace("'", "''");
                sb.Append(nvc["criteria"]).Append(" ").Append(sOperator).Append(" ").Append("'").Append(pattern).Append("'");
            }
            else if (sOperator != null && (sOperator.ToLower()).IndexOf("in") > 0)
            {
                sb.Append(nvc["criteria"]).Append("  ").Append(sOperator).Append(" (").Append(pattern).Append(")");
            }
            else
            {
                sOperator = sOperator.Replace("LIKE", pattern);
                sOperator = sOperator.Replace("'", "''");
                sb.Append(nvc["criteria"]).Append(" LIKE '").Append(sOperator).Append("'");
            }
            if (filterOption == "2")
            {
                sb.Append(" and is_active = 1");
            }
            else if (filterOption == "3")
            {
                sb.Append(" and is_active = 0");
            }
            return sb.ToString();
        }
    }
}