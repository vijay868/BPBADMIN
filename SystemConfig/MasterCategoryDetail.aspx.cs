﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;
namespace Bestprintbuy.Admin.Web.SystemConfig
{
    public partial class MasterCategoryDetail : PageBase
    {
        protected int mastercatKey = -2;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetDetails();
            }
        }

        private void GetDetails()
        {
            try
            {
                mastercatKey = Convert.ToInt32(Request.QueryString["mastercatKey"] == "" ? "-2" : Request.QueryString["mastercatKey"]);
                TypeModel masterCategoryDtls = SupportTypesService.GetMasterCategoryDetails(mastercatKey, ConnString);
                if (masterCategoryDtls.MasterCategories.Rows.Count > 0)
                {
                    TypeModel.MasterCategoriesRow row = (TypeModel.MasterCategoriesRow)masterCategoryDtls.MasterCategories.Rows[0];
                    lblDefaultName.Text = row.fixed_name;
                    txtDefaultDesc.Text = row.fixed_comments;
                    txtDisplayName.Text = row.mc_name;
                    txtDisplayDesc.Text = row.comments;
                }
                if (masterCategoryDtls.Screens.Rows.Count > 0)
                {
                    rptrCategorylist.DataSource = masterCategoryDtls.Screens;
                    rptrCategorylist.DataBind();
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                TypeModel model = new TypeModel();
                TypeModel.MasterCategoriesRow row = (TypeModel.MasterCategoriesRow)model.MasterCategories.NewMasterCategoriesRow();
                row.pkey_mastercategory = Convert.ToInt32(Request.QueryString["mastercatKey"] == "" ? "-2" : Request.QueryString["mastercatKey"]);
                row.mc_name = txtDisplayName.Text;
                row.comments = txtDisplayDesc.Text;
                row.fkey_user = Identity.UserPK;
                model.MasterCategories.AddMasterCategoriesRow(row);
                mastercatKey = SupportTypesService.UpdateMasterCategoryDetails(model, ConnString);
                Response.Redirect("MasterCategoryList.aspx");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript("Save/Update failed. try again.");
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("MasterCategoryList.aspx");
        }
    }
}