﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="TemplateList.aspx.cs" Inherits="Bestprintbuy.Admin.Web.SystemConfig.TemplateList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function addTemplate() {
            if ('<%=HasAccess("TemplateList", "add")%>' == 'False') {
                alert('User does not have access.');
                return false;
            }
            return true;
        }
    </script>
    <div class="clr"></div>
    <!-- Container Starts -->
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
             <li class="liColor">System Configuration</li>
            <li>Template List</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Template List</h2>
        <form id="frmTemplateList" name="frmTemplateList" runat="server" class="form">
            <table cellpadding="0" cellspacing="0" border="0" width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="7%">Product Category </td>
                        <td width="15%">
                            <asp:DropDownList name="ddlProductCategory" ID="ddlProductCategory" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlProductCategory_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td width="7%">Template Category</td>
                        <td width="15%">
                            <asp:DropDownList name="ddlTemplateCategory" ID="ddlTemplateCategory" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlTemplateCategory_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td width="1%"></td>
                        <td width="7%">Template Type</td>
                        <td width="15%">
                            <asp:DropDownList name="ddlTemplateType" ID="ddlTemplateType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlTemplateType_SelectedIndexChanged">
                            <asp:ListItem Text="No Option" Value="" />
                            </asp:DropDownList></td>

                        <td width="25%"></td>
                    </tr>
                </tbody>
            </table>           
            <div class="clr"></div>
            <asp:GridView ID="TemplateListGrid" runat="server" AutoGenerateColumns="false" EnableViewState="true"
                ForeColor="Black" Width="100%" PageSize="20" AllowPaging="true" AllowSorting="true"
                CssClass="table" name="TemplateListGrid" OnPageIndexChanging="TemplateListGrid_PageIndexChanging" OnSorting="CustomerslistGrid_Sorting">
                <PagerStyle HorizontalAlign="Right" CssClass="dtPage"></PagerStyle>
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />
                <Columns>
                    <asp:TemplateField HeaderText="#SL">
                        <HeaderStyle Width="6%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <span><%# GetRecordCountForPage()%></span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Product Category & Type">
                        <ItemTemplate>
                            <a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("TemplateDetail", "view")%>','TemplateDetail.aspx?pkeyTemplate=<%# DataBinder.Eval(Container,"DataItem.pkey_template_master") %>&fkeyProductType=<%# DataBinder.Eval(Container,"DataItem.fkey_product_type") %>')" data-gravity="s" title="Edit"><%# DataBinder.Eval(Container,"DataItem.product_category_type") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Template Category & Type">
                        <ItemTemplate>
                            <a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("TemplateDetail", "view")%>','TemplateDetail.aspx?pkeyTemplate=<%# DataBinder.Eval(Container,"DataItem.pkey_template_master") %>&fkeyProductType=<%# DataBinder.Eval(Container,"DataItem.fkey_product_type") %>')" data-gravity="s" title="Edit"><%# DataBinder.Eval(Container,"DataItem.product_category_type") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Favourites">
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container,"DataItem.is_favourite") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Location">
                        <ItemTemplate>
                            <%# GetSelectedText(DataBinder.Eval(Container,"DataItem.template_url")) %>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <div class="ttl"><b>Total Number of Templates: </b><%=recordCount%></div>
            <!-- table Ends -->
            <div class="btn_lst">
                <div class="lft">
                    <asp:Button runat="server" ID="btnAdd" CssClass="button rounded" Text="Add" OnClientClick="return addTemplate();" OnClick="btnAdd_Click"></asp:Button>
                </div>
                <div>
                    <asp:Button ID="btnClose" Text="Close" CssClass="button rounded" runat="server" OnClick="btnClose_Click"></asp:Button>
                </div>
            </div>
        </form>
    </div>
</asp:Content>
