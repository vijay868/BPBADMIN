﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="RealtorsList.aspx.cs" Inherits="Bestprintbuy.Admin.Web.SystemConfig.RealtorsList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="contant_hldr">
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
             <li class="liColor">System Configuration</li>
            <li>Realtors List</li>
        </ul>
        <h2>Realtors List</h2>
        <form class="" runat="server" id="frmRealtorsList">           
            <div class="clr"></div>           
            <asp:GridView ID="RealtorsListGrid" runat="server" AutoGenerateColumns="false" EnableViewCountry="true" AllowSorting="true"
                ForeColor="Black" Width="100%" PageSize="20" AllowPaging="true" OnPageIndexChanging="RealtorsListGrid_PageIndexChanging"
                class="table" name="RealtorsListGrid" OnSorting="RealtorsListGrid_Sorting">
                <PagerStyle HorizontalAlign="Right" CssClass="dtPage"></PagerStyle>
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />
                <Columns>
                    <asp:TemplateField HeaderText="#SL">
                        <HeaderStyle ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <span><%# GetRecordCountForPage()%></span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Realtor ID" SortExpression="realtor_id">
                        <HeaderStyle Width="30%" ForeColor="White" />
                        <ItemTemplate>
                            <a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("RealtorDetails", "view")%>','RealtorDetails.aspx?Pkey=<%#Eval("pkey_realtor") %>')"  data-gravity="s" title="Edit"><%#Eval("realtor_id") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Realtor Name" SortExpression="realtor_name">
                        <HeaderStyle Width="30%" ForeColor="White" />
                        <ItemTemplate>
                          <a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("RealtorDetails", "view")%>','RealtorDetails.aspx?Pkey=<%#Eval("pkey_realtor") %>')"  data-gravity="s" title="Edit"><%#Eval("realtor_name") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Is Active">
                        <HeaderStyle Width="30%" ForeColor="White" />
                        <ItemTemplate>
                          <%#Eval("is_active") %>
                        </ItemTemplate>
                    </asp:TemplateField>                    
                </Columns>
            </asp:GridView>
            <div class="ttl"><b>Total Number of Realtors: </b><%=recordCount%></div>
            <div class="btn_lst">
                <div class="rgt">
                    <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClick="btnClose_Click"></asp:Button>
                </div>
                 <div class="lft">
                     <asp:Button runat="server" ID="btnAddRealtor" CssClass="button rounded"  Text="Add a New Realtor" OnClick="btnAddRealtor_Click"></asp:Button>
                </div>
            </div>
        </form>
    </div>
</asp:Content>
