﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.SystemConfig
{
    public partial class VariantList : PageBase
    {
        protected int recordCount = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadData();
            }
        }

        private void LoadData()
        {
            try
            {
                ProductModel variantList = ProductService.GetVariantList(ConnString);
                recordCount = variantList.Variants.Rows.Count;
                if (VariantslistGrid.Attributes["SortExpression"] != null)
                    variantList.Variants.DefaultView.Sort = VariantslistGrid.Attributes["SortExpression"] + " " + VariantslistGrid.Attributes["SortDirection"];
                VariantslistGrid.DataSource = variantList.Variants.DefaultView;
                VariantslistGrid.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void VariantslistGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                SortGridView(VariantslistGrid, e);
                VariantslistGrid.PageIndex = 0;
                LoadData();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void VariantslistGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                VariantslistGrid.PageIndex = e.NewPageIndex;
                LoadData();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected int GetRecordCountForPage()
        {
            return GetRecordCountForPageSize(VariantslistGrid.PageSize, VariantslistGrid.PageIndex + 1);
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/DashBoard.aspx");
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbtn = (LinkButton)sender;
                int variantKey = Convert.ToInt32(lbtn.CommandArgument);
                ProductService.DeleteVariant(variantKey, ConnString);
                LoadData();
            }
            catch (Exception ex)
            {
                if (ex.Message.ToLower().IndexOf("") > -1)
                {
                    FileLogger.WriteException(ex);
                    AlertScript("Cannot delete variant");
                }
                else
                {
                    FileLogger.WriteException(ex);
                    AlertScript(ex.Message);
                }
            }
        }

        protected void btnAddnewVariant_Click(object sender, EventArgs e)
        {
            Response.Redirect("VariantDetails.aspx?variantKey=-2");
        }

        protected void VariantslistGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }
    }
}