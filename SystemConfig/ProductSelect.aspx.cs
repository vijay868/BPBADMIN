﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;

namespace Bestprintbuy.Admin.Web.SystemConfig
{
    public partial class ProductSelect : PageBase
    {
        ProductModel TypesData = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                GetData();
            }
        }
        private void GetData()
        {
            try
            {
                int promoKey = Convert.ToInt32(Request.QueryString["promoKey"]);
                string producttypekeys = Request.QueryString["producttypekeys"];
                int Productkey = 0; //Convert.ToInt32(Request.QueryString["templateOption"]);

                if (producttypekeys == "")
                {
                    TypesData = ProductService.GetListForType(producttypekeys, promoKey, ConnString);
                }
                else
                {
                    TypesData = ProductService.GetListProdForPromo(producttypekeys, promoKey, Productkey, ConnString);
                }
                rptrAvailableProductTypes.DataSource = TypesData.Products;
                rptrAvailableProductTypes.DataBind();

                string productIds = "";
                try
                {
                    productIds = Request.QueryString["productIds"];
                }
                catch { }
                List<int> lstProductIds = new List<int>();
                if (productIds != null)
                {
                    foreach (string id in productIds.Split(','))
                    {
                        if (id != "")
                        {
                            lstProductIds.Add(Convert.ToInt32(id));
                        }
                    }
                }

                foreach (RepeaterItem item in rptrAvailableProductTypes.Items)
                {
                    HiddenField hdnProductType = (HiddenField)item.FindControl("hdnProductType");
                    CheckBox chkbox = (CheckBox)item.FindControl("chkSelect");
                    if (lstProductIds.Contains(Convert.ToInt32(hdnProductType.Value)))
                    {
                        chkbox.Checked = true;
                    }
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                string ProdIds = "";
                string ProdNames = "";
                string prodCat = "";
                foreach (RepeaterItem item in rptrAvailableProductTypes.Items)
                {
                    HiddenField hdnProductCat = (HiddenField)item.FindControl("hdnProductCat");
                    HiddenField hdnProductType = (HiddenField)item.FindControl("hdnProductType");
                    HiddenField hdnProductName = (HiddenField)item.FindControl("hdnProductName");
                    CheckBox chkbox = (CheckBox)item.FindControl("chkSelect");
                    if (chkbox.Checked == true)
                    {
                        ProdIds += ((ProdIds == "") ? "" : ",") + hdnProductType.Value;
                        ProdNames += ((ProdNames == "") ? "" : ";:;") + hdnProductName.Value;
                        prodCat += ((prodCat == "") ? "" : ",") + hdnProductCat.Value;
                    }
                }
                string script = string.Format("SetDataAndClose('{0}','{1}','{2}','{3}');", false.ToString().ToLower(), ProdIds, ProdNames, prodCat);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "jscript", script, true);
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }
    }
}