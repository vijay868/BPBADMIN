﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.SystemConfig
{
    public partial class FreeProductsList : PageBase
    {
        protected int recordCount = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadData();
            }
        }

        //private  promotionsList = null;
        private void LoadData()
        {
            try
            {
                int quantityVariantKey = Convert.ToInt32(WebConfiguration.ProductVariants["Quantity"]);
                FreeProductsModel model = ProductService.GetFreeProductsList(quantityVariantKey, ConnString);

                if (FreeProductslistGrid.Attributes["SortExpression"] != null)
                    model.FreeProducts.DefaultView.Sort = FreeProductslistGrid.Attributes["SortExpression"] + " " + FreeProductslistGrid.Attributes["SortDirection"];
                recordCount = model.FreeProducts.Rows.Count;

                FreeProductslistGrid.DataSource = model.FreeProducts.DefaultView;
                FreeProductslistGrid.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void FreeProductslistGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            SortGridView(FreeProductslistGrid, e);
            FreeProductslistGrid.PageIndex = 0;
            LoadData();
        }

        protected void FreeProductslistGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            FreeProductslistGrid.PageIndex = e.NewPageIndex;
            LoadData();
        }

        protected int GetRecordCountForPage()
        {
            return GetRecordCountForPageSize(FreeProductslistGrid.PageSize, FreeProductslistGrid.PageIndex + 1);
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("FreeProductDetail.aspx?pKey=-2");
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/DashBoard.aspx");
        }
    }
}