﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.SystemConfig
{
    public partial class AttributeList : PageBase
    {
        protected int recordCount = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                GetVariantsData();
                LoadData();
            }
        }

        private void GetVariantsData()
        {
            try
            {
                ProductModel variants = ProductService.GetVariantList(ConnString);
                ddlvariants.DataSource = variants.Variants;
                ddlvariants.DataTextField = "variant_name";
                ddlvariants.DataValueField = "pkey_variant";
                ddlvariants.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        private void LoadData()
        {
            try
            {
                int variantKey = Convert.ToInt32(ddlvariants.SelectedValue);
                int attributeKey = -1;
                ProductModel attributeList = ProductService.GetAttributeList(attributeKey, variantKey, ConnString);
                recordCount = attributeList.Attributes.Rows.Count;
                if (AttributeslistGrid.Attributes["SortExpression"] != null)
                    attributeList.Attributes.DefaultView.Sort = AttributeslistGrid.Attributes["SortExpression"] + " " + AttributeslistGrid.Attributes["SortDirection"];
                AttributeslistGrid.DataSource = attributeList.Attributes.DefaultView;
                AttributeslistGrid.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void AttributeslistGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            SortGridView(AttributeslistGrid, e);
            AttributeslistGrid.PageIndex = 0;
            LoadData();
        }

        protected void AttributeslistGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            AttributeslistGrid.PageIndex = e.NewPageIndex;
            LoadData();
        }

        protected int GetRecordCountForPage()
        {
            return GetRecordCountForPageSize(AttributeslistGrid.PageSize, AttributeslistGrid.PageIndex + 1);
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/DashBoard.aspx");
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("AttributeDetails.aspx?attributeKey=-2");
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbtn = (LinkButton)sender;
                int attributeKey = Convert.ToInt32(lbtn.CommandArgument);
                ProductService.DeleteAttribute(attributeKey, ConnString);
                LoadData();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                if (ex.Message.ToLower().Contains("fk_pps_pro_price_variant_xref_pps_product_variants"))
                {
                    AlertScript("Cannot delete attribute");
                }
                else
                {
                    AlertScript(ex.Message);
                }
            }
        }

        protected void ddlvariants_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadData();
        }
    }
}