﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Data;

namespace Bestprintbuy.Admin.Web.SystemConfig
{
    public partial class ProductDetails : PageBase
    {
        protected int hasDefaultProduct = 0;
        protected int isADefaultProduct = 0;
        protected int productkey;
        protected void Page_Load(object sender, EventArgs e)
        {
            productkey = Convert.ToInt32(Request.QueryString["productKey"]);
            if (!Page.IsPostBack)
            {
                GetData();
            }
        }

        private void GetData()
        {
            try
            {
                int productKey = Convert.ToInt32(Request.QueryString["productKey"]);
                int pkey_type = -1;     //To Do
                ProductModel productDetails = ProductService.GetProductList(productKey, -1, pkey_type, ConnString);
                if (productKey > 0)
                {
                    btnDelete.Visible = true;
                }
                if (productDetails.Products.Rows.Count > 0)
                {
                    ProductModel.ProductsRow row = (ProductModel.ProductsRow)productDetails.Products.Rows[0];
                    lblProductCategory.Visible = true;
                    lblProductType.Visible = true;
                    cboProductType.Value = row.fkey_category.ToString();
                    lblProductCategory.Text = row.product_category;
                    lblProductType.Text = row.product_type;
                    txtProductID.Text = row.product_id;
                    txtProductName.Text = row.product_name;
                    txtProductDisplayName.Text = row.product_display_name;
                    txtComments.Text = row.comments;
                    chkDefaultProduct.Checked = row.isDefault;
                    if (row.isDefault)
                        isADefaultProduct = 1;
                    chkHasReverse.Checked = row.has_reverse;
                    // chkIsDirectMail.Checked = row.is_directmail;
                    txtProdDisplayDiscription.Text = row.display_comments;
                }
                else
                {
                    ddlProductCategory.Visible = true;
                    ddlProductType.Visible = true;
                    ViewState["ProductTypes"] = productDetails.Types;

                    ddlProductCategory.DataSource = productDetails.Categories;
                    ddlProductCategory.DataTextField = "ct_name";
                    ddlProductCategory.DataValueField = "pkey_category_type";
                    ddlProductCategory.DataBind();

                    ddlProductType.DataSource = productDetails.Types;
                    ddlProductType.DataTextField = "ct_name";
                    ddlProductType.DataValueField = "pkey_category_type";
                    ddlProductType.DataBind();
                }

                if (productDetails.HasDefault.Rows.Count > 0)
                {
                    bool hasDefault = false;
                    try { hasDefault = Convert.ToBoolean(productDetails.HasDefault.Rows[0][0]); }
                    catch { }
                    if (hasDefault)
                    {
                        hasDefaultProduct = 1;
                    }
                    //chkDefaultProduct.Attributes["onclick"] = "alert('Default Setting on this product can not be changed.')"
                }

                if (productDetails.ShippingDetails.Rows.Count > 0)
                {
                    //rptrShipinglist.DataSource = productDetails.ShippingDetails;
                    //rptrShipinglist.DataBind();
                    ProductModel.ShippingDetailsRow row = (ProductModel.ShippingDetailsRow)productDetails.ShippingDetails.Rows[0];
                    if (!row.Isprice_1Null())
                        txtOneDay.Text = row.price_1.ToString();
                    if (!row.Isprice_2Null())
                        txtTwoDays.Text = row.price_2.ToString();
                    if (!row.Isprice_7Null())
                        txtSevenDays.Text = row.price_7.ToString();
                    if (!row.Isprice_14Null())
                        txtTenDays.Text = row.price_14.ToString();
                }

                BindPriceMatrix(productDetails, productKey);
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        private void BindPriceMatrix(ProductModel product, int productKey)
        {
            ProductModel.VariantsDataTable variants = product.Variants;
            ProductModel.VariantsRow row = variants.NewVariantsRow();
            row.pkey_variant = -1;
            row.variant_name = "Weight(Boxed)";
            product.Variants.Rows.Add(row);
            rptrPriceMatrix.DataSource = product.Variants;
            rptrPriceMatrix.DataBind();

            foreach (RepeaterItem item in rptrPriceMatrix.Items)
            {
                CheckBox chkIsApplicable = (CheckBox)item.FindControl("chkIsApplicable");
                DropDownList ddlAttribute = (DropDownList)item.FindControl("ddlAttribute");
                TextBox txtComments = (TextBox)item.FindControl("txtComments");
                HiddenField hdnVariant = (HiddenField)item.FindControl("hdnVariantID");
                HiddenField hdnPkeyPriceMatrix = (HiddenField)item.FindControl("hdnPkeyPriceMatrix");

                int variantKey = Convert.ToInt32(hdnVariant.Value);
                if (variantKey != -1)
                {
                    String filter = "fkey_variant='" + variantKey + "'";

                    DataRow[] attributes = product.Attributes.Select(filter, null, DataViewRowState.CurrentRows);
                    ddlAttribute.DataSource = attributes;
                    ddlAttribute.DataTextField = "attribute_value";
                    ddlAttribute.DataValueField = "pkey_attribute";
                    ddlAttribute.DataBind();
                    ddlAttribute.Items.Insert(0, new ListItem("No Option", "-1"));

                    DataRow priceRow = null;
                    foreach (ListItem lItem in ddlAttribute.Items)
                    {
                        DataRow[] tempRow = product.PriceMatrix.Select("fkey_attribute='" + lItem.Value + "'", null, DataViewRowState.CurrentRows);
                        if (tempRow != null && tempRow.Length > 0)
                        {
                            priceRow = tempRow[0];
                            lItem.Selected = true;
                            chkIsApplicable.Checked = true;
                            hdnPkeyPriceMatrix.Value = Convert.ToString(priceRow["pkey_price_matrix"]);
                            break;
                        }
                    }
                    if (priceRow != null)
                    {
                        if (priceRow["comments"] != Convert.DBNull && !"".Equals(priceRow["comments"]))
                        {
                            txtComments.Text = priceRow["comments"].ToString();
                        }
                    }
                    else if (productKey <= 0)
                    {
                        DataRow[] prodTypes = product.ApplicableProductTypes.Select(filter, null, DataViewRowState.CurrentRows);

                        if (prodTypes.Length == 1)
                        {
                            chkIsApplicable.Checked = true;
                        }
                    }
                }
                else
                {
                    // add weight
                    ddlAttribute.Visible = false;
                    txtComments.Visible = false;
                    TextBox txtAttribute = (TextBox)item.FindControl("txtAttribute");
                    txtAttribute.Visible = true;
                    if (product.Price.Rows.Count > 0)
                    {
                        if (product.Price.Rows[0]["weight"] != Convert.DBNull && product.Price.Rows[0]["weight"] != null)
                        {
                            txtAttribute.Text = product.Price.Rows[0]["weight"].ToString();
                            chkIsApplicable.Checked = true;
                        }
                    }
                }



                // creating the price row.
                if (product.Price.Rows.Count > 0)
                {
                    if (product.Price.Rows[0]["price"] != DBNull.Value || product.Price.Rows[0]["price"] != null)
                    {
                        txtPrice.Text = product.Price.Rows[0]["price"].ToString();
                    }
                    if (product.Price.Rows[0]["comments"] != DBNull.Value || product.Price.Rows[0]["comments"] != null)
                    {
                        txtPriceComments.Text = product.Price.Rows[0]["comments"].ToString();
                    }
                    if (product.Price.Rows[0]["pkey_price"] != DBNull.Value || product.Price.Rows[0]["pkey_price"] != null)
                    {
                        hdnPkeyPrice.Value = product.Price.Rows[0]["pkey_price"].ToString();
                    }
                }

                //is active
                if (product.Products.Rows.Count > 0)
                {
                    if (product.Products.Rows[0]["is_active"] != DBNull.Value || product.Products.Rows[0]["is_active"] != null)
                    {
                        chkIsActive.Checked = Convert.ToBoolean(product.Products.Rows[0]["is_active"]);
                    }
                    else
                    {
                        chkIsActive.Checked = false;
                    }
                }
            }
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int productKey = Convert.ToInt32(Request.QueryString["productKey"]);
                ProductModel productsDetails = new ProductModel();
                ProductModel.ProductsRow row = productsDetails.Products.NewProductsRow();
                row.pkey_product = productKey;
                row.product_id = txtProductID.Text;
                row.product_name = txtProductName.Text;
                row.product_display_name = txtProductDisplayName.Text;
                row.display_comments = txtProdDisplayDiscription.Text;
                row.comments = txtComments.Text;
                if (productKey == -2)
                {
                    row.product_category = ddlProductCategory.SelectedItem.Text;
                    row.product_type = ddlProductType.SelectedItem.Text;
                    row.fkey_category = Convert.ToInt32(ddlProductType.SelectedValue);
                }
                else
                {
                    row.fkey_category = Convert.ToInt32(cboProductType.Value);
                    row.product_category = lblProductCategory.Text;
                    row.product_type = lblProductType.Text;
                }
                row.is_directmail = chkIsDirectMail.Checked;
                row.isDefault = chkDefaultProduct.Checked;
                row.has_reverse = chkHasReverse.Checked;
                row.fkey_user = Identity.UserPK;
                row.is_active = false;
                int productType = row.fkey_category;
                productsDetails.Products.AddProductsRow(row);

                ProductModel.PriceRow lPricerow = productsDetails.Price.NewPriceRow();

                //A variable of the type ProductModel.PriceRow i.e. ProductModel DataModel's Price table's row
                // is declared and is assigned with a new row of this type.
                lPricerow.pkey_price = hdnPkeyPrice.Value == "" ? -2 : Convert.ToInt32(hdnPkeyPrice.Value);
                lPricerow.fkey_product = productKey;
                lPricerow.comments = txtPriceComments.Text;
                lPricerow.price = txtPrice.Text == "" ? 0 : Convert.ToDouble(txtPrice.Text);
                lPricerow.fkey_user = Identity.UserPK;


                foreach (RepeaterItem item in rptrPriceMatrix.Items)
                {
                    CheckBox chkIsApplicable = (CheckBox)item.FindControl("chkIsApplicable");
                    DropDownList ddlAttribute = (DropDownList)item.FindControl("ddlAttribute");
                    TextBox txtGComments = (TextBox)item.FindControl("txtComments");
                    HiddenField hdnVariant = (HiddenField)item.FindControl("hdnVariantID");
                    HiddenField hdnPkeyPriceMatrix = (HiddenField)item.FindControl("hdnPkeyPriceMatrix");
                    int variantKey = Convert.ToInt32(hdnVariant.Value);
                    if (variantKey != -1)
                    {
                        ProductModel.PriceMatrixRow l_PriceMatrixrow = productsDetails.PriceMatrix.NewPriceMatrixRow();
                        if (chkIsApplicable.Checked)
                        {
                            l_PriceMatrixrow.fkey_attribute = Convert.ToInt32(ddlAttribute.SelectedValue);
                            l_PriceMatrixrow.pkey_price_matrix = hdnPkeyPriceMatrix.Value == "" ? -2 : Convert.ToInt32(hdnPkeyPriceMatrix.Value);
                            l_PriceMatrixrow.comments = txtGComments.Text;
                        }
                        else
                        {
                            l_PriceMatrixrow.fkey_attribute = -2;
                            l_PriceMatrixrow.pkey_price_matrix = hdnPkeyPriceMatrix.Value == "" ? -2 : Convert.ToInt32(hdnPkeyPriceMatrix.Value);
                        }
                        l_PriceMatrixrow.fkey_user = Identity.UserPK;
                        productsDetails.PriceMatrix.Rows.Add(l_PriceMatrixrow);
                    }
                    else
                    {
                        TextBox txtAttribute = (TextBox)item.FindControl("txtAttribute");
                        if (chkIsApplicable.Checked)
                        {
                            lPricerow.weight = txtAttribute.Text == "" ? 0 : Convert.ToDouble(txtAttribute.Text);
                        }
                        else
                            lPricerow.weight = 0;
                    }
                }
                productsDetails.Price.Rows.Add(lPricerow);

                //ProductModel.PriceMatrixRow l_PriceMatrixrow;

                ProductModel.ShippingDetailsRow shippingRow = productsDetails.ShippingDetails.NewShippingDetailsRow();
                shippingRow.fkey_producttype = productType;
                shippingRow.fkey_product = productType;

                shippingRow.price_1 = txtOneDay.Text == "" ? 0 : Convert.ToDouble(txtOneDay.Text.Replace("$", ""));
                shippingRow.price_2 = txtTwoDays.Text == "" ? 0 : Convert.ToDouble(txtTwoDays.Text.Replace("$", ""));
                shippingRow.price_7 = txtSevenDays.Text == "" ? 0 : Convert.ToDouble(txtSevenDays.Text.Replace("$", ""));
                shippingRow.price_14 = txtTenDays.Text == "" ? 0 : Convert.ToDouble(txtTenDays.Text.Replace("$", ""));
                shippingRow.fkey_user = Identity.UserPK;


                productsDetails.ShippingDetails.AddShippingDetailsRow(shippingRow);

                productKey = ProductService.UpdateProduct(productsDetails, ConnString);

                Response.Redirect("ProductList.aspx");
            }
            catch (Exception ex)
            {
                if (ex.Message.ToLower().IndexOf("duplicate") > -1)
                {
                    FileLogger.WriteException(ex);
                    AlertScript("Product Id must be unique. Please re-enter Product Id");
                }
                else
                {
                    FileLogger.WriteException(ex);
                    AlertScript(ex.Message);
                }
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("ProductList.aspx");
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int productKey = Convert.ToInt32(Request.QueryString["productKey"]);
                ProductService.DeleteProduct(productKey, ConnString);
                Response.Redirect("ProductList.aspx");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void ddlProductCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ProductModel.TypesDataTable Types = (ProductModel.TypesDataTable)ViewState["ProductTypes"];
                ddlProductType.DataTextField = "ct_name";
                ddlProductType.DataValueField = "pkey_category_type";
                ddlProductType.DataSource = Types.Select("fkey_self = " + ddlProductCategory.SelectedValue);
                ddlProductType.DataBind();
                GetData();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void ddlProductType_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetData();
        }
    }
}