﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;

namespace Bestprintbuy.Admin.Web.SystemConfig
{
    public partial class CategoryDetails : PageBase
    {
        protected int mastercatKey = -2;
        protected int categoryKey = -2;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetDetails();
            }
        }

        private void GetDetails()
        {
            try
            {
                mastercatKey = Convert.ToInt32(Request.QueryString["masterKey"]);
                categoryKey = Convert.ToInt32(Request.QueryString["categoryKey"]);
                TypeModel model = SupportTypesService.GetData(categoryKey, mastercatKey, ConnString);

                if (model.Category.Rows.Count > 0)
                {
                    TypeModel.CategoryRow row = (TypeModel.CategoryRow)model.Category.Rows[0];
                    txtCategoryName.Text = row.category_name;
                    txtCategoryID.Text = row.category_id;
                    txtComments.Text = row.IscommentsNull() ? "" : row.comments;
                    lblScreens.Text = row.screens.ToString();
                }
                if (model.MasterCategories.Rows.Count == 1)
                {
                    lblMasterCategory.Text = model.MasterCategories.Rows[0]["mc_name"].ToString();
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            mastercatKey = Convert.ToInt32(Request.QueryString["masterKey"]);
            categoryKey = Convert.ToInt32(Request.QueryString["categoryKey"]);
            TypeModel model = new TypeModel();
            TypeModel.CategoryRow row = (TypeModel.CategoryRow)model.Category.NewCategoryRow();
            row.pkey_category = categoryKey;
            row.fkey_master = mastercatKey;
            row.category_name = txtCategoryName.Text;
            row.category_id = txtCategoryID.Text;
            row.comments = txtComments.Text;
            row.fkey_user = Identity.UserPK;
            model.Category.AddCategoryRow(row);
            try
            {
                categoryKey = SupportTypesService.UpdateCategoryDetails(model, ConnString);
                Response.Redirect("CategoryList.aspx?mastercatKey=" + mastercatKey + "&edit=true");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript("Insert/Update failed. Please try Again.");
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("CategoryList.aspx?edit=true");
        }
    }
}