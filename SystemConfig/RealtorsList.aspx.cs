﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.SystemConfig
{
    public partial class RealtorsList : PageBase
    {
        protected int recordCount = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadData();
            }
        }

        private void LoadData()
        {
            try
            {
                int ExclusiveCustomerKey = Convert.ToInt32(WebConfiguration.ProductVariants["ExclusiveCustomer"]);
                RealtorsModel model = ProductService.GetRealtorsData(-1, ExclusiveCustomerKey, ConnString);

                if (RealtorsListGrid.Attributes["SortExpression"] != null)
                    model.Realtors.DefaultView.Sort = RealtorsListGrid.Attributes["SortExpression"] + " " + RealtorsListGrid.Attributes["SortDirection"];
                recordCount = model.Realtors.Rows.Count;
                RealtorsListGrid.DataSource = model.Realtors.DefaultView;
                RealtorsListGrid.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected int GetRecordCountForPage()
        {
            return GetRecordCountForPageSize(RealtorsListGrid.PageSize, RealtorsListGrid.PageIndex + 1);
        }

        protected void RealtorsListGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                RealtorsListGrid.PageIndex = e.NewPageIndex;
                LoadData();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void RealtorsListGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            SortGridView(RealtorsListGrid, e);
            RealtorsListGrid.PageIndex = 0;
            LoadData();
        }

        protected void btnAddRealtor_Click(object sender, EventArgs e)
        {
            Response.Redirect("RealtorDetails.aspx?Pkey=-2");
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/DashBoard.aspx");
        }
    }
}