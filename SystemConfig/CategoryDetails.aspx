﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="CategoryDetails.aspx.cs" Inherits="Bestprintbuy.Admin.Web.SystemConfig.CategoryDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <script type="text/javascript">
         function saveCategory() {
             if ('<%=HasAccess("CategoryList", categoryKey != -2 ? "edit" : "add")%>' == 'False') {
                 alert('User does not have access.');
                 return false;
             }
             return true;
         }

         $().ready(function () {
             $("#frmMasterCategoryDetail").validate();
         });
    </script>
    <div class="contant_hldr">
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">System Configuration</li>
             <li><a href="CategoryList.aspx"> Category Lists</a></li>
            <li>Category Details</li>
        </ul>
        <h2>Category Details</h2>
        <form class="" runat="server" id="frmMasterCategoryDetail">
            <div >
   
			 <table width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="15%" style="vertical-align: top;">
                            <label>Category ID*</label>
                        </td>
                        <td width="20%" style="vertical-align: top;">
                              <asp:TextBox  name="txtCategoryID" ID="txtCategoryID" CssClass="text-box" runat="server" MaxLength="3" required></asp:TextBox>
                        </td>
                        <td width="15%" style="vertical-align: top;" rowspan="4">
                            <label>Comments</label>
                        </td>
                        <td width="35%" style="vertical-align: top;" rowspan="4">
                             <asp:TextBox name="txtComments" ID="txtComments" CssClass="text-box" runat="server" style="resize:none;" TextMode="MultiLine" Rows="8" Width="295px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">
                            <label>Category Name*</label>
                        </td>
                        <td style="vertical-align: top;">
                              <asp:TextBox  name="txtCategoryName" ID="txtCategoryName" CssClass="text-box" runat="server" required></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">
                            <label>Master Category</label>
                        </td>
                        <td style="vertical-align: top;">
                            <asp:Label ID="lblMasterCategory" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                         <tr>
                        <td style="vertical-align: top;">
                            <label># of Screens</label>
                        </td>
                        <td style="vertical-align: top;">
                              <asp:Label ID="lblScreens" runat="server" Font-Size="14px" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                </tbody>
            </table>
                </div>
		  <div class="btn_lst">
                <div class="rgt" style="padding-right:7%">
                  
                    <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClick="btnClose_Click" UseSubmitBehavior="false"></asp:Button>
                      <asp:Button ID="btnSave" CssClass="button rounded" runat="server" OnClientClick="return saveCategory();"  Text="Save"  OnClick="btnSave_Click"/>
                    
                </div>
            </div>  
                
    </form>
   </div>
</asp:Content>
