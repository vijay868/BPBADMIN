﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;

namespace Bestprintbuy.Admin.Web.SystemConfig
{
    public partial class MasterCategoryList : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadData();
            }
        }

        private void LoadData()
        {
            try
            {
                TypeModel.MasterCategoriesDataTable masterCategories = SupportTypesService.GetList(ConnString);
                if (MasterCategoryGrid.Attributes["SortExpression"] != null)
                    masterCategories.DefaultView.Sort = MasterCategoryGrid.Attributes["SortExpression"] + " " + MasterCategoryGrid.Attributes["SortDirection"];
                MasterCategoryGrid.DataSource = masterCategories;
                MasterCategoryGrid.DataBind();
                lblTotalRecords.Text = string.Format("Total Number of Categories {0} ", masterCategories.Rows.Count);
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected int GetRecordCountForPage()
        {
            return GetRecordCountForPageSize(MasterCategoryGrid.PageSize, MasterCategoryGrid.PageIndex + 1);
        }

        protected void MasterCategoryGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            MasterCategoryGrid.PageIndex = e.NewPageIndex;
            LoadData();
        }

        protected void MasterCategoryGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            SortGridView(MasterCategoryGrid, e);
            MasterCategoryGrid.PageIndex = 0;
            LoadData();
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/DashBoard.aspx");
        }
    }
}