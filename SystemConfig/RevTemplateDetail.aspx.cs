﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace Bestprintbuy.Admin.Web.SystemConfig
{
    public partial class RevTemplateDetail : PageBase
    {
        protected int pkey_reverse_templates = -1;
        protected int paperVariantKey = Convert.ToInt32(WebConfiguration.ProductVariants["PaperType"]);
        protected int backcolorvariantkey = Convert.ToInt32(WebConfiguration.ProductVariants["BackColor"]);
        protected int sizevariantkey = Convert.ToInt32(WebConfiguration.ProductVariants["Size"]);
        protected int templateCategoryKey = Convert.ToInt32(WebConfiguration.ProductTypeKeys["Reverse Templates"]);

        protected void Page_Load(object sender, EventArgs e)
        {
            pkey_reverse_templates = Convert.ToInt32(Request.QueryString["fkeyRevTemplate"]);
            if (!Page.IsPostBack)
            {
                GetData();
            }
        }

        private void GetData()
        {
            try
            {
                pkey_reverse_templates = Convert.ToInt32(Request.QueryString["fkeyRevTemplate"]);
                int selectedProductType = 0;   // Convert.ToInt32(Request.QueryString["cboProductType"]);
                if (selectedProductType == 0 && pkey_reverse_templates == -2)
                {
                    selectedProductType = Convert.ToInt32(WebConfiguration.ProductTypeKeys["BussinessCards"]);
                }
                else
                {
                    selectedProductType = Convert.ToInt32(Request.QueryString["cboProductType"]);
                }
                ReverseTemplateModel TemplateDetails = TemplateService.GetReverseTemplateDetails(pkey_reverse_templates, paperVariantKey, backcolorvariantkey, sizevariantkey, selectedProductType, templateCategoryKey, ConnString);

                ddlBackColor.DataSource = TemplateDetails.ColorAttributes;
                ddlBackColor.DataTextField = "attribute_value";
                ddlBackColor.DataValueField = "pkey_attribute";
                ddlBackColor.DataBind();

                ddlTemplateType.DataSource = TemplateDetails.TemplateTypes;
                ddlTemplateType.DataTextField = "ct_name";
                ddlTemplateType.DataValueField = "pkey_category_type";
                ddlTemplateType.DataBind();

                ddlPaperType.DataSource = TemplateDetails.PaperAttributes;
                ddlPaperType.DataTextField = "attribute_value";
                ddlPaperType.DataValueField = "pkey_attribute";
                ddlPaperType.DataBind();

                ddlPaperSize.DataSource = TemplateDetails.SizeAttribute;
                ddlPaperSize.DataTextField = "attribute_value";
                ddlPaperSize.DataValueField = "pkey_attribute";
                ddlPaperSize.DataBind();

                if (TemplateDetails.ReverseTemplate.Rows.Count > 0)
                {
                    ReverseTemplateModel.ReverseTemplateRow row = (ReverseTemplateModel.ReverseTemplateRow)TemplateDetails.ReverseTemplate.Rows[0];
                    lblProductCategory.Visible = true;
                    lblProductType.Visible = true;
                    lblProductCategory.Text = row.product_category_name;
                    lblProductType.Text = row.product_type_name;
                    cboProdCategory.Value = row.fkey_product_category.ToString();
                    cboProductType.Value = row.fkey_product_type.ToString();

                    txtTemplateName.Text = row.template_id;
                    chkAlltimeFavourites.Checked = row.is_all_time_favorites;
                    chkhasBackText.Checked = row.has_backform_fields;
                    chkTempeligble.Checked = row.is_freetemplate;
                    chkIsCustomisable.Value = row.is_customizable ? "1" : "0";
                    txtTemplateLocation.Text = row.template_url;
                    txtThumbnailUrl.Text = row.thumbnail_url;

                    ddlBackColor.SelectedValue = row.fkey_color_type.ToString();
                    ddlTemplateType.SelectedValue = row.fkey_rev_template_type.ToString();
                    ddlPaperType.SelectedValue = row.fkey_paper_type.ToString();
                    ddlPaperSize.SelectedValue = row.fkey_size_attribute.ToString();
                    ddlPaperSize.SelectedValue = row.card_format.ToString();
                }
                else
                {
                    ddlProductType.Visible = true;
                    ddlProductCategory.Visible = true;
                    ViewState["ProductTypes"] = TemplateDetails.ProductTypes;

                    ddlProductCategory.DataSource = TemplateDetails.ProductCategories;
                    ddlProductCategory.DataTextField = "ct_name";
                    ddlProductCategory.DataValueField = "pkey_category_type";
                    ddlProductCategory.DataBind();

                    ddlProductType.DataSource = TemplateDetails.ProductTypes;
                    ddlProductType.DataTextField = "ct_name";
                    ddlProductType.DataValueField = "pkey_category_type";
                    ddlProductType.DataBind();

                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                pkey_reverse_templates = Convert.ToInt32(Request.QueryString["fkeyRevTemplate"]);
                ReverseTemplateModel revTemplatesDetails = new ReverseTemplateModel();
                ReverseTemplateModel.ReverseTemplateRow row = revTemplatesDetails.ReverseTemplate.NewReverseTemplateRow();
                row.pkey_reverse_template = pkey_reverse_templates;
                if (pkey_reverse_templates != -2)
                {
                    row.fkey_product_type = Convert.ToInt32(cboProductType.Value);
                    row.fkey_product_category = Convert.ToInt32(cboProdCategory.Value);
                    row.product_category_name = lblProductCategory.Text;
                    row.product_type_name = lblProductType.Text;
                }
                else
                {
                    row.product_category_name = ddlProductCategory.SelectedItem.Text;
                    row.product_type_name = ddlProductType.SelectedItem.Text;
                    row.fkey_product_type = Convert.ToInt32(ddlProductType.SelectedValue);
                    row.fkey_product_category = Convert.ToInt32(ddlProductCategory.SelectedValue);
                }
                row.is_all_time_favorites = chkAlltimeFavourites.Checked;
                row.is_customizable = chkIsCustomisable.Value == "1" ? true : false;
                row.is_freetemplate = chkTempeligble.Checked;
                row.has_backform_fields = chkhasBackText.Checked;
                row.template_id = txtTemplateName.Text;
                row.template_url = txtTemplateLocation.Text;
                row.thumbnail_url = txtThumbnailUrl.Text; //txtThumbnailUrl.Text;
                row.fkey_user = Identity.UserPK;
                row.fkey_color_type = Convert.ToInt32(ddlBackColor.SelectedValue);
                row.card_format = Convert.ToInt32(ddlCardFormat.SelectedValue);
                row.fkey_size_attribute = Convert.ToInt32(ddlPaperSize.SelectedValue);
                row.fkey_paper_type = Convert.ToInt32(ddlPaperType.SelectedValue);
                row.fkey_rev_template_type = Convert.ToInt32(ddlTemplateType.SelectedValue);
                revTemplatesDetails.ReverseTemplate.AddReverseTemplateRow(row);

                pkey_reverse_templates = TemplateService.Updaterversetemplate(revTemplatesDetails, ConnString);
                ViewState["fkeyRevTemplate"] = pkey_reverse_templates;

                Response.Redirect("RevTemplateList.aspx");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript("Save/Update failed. try again.");
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("RevTemplateList.aspx");
        }

        protected void ddlProductCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ReverseTemplateModel.ProductTypesDataTable Types = (ReverseTemplateModel.ProductTypesDataTable)ViewState["ProductTypes"];
                ddlProductType.DataTextField = "ct_name";
                ddlProductType.DataValueField = "pkey_category_type";
                ddlProductType.DataSource = Types.Select("fkey_self = " + ddlProductCategory.SelectedValue);
                ddlProductType.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }
    }
}