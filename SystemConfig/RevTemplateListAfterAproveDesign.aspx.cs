﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Collections;

namespace Bestprintbuy.Admin.Web.SystemConfig
{
    public partial class RevTemplateListAfterAproveDesign : PageBase
    {
        protected int variantKey = Convert.ToInt32(WebConfiguration.ProductVariants["ExclusiveCustomer"]);
        protected string mCustomerPortalURL = "";
        protected int SiteKey = -1;
        protected int productCategoryKey = 591;
        protected void Page_Load(object sender, EventArgs e)
        {
            mCustomerPortalURL = customerPotralUrl + "/DesignCenter/";
            if (!Page.IsPostBack)
            {
                GetKioskList(variantKey, ConnString);
                GetRevTemplateList(SiteKey, productCategoryKey, ConnString);
            }
        }

        private void GetRevTemplateList(int SiteKey, int productCategoryKey, string ConnString)
        {
            try
            {
                ReverseTemplateModel RevTemplateList = ProductService.GetRevTemplateList(SiteKey, productCategoryKey, ConnString);

                ddlTemplateType.DataSource = RevTemplateList.ProductCategories;
                ddlTemplateType.DataTextField = "ct_name";
                ddlTemplateType.DataValueField = "pkey_category_type";
                ddlTemplateType.DataBind();

                dtlRevTemplates.DataSource = RevTemplateList.ReverseTemplate;
                dtlRevTemplates.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        private void GetKioskList(int variantKey, string ConnString)
        {
            try
            {
                ProductTemplateModel.KiosksDataTable kioskList = ProductService.getKioskList(variantKey, ConnString);
                ddlKiosk.DataSource = kioskList;
                ddlKiosk.DataTextField = "attribute_value";
                ddlKiosk.DataValueField = "pkey_attribute";
                ddlKiosk.DataBind();
                ddlKiosk.SelectedValue = SiteKey.ToString();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void ddlKiosk_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SiteKey = Convert.ToInt32(ddlKiosk.SelectedValue);
                productCategoryKey = Convert.ToInt32(ddlTemplateType.SelectedValue);
                GetRevTemplateList(SiteKey, productCategoryKey, ConnString);
                ddlTemplateType.SelectedValue = productCategoryKey.ToString();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void ddlTemplateType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SiteKey = Convert.ToInt32(ddlKiosk.SelectedValue);
                productCategoryKey = Convert.ToInt32(ddlTemplateType.SelectedValue);
                GetRevTemplateList(SiteKey, productCategoryKey, ConnString);
                ddlTemplateType.SelectedValue = productCategoryKey.ToString();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            ReverseTemplateModel revTemplateModel = new ReverseTemplateModel();
            try
            {
                foreach (DataListItem item in dtlRevTemplates.Items)
                {
                    ReverseTemplateModel.ReverseTemplateRow row = revTemplateModel.ReverseTemplate.NewReverseTemplateRow();
                    CheckBox chk = (CheckBox)item.FindControl("chkTemplate");
                    HiddenField hdnFkeyTemplateLayout = (HiddenField)item.FindControl("hdnFkeyTemplateLayout");
                    TextBox txtDisplayName = (TextBox)item.FindControl("txtdisplay");

                    row.fkey_template_layout = Convert.ToInt32(hdnFkeyTemplateLayout.Value);
                    row.display_text = txtDisplayName.Text;
                    row.enable_ad_selection = chk.Checked;
                    revTemplateModel.ReverseTemplate.AddReverseTemplateRow(row);
                }

                SiteKey = Convert.ToInt32(ddlKiosk.SelectedValue);
                productCategoryKey = Convert.ToInt32(ddlTemplateType.SelectedValue);
                revTemplateModel = ProductService.UpdateReverseTemplateInfo(revTemplateModel, productCategoryKey, SiteKey, ConnString);

                dtlRevTemplates.DataSource = revTemplateModel.ReverseTemplate;
                dtlRevTemplates.DataBind();
                AlertScript("Successfully Updated.");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript("Update failed. try again.");
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("RevTemplateList.aspx");
        }
    }
}