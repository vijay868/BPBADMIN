﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;
namespace Bestprintbuy.Admin.Web.Customers
{
    public partial class CustCreditStatus : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                GetData();
            }
        }

        private void GetData()
        {
            try
            {
                int pkey_credit = Convert.ToInt32(Request.QueryString["pkey_credit"]);
                ServiceCreditsModel status = CustomerService.GetCustomerCreditPOStatus(pkey_credit, ConnString);

                rptrCustomerCreditStatus.DataSource = status.POCreditStatus;
                rptrCustomerCreditStatus.DataBind();

                if (status.CustomerCredit.Rows.Count > 0)
                {
                    ServiceCreditsModel.CustomerCreditRow row = (ServiceCreditsModel.CustomerCreditRow)status.CustomerCredit.Rows[0];
                    lblCustName.Text = row.customer_name;
                    lblCustEmail.Text = row.email_id;
                    lblPONumber.Text = row.po_number;
                    lblDt.Text = row.date_created.ToShortDateString();
                    lblCreditType.Text = row.service_credit_abbr + " - " + row.service_credit_desc;
                    lblAmount.Text = row.credit_amount.ToString();
                    lblAvailedAmt.Text = row.availed_amount.ToString();
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }
    }
}