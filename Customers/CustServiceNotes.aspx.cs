﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using System.Data;
using Bestprintbuy.Admin.Utilities;
using System.Text;

namespace Bestprintbuy.Admin.Web.Customers
{
    public partial class CustServiceNotes : PageBase
    {
        protected string commentType = "admin";
        protected string fkey_customer = null;
        SupplierPOModel poModel = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    hdnPkeyCustPoDtl.Value = Request.QueryString["pkey_cust_po_dtl"] != null ? Request.QueryString["pkey_cust_po_dtl"] : "-1";
                    BindCustServiceNotes(Convert.ToInt32(Request.QueryString["pkey_cust_po_dtl"] != null ? Request.QueryString["pkey_cust_po_dtl"] : "-1"));
                    //Page.DataBind();
                }
                catch (Exception ex)
                {
                    FileLogger.WriteException(ex);
                }
            }
        }

        private void BindCustServiceNotes(int pkey_cust_po_dtl)
        {
            try
            {
                poModel = CustomerService.GetCustServiceNotes(pkey_cust_po_dtl, commentType, ConnString);
                if (poModel.CustServiceComments.Rows.Count > 0)
                {
                    SupplierPOModel.CustServiceCommentsRow row = poModel.CustServiceComments[0];
                    txtNote.Text = row.SERVICEREP_COMMENTS;
                    lblPONumber.Text = Request.QueryString["po_number"];
                    hdnPkeyCustPoDtlServiceComment.Value = row.PKEY_CUST_PO_DTL_SERVICEREP_COMMENTS.ToString();
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string image1 = "";  //CopyImagesToWorkingArea(uploadImgFile1.Value, "image1");
                string image2 = "";  // CopyImagesToWorkingArea(uploadImgFile2.Value, "image2");
                SupplierPOModel poModel = new SupplierPOModel();
                SupplierPOModel.CustServiceCommentsRow row = poModel.CustServiceComments.NewCustServiceCommentsRow();
                row.FKEY_CUST_PO_DTL = Convert.ToInt32(hdnPkeyCustPoDtl.Value);
                row.PKEY_CUST_PO_DTL_SERVICEREP_COMMENTS = Convert.ToInt32(hdnPkeyCustPoDtlServiceComment.Value == "" ? "-2" : hdnPkeyCustPoDtlServiceComment.Value);
                row.SERVICEREP_COMMENTS = txtNote.Text;
                row.COMMENT_TYPE = commentType;
                row.IMAGE1 = image1;
                row.IMAGE2 = image2;
                row.FKEY_USER = Identity.UserPK;

                poModel.CustServiceComments.AddCustServiceCommentsRow(row);
                poModel = CustomerService.UpdateCustServiceNotes(poModel, ConnString);
                AlertScript("Successfully Saved.");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript("Save Failed. Try again.");
            }
        }

        //private string CopyImagesToWorkingArea(string url, string Side)
        //{
        //    if (string.IsNullOrEmpty(url))
        //        return "";
        //    //fkey_customer = Request.QueryString["pkey_customer"];
        //    fkey_customer = Identity.UserPK.ToString();
        //    string serverPath = System.Configuration.ConfigurationManager.AppSettings["BPBCustomerBaseDirectory"];
        //    serverPath = serverPath + "\\DesignCenter\\Users\\" + string.Format("{0}\\{1}\\", fkey_customer, "CustomerProofs");

        //    string CustomerBaseUrl = System.Configuration.ConfigurationManager.AppSettings["BPBCustomerBaseURL"];
        //    CustomerBaseUrl = CustomerBaseUrl + "/DesignCenter/Users/" + string.Format("{0}/{1}/", fkey_customer, "CustomerProofs");
        //    System.IO.FileInfo fileCopy = new System.IO.FileInfo(url);

        //    System.IO.DirectoryInfo pathDir = new System.IO.DirectoryInfo(serverPath);
        //    if (!pathDir.Exists)
        //    {
        //        pathDir.Create();
        //    }
        //    else
        //    {
        //        pathDir.Refresh();
        //    }
        //    string Filename = fileCopy.Name;
        //    try
        //    {
        //        serverPath = serverPath + Filename;
        //        if (Side == "image1")
        //        {
        //            uploadImgFile1.PostedFile.SaveAs(serverPath);
        //        }
        //        else if (Side == "image2")
        //        {
        //            uploadImgFile2.PostedFile.SaveAs(serverPath);
        //        }

        //        return CustomerBaseUrl + Filename;
        //    }
        //    catch (Exception ex)
        //    {
        //        FileLogger.WriteException(ex);
        //    }
        //    return "";
        //}

        //public object PreviousImages()
        //{
        //    DataRow[] drs = null;
        //    StringBuilder sb = new StringBuilder("");
        //    //Dim strUrl As String = "<a href='{0}'class='a1b'>{1}</a></br>"
        //    string strUrl = "<a href='javascript:;' {0} class='a1b'>{1}</a></br>";
        //    string strOnClick = "onclick=\"javascript:window.open('{0}'); return false;\"";
        //    string strOnClick1 = "onclick=\"javascript:window.open('{0}'); return false;\"";
        //    drs = poModel.CustServiceComments.Select();
        //    if (drs.Length <= 0)
        //        return "";
        //    foreach (DataRow dr in poModel.CustServiceComments.Rows)
        //    {
        //        if (!IsDBNull(dr["IMAGE1"]))
        //        {
        //            if (!string.IsNullOrEmpty(dr["IMAGE1"].ToString()))
        //            {
        //                strOnClick = string.Format(strOnClick, dr["IMAGE1"]);
        //                sb.AppendFormat(strUrl, strOnClick, "Image1");
        //            }
        //        }
        //        if (!IsDBNull(dr["IMAGE2"]))
        //        {
        //            if (!string.IsNullOrEmpty(dr["IMAGE2"].ToString()))
        //            {
        //                strOnClick1 = string.Format(strOnClick1, dr["IMAGE2"]);
        //                sb.AppendFormat(strUrl, strOnClick1, "Image2");
        //            }
        //        }
        //        break;
        //    }
        //    return sb.ToString();
        //}
    }
}