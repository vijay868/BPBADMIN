﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;

namespace Bestprintbuy.Admin.Web.Customers
{
    public partial class CustomerRegistration : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ViewState["Customerpkey"] = Convert.ToInt32(Request.QueryString["Customerpkey"]);
                GetData();
            }
        }

        private void GetData()
        {
            try
            {
                int CustomerPkey = Convert.ToInt32(ViewState["Customerpkey"]);
                CustomerModel Customer = UserService.GetCustomerDetailbykey(CustomerPkey, ConnString);

                ddlCountry.DataSource = Customer.Countries;
                ddlCountry.DataTextField = "country_name";
                ddlCountry.DataValueField = "pkey_country";
                ddlCountry.DataBind();
                ddlCountry.Items.Insert(0, new ListItem("Select Country", ""));

                ddlState.DataSource = Customer.States;
                ddlState.DataTextField = "state_name";
                ddlState.DataValueField = "pkey_state";
                ddlState.DataBind();
                ddlState.Items.Insert(0, new ListItem("Select State", ""));

                if (Customer.Customers.Rows.Count > 0)
                {
                    CustomerModel.CustomersRow CustomerRow = (CustomerModel.CustomersRow)Customer.Customers.Rows[0];
                    txtFirstName.Text = CustomerRow.cus_firstname;
                    txtLastName.Text = CustomerRow.cus_lastname;
                    txtAddress1.Text = CustomerRow.address1;
                    txtAddress2.Text = CustomerRow.address2;
                    txtCity.Text = CustomerRow.city;
                    txtZip.Text = CustomerRow.zip;
                    txtPhone.Text = CustomerRow.phone;
                    txtCompanyName.Text = CustomerRow.company_name;
                    txtTitle.Text = CustomerRow.title;
                    txtEmail.Text = CustomerRow.email;

                    chkPersonal.Checked = CustomerRow.is_personal;
                    ChkBusiness.Checked = CustomerRow.is_business;
                    ChkBusinessCards.Checked = CustomerRow.is_businesscards;
                    ChkPostCards.Checked = CustomerRow.is_postcards;
                    ChkFoldedCards.Checked = CustomerRow.is_foldedcards;
                    ChkBrochures.Checked = CustomerRow.is_bruchures;
                    ChkMarketing.Checked = CustomerRow.is_marketing;
                    ChkBanners.Checked = CustomerRow.is_banners;
                    chkNewProducts.Checked = CustomerRow.is_newoffers;
                    chkNewsLetters.Checked = CustomerRow.is_newsletters;
                    chkAcceptPrivacy.Checked = CustomerRow.is_accept;
                    if (CustomerRow.email_format)
                        rbtnHtml.Checked = true;
                    else
                        rbtnPlainText.Checked = true;
                    ddlCountry.SelectedValue = CustomerRow.fkey_country;
                    ddlState.SelectedValue = CustomerRow.fkey_state;
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                int Customerpkey = Convert.ToInt32(ViewState["Customerpkey"]);
                CustomerModel CustomerDetails = new CustomerModel();
                CustomerModel.CustomersRow Customer = CustomerDetails.Customers.NewCustomersRow();
                Customer.pkey_customer = Customerpkey;
                Customer.cus_firstname = txtFirstName.Text;
                Customer.cus_lastname = txtLastName.Text;
                Customer.address1 = txtAddress1.Text;
                Customer.address2 = txtAddress2.Text;
                Customer.city = txtCity.Text;
                Customer.zip = txtZip.Text;
                Customer.phone = txtPhone.Text;
                Customer.company_name = txtCompanyName.Text;
                Customer.title = txtTitle.Text;
                Customer.email = txtEmail.Text;
                Customer.password = txtPassword.Text == "" ? "" : MD5Encryption.Encrypt(txtPassword.Text.Trim());
                Customer.is_personal = chkPersonal.Checked;
                Customer.is_business = ChkBusiness.Checked;
                Customer.is_businesscards = ChkBusinessCards.Checked;
                Customer.is_postcards = ChkPostCards.Checked;
                Customer.is_foldedcards = ChkFoldedCards.Checked;
                Customer.is_bruchures = ChkBrochures.Checked;
                Customer.is_marketing = ChkMarketing.Checked;
                Customer.is_banners = ChkBanners.Checked;
                Customer.is_newoffers = chkNewProducts.Checked;
                Customer.is_newsletters = chkNewsLetters.Checked;
                Customer.is_accept = chkAcceptPrivacy.Checked;
                if (rbtnHtml.Checked)
                    Customer.email_format = true;
                else
                    Customer.email_format = false;
                Customer.fkey_country = ddlCountry.SelectedValue;
                Customer.fkey_state = ddlState.SelectedValue;

                CustomerDetails.Customers.AddCustomersRow(Customer);
                Customerpkey = UserService.UpdateCustomer(CustomerDetails, ConnString);
                ViewState["Customerpkey"] = Customerpkey;
                Response.Redirect("CustomersList.aspx");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("CustomersList.aspx");
        }
    }
}