﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;

namespace Bestprintbuy.Admin.Web.Customers
{
    public partial class TemplateTypeSelect : PageBase
    {
        ProductModel TypesData = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                GetData();
            }
        }

        private void GetData()
        {
            try
            {
                int promoKey = Convert.ToInt32(Request.QueryString["promoKey"]);
                string producttypekeys = Request.QueryString["producttypekeys"];
                int templateOption = Convert.ToInt32(Request.QueryString["templateOption"]);

                if (producttypekeys == "")
                {
                    TypesData = ProductService.GetListForType(producttypekeys, promoKey, ConnString);
                }
                else
                {
                    TypesData = ProductService.GetListTemplateTypesForPromo(promoKey, producttypekeys, templateOption, ConnString);
                }
                rptrAvailableTemplateTypes.DataSource = TypesData.AvailableTemplateTypes;
                rptrAvailableTemplateTypes.DataBind();

                string templateIds = "";
                try
                {
                    templateIds = Request.QueryString["templateIds"];
                }
                catch { }
                List<int> lstTemplateIds = new List<int>();
                if (templateIds != null)
                {
                    foreach (string id in templateIds.Split(','))
                    {
                        if (id != "")
                        {
                            lstTemplateIds.Add(Convert.ToInt32(id));
                        }
                    }
                }

                foreach (RepeaterItem item in rptrAvailableTemplateTypes.Items)
                {
                    HiddenField hdnCategoryType = (HiddenField)item.FindControl("hdnCategoryType");
                    //HiddenField hdnTemplateCat = (HiddenField)item.FindControl("hdnTemplateCat");
                    CheckBox chkbox = (CheckBox)item.FindControl("chkSelect");
                    if (lstTemplateIds.Contains(Convert.ToInt32(hdnCategoryType.Value)))
                    {
                        chkbox.Checked = true;
                    }
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                //PromotionModel promoData = new PromotionModel();
                string typeIds = "";
                string typeNames = "";
                string typeCat = "";
                foreach (RepeaterItem item in rptrAvailableTemplateTypes.Items)
                {

                    HiddenField hdnTemplateCat = (HiddenField)item.FindControl("hdnTemplateCat");
                    HiddenField hdnCategoryType = (HiddenField)item.FindControl("hdnCategoryType");
                    HiddenField hdnCategoryName = (HiddenField)item.FindControl("hdnCategoryName");
                    HiddenField hdnTypeName = (HiddenField)item.FindControl("hdnTypeName");
                    CheckBox chkbox = (CheckBox)item.FindControl("chkSelect");
                    if (chkbox.Checked == true)
                    {
                        //PromotionModel.TemplatePromoRow row = promoData.TemplatePromo.NewTemplatePromoRow();
                        typeIds += ((typeIds == "") ? "" : ",") + hdnCategoryType.Value;
                        typeNames += ((typeNames == "") ? "" : ";:;") + hdnTypeName.Value;
                        typeCat += ((typeCat == "") ? "" : ",") + hdnTemplateCat.Value;
                        //row.category_name = hdnCategoryName.Value;
                        //row.pkey_category_type = Convert.ToInt32(hdnCategoryType.Value);
                        //row.pkey_promo_template_cat = Convert.ToInt32(hdnTemplateCat.Value);
                        //row.type_name = hdnTypeName.Value;
                        //promoData.TemplatePromo.AddTemplatePromoRow(row);
                    }
                }
                //Session["PromoDetailData"] = promoData;
                string script = string.Format("SetDataAndClose('{0}','{1}','{2}','{3}');", false.ToString().ToLower(), typeIds, typeNames, typeCat);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "jscript", script, true);
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }            
        }
    }
}