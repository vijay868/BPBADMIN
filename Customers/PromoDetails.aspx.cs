﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace Bestprintbuy.Admin.Web.Customers
{
    public partial class PromoDetails : PageBase
    {
        protected int promotionKey = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            ViewState["promoKey"] = Convert.ToInt32(Request.QueryString["promoKey"]);
            promotionKey = Convert.ToInt32(ViewState["promoKey"]);
            if (!Page.IsPostBack)
            {
                GetData();
            }
        }

        private void GetData()
        {
            try
            {
                int promoKey = Convert.ToInt32(ViewState["promoKey"]);
                PromotionModel promoDetails = PromoService.GetPromoDetails(promoKey, 0, "", ConnString);
                ProductModel TypesData = ProductService.GetListTemplateTypesForPromo(promoKey, "244", 2, ConnString);

                lbAvailableProductTypes.DataSource = promoDetails.AvailableProductTypes;
                lbAvailableProductTypes.DataTextField = "ct_name";
                lbAvailableProductTypes.DataValueField = "pkey_category_type";
                lbAvailableProductTypes.SelectionMode = ListSelectionMode.Multiple;
                lbAvailableProductTypes.DataBind();

                lbAssignedProductTypes.DataSource = promoDetails.AssignedProductTypes;
                lbAssignedProductTypes.DataTextField = "ct_name";
                lbAssignedProductTypes.DataValueField = "fkey_product_type";
                lbAssignedProductTypes.SelectionMode = ListSelectionMode.Multiple;
                lbAssignedProductTypes.DataBind();

                lbAvailableShipments.DataSource = promoDetails.AvailableShipments;
                lbAvailableShipments.DataTextField = "shipment_description";
                lbAvailableShipments.DataValueField = "shipment_on";
                lbAvailableShipments.SelectionMode = ListSelectionMode.Multiple;
                lbAvailableShipments.DataBind();

                lbAssignedShipments.DataSource = promoDetails.AssignedShipments;
                lbAssignedShipments.DataTextField = "shipment_description";
                lbAssignedShipments.DataValueField = "shipment_on";
                lbAssignedShipments.SelectionMode = ListSelectionMode.Multiple;
                lbAssignedShipments.DataBind();

                if (promoDetails.TemplatePromo.Rows.Count > 0)
                {
                    string temptype = "";
                    foreach (PromotionModel.TemplatePromoRow row in promoDetails.TemplatePromo)
                    {
                        temptype += row.type_name + "<br/>";
                    }
                }

                if (promoDetails.Promotions.Rows.Count > 0)
                {
                    PromotionModel.PromotionsRow promotionRow = (PromotionModel.PromotionsRow)promoDetails.Promotions.Rows[0];
                    txtPromoCode.Text = promotionRow.promo_code;
                    txtDescription.Text = promotionRow.description;
                    txtProductDiscount.Text = promotionRow.discount.ToString().Replace(".00", "") == "" ? "" : promotionRow.discount.ToString().Replace(".00", "");
                    txtShipmentDiscount.Text = promotionRow.ship_discount.ToString().Replace(".00", "") == "" ? "" : promotionRow.ship_discount.ToString().Replace(".00", "");
                    txtFromDate.Text = Convert.ToDateTime(promotionRow.start_date).ToString("MM/dd/yyyy");
                    txtToDate.Text = Convert.ToDateTime(promotionRow.end_date).ToString("MM/dd/yyyy");
                    chkActive.Checked = promotionRow.is_active;
                    chkNewCustomers.Checked = promotionRow.check_new_customer;
                    chkNewAgentPkg.Checked = promotionRow.apply_on_agent_package;
                    chkAdwords.Checked = promotionRow.enabled_for_ads;
                    ddlOfferedOn.SelectedValue = promotionRow.rule_type.Trim();

                    if (promotionRow.template_option == 0)
                        rbtnNone.Checked = true;
                    else if (promotionRow.template_option == 1)
                        rbtnParentCategories.Checked = true;
                    else if (promotionRow.template_option == 2)
                        rbtnTemplateTypes.Checked = true;

                    if (promotionRow.ship_discount_type)
                    {
                        rbtnShipPercentage.Checked = false;
                        rbtnShipFixed.Checked = true;
                    }
                    else
                    {
                        rbtnShipPercentage.Checked = true;
                        rbtnShipFixed.Checked = false;
                    }

                    if (promotionRow.discount_type)
                    {
                        rbtnProdPercentage.Checked = false;
                        rbtnProdFixed.Checked = true;
                    }
                    else
                    {
                        rbtnProdPercentage.Checked = true;
                        rbtnProdFixed.Checked = false;
                    }

                    txtPromoDescription.Text = promotionRow.web_desc;
                    txtPromoDisclaimer.Text = promotionRow.web_disclaimer;
                    btnDelete.Visible = true;
                }
                else
                {
                    ddlOfferedOn.SelectedValue = "D";
                    rbtnProdPercentage.Checked = true;
                    rbtnShipPercentage.Checked = true;
                    rbtnNone.Checked = true;
                    chkActive.Checked = true;
                    chkNewCustomers.Checked = true;
                }

                if (promoDetails.TemplatePromo.Rows.Count > 0)
                {
                    string temptype = "";
                    string tempids = "";
                    string tempCat = "";
                    string tempNames = "";
                    foreach (PromotionModel.TemplatePromoRow row in promoDetails.TemplatePromo)
                    {
                        tempids += row.pkey_category_type.ToString() + ",";
                        temptype += row.type_name + "\r\n";
                        tempCat += row.pkey_promo_template_cat.ToString() + ",";
                        tempNames += row.type_name + "^";
                    }
                    txtTemplateTypes.Text = temptype;
                    hdnTemplateTypeIds.Value = tempids.TrimEnd(',');
                    hdnTemplateTypeCat.Value = tempCat.TrimEnd(',');
                    hdnTemplateTypeNames.Value = tempNames.TrimEnd('^');
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("PromoList.aspx");
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int promoKey = Convert.ToInt32(ViewState["promoKey"]);
                PromotionModel PromotionDetails = new PromotionModel();
                PromotionModel.PromotionsRow promotionRow = PromotionDetails.Promotions.NewPromotionsRow();
                promotionRow.pkey_promotion = promoKey;
                promotionRow.promo_code = txtPromoCode.Text;
                promotionRow.description = txtDescription.Text;
                promotionRow.discount = txtProductDiscount.Text;
                promotionRow.ship_discount = txtShipmentDiscount.Text;
                promotionRow.start_date = Convert.ToDateTime(txtFromDate.Text);
                promotionRow.end_date = Convert.ToDateTime(txtToDate.Text);
                promotionRow.is_active = chkActive.Checked;
                promotionRow.apply_on_agent_package = chkNewAgentPkg.Checked;
                promotionRow.check_new_customer = chkNewCustomers.Checked;
                promotionRow.enabled_for_ads = chkAdwords.Checked;
                promotionRow.rule_type = ddlOfferedOn.SelectedValue;
                promotionRow.fkey_user = Identity.UserPK;
                promotionRow.is_free_promo = false;
                promotionRow.apply_to_single_item = false;

                if (rbtnNone.Checked)
                    promotionRow.template_option = 0;
                else if (rbtnParentCategories.Checked)
                    promotionRow.template_option = 1;
                else if (rbtnTemplateTypes.Checked)
                    promotionRow.template_option = 2;

                if (rbtnShipFixed.Checked)
                    promotionRow.ship_discount_type = true;
                else
                    promotionRow.ship_discount_type = false;

                if (rbtnProdFixed.Checked)
                    promotionRow.discount_type = true;
                else
                    promotionRow.discount_type = false;

                promotionRow.web_desc = txtPromoDescription.Text;
                promotionRow.web_disclaimer = txtPromoDisclaimer.Text;
                PromotionDetails.Promotions.AddPromotionsRow(promotionRow);

                string AssignedProductTypes = "";
                if (Request["ctl00$ContentPlaceHolder1$lbAssignedProductTypes"] != null)
                    AssignedProductTypes = Request["ctl00$ContentPlaceHolder1$lbAssignedProductTypes"].ToString();

                if (AssignedProductTypes != "")
                {
                    foreach (string item in AssignedProductTypes.Split(','))
                    {
                        PromotionModel.AssignedProductTypesRow assignedProductRow = PromotionDetails.AssignedProductTypes.NewAssignedProductTypesRow();
                        assignedProductRow.fkey_product_type = Convert.ToInt32(item);
                        PromotionDetails.AssignedProductTypes.AddAssignedProductTypesRow(assignedProductRow);
                    }
                }

                string AssignedShipments = "";
                if (Request["ctl00$ContentPlaceHolder1$lbAssignedShipments"] != null)
                    AssignedShipments = Request["ctl00$ContentPlaceHolder1$lbAssignedShipments"].ToString();

                if (AssignedShipments != "")
                {
                    foreach (string item in AssignedShipments.Split(','))
                    {
                        PromotionModel.AssignedShipmentsRow assignedShipmentRow = PromotionDetails.AssignedShipments.NewAssignedShipmentsRow();
                        assignedShipmentRow.shipment_on = item;
                        PromotionDetails.AssignedShipments.AddAssignedShipmentsRow(assignedShipmentRow);
                    }
                }

                if (hdnTemplateTypeIds.Value != "")
                {
                    string[] templatetypeIds = hdnTemplateTypeIds.Value.Split(",".ToCharArray());
                    string[] templatetypeCat = hdnTemplateTypeCat.Value.Split(",".ToCharArray());
                    string[] templatetypeNames = hdnTemplateTypeNames.Value.Split("^".ToCharArray());
                    for (int i = 0; i < templatetypeIds.Length; i++)
                    {
                        PromotionModel.TemplatePromoRow TemplatePromoRow = PromotionDetails.TemplatePromo.NewTemplatePromoRow();
                        TemplatePromoRow.pkey_category_type = Convert.ToInt32(templatetypeIds[i]);
                        TemplatePromoRow.pkey_promo_template_cat = Convert.ToInt32(templatetypeCat[i]);
                        TemplatePromoRow.type_name = templatetypeNames[i];
                        TemplatePromoRow.fkey_promotion = promoKey;

                        PromotionDetails.TemplatePromo.AddTemplatePromoRow(TemplatePromoRow);
                    }
                }
                promoKey = PromoService.UpdatePromoData(PromotionDetails, ConnString);
                ViewState["promoKey"] = promoKey;
                AlertScript("Promotion data saved");
                GetData();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                if (ex.Message.Contains("IX_PPS_PROMO_MASTER"))
                {
                    AlertScript("Promotion Code must be unique. Please re-enter");
                }
                else
                {
                    AlertScript(ex.Message);
                }
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int promoKey = Convert.ToInt32(ViewState["promoKey"]);
                PromoService.DeletePromotion(promoKey, ConnString);
                Response.Redirect("PromoList.aspx");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }
    }
}