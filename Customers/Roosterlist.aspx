﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Roosterlist.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Customers.Roosterlist" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Best Print Buy</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" media="all" href="../css/bestprintbuy_style.css" />
    <link rel="stylesheet" media="all" href="../css/jquery.fancybox.css" />
    <script src="../Scripts/jquery.js"></script>
    <script src="../Scripts/common.js"></script>
    <script src="../Scripts/ddsmoothmenu.js"></script>
    <script src="../Scripts/plugin/fancybox/jquery.fancybox.js"></script>
    <script src="../Scripts/jquery.jSuggest-1.1.js"></script>
    <!--[if lt IE 9]>
			<script src="../Scripts/html5.js"></script>
		<![endif]-->
    <!--[if lte IE 7]> <html class="ie7"> <![endif]-->
    <!--[if IE 8]>     <html class="ie8"> <![endif]-->
    <!--[if IE 9]>     <html class="ie9"> <![endif]-->
    <!--[if IE 10]>     <html class="ie10"> <![endif]-->
    <!--[if !IE]><!-->
    <!--<![endif]-->
    <script lang="javascript" type="text/javascript">
        $(function () {

            setTimeout(function () {
                $("#txtSearchTerm").jSuggest({
                    default_text: 'Enter Office Name',
                    terms_url: 'SearchSuggestion.aspx?input=%input%&sid=officerooster&cboKiosks=' + $("#hdncboKiosks").val(),
                    limit: 5,
                    li_onclick: 'searchSuggestion'
                });
            }, 1000);
        });

        function searchSuggestion() {
            __doPostBack('lbtnSearch', '');
        }
    </script>
    <script type="text/javascript">
        function SetDataAndClose(arrKeys, arrMCs, arrMCAddr, arrOps) {
            /*if (isNodata) {
                alert("Plaese select");
                return false;
            }*/
            window.top.SetMCData(arrKeys, arrMCs, arrMCAddr, arrOps);
            parent.$.fancybox.close();
            return false;
        }

    </script>
</head>
<body id="Body" runat="server">
    <!-- Wrapper starts -->
    <div class="popup-wrapper" runat="server">
        <div class="popup-title">
            Rooster List
        </div>
        <!-- Header Ends -->
        <div class="popup-contnt">
            <!-- Menu starts -->
            <form id="Form1" runat="server">
                <div id="trUpload" runat="server">
                    <table>
                        <tr>
                            <td colspan="2" class="bodytext">Type your company name in the search box.<br>
                                For example, <b>Houston Katy</b> can be searched by typing <b>Houston Katy</b> or
						            <b>Houston</b>. After the suggestions appear simply click on the suggestion.</td>
                        </tr>
                        <tr>
                            <td width="20%" nowrap>Company / Office Name</td>
                            <td width="70%">
                                <asp:TextBox ID="txtSearchTerm" runat="Server" CssClass="text-box" BorderStyle="NotSet" autocomplete="off"></asp:TextBox>&nbsp;&nbsp;
                                <asp:Button ID="btnReset" runat="server" OnClick="btnReset_Click" Text="Reset" CssClass="button rounded"></asp:Button>
                                <asp:LinkButton ID="lbtnSearch" OnClick="lbtnSearch_Click" runat="server" Width="0px" Height="0px"></asp:LinkButton></td>
                        </tr>
                    </table>
                </div>
            <div class="clr"></div>
                <table class="tablep">
                    <thead>
                        <tr>
                            <th>MC#</th>
                            <th>Office Name</th>
                            <th>Region</th>
                            <th>Address</th>
                            <th>Phone</th>
                            <th>OP Name</th>
                            <th>Team Lead Name</th>
                            <th>MCA Name</th>
                            <th>TechCord Name</th>
                            <th>Active</th>
                            <th>Select</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rptrRoosterList" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td style="line-height: 20px;">
                                        <%#Eval("mcid") %>
                                    </td>
                                    <td style="line-height: 20px;">
                                        <%#Eval("officename") %>
                                    </td>
                                    <td style="line-height: 20px;">
                                        <%#Eval("region") %>
                                    </td>
                                    <td style="line-height: 20px;">
                                        <%#Eval("Address") %><br />
                                        <%#Eval("city") %>
                                        <%#Eval("state") %>
                                        <%#Eval("zipcode") %>
                                    </td>
                                    <td style="line-height: 20px;">
                                        <%#Eval("phone") %>
                                    </td>
                                    <td style="line-height: 20px;">
                                        <%#Eval("opname") %>
                                    </td>
                                    <td style="line-height: 20px;">
                                        <%#Eval("teamleadname") %>
                                    </td>
                                    <td style="line-height: 20px;">
                                        <%#Eval("mcaname") %>
                                    </td>
                                    <td style="line-height: 20px;">
                                        <%#Eval("techcordname") %>
                                    </td>
                                    <td style="line-height: 20px;">
                                        <%#Convert.ToBoolean(Eval("isactive")) ? "Yes" : "No" %>
                                    </td>
                                    <td style="line-height: 20px;">
                                        <asp:CheckBox ID="chkSelect" value='<%#Eval("mcid") %>' runat="server" Checked='<%# Convert.ToBoolean(Eval("pkey_office_rooster"))%>' />
                                        <asp:HiddenField ID="hdnChkValue" runat="server" Value='<%# Eval("mcid")%>' />
                                        <asp:HiddenField ID="hdnPkey" runat="server" Value='<%# Eval("pkey_office_rooster")%>' />
                                        <asp:HiddenField ID="hdnOP" runat="server" Value='<%# Eval("opname")%>' />
                                        <asp:HiddenField ID="hdnAddr" runat="server" Value='<%# Eval("Address")%>' />
                                        <asp:HiddenField ID="hdnCity" runat="server" Value='<%# Eval("city")%>' />
                                        <asp:HiddenField ID="hdnState" runat="server" Value='<%# Eval("state")%>' />
                                        <asp:HiddenField ID="hdnzipcode" runat="server" Value='<%# Eval("zipcode")%>' />
                                    </td>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
                <table>
                    <tr>
                        <td style="float: right;">
                            <asp:Button runat="server" ID="btnSelect" CssClass="button rounded" Text="Select" OnClick="btnSelect_Click"></asp:Button>
                            <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClientClick="parent.$.fancybox.close(); return false;"></asp:Button>
                            <input id="hdncboKiosks" type="hidden" runat="server" />
                            <input id="mckeys" type="hidden" name="mckeys" runat="server" />
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</body>
</html>

