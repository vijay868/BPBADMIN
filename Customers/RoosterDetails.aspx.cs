﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;

namespace Bestprintbuy.Admin.Web.Customers
{
    public partial class RoosterDetails : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int pkey_office_rooster = Convert.ToInt32(Request.QueryString["pkey_office_rooster"]);
            CustomerModel model = UserService.GetOfficeRoosterDetail(pkey_office_rooster, ConnString);
            if (model.OfficeRooster.Rows.Count > 0)
            {
                CustomerModel.OfficeRoosterRow dr = (CustomerModel.OfficeRoosterRow)model.OfficeRooster.Rows[0];

                lblMCID.Text = dr.IsmcidNull() ? "" : dr.mcid;
                lblOffice.Text = dr.IsofficenameNull() ? "" : dr.officename;
                lblAddress.Text = dr.IsaddressNull() ? "" : dr.address;
                lblCity.Text = dr.IscityNull() ? "" : dr.city;
                lblState.Text = dr.IsstateNull() ? "" : dr.state;
                lblZip.Text = dr.IszipcodeNull() ? "" : dr.zipcode;
                lblPhone.Text = dr.IsphoneNull() ? "" : dr.phone;
                lblFax.Text = dr.IsfaxNull() ? "" : dr.fax;
                lblOP.Text = dr.IsopnameNull() ? "" : dr.opname;
                lblMCA.Text = dr.IsmcanameNull() ? "" : dr.mcaname;
                lblTL.Text = dr.IsteamleadnameNull() ? "" : dr.teamleadname;
                lblTC.Text = dr.IstechcordnameNull() ? "" : dr.techcordname;
            }

        }
    }
}