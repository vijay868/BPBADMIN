﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Collections.Specialized;
using System.Text;
using System.Configuration;
using System.Web.Services;

namespace Bestprintbuy.Admin.Web.Customers
{
    public partial class CustomerCreditList : PageBase
    {
        protected int creditcount = 0;
        protected string searchstring = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadData();
            }
        }

        private void LoadData()
        {
            try
            {
                searchstring = GetSearchCondition();
                ServiceCreditsModel model = CustomerService.GetCustomerCredits(searchstring, ConnString);
                creditcount = model.CustomerCredit.Rows.Count;
                if (CustomercreditGrid.Attributes["SortExpression"] != null)
                    model.CustomerCredit.DefaultView.Sort = CustomercreditGrid.Attributes["SortExpression"] + " " + CustomercreditGrid.Attributes["SortDirection"];
                CustomercreditGrid.DataSource = model.CustomerCredit;
                CustomercreditGrid.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnApplyCredit_Click(object sender, EventArgs e)
        {
            Response.Redirect("CustomerValidate.aspx");
        }

        protected void CustomercreditGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CustomercreditGrid.PageIndex = e.NewPageIndex;
            searchstring = GetSearchCondition();
            LoadData();
        }

        protected void CustomercreditGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            SortGridView(CustomercreditGrid, e);
            CustomercreditGrid.PageIndex = 0;
            searchstring = GetSearchCondition();
            LoadData();
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/DashBoard.aspx");
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            CustomercreditGrid.PageIndex = 0;
            btnReset.Visible = false;
            ddlOperator.SelectedIndex = 0;
            ddlFilterFor.SelectedIndex = 0;
            txtPattern.Text = "";
            LoadData();
        }

        protected int GetRecordCountForPage()
        {
            return GetRecordCountForPageSize(CustomercreditGrid.PageSize, CustomercreditGrid.PageIndex + 1);
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            try
            {
                searchstring = GetSearchCondition();
                LoadData();
                btnReset.Visible = true;
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        public String GetSearchCondition()
        {
            //Variable holding the current session object
            System.Web.SessionState.HttpSessionState currsess = System.Web.HttpContext.Current.Session;

            //Variable holding the parameters - values from the request in a named value collection
            NameValueCollection requestCollections = System.Web.HttpContext.Current.Request.Params;

            //Get the Critera , pattern, operator from the request
            String criteria = ddlFilterFor.SelectedValue;
            String roperator = ddlOperator.SelectedValue;
            String pattern = txtPattern.Text.Trim();

            //Check if pattern exists
            if ("".Equals(pattern)) return "";

            //Create a new instance of named value collection
            NameValueCollection nvc = new NameValueCollection();

            //Add the Critera , pattern, operator to the nvc
            nvc.Add("criteria", criteria);
            nvc.Add("operator", roperator);
            nvc.Add("pattern", pattern);
            currsess["combovalues"] = nvc;

            String searchfor = FormSearchString(nvc);
            currsess["search_condition"] = searchfor;
            currsess["pattern_value"] = pattern;

            return searchfor;
        }

        private String FormSearchString(NameValueCollection nvc)
        {
            String sOperator = nvc["operator"];
            sOperator = sOperator != null ? sOperator.Trim() : sOperator;
            string pattern = nvc["pattern"];
            StringBuilder sb = new StringBuilder();


            if (sOperator.IndexOf("LIKE") < 0)
            {
                pattern = pattern.Replace("'", "''");
                sb.Append(nvc["criteria"]).Append(" ").Append(sOperator).Append(" ").Append("'").Append(pattern).Append("'");
            }
            else if (sOperator != null && (sOperator.ToLower()).IndexOf("in") > 0)
            {
                sb.Append(nvc["criteria"]).Append("  ").Append(sOperator).Append(" (").Append(pattern).Append(")");
            }
            else
            {
                sOperator = sOperator.Replace("LIKE", pattern);
                sOperator = sOperator.Replace("'", "''");
                sb.Append(nvc["criteria"]).Append(" LIKE '").Append(sOperator).Append("'");
            }
            return sb.ToString();
        }

        public string GetAnchor(object obj, object obj1)
        {
            if (obj == null)
                return "N/A";
            else if (obj.ToString() == "0.00")
                return "N/A";
            try
            {
                return string.Format("<a href=\"#\" class=\"bold\" onclick=\"javascript:OpenCreditStetusDialog('{0}')\">{1}</a>", obj1.ToString(), obj.ToString());
            }
            catch
            {
                return "";
            }
        }

        [WebMethod]
        public static List<string> GetAutoCompleteData(string input, int field)
        {
            List<string> result = new List<string>();
            string strField = "";
            if (field == 0)
                strField = "EMAIL_ID";
            else if (field == 1)
                strField = "NAME";
            else if (field == 2)
                strField = "PO_NUMBER";
            string connstring = ConfigurationManager.AppSettings["BestPrintBuyDBConn"];
            result = UserService.GetAutoSearchInfoList(1, "%" + input + "%", strField, connstring);
            return result;
        }
    }
}