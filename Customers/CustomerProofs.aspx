﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomerProofs.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Customers.CustomerProofs" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Best Print Buy</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" media="all" href="../css/bestprintbuy_style.css" />    
	<link rel="stylesheet" media="all" href="../css/jquery.fancybox.css" />
	<script src="../Scripts/jquery.js"></script>
	<script src="../Scripts/common.js"></script>
    <script src="../Scripts/ddsmoothmenu.js"></script>
	<script src="../Scripts/plugin/fancybox/jquery.fancybox.js"></script>
    <!--[if lt IE 9]>
			<script src="../Scripts/html5.js"></script>
		<![endif]-->
    <!--[if lte IE 7]> <html class="ie7"> <![endif]-->
    <!--[if IE 8]>     <html class="ie8"> <![endif]-->
    <!--[if IE 9]>     <html class="ie9"> <![endif]-->
    <!--[if IE 10]>     <html class="ie10"> <![endif]-->
    <!--[if !IE]><!-->
    <!--<![endif]-->

    <script lang="javascript" type="text/javascript">

        function ApproveStatus(targetaction) {
            if (document.getElementById('hdnChekValue').value != "") {
                //document.getElementById("TargetAction").value = targetaction;
                //document.frmCustomerProofs.submit();
                return true;
            }
            else {
                alert('Plese select atleast one proof');
                return false;
            }
        }

        //function OpenImagePopUp(url) {
        //    props = "width=650,height=400,resizable=1,scrollbars=1,toolbar=0,menubar=0";
        //    newwindow = window.open(url, "Proof", props);
        //}

        function ProofSelect(check) {
            if (check.checked == true) {
                document.getElementById('hdnChekValue').value = check.value;
                var frmCustomerProofs = document.forms['frmCustomerProofs'];
                for (i = 0; i < frmCustomerProofs.elements.length; i++) {
                    if (frmCustomerProofs.elements[i].type == "checkbox") {
                        if (frmCustomerProofs.elements[i].name != check.name && frmCustomerProofs.elements[i].disabled != true) {
                            frmCustomerProofs.elements[i].checked = false;
                        }
                    }
                }
            }
            else {
                document.getElementById('hdnChekValue').value = "";
            }
        }

        function ShowImgPreview(url) {
            $.fancybox({
                'width': '50%',
                'height': '40%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': url,
                'type': 'iframe'
            });
            return false;
        }
    </script>
</head>

<body id="Body" runat="server">
   <!-- Wrapper starts -->
    <div id="Div1" class="popup-wrapper" runat="server">
        <div class="popup-title">	
           Customer Proofs
			</div>
        <!-- Header Ends -->
        <div class="popup-contnt">
            <!-- Menu starts -->
            <form id="frmCustomerProofs" runat="server">
                    <table class="tablep">
                        <thead>
                            <tr>
                                <th>Version Name</th>
                                <th>Front</th>
                                <th>Reverse</th>
                                <th>Approve</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater ID="rptrCustomerProofs" runat="server" OnItemDataBound="rptrCustomerProofs_ItemDataBound">
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <%# GetProofDetailUrl(DataBinder.Eval(Container, "DataItem.pkey_proof"),DataBinder.Eval(Container, "DataItem.proof_name"),DataBinder.Eval(Container, "DataItem.isunread"),DataBinder.Eval(Container, "DataItem.isconfirmed")) %>
                                        </td>
                                        <td>
                                            <%# GetImageUrl(DataBinder.Eval(Container, "DataItem.front_image_url"),"Front Image") %>
                                        </td>
                                        <td>
                                            <%# GetImageUrl(DataBinder.Eval(Container, "DataItem.reverse_image_url"),"Reverse Image") %>
                                        </td>
                                        <td>
                                            <input type="checkbox" runat="server" onclick="ProofSelect(this)" value='<%# DataBinder.Eval(Container, "DataItem.pkey_proof")%>' id="chkAdminApproval" name="chkAdminApproval" />
                                        </td>
                                        <td>
                                            <input type="checkbox" runat="server" onclick="ProofSelect(this)" value='<%# DataBinder.Eval(Container, "DataItem.pkey_proof")%>' id="chkDelete" name="chkDelete" />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                <asp:Panel ID="pnlAddProof" runat="server" Visible="False">
                    <table>
                        <tr>
                            <td><b>Version</b></td>
                            <td>
                                <asp:TextBox runat="server" ID="txtProofName" CssClass="text"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Front Proof</b></td>
                            <td>
                                <input id="uploadFrontProof" type="file" name="uploadFrontProof" runat="server" />
                            </td>
                        </tr>
                        <div id="pnlBackProof" Visible="False" Runat="server">
                        <tr>
                            <td><b>Back Proof</b></td>
                            <td>
                                <input id="uploadBackProof" type="file" name="uploadBackProof" runat="server" />
                            </td>
                        </tr></div>
                    </table>
                </asp:Panel>
                <table>
                    <tr>
                        <td style="float:left">
                            <asp:Button runat="server" ID="btnAdd" CssClass="button rounded" Text="Add Proofs" OnClick="btnAdd_Click"></asp:Button>
                        </td>
                        <td style="float:right">
                        <asp:Button runat="server" ID="btnApprove" CssClass="button rounded" Text="Approve" OnClick="btnApprove_Click" OnClientClick="return ApproveStatus('Approve')"></asp:Button>
                        <asp:Button runat="server" ID="btnSave" CssClass="button rounded" Text="Save" OnClick="btnSave_Click"></asp:Button>
                        <asp:Button runat="server" ID="btnDelete" CssClass="button rounded" Text="Delete" OnClick="btnDelete_Click" OnClientClick="return ApproveStatus('')"></asp:Button>
                            <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClientClick="parent.$.fancybox.close(); return false;"></asp:Button>
                        <asp:HiddenField ID="hdnChekValue" runat="server" />
                        <asp:HiddenField ID="txtEmailAddress" runat="server" />
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</body>
</html>
