﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CatelogReqPopup.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Customers.CatelogReqPopup" %>

<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->

<!-- Consider adding a manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<head id="Head1" runat="server">
    <title>Best Print Buy</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" media="all" href="../css/bestprintbuy_style.css" />
    <link rel="stylesheet" media="all" href="../css/jquery.fancybox.css" />
    <link rel="stylesheet" href="../css/jquery-ui.css" />
    <script src="../Scripts/jquery.js"></script>
    <script src="../Scripts/jquery-ui.js"></script>
    <script src="../Scripts/common.js"></script>
    <script src="../Scripts/ddsmoothmenu.js"></script>
    <script src="../Scripts/plugin/fancybox/jquery.fancybox.js"></script>
    <script src="../Scripts/jquery.validate.js"></script>
    <!--[if lt IE 9]>
			<script src="../Scripts/html5.js"></script>
		<![endif]-->
    <!--[if lte IE 7]> <html class="ie7"> <![endif]-->
    <!--[if IE 8]>     <html class="ie8"> <![endif]-->
    <!--[if IE 9]>     <html class="ie9"> <![endif]-->
    <!--[if IE 10]>     <html class="ie10"> <![endif]-->
    <!--[if !IE]><!-->
  
    <!--<![endif]-->
</head>

   <body id="Body" runat="server">
    <!-- Wrapper starts -->
    <div id="Div1" class="popup-wrapper" runat="server">
     <div class="popup-title">
           Catalog Request details
        </div>
        <!-- Header Ends -->
        <div class="popup-contnt">
            <!-- Menu starts -->
            <form id="Form1" runat="server">
                <table style="padding-right:20px;">
                    <tr>
                        <td><label>Name</label></td>
                        <td class="tdalign">
                            <asp:Label ID="lblName" runat="server" Font-Size="12px" ></asp:Label></td>
                    </tr>
                    <tr>
                        <td><label>Email</label></td>
                        <td>
                            <asp:Label ID="lblEmail" runat="server" Font-Size="12px" ></asp:Label></td>
                    </tr>
                    <tr>
                        <td><label>Company (optional)</label></td>
                        <td class="tdalign">
                            <asp:Label ID="lblCompany" runat="server" Font-Size="12px" ></asp:Label></td>
                    </tr>
                    <tr>
                        <td><label>Street</label></td>
                        <td class="tdalign">
                            <asp:Label ID="lblStreet" runat="server" Font-Size="12px" ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td><label>City</label></td>
                        <td>
                            <asp:Label ID="lblCity" runat="server" Font-Size="12px" ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td><label>State</label></td>
                        <td class="tdalign">
                            <asp:Label ID="lblState" runat="server" Font-Size="12px" ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td><label>Zip</label></td>
                        <td class="tdalign">
                            <asp:Label ID="lblZip" runat="server" Font-Size="12px" ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td><label>Country</label></td>
                        <td class="tdalign">
                            <asp:Label ID="lblCountry" runat="server" Font-Size="12px" ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td><label>No of agents in your office</label></td>
                        <td class="tdalign">
                            <asp:Label ID="lblNoofAgents" runat="server" Font-Size="12px" ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td><label>Contact Person</label></td>
                        <td class="tdalign">
                            <asp:Label ID="lblContactPerson" runat="server" Font-Size="12px" ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td><label>Telephone</label></td>
                        <td class="tdalign">
                            <asp:Label ID="lblPhone" runat="server" Font-Size="12px" ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td><label>Quantity of Catalogs</label></td>
                        <td class="tdalign">
                            <asp:Label ID="lblCatalogQty" runat="server" Font-Size="12px" ></asp:Label>
                        </td>
                    </tr>
                     <tr><td class="tdalign"></td>
                        <td class="tdalign" style="text-align:right">
                            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="button rounded" OnClientClick="parent.$.fancybox.close(); return false;"/>
                        </td>
                    </tr>
                </table>
                </form>
          </div>  
  </div>
</body>
</html>
