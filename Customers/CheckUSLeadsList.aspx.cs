﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Utilities;
using Bestprintbuy.Admin.Web.Common;

namespace Bestprintbuy.Admin.Web.Customers
{
    public partial class CheckUSLeadsList : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckPendingUSLeadsForDownload();
        }

        public void CheckPendingUSLeadsForDownload()
        {
            try
            {
                SupplierPOModel orderData = CartService.GetPendingUSLeadsListForDownload(ConnString);
                foreach (SupplierPOModel.CustOrdersRow orderRow in orderData.CustOrders.Rows)
                {
                    string myServiceUrl = WebConfiguration.USLeadsWebServiceURL;
                    string myToken = null;
                    Int32 pkey_customer = orderRow.pkey_customer;
                    //Dim serverPath As String = System.Web.HttpContext.Current.Server.MapPath(".").ToLower()
                    //serverPath = serverPath.Replace("\transactions", "") & "\DesignCenter\Users\"
                    string serverPath = System.Configuration.ConfigurationManager.AppSettings["BPBCustomerBaseDirectory"];
                    serverPath = serverPath + "\\DesignCenter\\Users\\";

                    myToken = orderRow.usleadstoken;

                    Usadata.ListModuleV3.Gateway.GatewayService myService = new Usadata.ListModuleV3.Gateway.GatewayService();
                    Usadata.ListModuleV3.Gateway.OrderStatusResult myOrderStatus;

                    System.IO.DirectoryInfo pathDir = new System.IO.DirectoryInfo(serverPath + string.Format("{0}\\{1}", pkey_customer, "DirectMail"));
                    if (!pathDir.Exists)
                    {
                        pathDir.Create();
                    }
                    else
                    {
                        pathDir.Refresh();
                    }


                    try
                    {
                        myService.Url = myServiceUrl;
                        myOrderStatus = myService.getOrderStatus(myToken);

                        switch (myOrderStatus.Status)
                        {
                            case Usadata.ListModuleV3.Gateway.OrderStatus.Ready:
                                System.Net.WebClient myWebClient = new System.Net.WebClient();
                                string myFileName = serverPath + string.Format("{0}\\{1}\\{2}.csv", pkey_customer, "DirectMail", myToken);
                                string poFileName = serverPath + string.Format("{0}\\{1}\\{2}", pkey_customer, "DirectMail", orderRow.csv_FileNM);
                                myWebClient.DownloadFile(myOrderStatus.DownloadUrl, myFileName);
                                System.IO.File.Copy(myFileName, poFileName, true);


                                PDFCreatorWS.PDFCreator pdfws = new PDFCreatorWS.PDFCreator();
                                pdfws.CopyCSVToAssetsFolder(orderRow.pkey_cust_po_dtl, poFileName, orderRow.csv_FileNM);
                                break;
                            //Server.Execute(Me.VitualPath + "/web/CartOrder.aspx")

                        }

                    }
                    catch (Exception ex)
                    {
                        string str = ex.StackTrace;
                        FileLogger.WriteException(ex);
                    }
                }
            }
            catch (Exception ex)
            {
                string str = ex.StackTrace;
                FileLogger.WriteException(ex);
            }
        }

        private void CheckTestOrder()
        {
            // --- the URL of the Leads Module service
            //
            string myServiceUrl = WebConfiguration.USLeadsWebServiceURL;

            // --- The token obtained by the launching application.
            //
            string myToken = "ed0bff1ff6d84e18a485a2c7893f3210";

            // --- create an instance of the web service client.
            //
            Usadata.ListModuleV3.Gateway.GatewayService myService = new Usadata.ListModuleV3.Gateway.GatewayService();

            // --- declare the order status return object.
            //
            Usadata.ListModuleV3.Gateway.OrderStatusResult myOrderStatus;


            // --- call the "getOrderStatus()" method.
            //

            try
            {
                myService.Url = myServiceUrl;
                myOrderStatus = myService.getOrderStatus(myToken);

                // --- inspect the returned object status
                //
                switch (myOrderStatus.Status)
                {

                    case Usadata.ListModuleV3.Gateway.OrderStatus.Ready:

                        // --- the order is ready, download the file to 
                        // --- the local disk using the WebClient class.
                        //
                        System.Net.WebClient myWebClient = new System.Net.WebClient();
                        string myFileName = string.Format("c:\\someorderid.csv");

                        myWebClient.DownloadFile(myOrderStatus.DownloadUrl, myFileName);

                        System.Diagnostics.Debug.WriteLine("Order was successfully downloaded from " + myOrderStatus.DownloadUrl);

                        break;
                    default:

                        // --- order is not ready, report the status and 
                        // --- check the status at a later time.
                        //
                        System.Diagnostics.Debug.WriteLine("Order Status: " + myOrderStatus.Status.ToString());

                        break;
                }


            }
            catch (Exception ex)
            {
                // --- handle the exception here
                //
                System.Diagnostics.Debug.WriteLine("There was an error attempting to check the status of an session order ('" + myToken + "') - " + ex.Message.ToString());

            }
        }
    }
}