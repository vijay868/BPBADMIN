﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using System.Data;
using Bestprintbuy.Admin.Utilities;


namespace Bestprintbuy.Admin.Web.Customers
{
    public partial class CustomerProofs : PageBase
    {
        CustomerProofModel custProofData = null;
        protected string fkey_cust_po_dtl;
        protected bool HasReverse = false;
        protected Int32 fkey_customer = 0;
        protected string CustomerName;
        string ponumber;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Identity.IsSuperUser)
            {
                btnApprove.Visible = true;
            }
            if (!IsPostBack)
            {
                int fkey_cust_po_dtl = Convert.ToInt32(Request.QueryString["poDtlKey"]);
                //Dim isBackExits As String = Convert.ToString(Request.Item("IsBack"))


                btnSave.Visible = false;
                custProofData = CustomerService.GetCustomerProofList(fkey_cust_po_dtl, ConnString);
                InitPageData();
            }
            Page.DataBind();
        }

        private void InitPageData()
        {
            try
            {
                if (custProofData.CustomerProofs.Rows.Count > 0)
                {
                    rptrCustomerProofs.DataSource = custProofData.CustomerProofs;
                    rptrCustomerProofs.DataBind();
                }
                if (custProofData.Custorderdtls.Rows.Count > 0)
                {
                    CustomerProofModel.CustorderdtlsRow row = (CustomerProofModel.CustorderdtlsRow)custProofData.Custorderdtls.Rows[0];
                    fkey_cust_po_dtl = row.pkey_cust_po_dtl.ToString();
                    HasReverse = row.hasreverse;
                    fkey_customer = row.fkey_customer;
                    ponumber = row.po_number;
                    txtEmailAddress.Value = row.email_id;
                    CustomerName = row.first_name;
                    if (ViewState["fkey_customer"] == null)
                    {
                        ViewState["fkey_customer"] = fkey_customer;
                    }
                }
                if (custProofData.VersionName.Rows.Count > 0)
                {
                    pnlAddProof.Visible = true;
                    btnAdd.Visible = false;
                    btnSave.Visible = true;
                    btnApprove.Visible = false;
                    txtProofName.Text = custProofData.VersionName.Rows[0]["proof_name"].ToString();
                }
                if (HasReverse)
                {
                    pnlBackProof.Visible = true;
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string frontimageurl = "";
                string backimageurl = "";
                if (!string.IsNullOrEmpty(uploadFrontProof.Value))
                {
                    frontimageurl = CopyImagesToWorkingArea(uploadFrontProof.Value, "FRONT");
                }
                if (!string.IsNullOrEmpty(uploadBackProof.Value))
                {
                    backimageurl = CopyImagesToWorkingArea(uploadBackProof.Value, "BACK");
                }
                int fkey_cust_po_dtl = Convert.ToInt32(Request.QueryString["poDtlKey"]);
                CustomerService.UpdateCustomerProofs(fkey_cust_po_dtl, txtProofName.Text, frontimageurl, backimageurl, ConnString);
                pnlAddProof.Visible = false;
                btnAdd.Visible = true;
                btnSave.Visible = false;
                custProofData = CustomerService.GetCustomerProofList(fkey_cust_po_dtl, ConnString);
                InitPageData();
                AlertScript("Successfully Saved.");
                return;
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript("Save Failed. Try again.");
            }
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            try
            {
                int fkey_cust_po_dtl = Convert.ToInt32(Request.QueryString["poDtlKey"]);
                CustomerService.UpdateStatus(Convert.ToInt32(hdnChekValue.Value), ConnString);
                pnlAddProof.Visible = false;
                custProofData = CustomerService.GetCustomerProofList(fkey_cust_po_dtl, ConnString);
                AlertScript("The proof has been sent to the user account");

                InitPageData();
                AuthnticateCustomer();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
            return;
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                int fkey_cust_po_dtl = Convert.ToInt32(Request.QueryString["poDtlKey"]);
                custProofData = CustomerService.GetCustomerProofList(fkey_cust_po_dtl, ConnString);
                custProofData = CustomerService.GetVersionName(fkey_cust_po_dtl, custProofData, ConnString);
                InitPageData();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
            return;
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int fkey_cust_po_dtl = Convert.ToInt32(Request.QueryString["poDtlKey"]);
                CustomerService.DeleteProof(Convert.ToInt32(hdnChekValue.Value), ConnString);
                pnlAddProof.Visible = false;
                custProofData = CustomerService.GetCustomerProofList(fkey_cust_po_dtl, ConnString);
                InitPageData();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
            return;
        }

        private string CopyImagesToWorkingArea(string url, string Side)
        {
            fkey_customer = Convert.ToInt32(ViewState["fkey_customer"]);
            string serverPath = System.Configuration.ConfigurationManager.AppSettings["BPBCustomerBaseDirectory"];
            serverPath = serverPath + "\\DesignCenter\\Users\\" + string.Format("{0}\\{1}\\", fkey_customer, "CustomerProofs");

            string CustomerBaseUrl = System.Configuration.ConfigurationManager.AppSettings["BPBCustomerBaseURL"];
            CustomerBaseUrl = CustomerBaseUrl + "/DesignCenter/Users/" + string.Format("{0}/{1}/", fkey_customer, "CustomerProofs");
            System.IO.FileInfo fileCopy = new System.IO.FileInfo(url);

            System.IO.DirectoryInfo pathDir = new System.IO.DirectoryInfo(serverPath);
            if (!pathDir.Exists)
            {
                pathDir.Create();
            }
            else
            {
                pathDir.Refresh();
            }
            string Filename = fileCopy.Name;
            string ProofName = txtProofName.Text;
            ProofName = ProofName + "-" + Side + ".jpg";
            Filename = Filename.Replace(Filename, ProofName);
            try
            {
                serverPath = serverPath + Filename;
                if (Side == "BACK")
                {
                    uploadBackProof.PostedFile.SaveAs(serverPath);
                }
                else
                {
                    uploadFrontProof.PostedFile.SaveAs(serverPath);
                }

                return CustomerBaseUrl + Filename;

            }
            catch (Exception ex)
            {
            }
            return "";
        }

        private void AuthnticateCustomer()
        {
            UserIdentity identity = ValidateCustomerEMail();
            UserPrincipal principal = new UserPrincipal(identity, "roles");
            string strAUIUrl = GetCustomerAccessAUIUrl();
            if (identity.UserPK > 0)
            {
                strAUIUrl = string.Format("{0}?uid={1}&email={2}&fkey_admin={3}&cp=true", strAUIUrl, GetAdminCustAccessGUID(), txtEmailAddress.Value, this.Identity.UserPK);
            }
            string strMessage = ReadMailContent("CustomerOrderProof");
            strMessage = string.Format(strMessage, CustomerName, strAUIUrl, ponumber);
            //WebLogger.WriteInfo(String.Format("{1}-{2}{0}{3}{0}", Environment.NewLine, CustomerName, ponumber, strMessage))
            SendEmail(txtEmailAddress.Value, strMessage, ponumber);
            return;
        }

        private UserIdentity ValidateCustomerEMail()
        {
            UserIdentity uidentity = null;
            string emailid = txtEmailAddress.Value;
            int loginOption = 1;
            //loginOption = Convert.ToInt32(ddlLoginID.SelectedValue);
            int fkey_site = 1;
            //if (ddlKiosk.SelectedItem.Text == "smb")
            //{
            //    fkey_site = 2;
            //}
            string superUserPwd = "";
            string accessGuid = "";
            int fkey_admin_user = -1;
            if (Identity != null)
            {
                fkey_admin_user = Identity.UserPK;
            }

            //if (chksuperuser.Checked)
            //{
            //    if (txtPassword.Text != "")
            //    {
            //        superUserPwd = MD5Encryption.Encrypt(txtPassword.Text);
            //    }
            //}
            accessGuid = string.Format("{0}{1}{2}", fkey_admin_user, emailid,
                    DateTime.Now.ToString("MM/dd/yyyy hh:mi:ss"));
            accessGuid = MD5Encryption.Encrypt(accessGuid);
            accessGuid = accessGuid.Substring(0, Math.Min(244, accessGuid.Length));

            int intfkey_ec_attribute = -1;
            string m_strExclusiveCustomer = "";
            if (m_strExclusiveCustomer != string.Empty)
            {
                intfkey_ec_attribute = Convert.ToInt32(
                    WebConfiguration.ProductColorKeys["ExclusiveCustomer " + m_strExclusiveCustomer.ToUpper()]);
            }
            CustomerModel l_customerModel = CustomerService.AdminValidateCustomerModel(emailid, intfkey_ec_attribute,
                loginOption, superUserPwd, fkey_admin_user, accessGuid, fkey_site, ConnString);


            int cuspkey = -1;
            bool login = false;
            string email;
            string name;
            bool isAdmin = false;
            string role = "USER";
            int fkeyregister = -1;

            if (l_customerModel.Customers.Rows.Count == 1)
            {
                CustomerModel.CustomersRow cusRow = (CustomerModel.CustomersRow)l_customerModel.Customers.Rows[0];
                emailid = cusRow.customer_id;
                cuspkey = cusRow.pkey_customer;
                login = true;
                name = cusRow.cus_firstname;
                email = cusRow.email;

                adminCustAccessGUID = cusRow.superuserguid;

                customerAccessAUIUrl = cusRow.CustPortalAUIUrl;

                uidentity = new UserIdentity(emailid, cuspkey, login, isAdmin, name, email, role, fkeyregister);
                if (loginOption == 2)
                {
                    Identity.ExclusiveCustomerOf = cusRow.Isattribute_codeNull() ? "" : cusRow.attribute_code;
                    Identity.ExclusiveCustomerKey = cusRow.Isfkey_ec_attributeNull() ? -1 : Convert.ToInt32(cusRow.fkey_ec_attribute);
                }
            }
            return uidentity;
        }

        private string adminCustAccessGUID;
        public string GetAdminCustAccessGUID()
        {
            return adminCustAccessGUID;
        }

        private string customerAccessAUIUrl = "";
        public string GetCustomerAccessAUIUrl()
        {
            return customerAccessAUIUrl;
        }

        private string ReadMailContent(string strMailFor)
        {
            try
            {
                string strFile = null;
                string strContent = null;
                strFile = WebConfiguration.GetEmailHTMLLoc(strMailFor);
                System.IO.StreamReader file = new System.IO.StreamReader(strFile);
                strContent = file.ReadToEnd();
                file.Close();
                file = null;
                return strContent;
            }
            catch (System.IO.FileNotFoundException fnfEx)
            {
                return string.Empty;
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                return string.Empty;
            }
        }

        protected string GetProofDetailUrl(object proofKey, object proofname, object IsUnread, object Approved)
        {
            if (!IsDBNull(proofKey))
            {
                bool unread = Convert.ToBoolean(IsUnread);
                string classname = "";
                string Commentedicon = "";
                if (unread)
                {
                    classname = "a1b";
                    Commentedicon = "<img src='../images/admin/custcomments.png' border=0 >";
                }
                if (!IsDBNull(Approved))
                {
                    if (Convert.ToBoolean(Approved))
                    {
                        Commentedicon = "<img src='../images/admin/approved.png' border=0 >";
                    }
                }
                string strUrl = "<a href='{0}' class='{1}'>{2}</a>{3}";
                //Dim strOnClick As String = "onclick=""javascript:window.open('{0}', 'Proof', 'width=650px, height=500px, toolbar=0, scrollbars=1');"""
                string Url = "CustomerProofDetail.aspx?PkeyProof=" + Convert.ToString(proofKey) + "&PONumber=" + Convert.ToString(ponumber) + "&timeStamp = " + DateTime.Now.Ticks.ToString();
                //strOnClick = String.Format(strOnClick, imageurl)
                return string.Format(strUrl, Url, classname, proofname, Commentedicon);
            }


            return "N/A";
        }
        protected string GetImageUrl(object imageurl, object Side)
        {
            if (!IsDBNull(imageurl) & !string.IsNullOrEmpty(imageurl.ToString()))
            {
                string strUrl = "<a href='#' {0} class='a1b'>{1}</a>";
                //Dim downloadUrl As String = "&nbsp;&nbsp;<a href='{0}' alt = '{1}' target='_blank'><img src='../images/admin/download.png' border=0 ></a>"
                string strOnClick = "onclick=\"javascript:ShowImgPreview('{0}'); return false;\"";
                strOnClick = string.Format(strOnClick, imageurl);
                return string.Format(strUrl, strOnClick, Side);
            }

            return "N/A";
        }

        private void SendEmail(string mailId, string strMessage, string ponumber)
        {
            string fromMailId = WebConfiguration.BPBCustomerProofsEmail;
            strMessage = strMessage.Replace("##kiosk##", "");
            strMessage = strMessage.Replace("##ecid##", "");
            strMessage = strMessage.Replace("##eckey##", "");
            strMessage = strMessage.Replace("&#123;", "{");
            strMessage = strMessage.Replace("&#125", "}");
            EmailProcessor.SendMail(fromMailId, mailId + ";" + fromMailId, strMessage, "[DO NOT REPLY] Order Proof - " + ponumber);

        }

        protected void rptrCustomerProofs_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item | e.Item.ItemType == ListItemType.AlternatingItem)
            {
                bool proofvalid = false;
                HtmlInputCheckBox chkSel = default(HtmlInputCheckBox);
                chkSel = (HtmlInputCheckBox)e.Item.FindControl("chkAdminApproval");
                HtmlInputCheckBox chkDel = default(HtmlInputCheckBox);
                chkDel = (HtmlInputCheckBox)e.Item.FindControl("chkDelete");
                DataRowView row = (DataRowView)e.Item.DataItem;
                if (!IsDBNull(row["isproofvalid"]))
                {
                    proofvalid = Convert.ToBoolean(row["isproofvalid"]);
                }
                if (!IsDBNull(row["isconfirmed"]))
                {
                    if (Convert.ToBoolean(row["isconfirmed"]))
                    {
                        btnAdd.Visible = false;
                        btnApprove.Visible = false;
                        btnDelete.Visible = false;
                        chkDel.Disabled = true;
                    }
                }
                if (proofvalid)
                {
                    chkSel.Checked = true;
                    chkSel.Disabled = true;
                }
                else
                {
                    chkSel.Checked = false;
                    chkSel.Disabled = false;
                }
            }
        }
    }
}