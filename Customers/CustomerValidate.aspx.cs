﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.Customers
{
    public partial class CustomerValidate : PageBase
    {
        protected int loginOption = 1;
        protected string txtEmail = "";
        protected bool ispo;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void lbtnValidate_Click(object sender, EventArgs e)
        {
            try
            {
                int fkey_customer = -1;
                bool isActive = false;
                txtEmail = txtEmailID.Text;
                loginOption = Convert.ToInt32(ddlValidateType.SelectedValue);
                ServiceCreditsModel.CustomerCreditDataTable validatecustomer = CustomerService.ValidateCustomerInfo(txtEmail, loginOption, ConnString);
                if (validatecustomer.Rows.Count > 0)
                {
                    //Response.Redirect("CustomerCreditDetails.aspx?Pkey=" + validatecustomer.Rows[0]["pkey_service_credit"] + "&fkeycustomer=" + validatecustomer.Rows[0]["fkey_customer"] + "&ponumber=" + validatecustomer.Rows[0]["po_number"] + "&ispo=true");
                    fkey_customer = Convert.ToInt32(validatecustomer.Rows[0]["fkey_customer"]);
                    isActive = Convert.ToBoolean(validatecustomer.Rows[0]["is_active"]);
                }
                if (fkey_customer <= 0)
                {
                    if (loginOption == 1)
                        AlertScript("Invalid Customer Email ID.");
                    else
                        AlertScript("Invalid Customer PO Number.");
                    txtEmail = "";
                    loginOption = 1;
                }
                else if (loginOption == 2 && !isActive)
                {
                    txtEmail = "";
                    loginOption = 1;
                    AlertScript("You cannot avail credits on purchase orders that are older than 90 days.");
                }
                else
                {
                    if (loginOption == 2)
                    {
                        ispo = true;
                    }
                    Response.Redirect("CustomerCreditDetails.aspx?Pkey=-2&fkeycustomer=" + fkey_customer + "&ponumber=" + txtEmail + "&ispo=" + ispo);
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("CustomerCreditList.aspx");
        }
    }
}