﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;

namespace Bestprintbuy.Admin.Web.Customers
{
    public partial class Roosterlist : PageBase
    {
        protected int fkey_ec_attribute = 170;
        private string[] mcofficeKeys;
        private string mcKey = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["cboKiosks"] != null && Request.QueryString["cboKiosks"] != "undefined")
            {
                hdncboKiosks.Value = Request.QueryString["cboKiosks"];
                fkey_ec_attribute = Convert.ToInt32(Request.QueryString["cboKiosks"]);
            }
            if (Request.QueryString["mckeys"] != null && Request.QueryString["mckeys"] != "undefined")
            {
                mcKey = Request.QueryString["mckeys"];
                mcofficeKeys = mcKey.Split(',');
            }
            mckeys.Value = mcKey;
            if (!Page.IsPostBack)
            {
                CustomerModel model = UserService.GetOfficeRoosterList(hdncboKiosks.Value, txtSearchTerm.Text, mckeys.Value, ConnString);
                rptrRoosterList.DataSource = model.OfficeRooster;
                rptrRoosterList.DataBind();
            }
        }

        //protected void rptrRoosterList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        //{
        //    foreach (RepeaterItem item in rptrRoosterList.Items)
        //    {
        //        CheckBox chk = (CheckBox)item.FindControl("chkSelect");
        //        HiddenField hdnPkey = (HiddenField)item.FindControl("hdnPkey");
        //        chk.Checked = IsSelected(hdnPkey.Value);
        //    }

        //}

        //private bool IsSelected(string obj)
        //{
        //    if (mcKey == null)
        //        return false;
        //    if (string.IsNullOrEmpty(mcKey))
        //        return false;
        //    bool strChkStatus = false;
        //    try
        //    {
        //        Int32 iCounter = default(Int32);
        //        for (iCounter = 0; iCounter <= mcofficeKeys.Length - 1; iCounter++)
        //        {
        //            if (mcofficeKeys[iCounter] == obj)
        //            {
        //                strChkStatus = true;
        //                break;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    return strChkStatus;
        //}

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                txtSearchTerm.Text = "Halifax";
                CustomerModel model = UserService.GetOfficeRoosterList(hdncboKiosks.Value, txtSearchTerm.Text, mckeys.Value, ConnString);
                rptrRoosterList.DataSource = model.OfficeRooster;
                rptrRoosterList.DataBind();
                btnReset.Visible = true;
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void lbtnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                CustomerModel model = UserService.GetOfficeRoosterList(hdncboKiosks.Value, txtSearchTerm.Text, mckeys.Value, ConnString);
                rptrRoosterList.DataSource = model.OfficeRooster;
                rptrRoosterList.DataBind();
                btnReset.Visible = true;
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnSelect_Click(object sender, EventArgs e)
        {
            //PromotionModel promoData = new PromotionModel();
            string arrKeys = "";
            string arrMCs = "";
            string arrMCAddr = "";
            string arrOps = "";

            foreach (RepeaterItem item in rptrRoosterList.Items)
            {

                HiddenField hdnPkey = (HiddenField)item.FindControl("hdnPkey");
                HiddenField hdnOP = (HiddenField)item.FindControl("hdnOP");
                HiddenField hdnAddr = (HiddenField)item.FindControl("hdnAddr");
                HiddenField hdnCity = (HiddenField)item.FindControl("hdnCity");
                HiddenField hdnState = (HiddenField)item.FindControl("hdnState");
                HiddenField hdnzipcode = (HiddenField)item.FindControl("hdnzipcode");
                HiddenField hdnChkValue = (HiddenField)item.FindControl("hdnChkValue");
                CheckBox chkbox = (CheckBox)item.FindControl("chkSelect");
                if (chkbox.Checked == true)
                {
                    arrKeys += ((arrKeys == "") ? "" : ",") + hdnPkey.Value;
                    arrMCs += ((arrMCs == "") ? "" : ",") + hdnChkValue.Value;
                    arrMCAddr += ((arrMCAddr == "") ? "" : ":;:") + hdnAddr.Value + "^" + hdnCity.Value + "^" + hdnState.Value + "^" + hdnzipcode.Value;
                    arrOps += ((arrOps == "") ? "" : ",") + hdnOP.Value;
                }
            }
            string script = string.Format("SetDataAndClose('{0}','{1}','{2}','{3}');", arrKeys, arrMCs, arrMCAddr, arrOps);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "jscript", script, true);
        }
    }
}