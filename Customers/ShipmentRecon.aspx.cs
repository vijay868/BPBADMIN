﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.Customers
{
    public partial class ShipmentRecon : PageBase
    {
        private string[] shipperKeys;
        private string[] shipperValues;
        private string[] ShipmentReconCols;
        private ArrayList SRColumns = new ArrayList();
        private string SROrderNoCol;
        private string SRShipperCol;
        private string SRTrackNoCol;

        private string SRShipDtCol;
        protected Int32 intFileSize = 12288000;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                btnSave.Visible = false;
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                SupplierPOModel poModel = new SupplierPOModel();
                foreach (RepeaterItem item in rptrShipmentList.Items)
                {
                    int podtlKey;
                    CheckBox chkSelect = (CheckBox)item.FindControl("chkSelect");
                    if (chkSelect.Checked)
                    {
                        HiddenField hdnBrPoDtl = (HiddenField)item.FindControl("hdnBrPoDtl");
                        HiddenField hdnpkeycustpodtl = (HiddenField)item.FindControl("hdnpkeycustpodtl");
                        podtlKey = Convert.ToInt32(hdnpkeycustpodtl.Value);
                        HiddenField hdnPoMaster = (HiddenField)item.FindControl("hdnPoMaster");
                        TextBox txtTrackingNo = (TextBox)item.FindControl("txtTrackingNo");
                        TextBox txtShipDate = (TextBox)item.FindControl("txtShipDate");
                        DropDownList ddlShipper = (DropDownList)item.FindControl("ddlShipper");

                        SupplierPOModel.CustOrdersRow cr = poModel.CustOrders.NewCustOrdersRow();

                        cr.pkey_br_po_dtl = Convert.ToInt32(hdnBrPoDtl.Value);
                        cr.pkey_cust_po_master = Convert.ToInt32(hdnPoMaster.Value);
                        cr.pkey_cust_po_dtl = podtlKey;
                        cr.tracking_number = txtTrackingNo.Text;
                        cr.carrier_service_name = ddlShipper.SelectedValue;
                        if (cr.carrier_service_name == "-1")
                        {
                            cr.carrier_service_name = "";
                        }
                        else if (!(cr.carrier_service_name.ToLower().IndexOf("usps ") == -1))
                        {
                            cr.tracking_url = string.Format(WebConfiguration.USPSTrackUrl, (cr.Istracking_numberNull() ? "" : cr.tracking_number));
                        }
                        else if (!(cr.carrier_service_name.ToLower().IndexOf("ups ") == -1))
                        {
                            cr.tracking_url = string.Format(WebConfiguration.UPSTrackUrl, (cr.Istracking_numberNull() ? "" : cr.tracking_number));
                        }
                        else if (!(cr.carrier_service_name.ToLower().IndexOf("fedex ") == -1))
                        {
                            cr.tracking_url = string.Format(WebConfiguration.FEDEXTrackUrl, (cr.Istracking_numberNull() ? "" : cr.tracking_number));
                        }
                        if (!(txtShipDate.Text == string.Empty))
                        {
                            cr.orderdate = Convert.ToDateTime(txtShipDate.Text);
                        }
                        poModel.CustOrders.Rows.Add(cr);
                    }
                }
                CustomerService.UpdateShipmentRecon(poModel, ConnString);
                AlertScript("Shipment Data updated successfully.");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void rptrShipmentList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item | e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DropDownList ddlShipper = (DropDownList)e.Item.FindControl("ddlShipper");
                string shipper = null;
                ddlShipper.Items.Clear();

                Int32 iCounter = 0;
                Int32 iSelIndex = 0;
                DataRowView row = (DataRowView)e.Item.DataItem;
                if (SRColumns.Contains(SRShipperCol.ToUpper()))
                {
                    if (row[SRShipperCol] != null)
                    {
                        shipper = Convert.ToString(row[SRShipperCol]);
                    }
                }
                else
                {
                    if (row["carrier_service_name"] != null)
                    {
                        shipper = Convert.ToString(row["carrier_service_name"]);
                    }
                    if (shipper == null | string.IsNullOrEmpty(shipper))
                    {
                        shipper = GetDefaultCarrier(row["Ship_Type"]);
                    }
                }

                ddlShipper.Items.Add(new ListItem("Shipper", "-1"));

                for (iCounter = 0; iCounter < shipperValues.Length; iCounter++)
                {
                    ddlShipper.Items.Add(new ListItem(shipperValues[iCounter], shipperKeys[iCounter]));
                    if (shipper == shipperKeys[iCounter])
                    {
                        iSelIndex = iCounter + 1;
                    }
                }
                ddlShipper.SelectedIndex = iSelIndex;
                CheckBox chkSelect = (CheckBox)e.Item.FindControl("chkSelect");
                HiddenField hdnpkeycustpodtl = (HiddenField)e.Item.FindControl("hdnpkeycustpodtl");
                int podtlKey = Convert.ToInt32(hdnpkeycustpodtl.Value);
                if (podtlKey > 0)
                {
                    chkSelect.Checked = true;
                    chkSelect.Enabled = true;
                }
                else
                {
                    chkSelect.Checked = false;
                    chkSelect.Enabled = false;
                }
            }
        }
        protected string GetDefaultCarrier(object obj)
        {
            if (obj != null)
            {
                string strType = Convert.ToString(obj);
                switch (strType)
                {
                    case "PRICE_ONE":
                        return "UPS Next Day Air";
                    case "PRICE_TWO":
                        return "UPS 2nd Day Air";
                    case "PRICE_SEVEN":
                        return "UPS Ground";
                    case "PRICE_FOURTEEN":
                        return "UPS Ground";
                    case "PRICE_FREE":
                        return "UPS Ground";
                }
            }
            return "";
        }
        protected string GetPOStatus(object status)
        {
            try
            {
                switch (Convert.ToInt32(status))
                {
                    case 10:
                        return "Special Processing Queue";
                    case 9:
                        return "Clipper Ship Void";
                    case 8:
                        return "Cancelled Orders";
                    case 7:
                        return "Unclaimed Orders";
                    case 6:
                        return "Customer Feedback";
                    case 5:
                        return "<span style='color:#266100'>Processed Queue</span>";
                    case 4:
                        return "Shipment Confirmation";
                    case 3:
                        return "Supplier Queue";
                    case 2:
                        return "Print Queue";
                    default:
                        return "Pre Press";
                }
            }
            catch (Exception ex)
            {
                return "<span style='color:red'>Not Valid</span>";
            }
        }

        protected void btnUplad_Click(object sender, EventArgs e)
        {
            try
            {

                if ((uploadExcelFile.PostedFile != null) & uploadExcelFile.PostedFile.ContentLength > 0)
                {
                    if (uploadExcelFile.PostedFile.ContentLength > intFileSize)
                    {
                        AlertScript("The file uploaded is bigger than allowed size (12MB). Try uploading a smaller file.");
                        return;
                    }

                    string extension = Path.GetExtension(uploadExcelFile.PostedFile.FileName).ToLower();
                    if (!(extension == ".xls") & !(extension == ".xlsx"))
                    {
                        AlertScript("The file uploaded is invalid. Please upload an Excel Sheet.");
                        return;
                    }

                    string shipReconDir = ConfigurationSettings.AppSettings["BPBShipReconDirectoy"];
                    if (!Directory.Exists(shipReconDir))
                        Directory.CreateDirectory(shipReconDir);

                    string strFile = null;
                    strFile = string.Format("{0}\\ShipRecon_{1}{2}", shipReconDir, DateTime.Now.Ticks, extension);
                    uploadExcelFile.PostedFile.SaveAs(strFile);

                    shipperKeys = ConfigurationSettings.AppSettings["ShipperKeys"].Split(',');
                    shipperValues = ConfigurationSettings.AppSettings["ShipperVals"].Split(',');
                    ShipmentReconCols = ConfigurationSettings.AppSettings["ShipmentReconColumns"].Split(',');

                    SROrderNoCol = ConfigurationSettings.AppSettings["SROrderColumn"];
                    SRShipperCol = ConfigurationSettings.AppSettings["SRShipperColumn"];
                    SRTrackNoCol = ConfigurationSettings.AppSettings["SRTrackingNumberColumn"];
                    SRShipDtCol = ConfigurationSettings.AppSettings["SRShipmentDateColumn"];

                    foreach (string item in ShipmentReconCols)
                    {
                        SRColumns.Add(item.ToUpper());
                    }

                    ReadExcelData(strFile);


                }
            }
            catch (Exception ex)
            {
                AlertScript(ex.Message);
            }
        }

        private void ReadExcelData(string filePath)
        {

            try
            {
                DataTable dtData = new DataTable();

                string strCommand = string.Empty;
                string strConnect = string.Empty;

                //string strFile = Path.GetFileName(filePath);
                //string strPath = Path.GetDirectoryName(filePath) + "\\";

                string strHeader = string.Empty;
                strHeader = "Yes";

                string strSheetName = "Sheet1$";
                strSheetName = ConfigurationSettings.AppSettings["ShipmentReconSheetName"];

                strCommand = "SELECT * FROM [" + strSheetName + "]";
                strConnect = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties=Excel 12.0;";
                OleDbConnection objConnection = new OleDbConnection(strConnect);
                OleDbCommand objCommand = new OleDbCommand(strCommand, objConnection);
                OleDbDataAdapter objAdapter = new OleDbDataAdapter(objCommand);
                objAdapter.Fill(dtData);

                if (!(SRColumns.Count == dtData.Columns.Count))
                {
                    throw new Exception("Column mismatch. Please verify.");
                }

                foreach (DataColumn dc in dtData.Columns)
                {
                    if (!SRColumns.Contains(dc.ColumnName.ToUpper()))
                    {
                        throw new Exception("Column mismatch. Please verify.");
                    }
                }


                StringBuilder sbFilter = new StringBuilder();
                dtData.Columns.Add(new DataColumn("pkey_cust_po_dtl"));
                dtData.Columns.Add(new DataColumn("pkey_cust_po_master"));
                dtData.Columns.Add(new DataColumn("orderdate"));
                dtData.Columns.Add(new DataColumn("pkey_br_po_dtl"));
                dtData.Columns.Add(new DataColumn("cards_for"));
                dtData.Columns.Add(new DataColumn("customer_name"));
                dtData.Columns.Add(new DataColumn("pkey_customer"));
                dtData.Columns.Add(new DataColumn("fkey_br_supplier"));
                dtData.Columns.Add(new DataColumn("po_dtl_number"));
                dtData.Columns.Add(new DataColumn("IsValid"));
                dtData.Columns.Add(new DataColumn("carrier_service_name"));
                dtData.Columns.Add(new DataColumn("Ship_Type"));
                dtData.Columns.Add(new DataColumn("podtlnumber"));
                dtData.Columns.Add(new DataColumn("podtltrackingnumber"));
                dtData.Columns.Add(new DataColumn("po_ship_date"));
                dtData.Columns.Add(new DataColumn("tracking_number"));
                dtData.Columns.Add(new DataColumn("status_type"));


                sbFilter.Append(" pps_cust_po_dtl.po_dtl_number in (");
                foreach (DataRow dr in dtData.Rows)
                {
                    dr["IsValid"] = "0";
                    dr["pkey_cust_po_dtl"] = -1;
                    dr["pkey_cust_po_master"] = -1;
                    dr["pkey_br_po_dtl"] = -1;

                    dr[SROrderNoCol] = Convert.ToString(dr[SROrderNoCol]).Trim();
                    dr[SRTrackNoCol] = Convert.ToString(dr[SRTrackNoCol]).Trim();

                    sbFilter.AppendFormat("'{0}',", dr[SROrderNoCol]);
                }
                sbFilter.Append("'#-1#')");

                SupplierPOModel poModel = CustomerService.GetOrdersForShipmentRecon(sbFilter.ToString(), ConnString);

                if (poModel.CustOrders.Rows.Count > 0)
                {
                    SupplierPOModel.CustOrdersRow cr = poModel.CustOrders.NewCustOrdersRow();

                    foreach (DataRow dr in dtData.Rows)
                    {
                        DataRow[] crs = null;

                        dr["podtlnumber"] = dr[SROrderNoCol];
                        dr["podtltrackingnumber"] = dr[SRTrackNoCol];
                        if (SRColumns.Contains(SRShipDtCol.ToUpper()))
                        {
                            dr["po_ship_date"] = dr[SRTrackNoCol];
                        }

                        crs = poModel.CustOrders.Select(string.Format("po_dtl_number = '{0}'", dr[SROrderNoCol]));
                        if (crs.Length > 0)
                        {
                            cr = (SupplierPOModel.CustOrdersRow)crs[0];
                            dr["IsValid"] = "1";

                            dr["pkey_cust_po_dtl"] = cr.pkey_cust_po_dtl;
                            dr["pkey_cust_po_master"] = cr.pkey_cust_po_master;

                            dr["orderdate"] = cr.orderdate;
                            dr["pkey_br_po_dtl"] = cr.pkey_br_po_dtl;


                            dr["cards_for"] = cr.cards_for;
                            dr["customer_name"] = cr.customer_name;

                            dr["pkey_customer"] = cr.pkey_customer;
                            dr["fkey_br_supplier"] = cr.fkey_br_supplier;

                            dr["carrier_service_name"] = cr.carrier_service_name;
                            dr["Ship_Type"] = cr.Ship_Type;

                            dr["status_type"] = cr.status_type;
                            dr["tracking_number"] = cr.tracking_number;

                        }
                    }
                }
                else
                {
                    throw new Exception("Data Provided in spread sheet does not match with records in database. Please review.");
                }


                rptrShipmentList.DataSource = dtData;
                rptrShipmentList.DataBind();

                bool isDateExists = SRColumns.Contains(SRShipDtCol.ToUpper());
                foreach (RepeaterItem item in rptrShipmentList.Items)
                {
                    item.FindControl("tdShipDate").Visible = isDateExists;
                }
                thShipDate.Visible = isDateExists;

                btnSave.Visible = true;
                tdShipmentRecon.Visible = true;

                trUpload.Visible = false;
                uploadExcelFile.Visible = false;
                btnUplad.Visible = false;
            }
            catch (Exception exp)
            {
                FileLogger.WriteException(exp);
                AlertScript(exp.Message);
            }
        }
    }
}