﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExportOrders.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Customers.ExportOrders" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
		<form id="Form1" method="post" runat="server">
			<asp:DataGrid id="grdOrders" runat="server" AutoGenerateColumns="True" Visible="False"></asp:DataGrid>
            <asp:DataGrid id="grdExitOrders" runat="server" AutoGenerateColumns="False" Visible="False">
				<Columns>
					<asp:BoundColumn DataField="ec_company_name" HeaderText="Name of Office sold to"></asp:BoundColumn>
					<asp:BoundColumn DataField="state_abbr" HeaderText="State"></asp:BoundColumn>
					<asp:BoundColumn DataField="ec_company_id" HeaderText="Franchise Office Code"></asp:BoundColumn>
					<asp:BoundColumn DataField="price_per_unit" HeaderText="SALES TO USA OFFICES(US$)"></asp:BoundColumn>
					<asp:BoundColumn HeaderText="SALES TO CANADIAN OFFICES(CDN$)" DataField="SalesToCanadianOffice"></asp:BoundColumn>
				</Columns>
			</asp:DataGrid><asp:DataGrid id="grdStockImgPurchasedHistory" runat="server" AutoGenerateColumns="False" Visible="False">
				<Columns>
					<asp:BoundColumn DataField="customer" HeaderText="Customer Name"></asp:BoundColumn>
					<asp:BoundColumn DataField="email_id" HeaderText="EmailID"></asp:BoundColumn>
					<asp:BoundColumn DataField="po_dtl_number" HeaderText="PO Nimber"></asp:BoundColumn>
					<asp:BoundColumn DataField="cards_for" HeaderText="Cards For"></asp:BoundColumn>
					<asp:BoundColumn DataField="kiosk" HeaderText="Kiosk"></asp:BoundColumn>
					<asp:BoundColumn DataField="date_created" HeaderText="Date"></asp:BoundColumn>
					<asp:BoundColumn DataField="stock_image_id" HeaderText="Stock Image ID"></asp:BoundColumn>
					<asp:BoundColumn DataField="stock_site" HeaderText="Stock Site"></asp:BoundColumn>
					<asp:BoundColumn DataField="amount" HeaderText="Amount"></asp:BoundColumn>
					<asp:BoundColumn DataField="download_id" HeaderText="Download ID"></asp:BoundColumn>
				</Columns>
			</asp:DataGrid>
			<asp:DataGrid id="grdRoyaltiesOrders" runat="server" AutoGenerateColumns="False" Visible="False">
				<Columns>
					<asp:BoundColumn DataField="template_name" HeaderText="Card Name"></asp:BoundColumn>
					<asp:BoundColumn DataField="po_date" HeaderText="Date"></asp:BoundColumn>
					<asp:BoundColumn DataField="quantity" HeaderText="Quantity"></asp:BoundColumn>
					<asp:BoundColumn DataField="PostcardSize" HeaderText="Postcard Size"></asp:BoundColumn>
					<asp:BoundColumn DataField="PostcardBack" HeaderText="Postcard Back"></asp:BoundColumn>
					<asp:BoundColumn DataField="netamount" HeaderText="Price"></asp:BoundColumn>
				</Columns>
			</asp:DataGrid><asp:DataGrid id="grdKioskTransactions" runat="server" AutoGenerateColumns="False" Visible="False">
				<Columns>
					<asp:BoundColumn DataField="Details" HeaderText="Detail"></asp:BoundColumn>
					<asp:BoundColumn DataField="ORDERS" HeaderText="Quantity"></asp:BoundColumn>
					<asp:BoundColumn DataField="ProductCost" HeaderText="Product Cost"></asp:BoundColumn>
					<asp:BoundColumn DataField="ShippingCost" HeaderText="Shipping Cost"></asp:BoundColumn>
					<asp:BoundColumn DataField="Discount" HeaderText="Discount"></asp:BoundColumn>
					<asp:BoundColumn DataField="AMOUNT" HeaderText="Total"></asp:BoundColumn>
				</Columns>
			</asp:DataGrid><asp:DataGrid id="grdTransactions" runat="server" AutoGenerateColumns="False" Visible="False">
				<Columns>
					<asp:BoundColumn DataField="po_date" HeaderText="Date"></asp:BoundColumn>
					<asp:BoundColumn DataField="po_number" HeaderText="PO #"></asp:BoundColumn>
					<asp:BoundColumn DataField="so_number" HeaderText="SO #"></asp:BoundColumn>
					<asp:BoundColumn DataField="customer_name" HeaderText="Customer Name"></asp:BoundColumn>
					<asp:BoundColumn DataField="transaction_amount" HeaderText="Transaction Amount"></asp:BoundColumn>
					<asp:BoundColumn DataField="price_per_unit" HeaderText="Printing Cost"></asp:BoundColumn>
					<asp:BoundColumn DataField="ship_cost" HeaderText="Shipping Cost"></asp:BoundColumn>
					<asp:BoundColumn DataField="dm_cost" HeaderText="DM Cost"></asp:BoundColumn>
					<asp:BoundColumn DataField="po_dtl_state_tax" HeaderText="Sales Tax"></asp:BoundColumn>
				</Columns>
			</asp:DataGrid><asp:DataGrid id="grdTotals" runat="server" AutoGenerateColumns="True" Visible="False" HeaderStyle-Height="0px"></asp:DataGrid>
            <asp:DataGrid id="grdCreditSummary" runat="server" AutoGenerateColumns="False" Visible="False">
				<Columns>
					<asp:BoundColumn DataField="email_id" HeaderText="CustomerEmail" ItemStyle-Width="25%"></asp:BoundColumn>
					<asp:BoundColumn DataField="customer_name" HeaderText="Name" ItemStyle-Width="25%"></asp:BoundColumn>
					<asp:BoundColumn DataField="credit_amount" HeaderText="Credit Amount" ItemStyle-Width="15%"></asp:BoundColumn>
					<asp:BoundColumn DataField="availed_amount" HeaderText="Availed Amount" ItemStyle-Width="15%"></asp:BoundColumn>
					<asp:TemplateColumn HeaderText="Balance Amount" ItemStyle-Width="15%">
						<ItemTemplate>
							<%# Convert.ToDecimal(DataBinder.Eval(Container, "DataItem.credit_amount"))- Convert.ToDecimal(DataBinder.Eval(Container, "DataItem.availed_amount")) %>
						</ItemTemplate>
					</asp:TemplateColumn>
				</Columns>
			</asp:DataGrid>
			<asp:DataGrid Visible="False" id="grdLeads" runat="server" AutoGenerateColumns="False">
				<Columns>
					<asp:BoundColumn DataField="date_created" HeaderText="Registered On"></asp:BoundColumn>
					<asp:BoundColumn DataField="first_name" HeaderText="First Name"></asp:BoundColumn>
					<asp:BoundColumn DataField="last_name" HeaderText="Last Name"></asp:BoundColumn>
					<asp:BoundColumn DataField="address1" HeaderText="Address 1"></asp:BoundColumn>
					<asp:BoundColumn DataField="address2" HeaderText="Address 2"></asp:BoundColumn>
					<asp:BoundColumn DataField="city" HeaderText="City"></asp:BoundColumn>
					<asp:BoundColumn DataField="state" HeaderText="State"></asp:BoundColumn>
					<asp:BoundColumn DataField="zipcode" HeaderText="Zip"></asp:BoundColumn>
					<asp:BoundColumn DataField="phone" HeaderText="Phone"></asp:BoundColumn>
					<asp:BoundColumn DataField="email" HeaderText="Email"></asp:BoundColumn>
					<asp:BoundColumn DataField="orderplaced" HeaderText="Order Placed"></asp:BoundColumn>
					<asp:BoundColumn DataField="po_number" HeaderText="PO#"></asp:BoundColumn>
				</Columns>
			</asp:DataGrid>
			<asp:DataGrid Visible="False" id="grdMailCampaign" runat="server" AutoGenerateColumns="False">
				<Columns>
					<asp:BoundColumn DataField="customer_name" HeaderText="Customer Name"></asp:BoundColumn>
					<asp:BoundColumn DataField="email_id" HeaderText="Email Address"></asp:BoundColumn>
					<asp:BoundColumn DataField="phone" HeaderText="Phone"></asp:BoundColumn>
					<asp:TemplateColumn HeaderText="Date Registered" ItemStyle-Width="15%">
						<ItemTemplate>
							<%# Convert.ToDateTime(DataBinder.Eval(Container, "DataItem.date_created")).ToString("MM/dd/yyyy") %>
						</ItemTemplate>
					</asp:TemplateColumn>
				</Columns>
			</asp:DataGrid>
			<asp:DataGrid id="grdCustomersList" Visible="False" runat="server" AutoGenerateColumns="False">
				<Columns>
					<asp:BoundColumn DataField="customer_name" HeaderText="Name"></asp:BoundColumn>
					<asp:BoundColumn DataField="email" HeaderText="Email"></asp:BoundColumn>
					<asp:BoundColumn DataField="phone" HeaderText="Phone"></asp:BoundColumn>
					<asp:BoundColumn DataField="op_name" HeaderText="OP Name"></asp:BoundColumn>
					<asp:BoundColumn DataField="op_phone" HeaderText="OP Phone"></asp:BoundColumn>
					<asp:BoundColumn DataField="product_discount_level" HeaderText="Product Discount Level"></asp:BoundColumn>
					<asp:BoundColumn DataField="shipment_discount_level" HeaderText="Ship Discount Level"></asp:BoundColumn>
				</Columns>
			</asp:DataGrid>
			<asp:DataGrid Visible="False" id="grdPromotions" runat="server" AutoGenerateColumns="False">
				<Columns>
					<asp:BoundColumn DataField="customer_name" HeaderText="Customer Name"></asp:BoundColumn>
					<asp:BoundColumn DataField="po_number" HeaderText="PO Number"></asp:BoundColumn>
					<asp:TemplateColumn HeaderText="Date">
						<ItemTemplate>
							<%# Convert.ToDateTime(DataBinder.Eval(Container, "DataItem.po_date")).ToString("MM/dd/yyyy") %>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:BoundColumn DataField="promo_code" HeaderText="Promo Code"></asp:BoundColumn>
					<asp:BoundColumn DataField="po_net_amount" HeaderText="Actual Price (US$)"></asp:BoundColumn>
					<asp:BoundColumn DataField="net_promo_discount" HeaderText="Discount (US$)"></asp:BoundColumn>
				</Columns>
			</asp:DataGrid>
			<asp:DataGrid id="grdPowerUsersList" Visible="False" runat="server" AutoGenerateColumns="False">
				<Columns>
					<asp:BoundColumn DataField="name" HeaderText="Name"></asp:BoundColumn>
					<asp:BoundColumn DataField="company" HeaderText="Company"></asp:BoundColumn>
					<asp:BoundColumn DataField="street" HeaderText="Street"></asp:BoundColumn>
					<asp:BoundColumn DataField="suite" HeaderText="Suite"></asp:BoundColumn>
					<asp:BoundColumn DataField="city" HeaderText="City"></asp:BoundColumn>
					<asp:BoundColumn DataField="state" HeaderText="State"></asp:BoundColumn>
					<asp:BoundColumn DataField="zip" HeaderText="ZIP"></asp:BoundColumn>
					<asp:BoundColumn DataField="phone" HeaderText="Phone"></asp:BoundColumn>
					<asp:BoundColumn DataField="email" HeaderText="Email"></asp:BoundColumn>
					<asp:BoundColumn DataField="kiosk" HeaderText="Kiosk"></asp:BoundColumn>
					<asp:BoundColumn DataField="total orders" HeaderText="Total Orders"></asp:BoundColumn>
					<asp:BoundColumn DataField="latest order date" HeaderText="Latest Order Date"></asp:BoundColumn>
				</Columns>
			</asp:DataGrid>
		</form>
	</body>
</html>
