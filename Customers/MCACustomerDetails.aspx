﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="MCACustomerDetails.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Customers.MCACustomerDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
        $().ready(function () {
            $("#MCACustomersDetails").validate();
        });
</script>

    <script type="text/javascript">
        function OpenOfficeIDDialog() {
            var ecKey = $("#ContentPlaceHolder1_ddlkiosk").val();
            selectMoverOptions();
            var mckeys = $("#ContentPlaceHolder1_txtMCS").val();
            var url = "Roosterlist.aspx?cboKiosks=" + ecKey + "&mckeys=" + mckeys;

            $.fancybox({
                'width': '80%',
                'height': '80%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': url,
                'type': 'iframe'
             });
            return false;
        }

        function ShowMCADetails(pkey) {
            var tsUrl = (new Date()).getTime();
            var url = "RoosterDetails.aspx?pkey_office_rooster=" + pkey + "&ts=" + tsUrl;

            $.fancybox({
                'width': '40%',
                'height': '80%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': url,
                'type': 'iframe'
            });
            return false;
        }

        function selectMoverOptions() {
            if ($("#ContentPlaceHolder1_ddlAssignedMCs")) {
                var leng = $("#ContentPlaceHolder1_ddlAssignedMCs option").length;
                var values = [];
                $('#ContentPlaceHolder1_ddlAssignedMCs option').each(function () {
                    values.push($(this).attr('value'));
                });
                if (leng > 0) {
                    $("#ContentPlaceHolder1_txtMCS").val(values.join(","));
                } else {
                    $("#ContentPlaceHolder1_txtMCS").val("-1");
                }
            }
        }

        function SetMCData(arrKeys, arrMCs, arrMCAddr, arrOPs) {

            var arrKeys = arrKeys.split(',');
            var arrMCs = arrMCs.split(',');
            var arrMCAddr = arrMCAddr.split(':;:');
            var arrOPs = arrOPs.split(',');
            var arrMCAddr = arrMCAddr[0].split('^');

            var form = document.forms['MCACustomersDetails'];
            var typesSize = $("#ContentPlaceHolder1_ddlAssignedMCs option").length;
            for (j = typesSize - 1; j > -1; j--) {
                form.ContentPlaceHolder1_ddlAssignedMCs.options[j] = null;
            }
            var displayLinks = "";
            for (j = 0; j < arrKeys.length; j++) {
                form.ContentPlaceHolder1_ddlAssignedMCs.options[j] = new Option(arrMCs[j], arrKeys[j]);
                displayLinks = displayLinks + "<a href=\"javascript:;\" class=\"a1b\" onclick=\"javascript:ShowMCADetails(" + arrKeys[j] + ")\">" + arrMCs[j] + "</a>&nbsp;&nbsp;&nbsp;&nbsp;";
                if (j % 5 == 0 && j!=0)
                    displayLinks = displayLinks + "</br>";
            }
            $("#ContentPlaceHolder1_lblOfficeID").html(displayLinks);


            if ($("#ContentPlaceHolder1_txtCity").val() == "" && arrMCAddr.length > 0) {
                $("#ContentPlaceHolder1_txtCity").val(arrMCAddr[1]);
            }
            if ($("#ContentPlaceHolder1_txtStreetsuit").val() == "" && arrMCAddr.length > 0) {
                $("#ContentPlaceHolder1_txtStreetsuit").val(arrMCAddr[0]);
            }
            if ($("#ContentPlaceHolder1_txtZip").val() == "" && arrMCAddr.length > 0) {
                $("#ContentPlaceHolder1_txtZip").val(arrMCAddr[3]);
            }
            var arrName = arrOPs[0].split(" ");
            if ($("#ContentPlaceHolder1_txtOwnerLastName").val() == "" && arrOPs.length > 0) {

                $("#ContentPlaceHolder1_txtOwnerLastName").val(arrName[1]);
            }
            if ($("#ContentPlaceHolder1_txtOwnerFirstName").val() == "" && arrOPs.length > 0) {
                $("#ContentPlaceHolder1_txtOwnerFirstName").val(arrName[0]);
            }

            if ((form.ContentPlaceHolder1_ddlState.value == "-1" || form.ContentPlaceHolder1_ddlState.value == "") && arrMCAddr.length > 0) {
                for (j = 0; j < form.ContentPlaceHolder1_ddlState.options.length; j++) {
                    if (form.ContentPlaceHolder1_ddlState.options[j].text == arrMCAddr[2]) {
                        form.ContentPlaceHolder1_ddlState.options[j].selected = true;
                        break;
                    }
                }
            }
            selectMoverOptions();
        }

        function saveCustomer() {
            if ('<%=HasAccess("MCACustomerList", Customerkey != -2 ? "edit" : "add")%>' == 'False') {
                alert('User does not have access.');
                return false;
            }
            return true;
        }

        function SetRequirePassword()
        {            
            if ('<%=Customerkey%>' != '-2')
            {
                $('#ContentPlaceHolder1_txtConfirmPassword').removeAttr('required');
                $('#ContentPlaceHolder1_txtPassword').removeAttr('required');
                $('#ContentPlaceHolder1_txtOfficeName').removeAttr('required');
            }
        }
    </script>
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Customer Service</li>
            <li><a href="MCACustomerList.aspx">MCA Customers List</a></li>
            <li>MCA Customer Details</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>MCA Customers Details</h2>
        <form id="MCACustomersDetails" name="MCACustomersDetails" runat="server">
            <table cellpadding="0" cellspacing="0" border="0" width="100%" class="accounts-table">
                <thead>
                    <tr>
                        <th colspan="4" width="100%">Customer Information</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="15%">
                            <label>isMCA</label></td>
                        <td width="20%">
                            <asp:CheckBox ID="chkisMCA" runat="server" CssClass="check-box" />
                        </td>
                        <td width="15%">
                            <label>Password* </label>
                        </td>
                        <td width="35%">
                            <asp:TextBox CssClass="text-box" name="txtPassword" ID="txtPassword" runat="server" TextMode="Password" required></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Email*</label></td>
                        <td>
                            <asp:TextBox CssClass="text-box email" name="txtEmail" ID="txtEmail" runat="server" required></asp:TextBox>
                        </td>
                        <td>
                            <label>Confirm Password* </label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtConfirmPassword" ID="txtConfirmPassword" runat="server" TextMode="Password" equalTo="#ContentPlaceHolder1_txtPassword" required></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Confirm Email*</label></td>
                        <td>
                            <asp:TextBox CssClass="text-box email" name="txtConfirmEmail" ID="txtConfirmEmail" equalTo="#ContentPlaceHolder1_txtEmail" runat="server" required></asp:TextBox>
                        </td>
                        <td>
                            <label>Security Question*</label></td>
                        <td>
                            <asp:DropDownList name="ddlSecurityQuestion" ID="ddlSecurityQuestion" CssClass="slect-box" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSecurityQuestion_SelectedIndexChanged" required>
                                <asp:ListItem Value="What is your mother's maiden name?">What is your mother's maiden name?</asp:ListItem>
                                <asp:ListItem Value="What is the name of your favorite childhood friend?">What is the name of your favorite childhood friend?</asp:ListItem>
                                <asp:ListItem Value="What is your first pet's name?">What is your first pet's name?</asp:ListItem>
                                <asp:ListItem Value="In what city or town was your first job?">In what city or town was your first job?</asp:ListItem>
                                <asp:ListItem Value="What town was your father born in?">What town was your father born in?</asp:ListItem>
                                <asp:ListItem Value="What town was your mother born in?">What town was your mother born in?</asp:ListItem>
                                <asp:ListItem Value="CustomQ">- Frame your own question -</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <div style="visibility: hidden; display: none;" runat="server" visible="false" id="trCustomQuestion">                            
                        <td>
                            <label>Custom Question </label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtCustomQuestion" ID="txtCustomQuestion" runat="server" required></asp:TextBox>
                        </td>
                        </div>
                        <div runat="server" visible="true" id="trCustomQuestion1">
                            <td></td>
                            <td></td>
                        </div>
                        <td>
                            <label>Security Answer*</label>
                        </td>
                        <td>
                            <asp:TextBox data-error-type="inline" CssClass="text-box" name="txtSecurityAnswer" ID="txtSecurityAnswer" runat="server" required></asp:TextBox>
                        </td>
                    </tr>
                </tbody>
            </table>

            <table cellpadding="0" cellspacing="0" border="0" width="100%" class="accounts-table">
                <thead>
                    <tr>
                        <th colspan="4" width="100%">Contact Information</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="15%">
                            <label>Kiosk*</label></td>
                        <td width="20%">
                            <asp:DropDownList name="ddlkiosk" ID="ddlkiosk" CssClass="slect-box" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlkiosk_SelectedIndexChanged" required>
                                <asp:ListItem Value=""></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td width="15%">
                            <label>First Name*</label></td>
                        <td width="35%">
                            <asp:TextBox CssClass="text-box" name="txtFirstName" ID="txtFirstName" runat="server" required></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Office ID</label></td>
                        <td>
                            <div id="divMcatext" runat="server" visible="false">
                                <asp:TextBox CssClass="text-box" name="txtOfficeID" ID="txtOfficeID" runat="server"></asp:TextBox>
                                <input type="hidden" runat="server" id="hdnkioskKey" />
                            </div>
                            <div id="divKWMcaOptions" runat="server" visible="false">
                                    <asp:Label ID="lblOfficeID" runat="server"></asp:Label>&nbsp;&nbsp;
                                    <asp:Button ID="btnOfficeID" runat="server" Text="Select Office/MC" CssClass="button rounded" UseSubmitBehavior="false" OnClientClick="OpenOfficeIDDialog(); return false;" />
                            </div>
                        </td>
                        <td>
                            <label>Last Name*</label></td>
                        <td>
                            <asp:TextBox name="txtLastName" ID="txtLastName" runat="server" CssClass="text-box" required></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <div class="row" id="div1" runat="server" style="display: none;">
                            <label for="v2_normal_input">Office / MC <span class="spanrequire">*</span></label>
                            <div>
                                <asp:DropDownList name="ddlAssignedMCs" ID="ddlAssignedMCs" runat="server">
                                    <asp:ListItem Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <input type="hidden" id="txtMCS" runat="server" name="txtMCS" />
                            </div>
                        </div>
                        <div id="divOfficeName" runat="server" visible="false">
                            <td>
                                <label>Office Name*</label></td>
                            <td>
                                <asp:TextBox CssClass="text-box" name="txtOfficeName" ID="txtOfficeName" runat="server" required></asp:TextBox>
                            </td>
                        </div>
                        <td>
                            <label>Phone</label></td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtPhone" ID="txtPhone" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                </tbody>
            </table>

            <table cellpadding="0" cellspacing="0" border="0" width="100%" class="accounts-table">
                <thead>
                    <tr>
                        <th colspan="4" width="100%">Office Address</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="15%">
                            <label>Street / Suite / Apt</label></td>
                        <td width="20%">
                            <asp:TextBox CssClass="text-box" name="txtStreetsuit" ID="txtStreetsuit" runat="server"></asp:TextBox>
                        </td>
                        <td width="15%">
                            <label>State</label></td>
                        <td width="35%">
                            <asp:DropDownList name="ddlState" ID="ddlState" CssClass="slect-box" runat="server">
                                <asp:ListItem Value=""></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>City</label></td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtCity" ID="txtCity" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <label>Zip</label></td>
                        <td>
                            <asp:TextBox CssClass="text-box digits" name="txtZip" ID="txtZip" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                </tbody>
            </table>

            <table cellpadding="0" cellspacing="0" border="0" width="100%" class="accounts-table mbot100">
                <thead>
                    <tr>
                        <th colspan="4" width="100%">Broker / Owner Information</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="15%">
                            <label>First Name</label></td>
                        <td width="20%">
                            <asp:TextBox CssClass="text-box" name="txtOwnerFirstName" ID="txtOwnerFirstName" runat="server"></asp:TextBox>
                        </td>
                        <td width="15%">
                            <label>Discount Level (Product)*</label></td>
                        <td width="35%">
                            <asp:DropDownList name="ddlProdDiscount" ID="ddlProdDiscount" CssClass="slect-box" runat="server" required>
                                <asp:ListItem Value=""></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Last Name</label></td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtOwnerLastName" ID="txtOwnerLastName" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <label>Discount Level (Shipping)*</label></td>
                        <td>
                            <asp:DropDownList name="ddlShpDiscount" ID="ddlShpDiscount" CssClass="slect-box" runat="server" required>
                                <asp:ListItem Value=""></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Phone</label></td>
                        <td>
                            <asp:TextBox CssClass="text-box digits" name="txtOwnerPhone" ID="txtOwnerPhone" runat="server"></asp:TextBox>
                        </td>
                        <td colspan="2" align="right">
                            <asp:Button runat="server" ID="btnSubmit" CssClass="button rounded" Text="Save" OnClientClick="return saveCustomer();" OnClick="btnSubmit_Click"></asp:Button>
                            <asp:Button runat="server" CssClass="button rounded close-btn" ID="btnClose" Text="Close" UseSubmitBehavior="false" OnClick="btnClose_Click"></asp:Button>
                        </td>
                    </tr>
                </tbody>
            </table>
            <!-- table Ends -->
        </form>
        <%--<script lang="javascript" type="text/javascript">
            ShowCustomQuestion();
            //SetKiosk();
        </script>--%>

         <script>
             SetRequirePassword();
        </script>
    </div>
</asp:Content>
