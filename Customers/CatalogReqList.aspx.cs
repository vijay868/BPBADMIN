﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.UI.HtmlControls;
using System.Text;

namespace Bestprintbuy.Admin.Web.Customers
{
    public partial class CatalogReqList : PageBase
    {
        protected int serialNo;
        protected string fromDate = "";
        protected string toDate = "";
        protected int IsSent;
        protected string StrCatSentKeys = "0";
        StringBuilder l_strCSV = new StringBuilder();
        CustomerModel model = new CustomerModel();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                RunScript("var pTitle = 'CatalogRequest'");
                GetDates();
                LoadData();
            }
        }

        private void GetDates()
        {
            try
            {
                fromDate = txtFromDate.Text == "" ? "" : txtFromDate.Text;
                toDate = txtToDate.Text == "" ? "" : txtToDate.Text;
                if (fromDate == "")
                {
                    fromDate = DateTime.Now.Month.ToString() + "/01/" + DateTime.Now.Year.ToString();
                }
                if (toDate == "")
                {
                    if (DateTime.Now.Day < 10)
                        toDate = DateTime.Now.Month.ToString() + "/0" + DateTime.Now.Day.ToString() + "/" + DateTime.Now.Year.ToString();
                    else
                        toDate = DateTime.Now.ToShortDateString();
                }
                txtFromDate.Text = fromDate;
                txtToDate.Text = toDate;
                IsSent = Convert.ToInt32(OptSentVal.Value == "" ? "0" : OptSentVal.Value);
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        private void LoadData()
        {
            try
            {
                model = CustomerService.GetCatalogReq(Convert.ToDateTime(fromDate + " 00:00"), Convert.ToDateTime(toDate + " 23:59"), IsSent, StrCatSentKeys, ConnString);
                if (CatelogReqGrid.Attributes["SortExpression"] != null)
                    model.CatalogReq.DefaultView.Sort = CatelogReqGrid.Attributes["SortExpression"] + " " + CatelogReqGrid.Attributes["SortDirection"];
                lblTotalRecords.Text = string.Format("Total Number of Orders :{0} ", model.CatalogReq.Rows.Count);
                CatelogReqGrid.DataSource = model.CatalogReq.DefaultView;
                CatelogReqGrid.DataBind();

                if (IsSent == 0)
                {
                    CatelogReqGrid.Columns[4].Visible = true;
                    rbtnSenttoCustomer.Checked = false;
                    rbtnReqUnsent.Checked = true;
                }
                else
                {
                    CatelogReqGrid.Columns[4].Visible = false;
                    rbtnReqUnsent.Checked = false;
                    rbtnSenttoCustomer.Checked = true;
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            CatelogReqGrid.PageIndex = 0;
            GetDates();
            LoadData();
        }

        protected int GetRecordCountForPage()
        {
            return GetRecordCountForPageSize(CatelogReqGrid.PageSize, CatelogReqGrid.PageIndex + 1);
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            txtFromDate.Text = DateTime.Now.Month.ToString() + "/01/" + DateTime.Now.Year.ToString();
            if (DateTime.Now.Day < 10)
                txtToDate.Text = DateTime.Now.Month.ToString() + "/0" + DateTime.Now.Day.ToString() + "/" + DateTime.Now.Year.ToString();
            else
                txtToDate.Text = DateTime.Now.ToShortDateString();

            CatelogReqGrid.DataSource = null;
            CatelogReqGrid.DataBind();
            fromDate = txtFromDate.Text;
            toDate = txtToDate.Text;
            IsSent = 0;
            LoadData();
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/DashBoard.aspx");
        }

        protected void CatelogReqGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CatelogReqGrid.PageIndex = e.NewPageIndex;
            GetDates();
            LoadData();
        }

        protected void CatelogReqGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            SortGridView(CatelogReqGrid, e);
            CatelogReqGrid.PageIndex = 0;
            GetDates();
            LoadData();
        }

        private void CsvStrBuild()
        {
            l_strCSV.AppendFormat("\"Company Name\",\" Name\",\" Address\",\"City\",\"State\",\"Zip\",\" Quantity\"{0}", Environment.NewLine);


            foreach (CustomerModel.CatalogReqRow catalogRow in model.CatalogReq)
            {
                l_strCSV.AppendFormat("\"{0}\",", catalogRow.IscompanyNull() ? "" : catalogRow.company);
                l_strCSV.AppendFormat("\"{0}\",", catalogRow.FName + " " + catalogRow.LName);
                l_strCSV.AppendFormat("\"{0}\",", catalogRow.IsstreetNull() ? "" : catalogRow.street);
                l_strCSV.AppendFormat("\"{0}\",", catalogRow.IscityNull() ? "" : catalogRow.city);
                l_strCSV.AppendFormat("\"{0}\",", catalogRow.IsstateNull() ? "" : catalogRow.state);
                l_strCSV.AppendFormat("\"{0}\",", catalogRow.IszipNull() ? "" : catalogRow.zip);
                l_strCSV.AppendFormat("\"{0}\",", catalogRow.IsquantityNull() ? "" : catalogRow.quantity.ToString());
                l_strCSV.AppendFormat(Environment.NewLine);
            }
        }

        protected void btnExportTocsv_Click(object sender, EventArgs e)
        {
            CreateCSVFile();
        }

        private void CreateCSVFile()
        {
            try
            {
                GetDates();
                LoadData();
                string strFileName = string.Format("{0}-RequestCatalog.csv", DateTime.Now.ToString("MM-dd-yyyy"));
                Response.Clear();
                Response.ContentType = "text/csv";
                Response.AppendHeader("content-disposition", "attachment; filename= " + strFileName);
                CsvStrBuild();
                Response.Write(l_strCSV);
                Response.Flush();
                Response.End();
            }
            catch (Exception)
            {
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (GridViewRow row in CatelogReqGrid.Rows)
                {
                    HiddenField hdn = (HiddenField)row.FindControl("StrCatSentKeys");
                    CheckBox chk = (CheckBox)row.FindControl("chkSent");
                    if (chk.Checked)
                    {
                        if (StrCatSentKeys == "0")
                            StrCatSentKeys = "";
                        StrCatSentKeys += StrCatSentKeys + "," + hdn.Value.ToString();
                    }
                }
                //StrCatSentKeys = StrCatSentKeys.TrimEnd(',');
                GetDates();
                LoadData();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }
    }
}