﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using System.Data;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.Customers
{
    public partial class CustomerComments : PageBase
    {
        protected string Otherreasons;
        protected string creditDesc;
        string PONumber;
        Int32 pkey_po_master;
        Int32 pageBindError = 0;
        protected bool ispreviewcanceled = false;
        protected bool rowExits = false;
        protected CommentsModel m_CustProofData = new CommentsModel();
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Request["pkey_cust_po_master"] != null))
            {
                pkey_po_master = Convert.ToInt32(Request["pkey_cust_po_master"]);
            }
            m_CustProofData = CustomerService.GetCustomerComments(pkey_po_master, ConnString);
            InitPageData();
        }

        private void InitPageData()
        {
            try
            {
                if (pageBindError != 1)
                {
                    if (m_CustProofData.CustomerComments.Rows.Count == 1)
                    {
                        rowExits = true;
                    }
                }
            }
            catch (Exception ex)
            {
                pageBindError = 1;
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
            Page.DataBind();
        }

        protected string GetValue(string key)
        {
            try
            {
                if (pageBindError != 3 & rowExits)
                {
                    if (!Convert.IsDBNull(m_CustProofData.CustomerComments.Rows[0][key]))
                    {
                        return Convert.ToString(m_CustProofData.CustomerComments.Rows[0][key]);
                    }
                }
                else
                {
                    return "";
                }


            }
            catch (Exception ex)
            {
                pageBindError = 3;
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
            return "";
        }

        protected string GetImageUrl(object key)
        {
            try
            {
                if (!Convert.IsDBNull(m_CustProofData.CustomerComments.Rows[0][key.ToString()]))
                {
                    string mCustomerPortalURL = System.Configuration.ConfigurationManager.AppSettings["BPBCustomerBaseURL"];
                    string imageurl = Convert.ToString(m_CustProofData.CustomerComments.Rows[0][key.ToString()]);
                    if (!string.IsNullOrEmpty(imageurl))
                    {
                        imageurl = mCustomerPortalURL + imageurl.Substring(imageurl.IndexOf("/DesignCenter"));
                        string strUrl = "<a href='{0}' target='_blank' class='a1b'>Download Image</a>";
                        //Dim strOnClick As String = "onclick=""javascript:window.open('{0}', 'Proof', 'width=650px, height=500px, toolbar=0, scrollbars=1');"""
                        //strOnClick = String.Format(strOnClick, imageurl)
                        return string.Format(strUrl, imageurl);
                    }
                    return "N/A";
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
            return "N/A";
        }
    }
}