﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustServiceNotes.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Customers.CustServiceNotes" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Best Print Buy</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" media="all" href="../css/bestprintbuy_style.css" />    
	<link rel="stylesheet" media="all" href="../css/jquery.fancybox.css" />
	<script src="../Scripts/jquery.js"></script>
	<script src="../Scripts/common.js"></script>
    <script src="../Scripts/ddsmoothmenu.js"></script>
	<script src="../Scripts/plugin/fancybox/jquery.fancybox.js"></script>
    <!--[if lt IE 9]>
			<script src="../Scripts/html5.js"></script>
		<![endif]-->
    <!--[if lte IE 7]> <html class="ie7"> <![endif]-->
    <!--[if IE 8]>     <html class="ie8"> <![endif]-->
    <!--[if IE 9]>     <html class="ie9"> <![endif]-->
    <!--[if IE 10]>     <html class="ie10"> <![endif]-->
    <!--[if !IE]><!-->
    <!--<![endif]-->
    <script>
        function validate()
        { 
            if ($("#txtNote").text() == "")
            {
                alert("Please Enter Notes.");
                return false;
            }
            return true;
        }

        function ShowImgPreview(url) {
            $.fancybox({
                'width': '80%',
                'height': '40%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': url,
                'type': 'iframe'
            });
            return false;
        }
    </script>
</head>

<body id="Body" runat="server">
    <!-- Wrapper starts -->
    <div id="Div1" class="popup-wrapper" runat="server">
        <div class="popup-title">	
            	Customer Service Notes
			</div>
        <!-- Header Ends -->
        <div class="popup-contnt">
            <!-- Menu starts -->
            <form id="frmCustServiceNotes" runat="server">
                <table>
                    <tr>
                        <td width="30%"><label>PO Number</label></td>
                        <td>
                            <asp:Label ID="lblPONumber" runat="server" Font-Size="12px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td><label>Notes</label></td>
                        <td>
                            <asp:TextBox runat="server" ID="txtNote" style="resize:none;" TextMode="MultiLine" Rows="8" Columns="50" required />
                        </td>
                    </tr>
                    <%--<tr>
                        <td>
                            <label>Image1</label></td>
                        <td>
                           <input id="uploadImgFile1" size="30" type="file" name="uploadImgFile1" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Image2</label></td>
                        <td>
                           <input id="uploadImgFile2" size="30" type="file" name="uploadImgFile2" runat="server" />
                        </td>
                    </tr>--%>
                    <tr>
                        <td align="right" colspan="2">
                            <asp:Button runat="server" ID="btnSave" CssClass="button rounded" Text="Save" OnClick="btnSave_Click" OnClientClick="return validate();"></asp:Button>
                            <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClientClick="parent.$.fancybox.close(); return false;"></asp:Button>
                            <asp:HiddenField ID="hdnPkeyCustPoDtl" runat="server" />
                            <asp:HiddenField ID="hdnPkeyCustPoDtlServiceComment" runat="server" />
                        </td>
                    </tr>
                </table>
                <%--<div>
                    <table>
                        <tr>
                            <td style="text-align: left; padding-left: 5px;">
                                <%#PreviousImages()%>
                            </td>
                        </tr>
                    </table>
                </div>--%>
            </form>
        </div>
    </div>
</body>
</html>
