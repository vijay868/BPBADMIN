﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="CustomerCreditDetails.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Customers.CustomerCreditDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        $().ready(function () {
            $("#frmCustomerCreditDetails").validate();
        });
        function deleteCredit() {
            if ('<%=HasAccess("CustomerCreditDetails", "delete")%>' != 'False') {
                return confirm('Do you want to delete this record. Are you sure?');
            }
            else {
                alert('User does not have access.');
                return false;
            }
        }

        function saveCredit() {
            if ('<%=HasAccess("CustomerCreditDetails", "edit")%>' == 'False') {
                alert('User does not have access.');
                return false;
            }
            return true;
        }

        function viewCredit(fkeycustomer) {
            if ('<%=HasAccess("CustomerCreditHistory", "view")%>' == 'False') {
               alert('User does not have access.');
               return false;
           }
           else {
               OpenCreditHistoryDialog();
           }
           return true;
       }

       function OpenCreditHistoryDialog() {
           var fkeycustomer = "<%= hdnFkeyCustomer.Value%>";
           var url = "CustomerCreditHistory.aspx?fkeycustomer=" + fkeycustomer;
           $.fancybox({
               'width': '80%',
               'height': '100%',
               'autoScale': true,
               'transitionIn': 'fade',
               'transitionOut': 'fade',
               'href': url,
               'type': 'iframe'
           });
           return false;
       }

        $(document).ready(function () {
            $("#ContentPlaceHolder1_lblPONumber").text($("#ContentPlaceHolder1_txtPONumber").val());
            $("#ContentPlaceHolder1_lblAmount").text($("#ContentPlaceHolder1_txtAmount").val());
            $("#ContentPlaceHolder1_lblReason").text($("#ContentPlaceHolder1_txtReason").val());

            
            $("#ContentPlaceHolder1_btnDeactivate").click(function () {
                if ($("#ContentPlaceHolder1_txtAmount").hasClass('error')) {
                    return false;
                }
                if (confirm('De-activating the Credit will not allow the customer to avail it. Do you want to de-activate?')) {
                    $.fancybox({
                        'width': '100%',
                        'height': '100%',
                        'autoScale': true,
                        'transitionIn': 'fade',
                        'transitionOut': 'fade',
                        'href': '#CofirmCredit',
                        'type': 'inline'
                    });
                    $("#ContentPlaceHolder1_lblIsActive").text("No").val();
                    $("#ContentPlaceHolder1_hdnIsactive").val("No");
                }
                return false;
            });
            $("#ContentPlaceHolder1_btnConfirmCredit").click(function () {
                if ($("#ContentPlaceHolder1_txtAmount").hasClass('error')) {
                    return false;
                }
                validate();
                var povalue = "<%= povalue %>";
                var isPO = "<%= isPO %>";
                if ($("#ContentPlaceHolder1_txtAmount").val() == "") {
                    alert("Amount is required and must be numeric.");
                    return false;
                }
                if (isPO.toLowerCase() == "true") {
                    povalue = povalue == "" ? "0" : povalue;
                    var creditAmt1 = $("#ContentPlaceHolder1_txtAmount").val() * 1;
                    if ((povalue * 1) < creditAmt1) {
                        alert("The Credit Amount cannot exceed the PO Amount $" + povalue + ".")
                        $("#ContentPlaceHolder1_txtAmount").val(povalue);
                        return false;
                    }
                }
                var PkeyServiceCredit = "<%= hdnPkeyServiceCredit.Value %>";
                if (PkeyServiceCredit != "-2") {
                    var acCreditAmount = '<%= creditamount %>';
                    var creditAmt = $("#ContentPlaceHolder1_txtAmount").val() * 1;
                    var availAmt = $("#ContentPlaceHolder1_lblAvailedAmount").text() * 1;
                    if (creditAmt < availAmt) {
                        alert("The customer has availed a credit of $" + availAmt + ". Amount cannot be less than the availed amount.")
                        $("#ContentPlaceHolder1_txtAmount").val(acCreditAmount);
                        return false;
                    }
                }

                $.fancybox({
                    'width': '100%',
                    'height': '100%',
                    'autoScale': true,
                    'transitionIn': 'fade',
                    'transitionOut': 'fade',
                    'href': '#CofirmCredit',
                    'type': 'inline'
                });
                $("#ContentPlaceHolder1_lblIsActive").text("Yes").val();
                $("#ContentPlaceHolder1_hdnIsactive").val("Yes");
                return false;
            });
       });

        function validate() {
            $("#ContentPlaceHolder1_lblPONumber").text($("#ContentPlaceHolder1_txtPONumber").val());
            $("#ContentPlaceHolder1_lblAmount").text($("#ContentPlaceHolder1_txtAmount").val());
            $("#ContentPlaceHolder1_lblReason").text($("#ContentPlaceHolder1_txtReason").val());
        }
    </script>    
     <style type="text/css">
        #CofirmCredit {
        overflow:hidden;        
        }
    </style>
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Customer Service</li>
            <li><a href="CustomerCreditList.aspx">Customer Credits</a></li>
            <li>Customer Credit Details</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Customer Credit Details</h2>
        <form class="form" runat="server" id="frmCustomerCreditDetails">
            <div class="form_sep_left" style="float: left;">
                <table style="padding-right: 20px;" class="accounts-table">
                    <tr>
                        <td class="a_lft">Customer Name</td>
                        <td style="width: 10px"></td>
                        <td width="70%">
                            <asp:Label ID="lblCustomerName" runat="server" Font-Size="12px" Font-Bold="true"></asp:Label></td>
                    </tr>
                    <tr>
                        <td class="a_lft">Customer Email</td>
                        <td></td>
                        <td class="a_lft">
                            <asp:Label ID="lblCustomerEmail" runat="server" Font-Size="12px" Font-Bold="true"></asp:Label></td>
                    </tr>
                    <tr>
                        <td class="a_lft">Credit Type</td>
                        <td></td>
                        <td class="a_lft">
                            <asp:DropDownList ID="ddlCreditType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td class="a_lft">Credit Type Desc*</td>
                        <td></td>
                        <td class="a_lft">
                            <asp:Label ID="lblCreditTypeDesc" runat="server" Font-Size="12px" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="a_lft">Reason*</td>
                        <td></td>
                        <td class="a_lft">
                            <asp:TextBox name="txtReason" CssClass="required" ID="txtReason" Enabled="false" runat="server" onblur="validate();"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="a_lft">Amount*</td>
                        <td></td>
                        <td class="a_lft">
                            <asp:TextBox ID="txtAmount" runat="server" CssClass="number" required></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="a_lft">Availed Amount*</td>
                        <td></td>
                        <td class="a_lft">
                            <asp:Label ID="lblAvailedAmount" runat="server" Font-Size="12px" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="a_lft">Date*</td>
                        <td></td>
                        <td class="a_lft">
                            <asp:Label ID="lblDate" runat="server" Font-Size="12px" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="a_lft">PO Number</td>
                        <td></td>
                        <td class="a_lft">
                            <asp:TextBox name="txtPONumber" ID="txtPONumber" runat="server" onblur="validate();"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="btn_lst">
                <div class="right" style="padding-right: 52%;">
                    <asp:HiddenField ID="hdnIsactive" runat="server" />
                    <asp:HiddenField ID="hdnFkeyCustomer" runat="server" />
                    <asp:HiddenField ID="hdnPkeyServiceCredit" runat="server" />
                    <asp:Button runat="server" ID="btnClose" Text="Close" UseSubmitBehavior="false" CssClass="button rounded" OnClick="btnClose_Click"></asp:Button>
                    <asp:Button runat="server" ID="btnConfirmCredit" Text="Confirm Credit" Visible="false" CssClass="button rounded"></asp:Button>
                    <asp:Button runat="server" ID="btnCreditHistory" Text="View Credit History" Visible="false" CssClass="button rounded" UseSubmitBehavior="false" OnClientClick="return viewCredit();"></asp:Button>
                    <asp:Button runat="server" ID="btnDeactivate" Text="De-activate Credit" Visible="false" CssClass="button rounded"></asp:Button>
                    <asp:Button runat="server" ID="btnDelete" Text="Delete" UseSubmitBehavior="false" CssClass="button rounded" Visible="false" OnClientClick="return deleteCredit();" OnClick="btnDelete_Click"></asp:Button>
                </div>
            </div>
            <div style="display: none;">
                <div class="accounts-table" id="CofirmCredit" title="Customer Credit Preview">
                    <div class="popup-title">
                        Customer Credit Preview
                    </div>
                    <!-- Header Ends -->
                    <div class="popup-contnt">
                        <table>
                            <tr>
                                <td width="40%">
                                    <label for="lblCustName">Customer Name</label>
                                </td>
                                <td>
                                    <asp:Label ID="lblCustName" runat="server" ></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="lblCustEmail">Customer Email</label>
                                </td>
                                <td>
                                    <asp:Label ID="lblCustEmail" runat="server" ></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="lblCreditType">Credit Type</label>
                                </td>
                                <td>
                                    <asp:Label ID="lblCreditType" runat="server" ></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="lblReason">Reason</label>
                                </td>
                                <td>
                                    <asp:Label ID="lblReason" runat="server" ></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="lblAmount">Amount</label>
                                </td>
                                <td>
                                    <asp:Label ID="lblAmount" runat="server" ></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="lblAvailedAmt">Availed Amount</label>
                                </td>
                                <td>
                                    <asp:Label ID="lblAvailedAmt" runat="server" ></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="lblDt">Date</label>
                                </td>
                                <td>
                                    <asp:Label ID="lblDt" runat="server" ></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="lblPONumber">PO Number</label>
                                </td>
                                <td>
                                    <asp:Label ID="lblPONumber" runat="server" ></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="lblIsActive">Is Active</label>
                                </td>
                                <td>
                                    <asp:Label ID="lblIsActive" runat="server" ></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td class="a_rgt">                                      
                                    <asp:Button runat="server" ID="btnSave" CssClass="button rounded" Text="Save" UseSubmitBehavior="false" OnClientClick="saveCredit();" OnClick="btnSave_Click"></asp:Button>
                                    <asp:Button ID="btnClosepop" runat="server" Text="Close" CssClass="button rounded" OnClientClick="parent.$.fancybox.close(); return false;"/>
                                </td>
                            </tr>
                        </table>
                       
                    </div>
                </div>
            </div>
        </form>
    </div>
</asp:Content>
