﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;

namespace Bestprintbuy.Admin.Web.Customers
{
    public partial class CatelogReqPopup : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                GetData();
            }
        }

        private void GetData()
        {
            try
            {
                int pkeyCatalogReq = Convert.ToInt32(Request.QueryString["pkeyCatalogReq"]);
                CustomerModel model = CustomerService.GetCatalogReqDetail(pkeyCatalogReq, ConnString);
                if (model.CatalogReq.Rows.Count > 0)
                {
                    CustomerModel.CatalogReqRow row = (CustomerModel.CatalogReqRow)model.CatalogReq.Rows[0];
                    lblName.Text = row.IsCustomerNull() ? "" : row.Customer;
                    lblEmail.Text = row.IsfromemailNull() ? "" : row.fromemail;
                    lblCompany.Text = row.IscompanyNull() ? "" : row.company;
                    lblStreet.Text = row.IsstreetNull() ? "" : row.street;
                    lblCity.Text = row.IscityNull() ? "" : row.city;
                    lblState.Text = row.IsstateNull() ? "" : row.state;
                    lblZip.Text = row.IszipNull() ? "" : row.zip;
                    lblCountry.Text = row.IscountryNull() ? "" : row.country;
                    lblNoofAgents.Text = row.IsagentnoNull() ? "" : row.agentno.ToString();
                    lblContactPerson.Text = row.IscontactpersonNull() ? "" : row.contactperson;
                    lblPhone.Text = row.IsphoneNull() ? "" : row.phone;
                    lblCatalogQty.Text = row.IsquantityNull() ? "" : row.quantity.ToString();
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }
    }
}