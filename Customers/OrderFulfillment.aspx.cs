﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Collections;
using System.Collections.Specialized;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Services;
using System.Configuration;

namespace Bestprintbuy.Admin.Web.Customers
{
    public partial class OrderFulfillment : PageBase
    {
        SupplierPOModel poModel = null;
        protected int UnprintedOrdersCount = 0;
        protected string UnPrintedOrderSummary = "";
        protected bool isProcessInitiated = false;
        protected bool isPendingOrdersFilter = true;
        protected bool isDisplayShipper = false;
        protected bool isDisplayCancel = false;
        protected bool isDisplaySendMail = false;
        protected bool isDisplayXsell = true;
        protected string sysMessage = string.Empty;
        int intProcessInitiatedUser = -1;
        public int OrderCount = 0;
        string DesignMode = "";
        protected string referredURL = "";
        int customerKey;
        string email_id;
        string url = "";
        string AuthUser = "";
        bool isReset = false;
        string[] shipperKeys;
        string[] shipperValues;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindProducts();
                BindOrdersFilter();
                ddlOrderFilter.SelectedValue = "1";

                //m_Processor = New ppsol.uip.OrderStatusProcessor
                //If (CInt(Session("FilterOrdersFor")) = 3 Or CInt(Session("FilterOrdersFor")) = 4) And targetaction = "pagenation" Then
                //    poModel = m_Processor.SaveOrderFulfillment(True)
                //Else
                string strSearch = GetSearchString();
                strSearch = GetSearchFilter();
                poModel = SupplierPOService.GetordersList(strSearch, false, Identity.UserPK, ConnString);
                BindProductCategories();
                BindPromos();

            }

            shipperKeys = System.Configuration.ConfigurationManager.AppSettings["ShipperKeys"].ToString().Split(',');
            shipperValues = System.Configuration.ConfigurationManager.AppSettings["ShipperVals"].ToString().Split(',');

            if (FilterOrdersFor == -1 || FilterOrdersFor == 4 || FilterOrdersFor == 3)
                isDisplayShipper = true;
            else
                isDisplayShipper = false;


            if (!IsPostBack)
            {
                BindGrid();
            }

            try
            {
                // 900 - 15 min
                string refreshTime = "900";
                try
                {
                    refreshTime = System.Configuration.ConfigurationManager.AppSettings["RefreshTimeinSeconds"];
                }
                catch { }
                Response.AppendHeader("Refresh", refreshTime);
            }
            catch { }
        }

        private void BindGrid()
        {
            if (poModel == null)
            {
                string strSearch = GetSearchFilter(); //GetSearchString();
                poModel = SupplierPOService.GetordersList(strSearch, false, Identity.UserPK, ConnString);
            }

            isPendingOrdersFilter = (FilterOrdersFor == 1 || FilterOrdersFor == 2 || FilterOrdersFor == 6 || FilterOrdersFor == 7 || FilterOrdersFor == 8 || FilterOrdersFor == 3 || FilterOrdersFor == 4 || FilterOrdersFor == 11) ? true : false;

            if (FilterOrdersFor == -1 || FilterOrdersFor == 4 || FilterOrdersFor == 3)
                isDisplayShipper = true;
            else
                isDisplayShipper = false;

            if (FilterOrdersFor == 1 || FilterOrdersFor == 2)
                isDisplayCancel = true;
            else
                isDisplayCancel = false;

            if (FilterOrdersFor == 1 || FilterOrdersFor == 2 || FilterOrdersFor == 3 || FilterOrdersFor == 4 || FilterOrdersFor == 10 || FilterOrdersFor == 11)
                isDisplaySendMail = true;
            else
                isDisplaySendMail = false;

            if (FilterproductsFor == 2 && (FilterOrdersFor == 1 || FilterOrdersFor == 2 || FilterOrdersFor == 3 || FilterOrdersFor == 4))
                isDisplayXsell = false;
            else
                isDisplayXsell = true;

            btnSendNotification.Visible = false;
            if (FilterOrdersFor == 11 && Identity.IsSuperUser)
                btnSendNotification.Visible = true;
            // COLUMNS - 
            // 0 - s.no, 1 - Name, 2 - PO #, 3 - SO #
            // 4 - Date, 5 - Back, 6 - Product, (7 8 9) - 7 (pdf, invoice, label)
            // 18 - notes, 19 - assets
            if (OrderFulfillmentGrid.Attributes["SortExpression"] != null)
                poModel.CustOrders.DefaultView.Sort = OrderFulfillmentGrid.Attributes["SortExpression"] + " " + OrderFulfillmentGrid.Attributes["SortDirection"];
            OrderFulfillmentGrid.DataSource = poModel.CustOrders.DefaultView;
            //OrderFulfillmentGrid.DataSource = poModel.CustOrders;
            hdnRecordCount.Value = poModel.CustOrders.Rows.Count.ToString();
            OrderFulfillmentGrid.Columns[11].Visible = isDisplayShipper;
            OrderFulfillmentGrid.Columns[12].Visible = isDisplayShipper;
            OrderFulfillmentGrid.Columns[13].Visible = isDisplaySendMail;
            OrderFulfillmentGrid.Columns[14].Visible = false;
            OrderFulfillmentGrid.Columns[15].Visible = false;
            OrderFulfillmentGrid.Columns[16].Visible = false;

            OrderFulfillmentGrid.Columns[5].Visible = !(FilterproductsFor == 2 && (FilterOrdersFor == 1 || FilterOrdersFor == 2 || FilterOrdersFor == 4 || FilterOrdersFor == 3));

            //OrderFulfillmentGrid.Columns[7].Visible = !(FilterproductsFor == 2 && (FilterOrdersFor == 1 || FilterOrdersFor == 2 || FilterOrdersFor == 4 || FilterOrdersFor == 3));

            OrderFulfillmentGrid.Columns[17].Visible = ((FilterOrdersFor == 2 || FilterOrdersFor == 1) && FilterproductsFor == -2);

            //OrderFulfillmentGrid.Columns[3].Visible = !(FilterOrdersFor == 11);
            //OrderFulfillmentGrid.Columns[9].Visible = !(FilterOrdersFor == 11);
            OrderFulfillmentGrid.Columns[10].Visible = !(FilterOrdersFor == 11);
            OrderFulfillmentGrid.Columns[22].Visible = (FilterOrdersFor == 11);
            OrderFulfillmentGrid.Columns[23].Visible = (FilterOrdersFor == 11);
            OrderFulfillmentGrid.Columns[24].Visible = (FilterOrdersFor == 11);


            if (FilterproductsFor == 2 && (FilterOrdersFor == 2 || FilterOrdersFor == 1))
                OrderFulfillmentGrid.Columns[13].HeaderText = "<a href='#' class='whiteHeader' title='Accept For Processing'>AFP</a> <INPUT name='chkSelectAll' onclick=\"javascript:SelectAllDFP(this)\" class=\"chk\" type=checkbox >";
            else if (FilterOrdersFor == 2 || FilterOrdersFor == 1 || FilterOrdersFor == 10)
            {
                OrderFulfillmentGrid.Columns[13].HeaderText = "<a href='#' class='whiteHeader' title='Downloaded For Printing'>DFP</a> <INPUT name='chkSelectAll' onclick=\"javascript:SelectAllDFP(this)\"  class=\"chk\" type=checkbox >";
            }
            else if (FilterOrdersFor == 11)
            {
                OrderFulfillmentGrid.Columns[13].HeaderText = "<a href='#' class='whiteHeader' title=Send Mail'>Send Mail</a> <INPUT name='chkSelectAll' onclick=\"javascript:SelectAllDFP(this)\"  class=\"chk\" type=checkbox >";
            }
            else
            {
                OrderFulfillmentGrid.Columns[13].HeaderText = "<a href='#' class='whiteHeader' title='Send Shipped Mail Confirmation'>Send Mail</a>";
            }

            if (FilterOrdersFor == 3)
                OrderFulfillmentGrid.PageSize = 50;
            else if (FilterOrdersFor == 1 || FilterOrdersFor == 2 || FilterOrdersFor == 4)
                OrderFulfillmentGrid.PageSize = 100;
            else
                OrderFulfillmentGrid.PageSize = 20;

            if (FilterOrdersFor == -1 || FilterOrdersFor == 5 || FilterOrdersFor == 9)
                OrderFulfillmentGrid.Columns[20].Visible = false;
            else
                OrderFulfillmentGrid.Columns[20].Visible = true;

            OrderFulfillmentGrid.Columns[21].Visible = false;
            CheckMailStatus();
            FindUnprintedCount();
            OrderFulfillmentGrid.DataBind();
            OrderCount = poModel.CustOrders.Rows.Count;

            foreach (GridViewRow row in OrderFulfillmentGrid.Rows)
            {
                try
                {
                    if(row.RowType == DataControlRowType.DataRow)
                    {
                        row.Attributes["data-po"] = ((HiddenField)row.FindControl("hdnPO")).Value;
                    }
                    // pdf
                    if (FilterproductsFor == 2 && (FilterOrdersFor == 1 || FilterOrdersFor == 2 || FilterOrdersFor == 4 || FilterOrdersFor == 3))
                    {
                        row.FindControl("pdfAction").Visible = false;
                    }

                    // label
                    if (FilterOrdersFor == 11)
                    {
                        row.FindControl("labelAction").Visible = false;

                    }
                }
                catch(Exception ex) {
                    FileLogger.WriteException(ex);
                }
            }
        }

        private void CheckMailStatus()
        {
            if (FilterOrdersFor == 4 || FilterOrdersFor == 3)
            {
                isProcessInitiated = true;
            }
        }

        private void FindUnprintedCount()
        {
            UnprintedOrdersCount = 0;
            UnPrintedOrderSummary = "";
            int iPRCount = 0;
            foreach (SupplierPOModel.CustPendingOrdersRow pendorderRow in poModel.CustPendingOrders.Rows)
            {
                UnprintedOrdersCount = UnprintedOrdersCount + pendorderRow.ordercount;
                UnPrintedOrderSummary = string.Format("{0}{1}-<b><a href=\"#\" onclick='QuantityAnchorClick(\"{1}\")'>{2}</a></b>{3} ", UnPrintedOrderSummary, pendorderRow.status, pendorderRow.ordercount, (iPRCount < poModel.CustPendingOrders.Rows.Count - 1 ? "," : ""));
                iPRCount = iPRCount + 1;
            }
        }

        private string GetSearchString()
        {
            string searchFor = "-1";
            string searchStatus = "";
            if (FilterOrdersFor > 0)
            {
                searchStatus = string.Format(" pps_cust_po_dtl.status_type = {0} ", FilterOrdersFor);
            }

            if (Session["search_condition"] != null)
            {
                searchFor = (String)Session["search_condition"];
                if (searchStatus != null)
                {
                    searchFor = searchFor + (searchStatus == "" ? "" : " and " + searchStatus);
                }
            }
            else
            {
                searchFor = (searchStatus == "" ? searchFor : searchStatus);
            }

            //Search for products
            if (FilterProductOrdersFor != -1)
            {
                if (searchFor != "-1")
                    searchFor = searchFor + " and " + string.Format("pps_products.fkey_type = {0}", FilterProductOrdersFor);
                else
                    searchFor = string.Format("pps_products.fkey_type = {0}", FilterProductOrdersFor);
            }

            //Search for Promotion
            if (FilterProductPromosFor != -1)
            {
                if (searchFor != "-1")
                    searchFor = searchFor + " and ( isnull(pps_cust_po_dtl.promo_cost_on_product,0) > 0 or isnull(pps_cust_po_dtl.promo_cost_on_shipping,0) > 0 ) and " + string.Format("pps_cust_po_dtl.fkey_promo = {0}", FilterProductPromosFor);
                else

                    searchFor = " ( isnull(pps_cust_po_dtl.promo_cost_on_product,0) > 0 or isnull(pps_cust_po_dtl.promo_cost_on_shipping,0) > 0 ) and " + string.Format("pps_cust_po_dtl.fkey_promo = {0}", FilterProductPromosFor);
            }
            searchFor = GetFilterOrdersForProds(searchFor);

            int adminUserKey = Identity.UserPK;

            Session["SearchCriteriaForOrderFulfilment"] = searchFor;
            return searchFor;
        }

        private string GetFilterOrdersForProds(string searchFor)
        {
            if (FilterproductsForSite > 0)
            {
                if (searchFor != "-1")
                    searchFor = searchFor + string.Format(" and pps_cust_po_master.fkey_site = {0} ", FilterproductsForSite);
                else
                    searchFor = string.Format(" pps_cust_po_master.fkey_site = {0} ", FilterproductsForSite);
            }
            if (FilterproductsFor != null)
            {
                if (FilterproductsFor == -1)
                {
                    if (searchFor != "-1")
                        searchFor = searchFor + " and pps_categories_types.fkey_self <> 786 ";
                    else
                        searchFor = " pps_categories_types.fkey_self <> 786 ";
                }
                else if (FilterproductsFor == 0)
                {
                    //This is modified to display "directmail in upload Designs" in DIRECTMAIL filter for products combo
                    if (searchFor != "-1")
                        searchFor = searchFor + " and pps_cust_po_dtl.is_direct_mail = 1 and isnull(pps_cust_po_dtl.usleadstoken,'') = ''  and pps_categories_types.fkey_self <> 786 ";
                    else
                        searchFor = " pps_cust_po_dtl.is_direct_mail = 1  and isnull(pps_cust_po_dtl.usleadstoken,'') = '' and pps_categories_types.fkey_self <> 786 ";
                }
                else if (FilterproductsFor == -3)
                {
                    //This is modified to display "directmail in upload Designs" in DIRECTMAIL filter for products combo
                    if (searchFor != "-1")
                        searchFor = searchFor + " and pps_cust_po_dtl.is_direct_mail = 1 and isnull(pps_cust_po_dtl.usleadstoken,'') <> '' and pps_categories_types.fkey_self <> 786 ";
                    else
                        searchFor = " pps_cust_po_dtl.is_direct_mail = 1 and isnull(pps_cust_po_dtl.usleadstoken,'') <> '' and pps_categories_types.fkey_self <> 786 ";
                }
                else if (FilterproductsFor == 1)
                {
                    if (searchFor != "-1")
                        searchFor = searchFor + " and isnull(pps_cust_po_dtl.is_direct_mail,0) = 0 and pps_cust_po_dtl.design_mode <> 'UploadDesign' and pps_categories_types.fkey_self <> 786 ";
                    else
                        searchFor = "  isnull(pps_cust_po_dtl.is_direct_mail,0) = 0  and pps_cust_po_dtl.design_mode <> 'UploadDesign' and pps_categories_types.fkey_self <> 786 ";
                }
                else if (FilterproductsFor == -2)
                {
                    //removed  and condition - and isnull(pps_cust_po_dtl.is_direct_mail,0) = 0
                    if (searchFor != "-1")
                        searchFor = searchFor + " and pps_cust_po_dtl.design_mode = 'UploadDesign'   and pps_categories_types.fkey_self <> 786 ";
                    else
                        searchFor = " pps_cust_po_dtl.design_mode = 'UploadDesign'   and pps_categories_types.fkey_self <> 786 ";
                }
                else if (FilterproductsFor == 2)
                {
                    if (searchFor != "-1")
                        searchFor = searchFor + " and pps_categories_types.fkey_self = 786 ";
                    else
                        searchFor = " pps_categories_types.fkey_self = 786 ";
                }
            }
            return searchFor;
        }

        private string GetSearchFilter()
        {
            try
            {
                string searchStatus = "-1";
                if (FilterOrdersFor > 0)
                {
                    searchStatus = string.Format(" pps_cust_po_dtl.status_type = {0} ", FilterOrdersFor);

                }
                if (FilterproductsFor == -1)
                {
                    if (searchStatus != "-1")
                        searchStatus = searchStatus + " and pps_categories_types.fkey_self <> 786 ";
                    else
                        searchStatus = " pps_categories_types.fkey_self <> 786 ";
                }
                else if (FilterproductsFor == 0)
                {
                    //This is modified to display "directmail in upload Designs" in DIRECTMAIL filter for products combo and click apply
                    if (searchStatus != "-1")
                        searchStatus = searchStatus + " and pps_cust_po_dtl.is_direct_mail = 1 and isnull(pps_cust_po_dtl.usleadstoken,'') = '' ";
                    else
                        searchStatus = " pps_cust_po_dtl.is_direct_mail = 1 and isnull(pps_cust_po_dtl.usleadstoken,'') = '' ";
                }
                else if (FilterproductsFor == -3)
                {
                    //This is modified to display "directmail in upload Designs" in DIRECTMAIL filter for products combo and click apply
                    if (searchStatus != "-1")
                        searchStatus = searchStatus + " and pps_cust_po_dtl.is_direct_mail = 1 and isnull(pps_cust_po_dtl.usleadstoken,'') <> '' ";
                    else
                        searchStatus = " pps_cust_po_dtl.is_direct_mail = 1 and isnull(pps_cust_po_dtl.usleadstoken,'') <> '' ";
                }
                else if (FilterproductsFor == 1)
                {
                    if (searchStatus != "-1")
                        searchStatus = searchStatus + " and isnull(pps_cust_po_dtl.is_direct_mail,0) = 0 and pps_cust_po_dtl.design_mode <> 'UploadDesign'";
                    else
                        searchStatus = "  isnull(pps_cust_po_dtl.is_direct_mail,0) = 0 and pps_cust_po_dtl.design_mode <> 'UploadDesign'";
                }
                else if (FilterproductsFor == -2)
                {
                    //This is modified to display "directmail in upload Designs" in DIRECTMAIL filter for products combo and click apply
                    if (searchStatus != "-1")
                        searchStatus = searchStatus + " and pps_cust_po_dtl.design_mode = 'UploadDesign' ";
                    else
                        searchStatus = " pps_cust_po_dtl.design_mode = 'UploadDesign' ";
                }
                else if (FilterproductsFor == 2)
                {
                    if (searchStatus != "-1")
                        searchStatus = searchStatus + " and pps_categories_types.fkey_self = 786 ";
                    else
                        searchStatus = " pps_categories_types.fkey_self = 786 ";
                }
                //}

                if (FilterProductOrdersFor != -1)
                {
                    if (searchStatus == "")
                    {
                        searchStatus = searchStatus + string.Format("pps_products.fkey_type = {0}", FilterProductOrdersFor);
                    }
                    else
                    {
                        if (searchStatus != "-1")
                            searchStatus = searchStatus + " and " + string.Format("pps_products.fkey_type = {0}", FilterProductOrdersFor);
                        else
                            searchStatus = string.Format("pps_products.fkey_type = {0}", FilterProductOrdersFor);
                    }
                }

                //Search for Promotion
                if (FilterProductPromosFor != -1)
                {
                    if (searchStatus != "-1" && searchStatus != "")
                        searchStatus = searchStatus + " and ( isnull(pps_cust_po_dtl.promo_cost_on_product,0) > 0 or isnull(pps_cust_po_dtl.promo_cost_on_shipping,0) > 0 ) and " + string.Format("pps_cust_po_dtl.fkey_promo = {0}", FilterProductPromosFor);
                    else

                        searchStatus = " ( isnull(pps_cust_po_dtl.promo_cost_on_product,0) > 0 or isnull(pps_cust_po_dtl.promo_cost_on_shipping,0) > 0 ) and " + string.Format("pps_cust_po_dtl.fkey_promo = {0}", FilterProductPromosFor);
                }

                // Retrieve the search condition.
                String searchFor = GetSearchCondition();
                // Retrive the from date and to date i.e. the date interval between which the records have to be
                // searched.
                String fromDate = txtFromDate.Text.Trim();
                String toDate = txtToDate.Text.Trim();
                try
                {
                    if (Convert.ToDateTime(fromDate) != null)
                    { }
                    if (Convert.ToDateTime(toDate) != null)
                    { }
                }
                catch
                {
                    txtFromDate.Text = fromDate = "";
                    txtToDate.Text = toDate = "";
                }

                // Form the search string and append last possible time for day i.e. 23:59 so that records 
                // till the last moment are retrieved.
                if (!"".Equals(fromDate) && !"".Equals(toDate))
                {
                    // If search is only on the date interval then code within if condition is executed 
                    // If records have to be searched with a dateinterval and satisfying another condition
                    // then the code within the else part is executed.
                    if ("-1".Equals(searchFor))
                        searchFor = " PPS_CUST_PO_MASTER.PO_DATE BETWEEN '" + fromDate + "' AND '" + toDate + " 23:59' ";
                    else
                        searchFor = searchFor + " AND PPS_CUST_PO_MASTER.PO_DATE BETWEEN '" + fromDate + "' AND '" + toDate + " 23:59' ";
                }

                String fromDate2 = txtSFromDate.Text.Trim();
                String toDate2 = txtSToDate.Text.Trim();

                try
                {
                    if (Convert.ToDateTime(fromDate2) != null)
                    { }
                    if (Convert.ToDateTime(toDate2) != null)
                    { }
                }
                catch
                {
                    txtSFromDate.Text = fromDate2 = "";
                    txtSToDate.Text = toDate2 = "";
                }

                // Form the search string and append last possible time for day i.e. 23:59 so that records 
                // till the last moment are retrieved.
                if (!"".Equals(fromDate2) && !"".Equals(toDate2))
                {
                    // If search is only on the date interval then code within if condition is executed 
                    // If records have to be searched with a dateinterval and satisfying another condition
                    // then the code within the else part is executed.
                    if ("-1".Equals(searchFor))
                        searchFor = " PPS_CUST_PO_DTL.directmailScheduleDt BETWEEN '" + fromDate2 + "' AND '" + toDate2 + " 23:59' ";
                    else
                        searchFor = searchFor + " AND PPS_CUST_PO_DTL.directmailScheduleDt BETWEEN '" + fromDate2 + "' AND '" + toDate2 + " 23:59' ";
                }

                // Put the search condition in session in order to maintain the search condition for 
                // later use untill reset is clicked.
                Session["search_condition"] = searchFor;

                //				if (!"-1".Equals(searchFor) && searchStatus != "")
                //				{
                //					searchFor = searchFor + " and " + searchStatus;
                //				}
                //
                //                if (!"-1".Equals(searchFor) && searchproductStatus != "")
                //				{
                //					searchFor = searchFor + " and " + searchproductStatus;
                //				}

                if (searchStatus != "")
                {
                    if (!"-1".Equals(searchFor))
                    {
                        searchFor = searchFor + " and " + searchStatus;
                    }
                    else
                    {
                        searchFor = searchStatus;
                    }
                }
                Session["SearchCriteriaForOrderFulfilment"] = searchFor;
                return searchFor;
            }
            catch(Exception ex)
            {
                FileLogger.WriteException(ex);
                return "";
            }
        }

        /// <summary>
        /// GetSearchCondition is used to retirve the 
        /// Search critera and pattern fron the request and 
        /// form the named value collection and then
        /// form the Saerch string and return
        /// for eg. "PPS_CUSTOMERS.CUTOMER_ID LIKE 'nagasree%' "
        /// </summary>
        /// <returns>String holding the search condition</returns>
        public String GetSearchCondition()
        {
            //Variable holding the current session object
            System.Web.SessionState.HttpSessionState currsess = System.Web.HttpContext.Current.Session;

            //Variable holding the parameters - values from the request in a named value collection
            NameValueCollection requestCollections = System.Web.HttpContext.Current.Request.Params;

            //Get the Critera , pattern, operator from the request
            String criteria = ddlFilterFor.SelectedValue;
            String roperator = ddlOperator.SelectedValue;
            String pattern = txtPattern.Text.Trim();
            String strFromDate = txtFromDate.Text;
            String strToDate = txtToDate.Text;

            //Check if pattern exists
            if ("".Equals(pattern)) return "-1";
            //if ("".Equals(strFromDate)) return "-1";
            //if ("".Equals(strToDate)) return "-1";

            //Create a new instance of named value collection
            NameValueCollection nvc = new NameValueCollection();

            //Add the Critera , pattern, operator to the nvc
            nvc.Add("criteria", criteria);
            nvc.Add("operator", roperator);
            nvc.Add("pattern", pattern);
            nvc.Add("txtFromDate", strFromDate);
            nvc.Add("txtToDate", strToDate);
            currsess["combovalues"] = nvc;

            //Invoke FormSearchString on validator set the
            //search condition and pattern on to the session and return the condition
            String searchfor = FormSearchString(nvc);
            currsess["search_condition"] = searchfor;
            currsess["pattern_value"] = pattern;
            currsess["txtFromDate"] = strFromDate;
            currsess["txtToDate"] = strToDate;

            return searchfor;
        }

        private String FormSearchString(NameValueCollection nvc)
        {
            String sOperator = nvc["operator"];
            sOperator = sOperator != null ? sOperator.Trim() : sOperator;
            string pattern = nvc["pattern"];
            StringBuilder sb = new StringBuilder();

            if (sOperator.IndexOf("LIKE") < 0)
            {
                pattern = pattern.Replace("'", "''");
                sb.Append(nvc["criteria"]).Append(" ").Append(sOperator).Append(" ").Append("'").Append(pattern).Append("'");
            }
            else if (sOperator != null && (sOperator.ToLower()).IndexOf("in") > 0)
            {
                sb.Append(nvc["criteria"]).Append("  ").Append(sOperator).Append(" (").Append(pattern).Append(")");
            }
            else
            {
                sOperator = sOperator.Replace("LIKE", pattern);
                sOperator = sOperator.Replace("'", "''");
                sb.Append(nvc["criteria"]).Append(" LIKE '").Append(sOperator).Append("'");
            }
            return sb.ToString();
        }

        private void BindOrdersFilter()
        {
            ddlOrderFilter.Items.Clear();
            ddlOrderFilter.Items.Add(new ListItem("All Orders", "-1"));
            ddlOrderFilter.Items.Add(new ListItem("Pre-Press", "1"));
            ddlOrderFilter.Items.Add(new ListItem("Print Queue", "2"));
            ddlOrderFilter.Items.Add(new ListItem("Supplier Queue", "3"));
            ddlOrderFilter.Items.Add(new ListItem("Shipment Confirmation", "4"));
            ddlOrderFilter.Items.Add(new ListItem("Processed Queue", "5"));
            ddlOrderFilter.Items.Add(new ListItem("Customer Feedback", "6"));
            ddlOrderFilter.Items.Add(new ListItem("Unclaimed Orders", "7"));
            ddlOrderFilter.Items.Add(new ListItem("Cancelled Orders", "8"));
            ddlOrderFilter.Items.Add(new ListItem("Clipper Ship Void", "9"));
            ddlOrderFilter.Items.Add(new ListItem("Special Processing Queue", "10"));
            ddlOrderFilter.Items.Add(new ListItem("Customer Proof Queue", "11"));
        }

        private void BindProducts()
        {
            string selValue = "-1";
            if (ddlProducts.Items.Count > 0)
            {
                selValue = ddlProducts.SelectedValue;
            }
            ddlProducts.Items.Clear();
            ddlProducts.Items.Add(new ListItem("All Orders", "-1"));
            ddlProducts.Items.Add(new ListItem("Online designs", "1"));
            if (ddlSite.SelectedValue != "2")
            {
                ddlProducts.Items.Add(new ListItem("Direct Mail", "0"));
                ddlProducts.Items.Add(new ListItem("Direct Mail - US Data Leads", "-3"));
            }
            ddlProducts.Items.Add(new ListItem("Uploaded Orders", "-2"));
            if (ddlSite.SelectedValue != "2")
            {
                ddlProducts.Items.Add(new ListItem("Warehoused Products", "2"));
            }
            if (ddlProducts.Items.FindByValue(selValue) != null)
            {
                ddlProducts.SelectedValue = selValue;
            }
            else
            {
                ddlProducts.SelectedValue = "-1";
            }

            if (poModel != null)
                BindProductCategories();
        }

        private void BindProductCategories()
        {
            if (poModel == null && IsPostBack)
            {
                string strSearch = GetSearchFilter(); //GetSearchString();
                poModel = SupplierPOService.GetordersList(strSearch, false, Identity.UserPK, ConnString);
            }
            DataRow[] DrProductTypes = null;
            if (FilterproductsFor == 1)
            {
                DrProductTypes = poModel.ProductTypes.Select(" fkey_self <> 786 ");
            }
            else if (FilterproductsFor == 2)
            {
                DrProductTypes = poModel.ProductTypes.Select(" fkey_self = 786 ");
            }
            else if (ddlSite.SelectedValue == "2")
            {
                DrProductTypes = poModel.ProductTypes.Select(" pkey_category_type in (243, 631, 844) ");
            }
            else
            {
                DrProductTypes = poModel.ProductTypes.Select();
            }

            if (FilterproductsFor == 0)
            {
                ddlProductCat.Items.Clear();
                ddlProductCat.Items.Add(new ListItem("Post Cards", WebConfiguration.ProductTypeKeys["PostCards"].ToString()));
            }
            else
            {
                ddlProductCat.DataTextField = "ct_name";
                ddlProductCat.DataValueField = "pkey_category_type";
                ddlProductCat.DataSource = DrProductTypes;
                ddlProductCat.DataBind();
                ddlProductCat.Items.Insert(0, new ListItem("All Types", "-1"));
            }
        }

        private void BindPromos()
        {
            if (poModel == null && IsPostBack)
            {
                string strSearch = GetSearchFilter(); //GetSearchString();
                poModel = SupplierPOService.GetordersList(strSearch, false, Identity.UserPK, ConnString);
            }
            DataRow[] drs = null;
            if (FilterProductOrdersFor > 0)
            {
                drs = poModel.ProductPromos.Select("fkey_product_type = " + FilterProductOrdersFor);
            }
            else
            {
                drs = poModel.ProductPromos.Select();
            }
            ddlPromotion.DataTextField = "promo_code";
            ddlPromotion.DataValueField = "pkey_promotion";
            ddlPromotion.DataSource = drs;
            ddlPromotion.DataBind();
            ddlPromotion.Items.Insert(0, new ListItem("None", "-1"));
        }

        protected void ddlOrderFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            OrderFulfillmentGrid.PageIndex = 0;
            BindGrid();
            btnReset.Visible = true;
        }

        protected void ddlPromotion_SelectedIndexChanged(object sender, EventArgs e)
        {
            OrderFulfillmentGrid.PageIndex = 0;
            BindGrid();
            btnReset.Visible = true;
        }

        protected void ddlProducts_SelectedIndexChanged(object sender, EventArgs e)
        {
            OrderFulfillmentGrid.PageIndex = 0;
            ddlProductCat.Items.Clear();
            BindProductCategories();
            BindGrid();
            btnReset.Visible = true;
        }

        protected void ddlSite_SelectedIndexChanged(object sender, EventArgs e)
        {
            OrderFulfillmentGrid.PageIndex = 0;
            ddlProducts.Items.Clear();
            ddlProductCat.Items.Clear();
            BindProducts();
            BindProductCategories();
            BindGrid();
            btnReset.Visible = true;
        }

        protected void ddlProductCat_SelectedIndexChanged(object sender, EventArgs e)
        {
            OrderFulfillmentGrid.PageIndex = 0;
            ddlPromotion.Items.Clear();
            BindPromos();
            BindGrid();
            btnReset.Visible = true;
        }

        protected void btnApply_Click(object sender, EventArgs e)
        {
            OrderFulfillmentGrid.PageIndex = 0;
            BindGrid();
            btnReset.Visible = true;
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            OrderFulfillmentGrid.PageIndex = 0;
            txtFromDate.Text = "";
            txtToDate.Text = "";
            ddlOperator.SelectedIndex = 0;
            ddlFilterFor.SelectedIndex = 0;
            ddlSite.SelectedIndex = -1;
            ddlProducts.SelectedIndex = -1;
            ddlOrderFilter.SelectedIndex = 1;
            ddlProductCat.SelectedIndex = -1;
            ddlPromotion.SelectedIndex = -1;
            txtPattern.Text = "";
            txtSFromDate.Text = "";
            txtSToDate.Text = "";
            BindGrid();
            btnReset.Visible = false;
        }

        public int FilterproductsForSite
        {
            get
            {
                try
                {
                    return Convert.ToInt32(ddlSite.SelectedValue);
                }
                catch { return 1; }
            }
        }

        public int FilterOrdersFor
        {
            get
            {
                try
                {
                    return Convert.ToInt32(ddlOrderFilter.SelectedValue);
                }
                catch { return 1; }
            }
        }

        public int FilterproductsFor
        {
            get
            {
                try
                {
                    return Convert.ToInt32(ddlProducts.SelectedValue);
                }
                catch { return -1; }
            }
        }

        public int FilterProductOrdersFor
        {
            get
            {
                try
                {
                    return Convert.ToInt32(ddlProductCat.SelectedValue);
                }
                catch { return -1; }
            }
        }

        public int FilterProductPromosFor
        {
            get
            {
                try
                {
                    return Convert.ToInt32(ddlPromotion.SelectedValue);
                }
                catch { return -1; }
            }
        }

        protected string GetShipTypeColor(object obj)
        {
            try
            {
                switch (obj.ToString())
                {
                    case "PRICE_ONE": return "Red";
                    case "PRICE_TWO": return "Blue";
                    case "PRICE_SEVEN": return "Black";
                    case "PRICE_FOURTEEN": return "Green";
                    case "PRICE_FREE": return "Green";
                }
            }
            catch { }
            return "Black";
        }

        protected string GetPOMailLink(object orderNumber, object potlKey)
        {
            if (FilterOrdersFor == 5 || FilterOrdersFor == 8)
            {
                string strUrl = "<a href='#' onclick='OpenPOMailLink(\"{0}\")' class='a1b'>{1}</a>";
                string strOnClick = "OrderMailSent.aspx?poDtlKey={0}&timestamp={1}";
                strOnClick = string.Format(strOnClick, potlKey, System.DateTime.Now.Ticks.ToString());
                return string.Format(strUrl, strOnClick, orderNumber);
            }
            else if (FilterOrdersFor == 11)
            {
                string strUrl = "<a href='#' onclick='OpenCustomerProof(\"{0}\")' class='a1b'>{1}</a>";
                string strOnClick = "CustomerProofs.aspx?poDtlKey={0}&timestamp={1}";
                strOnClick = string.Format(strOnClick, potlKey, System.DateTime.Now.Ticks.ToString());
                return string.Format(strUrl, strOnClick, orderNumber);
            }
            else
            {
                return orderNumber.ToString();
            }
        }

        protected string GetPrintShopFloorInvoiceUrl(object poKey, object poDtlKey, object customer, object brpoKey)
        {
            if (!IsDBNull(poKey) && !IsDBNull(poDtlKey))
            {
                string strUrl = "ShopFloorInvoice.aspx?EditKeys=" + Convert.ToString(poKey);
                return strUrl + "&PoDtlKey=" + Convert.ToString(poDtlKey) + "&customerKey=" + Convert.ToString(customer) + "&brpoKey=" + Convert.ToString(brpoKey) + "&timeStamp=" + DateTime.Now.Ticks.ToString();
            }
            return "";
        }

        protected string GetUSDataStatusLink(object cardsfor, string token, object potlKey)
        {
            if (FilterproductsFor == -3)
            {
                string strUrl = "<a href={0} class='a1b'>{1}</a>";
                string strOnClick = "'USLeadsStatus.aspx?poDtlKey={0}&leadstoken={1}&timestamp={2}'";
                strOnClick = string.Format(strOnClick, potlKey, token, System.DateTime.Now.Ticks.ToString());
                return string.Format(strUrl, strOnClick, cardsfor);
            }
            else
            {
                return cardsfor.ToString();
            }
        }

        protected int GetRecordCountForPage()
        {
            return GetRecordCountForPageSize(OrderFulfillmentGrid.PageSize, OrderFulfillmentGrid.PageIndex + 1);
        }

        protected string GetXLPDFSURLLink(object url, object photourl, object logourl, string cardsFor, string designMode = "")
        {
            string strXlLink = "<a href=\"\" onclick=\"javascript:{0}\"><img src=\"../images/excel.gif\" border=0 alt=\"Download Personalised Calendar Excel.\" title=\"Download Personalised Calendar Excel.\"></a>";
            string strURL = "copyExcel('{0}','{1}','{2}')";
            string strXl = "";
            string strPhoto = "";
            string strLogo = "";
            if (cardsFor == "HH")
            {
                return "";
            }
            if (designMode == "UploadDesign")
            {
                string strString = "<a href='' onclick=\"javascript:{0}\" title='Download PDF' class='pdf iframe'></a>";
                return string.Format(strString, string.Format("CopyUploadDesigns('{0}');", Convert.ToString(GetRowIndex())));
            }
            if (cardsFor.ToString() != "CL")
            {
                return GetProofURL(url);
            }

            try
            {
                if ((url != null))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(url).Trim()))
                    {
                        strXl = Convert.ToString(url);
                        strXl = ProdDesignCenterUrlBase + strXl.Substring(strXl.IndexOf("/Users/"));
                    }
                }
            }
            catch
            {
                strXl = "";
            }

            strPhoto = GetHiResImage(photourl);
            strLogo = GetHiResImage(logourl);

            return string.Format(strXlLink, string.Format(strURL, strXl, strPhoto, strLogo));
        }

        protected string GetProofURL(object url)
        {
            string strURLLink = "<a href='{0}?{1}' target='_blank' title='Download PDF' class='pdf'></a>";
            string strURL = "";
            try
            {
                if (url != null && url.ToString() != "")
                {
                    strURL = Convert.ToString(url);
                    strURL = strURL.Replace(WebConfiguration.PDFSavedFolder, WebConfiguration.PDFSavedFolderServer);
                }
            }
            catch
            {
                strURL = "";
            }
            return string.Format(strURLLink, strURL, DateTime.Now.Ticks.ToString());
        }


        protected string GetHiResImage(object ImageSrcUrl)
        {
            string ImageSrc = "";
            try
            {
                if (ImageSrcUrl != null && ImageSrcUrl != "")
                {
                    ImageSrc = Convert.ToString(ImageSrcUrl).Replace("/", "\\");
                    if (ImageSrc.IndexOf("BPBLogoLibrary\\thumbs\\") > -1)
                    {
                        ImageSrc = ImageSrc.Replace("BPBLogoLibrary\\thumbs\\", "BPBLogoLibrary\\PostCards\\Standard\\Lowres\\");
                    }
                    if (ImageSrc.IndexOf("BPBLogoLibrary\\") > -1)
                    {
                        ImageSrc = ImageSrc.Replace("\\Lowres50\\", "\\TIFF\\");
                        ImageSrc = ImageSrc.Replace("\\Lowres\\", "\\TIFF\\");
                        ImageSrc = ImageSrc.Replace(".jpg", ".tif");
                    }
                    ImageSrc = ImageSrc.Replace("_lowresImg", "");
                }
            }
            catch
            {
                ImageSrc = "";
            }
            return ImageSrc.Replace("\\", "/");
        }


        protected string GetPrintInvoiceUrl(object poKey, object poDtlKey, object customer)
        {
            if (!IsDBNull(poKey) && !IsDBNull(poDtlKey))
            {
                string strUrl = "PrintCustInvoice.aspx?EditKeys=" + Convert.ToString(poKey);
                strUrl += "&PoDtlKey=" + Convert.ToString(poDtlKey) + "&customerKey=" + Convert.ToString(customer) + "&brpoKey=" + DateTime.Now.Ticks.ToString();
                return strUrl;
            }
            return "#";
        }

        protected string DisplayCustNotes(object pkey_cust_po_dtl, object pkey_br_po_dtl)
        {
            if (!IsDBNull(pkey_cust_po_dtl))
            {
                string strUrl = "CustOrderNotes.aspx?pkey_cust_po_dtl=" + Convert.ToString(pkey_cust_po_dtl) + "&pkey_br_po_dtl" + Convert.ToString(pkey_br_po_dtl);
                return strUrl;
            }
            return "#";
        }

        protected string GetIsCanceledForOther(object isCancel)
        {
            try
            {
                if (Convert.ToBoolean(isCancel))
                {
                    return "disabled = true;";
                }
            }
            catch { }
            return "";
        }

        protected string GetPrintStatus(bool isPrintStatus, string automated_ship)
        {
            if (FilterOrdersFor == 4 || FilterOrdersFor == 3)
            {
                if (automated_ship.ToUpper() == "S")
                {
                    return "checked";
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return isPrintStatus ? "checked" : "";
            }
        }

        protected string GetNotesImageType(object obj)
        {
            string strNotes = obj.ToString();
            if (strNotes.Trim() == "")
            {
                return "doc1";
            }
            else
            {
                return "doc2";
            }
        }

        protected string GetServiceNotesImageType(object obj)
        {
            string fkeyCustPoDtl = obj.ToString();
            for (int commentsCount = 0; commentsCount < poModel.CustServiceComments.Rows.Count; commentsCount++)
            {
                if (fkeyCustPoDtl == poModel.CustServiceComments.Rows[commentsCount]["FKEY_CUST_PO_DTL"].ToString() && poModel.CustServiceComments.Rows[commentsCount]["FKEY_CUST_PO_DTL"].ToString() != "")
                {
                    return "doc2";
                }
            }
            return "doc1";
        }

        protected string GetStatusDropdown(bool isHeader)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder("");
            if (isHeader)
                sb.AppendFormat("<select id='cboHdrOrderStatus{0}' name='cboHdrOrderStatus{0}' onchange='SelectOrderStatus(this)'>", GetRowIndex());
            else
                sb.AppendFormat("<div class='gridDropdown'><select id='cboOrderStatus{0}' name='cboOrderStatus{0}'>", GetRowIndex());

            sb.AppendFormat("<option value=-1 >{0}</option>", isHeader ? "Move All Orders To" : "Move Order To");

            if (FilterOrdersFor == 1)  // pre press
            {
                sb.Append("<option value=2 >Print Queue</option>");
                sb.Append("<option value=3 >Supplier Queue</option>");
                sb.Append("<option value=6 >Customer Feedback</option>");
                sb.Append("<option value=7 >Unclaimed Orders</option>");
                sb.Append("<option value=8 >Cancelled Orders</option>");
                sb.Append("<option value=10 >Special Processing Queue</option>");
                sb.Append("<option value=11 >Customer Proof Queue</option>");
            }
            else if (FilterOrdersFor == 2) // print queue
            {
                sb.Append("<option value=3 >Supplier Queue</option>");
                sb.Append("<option value=4 >Shipment Confirmation</option>");
                sb.Append("<option value=6 >Customer Feedback</option>");
                sb.Append("<option value=7 >Unclaimed Orders</option>");
                sb.Append("<option value=8 >Cancelled Orders</option>");
                sb.Append("<option value=10 >Special Processing Queue</option>");
                sb.Append("<option value=11 >Customer Proof Queue</option>");
            }
            else if (FilterOrdersFor == 3) //  Supplier Queue
            {
                sb.Append("<option value=2 >Print Queue</option>");
                sb.Append("<option value=6 >Customer Feedback</option>");
                sb.Append("<option value=10 >Special Processing Queue</option>");
                sb.Append("<option value=5 >Processed Queue</option>");
            }
            else if (FilterOrdersFor == 4) //  Mail Conf pending Queue
            {
                sb.Append("<option value=10 >Special Processing Queue</option>");
            }
            else if (FilterOrdersFor == 6 || FilterOrdersFor == 7 || FilterOrdersFor == 8) //  Supplier Queue
            {
                sb.Append("<option value=1 >Pre Press</option>");
                sb.Append("<option value=2 >Print Queue</option>");
            }
            else if (FilterOrdersFor == 10)
            {
                sb.Append("<option value=1 >Pre Press</option>");
                sb.Append("<option value=2 >Print Queue</option>");
                sb.Append("<option value=3 >Supplier Queue</option>");
                sb.Append("<option value=11 >Customer Proof Queue</option>");
            }
            else if (FilterOrdersFor == 11)
            {
                sb.Append("<option value=1 >Pre Press</option>");
                sb.Append("<option value=2 >Print Queue</option>");
                sb.Append("<option value=10 >Special Processing Queue</option>");
            }
            sb.Append("</select>");
            if (!isHeader)
            {
                sb.Append("</div>");
            }

            return sb.ToString();
        }

        protected string GetSupplierDropdown(object obj)
        {
            if (FilterOrdersFor == 3)
                return "";
            int supplierkey = -1;
            try
            {
                supplierkey = Convert.ToInt32(obj);
            }
            catch { }
            System.Text.StringBuilder sb = new System.Text.StringBuilder("");
            sb.AppendFormat("<select name='cboSupplier{0}' class='s1'>", GetRowIndex());
            foreach (SupplierPOModel.SuppliersRow row in poModel.Suppliers)
            {
                sb.AppendFormat("<option value=\"{0}\" {1} >{2}</option>", row.pkey_business_register, row.pkey_business_register == supplierkey ? "selected" : "", row.supplier);
            }
            return sb.ToString();
        }

        protected string GetProofStatusByImage(object obj)
        {
            if (!IsDBNull(obj))
            {
                string strType = Convert.ToString(obj);
                string imageurl = "<img src=\"../images/Admin/{0}\" alt=\"{1}\" title=\"{1}\" border=0>";
                switch (strType)
                {
                    case "AP": return string.Format(imageurl, "approved.png", "Proof is approved by customer and can be printed."); //"USPS Express" //Confirmed by Sanjeev to use UPS Next Day Air if USPS Express on 02/15
                    case "CO": return string.Format(imageurl, "custcomments.png", "Customer has commented on proof and needs review by designers."); //"USPS Priority" //Confirmed by Sanjeev to use UPS 2nd Day Air if USPS Priority on 02/15
                    case "PA": return string.Format(imageurl, "ProofApprovedByAdmin.png", "Proof approved by admin and is pending for customer review.");
                    case "PR": return string.Format(imageurl, "ProofReviewByAdmin.png", "Proof is pending for admin review.");
                }
            }
            return "";
        }

        protected string GetCommetImageType(object obj)
        {
            bool HasComments = Convert.ToBoolean(obj.ToString());
            if (HasComments)
                return "editnotes_green.gif";
            else
                return "editnotes.gif";
        }

        protected string GetHiresProofURL(object url, string cardsFor)
        {
            string strURL = "";
            try
            {
                if (url != null)
                {
                    if (url.ToString() != "")
                    {
                        strURL = Convert.ToString(url);
                        if (cardsFor == "CL")
                        {
                            strURL = strURL.Replace(WebConfiguration.PDFSavedFolder, WebConfiguration.PDFSavedFolderServer);
                        }
                        else
                        {
                            strURL = ProdDesignCenterUrlBase + strURL.Substring(strURL.IndexOf("/Users/"));
                        }
                    }
                }
            }
            catch
            {
            }
            return strURL;
        }

        protected string GetCsvURL(object url, string cardsFor)
        {
            string strURL = "";
            try
            {
                if (url != null)
                {
                    if (url.ToString() != "")
                    {
                        strURL = Convert.ToString(url);
                        if (cardsFor == "CL")
                        {
                            strURL = WebConfiguration.PDFSavedFolderServer + strURL;
                        }
                        else
                        {
                            strURL = ProdDesignCenterUrlBase + strURL.Substring(strURL.IndexOf("/Users/"));
                        }
                    }
                }
            }
            catch
            {
            }
            return strURL;
        }

        protected string GetShipType(object obj)
        {
            string strType = "";
            if (!IsDBNull(obj))
            {
                strType = Convert.ToString(obj);
                switch (strType)
                {
                    case "PRICE_ONE": return "1";
                    case "PRICE_TWO": return "2";
                    case "PRICE_SEVEN": return "7";
                    case "PRICE_FOURTEEN": return "10";
                    case "PRICE_FREE": return "10";
                }
            }
            return strType;
        }

        protected string GetShipper(object obj, object objShipType)
        {
            string shipType = "";
            if (!IsDBNull(obj))
                shipType = Convert.ToString(obj);
            if (shipType == "")
                shipType = GetDefaultCarrier(objShipType);

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.AppendFormat("<div class='gridDropdown'><select name='cboShpperType{0}' class='s1'>", GetRowIndex());
            sb.AppendFormat("<option value=\"-1\" {0} >Shipper</option>", shipType == "" ? "selected" : "");
            string strSTKey = "";
            for (int i = 0; i < shipperValues.Length; i++)
            {
                if (shipType == shipperKeys[i])
                {
                    sb.AppendFormat("<option value=\"{1}\" {0} >{2}</option>", "selected", shipperKeys[i], shipperValues[i]);
                }
                else
                {
                    sb.AppendFormat("<option value=\"{1}\" {0} >{2}</option>", "", shipperKeys[i], shipperValues[i]);
                }
            }
            sb.Append("</select></div>");
            return sb.ToString();
        }

        protected string GetDefaultCarrier(object obj)
        {
            string strType = "";
            if (!IsDBNull(obj))
            {
                strType = Convert.ToString(obj);
                switch (strType)
                {
                    case "PRICE_ONE": return "UPS Next Day Air";
                    case "PRICE_TWO": return "UPS 2nd Day Air";
                    case "PRICE_SEVEN": return "UPS Ground";
                    case "PRICE_FOURTEEN": return "UPS Ground";
                    case "PRICE_FREE": return "UPS Ground";
                }
            }
            return strType;
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/DashBoard.aspx");
        }

        protected void OrderFulfillmentGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            OrderFulfillmentGrid.PageIndex = e.NewPageIndex;
            BindGrid();
        }

        protected void OrderFulfillmentGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            SortGridView(OrderFulfillmentGrid, e);
            OrderFulfillmentGrid.PageIndex = 0;
            BindGrid();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                SaveOrderFulfillment(false);

                ArrayList lst = (ArrayList)Session["CancelledOrders"];
                if (lst.Count > 0)
                {
                    SendCancellationMails(lst);
                }

                lst = (ArrayList)Session["AllowCustomersToModifyOrders"];
                if (lst.Count > 0)
                {
                    SendCustomersToModifyOrdersMails(lst);
                }
                Session.Remove("CancelledOrders");
                Session.Remove("AllowCustomersToModifyOrders");
            }
            catch (Exception ex)
            {
                AlertScript(ex.Message);
            }
            BindGrid();
        }

        protected void btnHold_Click(object sender, EventArgs e)
        {
            try
            {
                isOrderHeldClicked = true;
                SaveOrderFulfillment(false);
            }
            catch (Exception ex)
            {
                AlertScript(ex.Message);
            }
            BindGrid();
        }

        protected void btnSendMail_Click(object sender, EventArgs e)
        {
            try
            {
                SaveOrderFulfillment(true);
                // Open order fullfillment mail popup
                RunScript("OpenOFMail();");
            }
            catch (Exception ex)
            {
                AlertScript(ex.Message);
            }
            BindGrid();
        }

        private bool isOrderHeldClicked = false;
        protected int startIndex = 1;
        private void SaveOrderFulfillment(bool isSendMail)
        {
            HttpRequest request = HttpContext.Current.Request;
            try
            {
                SupplierPOModel poModel = new SupplierPOModel();
                double recCounter = Convert.ToDouble(request["recordCounter"]);

                if (FilterOrdersFor == 3)
                {
                    if (recCounter > 50) startIndex = Convert.ToInt32(Math.Floor((recCounter - 1) / 50) * 50 + 1);
                }
                else if (FilterOrdersFor == 4 || FilterOrdersFor == 1 || FilterOrdersFor == 2)
                {
                    if (recCounter > 100) startIndex = Convert.ToInt32(Math.Floor((recCounter - 1) / 100) * 100 + 1);
                }
                else
                {
                    if (recCounter > 20) startIndex = Convert.ToInt32(Math.Floor((recCounter - 1) / 20) * 20 + 1);
                }

                //Change End

                SupplierPOModel.CustOrdersRow row;
                Session.Remove("CancelledOrders");
                Session.Remove("AllowCustomersToModifyOrders");
                IList lst = new ArrayList();
                IList snedModificationList = new ArrayList();

                for (int iCounter = startIndex; iCounter <= recCounter; iCounter++)
                {
                    row = poModel.CustOrders.NewCustOrdersRow();
                    SetValue(request, row, "pkey_cust_po_dtl", "PODtlKey" + Convert.ToString(iCounter), -2);
                    SetValue(request, row, "pkey_cust_po_master", "POKey" + Convert.ToString(iCounter), -2);

                    SetValue(request, row, "status_type", "cboOrderStatus" + Convert.ToString(iCounter), -1);

                    row.is_order_canceled = row.status_type == 8;
                    row.is_queue_held = row.status_type == 10;
                    //base.SetValue(request,row,"is_order_canceled","chkIsOrderCanceled"+Convert.ToString(iCounter),false);

                    SetValue(request, row, "can_customer_modify_design", "chkCanUserModify" + Convert.ToString(iCounter), false);
                    if (row.can_customer_modify_design) snedModificationList.Add(row.pkey_cust_po_dtl);

                    if (row.is_order_canceled)
                    {
                        lst.Add(row.pkey_cust_po_dtl);
                        SetValue(request, row, "fkey_shipping_key", "-2", -2);
                        SetValue(request, row, "tracking_number", "", "");
                        SetValue(request, row, "carrier_service_name", "", "");
                        SetValue(request, row, "printstatus", "false", false);
                        SetValue(request, row, "is_mail_sent", "false", false);
                        SetValue(request, row, "is_tracking_number_req", "false", false);
                        row.Setis_queue_heldNull();
                    }
                    else
                    {
                        row.is_order_canceled = false;

                        SetValue(request, row, "fkey_shipping_key", "ddcShipCompany" + Convert.ToString(iCounter), -2);

                        SetValue(request, row, "tracking_number", "txtTrackingNumber" + Convert.ToString(iCounter), "");
                        SetValue(request, row, "carrier_service_name", "cboShpperType" + Convert.ToString(iCounter), "");

                        if (row.carrier_service_name == "-1")
                        {
                            row.carrier_service_name = "";
                        }
                        else if (row.carrier_service_name.ToLower().IndexOf("usps ") != -1)
                        {
                            row.tracking_url = String.Format(WebConfiguration.USPSTrackUrl,
                                row.Istracking_numberNull() ? "" : row.tracking_number);
                        }
                        else if (row.carrier_service_name.ToLower().IndexOf("ups ") != -1)
                        {
                            row.tracking_url = String.Format(WebConfiguration.UPSTrackUrl,
                                row.Istracking_numberNull() ? "" : row.tracking_number);
                        }
                        else if (row.carrier_service_name.ToLower().IndexOf("fedex ") != -1)
                        {
                            row.tracking_url = String.Format(WebConfiguration.FEDEXTrackUrl,
                                row.Istracking_numberNull() ? "" : row.tracking_number);
                        }

                        SetValue(request, row, "fkey_br_supplier", "cboSupplier" + Convert.ToString(iCounter), -1);

                        if (row.can_customer_modify_design)
                        {
                            SetValue(request, row, "can_customer_modify_design", "chkCanUserModify" + Convert.ToString(iCounter), false);
                            bool isModify = row.can_customer_modify_design;
                            SetValue(request, row, "printstatus", "false", false);
                            SetValue(request, row, "is_mail_sent", "false", false);
                        }
                        if (isOrderHeldClicked)
                        {
                            if (row.status_type == -1)
                            {
                                row.status_type = 10;
                                row.is_queue_held = true;
                            }
                            else
                            {
                                row.Setis_queue_heldNull();
                            }
                            SetValue(request, row, "printstatus", "false", false);
                            SetValue(request, row, "is_mail_sent", "false", false);
                        }
                        else if (isSendMail)
                        {
                            SetValue(request, row, "is_mail_sent", "chkPrinted" + Convert.ToString(iCounter), false);
                            SetValue(request, row, "printstatus", "true", true);
                            row.Setis_queue_heldNull();
                        }
                        else
                        {
                            row.printstatus = row.status_type == 4;
                            SetValue(request, row, "is_mail_sent", "false", false);
                            row.Setis_queue_heldNull();
                            if (row.status_type == 10)
                            {
                                row.is_queue_held = true;
                            }
                        }
                        SetValue(request, row, "is_tracking_number_req", "chkIsTrackingReq" + Convert.ToString(iCounter), false);
                    }
                    poModel.CustOrders.Rows.Add(row);
                }
                Session["CancelledOrders"] = lst;
                Session["AllowCustomersToModifyOrders"] = snedModificationList;

                SupplierPOService.UpdateCustomerOrderStatus(poModel, ConnString);
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                throw ex;
            }
        }

        /// <summary>
        /// SetValue method is used to set the integer data 
        /// from request to the  data row
        /// </summary>
        /// <param name="request">HttpRequest holding the current request object</param>
        /// <param name="row">DataRow holding the row object</param>
        /// <param name="dbKey">String holding the column name</param>
        /// <param name="requestKey">String holding the request parameter name</param>
        /// <param name="defaultValue">integer holding the default value to be set on to row</param>
        public void SetValue(HttpRequest request, DataRow row, String dbKey, String requestKey, int defaultValue)
        {
            //Invoke setvalue method to set request value on to the data
            SetValue(request, row, dbKey, requestKey, defaultValue, false);
        }

        /// <summary>
        /// SetValue method is used to set the integer data 
        /// from request to the  data row and condtionally 
        /// populates the default data
        /// </summary>
        /// <param name="request">HttpRequest holding the current request object</param>
        /// <param name="row">DataRow holding the row object</param>
        /// <param name="dbKey">String holding the column name</param>
        /// <param name="requestKey">String holding the request parameter name</param>
        /// <param name="defaultValue">Integer holding the default value to be set on to row</param>
        /// <param name="ignoreDefault">boolean value indicating if the default value can be ignored</param>
        public void SetValue(HttpRequest request, DataRow row, String dbKey, String requestKey, int defaultValue, bool ignoreDefault)
        {
            //Check if the value in the request is not null 
            //and not an empty string and set the converted value to the column
            //else set the default value after checking ignoreDefault flag
            if (request[requestKey] != null && !"".Equals(request[requestKey].Trim()))
                row[dbKey] = Convert.ToInt32(request[requestKey].Trim());
            else if (!ignoreDefault)
                row[dbKey] = defaultValue;
        }

        /// <summary>
        /// SetValue method is used to set the double data 
        /// from request to the  data row
        /// </summary>
        /// <param name="request">HttpRequest holding the current request object</param>
        /// <param name="row">DataRow holding the row object</param>
        /// <param name="dbKey">String holding the column name</param>
        /// <param name="requestKey">String holding the request parameter name</param>
        /// <param name="defaultValue">Double holding the default value to be set on to row</param>
        public void SetValue(HttpRequest request, DataRow row, String dbKey, String requestKey, double defaultValue)
        {
            //Invoke setvalue method to set request value on to the data
            SetValue(request, row, dbKey, requestKey, defaultValue, false);
        }

        /// <summary>
        /// SetValue method is used to set the string data 
        /// from request to the  data row
        /// </summary>
        /// <param name="request">HttpRequest holding the current request object</param>
        /// <param name="row">DataRow holding the row object</param>
        /// <param name="dbKey">String holding the column name</param>
        /// <param name="requestKey">String holding the request parameter name</param>
        /// <param name="defaultValue">String holding the default value to be set on to row</param>
        public void SetValue(HttpRequest request, DataRow row, String dbKey, String requestKey, string defaultValue)
        {
            //Invoke setvalue method to set request value on to the data
            SetValue(request, row, dbKey, requestKey, defaultValue, false);
        }

        /// <summary>
        /// SetValue method is used to set the Decimal data 
        /// from request to the  data row
        /// </summary>
        /// <param name="request">HttpRequest holding the current request object</param>
        /// <param name="row">DataRow holding the row object</param>
        /// <param name="dbKey">String holding the column name</param>
        /// <param name="requestKey">String holding the request parameter name</param>
        /// <param name="defaultValue">decimal holding the default value to be set on to row</param>
        public void SetValue(HttpRequest request, DataRow row, String dbKey, String requestKey, decimal defaultValue)
        {
            //Invoke setvalue method to set request value on to the data
            SetValue(request, row, dbKey, requestKey, defaultValue, false);
        }

        /// <summary>
        /// SetValue method is used to set the double data 
        /// from request to the  data row and condtionally 
        /// populates the default data
        /// </summary>
        /// <param name="request">HttpRequest holding the current request object</param>
        /// <param name="row">DataRow holding the row object</param>
        /// <param name="dbKey">String holding the column name</param>
        /// <param name="requestKey">String holding the request parameter name</param>
        /// <param name="defaultValue">Double : holding the default value to be set on to row</param>
        /// <param name="ignoreDefault">boolean value indicating if the default value can be ignored</param>
        public void SetValue(HttpRequest request, DataRow row, String dbKey, String requestKey, double defaultValue, bool ignoreDefault)
        {
            //Check if the value in the request is not null 
            //and not an empty string and set the converted value to the column
            //else set the default value after checking ignoreDefault flag
            if (request[requestKey] != null && !"".Equals(request[requestKey].Trim()))
                row[dbKey] = Convert.ToDouble(request[requestKey]);
            else if (!ignoreDefault)
                row[dbKey] = defaultValue;
        }

        /// <summary>
        /// SetValue method is used to set the string data 
        /// from request to the  data row and condtionally 
        /// populates the default data
        /// </summary>
        /// <param name="request">HttpRequest holding the current request object</param>
        /// <param name="row">DataRow holding the row object</param>
        /// <param name="dbKey">String holding the column name</param>
        /// <param name="requestKey">String holding the request parameter name</param>
        /// <param name="defaultValue">string : holding the default value to be set on to row</param>
        /// <param name="ignoreDefault">boolean value indicating if the default value can be ignored</param>
        public void SetValue(HttpRequest request, DataRow row, String dbKey, String requestKey, string defaultValue, bool ignoreDefault)
        {
            //Check if the value in the request is not null 
            //and not an empty string and set the converted value to the column
            //else set the default value after checking ignoreDefault flag
            if (request[requestKey] != null && !"".Equals(request[requestKey].Trim()))
                row[dbKey] = Convert.ToString(request[requestKey]);
            else if (!ignoreDefault)
                row[dbKey] = defaultValue;
        }

        /// <summary>
        /// SetValue method is used to set the Decimal data 
        /// from request to the  data row and condtionally 
        /// populates the default data
        /// </summary>
        /// <param name="request">HttpRequest holding the current request object</param>
        /// <param name="row">DataRow holding the row object</param>
        /// <param name="dbKey">String holding the column name</param>
        /// <param name="requestKey">String holding the request parameter name</param>
        /// <param name="defaultValue">Decimal : holding the default value to be set on to row</param>
        /// <param name="ignoreDefault">boolean value indicating if the default value can be ignored</param>
        public void SetValue(HttpRequest request, DataRow row, String dbKey, String requestKey, decimal defaultValue, bool ignoreDefault)
        {
            //Check if the value in the request is not null 
            //and not an empty string and set the converted value to the column
            //else set the default value after checking ignoreDefault flag
            if (request[requestKey] != null && !"".Equals(request[requestKey].Trim()))
                row[dbKey] = Convert.ToDecimal(request[requestKey]);
            else if (!ignoreDefault)
                row[dbKey] = defaultValue;
        }

        /// <summary>
        /// SetValue method is used to set the string data 
        /// from request to the  data row 
        /// </summary>
        /// <param name="request">HttpRequest holding the current request object</param>
        /// <param name="row">DataRow holding the row object</param>
        /// <param name="dbKey">String holding the column name</param>
        /// <param name="requestKey">String holding the request parameter name</param>
        public void SetValue(HttpRequest request, DataRow row, String dbKey, String requestKey)
        {
            //Check if the value in the request is not null 
            //and not an empty string and set it on to the column
            if (request[requestKey] != null && !"".Equals(request[requestKey].Trim()))
                row[dbKey] = Convert.ToString(request[requestKey]);
        }

        /// <summary>
        /// Used for setting the boolean value on to the column in
        /// row after checking the value existance in the request
        /// </summary>
        /// <param name="request">HttpRequest holding the current request object</param>
        /// <param name="row">DataRow holding the row object</param>
        /// <param name="dbKey">String holding the column name</param>
        /// <param name="requestKey">String holding the request parameter name</param>
        /// <param name="defaultValue">boolean value holding the default value to be set on to row</param>
        public void SetValue(HttpRequest request, DataRow row, String dbKey, String requestKey, bool defaultValue)
        {
            //Check if the value in the request is not null 
            //and not an empty string and set true to the column
            //else set the default value
            if (request[requestKey] != null && !"".Equals(request[requestKey].Trim()))
                row[dbKey] = true;
            else
                row[dbKey] = defaultValue;
        }

        private void SendCancellationMails(IList keyList)
        {
            EmailProcessor.InitiateCancelMails(WebConfiguration.BPBCustomerCareEmail, keyList, Convert.ToInt32(ddlProductCat.SelectedValue),
                Identity.UserPK, ReadMailContent("OrderCancellation"), ConnString);
        }

        private void SendCustomersToModifyOrdersMails(IList keyList)
        {
            EmailProcessor.InitiateSendOrderModificationMails(WebConfiguration.BPBCustomerCareEmail, keyList, Convert.ToInt32(ddlProductCat.SelectedValue),
                Identity.UserPK, ReadMailContent("OrderModification"), ConnString);
        }

        /// <summary>
        /// removing export orders functionality for Warehouse products
        /// </summary>
        /// <param name="invKey"></param>
        /// <returns></returns>
        protected string CheckInventoryProduct(object invKey)
        {
            try
            {
                if (invKey != null && Convert.ToInt32(invKey) > 0)
                {
                    // warehouse product
                    return "style='Display:none'";
                }
            }
            catch { }
            return "";
        }

        protected void btnSendNotification_Click(object sender, EventArgs e)
        {
            try
            {                
                string po_Dtl_keys = hdnPOKeys.Value.TrimEnd(',');
                CustomerProofModel m_CustProofData = CustomerService.GetPoProofsForNotificationMail(po_Dtl_keys, ConnString);

                if (m_CustProofData.CustomerProofs.Rows.Count > 0)
                {
                    foreach (DataRow dr in m_CustProofData.CustomerProofs)
                    {
                        SendNotificationEmail(dr);                    
                    }
                    AlertScript("Notification Mails Successfully Sent to Customers.");
                }
                else
                {
                    AlertScript("No valid Proofs.");
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        private void SendNotificationEmail(DataRow dr)
        {
            try
            {
                CustomerProofModel.CustomerProofsRow custProofrow = (CustomerProofModel.CustomerProofsRow)dr;
                string subject = null;
                subject = "Regarding Proof " + custProofrow.proof_name;

                string strMessage = null;
                strMessage = ReadMailContent1("ProofNotificationEmail");
                strMessage = string.Format(strMessage, custProofrow.proof_name, custProofrow.CustomerName);
                strMessage = strMessage.Replace("&#123;", "{");
                strMessage = strMessage.Replace("&#125;", "}");

                EmailProcessor.SendMail(WebConfiguration.BPBCustomerCareEmail, custProofrow.EMAIL_ID, strMessage, subject);
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
            }
        }

        private string ReadMailContent1(string strMailFor)
        {
            try
            {
                string strFile = null;
                string strContent = null;
                strFile = WebConfiguration.GetEmailHTMLLoc(strMailFor);
                System.IO.StreamReader file = new System.IO.StreamReader(strFile);
                strContent = file.ReadToEnd();
                file.Close();
                file = null;
                return strContent;
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                return string.Empty;
            }
        }

        [WebMethod]
        public static List<string> GetAutoCompleteData(string input, int field)
        {
            List<string> result = new List<string>();
            string strField = "";
            if (field == 0)
                strField = "EMAIL_ID";
            else if (field == 1)
                strField = "NAME";
            else if (field == 2)
                strField = "PO_NUMBER";
            else if (field == 3)
                strField = "PO_DTL_NUMBER";
            string connstring = ConfigurationManager.AppSettings["BestPrintBuyDBConn"];
            result = UserService.GetAutoSearchInfoList(1, "%" + input + "%", strField, connstring);
            return result;
        }
    }
}
