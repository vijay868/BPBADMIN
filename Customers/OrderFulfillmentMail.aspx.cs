﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Collections;

namespace Bestprintbuy.Admin.Web.Customers
{
    public partial class OrderFulfillmentMail : PageBase
    {
        SupplierPOModel poModel = null;
        protected bool isAdminUser = true;
        protected string OrderKeys = string.Empty;
        protected bool isProcessInitiated = false;
        private Int32 intProcessInitiatedUser = -1;
        protected int productTypeKey = -1;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                /*if ((Request["OrderKeys"] != null))
                {
                    OrderKeys = Request["OrderKeys"];
                }*/
                // TODO : Warehouse fullfillment
                //isAdminUser = (Identity.isWareHouseUser ? false : true);

                //if (!isAdminUser)
                //{
                //    productTypeKey = Convert.ToInt32(WebConfiguration.ProductTypeKeys["HeroDocumentFolder"]);
                //}
                //if ((Request["cboProdType"] != null))
                //{
                //    productTypeKey = Convert.ToInt32(Request["cboProdType"]);
                //}
                BindGrid();
                CheckMailStatus();
                BindProducts();
            }
        }

        private void CheckMailStatus()
        {
            if (poModel.MailStatus.Rows.Count > 0)
            {
                isProcessInitiated = true;
            }
        }

        private void BindGrid()
        {
            try
            {
                //If CInt(Session("FilterOrdersFor")) = -2 And targetaction = "pagenation" Then
                //       poModel = m_Processor.SaveOrderFulfillmentMail(adminUserKey, productTypeKey)
                //   Else
                //       poModel = m_Processor.GetMailOrders(adminUserKey, productTypeKey)
                //   End If

                if (ddlProduct.Items.Count > 0)
                {
                    productTypeKey = Convert.ToInt32(ddlProduct.SelectedValue);
                }

                poModel = SupplierPOService.GetMailOrders(Identity.UserPK, productTypeKey, ConnString);
                rptrOrderFulfillmentList.DataSource = poModel.CustOrders;
                rptrOrderFulfillmentList.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        private void BindProducts()
        {
            try
            {
                int masterkey = 5;
                poModel = SupplierPOService.UpdateProductTypes(masterkey, ConnString);

                if (poModel.ProductTypes.Rows.Count > 0)
                {
                    DataRow dr = poModel.ProductTypes.NewRow();
                    dr["ct_name"] = "Direct Mail";
                    dr["pkey_category_type"] = "-244";
                    poModel.ProductTypes.Rows.Add(dr);
                }

                ddlProduct.DataSource = poModel.ProductTypes;
                ddlProduct.DataTextField = "ct_name";
                ddlProduct.DataValueField = "pkey_category_type";
                ddlProduct.DataBind();
                ddlProduct.Items.Insert(0, new ListItem("All Types", "-1"));
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        private bool SendEmails(SupplierPOModel data, int productTypeKey)
        {
            try
            {
                ArrayList SendMailkeys = new ArrayList();
                foreach (RepeaterItem item in rptrOrderFulfillmentList.Items)
                {
                    CheckBox chk = (CheckBox)item.FindControl("chkSelect");
                    if (chk.Checked)
                    {
                        HiddenField hdnDetailKey = (HiddenField)item.FindControl("PODtlKey");
                        SendMailkeys.Add(Convert.ToInt32(hdnDetailKey.Value));
                    }
                }

                if (SendMailkeys.Count == 0)
                {
                    AlertScript("Please select a product to sendMail.");
                    return false;
                }

                if (data.MailStatus.Rows.Count > 0)
                {
                    intProcessInitiatedUser = Convert.ToInt32(data.MailStatus.Rows[0]["fkey_user"]);

                    if (Identity.UserPK == intProcessInitiatedUser)
                    {

                        EmailProcessor.InitiateMails(WebConfiguration.BPBCustomerCareEmail, SendMailkeys, Convert.ToInt32(ddlProduct.SelectedValue),
                Identity.UserPK, ReadMailContent(), ConnString);

                        /*OrderFulfilmentMails mailObject = new OrderFulfilmentMails();
                        mailObject.AdminUser = Identity.UserPK;
                        mailObject.ProductType = productTypeKey;
                        mailObject.FromMailId = WebConfiguration.BPBCustomerCareEmail;
                        if ((SendMailkeys != null))
                        {
                            mailObject.PODetailKeys = SendMailkeys;
                        }
                        mailObject.OrderMailContent = ReadMailContent();
                        Thread sendMail = new Thread(new ThreadStart(mailObject.InitiateMails));
                        sendMail.Start();*/
                    }
                    else
                    {
                        AlertScript("The Send Email Process has already been initiated by another user.");
                        return false;
                    }
                }
                else
                {
                    AlertScript("Unable to initate the send email process please try again.");
                    return false;
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript("Unable to initate the send email process please try again." + Environment.NewLine + ex.Message);
                return false;
            }
            return true;
        }

        private string ReadMailContent()
        {
            try
            {
                string strFile = null;
                string strContent = null;
                strFile = WebConfiguration.GetEmailHTMLLoc("OrderFulFillment");
                System.IO.StreamReader file = new System.IO.StreamReader(strFile);
                strContent = file.ReadToEnd();
                file.Close();
                file = null;
                return strContent;
            }
            catch (System.IO.FileNotFoundException fnfEx)
            {
                FileLogger.WriteException(fnfEx);
                return string.Empty;
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                return string.Empty;
            }
        }

        protected void btnSendMail_Click(object sender, EventArgs e)
        {
            try
            {
                poModel = SupplierPOService.GetordersList(Session["SearchCriteriaForOrderFulfilment"].ToString(), true, Identity.UserPK, ConnString);
                if (SendEmails(poModel, productTypeKey))
                {
                    RunScript("MailsSent();");
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void ddlProduct_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
        }
    }
}