﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;

namespace Bestprintbuy.Admin.Web.Customers
{
    public partial class PrintCustInvoice : PageBase
    {
        protected decimal m_cartCost = 0;
        protected int nonFreeproducts = 0;
        protected string PoDtlKey = "";
        protected bool rowExits = false;
        protected bool CartShipInfo = false;
        protected bool CartBillInfo = false;

        protected string EditKeys = "";
        protected string poNumber = "";
        protected string m_timeStamp = "";
        protected int customerKey = 0;
        protected bool isNotDM = false;
        protected string promotioncode = "";
        protected decimal m_customizationCost = 0;
        protected CartModel m_CartData = new CartModel();
        protected string StateCountry = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                int qtyVariantKey = Convert.ToInt32(WebConfiguration.ProductVariants["Quantity"]);
                int paperVariantKey = Convert.ToInt32(WebConfiguration.ProductVariants["PaperType"]);
                int customerPOKey = Convert.ToInt32(Request.QueryString["EditKeys"]);
                customerKey = Convert.ToInt32(Request.QueryString["customerKey"]);

                //try
                //{
                //    FileLogger.WriteEntry(string.Format("qtyVariantKey:{0}, paperVariantKey:{1}, customerPOKey:{2}, customerkey:{3}",
                //        qtyVariantKey.ToString(), paperVariantKey.ToString(), customerPOKey.ToString(), customerKey.ToString()));
                //}
                //catch
                //{ }

                m_CartData = CartService.GetPrintInvoiceData(customerPOKey, customerKey, qtyVariantKey, paperVariantKey, ConnString);

                if (m_CartData.CartShipInfo.Rows.Count > 0)
                {
                    CartModel.CartShipInfoRow cartShiprow = (CartModel.CartShipInfoRow)m_CartData.CartShipInfo.Rows[0];
                    lblShipName.Text = cartShiprow.ship_first_name + " " + cartShiprow.ship_last_name;
                    lblShipName2.Text = cartShiprow.ship_first_name + " " + cartShiprow.ship_last_name;
                    lblShipCompanyName.Text = cartShiprow.ship_company_name;
                    lblShipCompanyName2.Text = cartShiprow.ship_company_name;
                    lblShipAddress1.Text = cartShiprow.shipment_address1;
                    lblShipAddress2.Text = cartShiprow.shipment_address2;
                    lblShipAddress21.Text = cartShiprow.shipment_address1;
                    lblShipAddress22.Text = cartShiprow.shipment_address2;
                    lblShipCityZip.Text = cartShiprow.city + " " + cartShiprow.zipcode;
                    lblShipCityZip2.Text = cartShiprow.city + " " + cartShiprow.zipcode;
                    lblShipStateCountry.Text = GetState(cartShiprow.fkey_state, cartShiprow.fkey_country);
                    lblShipStateCountry2.Text = GetState(cartShiprow.fkey_state, cartShiprow.fkey_country);
                }
                if (m_CartData.CartBillingInfo.Rows.Count > 0)
                {
                    CartModel.CartBillingInfoRow cartBillrow = (CartModel.CartBillingInfoRow)m_CartData.CartBillingInfo.Rows[0];
                    lblBillName.Text = cartBillrow.bill_first_name + " " + cartBillrow.bill_last_name;
                    lblBillCompanyName.Text = cartBillrow.bill_company_name;
                    lblBillAddress1.Text = cartBillrow.bill_address1;
                    lblBillAddress2.Text = cartBillrow.bill_address2;
                    lblBillCityZip.Text = cartBillrow.bill_city + " " + cartBillrow.bill_zipcode;
                    lblBillStateCountry.Text = GetState(cartBillrow.fkey_bill_state.ToString(), cartBillrow.fkey_bill_country.ToString());
                }
                if (m_CartData.CustomerCart.Rows.Count > 0)
                {
                    CartModel.CustomerCartRow custCartrow = (CartModel.CustomerCartRow)m_CartData.CustomerCart.Rows[0];
                    lblPONumber.Text = custCartrow.po_number;
                    lblPOdate.Text = custCartrow.po_date.ToShortDateString();
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteEntry("PrintCustInvoiceerror:" + ex.Message);
                FileLogger.WriteException(ex);
            }
        }

        private string GetState(string fkeyState, string fkeyCountry)
        {
            try
            {
                if (m_CartData.States.Rows.Count > 0)
                {
                    StateCountry = "";
                    var list = (from collect in m_CartData.States
                                where collect.pkey_state == Convert.ToInt32(fkeyState)
                                select collect).ToList();
                    StateCountry += list[0].state_name;
                }
                if (m_CartData.Countries.Rows.Count > 0)
                {
                    StateCountry = StateCountry + " ";
                    var list = (from collect in m_CartData.Countries
                                where collect.pkey_country == Convert.ToInt32(fkeyCountry)
                                select collect).ToList();
                    StateCountry += list[0].country_name;
                    lblShipStateCountry.Text = StateCountry;
                }
                return StateCountry;
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
            return "";
        }

        protected string writeSelectedProducts()
        {
            try
            {
                int l_iCounter;
                string strText = "";
                m_cartCost = 0;
                decimal productCost = 0;
                decimal processCost = 0;
                decimal shipCost = 0;
                decimal shipPromoCost = 0;
                decimal productPromoCost = 0;
                bool IsUsLeads = false;
                bool isFreeProdcut;
                decimal freeShipCost = 0;
                string shipOption = "";
                decimal StateTaxOn = 0;
                decimal TaxAmount = 0;
                decimal dmShipCost = 0;
                decimal totalProcessCost = 0;

                string strClass = "bodytext";
                bool Is_OnlyDirectMail = true;

                decimal customerCreditAmt = 0;

                decimal xsellShipDiscount = 0;
                decimal xsellProductDiscount = 0;

                decimal revCustomizationCost = 0;
                decimal totalRevCustomizationCost = 0;

                string mTurnaroundTime = "0";
                decimal mTurnAroundCost = 0;

                string BackText = string.Empty;
                string PCSize = string.Empty;

                decimal mcashipDiscount = 0;
                decimal mcaproductDiscount = 0;
                decimal multiline_ShipDiscount = 0;
                CartModel.CustomerCartRow custCartrow = (CartModel.CustomerCartRow)m_CartData.CustomerCart.Rows[0];
                CartModel.CartBillingInfoRow cartBillrow = (CartModel.CartBillingInfoRow)m_CartData.CartBillingInfo.Rows[0];

                if (!custCartrow.Isservice_credit_discountNull())
                {
                    customerCreditAmt = custCartrow.service_credit_discount;
                }
                if (!custCartrow.Iscustomization_costsNull())
                {
                    m_customizationCost = custCartrow.customization_costs; ;
                }
                if (!custCartrow.IsturnaroundcostNull())
                {
                    mTurnAroundCost = custCartrow.turnaroundcost;
                }
                if (!custCartrow.IsturnarounddaysNull())
                {
                    mTurnaroundTime = custCartrow.turnarounddays.ToString();
                }

                for (l_iCounter = 0; l_iCounter < m_CartData.CartDetails.Rows.Count; l_iCounter++)
                {
                    CartModel.CartDetailsRow cartDetailrow = (CartModel.CartDetailsRow)m_CartData.CartDetails.Rows[l_iCounter];
                    isFreeProdcut = cartDetailrow.is_free; //Convert.ToBoolean(GetValue("is_free", l_iCounter));
                    decimal xsellShipDiscount1 = 0;
                    decimal xsellProductDiscount1 = 0;
                    decimal ml_ShipDiscount = 0;
                    xsellProductDiscount1 = cartDetailrow.Isxsell_product_discountNull() ? 0 : cartDetailrow.xsell_product_discount;   //Convert.ToDecimal(IIf(GetValue("xsell_product_discount", l_iCounter) = "", 0.0, GetValue("xsell_product_discount", l_iCounter)));
                    xsellShipDiscount1 = cartDetailrow.Isxsell_shipment_discountNull() ? 0 : cartDetailrow.xsell_shipment_discount;   // Convert.ToDecimal(IIf(GetValue("xsell_shipment_discount", l_iCounter) = "", 0.0, GetValue("xsell_shipment_discount", l_iCounter)));
                    ml_ShipDiscount = cartDetailrow.Ismultiline_shipment_discountNull() ? 0 : cartDetailrow.multiline_shipment_discount;     //Convert.ToDecimal(IIf(GetValue("multiline_shipment_discount", l_iCounter) = "", 0.0, GetValue("multiline_shipment_discount", l_iCounter)));

                    xsellProductDiscount = xsellProductDiscount + xsellProductDiscount1;
                    xsellShipDiscount = xsellShipDiscount + xsellShipDiscount1;

                    revCustomizationCost = 0;
                    revCustomizationCost = cartDetailrow.Isrev_customization_costNull() ? 0 : cartDetailrow.rev_customization_cost;    //Convert.ToDecimal(IIf(GetValue("rev_customization_cost", l_iCounter) = "", 0.0, GetValue("rev_customization_cost", l_iCounter)));
                    totalRevCustomizationCost = totalRevCustomizationCost + revCustomizationCost;


                    if (PoDtlKey == cartDetailrow.pkey_cust_po_dtl.ToString())  //  GetValue("pkey_cust_po_dtl", l_iCounter))
                        strClass = "bodytext"; //"head1";
                    else
                        strClass = "bodytext";

                    string frontlayout = string.Empty;
                    string prodDetails;
                    string sizekey;
                    prodDetails = cartDetailrow.Isproduct_detailsNull() ? "" : cartDetailrow.product_details;     // GetValue("product_details", l_iCounter);
                    if (prodDetails.IndexOf("Paper / Coating Type: ") > -1)
                        prodDetails = prodDetails.Replace("Paper / Coating Type: ", "Paper / Coating Type: <b>") + "</b>";

                    string cardsFor = cartDetailrow.Iscards_forNull() ? "" : cartDetailrow.cards_for;   //GetValue("cards_for", l_iCounter);
                    string strBackImage = cartDetailrow.Isreverse_layoutNull() ? "" : cartDetailrow.reverse_layout;   //GetValue("reverse_layout", l_iCounter);
                    if (cardsFor == "PC" || cardsFor == "CM" || cardsFor == "SM")
                    {
                        frontlayout = cartDetailrow.front_layout;   //GetValue("front_layout", l_iCounter);
                        sizekey = cartDetailrow.size_key.ToString();   //GetValue("size_key", l_iCounter);
                        if (frontlayout.IndexOf("PCOVE") >= 0 || sizekey == "32")
                            PCSize = "Jumbo ";
                        else
                            PCSize = "Standard ";
                    }
                    else
                        PCSize = "";

                    if ((cardsFor == "BC" || cardsFor == "RCBC") && strBackImage != "")
                        BackText = " With Back";
                    else
                        BackText = "";

                    if (!isFreeProdcut)
                    {
                        nonFreeproducts = nonFreeproducts + 1;
                        productCost = cartDetailrow.Isprice_per_unitNull() ? 0 : cartDetailrow.price_per_unit;    // GetValue("price_per_unit", l_iCounter);
                        processCost = cartDetailrow.Isprocess_costNull() ? 0 : cartDetailrow.process_cost;       //Convert.ToDecimal((GetValue("process_cost", l_iCounter) != "" ? GetValue("process_cost", l_iCounter) : 0.0));
                        m_cartCost = m_cartCost + productCost;
                        if (!cartDetailrow.Isis_direct_mailNull() && cartDetailrow.is_direct_mail)     // Convert.ToBoolean(GetValue("is_direct_mail", l_iCounter)))
                        {
                            decimal usLeadsCost = 0;
                            IsUsLeads = false;
                            if (!cartDetailrow.IsusleadstokenNull())    //!IsDBNull(m_CartData.CartDetails.Rows(l_iCounter)("usleadstoken")))
                            {
                                if (cartDetailrow.usleadstoken != "")
                                {
                                    IsUsLeads = true;
                                    usLeadsCost = cartDetailrow.Isusleads_costNull() ? 0 : cartDetailrow.usleads_cost;     // Convert.ToDecimal(m_CartData.CartDetails.Rows(l_iCounter)("usleads_cost"));
                                }
                            }
                            strText = strText + " <TR vAlign='top' bgColor='#ffffff'> ";
                            strText = strText + "    <TD class='bodytext'>" + Convert.ToString(l_iCounter + 1) + "</TD> ";
                            strText = strText + "    <TD class='bodytext'>" + cartDetailrow.date_created + "</TD>";
                            strText = strText + "    <TD class='bodytext'><B>" + PCSize + cartDetailrow.product_id + BackText + " </B><BR>";     //cartDetailrow.fkey_ec_attribute + 
                            strText = strText + cartDetailrow.product_details + String.Format("<BR><BR><B>Scheduled Date:</B> {0}", cartDetailrow.DirectMailScheduleDt);
                            strText = strText + String.Format("<br><br><B>Shipping: {0}</B>", cartDetailrow.ship_price_option) + "</TD>";
                            strText = strText + "    <TD class='bodytext' >" + cartDetailrow.quantity + " </TD>";
                            strText = strText + "    <TD class='bodytext'>" + Convert.ToString(productCost + processCost + usLeadsCost + cartDetailrow.shipping_price) + " </TD>";
                            strText = strText + " </TR>";
                            //'strText = strText + " <TR vAlign='top' bgColor='#ffffff'> "
                            //'strText = strText + "    <TD class='bodytext' >Shipping USPS</TD> "
                            //'strText = strText + "    <TD class='bodytext' ></TD> "
                            //'strText = strText + "    <TD class='bodytext' >" + Convert.ToString(GetValue("shipping_price", l_iCounter)) + "</TD> "
                            //'strText = strText + " </TR>"
                            dmShipCost = dmShipCost + usLeadsCost + cartDetailrow.shipping_price;
                            //'shipCost = shipCost + Convert.ToDecimal(GetValue("shipping_price", l_iCounter))
                        }
                        else
                        {
                            shipOption = cartDetailrow.ship_price_option;
                            shipCost = shipCost + (cartDetailrow.shipping_price.ToString() == "" ? 0 : cartDetailrow.shipping_price);

                            strText = strText + "<TR vAlign='top' bgColor='#ffffff'>";
                            strText = strText + "    <TD class='" + strClass + "'>" + Convert.ToString(l_iCounter + 1) + "</TD>";
                            strText = strText + "    <TD class='" + strClass + "'>" + cartDetailrow.date_created + "</TD>";
                            strText = strText + "    <TD class='" + strClass + "'><B>" + PCSize + cartDetailrow.product_id + BackText + "</B><BR>";    // + cartDetailrow.fkey_ec_attribute
                            strText = strText + cartDetailrow.product_details + "<BR>";
                            if (xsellProductDiscount1 > 0)
                            {
                                strText = strText + String.Format("<BR> <B>Product Discount: {0}</B>", ConvertToMoneyFormat(xsellProductDiscount1));
                            }
                            if (xsellShipDiscount1 > 0)
                            {
                                strText = strText + String.Format("<BR> <B>Shipment Discount: {0}</B>", ConvertToMoneyFormat(xsellShipDiscount1));
                            }
                            if (ml_ShipDiscount > 0)
                            {
                                strText = strText + String.Format("<BR> <B>Shipment Discount: {0}</B>", ConvertToMoneyFormat(ml_ShipDiscount));
                            }
                            if (revCustomizationCost > 0)
                            {
                                strText = strText + String.Format("<BR> <B>Custom Back Cost: {0}</B>", ConvertToMoneyFormat(revCustomizationCost));
                            }
                            strText = strText + "</TD>";
                            strText = strText + "    <TD class='" + strClass + "'>" + cartDetailrow.quantity + "</TD>";
                            strText = strText + "    <TD class='" + strClass + "' align='right'>" + ConvertToMoneyFormat(productCost + processCost) + " </TD>";
                            strText = strText + "</TR>";
                            Is_OnlyDirectMail = false;
                        }
                    }
                    else
                    {
                        freeShipCost = cartDetailrow.shipping_price;
                        m_cartCost = m_cartCost + freeShipCost;
                        productCost = freeShipCost;
                        strText = strText + " <TR vAlign='top' bgColor='#ffffff'> ";
                        strText = strText + "    <TD class='bodytext' rowSpan='2'>" + Convert.ToString(l_iCounter + 1) + "</TD> ";
                        strText = strText + "    <TD class='bodytext' rowSpan='2'>" + cartDetailrow.date_created + "</TD>";
                        strText = strText + "    <TD class='bodytext'><B>" + PCSize + cartDetailrow.product_id + BackText + " </B><BR>";    // cartDetailrow.fkey_ec_attribute +
                        strText = strText + cartDetailrow.product_details + "</TD>";
                        strText = strText + "    <TD class='bodytext'>" + cartDetailrow.quantity + " </TD>";
                        strText = strText + "    <TD class='bodytext'>Free</TD>";
                        strText = strText + " </TR>";
                        strText = strText + " <TR vAlign='bottom' bgColor='#ffffff'> ";
                        strText = strText + "    <TD class='bodytext' align='right' colSpan='2'><B>Shipping and Handling Charges :</B></TD>";
                        strText = strText + "    <TD class='bodytext' align='right'>" + (freeShipCost > 0 ? ConvertToMoneyFormat(freeShipCost) : "Free") + "</TD>";
                        strText = strText + " </TR>";
                    }

                    totalProcessCost = totalProcessCost + processCost;
                }
                StateTaxOn = m_cartCost - (xsellProductDiscount);
                m_cartCost = m_cartCost + shipCost + dmShipCost + totalProcessCost;
                TaxAmount = StateTaxOn * GetStateTax();
                // TaxAmount = Convert.ToDouble(String.Format("{0:n2}", TaxAmount));


                if (nonFreeproducts > 0 && Is_OnlyDirectMail == false)
                {
                    strText = strText + "<TR bgColor='#ffffff'>";
                    strText = strText + "    <TD class='bodytext' align='right' colSpan='4'><B>" + GetShipDisplayValue(shipOption, m_CartData.CartDetails.Rows.Count - 1) + "</B></TD>";
                    strText = strText + "    <TD class='bodytext' align='right'><B>" + (shipCost > 0 ? ConvertToMoneyFormat(shipCost) : "Free") + "</B></TD>";
                    strText = strText + "</TR>";
                    isNotDM = true;

                    if (mTurnAroundCost > 0 && mTurnaroundTime != "0")
                    {
                        strText = strText + "<TR bgColor='#ffffff'>";

                        strText = strText + "    <TD class='bodytext' align='right' colSpan='4'><B>" + String.Format("Turnaround premium for {0} Business days", mTurnaroundTime) + "</B></TD>";

                        strText = strText + "    <TD class='bodytext' align='right'><B>" + ConvertToMoneyFormat(mTurnAroundCost) + "</B></TD>";
                        strText = strText + "</TR>";
                        m_cartCost = m_cartCost + mTurnAroundCost;
                        //m_cartCost = Convert.ToDouble(String.Format("{0:n2}", m_cartCost));

                    }

                }

                if (!cartBillrow.Isfkey_promoNull())
                {
                    try
                    {
                        if (!cartBillrow.Ispromo_codeNull() && cartBillrow.promo_code != "")
                        {
                            promotioncode = string.Format("Promotional Discount Code: <b>{0}</b>", cartBillrow.promo_code);
                        }
                    }
                    catch
                    {
                        promotioncode = "";
                    }

                    productPromoCost = cartBillrow.Ispromo_product_discountNull() ? 0 : cartBillrow.promo_product_discount;
                    shipPromoCost = cartBillrow.Ispromo_shipment_discountNull() ? 0 : cartBillrow.promo_shipment_discount;

                    if (productPromoCost > 0)
                    {
                        strText = strText + " <TR vAlign='bottom' bgColor='#ffffff'> ";
                        strText = strText + "    <TD class='bodytext' align='right' colSpan='4'><B>Promotional discount on products :</B></TD>";
                        strText = strText + "    <TD class='bodytext' align='right'><B>" + ConvertToMoneyFormat(productPromoCost) + "</B></TD>";
                        strText = strText + " </TR>";
                        m_cartCost = m_cartCost - productPromoCost;
                        StateTaxOn = StateTaxOn - productPromoCost;
                        TaxAmount = StateTaxOn * GetStateTax();
                    }
                    if (shipPromoCost > 0)
                    {
                        strText = strText + " <TR vAlign='bottom' bgColor='#ffffff'> ";
                        strText = strText + "    <TD class='bodytext' align='right' colSpan='4'><B>Promotional discount on shipment :</B></TD>";
                        strText = strText + "    <TD class='bodytext' align='right'><B>" + ConvertToMoneyFormat(shipPromoCost) + "</B></TD>";
                        strText = strText + " </TR>";
                        m_cartCost = m_cartCost - shipPromoCost;
                    }
                }

                mcaproductDiscount = cartBillrow.Ismca_product_discountNull() ? 0 : cartBillrow.mca_product_discount;   // Convert.ToString(GetPromoCosts("mca_product_discount"));
                mcashipDiscount = cartBillrow.Ismca_shipment_discountNull() ? 0 : cartBillrow.mca_shipment_discount;     // Convert.ToString(GetPromoCosts("mca_shipment_discount"));
                multiline_ShipDiscount = cartBillrow.Ismultiline_shipment_discountNull() ? 0 : cartBillrow.multiline_shipment_discount;  // Convert.ToString(GetMultiLineDiscount("multiline_shipment_discount"));

                if (mcaproductDiscount > 0)
                {
                    strText = strText + " <TR vAlign='bottom' bgColor='#ffffff'> ";
                    strText = strText + "    <TD class='bodytext' align='right' colSpan='4'><B>MCA discount on products :</B></TD>";
                    strText = strText + "    <TD class='bodytext' align='right'><B>" + ConvertToMoneyFormat(mcaproductDiscount) + "</B></TD>";
                    strText = strText + " </TR>";
                    m_cartCost = m_cartCost - mcaproductDiscount;
                    StateTaxOn = StateTaxOn - mcaproductDiscount;
                    TaxAmount = StateTaxOn * GetStateTax();
                }
                if (mcashipDiscount > 0)
                {
                    strText = strText + " <TR vAlign='bottom' bgColor='#ffffff'> ";
                    strText = strText + "    <TD class='bodytext' align='right' colSpan='4'><B>MCA discount on shipment </B></TD>";
                    strText = strText + "    <TD class='bodytext' align='right'><B>" + ConvertToMoneyFormat(mcashipDiscount) + "</B></TD>";
                    strText = strText + " </TR>";
                    m_cartCost = m_cartCost - mcashipDiscount;
                }
                if (multiline_ShipDiscount > 0)
                {
                    strText = strText + " <TR vAlign='bottom' bgColor='#ffffff'> ";
                    strText = strText + "    <TD class='bodytext' align='right' colSpan='4'><B>Less Shipping Discount </B></TD>";
                    strText = strText + "    <TD class='bodytext' align='right'><B>" + ConvertToMoneyFormat(multiline_ShipDiscount) + "</B></TD>";
                    strText = strText + " </TR>";
                    m_cartCost = m_cartCost - multiline_ShipDiscount;
                }

                m_cartCost = m_cartCost - (xsellShipDiscount + xsellProductDiscount);
                m_cartCost = m_cartCost + totalRevCustomizationCost;
                if (xsellProductDiscount > 0)
                {
                    strText = strText + "<TR bgColor='#ffffff'>";
                    strText = strText + "<TD class='bodytext' align='right' colSpan='4'><B>Discount On Matching Products </B></TD>";
                    strText = strText + "<TD class='bodytext' align='right'><B>" + ConvertToMoneyFormat(xsellProductDiscount) + "</B></TD>";
                    strText = strText + "</TR>";
                }
                if (xsellShipDiscount > 0)
                {
                    strText = strText + "<TR bgColor='#ffffff'>";
                    strText = strText + "<TD class='bodytext' align='right' colSpan='4'><B>Discount On Shipment </B></TD>";
                    strText = strText + "<TD class='bodytext' align='right'><B>" + ConvertToMoneyFormat(xsellShipDiscount) + "</B></TD>";
                    strText = strText + "</TR>";
                }

                if (TaxAmount > 0)
                {
                    strText = strText + " <TR vAlign='bottom' bgColor='#ffffff'> ";
                    strText = strText + "    <TD class='bodytext' align='right' colSpan='4'><B>" + WebConfiguration.StateTaxLabel + " </B></TD>";
                    strText = strText + "    <TD class='bodytext' align='right'><B>" + ConvertToMoneyFormat(TaxAmount) + "</B></TD>";
                    strText = strText + " </TR>";
                    m_cartCost = m_cartCost + TaxAmount;
                }

                if (customerCreditAmt > 0)
                {
                    strText = strText + " <TR vAlign='bottom' bgColor='#ffffff'> ";
                    strText = strText + "    <TD class='bodytext' align='right' colSpan='4'><B>Less : Customer Credit </B></TD>";
                    strText = strText + "    <TD class='bodytext' align='right'><B>" + ConvertToMoneyFormat(customerCreditAmt) + "</B></TD>";
                    strText = strText + " </TR>";
                    m_cartCost = m_cartCost - customerCreditAmt;
                    // m_cartCost = Convert.ToDouble(String.Format("{0:n2}", m_cartCost));
                }
                if (totalRevCustomizationCost > 0)
                {
                    strText = strText + "<TR bgColor='#ffffff'>";
                    strText = strText + "<TD class='bodytext' align='right' colSpan='4'><B>Reverse Template Customization </B></TD>";
                    strText = strText + "<TD class='bodytext' align='right'><B>" + ConvertToMoneyFormat(totalRevCustomizationCost) + "</B></TD>";
                    strText = strText + "</TR>";
                }
                if (m_customizationCost > 0)
                {
                    strText = strText + " <TR vAlign='bottom' bgColor='#ffffff'> ";
                    strText = strText + "    <TD class='bodytext' align='right' colSpan='4'><B>Customization Additional Fee </B></TD>";
                    strText = strText + "    <TD class='bodytext' align='right'><B>" + ConvertToMoneyFormat(m_customizationCost) + "</B></TD>";
                    strText = strText + " </TR>";
                    m_cartCost = m_cartCost + m_customizationCost;
                    // m_cartCost = Convert.ToDouble(String.Format("{0:n2}", m_cartCost));
                }
                return strText;
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
            return "";
        }

        private string GetShipDisplayValue(string shipOption, int l_iIndex)
        {
            try
            {
                string strDate = "";
                try
                {
                    strDate = shipOption;
                    switch (strDate)
                    {
                        case "PRICE_ONE":
                            return "Shipping (Express 1 day air)";
                        case "PRICE_TWO":
                            return "Shipping (Priority 2-3 Days)";
                        case "PRICE_SEVEN":
                            return "Shipping (Regular 4-5 day ground)";
                        case "PRICE_FOURTEEN":
                            return "Shipping (Standard Ground 6-10 business days)";
                        default:
                            return "Shipping (Standard Ground 6-10 business days)";
                    }
                }
                catch (Exception)
                {
                    strDate = "";
                }
                return strDate;
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
            }
            return "";
        }

        private decimal GetStateTax()
        {
            int fkey_state;
            double tax = 0;
            try
            {
                if (m_CartData.CartShipInfo.Rows.Count > 0)
                {
                    fkey_state = Convert.ToInt32(m_CartData.CartShipInfo.Rows[0]["fkey_state"]);
                    var list = (from collect in m_CartData.States
                                where collect.pkey_state == fkey_state
                                select collect).ToList();
                    tax = WebConfiguration.GetApplicableStateTax(list[0].state_name);
                    tax = tax / 100;
                }
            }
            catch(Exception ex)
            {
                FileLogger.WriteException(ex);
                return Convert.ToDecimal(tax);
            }
            return Convert.ToDecimal(tax);
        }

        public string GetShipInfoIsUSPSOrder()
        {
            try
            {
                if (Convert.ToBoolean(m_CartData.CartShipInfo.Rows[0]["IsUSPSOrder"]) && isNotDM)
                {
                    return "<p>* Your Order will be Shipped via USPS.</font><br>We cannot guarantee that your package will arrive within the time provided. We also do not provide any tracking numbers for orders shipped to PO boxes through USPS.</p>";
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                throw ex;
            }
            return "";
        }

    }
}