﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TemplateTypeSelect.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Customers.TemplateTypeSelect" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head id="Head1" runat="server">
    <title>Best Print Buy</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" media="all" href="../css/bestprintbuy_style.css" />
    <link rel="stylesheet" media="all" href="../css/jquery.fancybox.css" />
    <script src="../Scripts/jquery.js"></script>
    <script src="../Scripts/common.js"></script>
    <script src="../Scripts/ddsmoothmenu.js"></script>
    <script src="../Scripts/plugin/fancybox/jquery.fancybox.js"></script>
    <!--[if lt IE 9]>
			<script src="../Scripts/html5.js"></script>
		<![endif]-->
    <!--[if lte IE 7]> <html class="ie7"> <![endif]-->
    <!--[if IE 8]>     <html class="ie8"> <![endif]-->
    <!--[if IE 9]>     <html class="ie9"> <![endif]-->
    <!--[if IE 10]>     <html class="ie10"> <![endif]-->
    <!--[if !IE]><!-->
    <!--<![endif]-->

    <script type="text/javascript">
        function SetDataAndClose(isNodata, typeIds, typeNames, typeCat) {
            /*if (isNodata) {
                alert("Plaese select");
                return false;
            }*/
            window.top.SetTemplateTypeData(typeIds, typeNames, typeCat);
            return false;
        }


        function checkSelect() {
            var ischecked = false;
            $('.select input:checkbox').each(function () {
                if (this.checked) {
                    ischecked = true;
                    return true;
                }
            });

            if (!ischecked) {
                alert("Please select atleast one template.");
                return false;
            }
            return true;
        }
    </script>
</head>

<body id="Body" runat="server">
    <!-- Wrapper starts -->
    <div id="Div1" class="popup-wrapper" runat="server">
        <div class="popup-title">
            Template types
        </div>
        <!-- Header Ends -->
        <div class="popup-contnt">
            <!-- Menu starts -->
            <form id="Form1" runat="server">                    
                        <table class="tablep">
                            <thead>
                                <tr>
                                    <th>Template Category</th>
                                    <th>Template Type</th>
                                    <th>Select</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rptrAvailableTemplateTypes" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <%#Eval("category_name") %>
                                            </td>
                                            <td>
                                                <%#Eval("type_name") %>
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkSelect" class="select"/>
                                                <asp:HiddenField ID="hdnTemplateCat" runat="server" Value='<%#Eval("pkey_promo_template_cat") %>' />
                                                <asp:HiddenField ID="hdnCategoryType" runat="server" Value='<%#Eval("pkey_category_type") %>' />
                                                <asp:HiddenField ID="hdnCategoryName" runat="server" Value='<%#Eval("category_name") %>' />
                                                <asp:HiddenField ID="hdnTypeName" runat="server" Value='<%#Eval("type_name") %>' />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>                                
                            </tbody>
                        </table>
                    <table>
                        <tr>
                            <td style="float:right;">
                                <asp:Button runat="server" ID="btnSelect" CssClass="button rounded" Text="Select" OnClientClick="if(!checkSelect()) return false;" OnClick="btnSelect_Click"></asp:Button>
                                <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClientClick="parent.$.fancybox.close(); return false;"></asp:Button>
                            </td>
                        </tr>
                    </table>
            </form>
        </div>
    </div>
</body>

</html>
