﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using System.Text;

namespace Bestprintbuy.Admin.Web.Customers
{
    public partial class MCACustomerList : PageBase
    {
        protected int serialNo;
        protected string searchfor = "-1";
        protected int recordCount = 0;
        protected int variantKey = Convert.ToInt32(WebConfiguration.ProductVariants["ExclusiveCustomer"]);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                RunScript("var pTitle = 'MCACustomers'");
                GetKioskList(variantKey, ConnString);
                LoadData();
            }
        }

        private void LoadData()
        {
            try
            {
                CustomerModel.CustomersDataTable MCAcustomers = UserService.GetMCACustomersList(searchfor, ConnString);
                if (CustomerslistGrid.Attributes["SortExpression"] != null)
                    MCAcustomers.DefaultView.Sort = CustomerslistGrid.Attributes["SortExpression"] + " " + CustomerslistGrid.Attributes["SortDirection"];
                recordCount = MCAcustomers.Rows.Count;
                CustomerslistGrid.DataSource = MCAcustomers.DefaultView;
                CustomerslistGrid.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        private void GetKioskList(int variantKey, string ConnString)
        {
            try
            {
                ProductTemplateModel.KiosksDataTable kioskList = ProductService.getKioskList(variantKey, ConnString);
                ddlkiosks.DataSource = kioskList;
                ddlkiosks.DataTextField = "attribute_value";
                ddlkiosks.DataValueField = "pkey_attribute";
                ddlkiosks.DataBind();
                ddlkiosks.Items.Insert(0, new ListItem("All Kiosks", "-1"));
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void CustomerslistGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            SortGridView(CustomerslistGrid, e);
            CustomerslistGrid.PageIndex = 0;
            searchfor = GetSearchCondition();
            LoadData();
        }

        protected void CustomerslistGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CustomerslistGrid.PageIndex = e.NewPageIndex;
            searchfor = GetSearchCondition();
            LoadData();
        }

        //protected void CustomerslistGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {
        //        e.Row.Cells[0].Text = Convert.ToString(++serialNo);
        //    }
        //}

        protected int GetRecordCountForPage()
        {
            return GetRecordCountForPageSize(CustomerslistGrid.PageSize, CustomerslistGrid.PageIndex + 1);
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/DashBoard.aspx");
        }

        protected void btnCreateMCAccount_Click(object sender, EventArgs e)
        {
            Response.Redirect("MCACustomerDetails.aspx?Customerpkey=-2");
        }

        protected void btnExportExcel_Click(object sender, EventArgs e)
        {

        }

        protected void btnAssignMCAstatus_Click(object sender, EventArgs e)
        {
            Response.Redirect("CustomerValidateAndAssign.aspx");
        }

        protected void ddlkiosks_SelectedIndexChanged(object sender, EventArgs e)
        {
            searchfor = "-1";
            if (ddlkiosks.SelectedValue != "-1")
                searchfor = "fkey_ec_attribute = " + ddlkiosks.SelectedValue;
            LoadData();
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            try
            {
                CustomerslistGrid.PageIndex = 0;
                searchfor = GetSearchCondition();
                LoadData();
                btnReset.Visible = true;
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            CustomerslistGrid.PageIndex = 0;
            btnReset.Visible = false;
            ddlOperator.SelectedIndex = 0;
            ddlFilterFor.SelectedIndex = 0;
            txtPattern.Text = "";
            LoadData();
        }

        public String GetSearchCondition()
        {
            //Variable holding the current session object
            System.Web.SessionState.HttpSessionState currsess = System.Web.HttpContext.Current.Session;

            //Variable holding the parameters - values from the request in a named value collection
            NameValueCollection requestCollections = System.Web.HttpContext.Current.Request.Params;

            //Get the Critera , pattern, operator from the request
            String criteria = ddlFilterFor.SelectedValue;
            String roperator = ddlOperator.SelectedValue;
            String pattern = txtPattern.Text.Trim();

            //Check if pattern exists
            if ("".Equals(pattern)) return "-1";

            //Create a new instance of named value collection
            NameValueCollection nvc = new NameValueCollection();

            //Add the Critera , pattern, operator to the nvc
            nvc.Add("criteria", criteria);
            nvc.Add("operator", roperator);
            nvc.Add("pattern", pattern);
            currsess["combovalues"] = nvc;

            String searchfor = FormSearchString(nvc);
            currsess["search_condition"] = searchfor;
            currsess["pattern_value"] = pattern;

            return searchfor;
        }

        private String FormSearchString(NameValueCollection nvc)
        {
            String sOperator = nvc["operator"];
            sOperator = sOperator != null ? sOperator.Trim() : sOperator;
            string pattern = nvc["pattern"];
            StringBuilder sb = new StringBuilder();


            if (sOperator.IndexOf("LIKE") < 0)
            {
                pattern = pattern.Replace("'", "''");
                sb.Append(nvc["criteria"]).Append(" ").Append(sOperator).Append(" ").Append("'").Append(pattern).Append("'");
            }
            else if (sOperator != null && (sOperator.ToLower()).IndexOf("in") > 0)
            {
                sb.Append(nvc["criteria"]).Append("  ").Append(sOperator).Append(" (").Append(pattern).Append(")");
            }
            else
            {
                sOperator = sOperator.Replace("LIKE", pattern);
                sOperator = sOperator.Replace("'", "''");
                sb.Append(nvc["criteria"]).Append(" LIKE '").Append(sOperator).Append("'");
            }
            return sb.ToString();
        }
    }
}