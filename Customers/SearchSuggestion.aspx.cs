﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;

namespace Bestprintbuy.Admin.Web.Customers
{
    public partial class SearchSuggestion : PageBase
    {
        //protected int field = -1;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //ppsol.uip.CustomerProcessor m_Processor = new ppsol.uip.CustomerProcessor();
                string input = Request.QueryString["input"];
                string searchfor = Request.QueryString["sid"];
                //field = Convert.ToInt32(Request.QueryString["Field"] == "" ? "-1" : Request.QueryString["Field"]);
                //string strField = "";
                //if (field == 0)
                //    strField = "EMAIL_ID";
                //else if (field == 1)
                //    strField = "NAME";
                //else if (field == 2)
                //    strField = "PO_NUMBER";
                //else if (field == 3)
                //    strField = "PO_DTL_NUMBER";
                string result = "";
                switch (searchfor)
                {
                    case "officerooster":
                        int fkey_ec_attribute = Convert.ToInt32(Request.QueryString["cboKiosks"]);
                        result = UserService.GetOfficeRoosterList(fkey_ec_attribute, input, ConnString);
                        break;
                    //case "orderfulfillment":
                    //    result = UserService.GetAutoSearchInfoList("%"+input+"%", strField, ConnString);
                    //    break;                        
                }
                Response.Clear();
                Response.Write(result);
                Response.End();
            }
        }
    }
}