﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Collections.Specialized;
using System.Text;

namespace Bestprintbuy.Admin.Web.Customers
{
    public partial class ProductsList : PageBase
    {
        protected int productPkey = 0;
        protected string searchstring = "";
        protected int recordCount = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadData();
            }
        }

        private void LoadData()
        {
            ProductModel ProductList = ProductService.GetProductList(searchstring, ConnString);
            //if (ProductlistGrid.Attributes["SortExpression"] != null)
            //    ProductList.Products.DefaultView.Sort = ProductlistGrid.Attributes["SortExpression"] + " " + ProductlistGrid.Attributes["SortDirection"];
            //recordCount = ProductList.Products.Rows.Count;

            //ProductlistGrid.DataSource = ProductList.Products.DefaultView;
            //ProductlistGrid.DataBind();
        }

        protected void ProductlistGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            SortGridView(ProductlistGrid, e);
            ProductlistGrid.PageIndex = 0;
            searchstring = GetSearchCondition();
            LoadData();
        }

        protected void ProductlistGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ProductlistGrid.PageIndex = e.NewPageIndex;
            searchstring = GetSearchCondition();
            LoadData();
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            ProductlistGrid.PageIndex = 0;
            btnReset.Visible = false;
            ddlOperator.SelectedIndex = 0;
            ddlFilterFor.SelectedIndex = 0;
            txtPattern.Text = "";
            LoadData();
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            ProductlistGrid.PageIndex = 0;
            searchstring = GetSearchCondition();
            LoadData();
            btnReset.Visible = true;
        }

        public String GetSearchCondition()
        {
            //Variable holding the current session object
            System.Web.SessionState.HttpSessionState currsess = System.Web.HttpContext.Current.Session;

            //Variable holding the parameters - values from the request in a named value collection
            NameValueCollection requestCollections = System.Web.HttpContext.Current.Request.Params;

            //Get the Critera , pattern, operator from the request
            String criteria = ddlFilterFor.SelectedValue;
            String roperator = ddlOperator.SelectedValue;
            String pattern = txtPattern.Text.Trim();

            //Check if pattern exists
            if ("".Equals(pattern)) return "";

            //Create a new instance of named value collection
            NameValueCollection nvc = new NameValueCollection();

            //Add the Critera , pattern, operator to the nvc
            nvc.Add("criteria", criteria);
            nvc.Add("operator", roperator);
            nvc.Add("pattern", pattern);
            currsess["combovalues"] = nvc;

            String searchfor = FormSearchString(nvc);
            currsess["search_condition"] = searchfor;
            currsess["pattern_value"] = pattern;

            return searchfor;
        }

        private String FormSearchString(NameValueCollection nvc)
        {
            String sOperator = nvc["operator"];
            sOperator = sOperator != null ? sOperator.Trim() : sOperator;
            string pattern = nvc["pattern"];
            StringBuilder sb = new StringBuilder();


            if (sOperator.IndexOf("LIKE") < 0)
            {
                pattern = pattern.Replace("'", "''");
                sb.Append(nvc["criteria"]).Append(" ").Append(sOperator).Append(" ").Append("'").Append(pattern).Append("'");
            }
            else if (sOperator != null && (sOperator.ToLower()).IndexOf("in") > 0)
            {
                sb.Append(nvc["criteria"]).Append("  ").Append(sOperator).Append(" (").Append(pattern).Append(")");
            }
            else
            {
                sOperator = sOperator.Replace("LIKE", pattern);
                sOperator = sOperator.Replace("'", "''");
                sb.Append(nvc["criteria"]).Append(" LIKE '").Append(sOperator).Append("'");
            }
            return sb.ToString();
        }

        protected int GetRecordCountForPage()
        {
            return GetRecordCountForPageSize(ProductlistGrid.PageSize, ProductlistGrid.PageIndex + 1);
        }

        protected void btnAddnewProduct_Click(object sender, EventArgs e)
        {
            Response.Redirect("ProductDetails.aspx?productKey=-2");
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/DashBoard.aspx");
        }
    }
}