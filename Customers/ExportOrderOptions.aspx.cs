﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Bestprintbuy.Admin.Web.Customers
{
    public partial class ExportOrderOptions : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtFromDate.Value = System.DateTime.Now.Month.ToString() + "/01/" + System.DateTime.Now.Year.ToString();
                if (System.DateTime.Now.Day < 10)
                {
                    txtToDate.Value = System.DateTime.Now.Month.ToString() + "/0" + System.DateTime.Now.Day.ToString() + "/" + System.DateTime.Now.Year.ToString();
                }
                else
                {
                    txtToDate.Value = System.DateTime.Now.Month.ToString() + "/" + System.DateTime.Now.Day.ToString() + "/" + System.DateTime.Now.Year.ToString();
                }
            }
        }
    }
}