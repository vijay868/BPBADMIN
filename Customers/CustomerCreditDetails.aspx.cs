﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.Customers
{
    public partial class CustomerCreditDetails : PageBase
    {
        protected int fkey_customer = -1;
        protected int pkey_service_credit = -2;
        protected string po_number = "";
        protected bool isPO = false;
        protected int povalue = 0;
        protected decimal creditamount = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                GetData();
            }
        }

        private void GetData()
        {
            try
            {
                ServiceCreditsModel customerCredit = new ServiceCreditsModel();
                fkey_customer = Convert.ToInt32(Request.QueryString["fkeycustomer"].Trim() == "" ? "-1" : Request.QueryString["fkeycustomer"].Trim());
                pkey_service_credit = Convert.ToInt32(Request.QueryString["Pkey"].Trim() == "" ? "-2" : Request.QueryString["Pkey"].Trim());
                po_number = Request.QueryString["ponumber"].Trim() == "" ? "" : Request.QueryString["ponumber"].Trim();
                isPO = Convert.ToBoolean(Request.QueryString["ispo"].ToLower());
                if (pkey_service_credit == -2 && po_number != string.Empty && Request.QueryString["ispo"].ToLower() == "true")             //Request["isPO"] != null && 
                {
                    customerCredit = CustomerService.GetCustomerCredit(fkey_customer, pkey_service_credit, po_number, ConnString);
                }
                else
                    customerCredit = CustomerService.GetCustomerCredit(fkey_customer, pkey_service_credit, ConnString);

                ViewState["CreditTypes"] = customerCredit.CreditTypes;
                if (customerCredit.CreditTypes != null)
                {
                    ddlCreditType.DataSource = customerCredit.CreditTypes;
                    ddlCreditType.DataTextField = "service_credit_abbr";
                    ddlCreditType.DataValueField = "pkey_service_credit_type";
                    ddlCreditType.DataBind();
                }
                if (customerCredit.CustomerCredit.Rows.Count > 0)
                {
                    ServiceCreditsModel.CustomerCreditRow row = (ServiceCreditsModel.CustomerCreditRow)customerCredit.CustomerCredit.Rows[0];
                    lblCustomerName.Text = row.customer_name;
                    lblCustName.Text = row.customer_name;
                    lblCustomerEmail.Text = row.email_id;
                    lblCustEmail.Text = row.email_id;
                    hdnFkeyCustomer.Value = row.fkey_customer.ToString();
                    if (!row.Ispkey_service_creditNull())
                    {
                        hdnPkeyServiceCredit.Value = row.pkey_service_credit.ToString();
                    }
                    else
                    {
                        hdnPkeyServiceCredit.Value = "-2";
                    }
                    if (!row.Isservice_credit_descNull())
                        lblCreditTypeDesc.Text = row.service_credit_desc;
                    else
                        lblCreditTypeDesc.Text = customerCredit.CreditTypes.Rows[0]["service_credit_desc"].ToString();
                    if (!row.Isavailed_amountNull())
                    {
                        lblAvailedAmount.Text = row.availed_amount.ToString();
                        lblAvailedAmt.Text = row.availed_amount.ToString();
                    }
                    else
                        lblAvailedAmount.Text = "";
                    if (!row.Isdate_createdNull())
                    {
                        lblDate.Text = row.date_created.ToShortDateString();
                        lblDt.Text = row.date_created.ToShortDateString();
                    }
                    else
                    {
                        lblDate.Text = DateTime.Now.ToShortDateString();
                        lblDt.Text = DateTime.Now.ToShortDateString();
                    }
                    if (!row.Iscredit_amountNull())
                    {
                        txtAmount.Text = row.credit_amount.ToString();
                        lblAmount.Text = row.credit_amount.ToString();
                        creditamount = Convert.ToDecimal(row.credit_amount);
                    }
                    else
                    {
                        txtAmount.Text = "";
                        creditamount = 0;
                    }

                    if (!row.IspovalueNull())
                    {
                        povalue = Convert.ToInt32(row.povalue);
                    }
                    else
                    {
                        povalue = 0;
                    }

                    if (!row.Ispo_numberNull())
                    {
                        txtPONumber.Text = row.po_number.ToString();
                        lblPONumber.Text = row.po_number.ToString();
                    }
                    else
                        txtPONumber.Text = "";
                    if (!row.Isother_reasonsNull())
                    {
                        if (row.other_reasons != "")
                        {
                            txtReason.Enabled = true;
                        }
                        txtReason.Text = row.other_reasons;
                        lblReason.Text = row.other_reasons;
                    }
                    else
                        txtReason.Text = "";
                    if (!row.Isfkey_service_credit_typeNull())
                    {
                        ddlCreditType.SelectedValue = row.fkey_service_credit_type.ToString();
                        lblCreditType.Text = row.service_credit_abbr + " - " + row.service_credit_desc;
                    }
                    else
                    {
                        ddlCreditType.SelectedValue = "1";
                    }
                    if (!row.Isis_activeNull())
                        lblIsActive.Text = row.is_active.ToString();
                    else
                        lblIsActive.Text = "True";
                    if ((row.Isavailed_amountNull() || row.availed_amount == 0) && pkey_service_credit != -2)
                    {
                        btnDelete.Visible = true;
                        if (lblIsActive.Text == "True")
                        {
                            btnDeactivate.Visible = true;
                        }
                    }
                    if (pkey_service_credit > 0)
                    {
                        btnCreditHistory.Visible = true;
                    }
                    if (lblIsActive.Text == "True")
                    {
                        btnConfirmCredit.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("CustomerCreditList.aspx");
        }

        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedvalue = ddlCreditType.SelectedValue;
            ServiceCreditsModel.CreditTypesDataTable creditTypes = (ServiceCreditsModel.CreditTypesDataTable)ViewState["CreditTypes"];
            foreach (ServiceCreditsModel.CreditTypesRow dr in creditTypes.Rows)
            {
                if (selectedvalue == dr.pkey_service_credit_type.ToString())
                {
                    lblCreditTypeDesc.Text = dr.service_credit_desc;
                    lblCreditType.Text = dr.service_credit_abbr + " - " + dr.service_credit_desc;
                }
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CustomerService.DeleteCustomerCredit(Convert.ToInt32(hdnPkeyServiceCredit.Value), ConnString);
                Response.Redirect("CustomerCreditList.aspx");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ServiceCreditsModel creditModel = new ServiceCreditsModel();
                ServiceCreditsModel.CustomerCreditRow row = creditModel.CustomerCredit.NewCustomerCreditRow();
                row.pkey_service_credit = Convert.ToInt32(hdnPkeyServiceCredit.Value == "" ? "-2" : hdnPkeyServiceCredit.Value);
                row.customer_name = lblCustName.Text;
                row.email_id = lblCustomerEmail.Text;
                row.fkey_customer = Convert.ToInt32(hdnFkeyCustomer.Value == "" ? "-1" : hdnFkeyCustomer.Value);
                row.fkey_service_credit_type = Convert.ToInt32(ddlCreditType.SelectedValue);
                row.other_reasons = txtReason.Text;
                row.credit_amount = Convert.ToDecimal(txtAmount.Text);
                row.availed_amount = Convert.ToDecimal(lblAvailedAmount.Text == "" ? "0.00" : lblAvailedAmount.Text);
                row.po_number = txtPONumber.Text;
                row.is_active = hdnIsactive.Value == "Yes" ? true : false;
                row.change_user = Identity.UserPK;
                creditModel.CustomerCredit.AddCustomerCreditRow(row);

                creditModel = CustomerService.UpdateCustomerCredit(creditModel, ConnString);
                Response.Redirect("CustomerCreditList.aspx");
                AlertScript("Successfully Saved");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript("Save/Update failed. try again.");
            }
        }
    }
}