﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using System.Data;
using System.Text;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.Customers
{
    public partial class AdminComments : PageBase
    {
        protected string poNumber = string.Empty;
        protected string soNumber = string.Empty;
        protected string cardsFor = string.Empty;
        string fkey_customer = null;
        public CommentsModel poModel = new CommentsModel();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                txtPkeyCustPoDtl.Value = Request["pkey_cust_po_dtl"].ToString();
                try
                {
                    poModel = CustomerService.GetAdminComments(Convert.ToInt32(txtPkeyCustPoDtl.Value), ConnString);
                    Page.DataBind();
                }
                catch (Exception ex)
                {                   
                    AlertScript(ex.Message);
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                string image1 = CopyImagesToWorkingArea(uploadImgFile1.Value, "image1");
                string image2 = CopyImagesToWorkingArea(uploadImgFile2.Value, "image2");
                string image3 = CopyImagesToWorkingArea(uploadImgFile3.Value, "image3");
                fkey_customer = Identity.UserPK.ToString();
                poModel = CustomerService.UpdateAdminComments(Convert.ToInt32(txtPkeyCustPoDtl.Value), txtComments.Text, image1, image2, image3, Convert.ToInt32(fkey_customer), ConnString);
                AlertScript("Successfully Saved.");
                txtComments.Text = "";
                Page.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        private string CopyImagesToWorkingArea(string url, string Side)
        {
            if (string.IsNullOrEmpty(url))
                return "";
            fkey_customer = Identity.UserPK.ToString();
            string serverPath = System.Configuration.ConfigurationManager.AppSettings["BPBCustomerBaseDirectory"];
            serverPath = serverPath + "\\DesignCenter\\Users\\" + string.Format("{0}\\{1}\\", fkey_customer, "CustomerProofs");

            string CustomerBaseUrl = System.Configuration.ConfigurationManager.AppSettings["BPBCustomerBaseURL"];
            CustomerBaseUrl = CustomerBaseUrl + "/DesignCenter/Users/" + string.Format("{0}/{1}/", fkey_customer, "CustomerProofs");
            System.IO.FileInfo fileCopy = new System.IO.FileInfo(url);

            System.IO.DirectoryInfo pathDir = new System.IO.DirectoryInfo(serverPath);
            if (!pathDir.Exists)
            {
                pathDir.Create();
            }
            else
            {
                pathDir.Refresh();
            }
            string Filename = fileCopy.Name;
            try
            {
                serverPath = serverPath + Filename;
                if (Side == "image1")
                {
                    uploadImgFile1.PostedFile.SaveAs(serverPath);
                }
                else if (Side == "image2")
                {
                    uploadImgFile2.PostedFile.SaveAs(serverPath);
                }
                else
                {
                    uploadImgFile3.PostedFile.SaveAs(serverPath);
                }

                return CustomerBaseUrl + Filename;

            }
            catch (Exception ex)
            {
            }
            return "";
        }

        public object PreviousComments()
        {
            DataRow[] drs = null;
            StringBuilder sb = new StringBuilder("");
            //Dim strUrl As String = "<a href='{0}'class='a1b'>{1}</a></br>"
            string strUrl = "<a href='javascript:;' {0} class='a1b'>{1}</a></br>";
            string strOnClick = "onclick=\"javascript:ShowImgPreview('{0}'); return false;\"";
            drs = poModel.AdminComments.Select();
            if (drs.Length <= 0)
                return "";
            foreach (DataRow dr in poModel.AdminComments.Rows)
            {
                //For Each dr In drs
                sb.AppendFormat("<b>{0} on {1}</b></br>{2}</br>", dr["login_id"], dr["date_created"], dr["admin_comments"]);
                if (!IsDBNull(dr["image1"]))
                {
                    if (!string.IsNullOrEmpty(dr["image1"].ToString()))
                    {
                        strOnClick = string.Format(strOnClick, dr["image1"]);
                        sb.AppendFormat(strUrl, strOnClick, "Image1");
                    }

                }
                if (!IsDBNull(dr["image2"]))
                {
                    if (!string.IsNullOrEmpty(dr["image2"].ToString()))
                    {
                        strOnClick = string.Format(strOnClick, dr["image2"]);
                        sb.AppendFormat(strUrl, strOnClick, "Image2");

                    }
                }
                if (!IsDBNull(dr["image3"]))
                {
                    if (!string.IsNullOrEmpty(dr["image3"].ToString()))
                    {
                        strOnClick = string.Format(strOnClick, dr["image3"]);
                        sb.AppendFormat(strUrl, strOnClick, "Image3");

                    }
                }

            }
            return sb.ToString();
        }

    }
}