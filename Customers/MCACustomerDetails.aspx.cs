﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Bestprintbuy.Admin.Web.Customers
{
    public partial class MCACustomerDetails : PageBase
    {
        CustomerModel MCAcustomer = new CustomerModel();
        protected int Customerkey;
        protected void Page_Load(object sender, EventArgs e)
        {
            ViewState["Customerpkey"] = Request.QueryString["Customerpkey"];
            Customerkey = Convert.ToInt32(ViewState["Customerpkey"]);
            if (!Page.IsPostBack)
            {
                GetData();
            }
        }

        private void GetData()
        {
            try
            {
                int CustomerPkey = Convert.ToInt32(ViewState["Customerpkey"]);
                MCAcustomer = UserService.GetMCACustomerDetailbykey(CustomerPkey, ConnString);

                ddlkiosk.DataSource = MCAcustomer.StoredFronts;
                ddlkiosk.DataTextField = "kiosk";
                ddlkiosk.DataValueField = "fkey_ec_attribute";
                ddlkiosk.DataBind();
                ddlkiosk.Items.Insert(0, new ListItem("Select Kiosk", ""));

                ddlState.DataSource = MCAcustomer.States;
                ddlState.DataTextField = "state_name";
                ddlState.DataValueField = "pkey_state";
                ddlState.DataBind();
                ddlState.Items.Insert(0, new ListItem("Select State", ""));

                ddlProdDiscount.DataSource = MCAcustomer.ProductDiscountLevels;
                ddlProdDiscount.DataTextField = "discount_level";
                ddlProdDiscount.DataValueField = "pkey_discount_level";
                ddlProdDiscount.DataBind();
                ddlProdDiscount.Items.Insert(0, new ListItem("Select Product Discount Level", ""));

                ddlShpDiscount.DataSource = MCAcustomer.ShipDiscountLevels;
                ddlShpDiscount.DataTextField = "discount_level";
                ddlShpDiscount.DataValueField = "pkey_discount_level";
                ddlShpDiscount.DataBind();
                ddlShpDiscount.Items.Insert(0, new ListItem("Select Shipment Discount Level", ""));

                if (MCAcustomer.Customers.Rows.Count > 0)
                {
                    CustomerModel.CustomersRow CustomerRow = (CustomerModel.CustomersRow)MCAcustomer.Customers.Rows[0];

                    txtEmail.Text = CustomerRow.email;
                    txtConfirmEmail.Text = CustomerRow.email;
                    txtFirstName.Text = CustomerRow.cus_firstname;
                    txtLastName.Text = CustomerRow.cus_lastname;
                    txtPhone.Text = CustomerRow.phone;
                    txtOwnerFirstName.Text = CustomerRow.op_first_name;
                    txtOwnerLastName.Text = CustomerRow.op_last_name;
                    txtOwnerPhone.Text = CustomerRow.op_phone;
                    txtStreetsuit.Text = CustomerRow.address1;
                    txtCity.Text = CustomerRow.city;
                    txtZip.Text = CustomerRow.zip;

                    ddlProdDiscount.SelectedValue = CustomerRow.fkey_discount_level.ToString();
                    ddlShpDiscount.SelectedValue = CustomerRow.fkey_ship_discount_level.ToString();
                    ddlState.SelectedValue = CustomerRow.fkey_state.ToString() == null ? "" : CustomerRow.fkey_state.ToString();
                    ddlkiosk.SelectedValue = CustomerRow.fkey_ec_attribute.ToString();
                    hdnkioskKey.Value = CustomerRow.fkey_ec_attribute;
                    if (CustomerRow.fkey_ec_attribute == "170")
                    {
                        divKWMcaOptions.Visible = true;
                        divMcatext.Visible = false;
                        divOfficeName.Visible = false;
                    }
                    else
                    {
                        divKWMcaOptions.Visible = false;
                        divMcatext.Visible = true;
                        divOfficeName.Visible = true;
                    }

                    bool isCustomQues = true;
                    foreach (ListItem item in ddlSecurityQuestion.Items)
                    {
                        if (item.Value == CustomerRow.securityquestion)
                        {
                            isCustomQues = false;
                            item.Selected = true;
                            break;
                        }
                    }

                    if (isCustomQues)
                    {
                        ddlSecurityQuestion.SelectedValue = "CustomQ";
                        txtCustomQuestion.Text = CustomerRow.securityquestion;
                        trCustomQuestion.Visible = true;
                        trCustomQuestion1.Visible = false;
                    }

                    txtSecurityAnswer.Text = Encryption.Decrypt(CustomerRow.securityanswer);

                    if (MCAcustomer.CustomerRooster.Rows.Count > 0)
                    {
                        Int32 l_iCounter = 0;
                        StringBuilder sb = new StringBuilder("");
                        CustomerModel.CustomerRoosterRow CustomerRoosters = (CustomerModel.CustomerRoosterRow)MCAcustomer.CustomerRooster.Rows[0];
                        txtOfficeID.Text = CustomerRoosters.officeid;
                        txtOfficeName.Text = CustomerRoosters.officename;
                        ddlAssignedMCs.DataSource = MCAcustomer.CustomerRooster;
                        ddlAssignedMCs.DataTextField = "rooster";
                        ddlAssignedMCs.DataValueField = "pkey_office_rooster";
                        ddlAssignedMCs.DataBind();
                        for (l_iCounter = 0; l_iCounter <= MCAcustomer.CustomerRooster.Rows.Count - 1; l_iCounter++)
                        {
                            sb.AppendFormat("<a href=\"javascript:;\" class=\"a1b\" onclick=\"javascript:ShowMCADetails({0})\">{1}</a>&nbsp;&nbsp;&nbsp;&nbsp;", MCAcustomer.CustomerRooster.Rows[l_iCounter]["pkey_office_rooster"], MCAcustomer.CustomerRooster.Rows[l_iCounter]["rooster"]);
                            if (l_iCounter % 5 == 0 && l_iCounter != 0)
                                sb.Append("</br>");
                        }
                        lblOfficeID.Text = sb.ToString();
                    }
                }
                chkisMCA.Checked = true;
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                int Customerpkey = Convert.ToInt32(ViewState["Customerpkey"]);
                CustomerModel MCACustomerDetails = new CustomerModel();
                CustomerModel.CustomersRow customer = MCACustomerDetails.Customers.NewCustomersRow();

                customer.is_mca = chkisMCA.Checked;
                customer.pkey_customer = Customerpkey;
                customer.email = txtEmail.Text;
                customer.email = txtConfirmEmail.Text;
                customer.password = txtPassword.Text.Trim() == "" ? "" : MD5Encryption.Encrypt(txtPassword.Text.Trim());
                customer.cus_firstname = txtFirstName.Text;
                customer.cus_lastname = txtLastName.Text;
                customer.phone = txtPhone.Text;
                customer.op_first_name = txtOwnerFirstName.Text;
                customer.op_last_name = txtOwnerLastName.Text;
                customer.op_phone = txtOwnerPhone.Text;
                customer.address1 = txtStreetsuit.Text;
                customer.city = txtCity.Text;
                customer.zip = txtZip.Text;
                customer.roosterkeys = txtMCS.Value == "" ? "-1" : txtMCS.Value;

                if (customer.Isis_personalNull())
                    customer.is_personal = false;
                else
                    customer.is_personal = true;

                if (customer.Isis_businessNull())
                    customer.is_business = false;
                else
                    customer.is_business = true;

                if (customer.Isemail_formatNull())
                    customer.email_format = false;
                else
                    customer.email_format = true;

                if (customer.Isis_businesscardsNull())
                    customer.is_businesscards = false;
                else
                    customer.is_businesscards = true;

                if (customer.Isis_postcardsNull())
                    customer.is_postcards = false;
                else
                    customer.is_postcards = true;

                if (customer.Isis_foldedcardsNull())
                    customer.is_foldedcards = false;
                else
                    customer.is_foldedcards = true;

                if (customer.Isis_bruchuresNull())
                    customer.is_bruchures = false;
                else
                    customer.is_bruchures = true;

                if (customer.Isis_marketingNull())
                    customer.is_marketing = false;
                else
                    customer.is_marketing = true;

                if (customer.Isis_bannersNull())
                    customer.is_banners = false;
                else
                    customer.is_banners = true;

                if (customer.Isis_newoffersNull())
                    customer.is_newoffers = false;
                else
                    customer.is_newoffers = true;

                if (customer.Isis_newslettersNull())
                    customer.is_newsletters = false;
                else
                    customer.is_newsletters = true;

                if (customer.Isis_acceptNull())
                    customer.is_accept = false;
                else
                    customer.is_accept = true;

                if (customer.IsisbillingNull())
                    customer.isbilling = false;
                else
                    customer.isbilling = true;

                if (customer.IsisshippingNull())
                    customer.isshipping = false;
                else
                    customer.isshipping = true;

                if (customer.IscandecryptpasswordNull())
                    customer.candecryptpassword = false;
                else
                    customer.candecryptpassword = true;

                customer.fkey_discount_level = ddlProdDiscount.SelectedValue;
                customer.fkey_ship_discount_level = ddlShpDiscount.SelectedValue;
                if (ddlState.SelectedValue != "")
                    customer.fkey_state = ddlState.SelectedValue;
                customer.fkey_ec_attribute = ddlkiosk.SelectedValue;

                if (ddlSecurityQuestion.SelectedValue == "CustomQ")
                {
                    customer.securityquestion = txtCustomQuestion.Text;
                }
                else
                {
                    customer.securityquestion = ddlSecurityQuestion.SelectedValue;
                }
                customer.securityanswer = Encryption.Encrypt(txtSecurityAnswer.Text);
                MCACustomerDetails.Customers.AddCustomersRow(customer);

                CustomerModel.CustomerRoosterRow customerRooster = MCACustomerDetails.CustomerRooster.NewCustomerRoosterRow();
                customerRooster.officeid = txtOfficeID.Text;
                customerRooster.officename = txtOfficeName.Text.Trim() == "" ? "" : txtOfficeName.Text.Trim();
                MCACustomerDetails.CustomerRooster.AddCustomerRoosterRow(customerRooster);

                Customerpkey = UserService.UpdateMCACustomer(MCACustomerDetails, ConnString);
                if (Convert.ToInt32(ViewState["Customerpkey"]) <= 0 && Customerpkey > 0)
                {
                    customer.pkey_customer = Customerpkey;
                    //Send Registration Mail
                    string strMailMessage = ReadMailContent("CustomerRegistration");
                    EmailProcessor.SendMCARegistrationEmail(strMailMessage, customer);
                    Response.Redirect("MCACustomerList.aspx");
                }
                else if (Customerpkey > 0)
                {
                    AlertScript("Updated successfully");
                    ViewState["Customerpkey"] = Customerpkey;
                    GetData();
                }
                else
                {
                    AlertScript("Save/Update failed please try again");
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.IndexOf("UNIQUE_CUSTOMER_EMAIL_ID") >= 0)
                {
                    AlertScript("The entered Email Address already exists, Please enter another and try.");
                }
                else
                {
                    FileLogger.WriteException(ex);
                    AlertScript(ex.Message);
                }
            }
        }

        private void SendRegistrationEmail(string msgBody, CustomerModel.CustomersRow custrow)
        {
            try
            {
                string UserName = "";

                if (!custrow.Iscus_firstnameNull())
                    UserName = UserName + custrow.cus_firstname;

                if (!custrow.Iscus_lastnameNull())
                    UserName = UserName + " " + custrow.cus_lastname;

                string emailID = custrow.email;
                string password = custrow.actualpassword;

                if (UserName.Trim() == "")
                    UserName = UserName + ",";
                else
                    UserName = emailID + ",";


                msgBody = string.Format(msgBody, UserName, emailID, password, DateTime.Now.ToString("MM/dd/yyyy hh:mm tt"));

                msgBody = msgBody.Replace("&#123;", "{");
                msgBody = msgBody.Replace("&#125;", "}");
                //SendEmail(emailID, msgBody);
            }
            catch (Exception ex)
            {

            }
        }

        /*Private Sub SendEmail(ByVal mailId As String, ByVal strMessage As String)
            Try
                Dim fromMailId = WebConfiguration.BPBCustomerCareEmail
                Dim mailObject As New MailModel
                strMessage = strMessage.Replace("##kiosk##", "")
                strMessage = strMessage.Replace("##ecid##", "")
                strMessage = strMessage.Replace("##eckey##", "")
                With mailObject
                    .FromMailID = fromMailId
                    .ToMailID = mailId
                    .MailSubject = "New User Registration Bestprintbuy.com"
                    .MailMessage = strMessage
                End With

                Dim sendMail As New Thread(New ThreadStart(AddressOf mailObject.SendMail))
                sendMail.Start()

            Catch ex As Exception

            End Try

        End Sub*/

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("MCACustomerList.aspx");
        }

        protected void ddlkiosk_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlkiosk.SelectedValue == "170")
            {
                divKWMcaOptions.Visible = true;
                divMcatext.Visible = false;
                divOfficeName.Visible = false;
            }
            else
            {
                divKWMcaOptions.Visible = false;
                divMcatext.Visible = true;
                divOfficeName.Visible = true;
            }
            this.txtPassword.Attributes.Add("value", txtPassword.Text);
            this.txtConfirmPassword.Attributes.Add("value", txtConfirmPassword.Text);
        }

        protected void ddlSecurityQuestion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSecurityQuestion.SelectedValue == "CustomQ")
            {
                trCustomQuestion.Visible = true;
                trCustomQuestion1.Visible = false;
            }
            else
            {
                trCustomQuestion.Visible = false;
                trCustomQuestion1.Visible = true;
            }

            this.txtPassword.Attributes.Add("value", txtPassword.Text);
            this.txtConfirmPassword.Attributes.Add("value", txtConfirmPassword.Text);
        }
    }
}