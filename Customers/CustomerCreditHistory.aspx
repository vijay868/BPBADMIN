﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomerCreditHistory.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Customers.CustomerCreditHistory" %>

<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->

<!-- Consider adding a manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<head id="Head1" runat="server">
    <title>Best Print Buy</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" media="all" href="../css/bestprintbuy_style.css" />
    <link rel="stylesheet" media="all" href="../css/jquery.fancybox.css" />
    <link rel="stylesheet" href="../css/jquery-ui.css" />
    <script src="../Scripts/jquery.js"></script>
    <script src="../Scripts/jquery-ui.js"></script>
    <script src="../Scripts/common.js"></script>
    <script src="../Scripts/ddsmoothmenu.js"></script>
    <script src="../Scripts/plugin/fancybox/jquery.fancybox.js"></script>
    <script src="../Scripts/jquery.validate.js"></script>
    <!--[if lt IE 9]>
			<script src="../Scripts/html5.js"></script>
		<![endif]-->
    <!--[if lte IE 7]> <html class="ie7"> <![endif]-->
    <!--[if IE 8]>     <html class="ie8"> <![endif]-->
    <!--[if IE 9]>     <html class="ie9"> <![endif]-->
    <!--[if IE 10]>     <html class="ie10"> <![endif]-->
    <!--[if !IE]><!-->

    <!--<![endif]-->
    <style type="text/css">
        .table td {
            text-align: left;
        }
    </style>
</head>

<body id="Body" runat="server">
    <!-- Wrapper starts -->
    <div id="Div1" class="popup-wrapper" runat="server">
        <div class="popup-title">
            Customer Credit History
        </div>
        <!-- Header Ends -->
        <div class="popup-contnt" style="padding-bottom: 40px">
            <!-- Menu starts -->

            <form id="frmCredirHistory" runat="server">
                <table>
                    <tr>
                        <td style="width: 25%">
                            <label>Name</label></td>
                        <td width="5%"></td>
                        <td>
                            <asp:Label ID="lblCustomerName" runat="server" Font-Size="12px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <label>Customer Email</label></td>
                        <td></td>
                        <td>
                            <asp:Label ID="lblCustomerEmail" runat="server" Font-Size="12px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <label>Total Credit Amount</label></td>
                        <td></td>
                        <td>
                            <asp:Label ID="lblTotalCreditAmt" runat="server" Font-Size="12px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <label>Total Active Credit Amount</label></td>
                        <td></td>
                        <td>
                            <asp:Label ID="lblTotalActiveCreditAmt" runat="server" Font-Size="12px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <label>Total Availed Amount</label></td>
                        <td></td>
                        <td>
                            <asp:Label ID="lblTotalAvailedAmt" runat="server" Font-Size="12px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <label>Balance Credit Amount</label></td>
                        <td></td>
                        <td>
                            <asp:Label ID="lblBalanceAmt" runat="server" Font-Size="12px"></asp:Label></td>
                    </tr>
                </table>
                <div class="clr"></div>
                <table>
                    <tbody>
                        <asp:Repeater ID="rptrCustomerCreditHistory" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td colspan="3" style="text-align: left; background-color: #7eb7df; color: white; padding-left: 5px; font-weight: bold; font-size: 18px;">Credit Details</td>
                                </tr>
                                <tr>
                                    <td style="width: 25%">
                                        <label>Credit Type</label></td>
                                    <td width="5%"></td>
                                    <td style="font-size: 12px;">
                                        <%# Eval("service_credit_abbr")%> - <%#Eval("service_credit_desc") %>
                                        <asp:HiddenField ID="hdnPkeyservice" runat="server" Value='<%# Eval("pkey_service_credit")%>' />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label>Date</label></td>
                                    <td></td>
                                    <td style="font-size: 12px;">
                                        <%# Eval("date_created","{0:MM/dd/yyyy}") %>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label>PO Number</label></td>
                                    <td></td>
                                    <td style="font-size: 12px;">
                                        <%#Eval("po_number").ToString() != "" ? Eval("po_number").ToString() : "-" %>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label>Credit Amount</label></td>
                                    <td></td>
                                    <td style="font-size: 12px;">
                                        <%# ConvertToMoneyFormat(Eval("credit_amount").ToString()) %>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label>Availed Amount</label></td>
                                    <td></td>
                                    <td style="font-size: 12px;">
                                        <%# ConvertToMoneyFormat(Eval("availed_amount").ToString()) %>
                                        <asp:HiddenField ID="hdnavailedamount" runat="server" Value='<%# Eval("availed_amount")%>' />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label>Is Active</label></td>
                                    <td></td>
                                    <td style="font-size: 12px;">
                                        <%# Eval("is_active") %>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="text-align: left; background-color: #7eb7df; color: white; padding-left: 5px; font-weight: bold; font-size: 18px;">PO Information</td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="text-align: left; background-color: #e4f3f9" id="tdRepeater" runat="server">
                                        <div class="clr"></div>
                                        <table class="tablep">
                                            <thead>
                                                <tr>
                                                    <th>PO Date</th>
                                                    <th>PO Number</th>
                                                    <th>Amount Applied</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="rptrCustomerPOList" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td>
                                                                <%# Eval("po_date","{0:MM/dd/yyyy}")%>
                                                            </td>
                                                            <td>
                                                                <%# Eval("po_number")%>
                                                            </td>
                                                            <td>
                                                                <%# ConvertToMoneyFormat(Eval("amount").ToString()) %>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td colspan="3" style="text-align: left;" id="tdNoData" runat="server" visible="false">Credit not yet availed by customer</td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>

                <div class="btn_lst">
                    <div class="rgt">
                        <asp:Button runat="server" ID="btnClose" Text="Close" CssClass="button rounded" OnClientClick="parent.$.fancybox.close(); return false;"></asp:Button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</body>
</html>
