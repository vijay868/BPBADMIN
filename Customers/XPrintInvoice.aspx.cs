﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Utilities;
using Bestprintbuy.Admin.Web.Common;

namespace Bestprintbuy.Admin.Web.Customers
{
    public partial class XPrintInvoice : PageBase
    {
        protected string StateCountry = "";
        protected bool rowExits = false;
        protected bool CartShipInfo = false;

        protected bool CartBillInfo = false;
        protected Int32 brpoKey;
        protected string EditKeys;
        protected string PoDtlKey;
        protected string poNumber;
        protected decimal m_cartCost;
        protected string m_timeStamp;
        protected Int32 nonFreeproducts;
        protected Int32 customerKey;

        protected string shipColor = "#000000";
        protected string m_Keys;
        protected string[] m_arrAllInvDetails;
        protected string[] m_arrInvDetails;

        protected string mCustomerPortalURL = "";
        protected CartModel m_CartData = new CartModel();

        protected void Page_Load(object sender, EventArgs e)
        {

            int qtyVariantKey = Convert.ToInt32(WebConfiguration.ProductVariants["Quantity"]);
            int paperVariantKey = Convert.ToInt32(WebConfiguration.ProductVariants["PaperType"]);
            mCustomerPortalURL = System.Configuration.ConfigurationManager.AppSettings["BPBCustomerBaseURL"];
            int intAllCounter = 0;
            if (Request.QueryString["ManifestPrint"] == null)
            {
                m_Keys = Request.QueryString["Keys"];
                m_arrAllInvDetails = m_Keys.Split('^');

                for (intAllCounter = 0; intAllCounter <= m_arrAllInvDetails.Length - 2; intAllCounter++)
                {
                    m_arrInvDetails = m_arrAllInvDetails[intAllCounter].Split(',');

                    if (!string.IsNullOrEmpty(m_arrInvDetails[0]))
                        EditKeys = m_arrInvDetails[0];
                    if (!string.IsNullOrEmpty(m_arrInvDetails[1]))
                        PoDtlKey = m_arrInvDetails[1];
                    if (!string.IsNullOrEmpty(m_arrInvDetails[2]))
                        customerKey = Convert.ToInt32(m_arrInvDetails[2]);
                    if (!string.IsNullOrEmpty(m_arrInvDetails[3]))
                        brpoKey = Convert.ToInt32(m_arrInvDetails[3]);

                    m_CartData = CartService.GetPrintInvoiceData(Convert.ToInt32(EditKeys), Convert.ToInt32(customerKey), qtyVariantKey, paperVariantKey, ConnString);

                    Print();
                    if (intAllCounter != m_arrAllInvDetails.Length - 2)
                        Response.Write("<BR CLASS=page>");
                }

            }
            else
            {
                string strFromDate = null;
                string strToDate = null;
                strFromDate = DateTime.Now.Month.ToString() + "/01/" + DateTime.Now.Year.ToString();



                if (System.DateTime.Now.Day < 10)
                {
                    strToDate = DateTime.Now.Month.ToString() + "/0" + DateTime.Now.Day.ToString() + "/" + DateTime.Now.Year.ToString();
                }
                else
                {
                    strToDate = DateTime.Now.ToShortDateString();
                }
                string searchString = "-1";
                if (Session["SearchCriteriaForOrderFulfilment"] != null)
                {
                    searchString = Convert.ToString(Session["SearchCriteriaForOrderFulfilment"]);
                }

                DataSet ds = CartService.GetExportOrderList(strFromDate, strToDate, searchString, "-1", "", "", "", "", false, false, false, false, false, true, ConnString);

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    m_arrInvDetails = dr["OrderKeys"].ToString().Split(',');

                    if (!string.IsNullOrEmpty(m_arrInvDetails[0]))
                        EditKeys = m_arrInvDetails[0];
                    if (!string.IsNullOrEmpty(m_arrInvDetails[1]))
                        PoDtlKey = m_arrInvDetails[1];
                    if (!string.IsNullOrEmpty(m_arrInvDetails[2]))
                        customerKey = Convert.ToInt32(m_arrInvDetails[2]);
                    if (!string.IsNullOrEmpty(m_arrInvDetails[3]))
                        brpoKey = Convert.ToInt32(m_arrInvDetails[3]);

                    m_CartData = CartService.GetPrintInvoiceData(Convert.ToInt32(EditKeys), Convert.ToInt32(customerKey), qtyVariantKey, paperVariantKey, ConnString);

                    Print();
                    Response.Write("<BR CLASS=page>");
                }

            }
        }

        protected string writeSelectedProducts()
        {
            Int32 l_iCounter = default(Int32);
            string strText = "";
            m_cartCost = 0;
            decimal productCost = default(decimal);
            decimal shipCost = default(decimal);
            decimal shipPromoCost = default(decimal);
            decimal productPromoCost = default(decimal);

            bool isFreeProdcut = false;
            decimal freeShipCost = default(decimal);
            string shipOption = "";
            decimal StateTaxOn = default(decimal);
            decimal TaxAmount = 0;
            string strFrontImage = null;
            string strBackImage = null;
            Int32 cardHeight = 122;
            Int32 cardWidth = 208;
            string strClass = "bodytext";
            string prodDetails = null;
            bool is_direct_mail = false;
            Int32 RowCount = 1;
            string BackText = string.Empty;
            string PCSize = string.Empty;

            decimal mcashipDiscount = default(decimal);
            decimal mcaproductDiscount = default(decimal);

            string frontlayout = string.Empty;
            Int32 fkeySite = 1;

            CartModel.CustomerCartRow custCartrow = (CartModel.CustomerCartRow)m_CartData.CustomerCart.Rows[0];
            CartModel.CartBillingInfoRow cartBillrow = (CartModel.CartBillingInfoRow)m_CartData.CartBillingInfo.Rows[0];
            CartModel.CartShipInfoRow shipInforow = (CartModel.CartShipInfoRow)m_CartData.CartShipInfo.Rows[0];
            for (l_iCounter = 0; l_iCounter <= m_CartData.CartDetails.Rows.Count - 1; l_iCounter++)
            {
                CartModel.CartDetailsRow CartDetailsrow = (CartModel.CartDetailsRow)m_CartData.CartDetails.Rows[l_iCounter];
                isFreeProdcut = CartDetailsrow.is_free;
                if (PoDtlKey == Convert.ToString(CartDetailsrow.pkey_cust_po_dtl))
                {
                    fkeySite = 1;
                    if (!CartDetailsrow.Isfkey_siteNull())
                    {
                        fkeySite = CartDetailsrow.fkey_site;
                        if (fkeySite == 2)
                        {
                            mCustomerPortalURL = System.Configuration.ConfigurationManager.AppSettings["BPBSMBCustomerBaseURL"];
                        }
                    }

                    prodDetails = CartDetailsrow.product_details;

                    if (prodDetails.IndexOf("Paper / Coating Type: ") > -1)
                    {
                        prodDetails = prodDetails.Replace("Paper / Coating Type: ", "Paper / Coating Type: <b>") + "</b>";
                    }

                    is_direct_mail = CartDetailsrow.is_direct_mail;
                    string cardsFor = CartDetailsrow.cards_for;

                    string sizekey = CartDetailsrow.size_key.ToString();

                    if (cardsFor == "PC" | cardsFor == "CM" | cardsFor == "SM")
                    {
                        frontlayout = CartDetailsrow.front_layout;
                        sizekey = CartDetailsrow.size_key.ToString(); ;
                        if (frontlayout.IndexOf("PCOVE") >= 0 | sizekey == "32")
                        {
                            cardHeight = 129;
                            cardWidth = 200;
                            PCSize = "Jumbo ";
                        }
                        else
                        {
                            cardHeight = 122;
                            cardWidth = 157;
                            PCSize = "Standard ";
                        }
                    }
                    else if (cardsFor == "FL")
                    {
                        cardHeight = 182;
                        cardWidth = 150;
                    }
                    else if (cardsFor == "DH")
                    {
                        cardHeight = 321;
                        cardWidth = 119;
                    }
                    else if (cardsFor == "BK")
                    {
                        cardHeight = 310;
                        cardWidth = 90;
                    }
                    else if (cardsFor == "NP" | cardsFor == "BCCM")
                    {
                        if (prodDetails.IndexOf("5.5") >= 0 | sizekey == "187")
                        {
                            cardHeight = 323;
                            cardWidth = 208;
                        }
                        else
                        {
                            cardHeight = 466;
                            cardWidth = 208;
                        }
                    }

                    strFrontImage = CartDetailsrow.low_res_proof_url;
                    strFrontImage = mCustomerPortalURL + strFrontImage.Substring(strFrontImage.IndexOf("/DesignCenter"));

                    strFrontImage = "<img src='" + strFrontImage + "' border=1 height=" + Convert.ToString(cardHeight) + " width=" + Convert.ToString(cardWidth) + ">";
                    if (!CartDetailsrow.Isreverse_layoutNull())
                    {
                        strBackImage = strFrontImage.Replace("_1.", "_2.");
                    }
                    if ((cardsFor == "BC" | cardsFor == "RCBC") & !string.IsNullOrEmpty(strBackImage))
                    {
                        BackText = " With Back";
                    }
                    else
                    {
                        BackText = "";
                    }
                    strClass = "bodytext";
                    if (!isFreeProdcut)
                    {
                        nonFreeproducts = nonFreeproducts + 1;
                        productCost = CartDetailsrow.price_per_unit;
                        shipOption = CartDetailsrow.ship_price_option;
                        string highliteColor = GetShipColorValue(shipOption, l_iCounter);
                        shipCost = shipCost + (CartDetailsrow.Isshipping_priceNull() ? 0 : CartDetailsrow.shipping_price);
                        m_cartCost = m_cartCost + productCost;
                        if (is_direct_mail == true)
                        {
                            productCost = productCost + shipCost;
                        }
                        strText = strText + "<TR vAlign='top' bgColor='#ffffff'>";
                        strText = strText + "    <TD class='" + strClass + "'><font color=" + highliteColor + " >" + Convert.ToString(RowCount) + "</font></TD>";
                        strText = strText + "    <TD class='" + strClass + "'><font color=" + highliteColor + " >" + CartDetailsrow.date_created + "</font></TD>";
                        strText = strText + "    <TD class='" + strClass + "'><font color=" + highliteColor + " ><B>" + PCSize + CartDetailsrow.product_id + BackText + "</B><BR>";
                        strText = strText + prodDetails + string.Format("</font><br><BR>{0}<BR><br>{1}</TD>", strFrontImage, strBackImage);
                        strText = strText + "    <TD class='" + strClass + "'>" + "<font color=" + highliteColor + " >" + CartDetailsrow.quantity + "</font></TD>";
                        strText = strText + "    <TD class='" + strClass + "' align='right'>" + "<font color=" + highliteColor + " >$" + Convert.ToString(productCost) + "</font> </TD>";
                        strText = strText + "</TR>";
                    }
                    else
                    {
                        freeShipCost = CartDetailsrow.shipping_price;
                        m_cartCost = m_cartCost + freeShipCost;
                        productCost = freeShipCost;

                        strText = strText + " <TR vAlign='top' bgColor='#ffffff'> ";
                        strText = strText + "    <TD class='" + strClass + "' rowSpan='2'>" + "<font color=green >" + Convert.ToString(RowCount) + "</font></TD> ";
                        strText = strText + "    <TD class='" + strClass + "' rowSpan='2'>" + "<font color=green >" + CartDetailsrow.date_created + "</font></TD>";
                        strText = strText + "    <TD class='" + strClass + "'><B>" + "<font color=green >" + PCSize + CartDetailsrow.product_id + BackText + " </B><BR>";
                        strText = strText + prodDetails + string.Format("</font><br>{0}<BR><br>{1}</TD>", strFrontImage, strBackImage);
                        strText = strText + "    <TD class='" + strClass + "'>" + "<font color=green >" + CartDetailsrow.quantity + "</font> </TD>";
                        strText = strText + "    <TD class='" + strClass + "'><font color=green >" + "Free" + "</font></TD>";
                        strText = strText + " </TR>";
                        strText = strText + " <TR vAlign='bottom' bgColor='#ffffff'> ";
                        strText = strText + "    <TD class='" + strClass + "' align='right' colSpan='2'>" + "<font color=green >" + "<B>Shipping and Handling Charges :</B></font></TD>";
                        strText = strText + "    <TD class='" + strClass + "' align='right'>" + "<font color=green >" + (freeShipCost > 0 ? ConvertToMoneyFormat(freeShipCost) : "Free") + "</font></TD>";
                        strText = strText + " </TR>";
                    }
                    RowCount = RowCount + 1;
                }
                else
                {
                    strClass = "bodytext";
                }
            }
            StateTaxOn = m_cartCost;
            m_cartCost = m_cartCost + shipCost;
            TaxAmount = StateTaxOn * GetStateTax();
            //TaxAmount = Convert.ToDouble(string.Format("{0:n2}", TaxAmount));


            if (nonFreeproducts > 0 & is_direct_mail != true)
            {
                strText = strText + "<TR bgColor='#ffffff'>";
                strText = strText + "    <TD class='bodytext' align='right' colSpan='4'><B>" + GetShipDisplayValue(shipOption, m_CartData.CartDetails.Rows.Count - 1) + ":</B></TD>";
                strText = strText + "    <TD class='bodytext' align='right'><B>" + (shipCost > 0 ? "$" + Convert.ToString(shipCost) : "Free") + "</B></TD>";
                strText = strText + "</TR>";
            }

            if (!cartBillrow.Isfkey_promoNull())
            {
                productPromoCost = cartBillrow.promo_product_discount;
                shipPromoCost = cartBillrow.promo_shipment_discount;

                if (productPromoCost > 0)
                {
                    strText = strText + " <TR vAlign='bottom' bgColor='#ffffff'> ";
                    strText = strText + "    <TD class='bodytext' align='right' colSpan='4'><B>Promotional discount on products :</B></TD>";
                    strText = strText + "    <TD class='bodytext' align='right'><B>" + ConvertToMoneyFormat(productPromoCost) + "</B></TD>";
                    strText = strText + " </TR>";
                    m_cartCost = m_cartCost - productPromoCost;
                    StateTaxOn = StateTaxOn - productPromoCost;
                    TaxAmount = StateTaxOn * GetStateTax();
                }
                if (shipPromoCost > 0)
                {
                    strText = strText + " <TR vAlign='bottom' bgColor='#ffffff'> ";
                    strText = strText + "    <TD class='bodytext' align='right' colSpan='4'><B>Promotional discount on shipment :</B></TD>";
                    strText = strText + "    <TD class='bodytext' align='right'><B>" + ConvertToMoneyFormat(shipPromoCost) + "</B></TD>";
                    strText = strText + " </TR>";
                    m_cartCost = m_cartCost - shipPromoCost;
                }
            }

            mcaproductDiscount = cartBillrow.mca_product_discount;
            mcashipDiscount = cartBillrow.mca_shipment_discount;
            if (mcaproductDiscount > 0)
            {
                strText = strText + " <TR vAlign='bottom' bgColor='#ffffff'> ";
                strText = strText + "    <TD class='bodytext' align='right' colSpan='4'><B>MCA discount on products :</B></TD>";
                strText = strText + "    <TD class='bodytext' align='right'><B>" + ConvertToMoneyFormat(mcaproductDiscount) + "</B></TD>";
                strText = strText + " </TR>";
                m_cartCost = m_cartCost - mcaproductDiscount;
                StateTaxOn = StateTaxOn - mcaproductDiscount;
                TaxAmount = StateTaxOn * GetStateTax();
            }
            if (mcashipDiscount > 0)
            {
                strText = strText + " <TR vAlign='bottom' bgColor='#ffffff'> ";
                strText = strText + "    <TD class='bodytext' align='right' colSpan='4'><B>MCA discount on shipment :</B></TD>";
                strText = strText + "    <TD class='bodytext' align='right'><B>" + ConvertToMoneyFormat(mcashipDiscount) + "</B></TD>";
                strText = strText + " </TR>";
                m_cartCost = m_cartCost - mcashipDiscount;
            }
            if (TaxAmount > 0)
            {
                strText = strText + " <TR vAlign='bottom' bgColor='#ffffff'> ";
                strText = strText + "    <TD class='bodytext' align='right' colSpan='4'><B>" + WebConfiguration.StateTaxLabel + " :</B></TD>";
                strText = strText + "    <TD class='bodytext' align='right'><B>" + ConvertToMoneyFormat(TaxAmount) + "</B></TD>";
                strText = strText + " </TR>";
                m_cartCost = m_cartCost + TaxAmount;
            }
            return strText;
        }

        private decimal GetStateTax()
        {
            int fkey_state;
            double tax = 0;
            try
            {
                if (m_CartData.CartShipInfo.Rows.Count > 0)
                {
                    fkey_state = Convert.ToInt32(m_CartData.CartShipInfo.Rows[0]["fkey_state"]);
                    var list = (from collect in m_CartData.States
                                where collect.pkey_state == fkey_state
                                select collect).ToList();
                    tax = WebConfiguration.GetApplicableStateTax(list[0].state_name);
                    tax = tax / 100;
                }
            }
            catch
            {
                return Convert.ToDecimal(tax);
            }
            return Convert.ToDecimal(tax);
        }

        protected string GetShipColorValue(string key, Int32 l_iIndex)
        {
            string strDate = "green";
            shipColor = "green";
            try
            {
                switch (key)
                {
                    case "PRICE_ONE":
                        shipColor = "red";
                        return "red";
                    case "PRICE_TWO":
                        shipColor = "blue";
                        return "blue";
                    case "PRICE_SEVEN":
                        shipColor = "black";
                        return "black";
                }
            }
            catch (Exception)
            {
                strDate = "";
            }
            return strDate;
        }

        private string GetShipDisplayValue(string shipOption, int l_iIndex)
        {
            string strDate = "";
            try
            {
                strDate = shipOption;
                switch (strDate)
                {
                    case "PRICE_ONE":
                        return "Shipping (Express 1 day air)";
                    case "PRICE_TWO":
                        return "Shipping (Priority 2-3 Days)";
                    case "PRICE_SEVEN":
                        return "Shipping (Regular 4-5 day ground)";
                    case "PRICE_FOURTEEN":
                        return "Shipping (Standard Ground 6-10 business days)";
                    default:
                        return "Shipping (Standard Ground 6-10 business days)";
                }
            }
            catch (Exception)
            {
                strDate = "";
            }
            return strDate;
        }

        private void Print()
        {
            CartModel.CartBillingInfoRow cartBillrow = (CartModel.CartBillingInfoRow)m_CartData.CartBillingInfo.Rows[0];
            CartModel.CartShipInfoRow shipInforow = (CartModel.CartShipInfoRow)m_CartData.CartShipInfo.Rows[0];
            CartModel.CustomerCartRow custCartrow = (CartModel.CustomerCartRow)m_CartData.CustomerCart.Rows[0];
            Response.Write("<TABLE cellSpacing='0' cellPadding='1' width='750' bgColor='#00526e' border='0'>");
            Response.Write("<TBODY>");
            Response.Write("<TR>");
            Response.Write("<TD>");
            Response.Write("<TABLE cellSpacing='0' cellPadding='0' width='100%' bgColor='#ffffff' border='0'>");
            Response.Write("<TBODY>");
            Response.Write("<TR>");
            Response.Write("<TD><!-- Header -->");
            Response.Write("<TABLE cellSpacing='0' cellPadding='10' width='100%' border='0'>");
            Response.Write("<TBODY>");
            Response.Write("<TR>");
            Response.Write("<TD><IMG height='30' alt='BestPrint Buy.com' src='../Images/bpb_logo_Black.png' width='210' border='0'></TD>");
            Response.Write("<TD class='bodytext' vAlign='top' align='right'>&nbsp;<BR>");
            Response.Write("<BR>");
            Response.Write("&nbsp;</TD>");
            Response.Write("</TR>");
            Response.Write("</TBODY>");
            Response.Write("</TABLE> <!-- /Header --> <!-- Invoice -->");
            Response.Write("<TABLE cellSpacing='10' cellPadding='0' width='100%' border='0'>");
            Response.Write("<TBODY>");
            Response.Write("<TR>");
            Response.Write("<TD>");
            Response.Write("<TABLE cellSpacing='0' cellPadding='0' width='100%' border='0'>");
            Response.Write("<TBODY>");
            Response.Write("<TR>");
            Response.Write("<TD><!-- address & order ID & Print Button -->");
            Response.Write("<TABLE cellSpacing='0' cellPadding='5' width='100%' border='0'>");
            Response.Write("<TBODY>");
            Response.Write("<TR>");
            Response.Write("<TD class='bodytext' align='center' colSpan='3'>");
            Response.Write("<HR SIZE='1'>");
            Response.Write("S H O P &nbsp; &nbsp; F L O O R &nbsp; &nbsp; I N V O I C E");
            Response.Write("<HR SIZE='1'>");
            Response.Write("</TD>");
            Response.Write("<TR>");
            Response.Write("<TD class='bodytext' vAlign='top'><B>Bill to:</B><BR>");

            Response.Write(cartBillrow.bill_first_name + "&nbsp;" + cartBillrow.bill_last_name);
            Response.Write("<BR>");

            Response.Write(cartBillrow.bill_company_name);
            Response.Write("<BR>");
            Response.Write(cartBillrow.bill_address1);
            Response.Write("<BR>");
            Response.Write(cartBillrow.bill_address2);
            Response.Write("<BR>");
            Response.Write(cartBillrow.bill_city);
            Response.Write("&nbsp;");
            Response.Write(cartBillrow.bill_zipcode);
            Response.Write("<BR>");
            Response.Write(GetState(cartBillrow.fkey_bill_state.ToString()));
            Response.Write("&nbsp;");
            Response.Write(GetCountry(cartBillrow.fkey_bill_country.ToString()));
            Response.Write("</TD>");

            Response.Write("<TD class='bodytext' vAlign='top'><B>Ship to:</B><BR>");
            Response.Write(shipInforow.ship_first_name);
            Response.Write("&nbsp;");
            Response.Write(shipInforow.ship_last_name);
            Response.Write("<BR>");

            Response.Write(shipInforow.ship_company_name);
            Response.Write("<BR>");
            Response.Write(shipInforow.shipment_address1);
            Response.Write("<BR>");
            Response.Write(shipInforow.shipment_address2);
            Response.Write("<BR>");
            Response.Write(shipInforow.city);
            Response.Write("&nbsp;" + shipInforow.zipcode + "<BR>");
            Response.Write(GetState(shipInforow.fkey_state.ToString()));
            Response.Write("&nbsp;" + GetCountry(shipInforow.fkey_country.ToString()) + "</TD>");

            Response.Write("<TD align='right'><span class='bodytext'>Date: <B>" + custCartrow.po_date + "</span></B>");
            Response.Write("<BR><span class='bodytext'>Shop Floor Invoice ID:</span><BR><span id='barCodeId' style='FONT-SIZE: 8pt; FONT-FAMILY: IDAutomationHC39M;COLOR:#000000'>*" + custCartrow.po_number + "-" + Convert.ToString(brpoKey) + "*</span>");
            Response.Write("</TD>");
            Response.Write("</TR>");
            Response.Write("</TBODY>");
            Response.Write("</TABLE> <!-- address & order ID & Print Button -->");
            Response.Write("<TR>");
            Response.Write("<TD class='bodytext' vAlign='top'>");
            Response.Write("<TABLE cellSpacing='0' cellPadding='0' width='100%' bgColor='#333333' border='0'>");
            Response.Write("<TBODY>");
            Response.Write("<TR>");
            Response.Write("<TD>");
            Response.Write("<TABLE cellSpacing='1' cellPadding='5' width='100%' border='0'>");
            Response.Write("<TBODY>");
            Response.Write("<TR bgColor='#ffffff'>");
            Response.Write("<TD class='bodytext'><B>#.</B></TD>");
            Response.Write("<TD class='bodytext'><B>Date</B></TD>");
            Response.Write("<TD class='bodytext'><B>Product</B></TD>");
            Response.Write("<TD class='bodytext'><B>Quantity</B></TD>");
            Response.Write("<TD class='bodytext'><B>Value</B></TD>");
            Response.Write("</TR>");

            Response.Write(writeSelectedProducts());

            Response.Write("<TR bgColor='#ffffff'>");
            Response.Write("<TD class='bodytext' align='right' colSpan='4'><B>Total for All Items :</B></TD>");
            Response.Write("<TD class='bodytext' align='right'><B>" + ConvertToMoneyFormat(m_cartCost) + "</B></TD>");
            Response.Write("</TR>");
            Response.Write("</TBODY>");
            Response.Write("</TABLE>");
            Response.Write("</TD>");
            Response.Write("</TR>");
            Response.Write("</TBODY>");
            Response.Write("</TABLE>");
            Response.Write("<P><!-- /invoice --> <!-- Bottom Navigation --></P>");
            Response.Write("</TD>");
            Response.Write("</TR>");
            Response.Write("</TBODY>");
            Response.Write("</TABLE>");

            Response.Write("<!-- Bottom Navigation -->");
            Response.Write("<TABLE width='100%' cellpadding=0 cellspacing=0 border=0>");
            Response.Write("<TR>");
            Response.Write("<TD align=right><IMG SRC='../Images/bpb_logo_Black.png' WIDTH='106' HEIGHT='15' BORDER='0' ALT='' hspace=10></TD>");
            Response.Write("</TR>");
            Response.Write("<TR>");
            Response.Write("<TD background='../images/admin/botbg.gif' height='23' align=center class=bodytext><FONT color='#ffffff'>&copy; 2004, BestPrintBuy.com</FONT></TD>");
            Response.Write("</TR>");            
            Response.Write("</TABLE>");
            Response.Write("<!-- /Bottom Navigation --><!-- /Bottom Navigation -->");
            Response.Write("<P></P>");
            Response.Write("</TD>");
            Response.Write("</TR>");
            Response.Write("</TBODY>");
            Response.Write("</TABLE>");
            Response.Write("</TD>");
            Response.Write("</TR>");
            Response.Write("</TBODY>");
            Response.Write("</TABLE> <!-- /Master Table --></TD>");
            Response.Write("</TR>");
            Response.Write("</TBODY>");
            Response.Write("</TABLE>");
            Response.Write("</TD></TR></TBODY></TABLE></TR></TBODY></TABLE>");
        }


        private string GetCountry(string fkeyCountry)
        {
            if (m_CartData.Countries.Rows.Count > 0)
            {
                StateCountry = "";
                var list = (from collect in m_CartData.Countries
                            where collect.pkey_country == Convert.ToInt32(fkeyCountry)
                            select collect).ToList();
                StateCountry = list[0].country_name;
            }
            return StateCountry;
        }
        private string GetState(string fkeyState)
        {
            if (m_CartData.States.Rows.Count > 0)
            {
                StateCountry = "";
                var list = (from collect in m_CartData.States
                            where collect.pkey_state == Convert.ToInt32(fkeyState)
                            select collect).ToList();
                StateCountry = list[0].state_name;
            }
            return StateCountry;
        }
    }
}