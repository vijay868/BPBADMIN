﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.IO;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.Customers
{
    public partial class ExportOrders : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Put user code to initialize the page here
                string strFromDate = Request["fromDate"] != null ? Request["fromDate"].Trim() : Request["fromDate"];
                string strToDate = Request["toDate"] + " 23:59";
                string shipOption = Request["ship"];
                if ((Request["ExitTransactions"] != null))
                {
                    ExportExitTransactions(strFromDate, strToDate);
                    return;
                }
                if ((Request["StockImages"] != null))
                {
                    ExportStockPurchasedHistory(strFromDate, strToDate);
                    return;
                }
                if (Request["PowerUser"] != null)
                {
                    Int32 orders = Convert.ToInt32(Request["nooforders"]);
                    bool IsCsv = Convert.ToBoolean(Request["iscsv"]);
                    Int32 months = Convert.ToInt32(Request["months"]);
                    ExportPowerUserList(strFromDate, orders, IsCsv, strToDate, months);
                    return;
                }

                if ((Request["Leads"] != null))
                {
                    ExportLeads(strFromDate, strToDate);
                    return;
                }

                if ((Request["CustomerCredit"] != null))
                {
                    ExportCustomerCreditSummary();
                    return;
                }

                if ((Request["EmailCampaignData"] != null))
                {
                    ExportEmailCampaignData();
                    return;
                }

                if ((Request["Royalties"] != null))
                {
                    ExportRoyaltiesTransactions(strFromDate, Request["toDate"], Request["Royalties"]);
                    return;
                }
                if ((Request["MCACustomerList"] != null))
                {
                    ExportMCACustomerList();
                    return;
                }
                if ((Request["TransReport"] != null))
                {
                    Int32 reportType = Convert.ToInt32(Request["TransReport"]);
                    Int32 kioskKey = Convert.ToInt32(Request["cboGetkiosk"]);
                    if (reportType == 1)
                    {
                        ExportKioskTransactionsReport(strFromDate, strToDate, reportType, kioskKey);
                    }
                    else
                    {
                        ExportTransactionsReport(strFromDate, strToDate, reportType, kioskKey);
                    }

                    return;
                }
                if ((Request["Promotions"] != null))
                {
                    ExportPromoList(strFromDate, strToDate, Convert.ToInt32(Request["promokey"]));
                    return;
                }

                string userPath = string.Empty;
                string userServerPath = string.Empty;
                string pdfPath = string.Empty;
                string pdfServerPath = string.Empty;
                //The above needs to be updated with tomcat and user proof paths if sanjeev asks for complete url in export




                bool isWHUser = Identity.isWareHouseUser;
                bool isSupplierManifest = false;
                bool isSupplierManifestProdCode = false;
                bool isSupplierManifestAuditProdCode = false;
                bool isShipManifest = false;

                if ((Request["Action"] != null) && !string.IsNullOrEmpty(Request["Action"]))
                {
                    if (Request["Action"] == "ShipManifest")
                    {
                        isShipManifest = true;
                    }
                    else if (Request["Action"] == "SupManifestPCode")
                    {
                        isSupplierManifestProdCode = true;
                    }
                    else if (Request["Action"] == "SupManifestPCodeAudit")
                    {
                        isSupplierManifestAuditProdCode = true;
                    }
                    else
                    {
                        isSupplierManifest = true;
                    }
                }

                string searchString = "-1";
                if (Session["SearchCriteriaForOrderFulfilment"] != null)
                {
                    searchString = Convert.ToString(Session["SearchCriteriaForOrderFulfilment"]);
                }
                DataSet ds = SupplierPOService.GetExportOrderList(strFromDate, strToDate, shipOption, userPath, userServerPath, pdfPath, pdfServerPath, searchString,
                    isWHUser, isSupplierManifest, isShipManifest, isSupplierManifestProdCode, isSupplierManifestAuditProdCode, false, ConnString);

                if ((ds != null))
                {
                    if (isSupplierManifest | isSupplierManifestProdCode | isSupplierManifestAuditProdCode)
                    {
                        ExportDataToCSVFile(ds.Tables[0]);
                    }
                    else
                    {
                        grdOrders.Visible = true;
                        Int32 rowcount = ds.Tables[0].Rows.Count;
                        grdOrders.DataSource = ds.Tables[0];
                        grdOrders.DataBind();
                        string fileName = strFromDate + "-" + shipOption + "-" + "Orders.xls";
                        Response.Buffer = true;
                        Response.ContentType = "application/vnd.ms-excel";
                        Response.AddHeader("content-disposition", "inline; filename=ExportOrders.xls");
                        System.IO.StringWriter oStringWriter = new System.IO.StringWriter();
                        System.Web.UI.HtmlTextWriter oHtmlTextWriter = new System.Web.UI.HtmlTextWriter(oStringWriter);
                        grdOrders.RenderControl(oHtmlTextWriter);
                        Response.Write(oStringWriter.ToString());
                        Response.End();
                    }
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        private void ExportTransactionsReport(string fromDate, string toDate, Int32 reportType, Int32 kioskKey)
        {
            SupplierPOModel poModel = new SupplierPOModel();
            double mTransCost = 0.0;
            double mPrintCost = 0.0;
            double mShipCost = 0.0;
            double mDMCost = 0.0;
            double mStateTax = 0.0;

            poModel = SupplierPOService.GetKIOSKOrderSatistics(Convert.ToDateTime(fromDate), Convert.ToDateTime(toDate), kioskKey, reportType, ConnString);
            if (poModel.TransactionsReport.Rows.Count > 0)
            {
                grdTransactions.Visible = true;
                grdTransactions.DataSource = poModel.TransactionsReport;
                grdTransactions.DataBind();

                foreach (SupplierPOModel.TransactionsReportRow orRow in poModel.TransactionsReport.Rows)
                {
                    mTransCost = mTransCost + orRow.transaction_amount;
                    mPrintCost = mPrintCost + orRow.price_per_unit;
                    mShipCost = mShipCost + orRow.ship_cost;
                    mDMCost = mDMCost + orRow.dm_cost;
                    mStateTax = mStateTax + orRow.po_dtl_state_tax;
                }

                DataSet ds = new DataSet();
                DataTable dt = new DataTable("Totals");
                dt.Columns.Add(new DataColumn("Column1"));
                dt.Columns.Add(new DataColumn("Column2"));
                dt.Columns.Add(new DataColumn("Column3"));
                dt.Columns.Add(new DataColumn("Column4"));
                dt.Columns.Add(new DataColumn("Column5"));
                dt.Columns.Add(new DataColumn("Column6"));
                dt.Columns.Add(new DataColumn("Column7"));
                dt.Columns.Add(new DataColumn("Column8"));
                dt.Columns.Add(new DataColumn("Column9"));
                ds.Tables.Add(dt);
                DataRow row = ds.Tables["Totals"].NewRow();
                row["Column1"] = "";
                row["Column2"] = "";
                row["Column3"] = "";
                row["Column4"] = "Totals";
                row["Column5"] = CurrencyUtils.ToMoneyFormat(mTransCost);
                row["Column6"] = CurrencyUtils.ToMoneyFormat(mPrintCost);
                row["Column7"] = CurrencyUtils.ToMoneyFormat(mShipCost);
                row["Column8"] = CurrencyUtils.ToMoneyFormat(mDMCost);
                row["Column9"] = CurrencyUtils.ToMoneyFormat(mStateTax);


                dt.Rows.Add(row);
                grdTotals.Visible = true;
                grdTotals.DataSource = ds.Tables["Totals"];
                grdTotals.DataBind();

                string fileName = fromDate + "-" + toDate + "_" + "Orders.xls";
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
                System.IO.StringWriter oStringWriter = new System.IO.StringWriter();
                System.Web.UI.HtmlTextWriter oHtmlTextWriter = new System.Web.UI.HtmlTextWriter(oStringWriter);
                grdTransactions.RenderControl(oHtmlTextWriter);
                //Response.Write(oStringWriter.ToString())

                //oStringWriter = New System.IO.StringWriter
                //oHtmlTextWriter = New System.Web.UI.HtmlTextWriter(oStringWriter)
                grdTotals.RenderControl(oHtmlTextWriter);
                Response.Write(oStringWriter.ToString());

                Response.End();
            }
        }

        private void ExportRoyaltiesTransactions(string fromDate, string toDate, string strFor)
        {
            SupplierPOModel poModel = new SupplierPOModel();
            decimal mTotalCost = 0;


            string searchString = "";
            int fkey_template_type = -1;
            if (Request["cboArtist"] != null)
            {
                fkey_template_type = Convert.ToInt16(Request["cboArtist"]);
            }
            searchString = "  pps_cust_po_master.po_date between '" + fromDate + "' and '" + toDate + " 23:59' ";
            int Categorykey = Convert.ToInt32(WebConfiguration.ProductTypeKeys["Humor Series By Artist"]);
            if (fkey_template_type == -2)
            {
                poModel = SupplierPOService.GetCardSatistics(Convert.ToDateTime(fromDate + " 00:00"),
                    Convert.ToDateTime(toDate + " 23:59"), Categorykey, Identity.UserPK, ConnString);
            }
            else
            {
                poModel = SupplierPOService.GetOrdersByArtist(Categorykey, fkey_template_type, searchString, Identity.UserPK, ConnString);
            }

            if (poModel.OrdersByArtist.Rows.Count > 0)
            {
                grdRoyaltiesOrders.Visible = true;
                grdRoyaltiesOrders.DataSource = poModel.OrdersByArtist;
                grdRoyaltiesOrders.DataBind();

                foreach (SupplierPOModel.OrdersByArtistRow orRow in poModel.OrdersByArtist.Rows)
                {
                    mTotalCost = mTotalCost + orRow.netamount;
                }

                DataSet ds = new DataSet();
                DataTable dt = new DataTable("Totals");
                dt.Columns.Add(new DataColumn("Column1"));
                dt.Columns.Add(new DataColumn("Column2"));
                dt.Columns.Add(new DataColumn("Column3"));
                dt.Columns.Add(new DataColumn("Column4"));
                dt.Columns.Add(new DataColumn("Column5"));
                dt.Columns.Add(new DataColumn("Column6"));
                ds.Tables.Add(dt);
                DataRow row = ds.Tables["Totals"].NewRow();
                row["Column1"] = "";
                row["Column2"] = "";
                row["Column3"] = "";
                row["Column4"] = "";
                row["Column5"] = "Total :";
                row["Column6"] = CurrencyUtils.ToMoneyFormat(mTotalCost);

                dt.Rows.Add(row);
                grdTotals.Visible = true;
                grdTotals.DataSource = ds.Tables["Totals"];
                grdTotals.DataBind();

                string fileName = fromDate + "-" + toDate + "_" + "Orders.xls";
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
                System.IO.StringWriter oStringWriter = new System.IO.StringWriter();
                System.Web.UI.HtmlTextWriter oHtmlTextWriter = new System.Web.UI.HtmlTextWriter(oStringWriter);
                grdRoyaltiesOrders.RenderControl(oHtmlTextWriter);
                //Response.Write(oStringWriter.ToString())

                //oStringWriter = New System.IO.StringWriter
                //oHtmlTextWriter = New System.Web.UI.HtmlTextWriter(oStringWriter)
                grdTotals.RenderControl(oHtmlTextWriter);
                Response.Write(oStringWriter.ToString());

                Response.End();
            }
        }

        private void ExportExitTransactions(string fromDate, string toDate)
        {

            SupplierPOModel poModel = new SupplierPOModel();
            decimal mTotalCost = 0;
            poModel = SupplierPOService.GetExitTransactions(Convert.ToDateTime(fromDate + " 00:00"), Convert.ToDateTime(toDate), ConnString);
            if (poModel.ExitTransactions.Rows.Count > 0)
            {
                grdExitOrders.Visible = true;
                grdExitOrders.DataSource = poModel.ExitTransactions;
                grdExitOrders.DataBind();

                foreach (SupplierPOModel.ExitTransactionsRow orRow in poModel.ExitTransactions.Rows)
                {
                    mTotalCost = mTotalCost + orRow.price_per_unit;
                }

                DataSet ds = new DataSet();
                DataTable dt = new DataTable("Totals");
                dt.Columns.Add(new DataColumn("Column1"));
                dt.Columns.Add(new DataColumn("Column2"));
                dt.Columns.Add(new DataColumn("Column3"));
                dt.Columns.Add(new DataColumn("Column4"));
                dt.Columns.Add(new DataColumn("Column5"));
                ds.Tables.Add(dt);
                DataRow row = ds.Tables["Totals"].NewRow();
                row["Column1"] = "TOTAL SALES :";
                row["Column2"] = "";
                row["Column3"] = "";
                row["Column4"] = CurrencyUtils.ToMoneyFormat(mTotalCost);
                row["Column5"] = "$0.00";
                dt.Rows.Add(row);
                grdTotals.Visible = true;
                grdTotals.DataSource = ds.Tables["Totals"];
                grdTotals.DataBind();


                string fileName = fromDate + "-" + toDate + "-" + "ExitOrders.xls";
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("content-disposition", "inline; filename=ExitApprovedSupplierTransactions.xls");
                //"attachment; filename=" + fileName)
                System.IO.StringWriter oStringWriter = new System.IO.StringWriter();
                System.Web.UI.HtmlTextWriter oHtmlTextWriter = new System.Web.UI.HtmlTextWriter(oStringWriter);
                grdExitOrders.RenderControl(oHtmlTextWriter);
                grdTotals.RenderControl(oHtmlTextWriter);
                Response.Write(oStringWriter.ToString());

                Response.End();
            }
        }

        private void ExportStockPurchasedHistory(string fromDate, string toDate)
        {

            SupplierPOModel poModel = new SupplierPOModel();
            poModel = SupplierPOService.GetStockPurchaseHistory(Convert.ToDateTime(fromDate + " 00:00"), Convert.ToDateTime(toDate), ConnString);
            if (poModel.StockImages.Rows.Count > 0)
            {
                grdStockImgPurchasedHistory.Visible = true;
                grdStockImgPurchasedHistory.DataSource = poModel.StockImages;
                grdStockImgPurchasedHistory.DataBind();

                string fileName = fromDate + "-" + toDate + "-" + "StockImagesPurchasedHistory.xls";
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("content-disposition", "inline; filename=StockImagesPurchasedHistory.xls");
                //"attachment; filename=" + fileName)
                System.IO.StringWriter oStringWriter = new System.IO.StringWriter();
                System.Web.UI.HtmlTextWriter oHtmlTextWriter = new System.Web.UI.HtmlTextWriter(oStringWriter);
                grdStockImgPurchasedHistory.RenderControl(oHtmlTextWriter);
                Response.Write(oStringWriter.ToString());

                Response.End();
            }
        }

        private void ExportCustomerCreditSummary()
        {
            ServiceCreditsModel creditModel = new ServiceCreditsModel();
            creditModel = CustomerService.GetCustomerCreditsSummary(ConnString);
            if (creditModel.CustomerCredit.Rows.Count > 0)
            {
                grdCreditSummary.Visible = true;
                grdCreditSummary.DataSource = creditModel.CustomerCredit;
                grdCreditSummary.DataBind();

                string fileName = "CustomerCreditSummary.xls";
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("content-disposition", "inline; filename=" + fileName);
                System.IO.StringWriter oStringWriter = new System.IO.StringWriter();
                System.Web.UI.HtmlTextWriter oHtmlTextWriter = new System.Web.UI.HtmlTextWriter(oStringWriter);
                grdCreditSummary.RenderControl(oHtmlTextWriter);
                Response.Write(oStringWriter.ToString());

                Response.End();
            }
        }


        private void ExportKioskTransactionsReport(string fromDate, string toDate, Int32 reportType, Int32 kioskKey)
        {

            SupplierPOModel poModel = new SupplierPOModel();
            poModel = SupplierPOService.GetKIOSKOrderSatistics(Convert.ToDateTime(fromDate + " 00:00"), Convert.ToDateTime(toDate), kioskKey, reportType, ConnString);
            if (poModel.kiosk_order_statistics.Rows.Count > 0)
            {
                grdKioskTransactions.Visible = true;
                grdKioskTransactions.DataSource = poModel.kiosk_order_statistics;
                grdKioskTransactions.DataBind();

                string fileName = fromDate + "-" + toDate + "-" + "Orders.xls";
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
                System.IO.StringWriter oStringWriter = new System.IO.StringWriter();
                System.Web.UI.HtmlTextWriter oHtmlTextWriter = new System.Web.UI.HtmlTextWriter(oStringWriter);
                grdKioskTransactions.RenderControl(oHtmlTextWriter);
                Response.Write(oStringWriter.ToString());

                Response.End();
            }
        }


        private void ExportLeads(string fromDate, string toDate)
        {
            CustomerModel poModel = new CustomerModel();
            poModel = CustomerService.GetCustomerLeads(Convert.ToDateTime(fromDate + " 00:00"), Convert.ToDateTime(toDate), ConnString);
            if (poModel.CustomerLeads.Rows.Count > 0)
            {
                grdLeads.Visible = true;
                grdLeads.DataSource = poModel.CustomerLeads;
                grdLeads.DataBind();

                string fileName = fromDate + "-" + toDate + "-" + "Orders.xls";
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
                System.IO.StringWriter oStringWriter = new System.IO.StringWriter();
                System.Web.UI.HtmlTextWriter oHtmlTextWriter = new System.Web.UI.HtmlTextWriter(oStringWriter);
                grdLeads.RenderControl(oHtmlTextWriter);
                Response.Write(oStringWriter.ToString());

                Response.End();
            }
        }

        private void ExportEmailCampaignData()
        {
            CustomerModel poModel = new CustomerModel();

            if ((Request["CSListFor"] != null))
            {
                poModel = CustomerService.GetCustomerStatistics(Request["CSListFor"], ConnString);
            }
            else if ((Request["pkey_email_campaign"] != null))
            {
                poModel = CustomerService.GetEmailCampaignDetails(Convert.ToInt32(Request["pkey_email_campaign"]), ConnString);
            }


            if (poModel.EmailCustomerInfo.Rows.Count > 0)
            {
                grdMailCampaign.Visible = true;
                grdMailCampaign.DataSource = poModel.EmailCustomerInfo;
                grdMailCampaign.DataBind();

                string fileName = "ExportCustomers.xls";
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
                System.IO.StringWriter oStringWriter = new System.IO.StringWriter();
                System.Web.UI.HtmlTextWriter oHtmlTextWriter = new System.Web.UI.HtmlTextWriter(oStringWriter);
                grdMailCampaign.RenderControl(oHtmlTextWriter);
                Response.Write(oStringWriter.ToString());

                Response.End();
            }
        }

        private void ExportDataToCSVFile(DataTable dtMaster)
        {

            try
            {

                DataRow[] drs = null;
                drs = dtMaster.Select();
                if ((dtMaster.Rows.Count <= 0))
                {
                    return;
                }


                Int32 excludeCols = 0;
                StringBuilder sb = new StringBuilder();
                Int32 i = 0;

                foreach (DataColumn dc in dtMaster.Columns)
                {
                    sb.AppendFormat("{0}{1}", dc.ColumnName, (i == dtMaster.Columns.Count - 1 ? Environment.NewLine : ","));
                    i = i + 1;
                }

                foreach (DataRow dr in dtMaster.Rows)
                {
                    i = 0;
                    foreach (DataColumn dc in dtMaster.Columns)
                    {
                        sb.AppendFormat("\"{0}\"{1}", dr[dc.ColumnName].ToString().Replace("\"", "\"\""), (i == dtMaster.Columns.Count - 1 ? Environment.NewLine : ","));
                        i = i + 1;
                    }

                }


                Response.Write(sb.ToString());
                string strFileName = null;
                strFileName = string.Format("SuplierManifest_{0}", DateTime.Now.ToString("MMddyyyy"));
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AppendHeader("content-disposition", "attachment; filename=" + strFileName + ".csv");
                //Response.AddHeader("content-disposition", "attachment; filename=" + strFileName + ".csv") 'attachment; filename=" + strFileName)
                StringWriter oStringWriter = new StringWriter();
                HtmlTextWriter oHtmlTextWriter = new HtmlTextWriter(oStringWriter);
                Response.Write(oStringWriter.ToString());
                Response.End();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ExportMCACustomerList()
        {
            CustomerModel.CustomersDataTable customermodel = new CustomerModel.CustomersDataTable();
            customermodel = UserService.GetMCACustomersList("-1", ConnString);
            if (customermodel.Rows.Count > 0)
            {
                grdCustomersList.Visible = true;
                grdCustomersList.DataSource = customermodel;
                grdCustomersList.DataBind();

                string fileName = "MCACustomerList.xls";
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
                System.IO.StringWriter oStringWriter = new System.IO.StringWriter();
                System.Web.UI.HtmlTextWriter oHtmlTextWriter = new System.Web.UI.HtmlTextWriter(oStringWriter);
                grdCustomersList.RenderControl(oHtmlTextWriter);
                Response.Write(oStringWriter.ToString());

                Response.End();


            }
        }

        private void ExportPromoList(string fromDate, string toDate, Int32 promokey)
        {
            decimal ActualPrice = 0;
            decimal DiscountPrice = 0;
            PromotionModel poModel = new PromotionModel();
            //poModel = m_Processor.GetPOPromoStats(Convert.ToDateTime(fromDate + " 00:00"), Convert.ToDateTime(toDate + " 23:59"), promokey)
            poModel = SupplierPOService.GetPOPromoStats(Convert.ToDateTime(fromDate + " 00:00"), Convert.ToDateTime(toDate), promokey, ConnString);
            if (poModel.POPromoStats.Rows.Count > 0)
            {
                grdPromotions.Visible = true;
                grdPromotions.DataSource = poModel.POPromoStats;
                grdPromotions.DataBind();
                foreach (PromotionModel.POPromoStatsRow poRow in poModel.POPromoStats.Rows)
                {
                    ActualPrice = ActualPrice + poRow.po_net_amount;
                    DiscountPrice = DiscountPrice + poRow.net_promo_discount;
                }

                DataSet ds = new DataSet();
                DataTable dt = new DataTable("Totals");
                dt.Columns.Add(new DataColumn("Column1"));
                dt.Columns.Add(new DataColumn("Column2"));
                dt.Columns.Add(new DataColumn("Column3"));
                dt.Columns.Add(new DataColumn("Column4"));
                dt.Columns.Add(new DataColumn("Column5"));
                dt.Columns.Add(new DataColumn("Column6"));
                ds.Tables.Add(dt);
                DataRow row = ds.Tables["Totals"].NewRow();
                row["Column1"] = "";
                row["Column2"] = "";
                row["Column3"] = "";
                row["Column4"] = "Total :";
                row["Column5"] = CurrencyUtils.ToMoneyFormat(ActualPrice);
                row["Column6"] = CurrencyUtils.ToMoneyFormat(DiscountPrice);

                dt.Rows.Add(row);
                grdTotals.Visible = true;
                grdTotals.DataSource = ds.Tables["Totals"];
                grdTotals.DataBind();
                string fileName = fromDate + "-" + toDate + "-" + "PromoList.xls";
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
                System.IO.StringWriter oStringWriter = new System.IO.StringWriter();
                System.Web.UI.HtmlTextWriter oHtmlTextWriter = new System.Web.UI.HtmlTextWriter(oStringWriter);
                grdPromotions.RenderControl(oHtmlTextWriter);
                grdTotals.RenderControl(oHtmlTextWriter);
                Response.Write(oStringWriter.ToString());

                Response.End();
            }
        }


        private void ExportPowerUserList(string fromDate, Int32 NoofOders, bool IsCsv, string toDate, Int32 months)
        {
            CustomerModel customermodel = new CustomerModel();
            bool option2 = Convert.ToBoolean(Request["option2"]);

            if (option2)
            {
                customermodel = CustomerService.GetPowerUsersList(Convert.ToDateTime(fromDate + " 00:00"), Convert.ToDateTime(toDate + " 00:00"), months, NoofOders, ConnString);
            }
            else
            {
                customermodel = CustomerService.GetPowerUsersList(Convert.ToDateTime(fromDate + " 00:00"), NoofOders, ConnString);
            }

            if (IsCsv)
            {
                ExportPowerUserDataToCSVFile(customermodel.PowerUsers);
            }
            else
            {
                if (customermodel.PowerUsers.Rows.Count > 0)
                {
                    grdPowerUsersList.Visible = true;
                    grdPowerUsersList.DataSource = customermodel.PowerUsers;
                    grdPowerUsersList.DataBind();

                    string fileName = "PowerUserList.xls";
                    Response.Buffer = true;
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
                    System.IO.StringWriter oStringWriter = new System.IO.StringWriter();
                    System.Web.UI.HtmlTextWriter oHtmlTextWriter = new System.Web.UI.HtmlTextWriter(oStringWriter);
                    grdPowerUsersList.RenderControl(oHtmlTextWriter);
                    Response.Write(oStringWriter.ToString());

                    Response.End();
                }
            }

        }


        private void ExportPowerUserDataToCSVFile(DataTable dtMaster)
        {
            try
            {
                DataRow[] drs = null;
                drs = dtMaster.Select();
                if ((dtMaster.Rows.Count <= 0))
                {
                    return;
                }
                //Int32 excludeCols = 0;
                StringBuilder sb = new StringBuilder();
                Int32 i = 0;

                foreach (DataColumn dc in dtMaster.Columns)
                {
                    sb.AppendFormat("{0}{1}", dc.ColumnName, (i == dtMaster.Columns.Count - 1 ? Environment.NewLine : ","));
                    i = i + 1;
                }


                foreach (DataRow dr in dtMaster.Rows)
                {
                    i = 0;
                    foreach (DataColumn dc in dtMaster.Columns)
                    {
                        sb.AppendFormat("\"{0}\"{1}", dr[dc.ColumnName].ToString().Replace("\"", "\"\""), (i == dtMaster.Columns.Count - 1 ? Environment.NewLine : ","));
                        i = i + 1;
                    }

                }


                Response.Write(sb.ToString());
                string strFileName = null;
                strFileName = string.Format("PoweUserList_{0}", DateTime.Now.ToString("MMddyyyy"));
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AppendHeader("content-disposition", "attachment; filename=" + strFileName + ".csv");
                //Response.AddHeader("content-disposition", "attachment; filename=" + strFileName + ".csv") 'attachment; filename=" + strFileName)
                StringWriter oStringWriter = new StringWriter();
                HtmlTextWriter oHtmlTextWriter = new HtmlTextWriter(oStringWriter);
                Response.Write(oStringWriter.ToString());
                Response.End();

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}