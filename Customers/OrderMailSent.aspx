﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderMailSent.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Customers.OrderMailSent" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Best Print Buy</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" media="all" href="../css/bestprintbuy_style.css" />    
	<link rel="stylesheet" media="all" href="../css/jquery.fancybox.css" />
	<script src="../Scripts/jquery.js"></script>
	<script src="../Scripts/common.js"></script>
    <script src="../Scripts/ddsmoothmenu.js"></script>
	<script src="../Scripts/plugin/fancybox/jquery.fancybox.js"></script>
    <!--[if lt IE 9]>
			<script src="../Scripts/html5.js"></script>
		<![endif]-->
    <!--[if lte IE 7]> <html class="ie7"> <![endif]-->
    <!--[if IE 8]>     <html class="ie8"> <![endif]-->
    <!--[if IE 9]>     <html class="ie9"> <![endif]-->
    <!--[if IE 10]>     <html class="ie10"> <![endif]-->
    <!--[if !IE]><!-->
    <!--<![endif]-->
        <script>
            window.focus();
        </script>
</head>
<body id="Body" runat="server">
    <!-- Wrapper starts -->
    <div id="popup_contant_hldr" runat="server">
        <!-- Header Ends -->
        <div style="padding-top:15px;"></div>
        <div class="popup_container rounded">
            <!-- Menu starts -->
            <form id="Form1" class="popup" runat="server">
                <div class="table_align">
                   <%
                   var mailData = pageData.Replace("Your order has been Printed & Shipped", "Your order has been Shipped");
                   mailData = mailData.Replace("Your order has been Printed", "Your order has been Shipped");
                   Response.Write(mailData);
                %>
                </div>
                <div class="btn_lst">
                    <div class="rgt">
                        <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClientClick="parent.$.fancybox.close(); return false;"></asp:Button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</body>
</html>
