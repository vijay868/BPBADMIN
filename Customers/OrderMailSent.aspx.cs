﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;


namespace Bestprintbuy.Admin.Web.Customers
{
    public partial class OrderMailSent : PageBase
    {
        protected SupplierPOModel poModel = new SupplierPOModel();
        protected string pageTitle = string.Empty;
        protected string pageData;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Put user code to initialize the page here
                bool isCancelled = false;
                int poDtlKey = Convert.ToInt32(Request["poDtlKey"]);
                DateTime emailDate = default(DateTime);
                poModel = SupplierPOService.GetMailOrderDtlInformation(poDtlKey, ConnString);
                if (poModel.CustOrders.Rows.Count <= 0)
                {
                    Response.Redirect("../Transactions/OrderFulfillment.aspx", true);
                    return;
                }
                SupplierPOModel.CustOrdersRow drOrder = (SupplierPOModel.CustOrdersRow)poModel.CustOrders.Rows[0];
                isCancelled = drOrder.is_order_canceled;
                emailDate = drOrder.email_date;
                string strMessage = ReadMailContent(isCancelled);
                string msgSubject = null;

                int fkeyecattribute = 0;
                string strEcAttribute = string.Empty;
                fkeyecattribute = -1;
                strEcAttribute = string.Empty;
                //Not strEcAttribute = String.Empty
                if (!drOrder.Isfkey_ec_attributeNull())
                {
                    fkeyecattribute = drOrder.fkey_ec_attribute;
                    strEcAttribute = GetExclusiveCustomerByECKey(fkeyecattribute);
                }

                if (isCancelled)
                {
                    if (strMessage == string.Empty)
                    {
                        strMessage = GetDefaultCancelContent();
                    }
                    msgSubject = "Your order has been {0} - Order Number {1}";
                    string orderstatus = string.Empty;

                    orderstatus = "Cancelled";
                    strMessage = string.Format(strMessage, orderstatus, drOrder.ordernumber, drOrder.customer_name, emailDate.ToString("MM/dd/yy H:mm:ss zzz"));
                    msgSubject = string.Format(msgSubject, orderstatus, drOrder.ordernumber);
                    strMessage = strMessage.Replace("&#123;", "{");
                    strMessage = strMessage.Replace("&#125;", "}");


                }
                else
                {
                    if (strMessage == string.Empty)
                    {
                        strMessage = GetDefaultContent();
                    }
                    msgSubject = "Your order has been {0} - Order Number {1}";
                    string orderstatus = "Shipped";
                    string shipTracking = string.Empty;

                    if (!drOrder.Iscarrier_service_nameNull())
                    {
                        //orderstatus = orderstatus & " & Shipped"
                        if (!string.IsNullOrEmpty(drOrder.carrier_service_name))
                        {
                            shipTracking = "Shipping Company: <B>" + Convert.ToString(drOrder.carrier_service_name) + "</B><BR>";
                        }
                    }

                    //If Not drOrder.tracking_number = String.Empty Then
                    //    If Not drOrder.tracking_number.Trim() = "" Then
                    //        shipTracking = shipTracking & "Tracking Number: <B>" & Convert.ToString(drOrder.tracking_number) & "</B>"
                    //    End If
                    //End If
                    if (!drOrder.Istracking_numberNull())
                    {
                        if (!string.IsNullOrEmpty(drOrder.tracking_number.Trim()))
                        {
                            if (!string.IsNullOrEmpty(drOrder.tracking_url.Trim()))
                            {
                                shipTracking = shipTracking + "Tracking Number: <a href='" + Convert.ToString(drOrder.tracking_url) + "' target=_blank><B>" + Convert.ToString(drOrder.tracking_number) + "</B></a>";
                            }
                            else
                            {
                                shipTracking = shipTracking + "Tracking Number: <B>" + Convert.ToString(drOrder.tracking_number) + "</B>";
                            }
                        }
                    }

                    strMessage = string.Format(strMessage, orderstatus, drOrder.ordernumber, drOrder.customer_name, shipTracking, emailDate.ToString("MM/dd/yyyy hh:mm tt"));
                    msgSubject = string.Format(msgSubject, orderstatus, drOrder.ordernumber);
                    strMessage = strMessage.Replace("&#123;", "{");
                    strMessage = strMessage.Replace("&#125;", "}");
                }
                strMessage = strMessage.Replace("##kiosk##", "/" + strEcAttribute);
                strMessage = strMessage.Replace("##ecid##", strEcAttribute);
                strMessage = strMessage.Replace("##eckey##", (fkeyecattribute > 0 ? Convert.ToString(fkeyecattribute) : ""));

                strMessage = string.Format("<span class='head'>Mail Subject: <B>{0}</B></span><BR><BR>{1}", msgSubject, strMessage);
                pageData = string.Format("<span class='head'>Mail To: <B>{0}</B></span><BR>{1}", drOrder.customer_email_id, strMessage);
                Page.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        private string ReadMailContent(bool isCancelled)
        {
            try
            {
                string strFile = null;
                string strContent = null;
                strFile = WebConfiguration.GetEmailHTMLLoc((isCancelled ? "OrderCancellation" : "OrderFulFillment"));
                System.IO.StreamReader file = new System.IO.StreamReader(strFile);
                strContent = file.ReadToEnd();
                file.Close();
                file = null;
                return strContent;
            }
            catch (System.IO.FileNotFoundException fnfEx)
            {
                FileLogger.WriteException(fnfEx);
                return string.Empty;
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                return string.Empty;
            }
        }

        private string GetDefaultCancelContent()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder("");
            sb.Append("<html><head><title>Order Cancellation.</title>");
            sb.Append("<style>");
            sb.Append(".bodytext &#123; font-family: verdana, arial, Helvetica; font-size:10px; color:#000000; &#125;");
            sb.Append(".head &#123; font-family: verdana, arial, Helvetica;  font-size:12px;  color:#000000; &#125;");
            sb.Append("</style></head>");
            sb.Append("<body>");
            sb.Append("<TABLE cellSpacing='1' cellPadding='0' width='100%' border='0' bgcolor='#000000' >");
            sb.Append("<TBODY><TR><TD>");
            sb.Append("<TABLE cellSpacing='0' cellPadding='10' width='100%' border='0' bgcolor='#ffffff' >");
            sb.Append("<TBODY><TR><TD>");
            sb.Append("\t<TABLE cellSpacing='10' cellPadding='0' width='100%' border='0' >");
            sb.Append("\t<TBODY><tr><td align='center' class='head'><B>Your order has been {0} - Order Number {1}</B></td></tr>");
            sb.Append("\t<tr><td align='right' class='head'>Date:{3}</td></tr>");
            sb.Append("\t<tr><td align='center' class='head'></td></tr>");
            sb.Append("\t<tr><td align='left' class='head'>Dear Mr/Mrs {2},<BR>");
            sb.Append("\tYour order number <B>{1}</B> has been {0}.</td></tr>");
            sb.Append("\t<tr><td align='left' class='head'>If you have any questions regarding your order,");
            sb.Append("\tplease reply directly to this message or send an e-mail to <a href='mailto:Customercare@Bestprintbuy.com'>\tCustomercare@Bestprintbuy.com</a></td></tr>");
            sb.Append("\t<tr><td align='left' class='head'>Please take some time to check back for a whole range of new products as we update our site.</td></tr>");
            sb.Append("\t<tr><td align='left' class='head'></td></tr>");
            //sb.Append("	<tr><td align='left' class='head'>###Disclaimer###</td></tr>")
            sb.Append("</TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></body></html>");
            return sb.ToString();
        }

        private string GetDefaultContent()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder("");
            sb.Append("<html><head><title>Order Fulfillment.</title>");
            sb.Append("<style>");
            sb.Append(".bodytext &#123; font-family: verdana, arial, Helvetica; font-size:10px; color:#000000; &#125;");
            sb.Append(".head &#123; font-family: verdana, arial, Helvetica;  font-size:12px;  color:#000000; &#125;");
            sb.Append("</style></head>");
            sb.Append("<body>");
            sb.Append("<TABLE cellSpacing='1' cellPadding='0' width='100%' border='0' bgcolor='#000000' >");
            sb.Append("<TBODY><TR><TD>");
            sb.Append("<TABLE cellSpacing='0' cellPadding='10' width='100%' border='0' bgcolor='#ffffff' >");
            sb.Append("<TBODY><TR><TD>");
            sb.Append("\t<TABLE cellSpacing='10' cellPadding='0' width='100%' border='0' >");
            sb.Append("\t<TBODY><tr><td align='center' class='head'><B>Your order has been {0} - Order Number {1}</B></td></tr>");
            sb.Append("\t<tr><td align='right' class='head'>Date:{4}</td></tr>");
            sb.Append("\t<tr><td align='center' class='head'></td></tr>");
            sb.Append("\t<tr><td align='left' class='head'>Dear Mr/Mrs {2},<BR>Thank you for your order. ");
            sb.Append("\tYour order number <B>{1}</B> has been {0}.</td></tr>");
            sb.Append("\t<tr><td align='left' class='head'>{3}</td></tr>        ");
            sb.Append("\t<tr><td align='left' class='head'>If you have any questions regarding your order,");
            sb.Append("\tplease reply directly to this message or send an e-mail to <a href='mailto:Customercare@Bestprintbuy.com'>\tCustomercare@Bestprintbuy.com</a></td></tr>");
            sb.Append("\t<tr><td align='left' class='head'>Thank you for ordering from Bestprintbuy.com the user friendly one stop shop for an assortment of business card themes and designs.</td></tr>");
            sb.Append("\t<tr><td align='left' class='head'>Please take some time to check back for a whole range of new products as we update our site.</td></tr>");
            sb.Append("\t<tr><td align='left' class='head'></td></tr>");
            //sb.Append("	<tr><td align='left' class='head'>###Disclaimer###</td></tr>")
            sb.Append("</TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></body></html>");
            return sb.ToString();
        }
    }
}