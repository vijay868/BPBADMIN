﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.Customers
{
    public partial class OrderLinks : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                txtPkeyCustPoDtl.Value = Request["pkey_cust_po_dtl"];
                txtPkeyBrPoDtl.Value = Request["pkey_br_po_dtl"];
                try
                {
                    SupplierPOModel poModel = SupplierPOService.GetOrderAssets(Convert.ToInt32(Request["pkey_cust_po_dtl"]), ConnString);

                    lblPONumber.Text = Convert.ToString(poModel.CustOrders.Rows[0]["ordernumber"]);
                    lblSONumber.Text = lblPONumber.Text + "-" + txtPkeyBrPoDtl.Value;

                    trCsvUrl.Visible = false;
                    trFrontImg.Visible = false;
                    trBackImage.Visible = false;

                    anchPdfUrl.NavigateUrl = GetHiresProofURL(poModel.CustOrders.Rows[0]["hi_res_proof_url"], Convert.ToString(poModel.CustOrders.Rows[0]["cards_for"]));
                    if (string.IsNullOrEmpty(anchPdfUrl.NavigateUrl))
                    {
                        trPDFLink.Visible = false;
                    }


                    if (Convert.ToString(poModel.CustOrders.Rows[0]["design_mode"]).ToLower() == "uploaddesign")
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(poModel.CustOrders.Rows[0]["front_image_url"])))
                        {
                            trFrontImg.Visible = true;
                            anchFrontImageUrl.NavigateUrl = Convert.ToString(poModel.CustOrders.Rows[0]["front_image_url"]);
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(poModel.CustOrders.Rows[0]["reverse_image_url"])))
                        {
                            trBackImage.Visible = true;
                            anchFrontImageUrl.NavigateUrl = Convert.ToString(poModel.CustOrders.Rows[0]["reverse_image_url"]);
                        }
                        if (Convert.ToBoolean(poModel.CustOrders.Rows[0]["is_direct_mail"]))
                        {
                            trCsvUrl.Visible = true;
                            anchCsvUrl.NavigateUrl = GetUploadDesignCsvURL(poModel.CustOrders.Rows[0]["csv_FileNM"], anchFrontImageUrl.NavigateUrl);
                        }
                    }
                    else
                    {
                        if (Convert.ToBoolean(poModel.CustOrders.Rows[0]["is_direct_mail"]))
                        {
                            trCsvUrl.Visible = true;
                            anchCsvUrl.NavigateUrl = GetCsvURL(poModel.CustOrders.Rows[0]["csv_FileNM"], Convert.ToString(poModel.CustOrders.Rows[0]["cards_for"]));
                        }
                    }

                    Page.DataBind();
                }
                catch (Exception ex)
                {
                    FileLogger.WriteException(ex);
                    AlertScript(ex.Message);
                }
            }
            else
            {
                //txtPkeyCustPoDtl.Value = Request("txtPkeyCustPoDtl")
                //txtPkeyBrPoDtl.Value = Request("txtPkeyCustPoDtl")
            }
        }

        protected string GetHiresProofURL(object url, string cardsFor)
        {
            string strURL = "";
            try
            {
                if ((url != null))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(url).Trim()))
                    {
                        strURL = Convert.ToString(url);
                        if (!(Convert.ToString(cardsFor) == "CL"))
                        {
                            strURL = strURL.Replace(WebConfiguration.PDFSavedFolder, WebConfiguration.PDFSavedFolderServer);
                        }
                        else
                        {
                            strURL = ProdDesignCenterUrlBase + strURL.Substring(strURL.IndexOf("/Users/"));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                strURL = "";
            }
            return strURL;
        }

        protected string GetUploadDesignCsvURL(object csvName, string frontdesignurl)
        {
            string strURL = "#";
            try
            {
                if ((csvName != null))
                {
                    //http://localhost:80/BestPrintBuy/DesignCenterUsers\15562\CompleteDesigns/PO-2010-12-1862850-18669-27800.pdf_front.pdf
                    string baseUrl = frontdesignurl.Replace("\\\\", "/");
                    baseUrl = baseUrl.Substring(0, baseUrl.LastIndexOf("/") + 1);
                    strURL = baseUrl + csvName;
                }
            }
            catch (Exception ex)
            {
                strURL = "#";
            }
            return strURL;
        }

        protected string GetCsvURL(object url, string cardsFor)
        {
            string strURL = "";
            try
            {
                if ((url != null))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(url).Trim()))
                    {
                        strURL = Convert.ToString(url);

                        if (!(Convert.ToString(cardsFor) == "CL"))
                        {
                            //strURL = strURL.Replace(WebConfiguration.PDFSavedFolder, WebConfiguration.PDFSavedFolderServer)
                            strURL = WebConfiguration.PDFSavedFolderServer + strURL;
                        }
                        else
                        {
                            strURL = ProdDesignCenterUrlBase + strURL.Substring(strURL.IndexOf("/Users/"));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                strURL = "";
            }
            return strURL;
        }
    }
}