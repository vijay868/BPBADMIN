﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;

namespace Bestprintbuy.Admin.Web.Customers
{
    public partial class USLeadsStatus : PageBase
    {
        protected string TokenNumber = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            TokenNumber = Request.QueryString["leadstoken"];
            if (!Page.IsPostBack)
            {
                GetLeadsData(TokenNumber);
            }
        }

        private void GetLeadsData(string myToken)
        {
            try
            {
                // --- NOTE: This sample assumes it is running inside an ASP.NET web form.
                //
                string myServiceUrl = WebConfiguration.USLeadsWebServiceURL;
                System.Text.StringBuilder sb = new System.Text.StringBuilder("");

                // --- create an instance of the web service client.
                Usadata.ListModuleV3.Gateway.GatewayService myService = new Usadata.ListModuleV3.Gateway.GatewayService();
                Usadata.ListModuleV3.Gateway.IntegratedPortalSessionDetails myDetails = default(Usadata.ListModuleV3.Gateway.IntegratedPortalSessionDetails);

                // --- execute the "getSessionDetails()" web method in order to retrieve
                // --- the details object based on a specific session token.
                try
                {
                    myService.Url = myServiceUrl;
                    myDetails = myService.getSessionDetails(myToken);
                }
                catch (Exception ex)
                {
                    // --- handle the exception here.
                }

                // --- render the session details object into a
                // --- StringWriter as HTML.
                //

                sb.Append("<table class=\"tablep\" border=\"0\" >");
                sb.AppendFormat("<tr><td  colspan=\"2\" height=\"20px\"><strong>Session Details As Of {0:F}</strong></td></tr>", DateTime.Now.ToString());

                sb.Append("<tr >");
                sb.AppendFormat("<tdalign=\"right\">{0}:</td>", "OrderMethod");
                sb.AppendFormat("<td>{0}</td>", myDetails.OrderMethod);
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.AppendFormat("<td align=\"right\">{0}:</td>", "TokenID");
                sb.AppendFormat("<td>{0}</td>", myDetails.TokenID);
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.AppendFormat("<td align=\"right\">{0}:</td>", "ClientSessionID");
                sb.AppendFormat("<td>{0}</td>", myDetails.ClientSessionID);
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.AppendFormat("<td align=\"right\">{0}:</td>", "ClientID");
                sb.AppendFormat("<td>{0}</td>", myDetails.ClientID);
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.AppendFormat("<td align=\"right\">{0}:</td>", "UserID");
                sb.AppendFormat("<td>{0}</td>", myDetails.UserID);
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.AppendFormat("<td align=\"right\">{0}:</td>", "OrderID");
                if ((myDetails.OrderID != null))
                {
                    sb.AppendFormat("<td>{0}</td>", myDetails.OrderID);
                }
                else
                {
                    sb.AppendFormat("<td>{0}</td>", "&lt;Order Not Placed&gt;");
                }
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.AppendFormat("<td align=\"right\">{0}:</td>", "OrderDate");
                if ((myDetails.OrderDate != null))
                {
                    sb.AppendFormat("<td>{0}</td>", myDetails.OrderDate);
                }
                else
                {
                    sb.AppendFormat("<td>{0}</td>", "&lt;Order Not Placed&gt;");
                }
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.AppendFormat("<td align=\"right\">{0}:</td>", "OrderType");
                sb.AppendFormat("<td>{0}</td>", myDetails.OrderType);
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.AppendFormat("<td align=\"right\">{0}:</td>", "OrderUsage");
                sb.AppendFormat("<td>{0}</td>", myDetails.OrderUsage);
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.AppendFormat("<td align=\"right\">{0}:</td>", "PaymentMethod");
                sb.AppendFormat("<td>{0}</td>", myDetails.PaymentMethod);
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.AppendFormat("<td align=\"right\">{0}:</td>", "PaymentStatus");
                sb.AppendFormat("<td>{0}</td>", myDetails.PaymentStatus);
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.AppendFormat("<td align=\"right\">{0}:</td>", "QuantitySelected");
                sb.AppendFormat("<td>{0}</td>", myDetails.QuantitySelected);
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.AppendFormat("<td align=\"right\">{0}:</td>", "QuantityAvailable");
                sb.AppendFormat("<td>{0}</td>", myDetails.QuantityAvailable);
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.AppendFormat("<td align=\"right\">{0}:</td>", "PhoneOption");
                sb.AppendFormat("<td>{0}</td>", myDetails.PhoneOption);
                sb.Append("</tr>");

                int I = 0;
                int J = 0;

                sb.Append("<tr>");
                sb.AppendFormat("<td align=\"right\" valign=\"top\">{0}:</td>", "GeographyCriteria");
                sb.Append("<td>");

                sb.AppendFormat("<strong><u>{0}</u></strong>", myDetails.GeographyCriteria.Name);
                for (I = 0; I <= (myDetails.GeographyCriteria.Values.Length - 1); I++)
                {
                    sb.AppendFormat("<li>{0}", myDetails.GeographyCriteria.Values[I].Name);
                }
                sb.Append("</td>");
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.AppendFormat("<td align=\"right\" valign=\"top\">{0}:</td>", "DemographicCriteria");
                sb.Append("<td>");

                if (myDetails.DemographicCriteria == null || myDetails.DemographicCriteria.Length < 1)
                {
                    sb.Append("&lt;No Demographic Criteria Selected&gt;");
                }
                else
                {
                    for (I = 0; I <= (myDetails.DemographicCriteria.Length - 1); I++)
                    {
                        sb.AppendFormat("<strong><u>{0}</u></strong>", myDetails.DemographicCriteria[I].Name);
                        for (J = 0; J <= (myDetails.DemographicCriteria[I].Values.Length - 1); J++)
                        {
                            sb.AppendFormat("<li>{0}", myDetails.DemographicCriteria[I].Values[J].Name);
                        }
                        sb.Append("<br>");
                    }
                }
                sb.Append("</td>");
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.AppendFormat("<td align=\"right\" valign=\"top\">{0}:</td>", "AdditionalOutputColumns");
                sb.Append("<td>");

                if (myDetails.AdditionalOutputColumns == null || myDetails.AdditionalOutputColumns.Length < 1)
                {
                    sb.Append("&lt;No Additional Output Columns&gt;");
                }
                else
                {
                    for (I = 0; I <= (myDetails.AdditionalOutputColumns.Length - 1); I++)
                    {
                        sb.AppendFormat("<li>{0}", myDetails.AdditionalOutputColumns[I].Name);
                    }
                }
                sb.Append("</td>");
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.AppendFormat("<td align=\"right\" valign=\"top\">{0}:</td>", "OrderLayout");
                sb.Append("<td>");

                if (myDetails.OrderLayout == null || myDetails.OrderLayout.Length < 1)
                {
                    sb.Append("&lt;No Layout Specified&gt;");
                }
                else
                {
                    sb.Append("<ol>");
                    for (I = 0; I <= (myDetails.OrderLayout.Length - 1); I++)
                    {
                        sb.AppendFormat("<li>{0}", myDetails.OrderLayout[I]);
                    }
                    sb.Append("</ol>");
                }
                sb.Append("</td>");
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.AppendFormat("<td align=\"right\">{0}:</td>", "PricePerRecord");
                sb.AppendFormat("<td>{0:C}</td>", myDetails.PricePerRecord);
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.AppendFormat("<td align=\"right\">{0}:</td>", "OrderPrice");
                sb.AppendFormat("<td>{0:C}</td>", myDetails.OrderPrice);
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.AppendFormat("<td align=\"right\">{0}:</td>", "CostPerRecord");
                sb.AppendFormat("<td>{0:C}</td>", myDetails.CostPerRecord);
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.AppendFormat("<td align=\"right\">{0}:</td>", "OrderCost");
                sb.AppendFormat("<td>{0:C}</td>", myDetails.OrderCost);
                sb.Append("</tr>");

                sb.Append("<tr>");
                sb.AppendFormat("<td align=\"right\">{0}:</td>", "RebateAmount");
                sb.AppendFormat("<td>{0:C}</td>", myDetails.RebateAmount);
                sb.Append("</tr>");

                sb.Append("</table>");

                ltrlLeadsData.Text = sb.ToString();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
            
        }
    }
}