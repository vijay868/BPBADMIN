﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;
using System.Text;
using System.Web.Services;
using System.Configuration;

namespace Bestprintbuy.Admin.Web.Customers
{
    public partial class CustomersList : PageBase
    {
        protected int serialNo;
        protected string searchstring = "-1";
        protected int recordCount = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadData();
            }
        }

        private void LoadData()
        {
            try
            {
                CustomerModel.CustomersDataTable customers = UserService.GetCustomerListBysearch(-1, searchstring, ConnString);

                if (CustomerslistGrid.Attributes["SortExpression"] != null)
                    customers.DefaultView.Sort = CustomerslistGrid.Attributes["SortExpression"] + " " + CustomerslistGrid.Attributes["SortDirection"];
                recordCount = customers.Rows.Count;
                CustomerslistGrid.DataSource = customers.DefaultView;
                CustomerslistGrid.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void CustomerslistGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            SortGridView(CustomerslistGrid, e);
            CustomerslistGrid.PageIndex = 0;
            searchstring = GetSearchCondition();
            LoadData();
        }

        protected void CustomerslistGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CustomerslistGrid.PageIndex = e.NewPageIndex;
            searchstring = GetSearchCondition();
            LoadData();
        }

        protected int GetRecordCountForPage()
        {
            return GetRecordCountForPageSize(CustomerslistGrid.PageSize, CustomerslistGrid.PageIndex + 1);
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/DashBoard.aspx");
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            CustomerslistGrid.PageIndex = 0;
            btnReset.Visible = false;
            ddlOperator.SelectedIndex = 0;
            ddlFilterFor.SelectedIndex = 0;
            txtPattern.Text = "";
            LoadData();
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            try
            {
                CustomerslistGrid.PageIndex = 0;
                searchstring = GetSearchCondition();
                LoadData();
                btnReset.Visible = true;
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        public String GetSearchCondition()
        {
            //Variable holding the current session object
            System.Web.SessionState.HttpSessionState currsess = System.Web.HttpContext.Current.Session;

            //Variable holding the parameters - values from the request in a named value collection
            NameValueCollection requestCollections = System.Web.HttpContext.Current.Request.Params;

            //Get the Critera , pattern, operator from the request
            String criteria = ddlFilterFor.SelectedValue;
            String roperator = ddlOperator.SelectedValue;
            String pattern = txtPattern.Text.Trim();

            //Check if pattern exists
            if ("".Equals(pattern)) return "-1";

            //Create a new instance of named value collection
            NameValueCollection nvc = new NameValueCollection();

            //Add the Critera , pattern, operator to the nvc
            nvc.Add("criteria", criteria);
            nvc.Add("operator", roperator);
            nvc.Add("pattern", pattern);
            currsess["combovalues"] = nvc;

            String searchfor = FormSearchString(nvc);
            currsess["search_condition"] = searchfor;
            currsess["pattern_value"] = pattern;

            return searchfor;
        }

        private String FormSearchString(NameValueCollection nvc)
        {
            String sOperator = nvc["operator"];
            sOperator = sOperator != null ? sOperator.Trim() : sOperator;
            string pattern = nvc["pattern"];
            StringBuilder sb = new StringBuilder();


            if (sOperator.IndexOf("LIKE") < 0)
            {
                pattern = pattern.Replace("'", "''");
                sb.Append(nvc["criteria"]).Append(" ").Append(sOperator).Append(" ").Append("'").Append(pattern).Append("'");
            }
            else if (sOperator != null && (sOperator.ToLower()).IndexOf("in") > 0)
            {
                sb.Append(nvc["criteria"]).Append("  ").Append(sOperator).Append(" (").Append(pattern).Append(")");
            }
            else
            {
                sOperator = sOperator.Replace("LIKE", pattern);
                sOperator = sOperator.Replace("'", "''");
                sb.Append(nvc["criteria"]).Append(" LIKE '").Append(sOperator).Append("'");
            }
            return sb.ToString();
        }

        [WebMethod]
        public static List<string> GetAutoCompleteData(string input, int field)
        {
            List<string> result = new List<string>();
            string strField = "";
            if (field == 0)
                strField = "EMAIL_ID";
            else if (field == 1)
                strField = "FIRST_NAME";
            else if (field == 2)
                strField = "LAST_NAME";
            string connstring = ConfigurationManager.AppSettings["BestPrintBuyDBConn"];
            result = UserService.GetAutoSearchInfoList(1, "%" + input + "%", strField, connstring);
            return result;
        }
    }
}