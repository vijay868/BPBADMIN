﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;

namespace Bestprintbuy.Admin.Web.Customers
{
    public partial class OrderFulfillmentSummary : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                GetData();
            }
        }

        private void GetData()
        {
            try
            {
                int sheetCount = 0;
                int cardCount = 0;
                SupplierPOModel model = SupplierPOService.GetSummary(ConnString);
                if (model.PendingOrders.Rows.Count > 0)
                {
                    foreach (SupplierPOModel.PendingOrdersRow row in model.PendingOrders.Rows)
                    {
                        sheetCount = sheetCount + row.totalsheets;
                        cardCount = cardCount + row.quantity;
                    }
                }
                string sb = "";
                sb += "<table class=\"tablep\">";
                sb += "<thead><tr>";
                sb += "<th width=\"15%\">Product Type</th>";
                sb += "<th width=\"15%\">Quantity</th>";
                sb += "<th width=\"30%\">Paper Stock</th>";
                sb += "<th width=\"13%\">Reverse (Y/N)</th>";
                sb += "<th width=\"15%\">Imposition Scheme</th>";
                sb += "<th width=\"12%\">Total Sheets</th>";
                sb += "</tr></thead>";
                for (int i = 0; i < model.PendingOrders.Rows.Count - 1; i++)
                {
                    DataRow[] dr = model.PendingOrders.Select("product_type='" + model.PendingOrders.Rows[i]["product_type"] + "' and paper_stock='" + model.PendingOrders.Rows[i]["paper_stock"] + "'");
                    if (dr.Length > 0)
                    {
                        sb += "<tr><td colspan=6 style=\"text-align:left; padding-left:5px;\"><b>" + dr[0]["product_type"] + "-" + dr[0]["paper_stock"] + "</b></td></tr>";
                        foreach (SupplierPOModel.PendingOrdersRow row in dr.ToList())
                        {
                            sb += "<tr>";
                            sb += "<td>" + row["product_type"].ToString() + "</td>";
                            sb += "<td>" + row["quantity"].ToString() + "</td>";
                            sb += "<td>" + row["paper_stock"].ToString() + "</td>";
                            sb += "<td style=\"text-align:center\">" + row["reverse_color"].ToString() + "</td>";
                            sb += "<td>" + row["imposition_scheme"].ToString() + "</td>";
                            sb += "<td>" + row["totalsheets"].ToString() + "</td>";
                            sb += "</tr>";
                            model.PendingOrders.RemovePendingOrdersRow(row);
                        }
                    }
                }
                sb += "<tr>";
                sb += "<td><label>Cum Cards</label></td>";
                sb += "<td>" + sheetCount.ToString() + "</td>";
                sb += "<td colspan=3  style=\"text-align:right\"><label>Cum Sheets</label></td>";
                sb += "<td>" + cardCount.ToString() + "</td>";
                sb += "</tr>";
                sb += "</table>";
                ltrlTable.Text = sb;
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }
    }
}