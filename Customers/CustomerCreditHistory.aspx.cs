﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;

namespace Bestprintbuy.Admin.Web.Customers
{
    public partial class CustomerCreditHistory : PageBase
    {
        protected decimal totalCreditAmount = 0;
        protected decimal totalAvailedAmount = 0;
        protected decimal totalActiveCreditAmount = 0;
        protected int fkey_customer = -1;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                GetData();
            }
        }

        private void GetData()
        {
            try
            {
                ViewState["fkey_customer"] = Convert.ToInt32(Request.QueryString["fkeycustomer"].Trim() == "" ? "-1" : Request.QueryString["fkeycustomer"].Trim());
                fkey_customer = Convert.ToInt32(ViewState["fkey_customer"]);
                ServiceCreditsModel creditHistory = CustomerService.GetCustomerCreditHistory(fkey_customer, ConnString);

                foreach (ServiceCreditsModel.CustomerCreditRow row in creditHistory.CustomerCredit.Rows)
                {
                    totalCreditAmount = totalCreditAmount + row.credit_amount;
                    totalAvailedAmount = totalAvailedAmount + row.availed_amount;
                    if (row.is_active)
                        totalActiveCreditAmount = totalActiveCreditAmount + row.credit_amount;
                    else
                        totalActiveCreditAmount = totalActiveCreditAmount + row.availed_amount;
                }
                ServiceCreditsModel.CustomerCreditRow Row = (ServiceCreditsModel.CustomerCreditRow)creditHistory.CustomerCredit.Rows[0];
                lblCustomerName.Text = Row.customer_name;
                lblCustomerEmail.Text = Row.email_id;
                lblTotalCreditAmt.Text = ConvertToMoneyFormat(totalCreditAmount.ToString());
                lblTotalActiveCreditAmt.Text = ConvertToMoneyFormat(totalActiveCreditAmount.ToString());
                lblTotalAvailedAmt.Text = ConvertToMoneyFormat(totalAvailedAmount.ToString());
                lblBalanceAmt.Text = ConvertToMoneyFormat((totalActiveCreditAmount - totalAvailedAmount).ToString());

                rptrCustomerCreditHistory.DataSource = creditHistory.CustomerCredit;
                rptrCustomerCreditHistory.DataBind();

                foreach (RepeaterItem item in rptrCustomerCreditHistory.Items)
                {
                    HiddenField hdn = (HiddenField)item.FindControl("hdnPkeyservice");
                    HiddenField hdnavailedamount = (HiddenField)item.FindControl("hdnavailedamount");
                    Repeater rptr = (Repeater)item.FindControl("rptrCustomerPOList");

                    var list = from collect in creditHistory.POCreditStatus
                               where collect.fkey_service_credit == Convert.ToInt32(hdn.Value)
                               select collect;

                    if (hdnavailedamount.Value != "0.00" || list.Count() > 0)
                    {
                        rptr.DataSource = list.ToList();
                        rptr.DataBind();
                        item.FindControl("tdRepeater").Visible = true;
                        item.FindControl("tdNoData").Visible = false;
                    }
                    else
                    {
                        item.FindControl("tdNoData").Visible = true;
                        item.FindControl("tdRepeater").Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }
    }
}