﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.Customers
{
    public partial class CustomerValidateAndAssign : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void lbtnValidate_Click(object sender, EventArgs e)
        {
            try
            {
                bool loginOption = true;
                CustomerModel.CustomerCreditDataTable validatecustomer = UserService.ValidateCustomer(txtEmailID.Text, loginOption, ConnString);
                if (validatecustomer.Rows.Count > 0)
                {
                    Response.Redirect("MCACustomerDetails.aspx?Customerpkey=" + validatecustomer.Rows[0]["fkey_customer"]);
                }
                else
                {
                    AlertScript("Invalid Email ID");
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("MCACustomerList.aspx");
        }
    }
}