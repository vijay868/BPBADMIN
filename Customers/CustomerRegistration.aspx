﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="CustomerRegistration.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Customers.CustomerRegistration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
        function saveCustomer() {
            if ('<%=HasAccess("CustomerRegistration", "edit")%>' == 'False') {
                alert('User does not have access.');
                return false;
            }
            return true;
        }

        $().ready(function () {
            $("#frmCustomerRegistration").validate();
        });

        function SetRequirePassword() {

            if ('<%=Convert.ToInt32(Request.QueryString["Customerpkey"])%>' != '-2') {
                $('#ContentPlaceHolder1_txtConfirmPassword').removeAttr('required');
                $('#ContentPlaceHolder1_txtPassword').removeAttr('required');
            }
        }
    </script>
    <!-- Container Starts -->
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Customer Service</li>
            <li><a href="CustomersList.aspx">Customers List</a></li>
            <li>Customers Details</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Customers Details</h2>
        <form id="frmCustomerRegistration" name="frmCustomerRegistration" runat="server">
            <table width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="15%">
                            <label>First Name*</label></td>
                        <td width="20%">
                            <asp:TextBox CssClass="text-box" name="txtFirstName" ID="txtFirstName" runat="server" required></asp:TextBox></td>
                        <td width="15%">
                            <label>Company Name </label>
                        </td>
                        <td width="35%">
                            <asp:TextBox CssClass="text-box" name="txtCompanyName" ID="txtCompanyName" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>
                            <label>Last Name*</label></td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtLastName" ID="txtLastName" runat="server" required></asp:TextBox></td>
                        <td>
                            <label>Title </label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtTitle" ID="txtTitle" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>
                            <label>Address 1</label></td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtAddress1" ID="txtAddress1" runat="server"></asp:TextBox></td>
                        <td>
                            <label>Address 2</label></td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtAddress2" ID="txtAddress2" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>
                            <label>City </label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtCity" ID="txtCity" runat="server"></asp:TextBox></td>
                        <td>
                            <label>Country </label>
                        </td>
                        <td>
                            <asp:DropDownList name="ddlCountry" ID="ddlCountry" runat="server" CssClass="slect-box">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td>
                            <label>Zip </label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box digits" name="txtZip" ID="txtZip" runat="server"></asp:TextBox></td>
                        <td>
                            <label>State </label>
                        </td>
                        <td>
                            <asp:DropDownList name="ddlState" ID="ddlState" runat="server" CssClass="slect-box">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td>
                            <label>Phone </label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtPhone" ID="txtPhone" runat="server"></asp:TextBox></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </tbody>
            </table>

            <table width="100%" class="accounts-table">
                <thead>
                    <tr>
                        <th colspan="4" width="100%">Please check email address to be given below to ensure receipt of password and other free subscriptions</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="15%">
                            <label>Email*</label></td>
                        <td width="20%">
                            <asp:TextBox CssClass="text-box email" name="txtEmail" ID="txtEmail" runat="server" required></asp:TextBox></td>
                        <td width="15%">
                            <label>Primary use of Products </label>
                        </td>
                        <td width="35%">
                            <asp:CheckBox ID="chkPersonal" CssClass="check-box" runat="server" /><asp:Label ID="Label1" runat="server" Text="   Personal"></asp:Label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:CheckBox ID="ChkBusiness" CssClass="check-box" runat="server" /><asp:Label ID="Label2" runat="server" Text="   Business"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Password*</label></td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtPassword" ID="txtPassword" TextMode="Password" runat="server" required></asp:TextBox>
                        </td>
                        <td>
                            <label>Receive email by </label>
                        </td>
                        <td>
                            <asp:RadioButton ID="rbtnHtml" runat="server" GroupName="emailformat" /><asp:Label ID="Label3" runat="server" Text="  HTML"></asp:Label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:RadioButton ID="rbtnPlainText" runat="server" GroupName="emailformat" /><asp:Label ID="Label4" runat="server" Text="  Plain Text"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Retype Password*</label></td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtConfirmPassword" ID="txtConfirmPassword" runat="server" TextMode="Password" equalTo="#ContentPlaceHolder1_txtPassword" required></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </tbody>
            </table>

            <table width="100%" class="accounts-table">
                <thead>
                    <tr>
                        <th colspan="3" width="100%">If you do not want to receive information on the following site offerings please click to remove the items.</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="30%"></td>
                        <td style="text-align: center;">
                            <asp:CheckBox ID="ChkBusinessCards" CssClass="check-box" runat="server" /><asp:Label ID="Label5" runat="server" Text="   Business Cards"></asp:Label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:CheckBox ID="ChkPostCards" CssClass="check-box" runat="server" /><asp:Label ID="Label6" runat="server" Text="   Post Cards"></asp:Label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:CheckBox ID="ChkFoldedCards" CssClass="check-box" runat="server" /><asp:Label ID="Label9" runat="server" Text="   Folded Cards"></asp:Label>
                        </td>
                        <td width="30%"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="text-align: center;">
                            <asp:CheckBox ID="ChkBrochures" CssClass="check-box" runat="server" /><asp:Label ID="Label7" runat="server" Text="   Brochures"></asp:Label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:CheckBox ID="ChkMarketing" CssClass="check-box" runat="server" /><asp:Label ID="Label8" runat="server" Text="   Marketing"></asp:Label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:CheckBox ID="ChkBanners" CssClass="check-box" runat="server" /><asp:Label ID="Label10" runat="server" Text="   Banners"></asp:Label>
                        </td>
                        <td></td>
                    </tr>
                </tbody>
            </table>

            <table width="100%" class="accounts-table">
                <thead>
                    <tr>
                        <th colspan="3" width="100%">If you do not want to receive information on the following site offerings please click to remove the items.</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="10%"></td>
                        <td style="text-align: center;">
                            <asp:CheckBox ID="chkNewProducts" CssClass="check-box" runat="server" /><asp:Label ID="Label11" runat="server" Text="   New Products, Services and Promotional offers"></asp:Label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:CheckBox ID="chkNewsLetters" CssClass="check-box" runat="server" /><asp:Label ID="Label12" runat="server" Text="   News Letters"></asp:Label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:CheckBox ID="chkAcceptPrivacy" CssClass="check-box" runat="server" /><asp:Label ID="Label13" runat="server" Text="   Read and accept PPSOL Privacy and Security policy."></asp:Label>
                        </td>
                        <td width="10%"></td>
                    </tr>
                </tbody>
            </table>

            <table width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="15%">&nbsp;</td>
                        <td width="17%">&nbsp;</td>
                        <td width="15%">&nbsp;</td>
                        <td width="35%" style="float: right;">
                            <asp:Button runat="server" CssClass="button rounded" ID="btnSubmit" Text="Submit" OnClientClick="return saveCustomer();" OnClick="btnSubmit_Click"></asp:Button><asp:Button runat="server" CssClass="button rounded" ID="btnClose" Text="Close" UseSubmitBehavior="false" OnClick="btnClose_Click"></asp:Button>
                        </td>
                    </tr>
                </tbody>
            </table>

            <!-- table Ends -->
        </form>
        <script>
            SetRequirePassword();
        </script>
    </div>
</asp:Content>
