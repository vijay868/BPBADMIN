﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Utilities;
using Bestprintbuy.Admin.Web.Common;

namespace Bestprintbuy.Admin.Web.Customers
{
    public partial class ShopFloorInvoice : PageBase
    {
        protected string StateCountry = "";
        protected int brpoKey = 0;
        protected string EditKeys = "";
        protected string PoDtlKey = "";
        protected string poNumber = "";
        protected decimal m_cartCost = 0;
        protected string m_timeStamp = "";
        protected int nonFreeproducts = 0;
        protected int customerKey = 0;
        protected string shipColor = "#000000";

        protected string PhotoSource = "";
        protected string PhotoHeight = "62";
        protected string PhotoWidth = "44";

        protected string LogoSource = "";
        protected string LogoHeight = "62";
        protected string LogoWidth = "62";
        protected string m_revPdfBackImage = "<BR><BR><img onerror='alert('Back Side Preview Image does not exists. Please try again by clicking \'Go back to Design Center\' button.')'  src='{0}' width='208' height='122'  border=0>";
        protected bool isNotDM = true;
        protected string promotioncode = "";
        protected decimal m_customizationCost = 0;
        protected string mCustomerPortalURL = "";
        protected CartModel m_CartData = new CartModel();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                mCustomerPortalURL = ConfigurationManager.AppSettings["BPBCustomerBaseURL"];
                if (!IsPostBack)
                {

                    int qtyVariantKey = Convert.ToInt32(WebConfiguration.ProductVariants["Quantity"]);
                    int paperVariantKey = Convert.ToInt32(WebConfiguration.ProductVariants["PaperType"]);
                    int customerPOKey = Convert.ToInt32(Request.QueryString["EditKeys"]);
                    int customerKey = Convert.ToInt32(Request.QueryString["customerKey"]);
                    PoDtlKey = Request.QueryString["PoDtlKey"].ToString();
                    m_CartData = CartService.GetPrintInvoiceData(customerPOKey, customerKey, qtyVariantKey, paperVariantKey, ConnString);

                    if (m_CartData.CartShipInfo.Rows.Count > 0)
                    {
                        CartModel.CartShipInfoRow shipInfo = (CartModel.CartShipInfoRow)m_CartData.CartShipInfo.Rows[0];
                        lblAddress1.Text = shipInfo.shipment_address1;
                        lblAddress2.Text = shipInfo.shipment_address2;
                        lblShipName.Text = shipInfo.ship_first_name + " " + shipInfo.ship_last_name;
                        lblCity.Text = shipInfo.city;
                        lblCompany.Text = shipInfo.ship_company_name;
                        GetState(shipInfo.fkey_state, shipInfo.fkey_country);
                        lblZip.Text = shipInfo.zipcode;

                        lblAddress11.Text = shipInfo.shipment_address1;
                        lblAddress21.Text = shipInfo.shipment_address2;
                        lblShipName1.Text = shipInfo.ship_first_name + " " + shipInfo.ship_last_name;
                        lblCity1.Text = shipInfo.city;
                        lblCompany1.Text = shipInfo.ship_company_name;
                        GetState(shipInfo.fkey_state, shipInfo.fkey_country);
                        lblZip1.Text = shipInfo.zipcode;
                    }
                    if (m_CartData.CustomerCart.Rows.Count > 0)
                    {
                        CartModel.CustomerCartRow custCartrow = (CartModel.CustomerCartRow)m_CartData.CustomerCart.Rows[0];
                        lblShipNumber.Text = "*" + custCartrow.po_number + "-" + Request.QueryString["brpoKey"] + "*";
                        lblpodate.Text = custCartrow.po_date.ToShortDateString();
                    }
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected string writeSelectedProducts()
        {
            Int32 l_iCounter = default(Int32);
            string strText = "";
            m_cartCost = 0;
            decimal productCost = default(decimal);
            decimal processCost = 0;
            decimal shipCost = default(decimal);
            decimal shipPromoCost = default(decimal);
            decimal productPromoCost = default(decimal);
            bool IsUsLeads = false;
            bool isFreeProdcut = false;
            decimal freeShipCost = default(decimal);
            string shipOption = "";
            decimal StateTaxOn = default(decimal);
            decimal TaxAmount = 0;
            string strFrontImage = null;
            string strBackImage = null;
            Int32 cardHeight = 122;
            Int32 cardWidth = 208;
            string strClass = "bodytext";
            string prodDetails = null;
            Int32 l_PoDtlKeyCounter = default(Int32);
            bool is_direct_mail = false;
            decimal usLeadsCost = 0;


            double customerCreditAmt = 0;
            decimal xsellShipDiscount = 0;
            decimal xsellShipDiscount1 = 0;
            decimal xsellProductDiscount1 = 0;
            decimal xsellProductDiscount = 0;
            decimal revCustomizationCost = 0;
            decimal totalRevCustomizationCost = 0;

            string mTurnaroundTime = null;
            decimal mTurnAroundCost = 0;

            decimal mcashipDiscount = default(decimal);
            decimal mcaproductDiscount = default(decimal);
            decimal multiline_ShipDiscount = default(decimal);

            Int32 fkeySite = 1;
            CartModel.CustomerCartRow custCartrow = (CartModel.CustomerCartRow)m_CartData.CustomerCart.Rows[0];

            CartModel.CartBillingInfoRow cartBillrow = (CartModel.CartBillingInfoRow)m_CartData.CartBillingInfo.Rows[0];
            if (!custCartrow.Isservice_credit_discountNull())
            {
                customerCreditAmt = Convert.ToDouble(custCartrow.service_credit_discount);
            }
            if (!custCartrow.Iscustomization_costsNull())
            {
                m_customizationCost = custCartrow.customization_costs; ;
            }
            if (!custCartrow.IsturnaroundcostNull())
            {
                mTurnAroundCost = custCartrow.turnaroundcost;
            }
            if (!custCartrow.IsturnarounddaysNull())
            {
                mTurnaroundTime = custCartrow.turnarounddays.ToString();
            }
            Int32 RowCount = 1;
            for (l_iCounter = 0; l_iCounter <= m_CartData.CartDetails.Rows.Count - 1; l_iCounter++)
            {
                CartModel.CartDetailsRow cartDetailrow = (CartModel.CartDetailsRow)m_CartData.CartDetails.Rows[l_iCounter];
                isFreeProdcut = cartDetailrow.Isis_freeNull() ? false : cartDetailrow.is_free; //Convert.ToBoolean(GetValue("is_free", l_iCounter));
                xsellProductDiscount1 = cartDetailrow.Isxsell_product_discountNull() ? 0 : cartDetailrow.xsell_product_discount;   //Convert.ToDecimal(IIf(GetValue("xsell_product_discount", l_iCounter) = "", 0.0, GetValue("xsell_product_discount", l_iCounter)));
                xsellShipDiscount1 = cartDetailrow.Isxsell_shipment_discountNull() ? 0 : cartDetailrow.xsell_shipment_discount;   // Convert.ToDecimal(IIf(GetValue("xsell_shipment_discount", l_iCounter) = "", 0.0, GetValue("xsell_shipment_discount", l_iCounter)));

                if (PoDtlKey == Convert.ToString(cartDetailrow.pkey_cust_po_dtl))
                {
                    fkeySite = 1;
                    if (!cartDetailrow.Isfkey_siteNull())
                    {
                        fkeySite = cartDetailrow.fkey_site;
                        if (fkeySite == 2)
                        {
                            mCustomerPortalURL = ConfigurationManager.AppSettings["BPBSMBCustomerBaseURL"];
                        }
                    }
                    xsellProductDiscount = xsellProductDiscount + xsellProductDiscount1;
                    xsellShipDiscount = xsellShipDiscount + xsellShipDiscount1;

                    revCustomizationCost = 0;
                    revCustomizationCost = Convert.ToDecimal(cartDetailrow.Isrev_customization_costNull() ? 0 : cartDetailrow.rev_customization_cost);
                    totalRevCustomizationCost = totalRevCustomizationCost + revCustomizationCost;

                    l_PoDtlKeyCounter = l_iCounter;

                    prodDetails = cartDetailrow.Isproduct_detailsNull() ? "" : cartDetailrow.product_details;

                    if (prodDetails.IndexOf("Paper / Coating Type: ") > -1)
                    {
                        prodDetails = prodDetails.Replace("Paper / Coating Type: ", "Paper / Coating Type: <b>") + "</b>";
                    }

                    is_direct_mail = cartDetailrow.Isis_direct_mailNull() ? false : cartDetailrow.is_direct_mail;
                    string cardsFor = cartDetailrow.Iscards_forNull() ? "" : cartDetailrow.cards_for;
                    string designMode = cartDetailrow.Isdesign_modeNull() ? "" : cartDetailrow.design_mode;
                    string frontUrl = cartDetailrow.Isfront_image_urlNull() ? "" : cartDetailrow.front_image_url;
                    string backUrl = cartDetailrow.Isreverse_image_urlNull() ? "" : cartDetailrow.reverse_image_url;
                    string frontlayout = string.Empty;
                    string backlayout = string.Empty;
                    string sizekey = null;
                    Int32 revCardFormat = default(Int32);
                    string BackText = string.Empty;
                    string PCSize = string.Empty;
                    if (cardsFor == "PC" | cardsFor == "CM" | cardsFor == "SM")
                    {
                        frontlayout = cartDetailrow.Isfront_layoutNull() ? "" : cartDetailrow.front_layout;
                        sizekey = cartDetailrow.Issize_keyNull() ? "" : cartDetailrow.size_key.ToString();
                        if (frontlayout.IndexOf("PCOVE") >= 0 | sizekey == "32")
                        {
                            cardHeight = 129;
                            cardWidth = 200;
                            PCSize = "Jumbo ";
                        }
                        else
                        {
                            cardHeight = 122;
                            cardWidth = 157;
                            PCSize = "Standard ";
                        }
                    }
                    else if (cardsFor == "BC")
                    {
                        if (cartDetailrow.card_orientation == 1)
                        {
                            cardHeight = 290;
                            cardWidth = 147;
                        }
                    }
                    else if (cardsFor == "FL")
                    {
                        cardHeight = 182;
                        cardWidth = 150;
                    }
                    else if (cardsFor == "DH")
                    {
                        cardHeight = 321;
                        cardWidth = 119;
                    }
                    else if (cardsFor == "BK")
                    {
                        cardHeight = 310;
                        cardWidth = 90;
                    }
                    else if (cardsFor == "NP" | cardsFor == "BCCM")
                    {
                        if (prodDetails.IndexOf("5.5") >= 0 | sizekey == "187")
                        {
                            cardHeight = 323;
                            cardWidth = 208;
                        }
                        else
                        {
                            cardHeight = 466;
                            cardWidth = 208;
                        }
                    }
                    if (designMode == "UploadDesign")
                    {
                        strFrontImage = cartDetailrow.Islow_res_proof_urlNull() ? "" : cartDetailrow.low_res_proof_url;
                        //strFrontImage = ".." + strFrontImage.Substring(strFrontImage.IndexOf("/DesignCenter"))
                        strFrontImage = mCustomerPortalURL + strFrontImage.Substring(strFrontImage.IndexOf("/DesignCenter"));
                        strFrontImage = "<img src='" + strFrontImage + "' border=1 height=" + Convert.ToString(cardHeight) + " width=" + Convert.ToString(cardWidth) + ">";
                        frontlayout = cartDetailrow.Isfront_layoutNull() ? "" : cartDetailrow.front_layout;
                        revCardFormat = cartDetailrow.Isrev_card_formatNull() ? 0 : cartDetailrow.rev_card_format;
                        if (revCardFormat == 1 & frontlayout.IndexOf("<b>Front & Back:</b>") >= 0)
                        {
                            m_revPdfBackImage = "<BR><BR>" + strFrontImage.Replace("_File1.", "_File2.");
                            //String.Format(m_revPdfBackImage, strFrontImage.Replace("_File1.", "_File2."))
                        }
                        else
                        {
                            m_revPdfBackImage = "";
                        }
                        if (!string.IsNullOrEmpty(backUrl))
                        {
                            strBackImage = strFrontImage.Replace("File1.", "File2.");
                            backlayout = cartDetailrow.Isreverse_layoutNull() ? "" : cartDetailrow.reverse_layout;
                        }
                    }
                    else if (cardsFor != "CL" & cardsFor != "HH")
                    {
                        strFrontImage = cartDetailrow.Islow_res_proof_urlNull() ? "" : cartDetailrow.low_res_proof_url;

                        if (strFrontImage.IndexOf("/DesignCenter") >= 0)
                        {
                            strFrontImage = mCustomerPortalURL + strFrontImage.Substring(strFrontImage.IndexOf("/DesignCenter"));
                            strFrontImage = "<img src='" + strFrontImage + "' border=1 height=" + Convert.ToString(cardHeight) + " width=" + Convert.ToString(cardWidth) + ">";
                        }
                        else
                        {
                            strFrontImage = "<img src='" + strFrontImage + "' border=1 >";
                        }

                        if (!cartDetailrow.Isreverse_layoutNull())
                        {
                            strBackImage = strFrontImage.Replace("_1.", "_2.");
                        }
                    }
                    else if (cardsFor == "CL")
                    {
                        GetFieldsFromProfileData();
                        if (!string.IsNullOrEmpty(PhotoSource))
                        {
                            strFrontImage = PhotoSource;
                            strFrontImage = mCustomerPortalURL + strFrontImage.Substring(strFrontImage.IndexOf("/DesignCenter"));
                            strFrontImage = "<img name = photo src='" + strFrontImage + "' border=1 height=" + Convert.ToString(PhotoHeight) + " width=" + Convert.ToString(PhotoWidth) + ">";
                        }
                        if (!string.IsNullOrEmpty(LogoSource))
                        {
                            strBackImage = LogoSource;
                            strBackImage = mCustomerPortalURL + strBackImage.Substring(strBackImage.IndexOf("/DesignCenter"));
                            strBackImage = "<img name = logo  src='" + strBackImage + "' border=1 height=" + Convert.ToString(LogoHeight) + " width=" + Convert.ToString(LogoWidth) + ">";
                        }
                    }
                    if ((cardsFor == "BC" | cardsFor == "RCBC") & (frontlayout.IndexOf("<b>Front & Back:</b>") >= 0 | !string.IsNullOrEmpty(strBackImage)))
                    {
                        BackText = " With Back";
                    }
                    strClass = "bodytext";
                    //"head1"
                    if (!isFreeProdcut)
                    {
                        nonFreeproducts = nonFreeproducts + 1;
                        productCost = cartDetailrow.Isprice_per_unitNull() ? 0 : cartDetailrow.price_per_unit;
                        processCost = Convert.ToDecimal(cartDetailrow.Isprocess_costNull() ? 0 : cartDetailrow.process_cost);
                        shipOption = cartDetailrow.Isship_price_optionNull() ? "" : cartDetailrow.ship_price_option;
                        string highliteColor = GetShipColorValue(shipOption, l_iCounter);
                        shipCost = shipCost + (cartDetailrow.Isshipping_priceNull() ? 0 : cartDetailrow.shipping_price);
                        m_cartCost = m_cartCost + productCost;

                        if (is_direct_mail)
                        {
                            isNotDM = false;
                            IsUsLeads = false;
                            if (!cartDetailrow.IsusleadstokenNull())
                            {
                                if (!cartDetailrow.IsusleadstokenNull())
                                {
                                    IsUsLeads = true;
                                    usLeadsCost = Convert.ToDecimal(cartDetailrow.usleads_cost);
                                }
                            }
                            productCost = productCost + shipCost;
                            prodDetails = prodDetails + string.Format("<BR><BR><B>Scheduled Date:</B> {0}", cartDetailrow.DirectMailScheduleDt);
                            prodDetails = prodDetails + string.Format("<br><br><B>Shipping: {0}</B>", cartDetailrow.ship_price_option);
                        }
                        strText = strText + "<TR vAlign='top' bgColor='#ffffff'>";
                        strText = strText + "    <TD class='" + strClass + "'><font color=" + highliteColor + " >" + Convert.ToString(RowCount) + "</font></TD>";
                        strText = strText + "    <TD class='" + strClass + "'><font color=" + highliteColor + " >" + cartDetailrow.date_created + "</font></TD>";
                        strText = strText + "    <TD class='" + strClass + "'><font color=" + highliteColor + " ><B>" + cartDetailrow.fkey_ec_attribute + PCSize + cartDetailrow.product_id + BackText + "</B><BR>";
                        if (designMode != "UploadDesign")
                        {
                            strText = strText + prodDetails + string.Format("</font><br><BR>{0}<BR><br>{1}", strFrontImage, strBackImage);
                        }
                        else
                        {
                            frontlayout = frontlayout.Replace("#err#", "");
                            backlayout = backlayout.Replace("#err#", "");
                            strText = strText + prodDetails + string.Format("</font><br><br>{0}{1}<br>{2}<br>{3}<br>{4}", strFrontImage, m_revPdfBackImage, frontlayout, strBackImage, backlayout);
                        }
                        if (xsellProductDiscount1 > 0)
                        {
                            strText = strText + string.Format("<BR> <B>Product Discount: {0}</B>", ConvertToMoneyFormat(xsellProductDiscount1));
                        }
                        if (xsellShipDiscount1 > 0)
                        {
                            strText = strText + string.Format("<BR> <B>Shipment Discount: {0}</B>", ConvertToMoneyFormat(xsellShipDiscount1));
                        }
                        if (revCustomizationCost > 0)
                        {
                            strText = strText + string.Format("<BR> <B>Custom Back Cost: {0}</B>", ConvertToMoneyFormat(revCustomizationCost));
                        }
                        strText = strText + "</TD>";
                        strText = strText + "    <TD class='" + strClass + "'>" + "<font color=" + highliteColor + " >" + cartDetailrow.quantity + "</font></TD>";
                        strText = strText + "    <TD class='" + strClass + "' align='right'>" + "<font color=" + highliteColor + " >$" + Convert.ToString(productCost + processCost + usLeadsCost) + "</font> </TD>";
                        strText = strText + "</TR>";
                    }
                    else
                    {
                        freeShipCost = Convert.ToDecimal(cartDetailrow.Isshipping_priceNull() ? 0 : cartDetailrow.shipping_price);
                        m_cartCost = m_cartCost + freeShipCost;
                        productCost = freeShipCost;

                        strText = strText + " <TR vAlign='top' bgColor='#ffffff'> ";
                        strText = strText + "    <TD class='" + strClass + "' rowSpan='2'>" + "<font color=green >" + Convert.ToString(RowCount) + "</font></TD> ";
                        strText = strText + "    <TD class='" + strClass + "' rowSpan='2'>" + "<font color=green >" + cartDetailrow.date_created + "</font></TD>";
                        strText = strText + "    <TD class='" + strClass + "'><B>" + "<font color=green >" + cartDetailrow.product_id + " </B><BR>";
                        if (designMode != "UploadDesign")
                        {
                            strText = strText + prodDetails + string.Format("</font><br>{0}<BR><br>{1}</TD>", strFrontImage, strBackImage);
                        }
                        else
                        {
                            frontlayout = frontlayout.Replace("#err#", "");
                            backlayout = backlayout.Replace("#err#", "");
                            strText = strText + prodDetails + string.Format("</font></TD>");
                            strText = strText + "<tr vAlign='top' bgColor='#ffffff'>";
                            strText = strText + "<td class='bodytext' width='35%' valign='middle' align='center'>{0}{1},strFrontImage,m_revPdfBackImage</td>";
                            strText = strText + "<td bgcolor='#DADEEB' align='left' class=bodytext>{0},frontlayout</td>";
                            strText = strText + "</tr>";
                            if (!string.IsNullOrEmpty(backlayout))
                            {
                                strText = strText + "<tr vAlign='top' bgColor='#ffffff'>";
                                strText = strText + "<td class='bodytext' width='35%' valign='middle' align='center'>{0},strBackImage</td>";
                                strText = strText + "<td bgcolor='#DADEEB' align='left' class=bodytext>{0},backlayout</td>";
                                strText = strText + "</tr>";
                            }
                        }
                        strText = strText + "    <TD class='" + strClass + "'>" + "<font color=green >" + (cartDetailrow.IsquantityNull() ? "" : cartDetailrow.quantity.ToString()) + "</font> </TD>";
                        strText = strText + "    <TD class='" + strClass + "'><font color=green >" + "Free" + "</font></TD>";
                        strText = strText + " </TR>";
                        strText = strText + " <TR vAlign='bottom' bgColor='#ffffff'> ";
                        strText = strText + "    <TD class='" + strClass + "' align='right' colSpan='2'>" + "<font color=green >" + "<B>Shipping and Handling Charges :</B></font></TD>";
                        strText = strText + "    <TD class='" + strClass + "' align='right'>" + "<font color=green >" + (freeShipCost > 0 ? "$" + String.Format("###,##0.00", freeShipCost) : "Free") + "</font></TD>";
                        strText = strText + " </TR>";
                    }
                    RowCount = RowCount + 1;
                }
                else
                {
                    strClass = "bodytext";
                }

            }
            StateTaxOn = m_cartCost;
            m_cartCost = m_cartCost + shipCost + processCost + usLeadsCost;
            TaxAmount = StateTaxOn * GetStateTax();
            TaxAmount = Convert.ToDecimal(string.Format("{0:n2}", TaxAmount));


            if (nonFreeproducts > 0 & is_direct_mail != true)
            {
                strText = strText + "<TR bgColor='#ffffff'>";

                strText = strText + "    <TD class='bodytext' align='right' colSpan='4'><B>" + GetShipDisplayValue(shipOption, m_CartData.CartDetails.Rows.Count - 1) + ":</B></TD>";

                strText = strText + "    <TD class='bodytext' align='right'><B>" + (shipCost > 0 ? "$" + Convert.ToString(shipCost) : "Free") + "</B></TD>";
                strText = strText + "</TR>";
            }

            if (!cartBillrow.Isfkey_promoNull())
            {
                if (!cartBillrow.Ispromo_codeNull())
                {
                    promotioncode = string.Format("Promotional Discount Code: <b>{0}</b>", cartBillrow.promo_code);
                }

                productPromoCost = GetPromotionCosts("promo_cost_on_product", l_PoDtlKeyCounter);
                shipPromoCost = GetPromotionCosts("promo_cost_on_shipping", l_PoDtlKeyCounter);


                if (productPromoCost > 0)
                {
                    strText = strText + " <TR vAlign='bottom' bgColor='#ffffff'> ";
                    strText = strText + "    <TD class='bodytext' align='right' colSpan='4'><B>Promotional discount on products :</B></TD>";
                    strText = strText + "    <TD class='bodytext' align='right'><B>" + ConvertToMoneyFormat(productPromoCost) + "</B></TD>";
                    strText = strText + " </TR>";
                    m_cartCost = m_cartCost - Convert.ToDecimal(productPromoCost);
                    StateTaxOn = StateTaxOn - Convert.ToDecimal(productPromoCost);
                    TaxAmount = StateTaxOn * GetStateTax();
                }
                if (shipPromoCost > 0)
                {
                    strText = strText + " <TR vAlign='bottom' bgColor='#ffffff'> ";
                    strText = strText + "    <TD class='bodytext' align='right' colSpan='4'><B>Promotional discount on shipment :</B></TD>";
                    strText = strText + "    <TD class='bodytext' align='right'><B>" + ConvertToMoneyFormat(shipPromoCost) + "</B></TD>";
                    strText = strText + " </TR>";
                    m_cartCost = m_cartCost - Convert.ToDecimal(shipPromoCost);
                }
            }
            mcaproductDiscount = GetPromotionCosts("mca_product_discount", l_PoDtlKeyCounter);
            mcashipDiscount = GetPromotionCosts("mca_shipment_discount", l_PoDtlKeyCounter);
            multiline_ShipDiscount = GetMultiLineDiscount("multiline_shipment_discount", l_PoDtlKeyCounter);

            if (mcaproductDiscount > 0)
            {
                strText = strText + " <TR vAlign='bottom' bgColor='#ffffff'> ";
                strText = strText + "    <TD class='bodytext' align='right' colSpan='4'><B>MCA discount on products :</B></TD>";
                strText = strText + "    <TD class='bodytext' align='right'><B>" + ConvertToMoneyFormat(mcaproductDiscount) + "</B></TD>";
                strText = strText + " </TR>";
                m_cartCost = m_cartCost - Convert.ToDecimal(mcaproductDiscount);
                StateTaxOn = StateTaxOn - Convert.ToDecimal(mcaproductDiscount);
                TaxAmount = StateTaxOn * GetStateTax();
            }
            if (mcashipDiscount > 0)
            {
                strText = strText + " <TR vAlign='bottom' bgColor='#ffffff'> ";
                strText = strText + "    <TD class='bodytext' align='right' colSpan='4'><B>MCA discount on shipment :</B></TD>";
                strText = strText + "    <TD class='bodytext' align='right'><B>" + ConvertToMoneyFormat(mcashipDiscount) + "</B></TD>";
                strText = strText + " </TR>";
                m_cartCost = m_cartCost - Convert.ToDecimal(mcashipDiscount);
            }
            if (multiline_ShipDiscount > 0)
            {
                strText = strText + " <TR vAlign='bottom' bgColor='#ffffff'> ";
                strText = strText + "    <TD class='bodytext' align='right' colSpan='4'><B>Less Shipping Discount :</B></TD>";
                strText = strText + "    <TD class='bodytext' align='right'><B>" + ConvertToMoneyFormat(multiline_ShipDiscount) + "</B></TD>";
                strText = strText + " </TR>";
                m_cartCost = m_cartCost - Convert.ToDecimal(multiline_ShipDiscount);
            }
            m_cartCost = m_cartCost - (xsellShipDiscount + xsellProductDiscount);
            m_cartCost = m_cartCost + totalRevCustomizationCost;
            if (TaxAmount > 0)
            {
                strText = strText + " <TR vAlign='bottom' bgColor='#ffffff'> ";
                strText = strText + "    <TD class='bodytext' align='right' colSpan='4'><B>" + WebConfiguration.StateTaxLabel + " :</B></TD>";
                strText = strText + "    <TD class='bodytext' align='right'><B>" + ConvertToMoneyFormat(TaxAmount) + "</B></TD>";
                strText = strText + " </TR>";
                m_cartCost = m_cartCost + TaxAmount;
            }

            if (customerCreditAmt > 0)
            {
                strText = strText + " <TR vAlign='bottom' bgColor='#ffffff'> ";
                strText = strText + "    <TD class='bodytext' align='right' colSpan='4'><B>Less : Customer Credit :</B></TD>";
                strText = strText + "    <TD class='bodytext' align='right'><B>" + ConvertToMoneyFormat(customerCreditAmt) + "</B></TD>";
                strText = strText + " </TR>";
                m_cartCost = m_cartCost - Convert.ToDecimal(customerCreditAmt);
                m_cartCost = Convert.ToDecimal(string.Format("{0:n2}", m_cartCost));
            }
            if (m_customizationCost > 0)
            {
                strText = strText + " <TR vAlign='bottom' bgColor='#ffffff'> ";
                strText = strText + "    <TD class='bodytext' align='right' colSpan='4'><B>Customization Additional Fee :</B></TD>";
                strText = strText + "    <TD class='bodytext' align='right'><B>" + ConvertToMoneyFormat(m_customizationCost) + "</B></TD>";
                strText = strText + " </TR>";
                m_cartCost = m_cartCost + m_customizationCost;
                m_cartCost = Convert.ToDecimal(string.Format("{0:n2}", m_cartCost));
            }

            return strText;
        }
        protected decimal GetMultiLineDiscount(string key, Int32 l_iIndex)
        {
            CartModel.CartDetailsRow cartDetailrow = (CartModel.CartDetailsRow)m_CartData.CartDetails.Rows[l_iIndex];
            decimal strDate = 0;
            try
            {
                if (m_CartData.CartDetails.Rows.Count > 0)
                {
                    strDate = Convert.ToInt32(m_CartData.CartDetails.Rows[l_iIndex][key]);
                }
            }
            catch (Exception ex)
            {
                strDate = 0;
            }
            return strDate;
        }

        private string GetState(string fkeyState, string fkeyCountry)
        {
            if (m_CartData.States.Rows.Count > 0)
            {
                StateCountry = "";
                var list = (from collect in m_CartData.States
                            where collect.pkey_state == Convert.ToInt32(fkeyState)
                            select collect).ToList();
                lblState.Text = list[0].state_name;
                lblState1.Text = list[0].state_name;
            }
            if (m_CartData.Countries.Rows.Count > 0)
            {
                StateCountry = StateCountry + " ";
                var list = (from collect in m_CartData.Countries
                            where collect.pkey_country == Convert.ToInt32(fkeyCountry)
                            select collect).ToList();
                StateCountry = list[0].country_name;
                lblCountry.Text = StateCountry;
                lblCountry1.Text = StateCountry;
            }
            return StateCountry;
        }

        protected decimal GetPromotionCosts(string key, Int32 l_iIndex)
        {
            CartModel.CartDetailsRow cartDetailrow = (CartModel.CartDetailsRow)m_CartData.CartDetails.Rows[l_iIndex];
            decimal strDate = 0;
            try
            {
                if (m_CartData.CartDetails.Rows.Count > 0)
                {
                    strDate = Convert.ToInt32(m_CartData.CartShipInfo.Rows[l_iIndex][key]);
                }
            }
            catch (Exception ex)
            {
                strDate = 0;
            }
            return strDate;
        }

        protected string GetShipColorValue(string key, Int32 l_iIndex)
        {
            string strDate = "green";
            shipColor = "green";
            try
            {
                switch (key)
                {
                    case "PRICE_ONE":
                        shipColor = "red";
                        return "red";
                    case "PRICE_TWO":
                        shipColor = "blue";
                        return "blue";
                    case "PRICE_SEVEN":
                        shipColor = "black";
                        return "black";
                }
            }
            catch (Exception ex)
            {
                strDate = "";
            }
            return strDate;
        }
        private decimal GetStateTax()
        {
            int fkey_state;
            double tax = 0;
            try
            {
                if (m_CartData.CartShipInfo.Rows.Count > 0)
                {
                    fkey_state = Convert.ToInt32(m_CartData.CartShipInfo.Rows[0]["fkey_state"]);
                    var list = (from collect in m_CartData.States
                                where collect.pkey_state == fkey_state
                                select collect).ToList();
                    tax = WebConfiguration.GetApplicableStateTax(list[0].state_name);
                    tax = tax / 100;
                }
            }
            catch
            {
                return Convert.ToDecimal(tax);
            }
            return Convert.ToDecimal(tax);
        }
        private string GetShipDisplayValue(string shipOption, int l_iIndex)
        {
            string strDate = "";
            try
            {
                strDate = shipOption;
                switch (strDate)
                {
                    case "PRICE_ONE":
                        return "Shipping (Express 1 day air)";
                    case "PRICE_TWO":
                        return "Shipping (Priority 2-3 Days)";
                    case "PRICE_SEVEN":
                        return "Shipping (Regular 4-5 day ground)";
                    case "PRICE_FOURTEEN":
                        return "Shipping (Standard Ground 6-10 business days)";
                    default:
                        return "Shipping (Standard Ground 6-10 business days)";
                }
            }
            catch (Exception)
            {
                strDate = "";
            }
            return strDate;
        }

        private void GetFieldsFromProfileData()
        {
            try
            {
                if (m_CartData.CartDesignProfile.Rows.Count > 0)
                {
                    CartModel.CartDesignProfileRow carProfileRow = (CartModel.CartDesignProfileRow)m_CartData.CartDetails.Rows[0];

                    if (!carProfileRow.IsphotosourceNull())
                    {
                        PhotoSource = carProfileRow.photosource;
                        InitializeImageProps(PhotoSource);
                    }

                    if (!carProfileRow.Iscompany_logoNull())
                    {
                        LogoSource = carProfileRow.company_logo;
                        InitializeLogoProps(LogoSource);
                    }
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        private void InitializeLogoProps(string strImageUrl)
        {
            try
            {
                string strServerPath = System.Web.HttpContext.Current.Server.MapPath(".").Replace("\\", "/");
                strServerPath = strServerPath.Replace("/DesignCenter", "/DesignCenter/");

                double convFactor = 1.0;
                string ImgSecHalf = null;
                Int16 IndexOfDesignCenter = default(Int16);
                IndexOfDesignCenter = Convert.ToInt16(strImageUrl.IndexOf("/DesignCenter/") + 14);
                strServerPath = strServerPath.Replace("Transactions", "DesignCenter/");

                ImgSecHalf = strImageUrl.Replace(strImageUrl.Substring(0, IndexOfDesignCenter), strServerPath);
                string strImgURL = ImgSecHalf;
                //Holds manipulated image path
                ImageProcessor imp = new ImageProcessor(ImgSecHalf);

                double aspectRatio = imp.Width / imp.Height;
                aspectRatio = Math.Min(3.0, aspectRatio);

                if (strImageUrl.IndexOf("/BPBLogoLibrary/") > -1)
                {
                    LogoWidth = "156";

                    LogoHeight = "62";


                }
                else if (imp.Height * convFactor < Convert.ToDouble(LogoHeight))
                {
                    LogoHeight = string.Format("{0:n0}", imp.Height * convFactor);
                    LogoWidth = string.Format("{0:n0}", imp.Height * aspectRatio * convFactor);
                }
                else
                {
                    if (aspectRatio <= 0.85)
                    {
                        LogoWidth = (Convert.ToDouble(LogoHeight) * 0.7).ToString();

                    }
                    else if (aspectRatio > 0.85 & aspectRatio <= 1.12)
                    {
                        LogoWidth = (Convert.ToDouble(LogoHeight) * 1.0).ToString();

                    }
                    else if (aspectRatio > 1.12 & aspectRatio <= 1.37)
                    {
                        LogoWidth = (Convert.ToDouble(LogoHeight) * 1.25).ToString();

                    }
                    else if (aspectRatio > 1.37 & aspectRatio <= 1.62)
                    {
                        LogoWidth = (Convert.ToDouble(LogoHeight) * 1.5).ToString();

                    }
                    else if (aspectRatio > 1.62 & aspectRatio <= 1.87)
                    {
                        LogoWidth = (Convert.ToDouble(LogoHeight) * 1.75).ToString();

                    }
                    else if (aspectRatio > 1.87 & aspectRatio <= 2.12)
                    {
                        LogoWidth = (Convert.ToDouble(LogoHeight) * 2.0).ToString();

                    }
                    else if (aspectRatio > 2.12 & aspectRatio <= 2.37)
                    {
                        LogoWidth = (Convert.ToDouble(LogoHeight) * 2.25).ToString(); ;

                    }
                    else if (aspectRatio > 2.37 & aspectRatio <= 2.62)
                    {
                        LogoWidth = (Convert.ToDouble(LogoHeight) * 2.5).ToString();

                    }
                    else if (aspectRatio > 2.62 & aspectRatio <= 2.87)
                    {
                        LogoWidth = (Convert.ToDouble(LogoHeight) * 2.75).ToString();

                    }
                    else if (aspectRatio > 2.87)
                    {
                        LogoWidth = (Convert.ToDouble(LogoHeight) * Math.Min(3.0, aspectRatio)).ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
            }
        }

        private void InitializeImageProps(string strImageUrl)
        {
            try
            {
                string strServerPath = System.Web.HttpContext.Current.Server.MapPath(".").Replace("\\", "/");

                strServerPath = strServerPath.Replace("Transactions", "DesignCenter/");
                PhotoHeight = "62";
                PhotoWidth = "44";

                string ImgSecHalf = null;
                Int16 IndexOfDesignCenter = default(Int16);
                IndexOfDesignCenter = Convert.ToInt16(strImageUrl.IndexOf("/DesignCenter/") + 14);
                ImgSecHalf = strImageUrl.Replace(strImageUrl.Substring(0, IndexOfDesignCenter), strServerPath);
                string strImgURL = ImgSecHalf;
                //Holds manipulated image path
                ImageProcessor imp = new ImageProcessor(strImgURL);

                if (imp.Height < Convert.ToDouble(PhotoHeight))
                {
                    PhotoHeight = string.Format("{0:n0}", imp.Height);
                    PhotoWidth = string.Format("{0:n0}", imp.Width);
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
            }
        }
        protected string GetShipInfoIsUSPSOrder()
        {

            try
            {
                if (m_CartData.CartShipInfo.Rows.Count > 0)
                {
                    CartModel.CartShipInfoRow shipInfo = (CartModel.CartShipInfoRow)m_CartData.CartShipInfo.Rows[0];
                    if ((Convert.ToBoolean(shipInfo.IsUSPSOrder) && isNotDM))
                    {
                        return "<p class=\"bodytext\"><FONT color=\"#ff0000\">* Your Order will be Shipped via USPS.</font><br>We cannot guarantee that your package will arrive within the time provided. We also do not provide any tracking numbers for orders shipped to PO boxes through USPS.</p>";
                    }
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
            return "";
        }
    }
}