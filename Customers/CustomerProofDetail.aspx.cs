﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using System.Data;

namespace Bestprintbuy.Admin.Web.Customers
{
    public partial class CustomerProofDetail : PageBase
    {
        protected string Otherreasons;
        protected string creditDesc;
        Int32 proofkey;
        Int32 pageBindError = 0;
        protected bool rowExits = false;
        protected CustomerProofModel m_CustProofData = new CustomerProofModel();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if ((Request["PkeyProof"] != null))
                {
                    proofkey = Convert.ToInt32(Request["PkeyProof"]);
                }
                m_CustProofData = CustomerService.GetCustomerProfDetails(proofkey, ConnString);
                InitPageData();
            }
            try
            {
                lblPONumber.Text = Request["PONumber"].ToString();
            }
            catch { }
        }

        private void InitPageData()
        {
            try
            {
                if (pageBindError != 1)
                {
                    rptrComments.DataSource = m_CustProofData.ProofComments;
                    rptrComments.DataBind();
                    if (m_CustProofData.CustomerProofs.Rows.Count == 1)
                    {
                        rowExits = true;
                        CustomerProofModel.CustomerProofsRow pRow = (CustomerProofModel.CustomerProofsRow)m_CustProofData.CustomerProofs.Rows[0];
                        btnApprove.Visible = false;
                        btnSendNotification.Visible = false;
                        if (pRow.isproofvalid == false & Identity.IsSuperUser)
                        {
                            btnApprove.Visible = true;
                        }
                        txtAdminComments.Visible = false;
                        btnReply.Visible = false;
                        if (pRow.isconfirmed == false & Identity.AllowCustomerCommunication)
                        {
                            txtAdminComments.Visible = true;
                            btnReply.Visible = true;
                            btnSendNotification.Visible = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                pageBindError = 1;
                AlertScript(ex.Message);
            }

            btnClose.PostBackUrl = "CustomerProofs.aspx?poDtlKey=" + GetValue("fkey_cust_po_dtl");
            Page.DataBind();
        }

        protected string GetValue(string key)
        {
            try
            {
                if (pageBindError != 3 & rowExits)
                {
                    if (!Convert.IsDBNull(m_CustProofData.CustomerProofs.Rows[0][key]))
                    {
                        return Convert.ToString(m_CustProofData.CustomerProofs.Rows[0][key]);
                    }
                }
            }
            catch (Exception ex)
            {
                pageBindError = 3;
                AlertScript(ex.Message);
            }
            return "";
        }

        protected string GetImageUrl(object key)
        {

            if (!Convert.IsDBNull(m_CustProofData.CustomerProofs.Rows[0][key.ToString()]))
            {
                string imageurl = Convert.ToString(m_CustProofData.CustomerProofs.Rows[0][key.ToString()]);
                if (!string.IsNullOrEmpty(imageurl))
                {
                    string strUrl = "<a href='javascript:;' {0} class='a1b'><img src='{1}' border=0  width=100px></a>";
                    string strOnClick = "onclick=\"javascript:ShowImgPreview('{0}'); return false;\"";
                    strOnClick = string.Format(strOnClick, imageurl);
                    return string.Format(strUrl, strOnClick, imageurl);
                }
                return "N/A";
            }
            return "N/A";
        }


        protected string GetDateValue(string key)
        {
            try
            {
                if (pageBindError != 3 & rowExits)
                {
                    if (!Convert.IsDBNull(m_CustProofData.CustomerProofs.Rows[0][key]))
                    {
                        return Convert.ToDateTime(m_CustProofData.CustomerProofs.Rows[0][key]).ToString("MM/dd/yyyy");
                    }
                    else
                    {
                        return DateTime.Now.ToString("MM/dd/yyyy");
                    }
                }
                else
                {
                    return DateTime.Now.ToString("MM/dd/yyyy");
                }
            }
            catch (Exception ex)
            {
                pageBindError = 3;
                AlertScript(ex.Message);
            }
            return DateTime.Now.ToString("MM/dd/yyyy");
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            proofkey = Convert.ToInt32(PkeyProof.Value);
            m_CustProofData = CustomerService.ApproveProofAndGetDetails(proofkey, ConnString);
            InitPageData();
        }

        protected void btnReply_Click(object sender, EventArgs e)
        {
            proofkey = Convert.ToInt32(PkeyProof.Value);
            if (string.IsNullOrEmpty(txtAdminComments.Value.Trim()))
            {
                m_CustProofData = CustomerService.ApproveProofAndGetDetails(proofkey, ConnString);
                InitPageData();
            }
            else
            {
                m_CustProofData = CustomerService.PostCommentsAndGetDetails(proofkey, txtAdminComments.Value.Trim(), Identity.UserPK, ConnString);
                txtAdminComments.Value = "";
                InitPageData();
                SendEmail();
            }
        }

        private void SendEmail()
        {
            try
            {
                string subject = "Regarding Proof " + GetValue("proof_name");

                string strMessage = null;
                strMessage = ReadMailContent1("AdminProofCommentNotification");
                strMessage = string.Format(strMessage, GetValue("proof_name"), GetValue("CustomerName"));
                strMessage = strMessage.Replace("&#123;", "{");
                strMessage = strMessage.Replace("&#125;", "}");

                EmailProcessor.SendMail(WebConfiguration.BPBCustomerCareEmail, GetValue("EMAIL_ID"), strMessage, subject);

            }
            catch (Exception ex)
            {
            }
        }

        private void SendNotificationEmail()
        {
            try
            {
                string subject = null;
                subject = "Regarding Proof " + GetValue("proof_name");

                string strMessage = null;
                strMessage = ReadMailContent1("ProofNotificationEmail");
                strMessage = string.Format(strMessage, GetValue("proof_name"), GetValue("CustomerName"));
                strMessage = strMessage.Replace("&#123;", "{");
                strMessage = strMessage.Replace("&#125;", "}");

                EmailProcessor.SendMail(WebConfiguration.BPBCustomerCareEmail, GetValue("EMAIL_ID"), strMessage, subject);
            }
            catch (Exception ex)
            {
            }
        }

        private string ReadMailContent1(string strMailFor)
        {
            try
            {
                string strFile = null;
                string strContent = null;
                strFile = WebConfiguration.GetEmailHTMLLoc(strMailFor);
                System.IO.StreamReader file = new System.IO.StreamReader(strFile);
                strContent = file.ReadToEnd();
                file.Close();
                file = null;
                return strContent;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }

        protected void btnSendNotification_Click(object sender, EventArgs e)
        {
            try
            {
                proofkey = Convert.ToInt32(PkeyProof.Value);
                m_CustProofData = CustomerService.ApproveProofAndGetDetails(proofkey, ConnString);
                InitPageData();
                SendNotificationEmail();

            }
            catch (Exception ex)
            {
                AlertScript(ex.Message);
                //Initialize the data that has to be displayed on the page.
                InitPageData();
            }
        }
    }
}