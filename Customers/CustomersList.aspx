﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="CustomersList.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Customers.CustomersList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server"> 
     <script>
         $().ready(function () {
             $("#CustomerList").validate();
             SearchText();
         });

         function SearchText() {
             $(".autosuggest").autocomplete({
                 source: function (request, response) {
                     $.ajax({
                         type: "POST",
                         contentType: "application/json; charset=utf-8",
                         url: "CustomersList.aspx/GetAutoCompleteData",
                         data: "{'input':'"+$("#ContentPlaceHolder1_txtPattern").val()+"', field: '"+$("#ContentPlaceHolder1_ddlFilterFor option:selected").index()+"'}",
                         dataType: "json",
                         success: function (data) {
                             response(data.d);
                         },
                         error: function (result) {
                             alert("Error");
                         }
                     });
                 },
                 select: function (e, ui) {
                     $("#ContentPlaceHolder1_txtPattern").val(ui.item.value);
                     __doPostBack('ctl00$ContentPlaceHolder1$btnGo', '');
                     return true;
                 }
             });
         }

         function ChangeIndex() {
             $("#ContentPlaceHolder1_txtPattern").val("");
         }
</script>
    <div class="clr"></div>
    <!-- Container Starts -->
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Customer Service</li>
            <li>Customers List</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Customers List</h2>
        <form id="CustomerList" name="CustomerList" runat="server" class="form">
            <table cellpadding="0" cellspacing="0" border="0" width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="7%">Filter List for </td>
                        <td width="15%">
                            <asp:DropDownList name="ddlFilterFor" ID="ddlFilterFor" runat="server" onchange="ChangeIndex();">
                                <asp:ListItem Value="pps_customers.email_id">Email Address</asp:ListItem>
                                <asp:ListItem Value="pps_customers.first_name">First Name</asp:ListItem>
                                <asp:ListItem Value="pps_customers.last_name">Last Name</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                         <td width="1%"></td>
                        <td width="15%">
                            <asp:DropDownList name="ddlOperator" ID="ddlOperator" runat="server">
                                <asp:ListItem Value="=">Equals</asp:ListItem>
                                <asp:ListItem Value="<>">Not Equals</asp:ListItem>
                                <asp:ListItem Value="LIKE%">Starts With</asp:ListItem>
                                <asp:ListItem Value="%LIKE">Ends with</asp:ListItem>
                                <asp:ListItem Value="%LIKE%">Contains</asp:ListItem>
                            </asp:DropDownList></td> 
                        <td width="1%"></td>
                        <td width="15%">
                            <asp:TextBox name="txtPattern" CssClass="text autosuggest" ID="txtPattern" runat="server" required></asp:TextBox>
                        </td>
                        <td width="5%">
                            <asp:Button runat="server" ID="btnGo" CssClass="button rounded" Text="GO" OnClick="btnGo_Click"></asp:Button>
                        </td>
                        <td width="5%">                            
                            <asp:Button runat="server" ID="btnReset" CssClass="button rounded" Text="Reset" OnClick="btnReset_Click" Visible="false" UseSubmitBehavior="false"></asp:Button>
                        </td>
                        <td width="35%"></td>
                    </tr>
                </tbody>
            </table>           
            <div class="clr"></div>
            <asp:GridView ID="CustomerslistGrid" runat="server" AutoGenerateColumns="false" EnableViewState="true"
                ForeColor="Black" Width="100%" PageSize="20" AllowPaging="true" AllowSorting="true"
                class="table" name="CustomerslistGrid" OnPageIndexChanging="CustomerslistGrid_PageIndexChanging" OnSorting="CustomerslistGrid_Sorting">
                <PagerStyle HorizontalAlign="Right" CssClass="dtPage"></PagerStyle>
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />
                <Columns>
                    <asp:TemplateField HeaderText="#SL">
                        <HeaderStyle Width="6%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <span><%# GetRecordCountForPage()%></span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="First Name" SortExpression="cus_firstname">
                        <HeaderStyle Width="30%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container,"DataItem.cus_firstname") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Last Name" SortExpression="cus_lastname">
                        <HeaderStyle Width="30%" ForeColor="White" />
                        <ItemTemplate>
                            <a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("CustomerRegistration", "view")%>','CustomerRegistration.aspx?Customerpkey=<%# DataBinder.Eval(Container,"DataItem.pkey_customer") %>')" data-gravity="s" title="Edit"><%# DataBinder.Eval(Container,"DataItem.cus_lastname") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Email Address" SortExpression="customer_id">
                        <HeaderStyle Width="30%" ForeColor="White" />
                        <ItemTemplate>
                            <a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("CustomerRegistration", "view")%>','CustomerRegistration.aspx?Customerpkey=<%# DataBinder.Eval(Container,"DataItem.pkey_customer") %>')" data-gravity="s" title="Edit"><%# DataBinder.Eval(Container,"DataItem.customer_id") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <div class="ttl"><b>Total Number of Customers: </b><%=recordCount%></div>
            <!-- table Ends -->
            <div class="btn_lst">
                <div class="rgt">
                    <asp:Button ID="btnClose" Text="Close" CssClass="button rounded" runat="server" OnClick="btnClose_Click" UseSubmitBehavior="false"></asp:Button>
                </div>
            </div>
        </form>
    </div>
</asp:Content>
