﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using ImageMagickObject;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.Customers
{
    public partial class CustOrderNotes : PageBase
    {
        private string oldPdfFile = "";
        protected string poNumber = "";
        protected string soNumber = "";
        protected string cardsFor = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                trUpload.Visible = false;
                trNote.Visible = false;
                IsUpload.Value = "false";
                if (Request.QueryString["IsUpload"] != null)
                {
                    trUpload.Visible = true;
                    IsUpload.Value = "true";
                }
                else
                    trNote.Visible = true;
                hdnPkeyCustPoDtl.Value = Request.QueryString["pkey_cust_po_dtl"] != null ? Request.QueryString["pkey_cust_po_dtl"] : "-1";
                hdnPkeyBrPoDtl.Value = Request.QueryString["pkey_br_po_dtl"] != null ? Request.QueryString["pkey_br_po_dtl"] : "-1";
                BindPrintFulfilmentNotes(Convert.ToInt32(Request.QueryString["pkey_cust_po_dtl"] != null ? Request.QueryString["pkey_cust_po_dtl"] : "-1"));
            }
        }

        private void BindPrintFulfilmentNotes(int pkey_cust_po_dtl)
        {
            try
            {
                SupplierPOModel poModel = CustomerService.GetPrintFulfilmentNotes(pkey_cust_po_dtl, ConnString);
                if (poModel.CustOrders.Rows.Count > 0)
                {
                    SupplierPOModel.CustOrdersRow row = poModel.CustOrders[0];
                    hdnHiresProofUrl.Value = row.hi_res_proof_url;
                    hdnLowresProofUrl.Value = row.low_res_proof_url;
                    hdnCustomerKey.Value = row.pkey_customer.ToString();
                    hdnHasback.Value = row.hasback;
                    hdnSizeKey.Value = row.sizekey.ToString();
                    txtNote.Text = row.PFNotes;
                    lblPONumber.Text = row.ordernumber;
                    lblSONumber.Text = row.ordernumber + "-" + hdnPkeyBrPoDtl.Value;
                    poNumber = row.ordernumber;
                    soNumber = row.ordernumber + "-" + hdnPkeyBrPoDtl.Value;
                    cardsFor = row.cards_for;
                }

            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                SupplierPOModel poModel = new SupplierPOModel();
                if (Convert.ToBoolean(IsUpload.Value))
                {
                    UploadPDF();

                    if (oldPdfFile == "")
                        poModel = CustomerService.GetPrintFulfilmentNotes(Convert.ToInt32(hdnPkeyCustPoDtl.Value), ConnString);
                    else
                        poModel = CustomerService.UpdatePrintAssetFile(Convert.ToInt32(hdnPkeyCustPoDtl.Value), oldPdfFile, ConnString);

                }
                else
                    poModel = CustomerService.UpdatePrintFulfilmentNotes(Convert.ToInt32(hdnPkeyCustPoDtl.Value), txtNote.Text, ConnString);
                AlertScript("Successfully Saved.");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript("Save Failed. Try again.");
            }
            
        }

        private void UploadPDF()
        {
            string errorMessage = "";
            try
            {
                if (uploadPdf.PostedFile.ContentLength > 12288000)
                {
                    AlertScript("The file uploaded is bigger than allowed size (12MB). Try uploading a smaller file.");
                    hdnErrMessage.Value = errorMessage;
                }
                string extension = Path.GetExtension(uploadPdf.PostedFile.FileName).ToLower();
                if (extension != ".pdf")
                {
                    AlertScript("The file uploaded is not a valid pdf file.");
                    hdnErrMessage.Value = errorMessage;
                    return;
                }

                string strOldPdfFile = hdnHiresProofUrl.Value;
                string strRenamePdfFile = "";
                if (File.Exists(strOldPdfFile))
                {
                    strRenamePdfFile = "RN_" + DateTime.Now.ToString("MMddyyyy") + "_" + Path.GetFileName(strOldPdfFile);
                    strRenamePdfFile = Path.Combine(Path.GetDirectoryName(strOldPdfFile), strRenamePdfFile);
                    FileInfo fi = new FileInfo(strOldPdfFile);
                    fi.CopyTo(strRenamePdfFile, true);
                    fi.Delete();
                    fi = null;
                }
                uploadPdf.PostedFile.SaveAs(strOldPdfFile);
                GenerateLowResImage(strOldPdfFile);
                oldPdfFile = strRenamePdfFile;
                AlertScript("The file is successfully replaced.");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }
        public string GetFolderPath()
        {
            string strServerPath = Server.MapPath("~");
            strServerPath = Path.Combine(strServerPath, "UploadedFiles");
            return strServerPath;
        }
        private void GenerateLowResImage(string strPdfFile)
        {
            try
            {
                string strLowresImage = "";
                string strLowresImageBack = "";
                string lowresImageDim = "417x245";
                string strRenamePdfFile = "";

                if (hdnSizeKey.Value == "10")
                    lowresImageDim = "526x405";
                else if (hdnSizeKey.Value == "32")
                    lowresImageDim = "817x526";
                else if (hdnSizeKey.Value == "100")
                    lowresImageDim = "240x828";
                else if (hdnSizeKey.Value == "164")
                    lowresImageDim = "396x1068";
                else if (hdnSizeKey.Value == "182")
                    lowresImageDim = "417x936";
                else if (hdnSizeKey.Value == "207")
                    lowresImageDim = "417x648";

                strLowresImage = hdnLowresProofUrl.Value;
                string basePath = String.Format("{0}/{1}/Users/{2}/{3}/", ConfigurationManager.AppSettings["BPBCustomerBaseDirectory"], "DesignCenter", hdnCustomerKey.Value, "Proofs");

                if (File.Exists(strLowresImage))
                {
                    FileInfo fi = new FileInfo(strLowresImage);
                    strRenamePdfFile = basePath + "RN_" + fi.Name;
                    fi.CopyTo(strRenamePdfFile, true);
                    fi = null;
                }

                if (hdnHasback.Value == "Y")
                {
                    string revFile = strLowresImage.Replace("_1.GIF", "_2.GIF");
                    if (File.Exists(revFile))
                    {
                        FileInfo fi = new FileInfo(revFile);
                        strRenamePdfFile = basePath + "RN_" + fi.Name;
                        fi.CopyTo(strRenamePdfFile, true);
                        fi = null;
                    }
                }
                string imgName = strLowresImage.Replace('/', '\\');
                imgName = imgName.Substring(imgName.LastIndexOf("\\") + 1, imgName.Length - (imgName.LastIndexOf("\\") + 1));

                object[] obj = new object[6];

                ImageMagickObject.MagickImage im = new ImageMagickObject.MagickImage();

                obj[0] = "-density";
                obj[1] = "300x300";
                obj[2] = "-resize";
                obj[3] = lowresImageDim;
                obj[4] = strPdfFile + "[0]";
                obj[5] = basePath + imgName;
                im.Convert(obj);

                if (hdnHasback.Value == "Y")
                {
                    obj[0] = "-density";
                    obj[1] = "300x300";
                    obj[2] = "-resize";
                    obj[3] = lowresImageDim;
                    obj[4] = strPdfFile + "[1]";
                    obj[5] = basePath + imgName.Replace("_1.GIF", "_2.GIF");
                    im.Convert(obj);
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript("GenerateLowResImage:" + ex.Message);
                Bestprintbuy.Admin.Utilities.FileLogger.WriteException(ex);
            }
        }
    }
}