﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="MCACustomerList.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Customers.MCACustomerList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function addCustomer() {
            $("#ContentPlaceHolder1_txtPattern").removeAttr("required");
            if ('<%=HasAccess("MCACustomerList", "add")%>' == 'False') {
                 alert('User does not have access.');
                 return false;
             }
             return true;
        }

        function ExportData() {
            var url = "ExportOrders.aspx?MCACustomerList=true&ts=<%# DateTime.Now.Ticks%>";
            window.open(url, "ExportData", "resizable=1,scrollbars=1,toolbar=0,menubar=0");
        }
        $().ready(function () {
            $("#frmMCACustomerList").validate();
        });
    </script>
    <div class="clr"></div>
    <!-- Container Starts -->
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Customer Service</li>
            <li>MCA Customers List</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>MCA Customers List</h2>
        <form id="frmMCACustomerList" name="frmMCACustomerList" runat="server" class="form">
                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="accounts-table">
                    <tbody>
                        <tr>
                            <td width="7%">Filter for </td>
                            <td width="15%">
                                <asp:DropDownList name="ddlkiosks" ID="ddlkiosks" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlkiosks_SelectedIndexChanged">
                                    <asp:ListItem Value=""></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                        <td width="7%">Filter List for </td>
                        <td width="15%">
                            <asp:DropDownList name="ddlFilterFor" ID="ddlFilterFor" runat="server">
                                <asp:ListItem Value="pps_customers.email_id">Email Address</asp:ListItem>
                                <asp:ListItem Value="pps_customers.first_name">First Name</asp:ListItem>
                                <asp:ListItem Value="pps_customers.last_name">Last Name</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                         <td width="1%"></td>
                        <td width="15%">
                            <asp:DropDownList name="ddlOperator" ID="ddlOperator" runat="server">
                                <asp:ListItem Value="=">Equals</asp:ListItem>
                                <asp:ListItem Value="<>">Not Equals</asp:ListItem>
                                <asp:ListItem Value="LIKE%">Starts With</asp:ListItem>
                                <asp:ListItem Value="%LIKE">Ends with</asp:ListItem>
                                <asp:ListItem Value="%LIKE%">Contains</asp:ListItem>
                            </asp:DropDownList></td> 
                        <td width="1%"></td>
                        <td width="15%">
                            <asp:TextBox name="txtPattern" CssClass="text" ID="txtPattern" runat="server" required></asp:TextBox>
                        </td>
                        <td width="5%">
                            <asp:Button runat="server" ID="btnGo" CssClass="button rounded" Text="GO" OnClick="btnGo_Click"></asp:Button>
                        </td>
                        <td width="5%">                            
                            <asp:Button runat="server" ID="btnReset" CssClass="button rounded" Text="Reset" OnClick="btnReset_Click" Visible="false" UseSubmitBehavior="false"></asp:Button>
                        </td>
                        <td width="25%"></td>
                    </tr>
                    </tbody>
                </table>           
            <div class="clr"></div>

            <asp:GridView ID="CustomerslistGrid" runat="server" AutoGenerateColumns="false" EnableViewState="true"
                ForeColor="Black" Width="100%" PageSize="20" AllowPaging="true" AllowSorting="true"
                class="table" name="CustomerslistGrid" OnPageIndexChanging="CustomerslistGrid_PageIndexChanging" OnSorting="CustomerslistGrid_Sorting">
                <PagerStyle HorizontalAlign="Right" CssClass="dtPage"></PagerStyle>
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />
                <Columns>
                    <asp:TemplateField HeaderText="#SL">
                        <HeaderStyle Width="6%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <span><%# GetRecordCountForPage()%></span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Name" SortExpression="customer_name">
                        <HeaderStyle Width="15%" ForeColor="White" />
                        <ItemTemplate>
                            <a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("MCACustomerDetails", "view")%>','MCACustomerDetails.aspx?Customerpkey=<%# DataBinder.Eval(Container,"DataItem.pkey_customer") %>')" title="Edit"><%# DataBinder.Eval(Container,"DataItem.customer_name") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Email" SortExpression="email">
                        <HeaderStyle Width="15%" ForeColor="White" />
                        <ItemTemplate>
                            <a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("MCACustomerDetails", "view")%>','MCACustomerDetails.aspx?Customerpkey=<%# DataBinder.Eval(Container,"DataItem.pkey_customer") %>')" data-gravity="s" title="Edit"><%# DataBinder.Eval(Container,"DataItem.email") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Phone" SortExpression="phone">
                        <HeaderStyle Width="11%" ForeColor="White" />
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container,"DataItem.phone") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="OP Name">
                        <HeaderStyle Width="11%" />
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container,"DataItem.op_name") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="OP Phone">
                        <HeaderStyle Width="11%" />
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container,"DataItem.op_phone") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Product Discount Level">
                        <HeaderStyle Width="15%" />
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container,"DataItem.product_discount_level") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Ship Discount Level">
                        <HeaderStyle Width="15%" />
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container,"DataItem.shipment_discount_level") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
             <div class="ttl"><b>Total Number of Customers: </b><%=recordCount%></div>
            <!-- table Ends -->
            <div class="btn_lst">
                <div class="lft">
                    <asp:Button runat="server" ID="btnCreateMCAccount" CssClass="button rounded" Text="Create MCA Account" OnClientClick="return addCustomer();" OnClick="btnCreateMCAccount_Click"></asp:Button>
                </div>
                <div>
                    <asp:Button ID="btnClose" Text="Close" CssClass="button rounded" runat="server" OnClick="btnClose_Click" UseSubmitBehavior="false"></asp:Button>
                    <asp:Button runat="server" ID="btnExportExcel" Text="Export to Excel" CssClass="button rounded" OnClientClick="javascript:ExportData();" UseSubmitBehavior="false"></asp:Button>
                    <asp:Button runat="server" ID="btnAssignMCAstatus" CssClass="button rounded" Text="Assign MCA Status to an Account" OnClick="btnAssignMCAstatus_Click" UseSubmitBehavior="false"></asp:Button>
                </div>
            </div>
        </form>
    </div>
</asp:Content>
