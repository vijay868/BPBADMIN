﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Collections;
using System.Collections.Specialized;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Services;
using System.Configuration;

namespace Bestprintbuy.Admin.Web.Customers
{
    public partial class TemplateSearch : PageBase
    {
        protected int serialNo;
        protected string mCustomerPortalURL = "";
        protected int recordCount = 0;
        public static ProductTemplateModel model = new ProductTemplateModel();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            mCustomerPortalURL = customerPotralUrl + "/DesignCenter/";
        }

        private void LoadData()
        {
            try
            {
                string strName = string.Format("%{0}%", txtTemplateName.Text);
                model = TemplateService.GetTemplatesByName(strName, ConnString);
                recordCount = model.TemplatesByName.Rows.Count;
                TmpltSrchGrid.DataSource = model.TemplatesByName;
                TmpltSrchGrid.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            TmpltSrchGrid.PageIndex = 0;
            LoadData();
        }

        protected void TmpltSrchGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            TmpltSrchGrid.PageIndex = e.NewPageIndex;
            LoadData();
        }

        protected void TmpltSrchGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            SortGridView(TmpltSrchGrid, e);
            TmpltSrchGrid.PageIndex = 0;
            LoadData();
        }

        protected int GetRecordCountForPage()
        {
            return GetRecordCountForPageSize(TmpltSrchGrid.PageSize, TmpltSrchGrid.PageIndex + 1);
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/DashBoard.aspx");
        }

        [WebMethod]
        public static List<string> GetAutoCompleteData(string templatename)
        {
            List<string> result = new List<string>();
            string connstring = ConfigurationManager.AppSettings["BestPrintBuyDBConn"];
            model = TemplateService.GetTemplatesByName("%" + templatename + "%", connstring);
            result = (from c in model.TemplatesByName
                      where c.template_name.ToLower().Contains(templatename.ToLower())
                    select c.template_name).Take(10).ToList(); 
            return result;
        }
    }
}