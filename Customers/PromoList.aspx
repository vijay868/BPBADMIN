﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="PromoList.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Customers.PromoList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <script type="text/javascript">
         function addPromotion() {
             $("#ContentPlaceHolder1_txtPattern").removeAttr("required");
             if ('<%=HasAccess("PromoList", "add")%>' == 'False') {
                 alert('User does not have access.');
                 return false;
             }
             return true;
         }

         $().ready(function () {
             $("#frmPromotionsList").validate(); 
             SearchText();
         });

         function SearchText() {
             $(".autosuggest").autocomplete({
                 source: function (request, response) {
                     $.ajax({
                         type: "POST",
                         contentType: "application/json; charset=utf-8",
                         url: "PromoList.aspx/GetAutoCompleteData",
                         data: "{'input':'" + $("#ContentPlaceHolder1_txtPattern").val() + "', field: '" + $("#ContentPlaceHolder1_ddlFilterFor option:selected").index() + "', status: '" + $("#ContentPlaceHolder1_ddlStatus option:selected").val() + "'}",
                         dataType: "json",
                         success: function (data) {
                             response(data.d);
                         },
                         error: function (result) {
                             alert("Error");
                         }
                     });
                 },
                 select: function (e, ui) {
                     $("#ContentPlaceHolder1_txtPattern").val(ui.item.value);
                     __doPostBack('ctl00$ContentPlaceHolder1$btnGo', '');
                     return true;
                 }
             });
         }

         function ChangeIndex() {
             $("#ContentPlaceHolder1_txtPattern").val("");
         }
</script>
<div class="clr"></div>
    <!-- Container Starts -->
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Customer Service</li>
            <li>Promotion List</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Promotion List</h2>
        <form id="frmPromotionsList" name="frmPromotionsList" runat="server" class="form">
                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="accounts-table">
                    <tbody>
                        <tr>
                            <td width="7%">Filter for </td>
                            <td width="15%">
                                <asp:DropDownList name="ddlStatus" ID="ddlStatus" runat="server" onchange="ChangeIndex();">
                                     <asp:ListItem Value="-1">All Promotions</asp:ListItem>
                                    <asp:ListItem Value="1">Active Promotions</asp:ListItem>
                                    <asp:ListItem Value="0">Inactive Promotions</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                        <td width="7%">Filter List for </td>
                        <td width="15%">
                            <asp:DropDownList name="ddlFilterFor" ID="ddlFilterFor" runat="server" onchange="ChangeIndex();">
                                <asp:ListItem Value="pps_promo_master.promo_code">Promo Code</asp:ListItem>
                                <asp:ListItem Value="pps_promo_master.description">Promo Description</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                         <td width="1%"></td>
                        <td width="15%">
                            <asp:DropDownList name="ddlOperator" ID="ddlOperator" runat="server">
                                <asp:ListItem Value="=">Equals</asp:ListItem>
                                <asp:ListItem Value="<>">Not Equals</asp:ListItem>
                                <asp:ListItem Value="LIKE%">Starts With</asp:ListItem>
                                <asp:ListItem Value="%LIKE">Ends with</asp:ListItem>
                                <asp:ListItem Value="%LIKE%">Contains</asp:ListItem>
                            </asp:DropDownList></td> 
                        <td width="1%"></td>
                        <td width="15%">
                            <asp:TextBox name="txtPattern" CssClass="text autosuggest" ID="txtPattern" runat="server" required></asp:TextBox>
                        </td>
                        <td width="5%">
                            <asp:Button runat="server" ID="btnGo" CssClass="button rounded" Text="GO" OnClick="btnGo_Click"></asp:Button>
                        </td>
                        <td width="5%">                            
                            <asp:Button runat="server" ID="btnReset" CssClass="button rounded" Text="Reset" OnClick="btnReset_Click" Visible="false" UseSubmitBehavior="false"></asp:Button>
                        </td>
                        <td width="25%"></td>
                    </tr>
                    </tbody>
                </table>           
            <div class="clr"></div>

            <asp:GridView ID="PromotionslistGrid" runat="server" AutoGenerateColumns="false" EnableViewState="true"
                ForeColor="Black" Width="100%" PageSize="20" AllowPaging="true" AllowSorting="true"
                class="table" name="PromotionslistGrid" OnPageIndexChanging="PromotionslistGrid_PageIndexChanging" OnSorting="PromotionslistGrid_Sorting" >
                <PagerStyle HorizontalAlign="Right" CssClass="dtPage"></PagerStyle>
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />
                <Columns>
                   <asp:TemplateField HeaderText="#SL">
                        <HeaderStyle Width="6%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <span><%# GetRecordCountForPage()%></span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Promo Code" SortExpression="promo_code">
                        <HeaderStyle Width="12%" ForeColor="White" />
                        <ItemTemplate>
                            <a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("PromoDetail", "view")%>','PromoDetails.aspx?promoKey=<%# DataBinder.Eval(Container,"DataItem.pkey_promotion") %>')" title="Edit"><%# DataBinder.Eval(Container,"DataItem.promo_code") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description" SortExpression="description">
                        <HeaderStyle Width="19%" ForeColor="White" />
                        <ItemTemplate>
                            <a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("PromoDetail", "view")%>','PromoDetails.aspx?promoKey=<%# DataBinder.Eval(Container,"DataItem.pkey_promotion") %>')" data-gravity="s" title="Edit"><%# DataBinder.Eval(Container,"DataItem.description") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Start Date">
                        <HeaderStyle Width="10%"/>
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container,"DataItem.start_date","{0:MM/dd/yyyy}") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="End Date">
                        <HeaderStyle Width="10%" />
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container,"DataItem.end_date","{0:MM/dd/yyyy}") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Rule">
                        <HeaderStyle Width="5%" />
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container,"DataItem.rule_type") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Discount">
                        <HeaderStyle Width="10%" />
                        <ItemTemplate>
                            <div style="width: 30%; float:left; border:1px solid #e4f3f9;"><%#DataBinder.Eval(Container,"DataItem.ship_discount").ToString().Replace(".00","%") %></div>
                            <span class="sep">|</span>
                            <div style="width: 55%; float:right; border:1px solid #e4f3f9;"><%#DataBinder.Eval(Container,"DataItem.discount").ToString().Replace(".00","%") %></div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Ship">
                        <HeaderStyle Width="10%" />
                        <ItemTemplate>
                            <%# GetShipment(DataBinder.Eval(Container,"DataItem.pkey_promotion")).ToString() == "" ? "NA" : GetShipment(DataBinder.Eval(Container,"DataItem.pkey_promotion")).ToString()%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Applicability">
                        <HeaderStyle Width="10%" />
                        <ItemTemplate>
                            <%# GetApplicability(DataBinder.Eval(Container,"DataItem.pkey_promotion")).ToString() == "" ? "NA" : GetApplicability(DataBinder.Eval(Container,"DataItem.pkey_promotion")).ToString()%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Active">
                        <HeaderStyle Width="8%" />
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container,"DataItem.is_active").ToString() == "True" ? "Active" : "Inactive" %>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <div class="ttl"><b>Total Number of Promotions: </b><%=recordCount%></div>
            <!-- table Ends -->
            <div class="btn_lst">
                <div class="lft">
                    <asp:Button runat="server" ID="btnAddPromotion" CssClass="button rounded" Text="Add a Promotion" OnClientClick="return addPromotion();" OnClick="btnAddPromotion_Click"></asp:Button>
                </div>
                <div>
                    <asp:Button ID="btnClose" Text="Close" CssClass="button rounded" runat="server" OnClick="btnClose_Click" UseSubmitBehavior="false"></asp:Button>
                </div>
            </div>
        </form>
    </div>
</asp:Content>