﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Configuration;

namespace Bestprintbuy.Admin.Web.Customers
{
    public partial class AdminCustLogin : PageBase
    {
        protected int variantKey = Convert.ToInt32(WebConfiguration.ProductVariants["ExclusiveCustomer"]);
        UserIdentity identity = null;
        protected string superUserPwd = "";
        protected string referredURL = "";
        protected string strExclusiveCustomer = "";
        protected string AuthUser = "";
        protected string txtEmail = "";
        protected int loginOption = 1;
        protected string m_strExclusiveCustomer = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!Page.IsPostBack)
            //{
            AuthUser = "&AuthUser=Admin";
            txtEmail = txtEmailorPO.Text;
            loginOption = Convert.ToInt32(ddlLoginID.SelectedValue);
            if (chksuperuser.Checked)
                superUserPwd = txtPassword.Text;
            if (txtEmail != "")
                AuthnticateCustomer();
            txtEmailorPO.Text = "";
            GetKioskList(variantKey, ConnString);
            //}
        }

        private void AuthnticateCustomer()
        {
            try
            {
                if (ddlKiosk.SelectedValue != "bpb" && ddlKiosk.SelectedValue != "smb")
                    strExclusiveCustomer = "";
                else
                    strExclusiveCustomer = ddlKiosk.SelectedValue;
                identity = ValidateCustomerEMail();
                //Dim principal As UserPrincipal = New UserPrincipal(identity, "roles")
                string strAUIUrl = GetCustomerAccessAUIUrl();
                if (strAUIUrl == string.Empty)
                {
                    //strAUIUrl = WebConfiguration.AdminUserInterfaceUrl;
                    strAUIUrl = ConfigurationManager.AppSettings["CustomerPortalAUIURL"];
                }

                if (identity != null)
                {
                    if (Identity.UserPK > 0)
                    {
                        if (superUserPwd == string.Empty)
                        {
                            if (strAUIUrl == "")
                            {
                                //if (strExclusiveCustomer == "")
                                //{
                                //    referredURL = "../web/MyAccount.aspx?TimeStamp1=" + System.DateTime.Now.Ticks.ToString() + AuthUser;
                                //}
                                //else if (strExclusiveCustomer == "KW")
                                //{
                                //    // Session("ExcluciveCustomer") = "kw"
                                //    referredURL = "../kw/kwMyAccount.aspx?TimeStamp1=" + System.DateTime.Now.Ticks.ToString() + AuthUser;
                                //}
                                //else
                                //{
                                //    //Session("ExcluciveCustomer") = strExclusiveCustomer
                                //    //Session("Iskiosk") = "true"
                                //    referredURL = "../ec/MyAccount.aspx?TimeStamp1=" + System.DateTime.Now.Ticks.ToString() + AuthUser;
                                //}
                                //Response.Redirect(referredURL, true);

                                AlertScript("Url is Empty");
                            }
                            else
                            {
                                pnlcustaccessinfo.Visible = true;
                                txtCustUrl.Text = String.Format("{0}?uid={1}&fkey_admin={2}&ec={3}&email={4}", strAUIUrl, adminCustAccessGUID, Identity.UserPK, strExclusiveCustomer, txtEmailorPO.Text);
                                // txtCustUrl.ReadOnly = True
                                // Dim sb As New System.Text.StringBuilder
                                string sb = "";
                                sb = sb + "<SCRIPT type=\"text/javascript\">";
                                sb = sb + "OpenCustomerAccount();</SCRIPT>";
                                ltrlCustomerUrlScript.Text = sb.ToString();
                            }
                        }
                        else
                        {
                            pnlcustaccessinfo.Visible = true;
                            if (strAUIUrl == "")
                                txtCustUrl.Text = String.Format("{0}/Customer/NavCustomer.aspx?uid={1}&fkey_admin={2}&ec={3}&email={4}", UrlBase, adminCustAccessGUID, Identity.UserPK, strExclusiveCustomer, txtEmailorPO.Text);
                            else
                                txtCustUrl.Text = String.Format("{0}?uid={1}&fkey_admin={2}&ec={3}&email={4}&su=true", strAUIUrl, adminCustAccessGUID, Identity.UserPK, strExclusiveCustomer, txtEmailorPO.Text);

                            //txtCustUrl.ReadOnly = true;
                        }
                    }
                    else
                    {
                        ltrlCustomerUrlScript.Text = "";
                        AlertScript("Entered Details are invalid.");
                    }
                }
                else
                {
                    ltrlCustomerUrlScript.Text = "";
                    AlertScript("Entered Details are invalid.");
                }
                txtEmailorPO.Text = "";
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        private UserIdentity ValidateCustomerEMail()
        {
            UserIdentity uidentity = null;
            try
            {
                string emailid = txtEmailorPO.Text;
                int loginOption = 1;
                loginOption = Convert.ToInt32(ddlLoginID.SelectedValue);
                int fkey_site = 1;
                if (ddlKiosk.SelectedItem.Text == "smb")
                {
                    fkey_site = 2;
                }
                string superUserPwd = "";
                string accessGuid = "";
                int fkey_admin_user = -1;
                if (Identity != null)
                {
                    fkey_admin_user = Identity.UserPK;
                }
                if (chksuperuser.Checked)
                {
                    if (txtPassword.Text != "")
                    {
                        superUserPwd = MD5Encryption.Encrypt(txtPassword.Text);
                    }
                }
                accessGuid = string.Format("{0}{1}{2}", fkey_admin_user, emailid,
                        DateTime.Now.ToString("MM/dd/yyyy hh:mi:ss"));
                accessGuid = MD5Encryption.Encrypt(accessGuid);
                accessGuid = accessGuid.Substring(0, Math.Min(244, accessGuid.Length));

                int intfkey_ec_attribute = -1;
                if (m_strExclusiveCustomer != string.Empty)
                {
                    intfkey_ec_attribute = Convert.ToInt32(
                        WebConfiguration.ProductColorKeys["ExclusiveCustomer " + m_strExclusiveCustomer.ToUpper()]);
                }
                CustomerModel l_customerModel = CustomerService.AdminValidateCustomerModel(emailid, intfkey_ec_attribute,
                    loginOption, superUserPwd, fkey_admin_user, accessGuid, fkey_site, ConnString);


                int cuspkey = -1;
                bool login = false;
                string email;
                string name;
                bool isAdmin = false;
                string role = "USER";
                int fkeyregister = -1;

                if (l_customerModel.Customers.Rows.Count == 1)
                {
                    CustomerModel.CustomersRow cusRow = (CustomerModel.CustomersRow)l_customerModel.Customers.Rows[0];
                    emailid = cusRow.customer_id;
                    cuspkey = cusRow.pkey_customer;
                    login = true;
                    name = cusRow.cus_firstname;
                    email = cusRow.email;

                    adminCustAccessGUID = cusRow.superuserguid;

                    customerAccessAUIUrl = cusRow.CustPortalAUIUrl;

                    uidentity = new UserIdentity(emailid, cuspkey, login, isAdmin, name, email, role, fkeyregister);
                    if (loginOption == 2)
                    {
                        Identity.ExclusiveCustomerOf = cusRow.Isattribute_codeNull() ? "" : cusRow.attribute_code;
                        Identity.ExclusiveCustomerKey = cusRow.Isfkey_ec_attributeNull() ? -1 : Convert.ToInt32(cusRow.fkey_ec_attribute);
                    }
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
            return uidentity;
        }

        private string adminCustAccessGUID;
        public string GetAdminCustAccessGUID()
        {
            return adminCustAccessGUID;
        }

        private string customerAccessAUIUrl = "";
        public string GetCustomerAccessAUIUrl()
        {
            return customerAccessAUIUrl;
        }

        private void GetKioskList(int variantKey, string ConnString)
        {
            try
            {
                ProductTemplateModel.KiosksDataTable kioskList = ProductService.getKioskList(variantKey, ConnString);
                ddlKiosk.DataSource = kioskList;
                ddlKiosk.DataTextField = "attribute_value";
                ddlKiosk.DataValueField = "attribute_code";
                ddlKiosk.DataBind();
                ddlKiosk.Items.Insert(0, new ListItem("BestPrintBuy", "bpb"));
                ddlKiosk.Items.Insert(1, new ListItem("BestPrintBuy Small Business", "smb"));
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/DashBoard.aspx");
        }
    }
}