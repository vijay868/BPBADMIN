﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.Products
{
    public partial class HomeScrollMessages : PageBase
    {
        protected int status = -2;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadData();
            }
        }

        private void LoadData()
        {
            try
            {
                status = Convert.ToInt32(ddlFilterAct.SelectedValue);
                CustMessageModel model = ProductService.GetHomeScrollMsgList(status, ConnString);
                HomeScrollGrid.DataSource = model.HomeScrollMessages;
                HomeScrollGrid.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }            
        }

        protected void ddlFilterAct_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadData();
        }

        protected void HomeScrollGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            HomeScrollGrid.PageIndex = e.NewPageIndex;
            LoadData();
        }

        protected int GetRecordCountForPage()
        {
            return GetRecordCountForPageSize(HomeScrollGrid.PageSize, HomeScrollGrid.PageIndex + 1);
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("HomeScrollMessageDetail.aspx?Pkey=-2");
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/DashBoard.aspx");
        }
    }
}