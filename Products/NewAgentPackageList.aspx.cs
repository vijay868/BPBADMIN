﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.Products
{
    public partial class NewAgentPackageList : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadData();
            }
        }

        private void LoadData()
        {
            try
            {
                AgentPackageModel agentPackageList = ProductService.GetAgentPackageList(ConnString);
                NewAgentPackageGrid.DataSource = agentPackageList.NewAgentPackage;
                NewAgentPackageGrid.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void NewAgentPackageGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            NewAgentPackageGrid.PageIndex = e.NewPageIndex;
            LoadData();
        }

        protected int GetRecordCountForPage()
        {
            return GetRecordCountForPageSize(NewAgentPackageGrid.PageSize, NewAgentPackageGrid.PageIndex + 1);
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/DashBoard.aspx");
        }

        protected void btnnapFC_Click(object sender, EventArgs e)
        {
            Response.Redirect("NewAgentPackageFC.aspx");
        }

        protected void btnnapInsideFC_Click(object sender, EventArgs e)
        {
            Response.Redirect("NewAgentInsideFC.aspx");
        }
    }
}