﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="XSellProducts.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Products.XSellProducts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">

        function moveMatchProductOptions(option, moveAll) {
            fromObject = document.getElementById("ContentPlaceHolder1_lbAvailableMatchProducts");
            toObject = document.getElementById("ContentPlaceHolder1_lbAssignedMatchProducts");

            if (option == "right") {
                from = eval(fromObject);
                to = eval(toObject);
            }
            else {
                from = eval(toObject);
                to = eval(fromObject);
            }

            if (from && to) {
                var fromSize = from.options.length;
                var toSize = to.options.length;
                var fromArray = new Array();
                var fromSelectedArray = new Array();
                var fromSelectedIndex = 0;
                var fromIndex = 0;
                for (i = fromSize - 1; i > -1; i--) {
                    var isSelected = false
                    if (from.options[i].selected)
                        isSelected = from.options[i].selected;
                    if ((isSelected && isSelected == true) || moveAll == true) {
                        fromSelectedArray[fromSelectedIndex] = new Array(from.options[i].value, from.options[i].text);
                        from.options[i] = null;
                        fromSelectedIndex++;
                    }
                    else {
                        fromArray[fromIndex] = new Array(from.options[i].value, from.options[i].text);
                        from.options[i] = null;
                        fromIndex++;
                    }
                }
                fromSize = fromArray.length;
                for (i = 0; i < fromSize; i++) {
                    from.options[i] = new Option(fromArray[fromSize - 1 - i][1], fromArray[fromSize - 1 - i][0]);
                }
                var newToSize = fromSelectedArray.length;
                var strData = "";
                for (i = 0; i < newToSize; i++) {
                    toSize = to.options.length;
                    to.options[toSize] = new Option(fromSelectedArray[i][1], fromSelectedArray[i][0]);
                    strData = strData + to.options[toSize].value;
                    if (i != newToSize - 1) strData = strData + ",";
                }
            }
        }

        function moveNonMatchProductOptions(option, moveAll) {
            fromObject = document.getElementById("ContentPlaceHolder1_lbAvailableNonMatchProducts");
            toObject = document.getElementById("ContentPlaceHolder1_lbAssignedNonMatchProducts");

            if (option == "right") {
                from = eval(fromObject);
                to = eval(toObject);
            }
            else {
                from = eval(toObject);
                to = eval(fromObject);
            }

            if (from && to) {
                var fromSize = from.options.length;
                var toSize = to.options.length;
                var fromArray = new Array();
                var fromSelectedArray = new Array();
                var fromSelectedIndex = 0;
                var fromIndex = 0;
                for (i = fromSize - 1; i > -1; i--) {
                    var isSelected = false
                    if (from.options[i].selected)
                        isSelected = from.options[i].selected;
                    if ((isSelected && isSelected == true) || moveAll == true) {
                        fromSelectedArray[fromSelectedIndex] = new Array(from.options[i].value, from.options[i].text);
                        from.options[i] = null;
                        fromSelectedIndex++;
                    }
                    else {
                        fromArray[fromIndex] = new Array(from.options[i].value, from.options[i].text);
                        from.options[i] = null;
                        fromIndex++;
                    }
                }
                fromSize = fromArray.length;
                for (i = 0; i < fromSize; i++) {
                    from.options[i] = new Option(fromArray[fromSize - 1 - i][1], fromArray[fromSize - 1 - i][0]);
                }
                var newToSize = fromSelectedArray.length;
                var strData = "";
                for (i = 0; i < newToSize; i++) {
                    toSize = to.options.length;
                    to.options[toSize] = new Option(fromSelectedArray[i][1], fromSelectedArray[i][0]);
                    strData = strData + to.options[toSize].value;
                    if (i != newToSize - 1) strData = strData + ",";
                }
            }
        }

        function selectMemberRoles() {
            if (document.getElementById('ContentPlaceHolder1_lbAssignedMatchProducts')) {
                var leng = document.getElementById('ContentPlaceHolder1_lbAssignedMatchProducts').options.length;
                for (i = leng - 1; i > -1; i--) {
                    document.getElementById('ContentPlaceHolder1_lbAssignedMatchProducts').options[i].selected = true;
                }
            }

            if (document.getElementById('ContentPlaceHolder1_lbAssignedNonMatchProducts')) {
                var leng = document.getElementById('ContentPlaceHolder1_lbAssignedNonMatchProducts').options.length;
                for (j = leng - 1; j > -1; j--) {
                    document.getElementById('ContentPlaceHolder1_lbAssignedNonMatchProducts').options[j].selected = true;
                }
            }
            return true;
        }
    </script>
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Product Setup & Configuration</li>
            <li>Cross Sell Product Setup</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Cross Sell Product Setup</h2>
        <form id="frmXSellProducts" name="frmXSellProducts" runat="server">
            <table width="100%" class="accounts-table">
                <thead>
                    <tr>
                        <th colspan="4" width="100%">Define Matching Products for Cross Selling</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="15%">
                            <label></label>
                        </td>
                        <td width="50%">
                            <div style="width: 40%; float: left">
                                <label style="padding-left:0px;"><b>Available Products</b></label>
                                <asp:ListBox ID="lbAvailableMatchProducts" CssClass="list-box" runat="server" SelectionMode="Multiple"></asp:ListBox>
                            </div>
                            <div style="width: 10%; float: left; padding: 40px 10px 0 10px;">
                                <button id="to2" type="button" class="button rounded width53" onclick="javascript:moveMatchProductOptions('right',false)">
                                    &nbsp;&gt;&nbsp;</button><br />
                                <button id="allTo2" type="button" class="button rounded width53" onclick="javascript:moveMatchProductOptions('right',true)">
                                    &nbsp;&gt;&gt;&nbsp;</button><br />
                                <button id="to1" type="button" class="button rounded width53" onclick="javascript:moveMatchProductOptions('left',false)">
                                    &nbsp;&lt;&nbsp;</button><br />
                                <button id="allTo1" type="button" class="button rounded width53" onclick="javascript:moveMatchProductOptions('left',true)">
                                    &nbsp;&lt;&lt;&nbsp;</button>
                            </div>
                            <div style="width: 35%; float: left; padding-right: 10px;">
                                <label style="padding-left:0px;"><b>Matching Products</b></label>
                                <asp:ListBox ID="lbAssignedMatchProducts" runat="server" CssClass="list-box" SelectionMode="Multiple"></asp:ListBox>
                            </div>
                        </td>
                        <td width="35%"></td>
                    </tr>
                </tbody>
            </table>

            <table width="100%" class="accounts-table">
                <thead>
                    <tr>
                        <th colspan="4" width="100%">Define Non Matching Products for Cross Selling</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="15%">
                            <label></label>
                        </td>
                        <td width="50%">
                            <div style="width: 40%; float: left">
                                <label style="padding-left:0px;"><b>Available Products</b></label>
                                <asp:ListBox ID="lbAvailableNonMatchProducts" runat="server" CssClass="list-box" SelectionMode="Multiple"></asp:ListBox>
                            </div>
                            <div style="width: 10%; float: left; padding: 40px 10px 0 10px;">
                                <button id="Button1" type="button" class="button rounded width53" onclick="javascript:moveNonMatchProductOptions('right',false)">
                                    &nbsp;&gt;&nbsp;</button><br />
                                <button id="Button2" type="button" class="button rounded width53" onclick="javascript:moveNonMatchProductOptions('right',true)">
                                    &nbsp;&gt;&gt;&nbsp;</button><br />
                                <button id="Button3" type="button" class="button rounded width53" onclick="javascript:moveNonMatchProductOptions('left',false)">
                                    &nbsp;&lt;&nbsp;</button><br />
                                <button id="Button4" type="button" class="button rounded width53" onclick="javascript:moveNonMatchProductOptions('left',true)">
                                    &nbsp;&lt;&lt;&nbsp;</button>
                            </div>
                            <div style="width: 35%; float: left; padding-right: 10px;">
                                <label style="padding-left:0px;"><b>Non Matching Products</b></label>
                                <asp:ListBox ID="lbAssignedNonMatchProducts" runat="server" CssClass="list-box" SelectionMode="Multiple"></asp:ListBox>
                            </div>
                        </td>
                        <td width="35%"></td>
                    </tr>
                </tbody>
            </table>
            <table width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="15%">&nbsp;</td>
                        <td width="17%">&nbsp;</td>
                        <td width="15%">&nbsp;</td>
                        <td width="35%" style="float: right;">
                            <asp:Button runat="server" CssClass="button rounded" ID="btnSave" Text="Save" OnClientClick="return selectMemberRoles();" OnClick="btnSave_Click"></asp:Button>
                            <asp:Button runat="server" CssClass="button rounded" ID="btnClose" Text="Close" UseSubmitBehavior="false" OnClick="btnClose_Click"></asp:Button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
</asp:Content>
