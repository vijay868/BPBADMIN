﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace Bestprintbuy.Admin.Web.Products
{
    public partial class XSellDiscountDetails : PageBase
    {
        protected int PkeyXsellmaster = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            ViewState["PkeyXsellmaster"] = Convert.ToInt32(Request.QueryString["PkeyXsellmaster"]);
            if (!Page.IsPostBack)
            {
                GetData();
            }
        }

        private void GetData()
        {
            try
            {
                PkeyXsellmaster = Convert.ToInt32(ViewState["PkeyXsellmaster"]);
                XSellProductModel XSellDiscountDetails = PromoService.GetXSellDiscountDataByKey(PkeyXsellmaster, ConnString);

                lbAvailableShipments.DataSource = XSellDiscountDetails.AvailableShipments;
                lbAvailableShipments.DataTextField = "shipment_description";
                lbAvailableShipments.DataValueField = "shipment_on";
                lbAvailableShipments.SelectionMode = ListSelectionMode.Multiple;
                lbAvailableShipments.DataBind();

                lbAssignedShipments.DataSource = XSellDiscountDetails.AssignedShipments;
                lbAssignedShipments.DataTextField = "shipment_description";
                lbAssignedShipments.DataValueField = "shipment_on";
                lbAssignedShipments.SelectionMode = ListSelectionMode.Multiple;
                lbAssignedShipments.DataBind();

                ddlProductType.DataSource = XSellDiscountDetails.MatchingProductsAvailable;
                ddlProductType.DataTextField = "ct_name";
                ddlProductType.DataValueField = "pkey_category_type";
                ddlProductType.DataBind();
                ddlProductType.Items.Insert(0, new ListItem("Select Product", ""));

                if (XSellDiscountDetails.XSellDiscount.Rows.Count > 0)
                {
                    XSellProductModel.XSellDiscountRow XSellDiscountRow = (XSellProductModel.XSellDiscountRow)XSellDiscountDetails.XSellDiscount.Rows[0];
                    ddlOfferedOn.SelectedValue = XSellDiscountRow.Isrule_typeNull() ? "-1" : XSellDiscountRow.rule_type.ToString().Trim();
                    txtProductDiscount.Text = XSellDiscountRow.IsdiscountNull() ? "" : XSellDiscountRow.discount.ToString().Replace(".00", "");
                    txtShipmentDiscount.Text = XSellDiscountRow.Isship_discountNull() ? "" : XSellDiscountRow.ship_discount.ToString().Replace(".00", "");
                    txtFromDate.Text = Convert.ToDateTime(XSellDiscountRow.start_date).ToString("MM/dd/yyyy");
                    txtToDate.Text = Convert.ToDateTime(XSellDiscountRow.end_date).ToString("MM/dd/yyyy");
                    chkActive.Checked = XSellDiscountRow.is_active;
                    ddlProductType.SelectedValue = XSellDiscountRow.Isfkey_product_typeNull() ? "" : XSellDiscountRow.fkey_product_type.ToString();

                    if (XSellDiscountRow.ship_discount_type)
                    {
                        rbtnShipPercentage.Checked = false;
                        rbtnShipFixed.Checked = true;
                    }
                    else
                    {
                        rbtnShipPercentage.Checked = true;
                        rbtnShipFixed.Checked = false;
                    }

                    if (XSellDiscountRow.discount_type)
                    {
                        rbtnProdPercentage.Checked = false;
                        rbtnProdFixed.Checked = true;
                    }
                    else
                    {
                        rbtnProdPercentage.Checked = true;
                        rbtnProdFixed.Checked = false;
                    }
                }
                else
                {
                    chkActive.Checked = true;
                    rbtnShipPercentage.Checked = true;
                    rbtnProdPercentage.Checked = true;
                    ddlOfferedOn.SelectedValue = "D";
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                PkeyXsellmaster = Convert.ToInt32(ViewState["PkeyXsellmaster"]);
                XSellProductModel XSellDiscountDetails = new XSellProductModel();
                XSellProductModel.XSellDiscountRow XSellDiscountRow = XSellDiscountDetails.XSellDiscount.NewXSellDiscountRow();
                XSellDiscountRow.pkey_xsell_discount_master = PkeyXsellmaster;
                XSellDiscountRow.discount = Convert.ToDecimal(txtProductDiscount.Text == "" ? "0" : txtProductDiscount.Text);
                XSellDiscountRow.ship_discount = Convert.ToDecimal(txtShipmentDiscount.Text == "" ? "0" : txtShipmentDiscount.Text);
                XSellDiscountRow.start_date = Convert.ToDateTime(txtFromDate.Text);
                XSellDiscountRow.end_date = Convert.ToDateTime(txtToDate.Text);
                XSellDiscountRow.is_active = chkActive.Checked;
                XSellDiscountRow.rule_type = ddlOfferedOn.SelectedValue;
                XSellDiscountRow.fkey_product_type = Convert.ToInt32(ddlProductType.SelectedValue);
                XSellDiscountRow.fkey_user = Identity.UserPK;
                XSellDiscountRow.is_ecard_enabled = false;
                XSellDiscountRow.ecard_price = 0;

                if (rbtnShipFixed.Checked)
                    XSellDiscountRow.ship_discount_type = true;
                else
                    XSellDiscountRow.ship_discount_type = false;

                if (rbtnProdFixed.Checked)
                    XSellDiscountRow.discount_type = true;
                else
                    XSellDiscountRow.discount_type = false;
                XSellDiscountDetails.XSellDiscount.AddXSellDiscountRow(XSellDiscountRow);

                string AssignedShipments = "";
                if (Request["ctl00$ContentPlaceHolder1$lbAssignedShipments"] != null)
                    AssignedShipments = Request["ctl00$ContentPlaceHolder1$lbAssignedShipments"].ToString();

                if (AssignedShipments != "")
                {
                    foreach (string item in AssignedShipments.Split(','))
                    {
                        XSellProductModel.AssignedShipmentsRow assignedShipmentRow = XSellDiscountDetails.AssignedShipments.NewAssignedShipmentsRow();
                        assignedShipmentRow.shipment_on = item;
                        XSellDiscountDetails.AssignedShipments.AddAssignedShipmentsRow(assignedShipmentRow);
                    }
                }

                PkeyXsellmaster = PromoService.UpdateXSellDiscountData(XSellDiscountDetails, ConnString);
                Response.Redirect("XSellDiscountList.aspx");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("XSellDiscountList.aspx");
        }
    }
}