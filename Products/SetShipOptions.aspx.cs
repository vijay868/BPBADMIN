﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.Products
{
    public partial class SetShipOptions : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadData();
            }
        }

        private void LoadData()
        {
            try
            {
                ProductModel ShipAndTAData = ProductService.GetShipAndTAOptions(ConnString);
                rptrTurnAroundOptions.DataSource = ShipAndTAData.TurnaroundTimes;
                rptrTurnAroundOptions.DataBind();

                if (ShipAndTAData.ShipOptions.Rows.Count > 0)
                {
                    ProductModel.ShipOptionsRow shipoptionsRow = (ProductModel.ShipOptionsRow)ShipAndTAData.ShipOptions.Rows[0];
                    chkExpressShip.Checked = shipoptionsRow.ShowOneDayOption;
                    chkSpeed.Checked = shipoptionsRow.ShowTwoDayOption;
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/DashBoard.aspx");
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            ProductModel ShipAndTADetails = new ProductModel();

            try
            {
                int oneDay = 1;
                int twoDay = 1;
                oneDay = chkExpressShip.Checked ? 1 : 0;
                twoDay = chkSpeed.Checked ? 1 : 0;

                if (ShipAndTADetails.TurnaroundTimes != null)
                {
                    foreach (RepeaterItem item in rptrTurnAroundOptions.Items)
                    {
                        ProductModel.TurnaroundTimesRow TARow = ShipAndTADetails.TurnaroundTimes.NewTurnaroundTimesRow();
                        HiddenField hdnpkeyTAtime = (HiddenField)item.FindControl("hdnpkeyTAtime");
                        TextBox txtTATime = (TextBox)item.FindControl("txtTATime");
                        TextBox txtCost = (TextBox)item.FindControl("txtCost");
                        TextBox txtBasePrice = (TextBox)item.FindControl("txtBasePrice");
                        TextBox txtOffsetPrecent = (TextBox)item.FindControl("txtOffsetPrecent");
                        CheckBox chkActive = (CheckBox)item.FindControl("chkActive");

                        TARow.pkey_turnaround_time = Convert.ToInt32(hdnpkeyTAtime.Value);
                        TARow.turnaround_time = txtTATime.Text;
                        TARow.turnaround_cost = Convert.ToDecimal(txtCost.Text.Replace("$", ""));
                        TARow.baseprice = Convert.ToDecimal(txtBasePrice.Text.Replace("$", ""));
                        TARow.offsetpercent = Convert.ToDecimal(txtOffsetPrecent.Text.Replace("$", ""));
                        TARow.isactive = chkActive.Checked;

                        ShipAndTADetails.TurnaroundTimes.AddTurnaroundTimesRow(TARow);
                    }
                }
                ShipAndTADetails = ProductService.UpdateShipAndTAOptions(oneDay, twoDay, ShipAndTADetails, ConnString);

                LoadData();
                AlertScript("Save/Update Successfully completed.");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }
    }
}