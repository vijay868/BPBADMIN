﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.Products
{
    public partial class NewAgentInsideFC : PageBase
    {
        protected string mCustomerPortalURL = "";
        protected int variantKey = Convert.ToInt32(WebConfiguration.ProductVariants["ExclusiveCustomer"]);
        protected int SiteKey = 170;
        protected void Page_Load(object sender, EventArgs e)
        {
            mCustomerPortalURL = customerPotralUrl + "/DesignCenter/";
            if (!Page.IsPostBack)
            {
                try
                {
                    GetKioskList(variantKey, ConnString);
                    GetDataNewAgentInside(SiteKey, ConnString);
                }
                catch (Exception ex)
                {
                    FileLogger.WriteException(ex);
                    AlertScript(ex.Message);
                }
            }
        }

        private void GetDataNewAgentInside(int SiteKey, string ConnString)
        {
            ReverseTemplateModel NewAgentFCData = ProductService.GetDataNewAgentInside(SiteKey, ConnString);
            var masterdrs = from collect in NewAgentFCData.ReverseDetails
                            select collect;

            var templateList = from collect in NewAgentFCData.ReverseTemplate
                               select collect;

            var list = from c in masterdrs
                       join t in templateList on c.fkey_rev_template equals t.pkey_reverse_template
                       select t;

            dtlAgentPackageFC.DataSource = templateList.ToList();
            dtlAgentPackageFC.DataBind();

            foreach (DataListItem item in dtlAgentPackageFC.Items)
            {
                CheckBox chk = (CheckBox)item.FindControl("chkTemplate");
                Label TemplateName = (Label)item.FindControl("lblTemplateName");
                foreach (var value in list)
                {
                    if (TemplateName.Text.Trim() == value.template_id.Trim())
                    {
                        chk.Checked = true;
                    }
                }
            }
        }

        private void GetKioskList(int variantKey, string ConnString)
        {
            ProductTemplateModel.KiosksDataTable kioskList = ProductService.getKioskList(variantKey, ConnString);
            ddlKiosk.DataSource = kioskList;
            ddlKiosk.DataTextField = "attribute_value";
            ddlKiosk.DataValueField = "pkey_attribute";
            ddlKiosk.DataBind();
            ddlKiosk.SelectedValue = SiteKey.ToString(); ;
        }

        protected void ddlKiosk_SelectedIndexChanged(object sender, EventArgs e)
        {
            SiteKey = Convert.ToInt32(ddlKiosk.SelectedValue);
            GetDataNewAgentInside(SiteKey, ConnString);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string alCustKeys = "";
                foreach (DataListItem item in dtlAgentPackageFC.Items)
                {
                    CheckBox chk = (CheckBox)item.FindControl("chkTemplate");
                    HiddenField hdnPkeyTemplateMaster = (HiddenField)item.FindControl("hdnPkeyTemplateMaster");
                    if (chk.Checked)
                    {
                        alCustKeys += hdnPkeyTemplateMaster.Value + ",";
                    }
                }
                alCustKeys = alCustKeys.TrimEnd(',');
                string[] productIds = alCustKeys.Split(",".ToCharArray());
                SiteKey = Convert.ToInt32(ddlKiosk.SelectedValue);
                ProductService.UpdateInsideTemplateInfo(productIds, SiteKey, ConnString);
                AlertScript("Successfully Saved.");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript("Save failed. try again.");
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/DashBoard.aspx");
        }
    }
}