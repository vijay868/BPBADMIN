﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="StateList.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Products.StateList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="contant_hldr">
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li><a href="#">Products</a></li>
            <li>States</li>
        </ul>
        <h2>States</h2>
        <form class="" runat="server" id="frmOrderFulfillmentList">
            <table class="accounts-table" width="70%">
                <tbody>
                    <tr>
                        <td width="6%">View States for </td>
                        <td width="10%">
                            <asp:DropDownList name="ddlCountry" ID="ddlCountry" CssClass="search" data-placeholder="Choose a Name" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                                <asp:ListItem Value=""></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                </tbody>
            </table>


            <asp:GridView ID="StatesGrid" runat="server" AutoGenerateColumns="false" EnableViewState="true"
                ForeColor="Black" Width="100%" PageSize="10" AllowPaging="true" OnPageIndexChanging="StatesGrid_PageIndexChanging"
                class="table" name="StatesGrid">
                <PagerStyle HorizontalAlign="Right" CssClass="dtPage"></PagerStyle>
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />
                <Columns>
                    <asp:TemplateField HeaderText="#SL" ItemStyle-Width="5%">
                        <HeaderStyle Width="5%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <span><%# GetRecordCountForPage()%></span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="State Abbrevation" ItemStyle-Width="9%">
                        <HeaderStyle Width="9%" ForeColor="White" />
                        <ItemTemplate>
                           <asp:LinkButton ID="btnCtAbbr" runat="server" CommandName="Delete" CausesValidation="true" OnClientClick="return viewState();"
                                                    ToolTip="Delete" CommandArgument='<%#Bind("pkey_state")%>' OnClick="btnStateAbbr_Click"><%#Eval("state_abbr") %></asp:LinkButton>                                            
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="State Name" ItemStyle-Width="10%">
                        <HeaderStyle Width="10%" ForeColor="White" />
                        <ItemTemplate>
                         <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Delete" CausesValidation="true" OnClientClick="return viewState();"
                                                    ToolTip="Delete" CommandArgument='<%#Bind("pkey_state")%>' OnClick="btnStateName_Click"><%#Eval("state_name") %></asp:LinkButton>  
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Delete" ItemStyle-Width="12%">
                        <HeaderStyle Width="12%" ForeColor="White" />
                        <ItemTemplate>
                            <asp:LinkButton ID="btnDelete" runat="server" CommandName="Delete" CausesValidation="true" 
                                                    ToolTip="Delete" CommandArgument='<%#Bind("pkey_state")%>' OnClientClick="return deleteState();"
                                                    CssClass="button small grey tooltip center" OnClick="btnDelete_Click"><i class="icon-remove"></i></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>

                    
                </Columns>
            </asp:GridView>
            <asp:Label ID="lblTotalRecords" runat="server" Font-Bold="true"></asp:Label>
            <div class="btn_lst">
                <div class="rgt">

                    <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClick="btnClose_Click"></asp:Button>
                    <asp:Button ID="btnExportTocsv1" CssClass="button rounded" runat="server" Text="Export To CSV" Visible="false" OnClick="btnExportTocsv_Click" />

                </div>
            </div>
        </form>
    </div>
</asp:Content>
