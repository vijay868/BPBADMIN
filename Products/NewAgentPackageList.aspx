﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="NewAgentPackageList.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Products.NewAgentPackageList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <div class="clr"></div>
    <!-- Container Starts -->
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Product Setup & Configuration</li>
            <li>New Agent Package</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>New Agent Package List</h2>
        <form id="fmNewAgentPackageList" name="fmNewAgentPackageList" runat="server">           
            <div class="clr"></div>
            <asp:GridView ID="NewAgentPackageGrid" runat="server" AutoGenerateColumns="false" EnableViewState="true"
                ForeColor="Black" Width="100%" PageSize="20" AllowPaging="true" AllowSorting="true"
                class="table" name="NewAgentPackageGrid" OnPageIndexChanging="NewAgentPackageGrid_PageIndexChanging">
                <PagerStyle HorizontalAlign="Right" CssClass="dtPage"></PagerStyle>
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />
                <Columns>
                     <asp:TemplateField HeaderText="#SL">
                        <HeaderStyle Width="5%"></HeaderStyle>
                        <ItemTemplate>
                            <span><%# GetRecordCountForPage()%></span>
                              </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Package">
                        <HeaderStyle Width="10%"/>
                        <ItemTemplate>
                            <a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("ProductDetail", "view")%>','NewAgentPackageDetail.aspx?agentPKey=<%#DataBinder.Eval(Container,"DataItem.pkey_new_agent_package") %>')" title="Edit"> <%#DataBinder.Eval(Container,"DataItem.package") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Package Discription">
                        <HeaderStyle Width="30%"/>
                        <ItemTemplate>
                             <a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("ProductDetail", "view")%>','NewAgentPackageDetail.aspx?agentPKey=<%#DataBinder.Eval(Container,"DataItem.pkey_new_agent_package") %>')" title="Edit"> <%#DataBinder.Eval(Container,"DataItem.package_desc") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Business Cards">
                        <HeaderStyle Width="9%" />
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container,"DataItem.bc_qty") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Letterheads">
                        <HeaderStyle Width="9%" />
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container,"DataItem.lh_qty") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="#10 Envelopes">
                        <HeaderStyle Width="9%" />
                        <ItemTemplate>
                             <%# DataBinder.Eval(Container,"DataItem.env_qty") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Folded Note Cards">
                        <HeaderStyle Width="10%" />
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container,"DataItem.fc_qty") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Package Cost">
                        <HeaderStyle Width="10%" />
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container,"DataItem.package_cost") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="IsActive">
                        <HeaderStyle Width="6%" />
                        <ItemTemplate>
                             <%# DataBinder.Eval(Container,"DataItem.is_active").ToString() == "True" ? "Active" : "Inactive" %>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <!-- table Ends -->
            <div class="btn_lst">
                <asp:Button ID="btnnapFC" Text="New Agent Package FC" CssClass="button rounded" runat="server" OnClick="btnnapFC_Click"></asp:Button>
                <asp:Button ID="btnnapInsideFC" Text="New Agent Package Inside FC" CssClass="button rounded" runat="server" OnClick="btnnapInsideFC_Click"></asp:Button>
                <div class="rgt">
                    <asp:Button ID="btnClose" Text="Close" CssClass="button rounded" runat="server" OnClick="btnClose_Click"></asp:Button>
                </div>
            </div>
        </form>
    </div>
</asp:Content>
