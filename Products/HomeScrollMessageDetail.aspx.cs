﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.Products
{
    public partial class HomeScrollMessageDetail : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ViewState["Pkey"] = Convert.ToInt32(Request.QueryString["Pkey"]);
                GetData();
            }
        }

        private void GetData()
        {
            try
            {
                CustMessageModel model = ProductService.GetHomeScrollMsgDataByKey(Convert.ToInt32(ViewState["Pkey"]), ConnString);

                if (model.HomeScrollMessages.Rows.Count > 0)
                {
                    CustMessageModel.HomeScrollMessagesRow row = (CustMessageModel.HomeScrollMessagesRow)model.HomeScrollMessages.Rows[0];
                    txtMessage.Text = row.description;
                    chkIsActive.Checked = row.isactive;
                    txtFromDate.Text = Convert.ToDateTime(row.fromdate).ToString("MM/dd/yyyy");
                    txtToDate.Text = Convert.ToDateTime(row.todate).ToString("MM/dd/yyyy");
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            CustMessageModel model = new CustMessageModel();
            CustMessageModel.HomeScrollMessagesRow row = model.HomeScrollMessages.NewHomeScrollMessagesRow();
            row.peky_portal_message = Convert.ToInt32(ViewState["Pkey"]);
            row.description = txtMessage.Text;
            row.isactive = chkIsActive.Checked;
            row.fromdate = Convert.ToDateTime(txtFromDate.Text);
            row.todate = Convert.ToDateTime(txtToDate.Text);
            try
            {
                model.HomeScrollMessages.AddHomeScrollMessagesRow(row);
                int key = ProductService.UpdateHomeScrollMessage(model, ConnString);
                Response.Redirect("HomeScrollMessages.aspx");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript("There is some problem. Please try again");
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("HomeScrollMessages.aspx");
        }
    }
}