﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.Products
{
    public partial class StateList : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                GetStates();
                LoadData();
            }
        }

        private void GetStates()
        {
            int countrykey = -1;
            CountryModel country = CountryService.GetList(countrykey, ConnString);
            ddlCountry.DataSource = country.Countries;
            ddlCountry.DataTextField = "country_name";
            ddlCountry.DataValueField = "pkey_country";
            ddlCountry.DataBind();
        }

        private void LoadData()
        {
            int statekey = -1;
            int countrykey = Convert.ToInt32(ddlCountry.SelectedValue);
            CountryModel states = CountryService.GetData(statekey, countrykey, ConnString);

            StatesGrid.DataSource = states.States;
            StatesGrid.DataBind();
        }

        protected void btnAddState_Click(object sender, EventArgs e)
        {
            Response.Redirect("StateDetails.aspx?pkeyState=-2&countrykey=" + ddlCountry.SelectedValue);
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/DashBoard.aspx");
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            LinkButton lbtn = (LinkButton)sender;
            int pkeyState = Convert.ToInt32(lbtn.CommandArgument);
            CountryService.DeleteState(pkeyState, ConnString);
            Response.Redirect("StateList.aspx");
        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        protected int GetRecordCountForPage()
        {
            return GetRecordCountForPageSize(StatesGrid.PageSize, StatesGrid.PageIndex + 1);
        }

        protected void CustomersListGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            StatesGrid.PageIndex = e.NewPageIndex;
            GetStates();
        }
        protected void btnStateAbbr_Click(object sender, EventArgs e)
        {
            LinkButton lbtn = (LinkButton)sender;
            int pkeytype = Convert.ToInt32(lbtn.CommandArgument);
            Response.Redirect("StateDetails.aspx?pkeyState=" + pkeytype + "&countrykey=" + ddlCountry.SelectedValue);
        }

        protected void btnStateName_Click(object sender, EventArgs e)
        {
            LinkButton lbtn = (LinkButton)sender;
            int pkeytype = Convert.ToInt32(lbtn.CommandArgument);
            Response.Redirect("StateDetails.aspx?pkeyState=" + pkeytype + "&countrykey=" + ddlCountry.SelectedValue);
        }
    }
}