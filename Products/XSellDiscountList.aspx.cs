﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.Products
{
    public partial class XSellDiscountList : PageBase
    {
        protected int serialNo;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadData();
            }
        }

        private void LoadData()
        {
            try
            {
                XSellProductModel xsellDiscountList = PromoService.GetXSellDiscountList(ConnString);
                CrossSellDiscountListGrid.DataSource = xsellDiscountList.XSellDiscount;
                CrossSellDiscountListGrid.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void CrossSellDiscountListGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CrossSellDiscountListGrid.PageIndex = e.NewPageIndex;
            LoadData();
        }

        protected int GetRecordCountForPage()
        {
            return GetRecordCountForPageSize(CrossSellDiscountListGrid.PageSize, CrossSellDiscountListGrid.PageIndex + 1);
        }

        protected void btnAddXsellDiscount_Click(object sender, EventArgs e)
        {
            Response.Redirect("XSellDiscountDetails.aspx?PkeyXsellmaster=-2");
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/DashBoard.aspx");
        }
    }
}