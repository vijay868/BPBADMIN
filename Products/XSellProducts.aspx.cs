﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.Products
{
    public partial class XSellProducts : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                GetData();
            }
        }

        private void GetData()
        {
            try
            {
                XSellProductModel model = PromoService.GetXSellProductsData(ConnString);

                lbAvailableMatchProducts.DataSource = model.XSellMatchingProductsAvailable;
                lbAvailableMatchProducts.DataTextField = "ct_name";
                lbAvailableMatchProducts.DataValueField = "pkey_category_type";
                lbAvailableMatchProducts.SelectionMode = ListSelectionMode.Multiple;
                lbAvailableMatchProducts.DataBind();

                lbAvailableNonMatchProducts.DataSource = model.XSellNonMatchingProductsAvailable;
                lbAvailableNonMatchProducts.DataTextField = "ct_name";
                lbAvailableNonMatchProducts.DataValueField = "pkey_category_type";
                lbAvailableNonMatchProducts.SelectionMode = ListSelectionMode.Multiple;
                lbAvailableNonMatchProducts.DataBind();

                lbAssignedMatchProducts.DataSource = model.XSellMatchingProducts;
                lbAssignedMatchProducts.DataTextField = "ct_name";
                lbAssignedMatchProducts.DataValueField = "pkey_category_type";
                lbAssignedMatchProducts.SelectionMode = ListSelectionMode.Multiple;
                lbAssignedMatchProducts.DataBind();

                lbAssignedNonMatchProducts.DataSource = model.XSellNonMatchingProducts;
                lbAssignedNonMatchProducts.DataTextField = "ct_name";
                lbAssignedNonMatchProducts.DataValueField = "pkey_category_type";
                lbAssignedNonMatchProducts.SelectionMode = ListSelectionMode.Multiple;
                lbAssignedNonMatchProducts.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            XSellProductModel model = new XSellProductModel();
            try
            {
                string AssignedMatchingProducts = "";
                if (Request["ctl00$ContentPlaceHolder1$lbAssignedMatchProducts"] != null)
                    AssignedMatchingProducts = Request["ctl00$ContentPlaceHolder1$lbAssignedMatchProducts"].ToString();

                if (AssignedMatchingProducts != "")
                {
                    foreach (string item in AssignedMatchingProducts.Split(','))
                    {
                        XSellProductModel.XSellMatchingProductsRow row = model.XSellMatchingProducts.NewXSellMatchingProductsRow();
                        row.pkey_category_type = Convert.ToInt32(item);
                        model.XSellMatchingProducts.AddXSellMatchingProductsRow(row);
                    }
                }

                string AssignedNonMatchingProducts = "";
                if (Request["ctl00$ContentPlaceHolder1$lbAssignedNonMatchProducts"] != null)
                    AssignedNonMatchingProducts = Request["ctl00$ContentPlaceHolder1$lbAssignedNonMatchProducts"].ToString();

                if (AssignedNonMatchingProducts != "")
                {
                    foreach (string item in AssignedNonMatchingProducts.Split(','))
                    {
                        XSellProductModel.XSellNonMatchingProductsRow row = model.XSellNonMatchingProducts.NewXSellNonMatchingProductsRow();
                        row.pkey_category_type = Convert.ToInt32(item);
                        model.XSellNonMatchingProducts.AddXSellNonMatchingProductsRow(row);
                    }
                }
                PromoService.UpdateXSellProductsData(model, ConnString);
                Response.Redirect("../Account/DashBoard.aspx");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript("Save/Update failed.Try again");
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/DashBoard.aspx");
        }
    }
}