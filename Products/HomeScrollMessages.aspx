﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="HomeScrollMessages.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Products.HomeScrollMessages" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="contant_hldr">
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Product Setup & Configuration</li>
            <li>Home Page Scrolling Messages</li>
        </ul>
        <h2>Home Page Scrolling Messages</h2>
        <form class="form" runat="server" id="frmOrderFulfillmentList">
            <div class="form_sep_left" style="float: left;">
            <table class="accounts-table">
                <tbody>
                    <tr>
                        <td width="20%">Filter List for </td>
                        <td class="a_lft" width="20%">
                            <asp:DropDownList name="ddlFilterAct" ID="ddlFilterAct" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterAct_SelectedIndexChanged">
                                <asp:ListItem Value="-2">All</asp:ListItem>
                                <asp:ListItem Value="1">Active</asp:ListItem>
                                <asp:ListItem Value="0">InActive</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
                </div>           
            <div class="clr"></div>
            <asp:GridView ID="HomeScrollGrid" runat="server" AutoGenerateColumns="false" EnableViewState="true"
                ForeColor="Black" Width="100%" PageSize="20" AllowPaging="true" AllowSorting="true"
                class="table" name="HomeScrollGrid" OnPageIndexChanging="HomeScrollGrid_PageIndexChanging">
                <PagerStyle HorizontalAlign="Right" CssClass="dtPage"></PagerStyle>
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />
                <Columns>
                    <asp:TemplateField HeaderText="#SL">
                        <HeaderStyle Width="6%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <span><%# GetRecordCountForPage()%></span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Message">
                        <HeaderStyle Width="52%" ForeColor="White" />
                        <ItemTemplate>
                           <a href="HomeScrollMessageDetail.aspx?Pkey=<%#Eval("peky_portal_message") %>" title="Edit"> <%#Eval("description") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:BoundField HeaderText="From Date" ItemStyle-Width="14%" DataField="fromdate" DataFormatString="{0:MM/dd/yyyy}"></asp:BoundField>

                    <asp:BoundField HeaderText="To Date" ItemStyle-Width="14%" DataField="todate" DataFormatString="{0:MM/dd/yyyy}"></asp:BoundField>

                    <asp:TemplateField HeaderText="Active">
                        <HeaderStyle Width="14%" ForeColor="White" />
                        <ItemTemplate>
                            <asp:Label ID="lblActive" runat="server" Text='<%#Eval("isactive").ToString() == "True" ? "Active" : "Inactive"%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <div class="btn_lst">


                 <div class="rgt">
                    <asp:Button runat="server" ID="btnClose" Text="Close" CssClass="button rounded" OnClick="btnClose_Click"></asp:Button>
                </div>
                <div class="lft">
                    <asp:Button runat="server" ID="btnAdd" Text="Add a New Message" CssClass="button rounded" OnClick="btnAdd_Click"></asp:Button>
                </div>

               
            </div>
        </form>
    </div>
</asp:Content>
