﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.UI.HtmlControls;

namespace Bestprintbuy.Admin.Web.Products
{
    public partial class SupplierManifestSettings : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadData();
            }
        }

        private void LoadData()
        {
            try
            {
                ProductModel model = ProductService.GetSupplierManifestSettings(-1, ConnString);
                ddlProducts.DataSource = model.Categories;
                ddlProducts.DataTextField = "ct_name";
                ddlProducts.DataValueField = "pkey_category_type";
                ddlProducts.DataBind();

                ManifestListGrid.DataSource = model.SupplierManifestSettings;
                ManifestListGrid.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected int GetRecordCountForPage()
        {
            return GetRecordCountForPageSize(ManifestListGrid.PageSize, ManifestListGrid.PageIndex + 1);
        }

        protected void ManifestListGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ManifestListGrid.PageIndex = e.NewPageIndex;
            LoadData();
        }

        protected void ddlProducts_SelectedIndexChanged(object sender, EventArgs e)
        {
            ProductModel model = ProductService.GetSupplierManifestSettings(Convert.ToInt32(ddlProducts.SelectedValue), ConnString);
            ManifestListGrid.DataSource = model.SupplierManifestSettings;
            ManifestListGrid.DataBind();
        }

        protected void btnSaveProductTranslation_Click(object sender, EventArgs e)
        {
            try
            {
                ProductModel model = new ProductModel();
                foreach (GridViewRow row in ManifestListGrid.Rows)
                {
                    ProductModel.SupplierManifestSettingsRow Row = model.SupplierManifestSettings.NewSupplierManifestSettingsRow();
                    HiddenField hdnpkeyProductTaXref = (HiddenField)row.FindControl("hdnpkeyProductTaXref");
                    TextBox txtCardsForSm = (TextBox)row.FindControl("txtCardsForSm");
                    TextBox txtSaDays = (TextBox)row.FindControl("txtSaDays");
                    TextBox txtTaSaCode = (TextBox)row.FindControl("txtTaSaCode");

                    Row.pkey_product_ta_xref = Convert.ToInt32(hdnpkeyProductTaXref.Value);
                    Row.cards_for_sm = txtCardsForSm.Text;
                    Row.sa_days = Convert.ToInt32(txtSaDays.Text == "" ? "0" : txtSaDays.Text);
                    Row.ta_sa_code = txtTaSaCode.Text;

                    model.SupplierManifestSettings.AddSupplierManifestSettingsRow(Row);
                }
                model = ProductService.UpdateSupplierManifestSettings(model, Convert.ToInt32(ddlProducts.SelectedValue), ConnString);
                AlertScript("Data Successfully Updated");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/DashBoard.aspx");
        }
    }
}