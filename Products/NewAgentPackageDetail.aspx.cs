﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.Products
{
    public partial class NewAgentPackageDetail : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                try
                {
                    GetData();
                }
                catch (Exception ex)
                {
                    FileLogger.WriteException(ex);
                    AlertScript(ex.Message);
                }
            }
        }

        private void GetData()
        {
            int agentPKey = Convert.ToInt32(Request.QueryString["agentPKey"]);
            AgentPackageModel agentPackageDetails = ProductService.GetAgentPackageDataByKey(agentPKey, ConnString);

            if (agentPackageDetails.PackageQuantities.Rows.Count > 0)
            {
                var bcQty = from collect in agentPackageDetails.PackageQuantities
                            where collect.fkey_type == 243
                            select collect;
                ddlBC.DataSource = bcQty.ToList();
                ddlBC.DataTextField = "quantity";
                ddlBC.DataValueField = "quantity";
                ddlBC.DataBind();

                var lhQty = from collect in agentPackageDetails.PackageQuantities
                            where collect.fkey_type == 249
                            select collect;
                ddlLH.DataSource = lhQty.ToList();
                ddlLH.DataTextField = "quantity";
                ddlLH.DataValueField = "quantity";
                ddlLH.DataBind();

                var enQty = from collect in agentPackageDetails.PackageQuantities
                            where collect.fkey_type == 250
                            select collect;
                ddlEN.DataSource = enQty.ToList();
                ddlEN.DataTextField = "quantity";
                ddlEN.DataValueField = "quantity";
                ddlEN.DataBind();

                var fcQty = from collect in agentPackageDetails.PackageQuantities
                            where collect.fkey_type == 853
                            select collect;
                ddlFC.DataSource = fcQty.ToList();
                ddlFC.DataTextField = "quantity";
                ddlFC.DataValueField = "quantity";
                ddlFC.DataBind();
            }

            if (agentPackageDetails.NewAgentPackage.Rows.Count > 0)
            {
                AgentPackageModel.NewAgentPackageRow agentPackageRow = (AgentPackageModel.NewAgentPackageRow)agentPackageDetails.NewAgentPackage.Rows[0];
                txtPackage.Text = agentPackageRow.package;
                txtDescription.Text = agentPackageRow.package_desc;
                txtActualPrice.Text = agentPackageRow.actual_cost.ToString();
                txtDiscountPrice.Text = agentPackageRow.package_cost.ToString();
                txtServiceCrAmt.Text = agentPackageRow.service_credit_amount.ToString();
                txtDiscountNote.Text = agentPackageRow.discount_note;
                chkActive.Checked = agentPackageRow.is_active;

                ddlBC.SelectedValue = agentPackageRow.bc_qty.ToString();
                ddlLH.SelectedValue = agentPackageRow.lh_qty.ToString();
                ddlEN.SelectedValue = agentPackageRow.env_qty.ToString();
                ddlFC.SelectedValue = agentPackageRow.fc_qty.ToString();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int agentPKey = Convert.ToInt32(Request.QueryString["agentPKey"]);
                AgentPackageModel agentPkgDtls = new AgentPackageModel();
                AgentPackageModel.NewAgentPackageRow agentPkgRow = agentPkgDtls.NewAgentPackage.NewNewAgentPackageRow();
                agentPkgRow.pkey_new_agent_package = agentPKey;
                agentPkgRow.package = txtPackage.Text;
                agentPkgRow.package_desc = txtDescription.Text;
                agentPkgRow.is_active = chkActive.Checked;
                agentPkgRow.bc_qty = Convert.ToInt32(ddlBC.SelectedValue);
                agentPkgRow.lh_qty = Convert.ToInt32(ddlLH.SelectedValue);
                agentPkgRow.env_qty = Convert.ToInt32(ddlEN.SelectedValue);
                agentPkgRow.fc_qty = Convert.ToInt32(ddlFC.SelectedValue);
                agentPkgRow.actual_cost = Convert.ToDecimal(txtActualPrice.Text);
                agentPkgRow.package_cost = Convert.ToDecimal(txtDiscountPrice.Text);
                agentPkgRow.service_credit_amount = Convert.ToDecimal(txtServiceCrAmt.Text);
                agentPkgRow.discount_note = txtDiscountNote.Text;
                agentPkgDtls.NewAgentPackage.AddNewAgentPackageRow(agentPkgRow);

                agentPKey = ProductService.UpdateAgentPackage(agentPkgDtls, ConnString);
                AlertScript("Save/Update Successfully completed.");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("NewAgentPackageList.aspx");
        }
    }
}