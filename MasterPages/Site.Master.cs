﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using System.Configuration;
using System.Data;

namespace Bestprintbuy.Admin.Web
{
    public partial class Site : System.Web.UI.MasterPage
    {
        public string m_timeStamp = DateTime.Now.Ticks.ToString();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["UserName"] == null || Session["UserName"].ToString() == "")
                {
                    Response.Redirect("../Login.aspx");
                }
                ltrlLoginName.Text = Session["UserName"].ToString();
                string absolutePath = Request.Url.AbsolutePath;
                if (absolutePath.Contains("/DashBoard.aspx"))
                {
                    liDashBoard.Attributes.Add("class", "active");
                }
                if (absolutePath.Contains("/UserList.aspx") || absolutePath.Contains("/GroupList.aspx") || absolutePath.Contains("/UserDetails.aspx")
                    || absolutePath.Contains("/GroupDetails.aspx") || absolutePath.Contains("/ChangePassword.aspx"))
                {
                    liSecurity.Attributes.Add("class", "active");
                }
                if (absolutePath.Contains("/OrderFulfillment.aspx") || absolutePath.Contains("/AdminCustLogin.aspx") || absolutePath.Contains("/CustomersList.aspx")
                    || absolutePath.Contains("/CustomerRegistration.aspx") || absolutePath.Contains("/PromoList.aspx") || absolutePath.Contains("/PromoDetails.aspx")
                    || absolutePath.Contains("/CatalogReqList.aspx") || absolutePath.Contains("/RegisteredUsersList.aspx") || absolutePath.Contains("/TemplateSearch.aspx")
                    || absolutePath.Contains("/CustomerCreditList.aspx") || absolutePath.Contains("/CustomerValidate.aspx") || absolutePath.Contains("/CustomerCreditDetails.aspx")
                     || absolutePath.Contains("/MCACustomerList.aspx") || absolutePath.Contains("/MCACustomerDetails.aspx"))
                {
                    liCustomerService.Attributes.Add("class", "active");
                }
                if (absolutePath.Contains("/XSellProducts.aspx") || absolutePath.Contains("/XSellDiscountList.aspx") || absolutePath.Contains("/XSellDiscountDetails.aspx") 
                    || absolutePath.Contains("/NewAgentPackageList.aspx") || absolutePath.Contains("/NewAgentPackageDetail.aspx") || absolutePath.Contains("/NewAgentPackageFC.aspx") 
                    || absolutePath.Contains("/NewAgentInsideFC.aspx") || absolutePath.Contains("/SupplierManifestSettings.aspx") || absolutePath.Contains("/HomeScrollMessageDetail.aspx")
                    || absolutePath.Contains("/HomeScrollMessages.aspx") || absolutePath.Contains("/SetShipOptions.aspx"))
                {
                    liProducts.Attributes.Add("class", "active");
                }
                if (absolutePath.Contains("/CustomerInvoiceList.aspx") || absolutePath.Contains("/CustomerInvoiceDetail.aspx") || absolutePath.Contains("/CustomerPayment.aspx")
                    || absolutePath.Contains("/CustomerPayments.aspx") || absolutePath.Contains("/CustomerReceipts.aspx") || absolutePath.Contains("/CustomerReceiptsDetail.aspx")
                    || absolutePath.Contains("/CustomerReceiptView.aspx") || absolutePath.Contains("/PaymentList.aspx") || absolutePath.Contains("/PaymentDetails.aspx")
                    || absolutePath.Contains("/PaymentDetailsView.aspx") || absolutePath.Contains("/StatementofAccount.aspx") || absolutePath.Contains("/AdjustAdvancePayments.aspx"))
                {
                    liAccount.Attributes.Add("class", "active");
                }
                if (absolutePath.Contains("/MCAReport.aspx") || absolutePath.Contains("/PendingSprocketOrders.aspx") || absolutePath.Contains("/CatalogReportStatus.aspx")
                    || absolutePath.Contains("/TransactionReport.aspx") || absolutePath.Contains("/CatalogReport.aspx"))
                {
                    liAdminReports.Attributes.Add("class", "active");
                }
                if (absolutePath.Contains("/EmailSummary.aspx") || absolutePath.Contains("/EMCustomersList.aspx") || absolutePath.Contains("/EMCustomersMailer.aspx") 
                    || absolutePath.Contains("/EMCustomerStats.aspx") || absolutePath.Contains("/ExcludeEmailFromReports.aspx") || absolutePath.Contains("/CustomersWithCart.aspx")
                    || absolutePath.Contains("/CustomersWithSaveDesign.aspx") || absolutePath.Contains("/CustomersByCampaign.aspx"))
                {
                    liMarketingReports.Attributes.Add("class", "active");
                }
                if (absolutePath.Contains("/VariantList.aspx") || absolutePath.Contains("/VariantDetails.aspx") || absolutePath.Contains("/AttributeList.aspx")
                    || absolutePath.Contains("/AttributeDetails.aspx") || absolutePath.Contains("/ProductList.aspx") || absolutePath.Contains("/ProductDetails.aspx")
                    || absolutePath.Contains("/FreeProductsList.aspx") || absolutePath.Contains("/FreeProductDetail.aspx") || absolutePath.Contains("/FreePromoList.aspx")
                    || absolutePath.Contains("/FreePromoDetails.aspx") || absolutePath.Contains("/TemplateList.aspx") || absolutePath.Contains("/TemplateDetail.aspx")
                    || absolutePath.Contains("/RevTemplateList.aspx") || absolutePath.Contains("/RevTemplateDetail.aspx") || absolutePath.Contains("/RevTemplateListAfterAproveDesign.aspx")
                    || absolutePath.Contains("/RealtorsList.aspx") || absolutePath.Contains("/RealtorDetails.aspx") || absolutePath.Contains("/CustMessagesList.aspx")
                    || absolutePath.Contains("/CustMessageDetail.aspx") || absolutePath.Contains("/ECInfoList.aspx") || absolutePath.Contains("/EcInfo.aspx")
                    || absolutePath.Contains("/KiosksCompanyList.aspx") || absolutePath.Contains("/MasterCategoryList.aspx") || absolutePath.Contains("/MasterCategoryDetail.aspx")
                    || absolutePath.Contains("/CategoryList.aspx") || absolutePath.Contains("/CategoryDetails.aspx") || absolutePath.Contains("/TypeList.aspx")
                    || absolutePath.Contains("/TypeDetails.aspx") || absolutePath.Contains("/CountryList.aspx") || absolutePath.Contains("/CountryDetail.aspx")
                    || absolutePath.Contains("/StateList.aspx") || absolutePath.Contains("/StateDetails.aspx") || absolutePath.Contains("/AddCompanyLogo.aspx"))
                {
                    liSystemConfig.Attributes.Add("class", "active");
                }
                if (absolutePath.Contains("/BusinessRegisterList.aspx") || absolutePath.Contains("/BusinessRegisterDetail.aspx") || absolutePath.Contains("/BusinessRegisterView.aspx")
                    || absolutePath.Contains("/SupplierModRemList.aspx") || absolutePath.Contains("/SupplierModReminder.aspx") || absolutePath.Contains("/ASupplierPayment.aspx")
                    || absolutePath.Contains("/BusinessContactDetails.aspx") || absolutePath.Contains("/BusinessContactsList.aspx"))
                {
                    liBusinessReg.Attributes.Add("class", "active");
                }
            }
        }

        protected void btnLogout_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Login.aspx");
        }

        protected UserModel accessModel = null;
        public bool HasAccess(string p_screenName, string p_functionaName)
        {
            UserIdentity identity = (new PageBase()).Identity;
            if (identity == null)
                return false;
            if (identity.IsSuperUser)
                return true;

            if (accessModel == null)
                accessModel = UserService.GetUserRoles(identity.UserPK, ConfigurationManager.AppSettings["BestPrintBuyDBConn"]);
            string l_screenName = GetScreenName(p_screenName);
            string l_filter = "Screen_Name='" + l_screenName + "'";
            if (p_functionaName == "list")
                l_filter += " and (listaccess = 'Yes' or listaccess = 'yes')";
            else if (p_functionaName == "view")
                l_filter += " and (viewaccess = 'Yes' or viewaccess = 'yes')";
            else if (p_functionaName == "delete")
                l_filter += " and (deleteaccess = 'Yes' or deleteaccess = 'yes')";
            else if (p_functionaName == "edit")
                l_filter += " and (editaccess = 'Yes' or editaccess = 'yes')";
            else if (p_functionaName == "add")
                l_filter += " and (addaccess = 'Yes' or addaccess = 'yes')";
            DataRow[] dr = accessModel.UserAccess.Select(l_filter);

            if (dr.Length == 1)
                return true;
            return false;
        }

        private string GetScreenName(string functionScreenName)
        {
            if (WebConfiguration.AdminLoginScreens.ContainsKey(functionScreenName))
                return WebConfiguration.AdminLoginScreens[functionScreenName].ToString();
            else if (WebConfiguration.PPSOLScreens.ContainsKey(functionScreenName))
                return WebConfiguration.PPSOLScreens[functionScreenName].ToString();
            else if (WebConfiguration.SupplierScreens.ContainsKey(functionScreenName))
                return WebConfiguration.SupplierScreens[functionScreenName].ToString();
            else if (WebConfiguration.PortalScreens.ContainsKey(functionScreenName))
                return WebConfiguration.PortalScreens[functionScreenName].ToString();
            return "";
        }
    }
}