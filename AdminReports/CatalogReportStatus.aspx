﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="CatalogReportStatus.aspx.cs" Inherits="Bestprintbuy.Admin.Web.AdminReports.CatalogReportStatus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function validate_twoDates() {
            var dateStart = $("#ContentPlaceHolder1_txtFromDate").val();
            var dateEnd = $("#ContentPlaceHolder1_txtToDate").val();
            if (Date.parse(dateStart) > Date.parse(dateEnd)) {
                alert("Invalid date range!");
                return false;
            }
        }
        $(function () {
            $("#ContentPlaceHolder1_txtFromDate").datepicker();
            $("#ContentPlaceHolder1_txtToDate").datepicker();
            $("#frmCatalogreportStatus").validate();
        });
    </script>
    <div class="contant_hldr">
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Administrative Reports</li>
            <li>Catalog Report Status</li>
        </ul>
        <h2>Catalog Report Status</h2>
        <form class="" runat="server" id="frmCatalogreportStatus">
            <table class="accounts-table" width="70%">
                <tbody>
                    <tr>
                        <td width="15%">From Date</td>
                        <td width="10%">
                            <asp:TextBox name="txtFromDate" ID="txtFromDate" CssClass="text-box date" runat="server"></asp:TextBox>
                        </td>
                        <td width="10%"></td>
                        <td width="5%">To Date</td>
                        <td width="15%">
                            <asp:TextBox name="txtToDate" ID="txtToDate" CssClass="text-box date" runat="server"></asp:TextBox></td>
                        <td width="45%"></td>
                    </tr>
                    <tr>
                        <td width="10%">Kiosk</td>
                        <td width="10%">
                            <asp:DropDownList name="ddlKiosk" ID="ddlKiosk" CssClass="slect-box" data-placeholder="Choose a Name" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td width="10%"></td>
                        <td width="5%">
                            <asp:Button runat="server" ID="btnFilter" CssClass="button rounded" Text="Filter" OnClick="btnFilter_Click" OnClientClick="return validate_twoDates();"></asp:Button>
                        </td>
                        <td width="15%">
                            <asp:Button runat="server" ID="btnReset" CssClass="button rounded" UseSubmitBehavior="false" Text="Reset" Visible="false" OnClick="btnReset_Click"></asp:Button>
                        </td>
                    </tr>
                </tbody>
            </table>
            <asp:Panel ID="pnlContent" runat="server" Visible="false">
                <div class="clr"></div>
                <table class="table" width="100%">
                    <thead>
                        <tr>
                            <th>#SL</th>
                            <th>Order No</th>
                            <th>SKU</th>
                            <th>SO#</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Status</th>
                            <th>Tracking#</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rptrCatalogStatus" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <%#Container.ItemIndex+1 %>
                                    </td>
                                    <td>
                                        <%#Eval("CR_MM_ORDERNO") %>
                                    </td>
                                    <td>
                                        <%#Eval("CR_SKU_NUMBER") %>
                                    </td>
                                    <td>
                                        <%#Eval("CR_SUPPLIER_ORDER_ID") %>
                                    </td>
                                    <td>
                                        <%#Eval("First Name") %>
                                    </td>
                                    <td>
                                        <%#Eval("Last Name") %>
                                    </td>
                                    <td>
                                        <%#Eval("FromEmail") %>
                                    </td>
                                    <td>
                                        <%#Eval("ORDER_STATUS") %>
                                    </td>
                                    <td>
                                        <%#Eval("TRACKING_NUMER") %>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
                 <div class="ttl"><b>Total Catalog Requests: </b><%=recordCount%></div>
                <div class="btn_lst">
                    <div class="rgt">
                        <asp:Button runat="server" CssClass="button rounded" ID="btnClose" Text="Close" OnClick="btnClose_Click" UseSubmitBehavior="false"></asp:Button>
                        <asp:Button runat="server" CssClass="button rounded" ID="btnUpdateOrderStatus" Text="Update Order Status" OnClick="btnUpdateOrderStatus_Click" UseSubmitBehavior="false"></asp:Button>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlClose" runat="server">
                 <div class="ttl"><b>Total Catalog Requests: </b><%=recordCount%></div>
                <div class="btn_lst">
                    <div class="rgt">
                        <asp:Button runat="server" CssClass="button rounded" ID="Button1" Text="Close" OnClick="btnClose_Click" UseSubmitBehavior="false"></asp:Button>
                    </div>
                </div>
            </asp:Panel>
        </form>
    </div>
</asp:Content>
