﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Text;
using System.Collections;

namespace Bestprintbuy.Admin.Web.AdminReports
{
    public partial class CustomersByCampaign : PageBase
    {
        StringBuilder l_strCSV = new StringBuilder();
        int pkey_campaign_master;
        private ReportsModel model = new ReportsModel();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    model = (ReportsModel)LoadCustomerList();
                    BindGrid(model);
                }
                catch (Exception ex)
                {
                    FileLogger.WriteException(ex);
                    AlertScript(ex.Message);
                }
            }
        }

        private ReportsModel LoadCustomerList()
        {
            pkey_campaign_master = Convert.ToInt32(Request["ID"]);
            int fkey_ec_attribute = -1;
            string customerStatus = "ALL";
            if (Session["EmailSummaryFilter"] != null)
            {
                try
                {
                    Hashtable ht = (Hashtable)Session["EmailSummaryFilter"];
                    if (ht.ContainsKey("fkey_ec_attributes"))
                        fkey_ec_attribute = Convert.ToInt32(ht["fkey_ec_attributes"]);
                    if (ht.ContainsKey("CustomerStatus"))
                        customerStatus = Convert.ToString(ht["CustomerStatus"]);
                }
                catch { }
            }

            model = ReportService.GetEmailCampaignDetails(pkey_campaign_master, fkey_ec_attribute, customerStatus, ConnString);
            SetTitle(pkey_campaign_master, model.EmailCustomerInfo.Rows.Count);
            return model;
        }

        private void SetTitle(int pkey_campaign_master, int count)
        {
            Hashtable htFilterData = (Hashtable)Session["EmailSummaryFilter"];
            switch (pkey_campaign_master)
            {
                case 1:
                case 6:
                case 11:
                case 19:
                    lblCategory.Text = string.Format("{0} new users registered but not ordered btn {1}-{2}",
                        count, htFilterData["FromDate"], htFilterData["ToDate"]);
                    break;
                case 2:
                case 7:
                case 12:
                case 20:
                    lblCategory.Text = string.Format("{0} users ordered BC only btn {1}-{2}",
                        count, htFilterData["FromDate"], htFilterData["ToDate"]);
                    break;
                case 3:
                case 8:
                case 13:
                case 21:
                    lblCategory.Text = string.Format("{0} users ordered multiple products btn {1}-{2}",
                        count, htFilterData["FromDate"], htFilterData["ToDate"]);
                    break;
                case 4:
                case 9:
                case 14:
                case 22:
                    lblCategory.Text = string.Format("{0} users ordered Postcards btn {1}-{2}",
                        count, htFilterData["FromDate"], htFilterData["ToDate"]);
                    break;
                case 5:
                case 10:
                case 15:
                case 23:
                    lblCategory.Text = string.Format("{0} users with items in the shopping cart btn {1}-{2}",
                        count, htFilterData["FromDate"], htFilterData["ToDate"]);
                    break;
                default:
                    lblCategory.Text = string.Format("{0} new users with just saved designs btn {1}-{2}",
                        count, htFilterData["FromDate"], htFilterData["ToDate"]);
                    break;
            }
        }
        public void BindGrid(ReportsModel model)
        {
            rptrCustStats.DataSource = model.EmailCustomerInfo;
            rptrCustStats.DataBind();
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("EmailSummary.aspx");
        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            CreateCSVFile();
        }

        private void CreateCSVFile()
        {
            try
            {
                model = (ReportsModel)LoadCustomerList();
                string strFileName = string.Format("{0}-CustomersByCampaign.csv", DateTime.Now.ToString("MM-dd-yyyy"));
                Response.Clear();
                Response.ContentType = "text/csv";
                Response.AppendHeader("content-disposition", "attachment; filename= " + strFileName);
                CsvStrBuild();
                Response.Write(l_strCSV);
                Response.Flush();
                Response.End();
            }
            catch (Exception)
            {
            }
        }

        private void CsvStrBuild()
        {
            // l_strCSV.AppendFormat("\"pkey_customer",\"customer_name\",\"email_id\",\"date_created\",\"fkey_ec_attribute\",\"email_type\"{0}", Environment.NewLine);

            l_strCSV.AppendFormat("\"S.No\",\"Customer Name\",\"Email Address\",\"Phone\",\"Date Registered\"{0}", Environment.NewLine);


            int iCounter = 1;
            foreach (ReportsModel.EmailCustomerInfoRow cartRow in model.EmailCustomerInfo.Rows)
            {
                l_strCSV.AppendFormat("\"{0}\",", iCounter);
                l_strCSV.AppendFormat("\"{0}\",", cartRow.customer_name);
                l_strCSV.AppendFormat("\"{0}\",", cartRow.email_id);
                l_strCSV.AppendFormat("\"{0}\",", cartRow.IsphoneNull() ? "" : cartRow.phone);
                l_strCSV.AppendFormat("\"{0}\",", cartRow.date_created);
                l_strCSV.AppendFormat(Environment.NewLine);
                iCounter++;
            }

        }
    }
}