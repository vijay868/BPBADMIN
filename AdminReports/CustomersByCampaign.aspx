﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="CustomersByCampaign.aspx.cs" Inherits="Bestprintbuy.Admin.Web.AdminReports.CustomersByCampaign" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="clr"></div>
    <!-- Container Starts -->
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Marketing Reports</li>
            <li>Email Marketing Program</li>
        </ul>
        <!-- Breadcrumbs Ends -->
             <h2><asp:Label runat="server" ID="lblCategory"></asp:Label></h2>
        <form id="frmBpbReports" name="frmBpbReports" runat="server">           
            <div class="clr"></div>
            <table class="table" width="100%">
                <thead>
                    <tr>
                        <th width="3%"></th>
                        <th>Customer Name</th>
                        <th>Email Address</th>
                        <th>Phone</th>
                        <th>Date Registered</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="rptrCustStats" runat="server">
                        <ItemTemplate>
                            <tr>
                                <td></td>
                                <td>
                                    <%#Eval("customer_name") %>
                                </td>
                                <td>
                                    <%#Eval("email_id") %>
                                </td>
                                <td>
                                    <%#Eval("phone") %>
                                </td>
                                <td>
                                    <%# Eval("date_created","{0:MM/dd/yyyy}")%>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
            <div class="btn_lst">
                <div class="rgt">
                    <asp:Button ID="btnClose" Text="Close" CssClass="button rounded" runat="server" OnClick="btnClose_Click"></asp:Button>
                    <asp:Button runat="server" ID="btnExportTocsv" Text="Export to CSV" CssClass="button rounded" OnClick="btnExportToExcel_Click" />
                </div>
            </div>
        </form>
    </div>
</asp:Content>

