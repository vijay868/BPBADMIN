﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="EMCustomerStats.aspx.cs" Inherits="Bestprintbuy.Admin.Web.AdminReports.EMCustomerStats" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="clr"></div>
    <!-- Container Starts -->
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Marketing Reports</li>
            <li>Email Marketing Program</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Customer Statistics</h2>
        <form id="frmBpbReports" name="frmBpbReports" runat="server" class="form">
            <table class="table">
                <thead>
                    <tr>
                        <th width="6%">#SL</th>
                        <th>Abbreviation</th>
                        <th>Category</th>
                        <th># Of Customers</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="rptrCustStats" runat="server">
                        <ItemTemplate>
                            <tr>
                                <td><%# Container.ItemIndex+1 %></td>
                                <td>
                                    <%#Eval("categoryabbr") %>
                                </td>
                                <td>
                                    <%#Eval("category") %>
                                </td>
                                <td>
                                    <a href="EMCustomersList.aspx?CSListFor=<%# Eval("categoryabbr")%>" data-gravity="s" title="Edit"><%#Eval("custcount") %></a>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
            <div class="btn_lst">
                <div class="lft">
                    <asp:Button runat="server" ID="btnReRun" CssClass="button rounded" Text="Re-Run Campaign Summaries" OnClick="btnReRun_Click"></asp:Button>
                </div>
                <div>
                    <asp:Button ID="btnClose" Text="Close" CssClass="button rounded" runat="server" OnClick="btnClose_Click"></asp:Button>
                    <asp:Button runat="server" ID="btnSendEmails" Text="Email Marketing" CssClass="button rounded" OnClick="btnSendEmails_Click"></asp:Button>
                </div>
            </div>
        </form>
    </div>
</asp:Content>
