﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Text;
using System.IO;
using System.Threading;

namespace Bestprintbuy.Admin.Web.AdminReports
{
    public partial class CustomersWithSaveDesign : PageBase
    {
        ReportsModel model = new ReportsModel();
        StringBuilder l_strCSV = new StringBuilder();
        int fkey_ec_attribute = -1;
        string currentDay = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtToDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
                currentDay = DateTime.Now.DayOfWeek.ToString();
                DateTime fromDate = DateTime.Now.AddDays(-7);
                while (fromDate.DayOfWeek != DayOfWeek.Sunday)
                    fromDate = fromDate.AddDays(1);
                txtFromDate.Text = fromDate.ToString("MM/dd/yyyy");
                LoadDDList();
                CustomersData();
            }
        }

        public void CustomersData()
        {
            try
            {                
                model = ReportService.GetCustomersSaveData(txtFromDate.Text, txtToDate.Text + " 23:59:59", Convert.ToInt32(ddlKiosk.SelectedValue), ddlProduct.Text, ddlCustomerstatus.Text, ConnString);
                CustomersSaveGrid.DataSource = model.CustomersWithCart;
                //serialNo = gvCustomersList.PageIndex * gvCustomersList.PageSize;
                CustomersSaveGrid.DataBind();
                lblTotalRecords.Text = string.Format("Total Items in Cart: {0}", model.CustomersWithCart.Rows.Count);
                btnExportTocsv1.Visible = model.CustomersWithCart.Rows.Count > 0;
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        public void LoadDDList()
        {
            try
            {                
                model = ReportService.GetKiosks(ConnString);
                ddlKiosk.DataSource = model.Kiosks;
                ddlKiosk.DataValueField = "fkey_ec_attribute";
                ddlKiosk.DataTextField = "kiosk";
                ddlKiosk.DataBind();
                ddlKiosk.Items.Insert(0, new ListItem("ALL", "-1"));
                ddlKiosk.SelectedIndex = 0;

                ddlDateRange.Items.Insert(0, new ListItem("Current Week", "-1"));
                ddlDateRange.Items.Insert(1, new ListItem("Last Week", "0"));
                ddlDateRange.Items.Insert(2, new ListItem("Last Month", "1"));
                ddlDateRange.Items.Insert(3, new ListItem("Date Range", "2"));
                //ddlDateRange.Items.Insert(4, new ListItem("last month", "3"));
                ddlDateRange.SelectedIndex = 0;

                ddlCustomerstatus.Items.Insert(0, new ListItem("ALL", "ALL"));
                ddlCustomerstatus.Items.Insert(1, new ListItem("New Customers", "New Customer"));
                ddlCustomerstatus.Items.Insert(2, new ListItem("Returning Customers", "Returning Customer"));
                ddlCustomerstatus.SelectedIndex = 0;

                LoadProductDDList(fkey_ec_attribute);
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        public void LoadProductDDList(int fkeyecattribute)
        {
            try
            {
                model = ReportService.GetProducts(fkey_ec_attribute, ConnString);

                ddlProduct.DataSource = model.Products;
                ddlProduct.DataValueField = "cards_for";
                ddlProduct.DataTextField = "product";
                ddlProduct.DataBind();

                ddlProduct.Items.Insert(0, new ListItem("ALL", "ALL"));
                ddlProduct.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/Dashboard.aspx");
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                ddlDateRange.Items.Clear();
                ddlCustomerstatus.Items.Clear();
                txtToDate.Text = System.DateTime.Now.ToString("MM/dd/yyyy");
                DateTime fromDate = System.DateTime.Now.AddDays(-7);
                while (fromDate.DayOfWeek != DayOfWeek.Sunday)
                    fromDate = fromDate.AddDays(1);
                txtFromDate.Text = fromDate.ToString("MM/dd/yyyy");
                LoadDDList();
                CustomersData();
                btnReset.Visible = false;
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            try
            {
                CustomersSaveGrid.PageIndex = 0;
                CustomersData();
                btnReset.Visible = true;    
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }
        protected int GetRecordCountForPage()
        {
            return GetRecordCountForPageSize(CustomersSaveGrid.PageSize, CustomersSaveGrid.PageIndex + 1);
        }

        protected void CustomersSaveGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CustomersSaveGrid.PageIndex = e.NewPageIndex;
            CustomersData();
        }

        protected void btnExportTocsv_Click(object sender, EventArgs e)
        {
            CreateCSVFile();
        }

        private void CreateCSVFile()
        {
            try
            {
                CustomersData();
                string strFileName = string.Format("CustomersWithItemsInCart-{0}-{1}.csv",
                    txtFromDate.Text.Replace("/", "-"), txtToDate.Text.Replace("/", "-"));
                Response.Clear();
                Response.ContentType = "text/csv";
                Response.AppendHeader("content-disposition", "attachment; filename= " + strFileName);
                CsvStrBuild();
                Response.Write(l_strCSV);
                Response.Flush();
                Response.End();
            }
            catch (Exception)
            {
            }
        }

        private void CsvStrBuild()
        {
            l_strCSV.AppendFormat("\"S.No\",\"Kiosk\",\"Customer Name\",\"Date Created\",\"Phone\",\"Last Logged In\",\"Product\",\"Qty\",\"Customer Status\",\"Ship Option\"{0}", Environment.NewLine);

            int iCounter = 1;
            foreach (ReportsModel.CustomersWithCartRow cartRow in model.CustomersWithCart.Rows)
            {
                l_strCSV.AppendFormat("\"{0}\",", iCounter);
                l_strCSV.AppendFormat("\"{0}\",", cartRow.IskioskNull() ? "" :cartRow.kiosk);
                l_strCSV.AppendFormat("\"{0}\",", cartRow.IscustomernameNull() ?"":cartRow.customername);
                l_strCSV.AppendFormat("\"{0}\",", cartRow.date_created);
                l_strCSV.AppendFormat("\"{0}\",", cartRow.IsphoneNull() ? "" :cartRow.phone);
                l_strCSV.AppendFormat("\"{0}\",", cartRow.lastloggedin);
                l_strCSV.AppendFormat("\"{0}\",", cartRow.IsproductNull() ? "" :cartRow.product);
                l_strCSV.AppendFormat("\"{0}\",", cartRow.IsquantityNull() ? 0 : cartRow.quantity);
                l_strCSV.AppendFormat("\"{0}\",", cartRow.IscustomerstatusNull() ? "" : cartRow.customerstatus);
                l_strCSV.AppendFormat("\"{0}\"", cartRow.IsshipoptionNull() ? "":cartRow.shipoption);
                l_strCSV.AppendFormat(Environment.NewLine);
                iCounter++;
            }

        }

        protected void ddlDateRange_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlDateRange.SelectedIndex == 0)
                {
                    txtToDate.Text = System.DateTime.Now.ToString("MM/dd/yyyy");
                    DateTime fromDate = System.DateTime.Now.AddDays(-7);
                    while (fromDate.DayOfWeek != DayOfWeek.Sunday)
                        fromDate = fromDate.AddDays(1);
                    txtFromDate.Text = fromDate.ToString("MM/dd/yyyy");
                }
                else if (ddlDateRange.SelectedIndex == 1)
                {
                    DateTime toDate = System.DateTime.Now.AddDays(-7);
                    while (toDate.DayOfWeek != DayOfWeek.Saturday)
                        toDate = toDate.AddDays(1);
                    DateTime fromDate = toDate.AddDays(-6);
                    txtToDate.Text = toDate.ToString("MM/dd/yyyy");
                    txtFromDate.Text = fromDate.ToString("MM/dd/yyyy");
                }
                else if (ddlDateRange.SelectedIndex == 2)
                {
                    DateTime dt = DateTime.Now;
                    int dayofDay = dt.Day;
                    //int dayofMonth = dt.Month;
                    dt = System.DateTime.Now.AddDays(-dayofDay + 1);
                    txtToDate.Text = System.DateTime.Now.AddDays(-dayofDay).ToString("MM/dd/yyyy");
                    txtFromDate.Text = dt.AddMonths(-1).ToString("MM/dd/yyyy");

                }
                else if (ddlDateRange.SelectedIndex == 3)
                {
                    txtToDate.Text = "";
                    txtFromDate.Text = "";
                }
                CustomersData();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }            
        }
    }
}