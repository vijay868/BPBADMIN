﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Text;
using System.IO;
using System.Threading;

namespace Bestprintbuy.Admin.Web.AdminReports
{
    public partial class EMCustomerStats : PageBase
    {
        int fkey_ec_attribute = -1;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadCustomerStats();
                Session.Remove("EmailSummaryFilter");
            }
        }

        private void LoadCustomerStats()
        {
            try
            {
                ReportsModel model = ReportService.GetCustomerStatistics(ConnString);
                rptrCustStats.DataSource = model.CustomerStatistics;
                rptrCustStats.DataBind();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/Dashboard.aspx");
        }

        protected void btnSendEmails_Click(object sender, EventArgs e)
        {
            Response.Redirect("EmailSummary.aspx");
        }

        protected void btnReRun_Click(object sender, EventArgs e)
        {
            try
            {
                ReportService.RunCampaignSummariesAndGetStatistics(ConnString);
                AlertScript("Re-Run Campaign Summaries successfully completed.");
            }
            catch(Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript("Unable to re-run the campaign summaries. Please try again.");
            }
        }
    }
}