﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Text;
using System.IO;
using System.Threading;
using System.Data;
using System.Collections;
using System.Web.UI.HtmlControls;

namespace Bestprintbuy.Admin.Web.AdminReports
{
    public partial class PendingSprocketOrders : PageBase
    {
        protected int recordCount = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                try
                {
                    BindSprocketOrderData();
                }
                catch (Exception ex)
                {
                    FileLogger.WriteException(ex);
                    AlertScript(ex.Message);
                }
            }
        }

        private TransactionsModel BindSprocketOrderData()
        {
            TransactionsModel cData = ReportService.GetSprocketPendingOrders(Convert.ToInt32(ddlStatus.SelectedValue), ConnString);

            rptrOrdersList.DataSource = cData.SprocketOrders;
            rptrOrdersList.DataBind();

            recordCount = cData.SprocketOrders.Rows.Count;

            if (cData.SprocketOrders.Rows.Count > 0)
            {
                pnlContent.Visible = true;
                pnlClose.Visible = false;
                btnPostOrdersToSupplier.Visible = true;
                btnUpdateOrderStatus.Visible = false;
                thchkEmail.Visible = true;
                thtracking.Visible = false;
                foreach (RepeaterItem item in rptrOrdersList.Items)
                {
                    HtmlTableCell tdchkEmail = (HtmlTableCell)item.FindControl("tdchkEmail");
                    HtmlTableCell tdtracking = (HtmlTableCell)item.FindControl("tdtracking");
                    tdtracking.Visible = false;
                    tdchkEmail.Visible = true;
                    if (Convert.ToInt32(ddlStatus.SelectedValue) > 2)
                    {
                        tdchkEmail.Visible = false;
                        tdtracking.Visible = true;
                    }
                }
                if (Convert.ToInt32(ddlStatus.SelectedValue) > 2)
                {
                    btnPostOrdersToSupplier.Visible = false;
                    thchkEmail.Visible = false;
                    thtracking.Visible = true;
                }
                if (ddlStatus.SelectedValue == "3")
                {
                    btnUpdateOrderStatus.Visible = true;
                }
            }
            else
            {
                pnlContent.Visible = false;
                pnlClose.Visible = true;
            }
            return cData;
        }

        protected void btnPostOrdersToSupplier_Click(object sender, EventArgs e)
        {
            try
            {
                TransactionsModel cData = ReportService.GetSprocketPendingOrders(Convert.ToInt32(ddlStatus.SelectedValue), ConnString);
                TransactionsModel TransData = new TransactionsModel();

                StringBuilder sbError = new StringBuilder();
                bool hasErr = false;
                foreach (RepeaterItem item in rptrOrdersList.Items)
                {
                    CheckBox chkSelect = (CheckBox)item.FindControl("chkEmail");
                    HiddenField skunumber = (HiddenField)item.FindControl("hdnSku");
                    HiddenField hdnPkeyCustPoDtl = (HiddenField)item.FindControl("hdnPkeyCustPoDtl");
                    HiddenField orderNum = (HiddenField)item.FindControl("hdnPoDtlNumber");

                    if (chkSelect.Checked)
                    {
                        int pkey_cust_po_dtl = Convert.ToInt32(hdnPkeyCustPoDtl.Value);

                        DataRow[] drs = cData.SprocketOrders.Select(string.Format("Units > 0 AND pkey_cust_po_dtl = {0}", pkey_cust_po_dtl));

                        if (drs.Length >= 1)
                        {

                            TransactionsModel.SprocketOrdersRow row = (TransactionsModel.SprocketOrdersRow)drs[0];
                            if (!row.Ispo_numberNull())
                            {
                                Hashtable data = SprocketProcessor.PostPrePressOrderToSprocket(row);

                                if (data != null)
                                {
                                    bool isError = Convert.ToBoolean(data["error"]);
                                    if (isError == false)
                                    {
                                        int api_order_id = Convert.ToInt32(data["api_order_id"]);
                                        TransactionsModel.SprocketOrdersRow catRow = TransData.SprocketOrders.NewSprocketOrdersRow();
                                        catRow.ItemArray = row.ItemArray;
                                        catRow.status_type = 3;
                                        catRow.supplier_comments = string.Format("{0}SENT TO SUPPLIER. SUPPLIER ORDER ID: {1}",
                                            catRow.Issupplier_commentsNull() ? "" : string.Format("{0} - ", catRow.supplier_comments),
                                            api_order_id.ToString());

                                        if (catRow.ship_price_option == "UPV")
                                        {
                                            catRow.carrier_name = "UPS Next Day Air";
                                        }
                                        else if (catRow.ship_price_option == "UR2")
                                        {
                                            catRow.carrier_name = "UPS 2nd Day Air";
                                        }
                                        else
                                        {
                                            catRow.carrier_name = "UPS Ground";
                                        }

                                        TransData.SprocketOrders.Rows.Add(catRow);
                                    }
                                    else
                                    {
                                        ArrayList al = (ArrayList)data["error_msgs"];
                                        sbError.AppendFormat("{0} cannot be posted to supplier - {1}{2}", orderNum.Value,
                                            (al != null && al.Count > 0) ? al[0] : "", "<br />");
                                        hasErr = true;
                                    }
                                }
                            }
                            else
                            {
                                sbError.AppendFormat("{0} cannot be posted to supplier - Email Order to supplier.{1}", orderNum.Value, "<br />");
                                hasErr = true;
                            }
                        }
                        else
                        {
                            sbError.AppendFormat("{0} cannot be posted to supplier - {1} not available.{2}", orderNum.Value, skunumber.Value, "<br />");
                            hasErr = true;
                        }
                    }
                }
                if (TransData.SprocketOrders.Rows.Count > 0)
                    ReportService.UpdateSprocketOrderSentStatus(TransData, ConnString);

                BindSprocketOrderData();
                if (hasErr == true)
                {
                    AlertScript(sbError.ToString().Replace("'", "`"));
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnExportTocsv_Click(object sender, EventArgs e)
        {
            CreateCSVFile();
        }

        private void CreateCSVFile()
        {
            try
            {
                TransactionsModel cData = BindSprocketOrderData();
                string strFileName = string.Format("{0}-SprocketOrders.csv", DateTime.Now.ToString("MM-dd-yyyy"));
                Response.Clear();
                Response.ContentType = "text/csv";
                Response.AppendHeader("content-disposition", "attachment; filename= " + strFileName);
                StringBuilder l_strCSV = CsvStrBuild(cData);
                Response.Write(l_strCSV);
                Response.Flush();
                Response.End();
            }
            catch (Exception ex)
            {
                AlertScript(ex.Message);
                FileLogger.WriteException(ex);
            }
        }

        private StringBuilder CsvStrBuild(TransactionsModel cData)
        {
            StringBuilder l_strCSV = new StringBuilder("");

            l_strCSV.AppendFormat("\"PO Detail Number\",\"SKU\",\"Product Description\",\"QTY\",\"Carrier Service\",\"Order#\",\"Sprocket Order#\",\"First Name\",\"Last Name\",\"Company\",\"Address1\",\"Address2\",\"City\",\"State\",\"Zip\",\"Phone\",\"Email\"{0}", Environment.NewLine);


            foreach (TransactionsModel.SprocketOrdersRow row in cData.SprocketOrders)
            {
                string supplierComments = row.Issupplier_commentsNull() ? "" : row.supplier_comments;
                if (supplierComments.IndexOf(" ") > -1)
                {
                    supplierComments = supplierComments.Substring(supplierComments.LastIndexOf(" ") + 1);
                }
                l_strCSV.AppendFormat("\"{0}\",", row.po_dtl_number);
                l_strCSV.AppendFormat("\"{0}\",", row.skunumber);
                l_strCSV.AppendFormat("\"{0}\",", row.description);
                l_strCSV.AppendFormat("\"{0}\",", row.quantity);
                l_strCSV.AppendFormat("\"{0}\",", row.ship_price_option);
                l_strCSV.AppendFormat("\"{0}\",", row.po_number.TrimStart('0'));
                l_strCSV.AppendFormat("\"{0}\",", ddlStatus.SelectedIndex > 1 ? supplierComments : "");
                l_strCSV.AppendFormat("\"{0}\",", row.ship_first_name);
                l_strCSV.AppendFormat("\"{0}\",", row.ship_last_name);
                l_strCSV.AppendFormat("\"{0}\",", row.ship_company_name);
                l_strCSV.AppendFormat("\"{0}\",", row.shipment_address1);
                l_strCSV.AppendFormat("\"{0}\",", row.shipment_address2);
                l_strCSV.AppendFormat("\"{0}\",", row.city);
                l_strCSV.AppendFormat("\"{0}\",", row.state_abbr);
                l_strCSV.AppendFormat("\"{0}\",", row.zipcode);
                l_strCSV.AppendFormat("\"{0}\",", row.ship_phone);
                l_strCSV.AppendFormat("\"{0}\"", row.email_id);
                l_strCSV.AppendFormat(Environment.NewLine);
            }
            return l_strCSV;
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/Dashboard.aspx");
        }

        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindSprocketOrderData();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnUpdateOrderStatus_Click(object sender, EventArgs e)
        {
            try
            {
                SprocketProcessor.ReadInventoryAndUpdate(ConnString);
                BindSprocketOrderData();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }
    }
}