﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Collections;
using System.Data;
using System.Text;
using System.Threading;
using System.Configuration;

namespace Bestprintbuy.Admin.Web.AdminReports
{
    public partial class EMCustomersMailer : PageBase
    {
        StringBuilder l_strCSV = new StringBuilder();
        public int pkey_email_campaign;
        private ReportsModel cData = new ReportsModel();
        private string strMailSubject = "";
        private string strMessageContent = "";
        private string strDiscount = "";
        private string strPromo = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    cData = (ReportsModel)LoadCustomerList();
                    BindGrid(cData);
                }
                catch (Exception ex)
                {
                    FileLogger.WriteException(ex);
                    AlertScript(ex.Message);
                }
            }
        }

        private ReportsModel LoadCustomerList()
        {
            pkey_email_campaign = Convert.ToInt32(Request.QueryString["ID"]);
            int fkey_ec_attribute = -1;
            string customerStatus = "ALL";
            if (Session["EmailSummaryFilter"] != null)
            {
                try
                {
                    Hashtable ht = (Hashtable)Session["EmailSummaryFilter"];
                    if (ht.ContainsKey("fkey_ec_attributes"))
                        fkey_ec_attribute = Convert.ToInt32(ht["fkey_ec_attributes"]);
                    if (ht.ContainsKey("CustomerStatus"))
                        customerStatus = Convert.ToString(ht["CustomerStatus"]);
                }
                catch (Exception ex)
                {
                    //ExceptionManager.Publish(ex);
                    throw ex;
                }
            }

            cData = ReportService.GetEmailCampaignDetails(pkey_email_campaign, fkey_ec_attribute, customerStatus, ConnString);
            SetTitle(pkey_email_campaign, cData.EmailCustomerInfo.Rows.Count);
            return cData;
        }

        private void SetTitle(int pkey_email_campaign, int count)
        {
            Hashtable htFilterData = (Hashtable)Session["EmailSummaryFilter"];
            switch (pkey_email_campaign)
            {
                case 1:
                case 6:
                case 11:
                    lblCategory.Text = string.Format("{0} new users registered but not ordered btn {1}-{2}",
                        count, htFilterData["FromDate"], htFilterData["ToDate"]);
                    break;
                case 2:
                case 7:
                case 12:
                    lblCategory.Text = string.Format("{0} users ordered BC only btn {1}-{2}",
                        count, htFilterData["FromDate"], htFilterData["ToDate"]);
                    break;
                case 3:
                case 8:
                case 13:
                    lblCategory.Text = string.Format("{0} users ordered multiple products btn {1}-{2}",
                        count, htFilterData["FromDate"], htFilterData["ToDate"]);
                    break;
                case 4:
                case 9:
                case 14:
                    lblCategory.Text = string.Format("{0} users ordered Postcards btn {1}-{2}",
                        count, htFilterData["FromDate"], htFilterData["ToDate"]);
                    break;
                case 5:
                case 10:
                case 15:
                    lblCategory.Text = string.Format("{0} users with items in the shopping cart btn {1}-{2}",
                        count, htFilterData["FromDate"], htFilterData["ToDate"]);
                    break;
                default:
                    lblCategory.Text = string.Format("{0} new users with just saved designs btn {1}-{2}",
                        count, htFilterData["FromDate"], htFilterData["ToDate"]);
                    break;

            }
        }

        public void BindGrid(ReportsModel cData)
        {
            rptrCustStats.DataSource = cData.EmailCustomerInfo;
            rptrCustStats.DataBind();
        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            CreateCSVFile();
        }

        private void CreateCSVFile()
        {
            try
            {
                cData = (ReportsModel)LoadCustomerList();
                string strFileName = string.Format("{0}-CustomersByCampaign.csv", DateTime.Now.ToString("MM-dd-yyyy"));
                Response.Clear();
                Response.ContentType = "text/csv";
                Response.AppendHeader("content-disposition", "attachment; filename= " + strFileName);
                CsvStrBuild();
                Response.Write(l_strCSV);
                Response.Flush();
                Response.End();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        private void CsvStrBuild()
        {
            // l_strCSV.AppendFormat("\"pkey_customer",\"customer_name\",\"email_id\",\"date_created\",\"fkey_ec_attribute\",\"email_type\"{0}", Environment.NewLine);

            l_strCSV.AppendFormat("\"S.No\",\"Customer Name\",\"Email Address\",\"Phone\",\"Date Registered\"{0}", Environment.NewLine);


            int iCounter = 1;
            foreach (ReportsModel.EmailCustomerInfoRow cartRow in cData.EmailCustomerInfo.Rows)
            {
                l_strCSV.AppendFormat("\"{0}\",", iCounter);
                l_strCSV.AppendFormat("\"{0}\",", cartRow.customer_name);
                l_strCSV.AppendFormat("\"{0}\",", cartRow.email_id);
                l_strCSV.AppendFormat("\"{0}\",", cartRow.IsphoneNull() ? "" : cartRow.phone);
                l_strCSV.AppendFormat("\"{0}\",", cartRow.date_created);
                l_strCSV.AppendFormat(Environment.NewLine);
                iCounter++;
            }

        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("EmailSummary.aspx");
        }

        protected void btnSendMail_Click(object sender, EventArgs e)
        {
            try
            {
                strMailSubject = "";
                strMessageContent = "";
                pkey_email_campaign = Convert.ToInt32(Request.QueryString["ID"]);
                SendCampaignEmails();
                cData = (ReportsModel)LoadCustomerList();
                BindGrid(cData);
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript("Unable to send campaign. Please try again.");
            }
        }

        private void SendCampaignEmails()
        {
            CampaignMailContent(pkey_email_campaign);
            if (strMessageContent == "")
            {
                AlertScript("Unable to load email content. Please try again.");
                return;
            }

            string fromEmail = ConfigurationManager.AppSettings["Rpt_CustomerSupportEmail"];     // WebConfiguration.BPBCustomerCareEmail;    
            string fromName = ConfigurationManager.AppSettings["Rpt_CustomerSupportName"];

            cData = ReportService.GetEmailCampaignDetails(pkey_email_campaign, ConnString);
            int fkeypromo = ReportService.GetEmailCampaignPromoKey(pkey_email_campaign, ConnString);

            if (fkeypromo > 0)
            {
                GetDiscount(fkeypromo);
            }
            else
            {
                AlertScript("Unable to load promotion details. Please try again.");
                return;
            }
            string mCustomerPortalURL = ConfigurationManager.AppSettings["Rpt_BPBCustomerBaseURL"];
            ArrayList alCustKeys = new ArrayList();

            ReportsModel emailData = new ReportsModel();

            foreach (RepeaterItem item in rptrCustStats.Items)
            {
                CheckBox chk = (CheckBox)item.FindControl("chkSendMail");
                HiddenField hdncustomerID = (HiddenField)item.FindControl("hdncustomerID");
                if (chk.Checked)
                {
                    int pkey_customer = Convert.ToInt32(hdncustomerID.Value);
                    DataRow[] drs = cData.EmailCustomerInfo.Select(
                        String.Format("pkey_customer='{0}' AND email_type = {1}", pkey_customer, pkey_email_campaign));
                    if (drs.Length > 0)
                    {
                        ReportsModel.EmailCustomerInfoRow eRow = (ReportsModel.EmailCustomerInfoRow)drs[0];

                        int fkeyecattribute = -1;
                        string strCustEmail = string.Empty;
                        string strMessage = "";
                        if (!eRow.Isfkey_ec_attributeNull()) fkeyecattribute = eRow.fkey_ec_attribute;
                        strCustEmail = eRow.email_id;

                        alCustKeys.Add(pkey_customer);

                        strMessage = strMessageContent;

                        string strImage = eRow.Isimage_urlNull() ? "" : eRow.image_url;
                        if (strImage != "")
                        {
                            if (strImage.IndexOf("/DesignCenter") != -1)
                            {
                                strImage = mCustomerPortalURL + strImage.Substring(strImage.IndexOf("/DesignCenter"));
                            }
                            strImage = String.Format("<img src='{0}' border=1 > ", strImage);
                        }


                        strMessage = strMessage.Replace("##USER_NAME##", eRow.customer_name);
                        strMessage = strMessage.Replace("##PO_NUMBER#", "");
                        strMessage = strMessage.Replace("##KIOSK##", Util.GetECHome(eRow.fkey_ec_attribute));
                        strMessage = strMessage.Replace("##DISCOUNT_PERCENT##", strDiscount);
                        strMessage = strMessage.Replace("##PROMO_CODE##", strPromo);
                        strMessage = strMessage.Replace("##GRAPHIC_LINK##", strImage);
                        strMessage = strMessage.Replace("##IMAGE##", strImage);


                        //messageList.Add(strMessage);
                        //if (ConfigurationManager.AppSettings["RedirectEmailCampaignToAdmin"] == "Y")
                        //    mailList.Add(Util.GetEmail(), strMessage);
                        //else
                        //    mailList.Add(strCustEmail);

                        if (ConfigurationManager.AppSettings["Rpt_RedirectEmailCampaignToAdmin"] == "Y")
                            strCustEmail = Util.GetEmail();

                        ReportsModel.EmailListRow emailRow = emailData.EmailList.NewEmailListRow();
                        emailRow.pkey_customer = pkey_customer;
                        emailRow.pkey_campaign = pkey_email_campaign;
                        emailRow.from_email = fromEmail;
                        emailRow.email_id = strCustEmail;
                        emailRow.subject = strMailSubject;
                        emailRow.message = strMessage;
                        emailRow.from_name = fromName;
                        emailData.EmailList.Rows.Add(emailRow);
                    }
                }
            }
            MailSender mailSender = new MailSender(emailData);
            Thread mailThread = new Thread(new ThreadStart(mailSender.SendEmailCampaign));
            mailThread.Start();
            if (ConfigurationManager.AppSettings["RedirectEmailCampaignToAdmin"] == "Y")
            {
                AlertScript("Emails queued successfully to test accounts. Status will not be updated to Y.");
            }
            else if (alCustKeys == null || alCustKeys.Count == 0)
            {
                AlertScript("Please select atleast one Email to SendMail");
            }
            else
            {
                ReportService.UpdateEmailCampaignSentStatus(alCustKeys, pkey_email_campaign, ConnString);
                AlertScript("Emails queued successfully.");
            }
        }

        private void CampaignMailContent(int pkeyEmailCampaign)
        {
            int mailindex = -1;
            switch (pkeyEmailCampaign)
            {
                case 1:
                    strMailSubject = "Order now and get fantastic discounts.";
                    mailindex = 2;
                    break;
                case 6:
                    strMailSubject = "Order now and get fantastic discounts.";
                    mailindex = 2;
                    break;
                case 11:
                    strMailSubject = "Order now and get fantastic discounts.";
                    mailindex = 2;
                    break;
                case 19:
                    strMailSubject = "Order now and get fantastic discounts.";
                    mailindex = 2;
                    break;
                case 2:
                    strMailSubject = "Great discounts from BestPrintBuy";
                    mailindex = 1;
                    break;
                case 7:
                    strMailSubject = "Great discounts from BestPrintBuy";
                    mailindex = 1;
                    break;
                case 12:
                    strMailSubject = "Great discounts from BestPrintBuy";
                    mailindex = 1;
                    break;
                case 20:
                    strMailSubject = "Great discounts from BestPrintBuy";
                    mailindex = 1;
                    break;
                case 3:
                    strMailSubject = "Great discounts from BestPrintBuy";
                    mailindex = 5;
                    break;
                case 8:
                    strMailSubject = "Great discounts from BestPrintBuy";
                    mailindex = 5;
                    break;
                case 13:
                    strMailSubject = "Great discounts from BestPrintBuy";
                    mailindex = 5;
                    break;
                case 21:
                    strMailSubject = "Great discounts from BestPrintBuy";
                    mailindex = 5;
                    break;
                case 4:
                    strMailSubject = "Great discounts from BestPrintBuy";
                    mailindex = 6;
                    break;
                case 9:
                    strMailSubject = "Great discounts from BestPrintBuy";
                    mailindex = 6;
                    break;
                case 14:
                    strMailSubject = "Great discounts from BestPrintBuy";
                    mailindex = 6;
                    break;
                case 22:
                    strMailSubject = "Great discounts from BestPrintBuy";
                    mailindex = 6;
                    break;
                case 5:
                    strMailSubject = "Great discounts from BestPrintBuy for orders on your cart.";
                    mailindex = 4;
                    break;
                case 10:
                    strMailSubject = "Great discounts from BestPrintBuy for orders on your cart.";
                    mailindex = 4;
                    break;
                case 15:
                    strMailSubject = "Great discounts from BestPrintBuy for orders on your cart.";
                    mailindex = 4;
                    break;
                case 23:
                    strMailSubject = "Great discounts from BestPrintBuy for orders on your cart.";
                    mailindex = 4;
                    break;
                case 16:
                    strMailSubject = "Order now and get fantastic discounts.";
                    mailindex = 7;
                    break;
                case 17:
                    strMailSubject = "Order now and get fantastic discounts.";
                    mailindex = 7;
                    break;
                case 18:
                    strMailSubject = "Order now and get fantastic discounts.";
                    mailindex = 7;
                    break;
                case 24:
                    strMailSubject = "Order now and get fantastic discounts.";
                    mailindex = 7;
                    break;
            }
            strMessageContent = Util.ReadMailContent(mailindex);
        }

        public void GetDiscount(int fkeypromo)
        {
            ReportsModel pData = ReportService.GetProductPromotions(ConnString);
            foreach (ReportsModel.ProductPromosRow pRow in pData.ProductPromos)
            {
                if (pRow.pkey_promotion == fkeypromo)
                {
                    if (pRow.rule_type.Trim() == "B" || pRow.rule_type.Trim() == "D")
                    {
                        strPromo = pRow.promo_code;
                        if (pRow.discount_type)
                            strDiscount = string.Format("${0}", pRow.discount);
                        else
                        {
                            strDiscount = string.Format("{0}%", pRow.discount);
                            strDiscount = strDiscount.Replace(".00%", "%");
                        }
                    }
                    break;
                }
            }
        }
    }
}