﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Text;
using System.IO;
using System.Threading;

namespace Bestprintbuy.Admin.Web.AdminReports
{
    public partial class MCAReport : PageBase
    {
        protected int recordCount = 0;
        protected ReportsModel cData = null;
        StringBuilder l_strCSV = new StringBuilder();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                try
                {
                    txtFromDate.Text = DateTime.Now.AddDays(-30).ToString("MM/dd/yyyy");
                    txtToDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
                    BindDropDowns();
                    BindMCAData();
                }
                catch (Exception ex)
                {
                    FileLogger.WriteException(ex);
                    AlertScript(ex.Message);
                }
            }
        }

        private void BindDropDowns()
        {
            ReportsModel filterData = ReportService.GetKiosksPromotions(ConnString);

            ddlKiosk.DataSource = filterData.Kiosks;
            ddlKiosk.DataValueField = "fkey_ec_attribute";
            ddlKiosk.DataTextField = "kiosk";
            ddlKiosk.DataBind();

            ddlKiosk.Items.Insert(0, new ListItem("ALL", "-1"));

            ddlPromos.DataSource = filterData.Promotions;
            ddlPromos.DataValueField = "pkey_promotion";
            ddlPromos.DataTextField = "promo_code";
            ddlPromos.DataBind();

            ddlPromos.Items.Insert(0, new ListItem("None", "-1"));

            ddlPromos.SelectedIndex = 0;
            ddlKiosk.SelectedIndex = 0;
        }

        private void BindMCAData()
        {
            cData = ReportService.GetMCAData(
                Convert.ToInt32(ddlKiosk.SelectedValue),
                Convert.ToDateTime(txtFromDate.Text),
                Convert.ToDateTime(txtToDate.Text),
                Convert.ToInt32(ddlPromos.SelectedValue),
                Convert.ToBoolean(chkIsEmailSent.Checked),
                ConnString);

            recordCount = cData.MCADM.Rows.Count;
            rptrMCAReports.DataSource = cData.MCADM;
            rptrMCAReports.DataBind();
            if (cData.MCADM.Rows.Count > 0)
            {
                pnlContent.Visible = true;
                pnlClose.Visible = false;
            }
            else
            {
                pnlContent.Visible = false;
                pnlClose.Visible = true;
            }
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            try
            {
                BindMCAData();
                btnReset.Visible = true;
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                txtToDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
                txtFromDate.Text = DateTime.Now.AddDays(-30).ToString("MM/dd/yyyy");
                ddlPromos.SelectedIndex = 0;
                ddlKiosk.SelectedIndex = 0;
                BindMCAData();
                btnReset.Visible = false;
                pnlContent.Visible = false;
                pnlClose.Visible = true;
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }            
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ReportsModel model = new ReportsModel();
                foreach (RepeaterItem item in rptrMCAReports.Items)
                {
                    ReportsModel.MCADMRow row = model.MCADM.NewMCADMRow();
                    CheckBox chkSelect = (CheckBox)item.FindControl("chkEmail");
                    HiddenField hdnSku = (HiddenField)item.FindControl("hdnSku");
                    HiddenField hdnPkey_customer = (HiddenField)item.FindControl("hdnPkey_customer");
                    if (chkSelect.Checked)
                    {
                        row.pkey_customer = Convert.ToInt32(hdnPkey_customer.Value);
                        row.SKU_Number = hdnSku.Value;
                        model.MCADM.Rows.Add(row);
                    }
                }
                ReportService.UpdateMCADMAMailSentStatus(model, ConnString);
                AlertScript("Successfully Saved");
                BindMCAData();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnEmailtoSupplier_Click(object sender, EventArgs e)
        {
            try
            {
                string attachement = SaveFiles();
                if (File.Exists(attachement))
                {
                    LoggedUser lUser = (LoggedUser)Session["LoggedUser"];
                    ReportsModel sData = ReportService.GetData("MCA", ConnString);

                    if (sData.ReportSettings.Rows.Count <= 0)
                    {
                        //ltrlScript.Text = string.Format("<script language='javascript' type='text/javascript'>alert('{0}');</script>", "Unable to send email.");
                        AlertScript("Unable to send email.");
                        return;
                    }
                    ReportsModel.ReportSettingsRow rRow = (ReportsModel.ReportSettingsRow)sData.ReportSettings.Rows[0];
                    MailSender mailSender = new MailSender(lUser.EmailID,
                        rRow.supplier_email,
                        string.Format("Please find the attached MCA DM List<br><br>{0}", lUser.Name),
                        "BestPrintBuy.com auto mailer - MCA DM List.",
                        "", "HTML", true, attachement);
                    Thread mailThread = new Thread(new ThreadStart(mailSender.SendEmail));
                    mailThread.Start();

                    ReportService.UpdateMCADMAMailSentStatus(cData, ConnString);
                    BindMCAData();
                    //ltrlScript.Text = string.Format("<script language='javascript' type='text/javascript'>alert('{0}');</script>", "Email has been successfully sent");
                    AlertScript("Email has been successfully sent");
                }
                else
                {
                    //ltrlScript.Text = string.Format("<script language='javascript' type='text/javascript'>alert('{0}');</script>", "Unable to send email.");
                    AlertScript("Unable to send email.");
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }            
        }

        protected void btnExportTocsv_Click(object sender, EventArgs e)
        {
            CreateCSVFile();
        }

        private void CreateCSVFile()
        {
            try
            {
                BindMCAData();
                string strFileName = string.Format("{0}-MCADMList.csv", DateTime.Now.ToString("MM-dd-yyyy"));
                Response.Clear();
                Response.ContentType = "text/csv";
                Response.AppendHeader("content-disposition", "attachment; filename= " + strFileName);
                CsvStrBuild();
                Response.Write(l_strCSV);
                Response.Flush();
                Response.End();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        private string SaveFiles()
        {
            BindMCAData();
            CsvStrBuild();
            string l_strFileLocation = "";
            string l_strCSVFilename;
            string l_strServerPath = Server.MapPath(".");
            l_strFileLocation = string.Format("{0}/EmailAttachments", l_strServerPath);
            Util.CheckDirExistance(l_strFileLocation);
            l_strCSVFilename = l_strFileLocation + "/" + string.Format("{0}-MCADMList.csv", DateTime.Now.ToString("MM-dd-yyyy"));
            Util.SaveFile(l_strCSVFilename, l_strCSV.ToString());
            return l_strCSVFilename;
        }

        private void CsvStrBuild()
        {
            l_strCSV.AppendFormat("\"Order No\",\"First Name\",\"Last Name\",\"Company Name\",\"Address 1\",\"Address 2\",\"City\",\"State\",\"Zip\",\"Country\",\"Phone\",\"Shipping Type\",\"SKU  Number\",\"Quantity\"{0}", Environment.NewLine);


            foreach (ReportsModel.MCADMRow mcaRow in cData.MCADM)
            {
                l_strCSV.AppendFormat("\"{0}\",", mcaRow.Order_No);
                l_strCSV.AppendFormat("\"{0}\",", mcaRow.First_Name);
                l_strCSV.AppendFormat("\"{0}\",", mcaRow.Last_Name);
                l_strCSV.AppendFormat("\"{0}\",", mcaRow.Company_Name);
                l_strCSV.AppendFormat("\"{0}\",", mcaRow.Address_1);
                l_strCSV.AppendFormat("\"{0}\",", mcaRow.IsAddress_2Null() ? "" : mcaRow.Address_2);
                l_strCSV.AppendFormat("\"{0}\",", mcaRow.City);
                l_strCSV.AppendFormat("\"{0}\",", mcaRow.State);
                l_strCSV.AppendFormat("\"{0}\",", mcaRow.Zip);
                l_strCSV.AppendFormat("\"{0}\",", mcaRow.Country);
                l_strCSV.AppendFormat("\"{0}\",", mcaRow.Phone);
                l_strCSV.AppendFormat("\"{0}\",", mcaRow.Shipping_Type);
                l_strCSV.AppendFormat("\"{0}\",", mcaRow.SKU_Number);
                l_strCSV.AppendFormat("\"{0}\"", mcaRow.Quantity);
                l_strCSV.AppendFormat(Environment.NewLine);
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/Dashboard.aspx");
        }
    }
}