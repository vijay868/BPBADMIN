﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.AdminReports
{
    public partial class CatalogAndSamplesRequestForm : PageBase
    {
        private CatalogRequestModel cData = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                try
                {
                    BindStates();
                    BindCountries();
                    BindCatalogData();
                }
                catch (Exception ex)
                {
                    FileLogger.WriteException(ex);
                    AlertScript(ex.Message);
                }
            }
        }

        private void BindCatalogData()
        {
            int pkeyCatalogReq = Convert.ToInt32(Request.QueryString["pkeyCatalogReq"]);

            cData = ReportService.GetCatalogDataRow(pkeyCatalogReq, ConnString);

            CatalogRequestModel.CatalogRequestRow CatalogRequest = (CatalogRequestModel.CatalogRequestRow)cData.CatalogRequest.Rows[0];
            txtFirstname.Text = CatalogRequest.Fname;
            txtLastname.Text = CatalogRequest.Lname;
            if (!CatalogRequest.IsCompanyNull())
                txtCompany.Text = CatalogRequest.Company;
            txtEmail.Text = CatalogRequest.fromemail;
            txtStreet.Text = CatalogRequest.Street;
            txtCity.Text = CatalogRequest.City;
            ddlStates.SelectedValue = CatalogRequest.fkey_state;
            ddlCountry.SelectedValue = CatalogRequest.fkey_country;
            txtZipcode.Text = CatalogRequest.Zip;
            txtNoOfAgents.Text = CatalogRequest.Agentno.ToString();
            txtPhone.Text = CatalogRequest.Phone;
            txtContactPerson.Text = CatalogRequest.Contactperson;
        }

        private void BindStates()
        {
            CatalogRequestModel stateModel = ReportService.GetStates(2, ConnString);
            ddlStates.DataSource = stateModel.States;
            ddlStates.DataTextField = "state_abbr";
            ddlStates.DataValueField = "pkey_state";
            ddlStates.DataBind();
        }

        private void BindCountries()
        {
            CatalogRequestModel countryModel = ReportService.GetCountries(ConnString);
            ddlCountry.DataSource = countryModel.Countries;
            ddlCountry.DataTextField = "country_abbr";
            ddlCountry.DataValueField = "pkey_country";
            ddlCountry.DataBind();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int pkeyCatalogReq = Convert.ToInt32(Request.QueryString["pkeyCatalogReq"]);
                CatalogRequestModel cData = new CatalogRequestModel();
                CatalogRequestModel.CatalogRequestRow drCatalog = cData.CatalogRequest.NewCatalogRequestRow();
                drCatalog.Pkey_CatalogReq = pkeyCatalogReq.ToString();
                drCatalog.Company = txtCompany.Text;
                drCatalog.Street = txtStreet.Text;
                drCatalog.City = txtCity.Text;
                drCatalog.fkey_state = ddlStates.SelectedValue;
                drCatalog.fkey_country = ddlCountry.SelectedValue;
                drCatalog.Zip = txtZipcode.Text;
                cData.CatalogRequest.Rows.Add(drCatalog);
                ReportService.UpdateCatalog(cData, ConnString);
                //Response.Redirect("CatalogReport.aspx");
                AlertScript("Successfully Saved.");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }
    }
}