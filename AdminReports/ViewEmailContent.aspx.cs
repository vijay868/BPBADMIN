﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;

namespace Bestprintbuy.Admin.Web.AdminReports
{
    public partial class ViewEmailContent : PageBase
    {
        protected string emailid = "";
        protected int pkey_email_campaign = -1;
        ReportsModel m_cusModel = new ReportsModel();
        protected string strEmailContent = "";
        protected string strDiscount;
        protected string strPromo;
        protected string mCustomerPortalURL = "";
        protected string strMailSubject = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            mCustomerPortalURL = customerPotralUrl;
            if (!Page.IsPostBack)
            {
                try
                {
                    pkey_email_campaign = Convert.ToInt32(Request.QueryString["pkey_email_campaign"]);
                    emailid = Request.QueryString["email_id"];
                    m_cusModel = ReportService.GetEmailCampaignDetails(pkey_email_campaign, emailid, ConnString);
                    if (m_cusModel.EmailCustomerInfo.Rows.Count > 0)
                    {
                        strEmailContent = CampaignMailContent(pkey_email_campaign, (ReportsModel.EmailCustomerInfoRow)(m_cusModel.EmailCustomerInfo.Rows[0]));
                    }
                }
                catch (Exception ex)
                {
                    FileLogger.WriteException(ex);
                    AlertScript(ex.Message);
                }                
            }
        }

        public string CampaignMailContent(int pkeyEmailCampaign, ReportsModel.EmailCustomerInfoRow eRow)
        {
            int mailindex = -1;
            switch (pkeyEmailCampaign)
            {
                case 1:
                    strMailSubject = "Order now and get fantastic discounts."; ;
                    mailindex = 2;
                    break;
                case 6:
                    strMailSubject = "Order now and get fantastic discounts.";
                    mailindex = 2;
                    break;
                case 11:
                    strMailSubject = "Order now and get fantastic discounts.";
                    mailindex = 2;
                    break;
                case 19:
                    strMailSubject = "Order now and get fantastic discounts.";
                    mailindex = 2;
                    break;
                case 2:
                    strMailSubject = "Great discounts from BestPrintBuy";
                    mailindex = 1;
                    break;
                case 7:
                    strMailSubject = "Great discounts from BestPrintBuy";
                    mailindex = 1;
                    break;
                case 12:
                    strMailSubject = "Great discounts from BestPrintBuy";
                    mailindex = 1;
                    break;
                case 20:
                    strMailSubject = "Great discounts from BestPrintBuy";
                    mailindex = 1;
                    break;
                case 3:
                    strMailSubject = "Great discounts from BestPrintBuy";
                    mailindex = 5;
                    break;
                case 8:
                    strMailSubject = "Great discounts from BestPrintBuy";
                    mailindex = 5;
                    break;
                case 13:
                    strMailSubject = "Great discounts from BestPrintBuy";
                    mailindex = 5;
                    break;
                case 21:
                    strMailSubject = "Great discounts from BestPrintBuy";
                    mailindex = 5;
                    break;
                case 4:
                    strMailSubject = "Great discounts from BestPrintBuy";
                    mailindex = 6;
                    break;
                case 9:
                    strMailSubject = "Great discounts from BestPrintBuy";
                    mailindex = 6;
                    break;
                case 14:
                    strMailSubject = "Great discounts from BestPrintBuy";
                    mailindex = 6;
                    break;
                case 22:
                    strMailSubject = "Great discounts from BestPrintBuy";
                    mailindex = 6;
                    break;
                case 5:
                    strMailSubject = "Great discounts from BestPrintBuy for orders on your cart.";
                    mailindex = 4;
                    break;
                case 10:
                    strMailSubject = "Great discounts from BestPrintBuy for orders on your cart.";
                    mailindex = 4;
                    break;
                case 15:
                    strMailSubject = "Great discounts from BestPrintBuy for orders on your cart.";
                    mailindex = 4;
                    break;
                case 23:
                    strMailSubject = "Great discounts from BestPrintBuy for orders on your cart.";
                    mailindex = 4;
                    break;
                case 16:
                    strMailSubject = "Order now and get fantastic discounts.";
                    mailindex = 7;
                    break;
                case 17:
                    strMailSubject = "Order now and get fantastic discounts.";
                    mailindex = 7;
                    break;
                case 18:
                    strMailSubject = "Order now and get fantastic discounts.";
                    mailindex = 7;
                    break;
                case 24:
                    strMailSubject = "Order now and get fantastic discounts.";
                    mailindex = 7;
                    break;
            }

            string strMessage = String.Empty;
            strDiscount = String.Empty;
            strPromo = String.Empty;

            GetDiscount(eRow.fkey_promo);

            strMessage = Util.ReadMailContent(mailindex);
            string strImage = eRow.Isimage_urlNull() ? "" : eRow.image_url;
            if (strImage != "")
            {
                if (strImage.IndexOf("/DesignCenter") != -1)
                {
                    strImage = mCustomerPortalURL + strImage.Substring(strImage.IndexOf("/DesignCenter"));
                }
                //else if (strImage.IndexOf("/INVENTORYITEMS") != -1)
                //{
                //    strImage = mCustomerPortalURL + strImage.Substring(strImage.IndexOf("/DesignCenter"));
                //}
                strImage = String.Format("<img src='{0}' border=1 > ", strImage);
            }


            strMessage = strMessage.Replace("##USER_NAME##", eRow.customer_name);
            strMessage = strMessage.Replace("##PO_NUMBER#", "");
            strMessage = strMessage.Replace("##KIOSK##", Util.GetECHome(eRow.fkey_ec_attribute));
            strMessage = strMessage.Replace("##DISCOUNT_PERCENT##", strDiscount);
            strMessage = strMessage.Replace("##PROMO_CODE##", strPromo);
            strMessage = strMessage.Replace("##GRAPHIC_LINK##", strImage);
            strMessage = strMessage.Replace("##IMAGE##", strImage);

            return strMessage;
        }

        public void GetDiscount(int fkeypromo)
        {
            ReportsModel pData = ReportService.GetProductPromotions(ConnString);
            foreach (ReportsModel.ProductPromosRow pRow in pData.ProductPromos)
            {
                if (pRow.pkey_promotion == fkeypromo)
                {
                    if (pRow.rule_type.Trim() == "B" || pRow.rule_type.Trim() == "D")
                    {
                        strPromo = pRow.promo_code;
                        if (pRow.discount_type)
                            strDiscount = string.Format("${0}", pRow.discount);
                        else
                        {
                            strDiscount = string.Format("{0}%", pRow.discount);
                            strDiscount = strDiscount.Replace(".00%", "%");
                        }
                    }
                    break;
                }
            }
        }
    }
}