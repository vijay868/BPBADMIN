﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;

namespace Bestprintbuy.Admin.Web.AdminReports
{
    public partial class ReportSettings : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                try
                {
                    SprocketProcessor.ReadInventoryAndUpdate(ConnString);
                    string strCatAbbr = Request.QueryString["cat"];
                    BindData(strCatAbbr);
                }
                catch (Exception ex)
                {
                    FileLogger.WriteException(ex);
                    AlertScript(ex.Message);
                }
            }
        }

        private void BindData(string strCatAbbr)
        {
            ReportsModel sData = ReportService.GetData(strCatAbbr, ConnString);

            if (sData.ReportSettings.Rows.Count <= 0)
            {
                //Response.Redirect("../AdminReports/ReportsDashboard.aspx", true);
                return;
            }
            ReportsModel.ReportSettingsRow rRow = (ReportsModel.ReportSettingsRow)sData.ReportSettings.Rows[0];

            ViewState["pkey_report_settings"] = rRow.pkey_report_settings;
            ViewState["report_abbr"] = rRow.report_abbr;
            txtRptDesc.Text = rRow.report_description;
            txtShipType.Text = rRow.shipping_type;
            txtQty.Text = Convert.ToString(rRow.shipping_quantity);
            txtSupplierEmail.Text = rRow.supplier_email;
            ddlCarrier.SelectedValue = rRow.shipping_carrier;

            rptrSKU.DataSource = sData.ReportSettingsSKU;
            rptrSKU.DataBind();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ReportsModel sData = new ReportsModel();
                ReportsModel.ReportSettingsRow rRow = sData.ReportSettings.NewReportSettingsRow();
                rRow.pkey_report_settings = Convert.ToInt32(ViewState["pkey_report_settings"]);
                rRow.report_abbr = Convert.ToString(ViewState["report_abbr"]);
                rRow.report_description = txtRptDesc.Text;
                rRow.shipping_type = txtShipType.Text;
                rRow.shipping_quantity = Convert.ToInt32(txtQty.Text);
                rRow.supplier_email = txtSupplierEmail.Text;
                rRow.shipping_carrier = ddlCarrier.SelectedValue;

                sData.ReportSettings.Rows.Add(rRow);

                foreach (RepeaterItem item in rptrSKU.Items)
                {
                    TextBox txtSku = (TextBox)item.FindControl("txtSkuNumber");
                    HiddenField hdnPkeyReportSettings = (HiddenField)item.FindControl("hdnPkeyReportSettings");
                    ReportsModel.ReportSettingsSKURow sRow = sData.ReportSettingsSKU.NewReportSettingsSKURow();
                    sRow.sku_number = txtSku.Text;
                    sRow.pkey_report_settings_kiosk_sku = Convert.ToInt32(hdnPkeyReportSettings.Value);
                    sData.ReportSettingsSKU.Rows.Add(sRow);
                }

                ReportService.UpdateData(sData, ConnString);
                AlertScript("Report Settings updated successfully.");
            }
            catch(Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript("Unable to save Data. Please try again.");
            }
        }
    }
}