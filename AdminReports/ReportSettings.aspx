﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportSettings.aspx.cs" Inherits="Bestprintbuy.Admin.Web.AdminReports.ReportSettings" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Best Print Buy</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" media="all" href="../css/bestprintbuy_style.css" />
    <link rel="stylesheet" media="all" href="../css/jquery.fancybox.css" />
    <link rel="stylesheet" href="../css/jquery-ui.css" />
    <script src="../Scripts/jquery.js"></script>
    <script src="../Scripts/jquery-ui.js"></script>
    <script src="../Scripts/common.js"></script>
    <script src="../Scripts/ddsmoothmenu.js"></script>
    <script src="../Scripts/plugin/fancybox/jquery.fancybox.js"></script>
    <script src="../Scripts/jquery.validate.js"></script>
    <!--[if lt IE 9]>
			<script src="../Scripts/html5.js"></script>
		<![endif]-->
    <!--[if lte IE 7]> <html class="ie7"> <![endif]-->
    <!--[if IE 8]>     <html class="ie8"> <![endif]-->
    <!--[if IE 9]>     <html class="ie9"> <![endif]-->
    <!--[if IE 10]>     <html class="ie10"> <![endif]-->
    <!--[if !IE]><!-->

    <!--<![endif]-->
</head>
<body id="Body" runat="server">
    <!-- Wrapper starts -->
    <div id="Div1" class="popup-wrapper" runat="server">
        <div class="popup-title">
            Report Settings
        </div>
        <!-- Header Ends -->
        <div class="popup-contnt">
            <!-- Menu starts -->
            <form id="Form1" runat="server">
                <table>
                    <tr>
                        <td width="20%">
                            <label>Report Description</label>
                        </td>
                        <td></td>
                        <td>
                            <asp:TextBox name="txtRptDesc" ID="txtRptDesc" runat="server" CssClass="text"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Shipping Carrier</label>
                        </td>
                        <td></td>
                        <td>
                            <asp:DropDownList name="ddlCarrier" ID="ddlCarrier" CssClass="ddlist" runat="server">
                                <asp:ListItem Text="USPS" Value="USPS"></asp:ListItem>
                                <asp:ListItem Text="UPS" Value="UPS"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Shipping Type</label>
                        </td>
                        <td></td>
                        <td>
                            <asp:TextBox name="txtShipType" ID="txtShipType" runat="server" CssClass="text"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Shipping Quantity</label>
                        </td>
                        <td></td>
                        <td>
                            <asp:TextBox name="txtQty" ID="txtQty" runat="server" CssClass="text"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Supplier Email</label>
                        </td>
                        <td></td>
                        <td>
                            <asp:TextBox name="txtSupplierEmail" ID="txtSupplierEmail" runat="server" CssClass="text"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <div class="clr"></div>
                            <table class="tablep">
                                <thead>
                                    <tr>
                                        <th width="2%"></th>
                                        <th>Kiosk</th>
                                        <th>SKU No</th>
                                        <th>Description</th>
                                        <th>Avail Qty</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rptrSKU" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <%#Eval("attribute_value") %>
                                                    <asp:HiddenField ID="hdnPkeyReportSettings" Value='<%#Eval("pkey_report_settings_kiosk_sku") %>' runat="server" />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtSkuNumber" CssClass="text" Width="150px" runat="server" Text='<%#Eval("sku_number") %>'></asp:TextBox>
                                                </td>
                                                <td>
                                                    <%#Eval("description") %>
                                                </td>
                                                <td>
                                                    <%#Eval("units") %>
                                                </td>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="3">
                            <asp:Button runat="server" ID="btnSave" CssClass="button rounded" Text="Save" OnClick="btnSave_Click" OnClientClick="return validateform();"></asp:Button>
                            <asp:Button runat="server" ID="Button1" CssClass="button rounded" Text="Close" OnClientClick="parent.$.fancybox.close(); return false;"></asp:Button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</body>
</html>
