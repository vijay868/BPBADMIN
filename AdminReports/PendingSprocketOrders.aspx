﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="PendingSprocketOrders.aspx.cs" Inherits="Bestprintbuy.Admin.Web.AdminReports.PendingSprocketOrders" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
            function OpenSprocketInventoryDialog(pkey_credit) {
            var url = "SprocketStock.aspx";
            $.fancybox({
                'width': '80%',
                'height': '80%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': url,
                'type': 'iframe'
            });
            return false;
        }
    </script>
    <div class="clr"></div>
    <!-- Container Starts -->
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Administrative Reports</li>
            <li>Pre-Press Sprocket Orders</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Pre-Press Sprocket Orders</h2>
        <form id="frmBpbReports" name="frmBpbReports" runat="server">
            <table cellpadding="0" cellspacing="0" border="0" width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="10%">Order Status</td>
                        <td width="15%">
                            <asp:DropDownList name="ddlStatus" ID="ddlStatus" runat="server" CssClass="slect-box" Width="223px" AutoPostBack="true" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged">
                                <asp:ListItem Value="1" Selected="True">Pre-Press Queue</asp:ListItem>
                                <asp:ListItem Value="2">Print Queue</asp:ListItem>
                                <asp:ListItem Value="3">Supplier Queue</asp:ListItem>
                                <asp:ListItem Value="5">Processed Queue</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td width="75%"></td>
                    </tr>
                </tbody>
            </table>           
            <div class="clr"></div>
            <asp:Panel ID="pnlContent" runat="server" Visible="false">
                <table class="table" width="100%">
                    <thead>
                        <tr>
                            <th id="thchkEmail" runat="server" width="6%"></th>
                            <th>Order No</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Address</th>
                            <th>Phone</th>
                            <th>SKU</th>
                            <th>Qty</th>
                            <th>Avail Qty</th>
                            <th>Shipping Type</th>
                            <th id="thtracking" runat="server">Tracking#</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rptrOrdersList" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td id="tdchkEmail" runat="server">
                                        <asp:CheckBox ID="chkEmail" runat="server" />
                                    </td>
                                    <td>
                                        <%#Eval("po_dtl_number") %>
                                        <asp:HiddenField ID="hdnSku" runat="server" Value='<%# Eval("skunumber")%>' />
                                        <asp:HiddenField ID="hdnPkeyCustPoDtl" runat="server" Value='<%# Eval("pkey_cust_po_dtl")%>' />
                                        <asp:HiddenField ID="hdnPoDtlNumber" runat="server" Value='<%# Eval("po_dtl_number")%>' />
                                    </td>
                                    <td>
                                        <%#Eval("ship_first_name") %>
                                    </td>
                                    <td>
                                        <%#Eval("ship_last_name") %>
                                    </td>
                                    <td>
                                        <%#Eval("shipment_address1") %>,<%#Eval("shipment_address2") %><br />
                                        <%#Eval("city") %>,  <%#Eval("state_abbr") %>  <%#Eval("zipcode") %>
                                    </td>
                                    <td>
                                        <%#Eval("ship_phone") %>
                                    </td>
                                    <td>
                                        <%#Eval("skunumber") %>
                                    </td>
                                    <td>
                                        <%#Eval("quantity") %>
                                    </td>
                                    <td>
                                        <%#Eval("units") %>
                                    </td>
                                    <td>
                                        <%#Eval("ship_price_option") %>
                                    </td>
                                    <td id="tdtracking" runat="server">
                                        <%#Eval("tracking_number") %>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
                 <div class="ttl"><b>Sprocket Order(s): </b><%=recordCount%></div>
                <div class="form">
                    <div class="btn_lst">
                     <div class="lft">
                        <asp:Button ID="btnViewInventory" Text="View Sprocket Inventory" CssClass="button rounded" runat="server" OnClientClick="OpenSprocketInventoryDialog(); return false;"></asp:Button>
                    </div>
                        <div>
                            <asp:Button ID="btnClose" Text="Close" CssClass="button rounded" runat="server" OnClick="btnClose_Click"></asp:Button>
                            <asp:Button runat="server" ID="btnPostOrdersToSupplier" Text="Post Orders To Supplier" CssClass="button rounded" OnClick="btnPostOrdersToSupplier_Click"></asp:Button>
                            <asp:Button runat="server" ID="btnUpdateOrderStatus" CssClass="button rounded" Text="Update Order Status" OnClick="btnUpdateOrderStatus_Click" Visible="false"></asp:Button>
                            <asp:Button runat="server" ID="btnExportTocsv" CssClass="button rounded" Text="Export To CSV" OnClick="btnExportTocsv_Click"></asp:Button>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlClose" runat="server" CssClass="form">
                 <div class="ttl"><b>Sprocket Order(s): </b><%=recordCount%></div>
                <div class="btn_lst">
                     <div class="lft">
                        <asp:Button ID="btnViewInventory1" Text="View Sprocket Inventory" CssClass="button rounded" runat="server" OnClientClick="OpenSprocketInventoryDialog(); return false;"></asp:Button>
                    </div>
                    <div>
                        <asp:Button ID="Button2" Text="Close" CssClass="button rounded" runat="server" OnClick="btnClose_Click"></asp:Button>
                    </div>
                </div>
            </asp:Panel>
        </form>
    </div>
</asp:Content>
