﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="EMCustomersMailer.aspx.cs" Inherits="Bestprintbuy.Admin.Web.AdminReports.EMCustomersMailer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function EmailPreview(Username) {
            var pkeyemailcampaign = "<%= pkey_email_campaign%>"
            url = "ViewEmailContent.aspx?pkey_email_campaign=" + pkeyemailcampaign + "&email_id=" + Username;
            $.fancybox({
                'width': '70%',
                'height': '80%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': url,
                'type': 'iframe'
            });
            return false;
        }
        function SetGlobalSelect(chk) {
            $('.mail input:checkbox').each(function () {
                this.checked = chk.checked;
            });
        }
    </script>
    <div class="clr"></div>
    <!-- Container Starts -->
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Marketing Reports</li>
            <li>Email Marketing Program</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2><asp:Label runat="server" ID="lblCategory"></asp:Label></h2>
        <form id="frmBpbReports" name="frmBpbReports" runat="server">
            <table class="table" width="100%">
                <thead>
                    <tr>
                        <th width="3%"></th>
                        <th>Customer Name</th>
                        <th>Email Addres</th>
                        <th>Phone</th>
                        <th>Date Registered</th>
                        <th>Mail Sent</th>
                        <th>Send Mail
                                        <br />
                            <asp:CheckBox ID="chkGlobalSendMail" runat="server" onclick="SetGlobalSelect(this)" /></th>
                        <th>Preview</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="rptrCustStats" runat="server">
                        <ItemTemplate>
                            <tr>
                                <td></td>
                                <td>
                                    <%#Eval("customer_name") %>
                                </td>
                                <td>
                                    <asp:Label ID="lblEmailID" runat="server" Text='<%#Eval("email_id") %>'></asp:Label>
                                </td>
                                <td>
                                    <%#Eval("phone") %>
                                </td>
                                <td>
                                    <%# Eval("date_created","{0:MM/dd/yyyy}")%>
                                </td>
                                <td>
                                    <%# Convert.ToBoolean(Eval("isMailSent")) ? "Y" : "N" %>
                                </td>
                                <td style="text-align: left;">
                                    <asp:CheckBox ID="chkSendMail" runat="server" CssClass="mail" />
                                    <asp:HiddenField ID="hdncustomerID" runat="server" Value='<%# Eval("pkey_customer") %>' />
                                </td>
                                <td>
                                    <asp:Button runat="server" ID="btnPreview" Text="Preview" CssClass="button rounded" UseSubmitBehavior="false" OnClientClick='<%# "return EmailPreview(\"" +Convert.ToString(Eval("email_id")) + "\")" %>'  BackColor="#a8a8a8"></asp:Button>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
            <div class="btn_lst">
                <div class="rgt">
                    <asp:Button ID="btnClose" Text="Close" CssClass="button rounded" runat="server" OnClick="btnClose_Click"></asp:Button>
                    <asp:Button runat="server" ID="btnSendMail" Text="Send Mails" CssClass="button rounded" OnClick="btnSendMail_Click"></asp:Button>
                    <asp:Button runat="server" ID="btnExportTocsv" Text="Export to CSV" CssClass="button rounded" OnClick="btnExportToExcel_Click" /> 
                </div>
            </div>
        </form>
    </div>
</asp:Content>
