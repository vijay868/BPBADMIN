﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Text;
using System.IO;
using System.Threading;
using System.Data;
using System.Collections;

namespace Bestprintbuy.Admin.Web.AdminReports
{
    public partial class ExcludeEmailFromReports : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {                    
                    LoadData();
                }
                catch (Exception ex)
                {
                    FileLogger.WriteException(ex);
                    AlertScript(ex.Message);
                }
            }
        }

        protected void LoadData()
        {
            ReportsModel model = ReportService.GetExcludeEmail(ConnString);
            rptrExcludeEmails.DataSource = model.ExcludeEmail;
            rptrExcludeEmails.DataBind();
        }

        protected void btnExclude_Click(object sender, EventArgs e)
        {
            try
            {
                int ReCount;
                string emailID = txtEmailID.Text;
                if (emailID != "")
                {
                    ReCount = (int)ReportService.InsExCusFrmRpts(emailID, ConnString);

                    if (ReCount <= 0)
                    {
                        AlertScript("The Email Id does not exist in Customers List");
                    }
                    txtEmailID.Text = "";
                    LoadData();
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }            
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbtn = (LinkButton)sender;
                long customerID = Convert.ToInt64(lbtn.CommandArgument);
                ReportService.GetDeleteEmail(customerID, ConnString);
                LoadData();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }            
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/Dashboard.aspx");
        }

    }
}