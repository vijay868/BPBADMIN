﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Text;
using System.IO;
using System.Threading;
using System.Collections;

namespace Bestprintbuy.Admin.Web.AdminReports
{
    public partial class EmailSummary : PageBase
    {
        Hashtable htFilterData = new Hashtable();
        string filterType = "C";
        ReportsModel promoData = new ReportsModel();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["EmailSummaryFilter"] != null)
                {
                    htFilterData = (Hashtable)Session["EmailSummaryFilter"];
                }
                try
                {
                    LoadDateFilter();
                    BindGrid();
                }
                catch (Exception ex)
                {
                    FileLogger.WriteException(ex);
                    AlertScript(ex.Message);
                }
            }
            else
            {
                if (Convert.ToBoolean(hdnInitMails.Value))
                {
                    SavePromotions();
                    Response.Redirect("EMCustomersMailer.aspx?ID=" + hdnPkeyEmailCampign.Value);
                }
            }
        }

        private void BindGrid()
        {
            filterType = "C";
            if (ddlDateRange.SelectedIndex == 0)
            {
                filterType = "T";
            }
            else if (ddlDateRange.SelectedIndex == 1)
            {
                filterType = "M";
            }
            else if (ddlDateRange.SelectedIndex == 2)
            {
                filterType = "M";
            }

            ReportsModel cData = ReportService.GetCustomerCampaignSummaryByDateRange(txtFromDate.Text.Trim(), string.Format("{0} 23:59:59", txtToDate.Text.Trim()),
                                                                                        filterType, Convert.ToInt32(ddlKiosk.SelectedValue),
                                                                                        ddlCustomerstatus.SelectedValue, ConnString);

            htFilterData = new Hashtable();

            htFilterData.Add("FilterType", ddlDateRange.SelectedIndex);
            htFilterData.Add("FromDate", txtFromDate.Text.Trim());
            htFilterData.Add("ToDate", txtToDate.Text.Trim());
            htFilterData.Add("fkey_ec_attributes", ddlKiosk.SelectedValue);
            htFilterData.Add("CustomerStatus", ddlCustomerstatus.SelectedValue);
            Session["EmailSummaryFilter"] = htFilterData;

            promoData = ReportService.GetProductPromotions(ConnString);
            rptrCampaignSummary.DataSource = cData.EmailCampaign;
            //gvCustomersList.Columns[2].Visible = true;
            rptrCampaignSummary.DataBind();

            foreach (RepeaterItem item in rptrCampaignSummary.Items)
            {
                DropDownList ddlPromotion = (DropDownList)item.FindControl("ddlPromotion");
                HiddenField hdnFkeyPromo = (HiddenField)item.FindControl("hdnFkeyPromo");
                HiddenField hdnPkeycampaign = (HiddenField)item.FindControl("hdnPkeycampaign");
                HiddenField hdncount = (HiddenField)item.FindControl("hdncount");
                Button btnMail = (Button)item.FindControl("btnInitiateMails");

                ddlPromotion.DataSource = promoData.ProductPromos;
                ddlPromotion.DataTextField = "promo_code";
                ddlPromotion.DataValueField = "pkey_promotion";
                ddlPromotion.DataBind();
                ddlPromotion.Items.Insert(0, new ListItem("Select Promotion", "-1"));

                if (hdnFkeyPromo.Value != "")
                    ddlPromotion.SelectedValue = hdnFkeyPromo.Value;

                btnMail.Attributes["onclick"] = string.Format("javascript:return InitiateEmails({0},'{1}',{2});", hdnPkeycampaign.Value, ddlPromotion.ClientID, hdncount.Value);
            }
        }

        private void LoadDateFilter()
        {
            ddlDateRange.Items.Clear();
            ddlDateRange.Items.Add(new ListItem("Current Week", "-1"));
            ddlDateRange.Items.Add(new ListItem("Last Week", "0"));
            ddlDateRange.Items.Add(new ListItem("Last Month", "1"));
            ddlDateRange.Items.Add(new ListItem("Date Range", "2"));
            txtFromDate.Enabled = false;
            txtToDate.Enabled = false;
            if (htFilterData.ContainsKey("FilterType"))
            {
                ddlDateRange.SelectedIndex = Convert.ToInt32(htFilterData["FilterType"]);
                txtFromDate.Text = Convert.ToString(htFilterData["FromDate"]);
                txtToDate.Text = Convert.ToString(htFilterData["ToDate"]);
            }
            else
            {
                ddlDateRange.SelectedIndex = 0;
                SetFilterDates();
            }
            if (ddlDateRange.SelectedIndex == 3)
            {
                txtFromDate.Enabled = true;
                txtToDate.Enabled = true;
            }

            ddlCustomerstatus.Items.Clear();
            ddlCustomerstatus.Items.Insert(0, new ListItem("ALL", "ALL"));
            ddlCustomerstatus.Items.Insert(1, new ListItem("New Customers", "New Customer"));
            ddlCustomerstatus.Items.Insert(2, new ListItem("Returning Customers", "Returning Customer"));
            ddlCustomerstatus.SelectedIndex = 0;
            BindKiosks();
        }

        private void BindKiosks()
        {
            ddlKiosk.Items.Clear();
            ReportsModel filterData = ReportService.GetKiosks(ConnString);
            ddlKiosk.DataSource = filterData.Kiosks;
            ddlKiosk.DataValueField = "fkey_ec_attribute";
            ddlKiosk.DataTextField = "kiosk";
            ddlKiosk.DataBind();
            ddlKiosk.Items.Insert(0, new ListItem("ALL", "-1"));
            ddlKiosk.SelectedIndex = 0;
            //LoadProductDDList(Convert.ToInt32(ddlKiosk.SelectedValue));
        }

        private void SetFilterDates()
        {
            filterType = "C";
            txtFromDate.Enabled = false;
            txtToDate.Enabled = false;
            if (ddlDateRange.SelectedIndex == 0)
            {
                txtToDate.Text = System.DateTime.Now.ToString("MM/dd/yyyy");
                DateTime fromDate = System.DateTime.Now.AddDays(-7);
                while (fromDate.DayOfWeek != DayOfWeek.Sunday)
                    fromDate = fromDate.AddDays(1);
                txtFromDate.Text = fromDate.ToString("MM/dd/yyyy");
                filterType = "T";
            }
            else if (ddlDateRange.SelectedIndex == 1)
            {
                DateTime toDate = System.DateTime.Now.AddDays(-7);
                while (toDate.DayOfWeek != DayOfWeek.Saturday)
                    toDate = toDate.AddDays(1);
                DateTime fromDate = toDate.AddDays(-6);
                txtToDate.Text = toDate.ToString("MM/dd/yyyy");
                txtFromDate.Text = fromDate.ToString("MM/dd/yyyy");
                filterType = "W";
            }
            else if (ddlDateRange.SelectedIndex == 2)
            {
                DateTime dt = DateTime.Now;
                int dayofDay = dt.Day;
                //int dayofMonth = dt.Month;
                dt = System.DateTime.Now.AddDays(-dayofDay + 1);
                txtToDate.Text = System.DateTime.Now.AddDays(-dayofDay).ToString("MM/dd/yyyy");
                txtFromDate.Text = dt.AddMonths(-1).ToString("MM/dd/yyyy");
                filterType = "M";
            }
            else
            {
                txtFromDate.Enabled = true;
                txtToDate.Enabled = true;
            }
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
                btnReset.Visible = true;
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                Session.Remove("EmailSummaryFilter");
                LoadDateFilter();
                BindGrid();
                btnReset.Visible = false;
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void ddlDateRange_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SetFilterDates();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnSavePromotioms_Click(object sender, EventArgs e)
        {
            try
            {
                SavePromotions();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("EMCustomerStats.aspx");
        }

        bool isDataSaved = true;
        private void SavePromotions()
        {
            isDataSaved = true;
            ReportsModel sData = new ReportsModel();
            try
            {
                foreach (RepeaterItem item in rptrCampaignSummary.Items)
                {
                    DropDownList ddlPromotion = (DropDownList)item.FindControl("ddlPromotion");
                    HiddenField hdnPkeycampaign = (HiddenField)item.FindControl("hdnPkeycampaign");
                    ReportsModel.EmailCampaignRow eRow = sData.EmailCampaign.NewEmailCampaignRow();
                    eRow.fkey_promo = Convert.ToInt32(ddlPromotion.SelectedValue);
                    eRow.pkey_email_campaign = Convert.ToInt32(hdnPkeycampaign.Value);
                    sData.EmailCampaign.Rows.Add(eRow);
                }
                ReportService.UpdateEmailCampaignPromotions(sData, ConnString);
                BindGrid();
                AlertScript("Promotions updated successfully.");
            }
            catch(Exception ex)
            {
                FileLogger.WriteException(ex);
                isDataSaved = false;
                AlertScript("Unable to update promotions. Please try again.");
            }
        }
    }
}