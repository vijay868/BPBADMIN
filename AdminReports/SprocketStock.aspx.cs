﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Bestprintbuy.Admin.Web.AdminReports
{
    public partial class SprocketStock : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                SprocketProcessor.ReadInventoryAndUpdate(ConnString);
                BindSprocketData();
            }
        }

        private void BindSprocketData()
        {
            CatalogRequestModel cData = ReportService.GetInventoryBySupplier("", ConnString);
            rptrInventory.DataSource = cData.InventoryItems;
            rptrInventory.DataBind();

            foreach (RepeaterItem item in rptrInventory.Items)
            {
                Label lbl = (Label)item.FindControl("lblUnits");
                Label lblskunumber = (Label)item.FindControl("lblskunumber");
                Label lbltemplate_name = (Label)item.FindControl("lbltemplate_name");
                Label lbldescription = (Label)item.FindControl("lbldescription");
                if (Convert.ToInt32(lbl.Text) < 10)
                {
                    lbl.Attributes.Add("style", "color:red;");
                    lblskunumber.Attributes.Add("style", "color:red;");
                    lbltemplate_name.Attributes.Add("style", "color:red;");
                    lbldescription.Attributes.Add("style", "color:red;");
                } 

                HyperLink hypImage = (HyperLink)item.FindControl("hypImage");
                Image image = (Image)item.FindControl("imgThumb");
                image.ImageUrl = string.Format("{0}/{1}", ConfigurationManager.AppSettings["Rpt_BPBCustomerBaseURL"], image.ImageUrl);
                hypImage.Attributes["onmouseover"] = string.Format("ShowToolTip('{0}');",
                image.ImageUrl.Replace("thumb.jpg", "AF.jpg"));
                hypImage.Attributes["onmouseout"] = "UnTip();";
            }
        }
    }
}
