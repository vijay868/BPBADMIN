﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Text;
using System.IO;
using System.Threading;

namespace Bestprintbuy.Admin.Web.AdminReports
{
    public partial class TransactionReport : PageBase
    {
        StringBuilder l_strCSV = new StringBuilder();
        private TransactionsModel cData = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                try
                {
                    txtFromDate.Text = DateTime.Now.AddDays(-30).ToString("MM/dd/yyyy");
                    txtToDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
                    BindDropDowns();
                    BindTransactionData();
                }
                catch (Exception ex)
                {
                    FileLogger.WriteException(ex);
                    AlertScript(ex.Message);
                }
            }
        }

        private void BindDropDowns()
        {
            ReportsModel filterData = ReportService.GetKiosks(ConnString);

            ddlKiosk.DataSource = filterData.Kiosks;
            ddlKiosk.DataValueField = "fkey_ec_attribute";
            ddlKiosk.DataTextField = "kiosk";
            ddlKiosk.DataBind();

            ddlKiosk.Items.Insert(0, new ListItem("ALL", "-2"));
            ddlKiosk.Items.Insert(1, new ListItem("BestPrintBuy", "-1"));
            ddlKiosk.SelectedIndex = 0;

            
        }

        private void BindTransactionData()
        {
            cData = ReportService.GetTransactionData(Convert.ToInt32(ddlKiosk.SelectedValue), Convert.ToDateTime(txtFromDate.Text), Convert.ToDateTime(txtToDate.Text), ConnString);
            lblTotalRecords.Text = string.Format(" Total Number of Transactions: {0}", cData.Transactions.Rows.Count);
            TransactionReportGrid.DataSource = cData.Transactions;
            TransactionReportGrid.DataBind();
            btnExportTocsv1.Visible = cData.Transactions.Rows.Count > 0;
           
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                txtToDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
                txtFromDate.Text = DateTime.Now.AddDays(-30).ToString("MM/dd/yyyy");

                ddlKiosk.SelectedIndex = 0;
                BindTransactionData();
                btnReset.Visible = false;
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            try
            {                
                TransactionReportGrid.PageIndex = 0;
                BindTransactionData();
                btnReset.Visible = true;
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }            
        }

        protected int GetRecordCountForPage()
        {
            return GetRecordCountForPageSize(TransactionReportGrid.PageSize, TransactionReportGrid.PageIndex + 1);
        }

        protected void TransactionReportGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            TransactionReportGrid.PageIndex = e.NewPageIndex;
            BindTransactionData();
        }

        protected void btnExportTocsv_Click(object sender, EventArgs e)
        {
            CreateCSVFile();
        }

        private void CreateCSVFile()
        {
            try
            {
                BindTransactionData();
                string strFileName = string.Format("{0}-TransactionReport.csv", DateTime.Now.ToString("MM-dd-yyyy"));
                Response.Clear();
                Response.ContentType = "text/csv";
                Response.AppendHeader("content-disposition", "attachment; filename= " + strFileName);
                CsvStrBuild();
                Response.Write(l_strCSV);
                Response.Flush();
                Response.End();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        private void CsvStrBuild()
        {
            l_strCSV.AppendFormat("\"Detail\",\"Quantity\",\"Product Cost\",\"Shipping Cost\",\"Discount\",\"Total\"{0}", Environment.NewLine);


            foreach (TransactionsModel.TransactionsRow TransRow in cData.Transactions)
            {
                l_strCSV.AppendFormat("\"{0}\",", TransRow.IsDetailNull() ? "" : TransRow.Detail);
                l_strCSV.AppendFormat("\"{0}\",", TransRow.IsQuantityNull() ? "0" : TransRow.Quantity);
                l_strCSV.AppendFormat("\"{0}\",", TransRow.IsProduct_CostNull() ? 0 : TransRow.Product_Cost);
                l_strCSV.AppendFormat("\"{0}\",", TransRow.IsShipping_CostNull() ? 0 : TransRow.Shipping_Cost);
                l_strCSV.AppendFormat("\"{0}\",", TransRow.IsDiscountNull() ? 0 : TransRow.Discount);
                l_strCSV.AppendFormat("\"{0}\",", TransRow.IsTotalNull() ? 0 : TransRow.Total);
                l_strCSV.AppendFormat(Environment.NewLine);
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/Dashboard.aspx");
        }
    }
}