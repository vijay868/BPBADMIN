﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="EMCustomersList.aspx.cs" Inherits="Bestprintbuy.Admin.Web.AdminReports.EMCustomersList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="clr"></div>
    <!-- Container Starts -->
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Marketing Reports</li>
            <li>Email Marketing Program</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Email Marketing Program - <asp:Label ID="lblCategory" runat="server"></asp:Label></h2>
        <form id="frmBpbReports" name="frmBpbReports" runat="server" class="form">           
            <div class="clr"></div>
             <asp:GridView ID="CustomersListGrid" runat="server" AutoGenerateColumns="false" EnableViewState="true"
                ForeColor="Black" Width="100%" PageSize="20" AllowPaging="true"
                CssClass="table" name="CustomersListGrid" OnPageIndexChanging="CustomersListGrid_PageIndexChanging">
                <PagerStyle HorizontalAlign="Right" CssClass="dtPage"></PagerStyle>
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />
                <Columns>
                    <asp:TemplateField HeaderText="#SL">
                        <HeaderStyle Width="6%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <span><%# GetRecordCountForPage()%></span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Customer Name">
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container,"DataItem.customer_name") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Email Address">
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container,"DataItem.email_id") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Phone">
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container,"DataItem.phone") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Address">
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container,"DataItem.AddressLine1") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="City">
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container,"DataItem.ShipCity") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="State & Zip">
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container,"DataItem.ShipState") %> <%# DataBinder.Eval(Container,"DataItem.ShipZip") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Date Registered">
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container,"DataItem.date_created") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Last Logged In">
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container,"DataItem.LastLoggedOn") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <div class="btn_lst">
                <div>
                    <asp:Button ID="btnClose" Text="Close" CssClass="button rounded" runat="server" OnClick="btnClose_Click"></asp:Button>
                    <asp:Button runat="server" ID="btnExportTocsv" Text="Export To CSV" CssClass="button rounded" OnClick="btnExportTocsv_Click"></asp:Button>
                </div>
            </div>
        </form>
    </div>
</asp:Content>
