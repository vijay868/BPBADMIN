﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Text;
using System.IO;
using System.Threading;
using System.Collections;
using System.Data;
using System.Web.UI.HtmlControls;

namespace Bestprintbuy.Admin.Web.AdminReports
{
    public partial class CatalogReport : PageBase
    {
        protected int recordCount = 0;
        protected CatalogRequestModel cData = null;
        StringBuilder l_strCSV = new StringBuilder();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                try
                {
                    SprocketProcessor.ReadInventoryAndUpdate(ConnString);
                    txtFromDate.Text = DateTime.Now.AddDays(-30).ToString("MM/dd/yyyy");
                    txtToDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
                    BindDropDowns();
                    BindCatalogData();
                }
                catch (Exception ex)
                {
                    FileLogger.WriteException(ex);
                    AlertScript(ex.Message);
                }
            }
        }

        private void BindCatalogData()
        {
            cData = ReportService.GetCatalogData(
                Convert.ToInt32(ddlKiosk.SelectedValue),
                Convert.ToDateTime(txtFromDate.Text),
                Convert.ToDateTime(txtToDate.Text),
                Convert.ToBoolean(chkIsEmailSent.Checked), ConnString);
            rptrCatalogReports.DataSource = cData.CatalogRequests;
            rptrCatalogReports.DataBind();

            recordCount = cData.CatalogRequests.Rows.Count;
            if (cData.CatalogRequests.Rows.Count > 0)
            {
                pnlContent.Visible = true;
                pnlClose.Visible = false;
            }
            else
            {
                pnlContent.Visible = false;
                pnlClose.Visible = true;
            }
        }

        private void BindDropDowns()
        {
            ReportsModel filterData = ReportService.GetKiosks(ConnString);
            ddlKiosk.DataSource = filterData.Kiosks;
            ddlKiosk.DataValueField = "fkey_ec_attribute";
            ddlKiosk.DataTextField = "kiosk";
            ddlKiosk.DataBind();

            ddlKiosk.Items.Insert(0, new ListItem("ALL", "-1"));
        }

        protected void btnPostOrderstoSupplier_Click(object sender, EventArgs e)
        {
            try
            {
                CatalogRequestModel CatalogListData = new CatalogRequestModel();
                cData = ReportService.GetCatalogData(
                    Convert.ToInt32(ddlKiosk.SelectedValue),
                    Convert.ToDateTime(txtFromDate.Text),
                    Convert.ToDateTime(txtToDate.Text),
                    Convert.ToBoolean(chkIsEmailSent.Checked), ConnString);
                StringBuilder sbError = new StringBuilder();
                bool hasErr = false;
                foreach (RepeaterItem item in rptrCatalogReports.Items)
                {

                    CheckBox chkSelect = (CheckBox)item.FindControl("chkEmail");
                    HiddenField hdnPkeyCatalogReq = (HiddenField)item.FindControl("hdnPkeyCatalogReq");
                    HiddenField orderNum = (HiddenField)item.FindControl("hdnOrderNo");
                    HiddenField skunumber = (HiddenField)item.FindControl("hdnSkuNo");
                    if (chkSelect.Checked)
                    {
                        int pkeyCatReq = Convert.ToInt32(hdnPkeyCatalogReq.Value);

                        DataRow[] drs = cData.CatalogRequests.Select(string.Format(
                            "AVAIL_QTY > 0 AND Pkey_CatalogReq = {0}", pkeyCatReq));

                        if (drs.Length >= 1)
                        {
                            CatalogRequestModel.CatalogRequestsRow row = (CatalogRequestModel.CatalogRequestsRow)drs[0];
                            if (!row.IsCR_MM_ORDERIDNull())
                            {
                                Hashtable data = SprocketProcessor.PostOrderToSprocket(row);

                                if (data != null)
                                {
                                    bool isError = Convert.ToBoolean(data["error"]);
                                    if (isError == false)
                                    {
                                        int api_order_id = Convert.ToInt32(data["api_order_id"]);
                                        CatalogRequestModel.CatalogRequestsRow catRow = CatalogListData.CatalogRequests.NewCatalogRequestsRow();
                                        catRow.ItemArray = row.ItemArray;
                                        catRow.ORDER_STATUS = "SENT TO SUPPLIER";
                                        catRow.CR_SUPPLIER_ORDER_ID = api_order_id.ToString();
                                        CatalogListData.CatalogRequests.Rows.Add(catRow);
                                    }
                                    else
                                    {
                                        ArrayList al = (ArrayList)data["error_msgs"];
                                        sbError.AppendFormat("{0} cannot be posted to supplier - {1}{2}", orderNum.Value,
                                            (al != null && al.Count > 0) ? al[0] : "", "<br />");
                                        hasErr = true;
                                    }
                                }
                            }
                            else
                            {
                                sbError.AppendFormat("{0} cannot be posted to supplier - Email Order to supplier.{1}", orderNum.Value, "<br />");
                                hasErr = true;
                            }
                        }
                        else
                        {
                            sbError.AppendFormat("{0} cannot be posted to supplier - {1} not available.{2}", orderNum.Value, skunumber.Value, "<br />");
                            hasErr = true;
                        }
                    }
                }
                if (CatalogListData.CatalogRequests.Rows.Count > 0)
                    ReportService.UpdateSprocketCatReqMailSentStatus(CatalogListData, ConnString);

                BindCatalogData();
                if (hasErr == true)
                {
                    //divActionMessages.Visible = true;
                    //ltrlScript.Text = string.Format("<script language='javascript' type='text/javascript'>SetFailMessage('{0}','{1}');</script>",
                    //    divActionMessages.ClientID, sbError.ToString().Replace("'", "`"));
                    AlertScript(sbError.ToString().Replace("'", "`"));
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }

        }

        protected void btnEmailtoSupplier_Click(object sender, EventArgs e)
        {
            try
            {
                string attachement = SaveFiles();
                if (File.Exists(attachement))
                {
                    LoggedUser lUser = (LoggedUser)Session["LoggedUser"];
                    ReportsModel sData = ReportService.GetData("RQCAT", ConnString);

                    if (sData.ReportSettings.Rows.Count <= 0)
                    {
                        //ltrlScript.Text = string.Format("<script language='javascript' type='text/javascript'>alert('{0}');</script>", "Unable to send email.");
                        AlertScript("Unable to send email.");
                        return;
                    }
                    ReportsModel.ReportSettingsRow rRow = (ReportsModel.ReportSettingsRow)sData.ReportSettings.Rows[0];
                    MailSender mailSender = new MailSender(lUser.EmailID,
                        rRow.supplier_email,
                        string.Format("Please find the attached Catalog DM List<br><br>{0}", lUser.Name),
                        "BestPrintBuy.com auto mailer - Catalog DM List.",
                        "", "HTML", true, attachement);
                    Thread mailThread = new Thread(new ThreadStart(mailSender.SendEmail));
                    mailThread.Start();
                    ReportService.UpdateCatReqMailSentStatus(cData, ConnString);
                    BindCatalogData();
                    //ltrlScript.Text = string.Format("<script language='javascript' type='text/javascript'>alert('{0}');</script>", "Email has been successfully sent.");
                    AlertScript("Email has been successfully sent.");
                }
                else
                {
                    //ltrlScript.Text = string.Format("<script language='javascript' type='text/javascript'>alert('{0}');</script>", "Unable to send email.");
                    AlertScript("Unable to send email.");
                    //alert unable to send email
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        private string SaveFiles()
        {
            BindCatalogData();
            CsvStrBuild();
            string l_strFileLocation = "";
            string l_strCSVFilename;
            string l_strServerPath = Server.MapPath(".");
            l_strFileLocation = string.Format("{0}/EmailAttachments", l_strServerPath);
            Util.CheckDirExistance(l_strFileLocation);
            l_strCSVFilename = l_strFileLocation + "/" + string.Format("{0}-RequestCatalog.csv", DateTime.Now.ToString("MM-dd-yyyy"));
            Util.SaveFile(l_strCSVFilename, l_strCSV.ToString());
            return l_strCSVFilename;
        }

        private void CsvStrBuild()
        {
            l_strCSV.AppendFormat("\"Order No\",\"First Name\",\"Last Name\",\"Company Name\",\"Address 1\",\"Address 2\",\"City\",\"State\",\"Zip\",\"Country\",\"Phone\",\"Shipping Type\",\"SKU  Number\",\"Quantity\"{0}", Environment.NewLine);


            foreach (CatalogRequestModel.CatalogRequestsRow catalogRow in cData.CatalogRequests)
            {
                l_strCSV.AppendFormat("\"{0}\",", catalogRow.Order_No);
                l_strCSV.AppendFormat("\"{0}\",", catalogRow.First_Name);
                l_strCSV.AppendFormat("\"{0}\",", catalogRow.Last_Name);
                l_strCSV.AppendFormat("\"{0}\",", catalogRow.Company_Name);
                l_strCSV.AppendFormat("\"{0}\",", catalogRow.Address_1);
                l_strCSV.AppendFormat("\"{0}\",", catalogRow.IsAddress_2Null() ? "" : catalogRow.Address_2);
                l_strCSV.AppendFormat("\"{0}\",", catalogRow.City);
                l_strCSV.AppendFormat("\"{0}\",", catalogRow.State);
                l_strCSV.AppendFormat("\"{0}\",", catalogRow.Zip);
                l_strCSV.AppendFormat("\"{0}\",", catalogRow.Country);
                l_strCSV.AppendFormat("\"{0}\",", catalogRow.Phone);
                l_strCSV.AppendFormat("\"{0}\",", catalogRow.Shipping_Type);
                l_strCSV.AppendFormat("\"{0}\",", catalogRow.SKU_Number);
                l_strCSV.AppendFormat("\"{0}\"", catalogRow.Quantity);
                l_strCSV.AppendFormat(Environment.NewLine);
            }
        }

        protected void btnExportTocsv_Click(object sender, EventArgs e)
        {
            CreateCSVFile();
        }

        private void CreateCSVFile()
        {
            try
            {
                BindCatalogData();
                string strFileName = string.Format("{0}-RequestCatalog.csv", DateTime.Now.ToString("MM-dd-yyyy"));
                Response.Clear();
                Response.ContentType = "text/csv";
                Response.AppendHeader("content-disposition", "attachment; filename= " + strFileName);
                CsvStrBuild();
                Response.Write(l_strCSV);
                Response.Flush();
                Response.End();
            }
            catch (Exception)
            {
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                CatalogRequestModel CatalogListData = new CatalogRequestModel();

                foreach (RepeaterItem item in rptrCatalogReports.Items)
                {
                    CatalogRequestModel.CatalogRequestsRow catRow = CatalogListData.CatalogRequests.NewCatalogRequestsRow();
                    CheckBox chkSelect = (CheckBox)item.FindControl("chkEmail");
                    HiddenField hdnPkeyCatalogReq = (HiddenField)item.FindControl("hdnPkeyCatalogReq");
                    HiddenField hdnSkuNo = (HiddenField)item.FindControl("hdnSkuNo");
                    if (chkSelect.Checked)
                    {
                        catRow.Pkey_CatalogReq = Convert.ToInt32(hdnPkeyCatalogReq.Value);
                        catRow.SKU_Number = hdnSkuNo.Value;
                        CatalogListData.CatalogRequests.Rows.Add(catRow);
                    }
                }

                ReportService.UpdateCatReqMailSentStatus(CatalogListData, ConnString);
                AlertScript("Successfully Saved.");
                BindCatalogData();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/Dashboard.aspx");
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            BindCatalogData();
            btnReset.Visible = true;
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                txtToDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
                txtFromDate.Text = DateTime.Now.AddDays(-30).ToString("MM/dd/yyyy");
                ddlKiosk.SelectedIndex = 0;
                BindCatalogData();
                btnReset.Visible = false;
                pnlContent.Visible = false;
                pnlClose.Visible = true;
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void chkIsEmailSent_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chkIsEmailSent.Checked)
                {
                    //pnlContent.Visible = false;
                    btnEmailtoSupplier.Visible = false;
                    btnExportTocsv.Visible = false;
                    btnPostOrderstoSupplier.Visible = false;
                    btnSave.Visible = false;
                    btnClose.Visible = false;
                    btnClose.Visible = false;
                }
                else
                {
                    if (rptrCatalogReports.Items.Count > 0)
                    {
                        pnlContent.Visible = true;
                        btnEmailtoSupplier.Visible = true;
                        btnExportTocsv.Visible = true;
                        btnPostOrderstoSupplier.Visible = true;
                        btnSave.Visible = true;
                        btnClose.Visible = true;
                    }
                    else
                        btnClose.Visible = true;
                }
                bool isDisabled = chkIsEmailSent.Checked;
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }
    }
}