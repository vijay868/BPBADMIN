﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="MCAReport.aspx.cs" Inherits="Bestprintbuy.Admin.Web.AdminReports.MCAReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function validate_twoDates() {
            var dateStart = $("#ContentPlaceHolder1_txtFromDate").val();
            var dateEnd = $("#ContentPlaceHolder1_txtToDate").val();
            if (Date.parse(dateStart) > Date.parse(dateEnd)) {
                alert("Invalid date range!");
                return false;
            }
        }

        $(function () {
            $("#ContentPlaceHolder1_txtFromDate").datepicker();
            $("#ContentPlaceHolder1_txtToDate").datepicker();
            $("#frmBpbReports").validate();
        });

        function OpenReportSettingsDialog(pkey_credit) {
            var url = "../AdminReports/ReportSettings.aspx?cat=MCA";
            $.fancybox({
                'width': '80%',
                'height': '80%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': url,
                'type': 'iframe'
            });
            return false;
        }
    </script>
    <div class="clr"></div>
    <!-- Container Starts -->
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Administrative Reports</li>
            <li>MCA Report</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>MCA Report</h2>
        <form id="frmBpbReports" name="frmBpbReports" runat="server">
            <table cellpadding="0" cellspacing="0" border="0" width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="15%">From Date</td>
                        <td width="10%">
                            <asp:TextBox CssClass="text-box date" Width="250px" name="txtFromDate" ID="txtFromDate" runat="server"></asp:TextBox></td>
                        <td width="15%" style="padding-left: 15px">To Date</td>
                        <td width="10%">
                            <asp:TextBox CssClass="text-box date" Width="250px" name="txtToDate" ID="txtToDate" runat="server"></asp:TextBox></td>
                        <td width="12%" style="padding-left: 15px">Kiosk </td>
                        <td width="15%">
                            <asp:DropDownList name="ddlKiosk" ID="ddlKiosk" runat="server" CssClass="slect-box" Width="223px">
                            </asp:DropDownList>
                        </td>
                        <td width="25%"></td>
                    </tr>
                    <tr>
                        <td>Promotion</td>
                        <td>
                            <asp:DropDownList name="ddlPromos" ID="ddlPromos" runat="server" CssClass="slect-box" Width="270px">
                            </asp:DropDownList>
                        </td>
                        <td style="padding-left: 15px">
                            <asp:CheckBox runat="server" ID="chkIsEmailSent"></asp:CheckBox>
                            Email Sent
                        </td>
                        <td>
                            <asp:Button runat="server" ID="btnFilter" CssClass="button rounded" Text="Filter" OnClick="btnFilter_Click" OnClientClick="return validate_twoDates();"></asp:Button>
                            <asp:Button runat="server" ID="btnReset" CssClass="button rounded" Text="Reset" OnClick="btnReset_Click" UseSubmitBehavior="false" Visible="false"></asp:Button>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>           
            <div class="clr"></div>
            <asp:Panel ID="pnlContent" runat="server" Visible="false">
                <table class="table" width="100%">
                    <thead>
                        <tr>
                            <th width="6%"></th>
                            <th width="10%">Order No</th>
                            <th width="10%">First Name</th>
                            <th width="10%">Last Name</th>
                            <th width="10%">Company Name</th>
                            <th width="10%">Address 1</th>
                            <th width="10%">Address 2</th>
                            <th width="8%">City</th>
                            <th width="6%">State</th>
                            <th width="6%">Zip</th>
                            <th width="6%">Shipping Type</th>
                            <th width="12%">SKU</th>
                            <th width="6%">Qty</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rptrMCAReports" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="chkEmail" runat="server" CssClass="chk" />
                                        <asp:HiddenField ID="hdnSku" runat="server" Value='<%# Eval("SKU Number")%>' />
                                        <asp:HiddenField ID="hdnPkey_customer" runat="server" Value='<%# Eval("pkey_customer")%>' />
                                    </td>
                                    <td>
                                        <%#Eval("Order No") %>
                                    </td>
                                    <td>
                                        <%#Eval("First Name") %>
                                    </td>
                                    <td>
                                        <%#Eval("Last Name") %>
                                    </td>
                                    <td>
                                        <%#Eval("Company Name") %>
                                    </td>
                                    <td>
                                        <%#Eval("Address 1") %>
                                    </td>
                                    <td>
                                        <%#Eval("Address 2") %>
                                    </td>
                                    <td>
                                        <%#Eval("City") %>
                                    </td>
                                    <td>
                                        <%#Eval("State") %>
                                    </td>
                                    <td>
                                        <%#Eval("Zip") %>
                                    </td>
                                    <td>
                                        <%#Eval("Shipping Type") %>
                                    </td>
                                    <td>
                                        <%#Eval("SKU Number") %>
                                    </td>
                                    <td>
                                        <%#Eval("Quantity") %>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
                <div class="ttl"><b>Total MCAs: </b><%=recordCount%></div>
                <div class="form">
                    <div class="btn_lst">
                        <div class="lft">
                            <asp:Button runat="server" ID="btnReportSettings" CssClass="button rounded" Text="MCA Report Settings" UseSubmitBehavior="false" OnClientClick="OpenReportSettingsDialog(); return false;"></asp:Button>
                        </div>
                        <div>
                            <asp:Button ID="btnClose" Text="Close" CssClass="button rounded" runat="server" OnClick="btnClose_Click" UseSubmitBehavior="false"></asp:Button>
                            <asp:Button runat="server" ID="btnEmailtoSupplier" Text="Email To Supplier" CssClass="button rounded" OnClick="btnEmailtoSupplier_Click" UseSubmitBehavior="false"></asp:Button>
                            <asp:Button runat="server" ID="btnExportTocsv" CssClass="button rounded" Text="Export to CSV" OnClick="btnExportTocsv_Click"></asp:Button>
                            <asp:Button runat="server" ID="btnSave" CssClass="button rounded" Text="Save" OnClick="btnSave_Click" UseSubmitBehavior="false"></asp:Button>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlClose" runat="server" CssClass="form">
                <div class="ttl"><b>Total MCAs: </b><%=recordCount%></div>
                <div class="btn_lst">
                    <div class="lft">
                        <asp:Button runat="server" ID="Button1" CssClass="button rounded" Text="MCA Report Settings" OnClientClick="OpenReportSettingsDialog(); return false;"></asp:Button>
                    </div>
                    <div>
                        <asp:Button ID="Button2" Text="Close" CssClass="button rounded" runat="server" UseSubmitBehavior="false" OnClick="btnClose_Click"></asp:Button>
                    </div>
                </div>
            </asp:Panel>
        </form>
    </div>
</asp:Content>
