﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Text;
using System.IO;
using System.Threading;

namespace Bestprintbuy.Admin.Web.AdminReports
{
    public partial class CatalogReportStatus : PageBase
    {
        private CatalogRequestModel cData = null;
        protected int recordCount = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                try
                {
                    SprocketProcessor.ReadInventoryAndUpdate(ConnString);
                    txtFromDate.Text = DateTime.Now.AddDays(-30).ToString("MM/dd/yyyy");
                    txtToDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
                    BindDropDowns();
                    BindCatalogData();
                }
                catch (Exception ex)
                {
                    FileLogger.WriteException(ex);
                    AlertScript(ex.Message);
                }
            }
        }

        private void BindDropDowns()
        {
            ReportsModel filterData = ReportService.GetKiosks(ConnString);
            ddlKiosk.DataSource = filterData.Kiosks;
            ddlKiosk.DataValueField = "fkey_ec_attribute";
            ddlKiosk.DataTextField = "kiosk";
            ddlKiosk.DataBind();

            ddlKiosk.Items.Insert(0, new ListItem("ALL", "-1"));
        }

        private void BindCatalogData()
        {
            cData = ReportService.GetCatalogSupplierData(
                Convert.ToInt32(ddlKiosk.SelectedValue),
                Convert.ToDateTime(txtFromDate.Text),
                Convert.ToDateTime(txtToDate.Text), ConnString);
          recordCount = cData.CatalogRequests.Rows.Count;
            rptrCatalogStatus.DataSource = cData.CatalogRequests;
            rptrCatalogStatus.DataBind();

            if (cData.CatalogRequests.Rows.Count > 0)
            {
                pnlContent.Visible = true;
                pnlClose.Visible = false;
            }
            else
            {
                pnlContent.Visible = false;
                pnlClose.Visible = true;
            }
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            try
            {
                BindCatalogData();
                btnReset.Visible = true;
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                txtToDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
                txtFromDate.Text = DateTime.Now.AddDays(-30).ToString("MM/dd/yyyy");
                ddlKiosk.SelectedIndex = 0;
                BindCatalogData();
                btnReset.Visible = false;
                pnlContent.Visible = false;
                pnlClose.Visible = true;
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnUpdateOrderStatus_Click(object sender, EventArgs e)
        {
            try
            {
                SprocketProcessor.ReadStatusAndUpdate(ConnString);
                SprocketProcessor.ReadTrackingStatusAndUpdate(ConnString);
                BindCatalogData();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/DashBoard.aspx");
        }
    }
}