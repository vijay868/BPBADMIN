﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewEmailContent.aspx.cs" Inherits="Bestprintbuy.Admin.Web.AdminReports.ViewEmailContent" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <div id="Div1" class="popup-wrapper" runat="server">
        <!-- Header Ends -->
        <div class="popup-contnt">
            <!-- Menu starts -->
            <form id="Form1" runat="server">
                <div>
                <%= strEmailContent %>
                </div>
            </form>
        </div>
    </div>
</body>
</html>
