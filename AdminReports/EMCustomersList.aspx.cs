﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Text;

namespace Bestprintbuy.Admin.Web.AdminReports
{
    public partial class EMCustomersList : PageBase
    {
        int fkey_ec_attribute = -1;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {                    
                    string strCat = Request.QueryString["CSListFor"];
                    LoadCustomerList(strCat);
                }
                catch (Exception ex)
                {
                    FileLogger.WriteException(ex);
                    AlertScript(ex.Message);
                }
            }
        }

        private void LoadCustomerList(string strCat)
        {
            ReportsModel usr = ReportService.GetCustomerStatistics(strCat, ConnString);
            CustomersListGrid.DataSource = usr.EmailCustomerInfo;
            CustomersListGrid.DataBind();
            SetLabel(usr.EmailCustomerInfo.Rows.Count, strCat);
        }

        protected int GetRecordCountForPage()
        {
            return GetRecordCountForPageSize(CustomersListGrid.PageSize, CustomersListGrid.PageIndex + 1);
        }

        protected void CustomerslistGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                CustomersListGrid.PageIndex = e.NewPageIndex;
                string strCat = Request.QueryString["CSListFor"];
                LoadCustomerList(strCat);
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        private void SetLabel(int recs, string strCat)
        {
            switch (strCat)
            {
                case "PUC":
                    lblCategory.Text = string.Format("{0} Power Users", recs);
                    break;
                case "PC":
                    lblCategory.Text = string.Format("{0} Paid Customers", recs);
                    break;
                case "UC":
                    lblCategory.Text = string.Format("{0} Un-committed Customers", recs);
                    break;
                case "UP":
                    lblCategory.Text = string.Format("{0} Un Paid registered users", recs);
                    break;
                case "USC":
                    lblCategory.Text = string.Format("{0} Users with service credits", recs);
                    break;
                case "NU":
                    lblCategory.Text = string.Format("{0} users registered in the last week", recs);
                    break;
            }
        }

        protected void btnExportTocsv_Click(object sender, EventArgs e)
        {
            CreateCSVFile();
        }

        private void CreateCSVFile()
        {
            try
            {
                string strCat = Request["CSListFor"];
                string strFileName = string.Format("EmailMarketingCustomers-{0}-{1}.csv", strCat, DateTime.Now.ToString("MM-dd-yyyy"));
                Response.Clear();
                Response.ContentType = "text/csv";
                Response.AppendHeader("content-disposition", "attachment; filename= " + strFileName);

                Response.Write(CsvStrBuild(strCat));
                Response.Flush();
                Response.End();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        private string CsvStrBuild(string strCat)
        {

            ReportsModel usr = ReportService.GetCustomerStatistics(strCat, ConnString);
            StringBuilder l_strCSV = new StringBuilder("");
            l_strCSV.AppendFormat("\"S.No\",\"Customer Name\",\"Email Address\",\"Phone\",\"Address1\",\"Address2\",\"City\",\"State\",\"Zip\",\"Date Registered\"{0}", Environment.NewLine);

            int iCounter = 1;
            foreach (ReportsModel.EmailCustomerInfoRow cartRow in usr.EmailCustomerInfo.Rows)
            {
                l_strCSV.AppendFormat("\"{0}\",", iCounter);
                l_strCSV.AppendFormat("\"{0}\",", cartRow.customer_name);
                l_strCSV.AppendFormat("\"{0}\",", cartRow.email_id);
                l_strCSV.AppendFormat("\"{0}\",", cartRow.IsphoneNull() ? "" : cartRow.phone);
                l_strCSV.AppendFormat("\"{0}\",", cartRow.IsShipAddress1Null() ? "" : cartRow.ShipAddress1);
                l_strCSV.AppendFormat("\"{0}\",", cartRow.IsShipAddress2Null() ? "" : cartRow.ShipAddress2);
                l_strCSV.AppendFormat("\"{0}\",", cartRow.IsShipCityNull() ? "" : cartRow.ShipCity);
                l_strCSV.AppendFormat("\"{0}\",", cartRow.IsShipStateNull() ? "" : cartRow.ShipState);
                l_strCSV.AppendFormat("\"{0}\",", cartRow.IsShipZipNull() ? "" : cartRow.ShipZip);
                l_strCSV.AppendFormat("\"{0}\",", cartRow.date_created);
                l_strCSV.AppendFormat(Environment.NewLine);
                iCounter++;
            }
            return l_strCSV.ToString();
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("EMCustomerStats.aspx");
        }

        protected void CustomersListGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                CustomersListGrid.PageIndex = e.NewPageIndex;
                string strCat = Request.QueryString["CSListFor"];
                LoadCustomerList(strCat);
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }
    }
}