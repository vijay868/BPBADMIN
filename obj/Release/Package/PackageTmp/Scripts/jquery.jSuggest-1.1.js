/**
 * jQuery jSuggest
 *
 * @url		http://jSuggest.scottreeddesign.com/
 * @author	Brian Reed <brian@scottreeddesign.com>
 * @version	0.0.0
**/
(function($){
			  
	// Main plugin function
	$.fn.jSuggest = function(options)
	{		
		// Build main options before element iteration
		var o = $.extend({}, $.fn.jSuggest.defaults, options);
		
		// Iterate through each element
		return this.each(function()
		{
			// input id
			var i_id = ($(this).attr('id').length > 0 ? $(this).attr('id') : 'input-'+Number(new Date().getTime()));		

			// create input box suggestions id {
			var sd_id = i_id + '-suggestions';
			$('body').append('<div id="'+sd_id+'" class="'+o.css_class+'"><ul></ul></div>');
			$('#'+sd_id).css({
				'z-index':'99999',
				'position':'absolute',
				'top':(($(this).offset().top)+$(this).outerHeight()) +'px',
				'left':$(this).offset().left+'px',
				'width':($(this).outerWidth()) + 'px',
				'text-align': 'left',
				'border': '1px solid',
				'background-color': 'white'
			});
			$('#'+sd_id).hide(); // }			
			
			// set default text to grey
			if(o.default_text.length > 0){
				$(this).css({'color':'#828282'});
				$(this).val(o.default_text);				
			}
			// clears default text
			function click_clear(id, default_text){
				var input = $('#'+id);
				if(input.val() == default_text){
					input.val("");
					input.css({'color':''});
				}else if(input.val().length == 0){
					input.val(default_text);
					input.css({'color':'#828282'});
				}
			}			
			
		    // input : blur class / click clear / hide / update suggestions
		    try {
		        $(this).unbind('blur');
		    } catch (ex) { }
			$(this).blur(function(){
				$(this).addClass('blur');
				click_clear(i_id, o.default_text);
				if(!$('#'+sd_id).hasClass('hover'))
					$('#'+sd_id).hide(); 
			});

		    try {
		        $(this).unbind('focus');
		    } catch (ex) { }
			$(this).focus(function(){
				$(this).removeClass('blur');
				click_clear(i_id, o.default_text);
				update_suggestions(i_id);
			});
			
			// suggestion box : hover class / hide
			$('#'+sd_id).hover(function(){
				$(this).addClass('hover');
			},function(){
				$(this).removeClass('hover');
				if($('#'+i_id).hasClass('blur'))
					$('#'+sd_id).hide(); 
			});
			
		    //
		    try {
		        $(this).unbind('keyup');
		    } catch (ex) { }
			$(this).keyup(function(){update_suggestions(i_id);});

			//
			function suggest_onclick(text, id) {
				$('#'+id).focus().val(text);
				$('#'+sd_id).hide();	
				if(o.li_onclick != ''){			
				    eval(o.li_onclick + "()");
				}
			}
			
			
			//			
			function update_suggestions(id){
			
				var items = '';
				var result_html = '<ul style="padding-left: 18px; margin: 0;">';
				var input_val = $('#'+id).val();
				var input_regex = new RegExp("("+input_val+")", "gi");
				var match_html = '';
				var matches = [];
				
				if(input_val.length == 0){
					$('#'+sd_id).hide();									
				} else if (o.terms_url.length > 0 && o.terms.length == 0) {

				    

				    //alert(o.terms_url.replace('%input%', escape(input_val)));
				    $.get(o.terms_url.replace('%input%', escape(input_val)), function (data) {

				        //alert(o.terms_url.replace('%input%', escape(input_val)));
				        //data = "['abcd','efgh','xyz']";
						var results = (new Function("return " + data))();
						
						if(results.length > 0){
							for (var i = 0; i < results.length; i++){
								if((i+1)>o.limit) break;
								matches = results[i].match(input_regex);
								match_html = results[i];
//								for (var j = 0; j < matches.length; j++){
//									match_html = match_html.replace(matches[j],'<b>'+matches[j]+'</b>');
//								}
								result_html += 
									'<li title="'+results[i]+'">'+match_html+'</li>';
							}
						}else{
							result_html += '<li class="no-suggestions-found">No suggestions found.</li>';
						}					
						result_html += "</ul>";						
						$('#'+sd_id).html(result_html);
						if(results.length > 0)
							$('#'+sd_id+' ul li').click(function(){suggest_onclick($(this).attr('title'), id);});
						$('#'+sd_id).show();
					});
				}else if(o.terms.length > 0){
					
					var x = 1;
					for (var i = 0; i < o.terms.length; i++){
						if(o.terms[i].toLowerCase().indexOf(input_val.toLowerCase()) < 0)
							continue;
						if(x>o.limit) break; x++;
						matches = o.terms[i].match(input_regex);
						match_html = o.terms[i];
						for (var j = 0; j < matches.length; j++){
							match_html = match_html.replace(matches[j],'<b>'+matches[j]+'</b>');
						}
						
						result_html += 
							'<li title="'+o.terms[i]+'">'+match_html+'</li>';
					}
					
					if(result_html == '<ul style="padding: 0; margin: 0;">')
						result_html += '<li class="not-found">No suggestions found.</li>';
					result_html += "</ul>";
					
					$('#'+sd_id).html(result_html);
					if(x>0)
					$('#'+sd_id+' ul li').click(function(){suggest_onclick($(this).attr('title'), id);});
					$('#'+sd_id).show();
				}
				
			}
			
		});
			
	}
	
	// Default settings
	$.fn.jSuggest.defaults =
	{
		terms_url:			'',
		terms:				[],
		default_text:		'',
		css_class:			'suggestions-dropdown',
		limit:				10,
		li_onclick:			''
	}
	
})(jQuery);