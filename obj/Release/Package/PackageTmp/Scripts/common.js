$(window).load (function(){

	$(".iframe").fancybox({
			type: 'iframe',
			height: '510px',
			width:'800px'
		});
	
});


$(document).ready(function(){
	/*-For Ie support Rounded corners-*/
	if ( $.browser.msie && $.browser.version <= 9.0) {
		setTimeout(function(){
			$.getScript("Scripts/plugin/pie/PIE.js", function(){
				if (window.PIE) {
					$('.rounded, .curvedCorners').each(function() {
						PIE.attach(this);
					});
				}
			})
		}, 100);
	}
	
	
	ddsmoothmenu.init({
	mainmenuid: "smoothmenu1", //menu DIV id
	orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu', //class added to menu's outer DIV
	//customtheme: ["#1c5a80", "#18374a"],
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
	})

	
	
});