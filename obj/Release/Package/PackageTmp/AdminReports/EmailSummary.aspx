﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="EmailSummary.aspx.cs" Inherits="Bestprintbuy.Admin.Web.AdminReports.EmailSummary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function InitiateEmails(pkey_email_campaign, cboID, ccount) {
            if (ccount <= 0) {
                alert("There are no customers for this email campaign.");
                return false;
            }
            var selPromo = document.getElementById(cboID).value;
            if (selPromo == "-1") {
                alert("Please select a promotion to initiate emails.");
                document.getElementById(cboID).focus();
                return false;
            }
            $("#ContentPlaceHolder1_hdnPkeyEmailCampign").val(pkey_email_campaign);
            $("#ContentPlaceHolder1_hdnInitMails").val("true");
            return true;
        }

        function validate_twoDates() {
            var dateStart = $("#ContentPlaceHolder1_txtFromDate").val();
            var dateEnd = $("#ContentPlaceHolder1_txtToDate").val();
            if (Date.parse(dateStart) > Date.parse(dateEnd)) {
                alert("Invalid date range!");
                return false;
            }
        }
        $(function () {
            $("#ContentPlaceHolder1_txtFromDate").datepicker();
            $("#ContentPlaceHolder1_txtToDate").datepicker();
            $("#frmBpbReports").validate();
        });
    </script>
    <div class="clr"></div>
    <!-- Container Starts -->
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Marketing Reports</li>
            <li>Email Marketing Program</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Email Marketing Program - Customer Statistics</h2>
        <form id="frmBpbReports" name="frmBpbReports" runat="server">
            <table cellpadding="0" cellspacing="0" border="0" width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="15%">Date Range</td>
                        <td width="10%">
                            <asp:DropDownList name="ddlDateRange" ID="ddlDateRange" runat="server" CssClass="slect-box" Width="223px" AutoPostBack="true" OnSelectedIndexChanged="ddlDateRange_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td width="15%" style="padding-left: 15px">From Date</td>
                        <td width="10%">
                            <asp:TextBox CssClass="text-box date" Height="18px" Width="250px" name="txtFromDate" ID="txtFromDate" runat="server"></asp:TextBox></td>
                        <td width="15%" style="padding-left: 15px">To Date</td>
                        <td width="10%">
                            <asp:TextBox CssClass="text-box date" Height="18px" Width="250px" name="txtToDate" ID="txtToDate" runat="server"></asp:TextBox></td>
                        <td width="20%"></td>
                    </tr>
                    <tr>
                        <td>Customer Status</td>
                        <td>
                            <asp:DropDownList name="ddlCustomerstatus" ID="ddlCustomerstatus" runat="server" CssClass="slect-box" Width="223px">
                            </asp:DropDownList>
                        </td>
                        <td colspan="5"></td>
                    </tr>
                    <tr>
                        <td>Kiosk</td>
                        <td>
                            <asp:DropDownList name="ddlKiosk" ID="ddlKiosk" runat="server" CssClass="slect-box" Width="223px">
                            </asp:DropDownList>
                        </td>
                        <td style="padding-left: 15px" colspan="2">
                            <asp:Button runat="server" ID="btnFilter" CssClass="button rounded" Text="Filter" OnClick="btnFilter_Click" OnClientClick="return validate_twoDates();"></asp:Button>
                            <asp:Button runat="server" ID="btnReset" CssClass="button rounded" Text="Reset" OnClick="btnReset_Click" UseSubmitBehavior="false" Visible="false"></asp:Button>
                        </td>
                        <td colspan="3"></td>
                    </tr>
                </tbody>
            </table>           
            <div class="clr"></div>
            <table class="table" width="100%">
                <thead>
                    <tr>
                        <th width="3%"></th>
                        <th>Description</th>
                        <th># Of Customers</th>
                        <th>Promotions</th>
                        <th>Initiate Emails</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="rptrCampaignSummary" runat="server">
                        <ItemTemplate>
                            <tr>
                                <td></td>
                                <td>
                                    <%# Eval("description") %>
                                </td>
                                <td>
                                    <a href="CustomersByCampaign.aspx?ID=<%# Eval("pkey_email_campaign")%>" data-gravity="s" title="Edit"><%# Eval("count") %></a>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlPromotion" runat="server"></asp:DropDownList>
                                    <asp:HiddenField ID="hdnFkeyPromo" runat="server" Value='<%# Eval("fkey_promo") %>' />
                                    <asp:HiddenField ID="hdncount" runat="server" Value='<%# Eval("count") %>' />
                                    <asp:HiddenField ID="hdnPkeycampaign" runat="server" Value='<%# Eval("pkey_email_campaign") %>' />
                                </td>
                                <td>
                                    <asp:Button ID="btnInitiateMails" runat="server" Text="Initiate Mails" CssClass="button rounded" BackColor="#a8a8a8"/>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>

            <asp:HiddenField ID="hdnInitMails" Value="false" runat="server" />
            <asp:HiddenField ID="hdnPkeyEmailCampign" runat="server" />
            <div class="btn_lst">
                <div class="rgt">
                    <asp:Button ID="btnClose" Text="Close" CssClass="button rounded" runat="server" OnClick="btnClose_Click" UseSubmitBehavior="false"></asp:Button>
                    <asp:Button runat="server" ID="btnSavePromotioms" Text="Save Promotions" CssClass="button rounded" OnClick="btnSavePromotioms_Click" UseSubmitBehavior="false"></asp:Button>
                </div>
            </div>
        </form>
    </div>
</asp:Content>
