﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="ExcludeEmailFromReports.aspx.cs" Inherits="Bestprintbuy.Admin.Web.AdminReports.ExcludeEmailFromReports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="clr"></div>
    <!-- Container Starts -->
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Marketing Reports</li>
            <li>Exclude Emails</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Exclude Emails</h2>
        <form id="frmBpbReports" name="frmBpbReports" runat="server">
            <table width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="10%">Email ID</td>
                        <td width="10%">
                            <asp:TextBox CssClass="text-box" Height="18px" Width="200px" name="txtEmailID" ID="txtEmailID" runat="server"></asp:TextBox></td>
                        <td width="10%" style="padding-left: 15px">
                            <asp:Button ID="btnExclude" Text="Exclude" CssClass="button rounded" runat="server" OnClick="btnExclude_Click"></asp:Button>
                        </td>
                        <td width="70%"></td>
                    </tr>
                </tbody>
            </table>
            <div class="clr"></div>
            <table class="table" width="100%">
                <thead>
                    <tr>
                        <th width="6%">#SL</th>
                        <th>Email</th>
                        <th>Customer Name</th>
                        <th>Include</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="rptrExcludeEmails" runat="server">
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <%#Container.ItemIndex+1 %>         
                                </td>
                                <td>
                                    <%#Eval("EMAIL_ID") %>
                                </td>
                                <td>
                                    <%#Eval("CustomerName") %>
                                </td>
                                <td style="text-align: left;">
                                    <asp:LinkButton ID="btnDelete" runat="server" CommandName="Delete" CausesValidation="true"
                                        ToolTip="Delete" CommandArgument='<%#Bind("FKEY_CUSTOMER")%>'
                                        OnClick="btnDelete_Click"><img src="../images/cross.png" /></asp:LinkButton>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
            <div class="btn_lst">
                <div class="rgt">
                    <asp:Button runat="server" CssClass="button rounded" ID="btnClose" Text="Close" OnClick="btnClose_Click"></asp:Button>
                </div>
            </div>
        </form>
    </div>
</asp:Content>
