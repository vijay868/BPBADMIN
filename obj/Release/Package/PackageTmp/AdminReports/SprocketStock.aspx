﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SprocketStock.aspx.cs" Inherits="Bestprintbuy.Admin.Web.AdminReports.SprocketStock" %>

<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->

<!-- Consider adding a manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<head id="Head1" runat="server">
    <title>Best Print Buy</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" media="all" href="../css/bestprintbuy_style.css" />
    <link rel="stylesheet" media="all" href="../css/jquery.fancybox.css" />
    <link rel="stylesheet" href="../css/jquery-ui.css" />
    <script src="../Scripts/jquery.js"></script>
    <script src="../Scripts/jquery-ui.js"></script>
    <script src="../Scripts/common.js"></script>
    <script src="../Scripts/ddsmoothmenu.js"></script>
    <script src="../Scripts/plugin/fancybox/jquery.fancybox.js"></script>
    <script src="../Scripts/jquery.validate.js"></script>
    <!--[if lt IE 9]>
			<script src="../Scripts/html5.js"></script>
		<![endif]-->
    <!--[if lte IE 7]> <html class="ie7"> <![endif]-->
    <!--[if IE 8]>     <html class="ie8"> <![endif]-->
    <!--[if IE 9]>     <html class="ie9"> <![endif]-->
    <!--[if IE 10]>     <html class="ie10"> <![endif]-->
    <!--[if !IE]><!-->

    <!--<![endif]-->
</head>
<body id="Body" runat="server">    
    <script type="text/javascript" src="../Scripts/wz_tooltip.js"></script>
    <!-- Wrapper starts -->
    <div id="Div1" class="popup-wrapper" runat="server">
        <div class="popup-title">
            Sprocket Inventory
        </div>
        <!-- Header Ends -->
        <div class="popup-contnt" style="padding-bottom: 40px">
            <!-- Menu starts -->
            <form id="Form1" runat="server">
                <table class="tablep">
                    <thead>
                        <tr>
                            <th width="3%"></th>
                            <th>SKU Number</th>
                            <th>BPB Template Name</th>
                            <th>Sprocket Description</th>
                            <th>Stock</th>
                            <th>Thumb</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rptrInventory" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Label ID="lblskunumber" runat="server" Text='<%#Eval("skunumber") %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbltemplate_name" runat="server" Text='<%#Eval("template_name") %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lbldescription" runat="server" Text='<%#Eval("description") %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblUnits" runat="server" Text='<%#Eval("units") %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:HyperLink ID="hypImage" runat="server" NavigateUrl='javascript:;'>
                                            <asp:Image ID="imgThumb" ImageUrl='<%# Eval("thumb_url")%>' Height="75" runat="server" />
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
                <div class="btn_lst">
                    <div class="rgt">
                        <asp:Button runat="server" ID="Button1" CssClass="button rounded" Text="Close" OnClientClick="parent.$.fancybox.close(); return false;"></asp:Button>
                    </div>
                </div>
            </form>
            <script lang="javascript">
                function ShowToolTip(imgurl) {
                    Tip('<table cellpadding=1 cellspacing=0 border=1 bgcolor="#333333"><tr><td align=left><img src="' + imgurl + '" width="400"></td></tr></table>',
                        PADDING, 5, BGCOLOR, '#ffffff', BORDERCOLOR, '#333333')
                }
            </script>
        </div>
    </div>
</body>
</html>

