﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CatalogAndSamplesRequestForm.aspx.cs" Inherits="Bestprintbuy.Admin.Web.AdminReports.CatalogAndSamplesRequestForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head id="Head1" runat="server">
    <title>Best Print Buy</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" media="all" href="../css/bestprintbuy_style.css" />
    <link rel="stylesheet" media="all" href="../css/jquery.fancybox.css" />
    <link rel="stylesheet" href="../css/jquery-ui.css" />
    <script src="../Scripts/jquery.js"></script>
    <script src="../Scripts/jquery-ui.js"></script>
    <script src="../Scripts/common.js"></script>
    <script src="../Scripts/ddsmoothmenu.js"></script>
    <script src="../Scripts/plugin/fancybox/jquery.fancybox.js"></script>
    <script src="../Scripts/jquery.validate.js"></script>
    <!--[if lt IE 9]>
			<script src="../Scripts/html5.js"></script>
		<![endif]-->
    <!--[if lte IE 7]> <html class="ie7"> <![endif]-->
    <!--[if IE 8]>     <html class="ie8"> <![endif]-->
    <!--[if IE 9]>     <html class="ie9"> <![endif]-->
    <!--[if IE 10]>     <html class="ie10"> <![endif]-->
    <!--[if !IE]><!-->

    <!--<![endif]-->
</head>
<body id="Body" runat="server">
    <!-- Wrapper starts -->
    <div id="Div1" class="popup-wrapper" runat="server">
        <div class="popup-title">
          Catalog and Samples Request Form
        </div>
        <!-- Header Ends -->
        <div class="popup-contnt">
            <!-- Menu starts -->
            <form id="Form1" runat="server">
                <table>
                    <tr>
                        <td width="40%">
                            <label>First Name</label>
                        </td>
                        <td></td>
                        <td>
                            <asp:TextBox name="txtFirstname" ID="txtFirstname" runat="server" CssClass="text" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Last Name</label>
                        </td>
                        <td></td>
                        <td>
                              <asp:TextBox name="txtLastname" ID="txtLastname" runat="server" CssClass="text" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Email</label>
                        </td>
                        <td></td>
                        <td>
                            <asp:TextBox name="txtEmail" ID="txtEmail" runat="server" CssClass="text" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Company(optional)</label>
                        </td>
                        <td></td>
                        <td>
                            <asp:TextBox name="txtCompany" ID="txtCompany" runat="server" CssClass="text"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Street & Suite</label>
                        </td>
                        <td></td>
                        <td>
                            <asp:TextBox name="txtStreet" ID="txtStreet" runat="server" CssClass="text"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>City</label>
                        </td>
                        <td></td>
                        <td>
                            <asp:TextBox name="txtCity" ID="txtCity" runat="server" CssClass="text"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>State</label>
                        </td>
                        <td></td>
                        <td>
                            <asp:DropDownList name="ddlStates" ID="ddlStates" CssClass="ddlist" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Country</label>
                        </td>
                        <td></td>
                        <td>
                            <asp:DropDownList name="ddlCountry" ID="ddlCountry" CssClass="ddlist" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Zip Code</label>
                        </td>
                        <td></td>
                        <td>
                            <asp:TextBox name="txtZipcode" ID="txtZipcode" runat="server" CssClass="text" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>No of agents in your office</label>
                        </td>
                        <td></td>
                        <td>
                            <asp:TextBox name="txtNoOfAgents" ID="txtNoOfAgents" runat="server" CssClass="text" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Contact Person</label>
                        </td>
                        <td></td>
                        <td>
                            <asp:TextBox name="txtContactPerson" ID="txtContactPerson" runat="server" CssClass="text" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Telephone</label>
                        </td>
                        <td></td>
                        <td>
                            <asp:TextBox name="txtPhone" ID="txtPhone" runat="server" CssClass="text" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="left">
                            <label>
                                Catalog & Samples take approximately 1 week for delivery from the date they have been requested.
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="3">
                            <asp:Button runat="server" ID="btnSave" CssClass="button rounded" Text="Save" OnClick="btnSave_Click"></asp:Button>
                            <asp:Button runat="server" ID="Button1" CssClass="button rounded" Text="Close" OnClientClick="parent.$.fancybox.close(); return false;"></asp:Button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</body>
</html>
