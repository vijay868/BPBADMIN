﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="TransactionReport.aspx.cs" Inherits="Bestprintbuy.Admin.Web.AdminReports.TransactionReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <script type="text/javascript">
         function validate_twoDates() {
             var dateStart = $("#ContentPlaceHolder1_txtFromDate").val();
             var dateEnd = $("#ContentPlaceHolder1_txtToDate").val();
             if (Date.parse(dateStart) > Date.parse(dateEnd)) {
                 alert("Invalid date range!");
                 return false;
             }
         }
         $(function () {
             $("#ContentPlaceHolder1_txtFromDate").datepicker();
             $("#ContentPlaceHolder1_txtToDate").datepicker();
             $("#frmTransactionReports").validate();
         });
    </script>
    <div class="contant_hldr">
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Administrative Reports</li>
            <li>Transaction Report</li>
        </ul>
        <h2>Transaction Report</h2>
        <form class="" runat="server" id="frmTransactionReports">
            <table class="accounts-table" width="70%">
                <tbody>
                    <tr>
                        <td width="15%">From Date</td>
                        <td width="10%">
                            <asp:TextBox name="txtFromDate" ID="txtFromDate" CssClass="text-box date" runat="server"></asp:TextBox>
                        </td>    
                        <td width="10%"></td>                   
                        <td width="5%" > To Date</td>
                        <td width="15%">
                            <asp:TextBox name="txtToDate" ID="txtToDate" CssClass="text-box date" runat="server"></asp:TextBox></td>
                         <td width="45%">
                           
                        </td>
                    </tr>
                    <tr>
                        <td width="10%">Kiosk</td>
                        <td width="10%">
                            <asp:DropDownList name="ddlKiosk" ID="ddlKiosk" CssClass="slect-box" data-placeholder="Choose a Name" runat="server">
                            </asp:DropDownList>
                        </td>
                          <td width="10%"></td>        
                        <td width="5%">
                            <asp:Button runat="server" ID="btnFilter" CssClass="button rounded" Text="Filter" OnClick="btnFilter_Click" OnClientClick="return validate_twoDates();"></asp:Button>
                         </td>                   
                        <td width="15%">
                             <asp:Button runat="server" ID="btnReset" CssClass="button rounded" Text="Reset" UseSubmitBehavior="false" Visible="false" OnClick="btnReset_Click"></asp:Button>
                        </td>
                    </tr>
                </tbody>
            </table>           
            <div class="clr"></div>

            <asp:GridView ID="TransactionReportGrid" runat="server" AutoGenerateColumns="false" EnableViewState="true"
                ForeColor="Black" Width="100%" PageSize="20" AllowPaging="true"  OnPageIndexChanging="TransactionReportGrid_PageIndexChanging"
                class="table" name="TransactionReportGrid">
                <PagerStyle HorizontalAlign="Right" CssClass="dtPage"></PagerStyle>
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />
                <Columns>
                      <asp:TemplateField HeaderText="#SL">
                        <HeaderStyle Width="5%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <span><%# GetRecordCountForPage()%></span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Kiosk">
                        <HeaderStyle Width="35%" ForeColor="White" />
                        <ItemTemplate>
                                <%#Eval("Detail") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Customer Name">
                        <HeaderStyle Width="12%" ForeColor="White" />
                        <ItemTemplate>
                               <%#Eval("Quantity") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Date Created">
                        <HeaderStyle Width="12%" ForeColor="White" />
                        <ItemTemplate>
                               <%#Eval("Product Cost") %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Last Logged In">
                        <HeaderStyle Width="12%" ForeColor="White" />
                        <ItemTemplate>
                               <%#Eval("Shipping Cost") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Phone">
                        <HeaderStyle Width="12%" ForeColor="White" />
                        <ItemTemplate>
                               <%#Eval("Discount") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Email">
                        <HeaderStyle Width="12%" ForeColor="White" />
                        <ItemTemplate>
                              <%#Eval("Total") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                </Columns>
            </asp:GridView>
             <asp:Label ID="lblTotalRecords" runat="server" Font-Bold="true"></asp:Label>           
            <div class="btn_lst" style="padding-top:20px">
                <div class="rgt">
                    <asp:Button runat="server" CssClass="button rounded" ID="btnClose" Text="Close" OnClick="btnClose_Click" UseSubmitBehavior="false"></asp:Button>
                     <asp:Button ID="btnExportTocsv1" CssClass="button rounded" runat="server" Text="Export To CSV" Visible="false"  OnClick="btnExportTocsv_Click"/>
                    
                </div>
            </div>
    
           
        </form>
    </div>

</asp:Content>
