﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="CustomersWithCart.aspx.cs" Inherits="Bestprintbuy.Admin.Web.AdminReports.CustomersWithCart" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
            <script type="text/javascript">
                function validate_twoDates() {
                    var dateStart = $("#ContentPlaceHolder1_txtFromDate").val();
                    var dateEnd = $("#ContentPlaceHolder1_txtToDate").val();
                    if (Date.parse(dateStart) > Date.parse(dateEnd)) {
                        alert("Invalid date range!");
                        return false;
                    }
                }
                $(function () {
                    $("#ContentPlaceHolder1_txtFromDate").datepicker();
                    $("#ContentPlaceHolder1_txtToDate").datepicker();
                    $("#frmCustomersWithCart").validate();
                });
     </script>
 <div class="contant_hldr">
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Marketing Reports</li>
            <li>Customers With Cart</li>
        </ul>
        <h2>Customers With Cart</h2>
        <form class="" runat="server" id="frmCustomersWithCart">
            <table class="accounts-table" width="70%">
                <tbody>
                    <tr>
                         <td width="8%">Date Range</td>
                         <td Width="10%"><asp:DropDownList name="ddlDateRange" ID="ddlDateRange" CssClass="slect-box"  width="220px"  runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlDateRange_SelectedIndexChanged">
                    </asp:DropDownList></td>
                         <td width="3%"></td>    
                        <td width="4%">From Date</td>
                        <td width="5%">
                            <asp:TextBox name="txtFromDate" ID="txtFromDate" CssClass="text-box date" width="250px" runat="server" ></asp:TextBox>
                        </td>    
                                      
                        <td width="4%" style="text-align:center" >To Date</td>
                        <td width="10%">
                            <asp:TextBox name="txtToDate" ID="txtToDate" CssClass="text-box date" width="250px" runat="server" ></asp:TextBox></td>
                         <td width="10%">
                           
                        </td>
                    </tr>
                    <tr>
                        <td width="8%">
                            Customer Status
                        </td>
                        <td>
                            <asp:DropDownList name="ddlCustomerstatus" ID="ddlCustomerstatus" CssClass="slect-box"  width="220px" runat="server">
                    </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td width="8%">Kiosk</td>
                        <td width="10%">
                            <asp:DropDownList name="ddlKiosk" ID="ddlKiosk" CssClass="slect-box"  width="220px" runat="server" >
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                         <td width="8%">Product</td>
                        <td width="10%">
                           <asp:DropDownList name="ddlProduct" ID="ddlProduct" CssClass="slect-box"  width="220px" runat="server" >
                    </asp:DropDownList>
                        </td>
                        <td>
                            
                        </td>
                        <td>
                             <asp:Button runat="server" ID="btnFilter" CssClass="button rounded" Text="Filter" OnClick="btnFilter_Click" OnClientClick="return validate_twoDates();"></asp:Button>
                        </td>
                        <td>
                            <asp:Button runat="server" ID="btnReset" CssClass="button rounded" Text="Reset" Visible="false" OnClick="btnReset_Click" UseSubmitBehavior="false"></asp:Button>
                        </td>
                    </tr>
                </tbody>
            </table>           
            <div class="clr"></div>           
       
            <asp:GridView ID="CustomersListGrid" runat="server" AutoGenerateColumns="false" EnableViewState="true"
                ForeColor="Black" Width="100%" PageSize="20" AllowPaging="true"  OnPageIndexChanging="CustomersListGrid_PageIndexChanging"
                class="table" name="CustomersListGrid">
                <PagerStyle HorizontalAlign="Right" CssClass="dtPage"></PagerStyle>
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />
                <Columns>
                      <asp:TemplateField HeaderText="#SL" ItemStyle-Width="5%">
                        <HeaderStyle Width="5%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <span><%# GetRecordCountForPage()%></span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Kiosk" ItemStyle-Width="9%">
                        <HeaderStyle Width="9%" ForeColor="White" />
                        <ItemTemplate>
                             <%#Eval("Kiosk") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Customer Name" ItemStyle-Width="10%">
                        <HeaderStyle Width="10%" ForeColor="White" />
                        <ItemTemplate>
                             <%#Eval("customername") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Date Created" ItemStyle-Width="12%">
                        <HeaderStyle Width="12%" ForeColor="White" />
                        <ItemTemplate>
                             <%#Eval("date_created") %>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Last Logged In" ItemStyle-Width="12%">
                        <HeaderStyle Width="12%" ForeColor="White" />
                        <ItemTemplate>
                              <%#Eval("lastloggedin") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Phone" ItemStyle-Width="9%">
                        <HeaderStyle Width="9%" ForeColor="White" />
                        <ItemTemplate>
                            <%#Eval("phone") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Email" ItemStyle-Width="10%">
                        <HeaderStyle Width="10%" ForeColor="White" />
                        <ItemTemplate>
                            <%#Eval("EMAIL_ID") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Product" ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center">
                        <HeaderStyle Width="5%" ForeColor="White" />
                        <ItemTemplate>
                             <%#Eval("product") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Qty" ItemStyle-Width="5%">
                        <HeaderStyle Width="5%" ForeColor="White" />
                        <ItemTemplate>
                            <%#Eval("quantity") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Customer Status" ItemStyle-Width="10%">
                        <HeaderStyle Width="10%" ForeColor="White" />
                        <ItemTemplate>
                            <%#Eval("customerstatus") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Ship Option" ItemStyle-Width="15%">
                        <HeaderStyle Width="15%" ForeColor="White" />
                        <ItemTemplate>
                           <%#Eval("shipoption") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <asp:Label ID="lblTotalRecords" runat="server" Font-Bold="true"></asp:Label>
            <div class="btn_lst" style="padding-top:20px">
                <div class="rgt">
      
                    <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClick="btnClose_Click" UseSubmitBehavior="false"></asp:Button>
                      <asp:Button ID="btnExportTocsv1" CssClass="button rounded" runat="server" Text="Export To CSV" Visible="false"  OnClick="btnExportTocsv_Click"/>
                    
                </div>
            </div>            
        </form>
    </div>
</asp:Content>
