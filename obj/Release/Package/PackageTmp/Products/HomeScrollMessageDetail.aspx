﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="HomeScrollMessageDetail.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Products.HomeScrollMessageDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript">
        function validate_twoDates() {
            var dateStart = $("#ContentPlaceHolder1_txtFromDate").val();
            var dateEnd = $("#ContentPlaceHolder1_txtToDate").val();
            if (Date.parse(dateStart) > Date.parse(dateEnd)) {
                alert("Invalid date range!");
                return false;
            }
        }
        $(function () {
            $("#ContentPlaceHolder1_txtFromDate").datepicker();
            $("#ContentPlaceHolder1_txtToDate").datepicker();
        });
        $().ready(function () {
            // validate the comment form when it is submitted
            $("#frmHomeScrollMessage").validate();
        });
    </script>
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Product Setup & Configuration</li>
            <li><a href="HomeScrollMessages.aspx">Home Page Scrolling</a></li>
            <li>Home Page Scrolling Messages Details</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Home Scroll Message Details</h2>
        <form class="" runat="server" id="frmHomeScrollMessage">
            <div class="form_sep_left" style="float: right; padding-right: 47%">

                <table class="accounts-table">
                    <tr>
                        <td class="a_lft" style="vertical-align:top"> <label> Message Description*</label></td>
                        <td style="width: 10px"></td>
                        <td >
                            <asp:TextBox name="txtMessage" ID="txtMessage" runat="server" style="resize:none;" TextMode="MultiLine" Rows="8" Columns="70" required></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="a_lft"> <label>From Date*</label></td>
                        <td></td>
                        <td class="a_lft">
                            <asp:TextBox CssClass="text-box date" Height="18px" Width="200px" name="txtFromDate" ID="txtFromDate" runat="server" required></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="a_lft"> <label>To Date*</label></td>
                        <td></td>
                        <td class="a_lft">
                            <asp:TextBox CssClass="text-box date" Height="18px" Width="200px" name="txtToDate" ID="txtToDate" runat="server" required></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="a_lft"></td>
                        <td></td>
                        <td class="a_lft">
                            <asp:CheckBox ID="chkIsActive" runat="server" />
                            IsActive
                        </td>
                    </tr>

                </table>
            </div>
            <div class="btn_lst">
                <div class="rgt" style="padding-right: 46%;">
                    <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" UseSubmitBehavior="false" OnClick="btnClose_Click"></asp:Button>
                    <asp:Button runat="server" ID="btnSave" CssClass="button rounded" Text="Save" OnClick="btnSave_Click" OnClientClick="return validate_twoDates();"></asp:Button>
                </div>
            </div>
        </form>
    </div>
</asp:Content>
