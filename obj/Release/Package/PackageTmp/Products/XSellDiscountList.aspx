﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="XSellDiscountList.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Products.XSellDiscountList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <div class="clr"></div>
    <!-- Container Starts -->
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Product Setup & Configuration</li>
            <li>Cross Sell Discount List</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Cross Sell Discount List</h2>
        <form id="frmCrossSellDiscountList" name="frmCrossSellDiscountList" runat="server">           
            <div class="clr"></div>
            <asp:GridView ID="CrossSellDiscountListGrid" runat="server" AutoGenerateColumns="false" EnableViewState="true"
                ForeColor="Black" Width="100%" PageSize="20" AllowPaging="true" AllowSorting="true"
                class="table" name="CrossSellDiscountListGrid" OnPageIndexChanging="CrossSellDiscountListGrid_PageIndexChanging">
                <PagerStyle HorizontalAlign="Right" CssClass="dtPage"></PagerStyle>
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />
                <Columns>
                     <asp:TemplateField HeaderText="#SL">
                        <HeaderStyle Width="6%"></HeaderStyle>
                        <ItemTemplate>
                            <span><%# GetRecordCountForPage()%></span>
                              </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Product Type">
                        <HeaderStyle Width="30%"/>
                        <ItemTemplate>
                            <a href="XSellDiscountDetails.aspx?PkeyXsellmaster=<%#DataBinder.Eval(Container,"DataItem.pkey_xsell_discount_master") %>" title="Edit"> <%#DataBinder.Eval(Container,"DataItem.ct_name") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Rule">
                        <HeaderStyle Width="17%"/>
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container,"DataItem.rule_type") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Product Discount">
                        <HeaderStyle Width="20%" />
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container,"DataItem.discount").ToString().Replace(".00","%") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Ship Discount">
                        <HeaderStyle Width="17%" />
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container,"DataItem.ship_discount").ToString().Replace(".00","%") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="IsActive">
                        <HeaderStyle Width="10%" />
                        <ItemTemplate>
                             <%# DataBinder.Eval(Container,"DataItem.is_active") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <!-- table Ends -->
            <div class="btn_lst">
                <asp:Button ID="btnAddXsellDiscount" Text="Add Cross Sell Discount" CssClass="button rounded" runat="server" OnClick="btnAddXsellDiscount_Click"></asp:Button>
                <div class="rgt">
                    <asp:Button ID="btnClose" Text="Close" CssClass="button rounded" runat="server" OnClick="btnClose_Click"></asp:Button>
                </div>
            </div>
        </form>
    </div>
</asp:Content>