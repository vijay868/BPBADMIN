﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="NewAgentPackageDetail.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Products.NewAgentPackageDetail" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <script type="text/javascript">
         $().ready(function () {
             $("#frmNewAgentPackageDetail").validate();
         });

         bkLib.onDomLoaded(function () {
             new nicEditor({ fullPanel: true }).panelInstance('ContentPlaceHolder1_txtDiscountNote');
         });

         function saveagentPackege() {
             if ('<%=HasAccess("ProductDetail", "edit")%>' == 'False') {
                alert('User does not have access.');
                return false;
            }
            return true;
        }
    </script>
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Product Setup & Configuration</li>
            <li><a href="NewAgentPackageList.aspx">New Agent Package</a></li>
            <li>New Agent Package Details</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>New Agent Package Details</h2>
        <form id="frmNewAgentPackageDetail" name="frmNewAgentPackageDetail" runat="server">
            <table width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="15%">
                            <label>Package</label></td>
                        <td width="20%">
                            <asp:TextBox CssClass="text-box" name="txtPackage" ID="txtPackage" runat="server"></asp:TextBox>
                        </td>
                        <td width="15%" rowspan="2">
                            <label>Package Description</label>
                        </td>
                        <td width="35%" rowspan="2">
                            <asp:TextBox CssClass="text-box1" name="txtDescription" ID="txtDescription" style="resize:none;" TextMode="MultiLine" runat="server" Rows="5" Columns="54"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                        </td>
                        <td>
                            <asp:CheckBox ID="chkActive" CssClass="check-box" runat="server" /><asp:Label ID="Label11" runat="server" Text="   Active"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Business Cards </label>
                        </td>
                        <td>
                            <asp:DropDownList name="ddlBC" ID="ddlBC" CssClass="search" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <label>Envelopes</label>
                        </td>
                        <td>
                            <asp:DropDownList name="ddlEN" ID="ddlEN" CssClass="search" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Letterheads</label>
                        </td>
                        <td>
                            <asp:DropDownList name="ddlLH" ID="ddlLH" CssClass="search" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <label>Folded Note Cards</label>
                        </td>
                        <td>
                            <asp:DropDownList name="ddlFC" ID="ddlFC" CssClass="search" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Actual Price*</label></td>
                        <td>
                            <asp:TextBox CssClass="text-box number" name="txtActualPrice" ID="txtActualPrice" runat="server" required></asp:TextBox>
                        </td>
                        <td>
                            <label>Discount Price*</label></td>
                        <td>
                            <asp:TextBox CssClass="text-box number" name="txtDiscountPrice" ID="txtDiscountPrice" runat="server" required></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Srvice Credit Amount*</label></td>
                        <td>
                            <asp:TextBox CssClass="text-box number" name="txtServiceCrAmt" ID="txtServiceCrAmt" runat="server" required></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <label>Discount Note</label></td>
                        <td colspan="3">
                            <asp:TextBox name="txtDiscountNote" ID="txtDiscountNote" CssClass="nic" Rows="8" Columns="100" runat="server" TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                </tbody>
            </table>

            <table width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="50%">&nbsp;</td>
                        <td width="35%" style="float: right;">
                            <asp:Button runat="server" CssClass="button rounded" ID="btnSave" Text="Save" OnClientClick="return saveagentPackege();" OnClick="btnSave_Click"></asp:Button>
                            <asp:Button runat="server" CssClass="button rounded" ID="btnClose" Text="Close" UseSubmitBehavior="false" OnClick="btnClose_Click"></asp:Button>
                        </td>
                    </tr>
                </tbody>
            </table>
            <!-- table Ends -->
        </form>
    </div>
</asp:Content>
