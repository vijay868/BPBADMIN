﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="SetShipOptions.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Products.SetShipOptions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Product Setup & Configuration</li>
            <li>Shipping and Turn around</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Set Shipping Options   </h2>
        <form class="" runat="server" id="frmSetShipoptions">
            <div class="form_sep_left" style="float: left; padding-left: 5%">
                <table class="accounts-table">
                    <tr>
                        <td class="a_lft">Express Shipping & Handling - (one day)</td>
                        <td style="width: 10px"></td>
                        <td>
                            <%--1px solid #a8a8a8;--%>
                            <asp:CheckBox BorderWidth="0px" CssClass="chk" BorderStyle="Solid" BorderColor="#a8a8a8" ID="chkExpressShip" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="a_lft">Speed Shipping & Handling - (two days)</td>
                        <td></td>
                        <td class="a_lft">
                            <asp:CheckBox BorderWidth="0px" CssClass="chk" BorderStyle="Solid" BorderColor="#a8a8a8" ID="chkSpeed" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
            <h2 style="padding-top: 10%">Set Turnaround Options  </h2>
                <table class="table" width="100%">
                    <thead>
                        <tr>
                            <th>Turnaround Time</th>
                            <th>Days</th>
                            <th>Default</th>
                            <th>TA Premium</th>
                            <th>Base Price</th>
                            <th>% on Base</th>
                            <th>IsActive</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rptrTurnAroundOptions" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <asp:TextBox   runat="server" CssClass="text textbox" ID="txtTATime" Text='<%#Eval("turnaround_time") %>'>  </asp:TextBox>
                                        <asp:HiddenField ID="hdnpkeyTAtime" runat="server" Value='<%#Eval("pkey_turnaround_time") %>' />
                                    </td>
                                    <td>
                                        <%#Eval("days") %>
                                    </td>
                                    <td>
                                        <%#Eval("is_default").ToString().Trim() == "True" ? "Y" : "N"%>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" CssClass="text textbox" ID="txtCost" Text='<%#Eval("turnaround_cost") %>'>  </asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" CssClass="text textbox" ID="txtBasePrice" Text='<%# Eval("baseprice") %>'></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" CssClass="text textbox" ID="txtOffsetPrecent" Text=' <%# Eval("offsetpercent") %>'></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkActive" BorderWidth="0px" CssClass="check-box" runat="server" Checked='<%#Eval("isactive")%>' Enabled='<%# Eval("is_default").ToString().Trim() == "True" ? false : true%>' />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            <div class="btn_lst">
                <div class="rgt">
                    <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClick="btnClose_Click"></asp:Button>
                    <asp:Button runat="server" ID="btnSave" CssClass="button rounded" Text="Save" OnClick="btnSave_Click"></asp:Button>
                </div>
            </div>
        </form>
    </div>
</asp:Content>
