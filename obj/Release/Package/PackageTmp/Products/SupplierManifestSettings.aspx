﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="SupplierManifestSettings.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Products.SupplierManifestSettings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function saveManifest() {
            if ('<%=HasAccess("ProductDetail", "edit")%>' == 'False') {
                 alert('User does not have access.');
                 return false;
             }
             return true;
         }
    </script>
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Product Setup & Configuration</li>
            <li>Manifest - Product Code Translation</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Manifest - Product Code Translation</h2>
        <form class="form" runat="server" id="frmOrderFulfillmentList">
            <table cellpadding="0" cellspacing="0" border="0" width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="7%">Product </td>
                        <td width="15%">
                            <asp:DropDownList name="ddlProducts" ID="ddlProducts" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlProducts_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td width="70%"></td>
                    </tr>
                </tbody>
            </table>           
            <div class="clr"></div>

            <asp:GridView ID="ManifestListGrid" runat="server" AutoGenerateColumns="false" EnableViewState="true"
                ForeColor="Black" Width="100%" class="table" name="ManifestListGrid">
                <Columns>
                    <asp:TemplateField HeaderText="#SL">
                        <HeaderStyle Width="9%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <span><%# GetRecordCountForPage()%></span>
                            <asp:HiddenField ID="hdnpkeyProductTaXref" runat="server" Value='<%#Eval("pkey_product_ta_xref") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Product">
                        <HeaderStyle Width="15%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate><%# DataBinder.Eval(Container, "DataItem.ct_name") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Code">
                        <HeaderStyle Width="8%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate><%# DataBinder.Eval(Container, "DataItem.cards_for") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="SM Product Code">
                        <HeaderStyle Width="15%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:TextBox Style="width: 140px;" name="txtCardsForSm" ID="txtCardsForSm" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.cards_for_sm") %>' type="text"></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Quantity">
                        <HeaderStyle Width="8%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate><%# DataBinder.Eval(Container, "DataItem.quantity") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="BPB TAT">
                        <HeaderStyle Width="15%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate><%# DataBinder.Eval(Container, "DataItem.turnaround_time") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="SM TAT">
                        <HeaderStyle Width="15%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:TextBox Style="width: 140px;" name="txtSaDays" ID="txtSaDays" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.sa_days") %>' type="text"></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="SM TATS">
                        <HeaderStyle Width="15%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <asp:TextBox Style="width: 140px;" name="txtTaSaCode" ID="txtTaSaCode" Text='<%# DataBinder.Eval(Container, "DataItem.ta_sa_code") %>' type="text" runat="server"></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <div class="btn_lst">
                <div class="rgt">                    
                    <asp:Button ID="btnClose" Text="Close" CssClass="button rounded" runat="server" OnClick="btnClose_Click"></asp:Button>
                    <asp:Button runat="server" ID="btnSaveProductTranslation" CssClass="button rounded" Text="Save Product Translation Settings" OnClientClick="return saveManifest();" OnClick="btnSaveProductTranslation_Click"></asp:Button>                
                </div>
            </div>
        </form>
    </div>
</asp:Content>
