﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="NewAgentPackageFC.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Products.NewAgentPackageFC" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        var mCustomerPortalURL = "<%= mCustomerPortalURL%>";
    </script>
    <div class="clr"></div>
    <!-- Container Starts -->
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Product Setup & Configuration</li>
            <li><a href="NewAgentPackageList.aspx">New Agent Package</a></li>
            <li>New Agent Package Folded Cards</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>New Agent Package Folded Cards</h2>
        <form id="frmNewAgentPackageFC" name="frmNewAgentPackageFC" runat="server">
            <table width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="15%">
                            <label>Kiosk</label></td>
                        <td width="20%">
                            <asp:DropDownList name="ddlKiosk" ID="ddlKiosk" CssClass="slect-box" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlKiosk_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td width="15%">
                            <label>Template Type</label>
                        </td>
                        <td width="35%">
                            <asp:DropDownList name="ddlTemplateType" ID="ddlTemplateType" CssClass="slect-box" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlTemplateType_SelectedIndexChanged">
                                <asp:ListItem Value="960" Selected>Announcing</asp:ListItem>
                                <asp:ListItem Value="961">General</asp:ListItem>
                                <asp:ListItem Value="662">Just a Note</asp:ListItem>
                                <asp:ListItem Value="822">Personal Promotion</asp:ListItem>
                                <asp:ListItem Value="532">Thank You</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4"></td>
                    </tr>
                </tbody>
            </table>
            <table wid="100%" class="accounts-table">
                <tr>
                    <td width="10%">&nbsp;</td>
                    <td width="80%" colspan="2">
                        <asp:DataList runat="server" ID="dtlAgentPackageFC" RepeatColumns="5" RepeatDirection="Horizontal">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td style="padding: 15px 30px 0px 30px;">
                                            <img src="<%# mCustomerPortalURL+Eval("thumbnail_url") %>" id="imgThumbnailUrl" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0px 30px 5px 30px;">
                                            <asp:CheckBox runat="server" ID="chkTemplate" CssClass="check-box"></asp:CheckBox>&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblTemplateName" runat="server" Text='<%#Eval("template_name")%>'></asp:Label>
                                            <asp:HiddenField ID="hdnPkeyTemplateMaster" Value='<%#Eval("pkey_template_master")%>' runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:DataList>
                    </td>
                    <td width="10%">&nbsp;
                    </td>
                </tr>
            </table>
            <table width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="50%">&nbsp;</td>
                        <td width="35%" style="float: right;">
                            <asp:Button runat="server" CssClass="button rounded" ID="btnSave" Text="Save" OnClick="btnSave_Click"></asp:Button>
                            <asp:Button runat="server" CssClass="button rounded" ID="btnClose" Text="Close" OnClick="btnClose_Click"></asp:Button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
</asp:Content>
