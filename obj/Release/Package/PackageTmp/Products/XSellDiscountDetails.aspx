﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="XSellDiscountDetails.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Products.XSellDiscountDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
        $().ready(function () {
            $("#frmCrossSellDiscountDetails").validate();
        });

        $(function () {
            $("#ContentPlaceHolder1_txtFromDate").datepicker();
            $("#ContentPlaceHolder1_txtToDate").datepicker();
        });
    </script>
    <script type="text/javascript">
        function moveShipmentsOptions(option, moveAll) {
            fromObject = document.getElementById("ContentPlaceHolder1_lbAvailableShipments");
            toObject = document.getElementById("ContentPlaceHolder1_lbAssignedShipments");

            if (option == "right") {
                from = eval(fromObject);
                to = eval(toObject);
            }
            else {
                from = eval(toObject);
                to = eval(fromObject);
            }

            if (from && to) {
                var fromSize = from.options.length;
                var toSize = to.options.length;
                var fromArray = new Array();
                var fromSelectedArray = new Array();
                var fromSelectedIndex = 0;
                var fromIndex = 0;
                for (i = fromSize - 1; i > -1; i--) {
                    var isSelected = false
                    if (from.options[i].selected)
                        isSelected = from.options[i].selected;
                    if ((isSelected && isSelected == true) || moveAll == true) {
                        fromSelectedArray[fromSelectedIndex] = new Array(from.options[i].value, from.options[i].text);
                        from.options[i] = null;
                        fromSelectedIndex++;
                    }
                    else {
                        fromArray[fromIndex] = new Array(from.options[i].value, from.options[i].text);
                        from.options[i] = null;
                        fromIndex++;
                    }
                }
                fromSize = fromArray.length;
                for (i = 0; i < fromSize; i++) {
                    from.options[i] = new Option(fromArray[fromSize - 1 - i][1], fromArray[fromSize - 1 - i][0]);
                }
                var newToSize = fromSelectedArray.length;
                var strData = "";
                for (i = 0; i < newToSize; i++) {
                    toSize = to.options.length;
                    to.options[toSize] = new Option(fromSelectedArray[i][1], fromSelectedArray[i][0]);
                    strData = strData + to.options[toSize].value;
                    if (i != newToSize - 1) strData = strData + ",";
                }
            }
        }

        function validate_twoDates() {
            var dateStart = $("#ContentPlaceHolder1_txtFromDate").val();
            var dateEnd = $("#ContentPlaceHolder1_txtToDate").val();
            if (Date.parse(dateStart) > Date.parse(dateEnd)) {
                alert("Invalid date range!");
                return false;
            }
            else {
                if (document.getElementById('ContentPlaceHolder1_lbAssignedShipments')) {
                    var leng = document.getElementById('ContentPlaceHolder1_lbAssignedShipments').options.length;
                    for (j = leng - 1; j > -1; j--) {
                        document.getElementById('ContentPlaceHolder1_lbAssignedShipments').options[j].selected = true;
                    }
                }
            }
            if ($("#ContentPlaceHolder1_rbtnProdPercentage").is(':checked') && eval($("#ContentPlaceHolder1_txtProductDiscount").val()) > 100) {              
                    alert("Product Discount Amount cannot exceed 100.");
                    return false;
            }

            if ($("#ContentPlaceHolder1_rbtnShipPercentage").is(':checked') && eval($("#ContentPlaceHolder1_txtShipmentDiscount").val()) > 100) {
                alert("Shipment Discount Amount cannot exceed 100.");
                return false;
            }
            var option = $("#ContentPlaceHolder1_ddlOfferedOn option:selected").val();
            if (option == "D" && $('#ContentPlaceHolder1_txtShipmentDiscount').val() != "") {
                alert('Shipment Discount Amount is not applicable for the selected Offer.');
                return false;
            }
            if (($("#ContentPlaceHolder1_lbAssignedShipments option:selected").val() == undefined ||
                 $("#ContentPlaceHolder1_lbAssignedShipments option:selected").val() == "") && option == "B") {
                alert("Atleast one Shipment must be selected.");
                return false;
            }
        }

        function SetShipDiscount() {
            var option = $("#ContentPlaceHolder1_ddlOfferedOn option:selected").val();
            if (option == "B") {
                $('#ContentPlaceHolder1_txtShipmentDiscount').attr('required', '');
                $('#ContentPlaceHolder1_txtProductDiscount').attr('required', '');
            }
            else {
                $('#ContentPlaceHolder1_txtShipmentDiscount').removeAttr('required');
                $('#ContentPlaceHolder1_txtProductDiscount').attr('required', '');
            }
        }
    </script>
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Product Setup & Configuration</li>
            <li><a href="XSellDiscountList.aspx">Cross Sell Discount List</a></li>
            <li>Cross Sell Discount Details</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Cross Sell Discount Details</h2>
        <form id="frmCrossSellDiscountDetails" name="frmCrossSellDiscountDetails" runat="server">
            <table width="100%" class="accounts-table">
                <thead>
                    <tr>
                        <th colspan="4" width="100%">Cross Sell Discount Definition</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="15%">
                            <label>Offerred On*</label></td>
                        <td width="20%">
                            <asp:DropDownList name="ddlOfferedOn" ID="ddlOfferedOn" CssClass="slect-box" runat="server" onchange="SetShipDiscount(); return false;" required>
                                <asp:ListItem Value="D">Only Product Cost</asp:ListItem>
                                <asp:ListItem Value="B">Product & Shipping</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td width="15%">&nbsp;
                        </td>
                        <td width="35%">&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Product Discount Type*</label></td>
                        <td>
                            <asp:RadioButton ID="rbtnProdPercentage" runat="server" GroupName="productDiscounttype" /><asp:Label ID="Label3" runat="server" Text="  Percentage"></asp:Label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:RadioButton ID="rbtnProdFixed" runat="server" GroupName="productDiscounttype" /><asp:Label ID="Label4" runat="server" Text="  Fixed"></asp:Label>
                        </td>
                        <td>
                            <label>Shipment Discount Type*</label></td>
                        <td>
                            <asp:RadioButton ID="rbtnShipPercentage" runat="server" GroupName="ShipmentDiscounttype" /><asp:Label ID="Label1" runat="server" Text="  Percentage"></asp:Label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:RadioButton ID="rbtnShipFixed" runat="server" GroupName="ShipmentDiscounttype" /><asp:Label ID="Label2" runat="server" Text="  Fixed"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Product Discount* </label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box number" name="txtProductDiscount" ID="txtProductDiscount" runat="server" required></asp:TextBox>
                        </td>
                        <td>
                            <label>Shipment Discount*</label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box number" name="txtShipmentDiscount" ID="txtShipmentDiscount" runat="server" required></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>From Date* </label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box date" name="txtFromDate" ID="txtFromDate" runat="server" required></asp:TextBox>
                        </td>
                        <td>
                            <label>To Date*</label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box date" name="txtToDate" ID="txtToDate" runat="server" required></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <asp:CheckBox ID="chkActive" CssClass="check-box" runat="server" /><asp:Label ID="Label11" runat="server" Text="   Active"></asp:Label>
                        </td>
                        <td>
                            <label>Product Type</label></td>
                        <td>
                            <asp:DropDownList name="ddlProductType" ID="ddlProductType" CssClass="slect-box" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </tbody>
            </table>

            <table width="100%" class="accounts-table">
                <thead>
                    <tr>
                        <th colspan="4" width="100%">Apply Shipping Options</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="15%">
                            <label></label>
                        </td>
                        <td width="50%">
                            <div style="width: 40%; float: left">
                                <label style="padding-left: 0px;"><b>Available Shipment</b></label>
                                <asp:ListBox ID="lbAvailableShipments" runat="server" CssClass="list-box" SelectionMode="Multiple"></asp:ListBox>
                            </div>
                            <div style="width: 10%; float: left; padding: 40px 10px 0 10px;">
                                <button id="Button1" type="button" class="button rounded width53" onclick="javascript:moveShipmentsOptions('right',false)">
                                    &nbsp;&gt;&nbsp;</button><br />
                                <button id="Button2" type="button" class="button rounded width53" onclick="javascript:moveShipmentsOptions('right',true)">
                                    &nbsp;&gt;&gt;&nbsp;</button><br />
                                <button id="Button3" type="button" class="button rounded width53" onclick="javascript:moveShipmentsOptions('left',false)">
                                    &nbsp;&lt;&nbsp;</button><br />
                                <button id="Button4" type="button" class="button rounded width53" onclick="javascript:moveShipmentsOptions('left',true)">
                                    &nbsp;&lt;&lt;&nbsp;</button>
                            </div>
                            <div style="width: 35%; float: left; padding-right: 10px;">
                                <label style="padding-left: 0px;"><b>Eligible Shipment</b></label>
                                <asp:ListBox ID="lbAssignedShipments" runat="server" CssClass="list-box" SelectionMode="Multiple"></asp:ListBox>
                            </div>
                        </td>
                        <td width="35%"></td>
                    </tr>
                </tbody>
            </table>

            <table width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="50%">&nbsp;</td>
                        <td width="35%" style="float: right;">
                            <asp:Button runat="server" CssClass="button rounded" ID="btnSave" Text="Save" OnClientClick="return validate_twoDates();" OnClick="btnSave_Click"></asp:Button>
                            <asp:Button runat="server" CssClass="button rounded" ID="btnClose" Text="Close" UseSubmitBehavior="false" OnClick="btnClose_Click"></asp:Button>
                        </td>
                    </tr>
                </tbody>
            </table>
            <!-- table Ends -->
        </form>
        <script>
            SetShipDiscount();
        </script>
    </div>
</asp:Content>
