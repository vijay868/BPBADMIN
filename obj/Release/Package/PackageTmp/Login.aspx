﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Best Print Buy</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" media="all" href="css/bestprintbuy_style.css" />
    <link rel="stylesheet" media="all" href="css/ddsmoothmenu.css" />
    <link rel="stylesheet" media="all" href="css/Style.css" />
	<link rel="stylesheet" media="all" href="css/jquery.fancybox.css" />
    <script src="Scripts/jquery.js"></script>
    <script src="Scripts/common.js"></script>
    <script src="Scripts/ddsmoothmenu.js"></script>
	<script src="Scripts/plugin/fancybox/jquery.fancybox.js"></script>
    <!--[if lt IE 9]>
			<script src="Scripts/html5.js"></script>
		<![endif]-->
    <!--[if lte IE 7]> <html class="ie7"> <![endif]-->
    <!--[if IE 8]>     <html class="ie8"> <![endif]-->
    <!--[if IE 9]>     <html class="ie9"> <![endif]-->
    <!--[if IE 10]>     <html class="ie10"> <![endif]-->
    <!--[if !IE]><!-->
    <!--<![endif]-->
        <script>
            $().ready(function () {
                if ($("#txtUserName").val()=="")
                    $("#txtUserName").focus();
                else
                    $("#txtPassword").focus();
            });
            function SessionTimeoutOnPopup() {
                try {
                    
                    var querystring = location.search.replace('?', '').split('&');
                    // declare object
                    var queryObj = {};
                    // loop through each name-value pair and populate object
                    for (var i = 0; i < querystring.length; i++) {
                        // get name and value
                        var name = querystring[i].split('=')[0];
                        var value = querystring[i].split('=')[1];
                        // populate object
                        queryObj[name] = value;
                    }
                    // ***now you can use queryObj["<name>"] to get the value of a url
                    // ***variable
                    //alert(queryObj["sessiontimeoutfrompopup"]);
                    if (queryObj["sessiontimeoutfrompopup"] === "true") {
                        window.top.location.href = "Login.aspx";
                    }
                }
                catch (e) { }
            }
            SessionTimeoutOnPopup();
    </script>
</head>
<body style="overflow:hidden">
    <div id="wrapper" >
        <div class="contant_hldr loginalign">
            <form id="frmLogin" class="form" runat="server">
                <div class="login_container rounded">
                    <div class="form_login">
                        <dl>
                            <dt>Username </dt>
                            <dd>
                                <asp:TextBox runat="server" ID="txtUserName" CssClass="logincontrols"></asp:TextBox>
                            </dd>
                        </dl>
                        <dl>
                            <dt>Password </dt>
                            <dd>
                                <asp:TextBox runat="server" TextMode="Password" ID="txtPassword" CssClass="logincontrols"></asp:TextBox>
                            </dd>
                        </dl>
                        <dl>
                            <dt></dt>
                            <dd>
                                <asp:Button Text="Sign In" ID="btnSubmit" OnClick="btnSubmit_Click" CssClass="button rounded" runat="server" /></dd>
                        </dl>
                    </div>
                </div>
            </form>
        </div>
    </div>
</body>
</html>
