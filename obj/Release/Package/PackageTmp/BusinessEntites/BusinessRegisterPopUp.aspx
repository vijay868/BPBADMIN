﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BusinessRegisterPopUp.aspx.cs" Inherits="Bestprintbuy.Admin.Web.BusinessEntites.BusinessRegisterPopUp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Best Print Buy</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" media="all" href="../css/bestprintbuy_style.css" />
    <link rel="stylesheet" media="all" href="../css/jquery.fancybox.css" />
    <script src="../Scripts/jquery.js"></script>
    <script src="../Scripts/common.js"></script>
    <script src="../Scripts/ddsmoothmenu.js"></script>
    <script src="../Scripts/plugin/fancybox/jquery.fancybox.js"></script>
    <!--[if lt IE 9]>
			<script src="../Scripts/html5.js"></script>
		<![endif]-->
    <!--[if lte IE 7]> <html class="ie7"> <![endif]-->
    <!--[if IE 8]>     <html class="ie8"> <![endif]-->
    <!--[if IE 9]>     <html class="ie9"> <![endif]-->
    <!--[if IE 10]>     <html class="ie10"> <![endif]-->
    <!--[if !IE]><!-->
    <!--<![endif]-->
</head>
<body id="Body" runat="server">
    <!-- Wrapper starts -->
    <div id="Div1" class="popup-wrapper" runat="server">
        <div class="popup-title">
            Supplier Account Summary
        </div>
        <!-- Header Ends -->
        <div class="popup-contnt">
            <!-- Menu starts -->
            <form id="Form1" runat="server">
                <table>
                    <tr>
                        <td width="30%">
                            <label>Business ID</label></td>
                        <td>
                            <asp:Label ID="lblBusinessID" runat="server" Font-Size="12px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Prefix</label></td>
                        <td>
                            <asp:Label ID="lblPrefix" runat="server" Font-Size="12px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Name</label></td>
                        <td>
                            <asp:Label ID="lblName" runat="server" Font-Size="12px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Other Names</label></td>
                        <td>
                            <asp:Label ID="lblOtherNames" runat="server" Font-Size="12px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Salutation</label></td>
                        <td>
                            <asp:RadioButton ID="rbtnBusiness" runat="server" value="optBusiness" Enabled="false" Text="Business" GroupName="Business" />
                            <asp:RadioButton ID="rbtnIndividual" value="optIndividual" runat="server" Enabled="false" Text="Individual" GroupName="Business" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label></label>
                        </td>
                        <td>
                            <asp:CheckBox ID="chkPrincContact" runat="server" Text=" Principal Contact" Enabled="false" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Address</label></td>
                        <td>
                            <asp:Label ID="lblAddress1" runat="server" Font-Size="12px"></asp:Label><br />
                            <br />
                            <asp:Label ID="lblAddress2" runat="server" Font-Size="12px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>City</label></td>
                        <td>
                            <asp:Label ID="lblCity" runat="server" Font-Size="12px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Country</label></td>
                        <td>
                            <asp:Label ID="lblCountry" runat="server" Font-Size="12px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>State</label></td>
                        <td>
                            <asp:Label ID="lblState" runat="server" Font-Size="12px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Postal Code</label></td>
                        <td>
                            <asp:Label ID="lblPostalCode" runat="server" Font-Size="12px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Category</label></td>
                        <td>
                            <asp:Label ID="lblCategory" runat="server" Font-Size="12px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Type</label></td>
                        <td>
                            <asp:Label ID="lblType" runat="server" Font-Size="12px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Tax ID</label></td>
                        <td>
                            <asp:Label ID="lblTaxID" runat="server" Font-Size="12px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Tax Type</label></td>
                        <td>
                            <asp:RadioButton ID="rbtnBusinessTaxType" runat="server" value="optBusinessTax" Enabled="false" Text="Business" GroupName="TaxType" />
                            <asp:RadioButton ID="rbtnIndividualTaxType" value="optIndividualTax" runat="server" Enabled="false" Text="Individual" GroupName="TaxType" />
                        </td>
                    </tr>
                </table>

                <table>
                    <thead>
                        <tr>
                            <th colspan="2" style="background-color: #7EB7DF;">Transaction and Credit Limits
                            </th>
                        </tr>
                    </thead>
                    <tr>
                        <td width="30%">
                            <label>Credit Per Transaction ($)</label></td>
                        <td>
                            <asp:Label ID="lblCreditPerTransaction" runat="server" Font-Size="12px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <label>Cummulative Credit ($)</label></td>
                        <td>
                            <asp:Label ID="lblCumilativeCredit" runat="server" Font-Size="12px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <label>Currency</label></td>
                        <td>
                            <asp:Label ID="lblCurrency" runat="server" Font-Size="12px"></asp:Label></td>
                    </tr>
                </table>           
            <div class="clr"></div>
                <table class="tablep">
                    <thead>
                        <tr>
                            <th width="2%"></th>
                            <th>Number /ID</th>
                            <th>Extension</th>
                            <th>Type</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rptrPhones" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td></td>
                                    <td>
                                        <%# Eval("phone_number")%>
                                    </td>
                                    <td>
                                        <%# Eval("extension")%>
                                    </td>
                                    <td>
                                        <%# Eval("contact_type") %>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
                <table>
                    <tr>
                        <td style="float: right;">
                            <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClientClick="parent.$.fancybox.close(); return false;"></asp:Button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</body>
</html>
