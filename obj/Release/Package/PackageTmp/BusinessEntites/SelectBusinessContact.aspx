﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SelectBusinessContact.aspx.cs" Inherits="Bestprintbuy.Admin.Web.BusinessEntites.SelectBusinessContact" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Best Print Buy</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" media="all" href="../css/bestprintbuy_style.css" />
    <link rel="stylesheet" media="all" href="../css/jquery.fancybox.css" />
    <script src="../Scripts/jquery.js"></script>
    <script src="../Scripts/common.js"></script>
    <script src="../Scripts/ddsmoothmenu.js"></script>
    <script src="../Scripts/plugin/fancybox/jquery.fancybox.js"></script>
    <!--[if lt IE 9]>
			<script src="../Scripts/html5.js"></script>
		<![endif]-->
    <!--[if lte IE 7]> <html class="ie7"> <![endif]-->
    <!--[if IE 8]>     <html class="ie8"> <![endif]-->
    <!--[if IE 9]>     <html class="ie9"> <![endif]-->
    <!--[if IE 10]>     <html class="ie10"> <![endif]-->
    <!--[if !IE]><!-->
    <!--<![endif]-->
    <script type="text/javascript">
        function customerSelect(obj) {
            var grid = obj.parentNode.parentNode.parentNode.parentNode;
            var inputs = grid.getElementsByTagName("input");
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].type == "checkbox") {
                    if (obj.checked && inputs[i] != obj && inputs[i].checked) {
                        inputs[i].checked = false;
                    }
                }
            }
        }

        function SetDataAndClose(PkeyContact, FirstName, LastName) {
            window.top.SetContactData(PkeyContact, FirstName, LastName);
            return false;
        }

        function checkSelect() {
            var ischecked = false;
            $('.select input:checkbox').each(function () {
                if (this.checked) {
                    ischecked = true;
                    return true;
                }
            });

            if (!ischecked) {
                alert("Please select atleast one bussiness contact.");
                return false;
            }
            return true;
        }
    </script>
</head>
<body id="Body" runat="server">
    <!-- Wrapper starts -->
    <div id="Div1" class="popup-wrapper" runat="server">
        <div class="popup-title">
            Business Contact Details
        </div>
        <!-- Header Ends -->
        <div class="popup-contnt">
            <!-- Menu starts -->
            <form id="Form1" runat="server">
                <table class="tablep">
                    <thead>
                        <tr>
                            <th>Contact ID</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Title</th>
                            <th>Salutation</th>
                            <th>Select</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rptrBusinessContactDetails" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <%#Eval("CONTACT_ID") %>
                                    </td>
                                    <td>
                                        <%#Eval("FIRST_NAME") %>
                                    </td>
                                    <td>
                                        <%#Eval("LAST_NAME") %>
                                    </td>
                                    <td>
                                        <%#Eval("Title") %>
                                    </td>
                                    <td>
                                        <%#Eval("Salutation") %>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkSelect" runat="server" onclick="customerSelect(this);" CssClass="select"/>
                                        <asp:HiddenField ID="hdnPkeyContact" runat="server" Value='<%#Eval("pkey_br_contact") %>' />
                                        <asp:HiddenField ID="hdnFirstName" runat="server" Value='<%#Eval("FIRST_NAME") %>' />
                                        <asp:HiddenField ID="hdnLastName" runat="server" Value='<%#Eval("LAST_NAME") %>' />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
                <table>
                    <tr>
                        <td style="float: right;">
                            <asp:Button runat="server" ID="btnSelect" CssClass="button rounded" Text="Select" OnClick="btnSelect_Click" OnClientClick="if(!checkSelect()) return false;"></asp:Button>
                            <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClientClick="parent.$.fancybox.close(); return false;"></asp:Button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</body>
</html>
