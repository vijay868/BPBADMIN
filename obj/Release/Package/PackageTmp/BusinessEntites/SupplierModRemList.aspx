﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="SupplierModRemList.aspx.cs" Inherits="Bestprintbuy.Admin.Web.BusinessEntites.SupplierModRemList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="clr"></div>
    <!-- Container Starts -->
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Business Partners Setup</li>
            <li>Supplier Modification Reminders</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Supplier Modification Reminders</h2>
        <form id="frmSupReminders" name="frmSupReminders" runat="server" class="form">
            <table cellpadding="0" cellspacing="0" border="0" width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="15%">
                            <asp:DropDownList name="ddltypes" ID="ddltypes" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddltypes_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td width="85%"></td>
                    </tr>
                </tbody>
            </table>           
            <div class="clr"></div>

            <asp:GridView ID="SuppliermodGrid" runat="server" AutoGenerateColumns="false" EnableViewState="true"
                ForeColor="Black" Width="100%" PageSize="20" AllowPaging="true" AllowSorting="true"
                CssClass="table" name="SuppliermodGrid" OnPageIndexChanging="SuppliermodGrid_PageIndexChanging" OnSorting="CustomerslistGrid_Sorting">
                <PagerStyle HorizontalAlign="Right" CssClass="dtPage"></PagerStyle>
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />
                <Columns>
                    <asp:TemplateField HeaderText="#SL">
                        <HeaderStyle Width="6%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <span><%# GetRecordCountForPage()%></span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Business ID">
                        <ItemTemplate>
                             <a href="SupplierModReminder.aspx?pkey=<%#Eval("pkey_business_register") %>&bid=<%= pkeyMasterCat %>"  title="Edit"><%#Eval("business_id") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Business Name">
                        <ItemTemplate>
                              <a href="SupplierModReminder.aspx?pkey=<%#Eval("pkey_business_register") %>&bid=<%= pkeyMasterCat %>"  title="Edit"><%#Eval("business_name") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Type Of Business Register">
                        <ItemTemplate>
                             <%#Eval("businesstype") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="City">
                        <ItemTemplate>
                                <%#Eval("city") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                      <asp:TemplateField HeaderText="State">
                        <ItemTemplate>
                            <%# Eval("state_name") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                      <asp:TemplateField HeaderText="Country">
                        <ItemTemplate>
                            <%# Eval("country_name") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
             <asp:Label ID="lblTotalRecords" runat="server" Font-Bold="true"></asp:Label>
          
            <!-- table Ends -->
            <div class="btn_lst">
               
                <div class="rgt">
                    <asp:Button ID="btnClose" Text="Close" CssClass="button rounded" runat="server" OnClick="btnClose_Click"></asp:Button>
                </div>
            </div>
        </form>
    </div>
</asp:Content>
