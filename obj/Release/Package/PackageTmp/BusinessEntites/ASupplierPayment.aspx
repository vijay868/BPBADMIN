﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="ASupplierPayment.aspx.cs" Inherits="Bestprintbuy.Admin.Web.BusinessEntites.ASupplierPayment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function validateSupplier() {
            if ($("#ContentPlaceHolder1_lblSupplier").text() == "") {
                alert("Supplier is required, Please select a supplier and try again.");
                return false;
            }

            if ($("#ContentPlaceHolder1_lblPayto").text() == "") {
                alert("Pay To  is required, Please select a contact and try again.");
                return false;
            }
            return true;
        }

        function SupplierList() {
            var fkeyregister = $("#ContentPlaceHolder1_hdnfkeyRegister").val();
            url = "SelectBusinessEntity.aspx?bid=sup&?pkeyregister=" + fkeyregister;
            $.fancybox({
                'width': '70%',
                'height': 'auto',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': url,
                'type': 'iframe'
            });
            return false;
        }

        function PaytoList() {
            if ($("#ContentPlaceHolder1_lblSupplier").text() == "") {
                alert("Please select a Supplier before selecting the Pay To contact.");
                return false;
            }

            var fkeyregister = $("#ContentPlaceHolder1_hdnfkeyRegister").val();
            url = "SelectBusinessContact.aspx?pkeyregister=" + fkeyregister;
            $.fancybox({
                'width': '70%',
                'height': 'auto',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': url,
                'type': 'iframe'
            });
            return false;
        }

        function SetContactData(PkeyContact, FirstName, LastName) {
            $("#ContentPlaceHolder1_hdnfkeyContact").val(PkeyContact);
            $("#ContentPlaceHolder1_lblPayto").text(FirstName + " " + LastName);
            parent.$.fancybox.close();
        }

        function SetSupplierData(RegisterKey, SupplierName) {
            $("#ContentPlaceHolder1_hdnfkeyRegister").val(RegisterKey);
            $("#ContentPlaceHolder1_lblPayto").text("");
            __doPostBack("ctl00$ContentPlaceHolder1$lbtnSupplier", "");
            parent.$.fancybox.close();
        }

        function SupplierPreview() {
            if ($("#ContentPlaceHolder1_lblSupplier").text() == "") {
                alert("Please select the supplier.");
                return false;
            }
            var RegisterKey = $("#ContentPlaceHolder1_hdnfkeyRegister").val() != "" ? $("#ContentPlaceHolder1_hdnfkeyRegister").val() : "<%= hdnfkeyRegister.Value%>";
            var url = "../BusinessEntites/BusinessRegisterPopUp.aspx?bid=bur&pkey=" + RegisterKey;
            $.fancybox({
                'width': '60%',
                'height': 'auto',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': url,
                'type': 'iframe'
            });
            return false;
        }

        function PaytoPreview() {
            if ($("#ContentPlaceHolder1_lblPayto").text() == "") {
                alert("Please select the Contact person.");
                return false;
            }
            var pkeyContact = $("#ContentPlaceHolder1_hdnfkeyContact").val() != "" ? $("#ContentPlaceHolder1_hdnfkeyContact").val() : "<%= hdnfkeyContact.Value%>";
            var url = "../BusinessEntites/ContactPopUp.aspx?pkeyContact=" + pkeyContact;
            $.fancybox({
                'width': '60%',
                'height': 'auto',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': url,
                'type': 'iframe'
            });
            return false;
        }

        $().ready(function () {
            $("#frmSupplierPayment").validate();
        });
    </script>

    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Business Partners Setup</li>
            <li>Supplier Payment Information </li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Supplier Payment Information</h2>
        <form id="frmSupplierPayment" name="frmSupplierPayment" runat="server">
            <table width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="15%" style="text-align: center;">
                            <asp:Button ID="btnSupplier" CssClass="button rounded" Text="Supplier" runat="server" UseSubmitBehavior="false" OnClientClick="return SupplierPreview();" />
                            <asp:LinkButton ID="lbtnSupplier" runat="server" OnClick="lbtnSupplier_Click" />
                            <asp:Button ID="btnSupplierDetail" CssClass="button rounded" Text="..." runat="server" UseSubmitBehavior="false" OnClientClick="return SupplierList();" />
                        </td>
                        <td width="20%">
                            <asp:Label ID="lblSupplier" runat="server" />
                        </td>
                        <td width="15%" style="text-align: center;">
                            <asp:Button ID="btnPayto" Text="Pay To" CssClass="button rounded" runat="server" UseSubmitBehavior="false" OnClientClick="return PaytoPreview();" />
                            <asp:Button ID="btnPaytiDetails" Text="..." CssClass="button rounded" runat="server" UseSubmitBehavior="false" OnClientClick="return PaytoList();" />
                        </td>
                        <td width="35%">
                            <asp:Label ID="lblPayto" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Payment Category*</label></td>
                        <td>
                            <asp:DropDownList name="ddlPaymentCategory" ID="ddlPaymentCategory" CssClass="slect-box" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPaymentCategory_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <label>Payment Type*</label>
                        </td>
                        <td>
                            <asp:DropDownList name="ddlPaymentType" ID="ddlPaymentType" CssClass="slect-box" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Attention of</label></td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtAttentionof" ID="txtAttentionof" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <label>Address</label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtAddress1" ID="txtAddress1" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>City</label></td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtCity" ID="txtCity" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <label></label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtAddress2" ID="txtAddress2" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Country </label>
                        </td>
                        <td>
                            <asp:DropDownList name="ddlCountry" ID="ddlCountry" CssClass="slect-box" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <label>State </label>
                        </td>
                        <td>
                            <asp:DropDownList name="ddlState" ID="ddlState" CssClass="slect-box" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Postal Code </label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box digits" name="txtPostalCode" ID="txtPostalCode" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <label></label>
                        </td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
            <table class="accounts-table" width="100%">
                <thead>
                    <tr>
                        <th colspan="4">Funds Transfer Information</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="15%">
                            <label>Supplier A/c Number</label></td>
                        <td width="20%">
                            <asp:TextBox CssClass="text-box digits" name="txtSupplierAccNumber" ID="txtSupplierAccNumber" runat="server"></asp:TextBox>
                        </td>
                        <td width="15%">
                            <label>Bank Name </label>
                        </td>
                        <td width="35%">
                            <asp:TextBox CssClass="text-box" name="txtBankName" ID="txtBankName" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Routing Number </label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtRoutingtNumber" ID="txtRoutingtNumber" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <label>Account Number </label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box digits" name="txtAccnumber" ID="txtAccnumber" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            </td>
                        <td style="float:right; padding-right:24%;">
                            <asp:HiddenField ID="hdnfkeyRegister" runat="server" />
                            <asp:HiddenField ID="hdnfkeyContact" runat="server" />
                            <asp:HiddenField ID="hdnpkey_br_payment_info" runat="server" />
                            <asp:Button runat="server" ID="btnSave" Text="Save" CssClass="button rounded" OnClick="btnSave_Click" OnClientClick="return validateSupplier();"></asp:Button>
                            <asp:Button runat="server" ID="btnClose" Text="Close" CssClass="button rounded" UseSubmitBehavior="false" OnClick="btnClose_Click"></asp:Button>
                        </td>
                    </tr>
                </tbody>
            </table>

        </form>
    </div>

</asp:Content>
