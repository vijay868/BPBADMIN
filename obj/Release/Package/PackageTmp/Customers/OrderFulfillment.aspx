﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="OrderFulfillment.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Customers.OrderFulfillment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        var strFilesNotFound;
        var gbool = false;
        var openOrderMail = false;

        $(document).ready(function () {
            $("#frmOrderFulfillmentList").validate();
            SearchText();

            // open order mail popup
            if (openOrderMail == true) {
                openOrderMail = false;

                $.fancybox({
                    'width': '80%',
                    'height': '80%',
                    'autoScale': true,
                    'transitionIn': 'fade',
                    'transitionOut': 'fade',
                    'href': 'OrderFulfillmentMail.aspx?timestamp=<%= System.DateTime.Now.Ticks.ToString()%>',
                    'type': 'iframe'
                });
            }
        });

        function ExportOrders() {
            $.fancybox({
                'width': '50%',
                'height': '40%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': 'ExportOrderOptions.aspx?timestamp=<%= System.DateTime.Now.Ticks.ToString()%>',
                'type': 'iframe'
            });
            return false;
        }

        function SubmitPageFromNotes() {
            var frmOrderFulfillmentList = document.forms['frmOrderFulfillmentList'];
            frmOrderFulfillmentList.submit();
        }

        function ShipRecon() {
            $.fancybox({
                'width': '80%',
                'height': 'auto',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': 'ShipmentRecon.aspx?timestamp=<%= System.DateTime.Now.Ticks.ToString()%>',
                'type': 'iframe'
            });
            return false;
        }

        function OpenOFMail() {
            openOrderMail = true;
            //setTimeout(function () { OpenOFMailPopup(); }, 1000)
        }

        function CopyAssetstoFTP() {
            var url = 'CopyAssetsToFTP.aspx?timestamp=<%= System.DateTime.Now.Ticks.ToString()%>';
            $("#divID").dialog("open");
            $("#summaryframe").attr("src", url);
            return false;
        }

        function XCopy() {
            var frmOrderFulfillmentList = document.forms['frmOrderFulfillmentList'];
            var bool = false;
            var arrFiles = new Array();
            var j = 0;
            var k = 0;
            strFilesNotFound = "The following files does not exist.\n";
            var iXCopy = 0;
            for (iXCopy = 0; iXCopy < frmOrderFulfillmentList.elements.length; iXCopy++) {
                var elementName = frmOrderFulfillmentList.elements[iXCopy].name;
                if (frmOrderFulfillmentList.elements[iXCopy].type == "checkbox" &&
                    elementName.indexOf("chkPrinted") != -1) {
                    var index = elementName.substring("chkPrinted".length, elementName.length);
                    var chkprint = eval("frmOrderFulfillmentList.chkPrinted" + index);
                    if (chkprint.checked == true) {
                        var hiresurl = eval("frmOrderFulfillmentList.hidPDF" + index + ".value");
                        var csvUrl = eval("frmOrderFulfillmentList.hidcsvUrl" + index + ".value");
                        arrFiles[j] = hiresurl;
                        if (eval("frmOrderFulfillmentList.is_direct_mail" + index + ".value") == "True") {
                            if (csvUrl != "") {
                                j++;
                                arrFiles[j] = csvUrl;
                                j++;
                                arrFiles[j] = csvUrl.toLowerCase().replace(".csv", ".html");
                            }
                        }
                        if (hiresurl.toLowerCase().indexOf(".xls") > 0) {
                            var strImgUrl = eval("frmOrderFulfillmentList.hidLogoUrl" + index + ".value");
                            if (strImgUrl != "") {
                                j++;
                                arrFiles[j] = strImgUrl;
                            }
                            strImgUrl = eval("frmOrderFulfillmentList.hidPhotoUrl" + index + ".value");
                            if (strImgUrl != "") {
                                j++;
                                arrFiles[j] = strImgUrl;
                            }
                        }
                        if (eval("frmOrderFulfillmentList.hidFrontImageURL" + index + ".value") != "") {
                            var strImgUrl = eval("frmOrderFulfillmentList.hidFrontImageURL" + index + ".value");
                            j++;
                            arrFiles[j] = strImgUrl;
                        }
                        if (eval("frmOrderFulfillmentList.hidBackImageURL" + index + ".value") != "") {
                            var strImgUrl = eval("frmOrderFulfillmentList.hidBackImageURL" + index + ".value");
                            j++;
                            arrFiles[j] = strImgUrl;
                        }

                        j++;
                        bool = true;
                    }

                }
            }
            if (bool == false) {
                alert("Select atleast one file to Copy.");
                return false;
            }
            else {
                var strAlert = "This WebPage uses some ActiveX Controls,\n";
                strAlert += "To continue with the 'X-Copy' process, it is necessary\n";
                strAlert += "that you change the browser settings as specified.\n";
                strAlert += "Go to 'Tools -> Internet Options...' Under 'Security' Tab click on 'Custom Level...'\n";
                strAlert += "and Set the 'Initialize and script ActiveX controls not marked as safe' to 'Enable'.";
                strAlert += "\n\n";
                strAlert += "This page may request a data source on another domain.\n"
                strAlert += "Hence,It is also necessary that, you set the 'Access Data Sources across domains'\n"
                strAlert += "option to 'Enable'.If this option is set to 'Prompt',you will be prompted for\n";
                strAlert += "each file being copied,you can choose whether to copy a particular file or not."
                strAlert += "\n\n";
                strAlert += "If you want to continue with the 'X-Copy',click 'OK',or click 'Cancel' if you want to\n";
                strAlert += "check the browser settings."

                if (confirm(strAlert)) {
                    var strInnerAlert;
                    var strDestFileName;
                    try {
                        var Shell = new ActiveXObject("Shell.Application");
                        var Folder = new Object;
                        Folder = Shell.BrowseForFolder(0, "Choose a folder to copy", 0);
                        var FolderItem = new Object;
                        FolderItem = Folder.Items().Item();
                        var strPath = FolderItem.Path;
                    } catch (e) {
                        if (e.description == "Permission denied") {
                            strInnerAlert = "Cannot continue with the 'X-Copy' process\n";
                            strInnerAlert += "A Security violation has occured,please change\n";
                            strInnerAlert += "browser settings as specified.\n";
                            strInnerAlert += "Go to 'Tools -> Internet Options...' Under 'Security' Tab click on 'Custom Level...'\n";
                            strInnerAlert += "and Set the 'Initialize and script ActiveX controls not marked as safe' to 'Enable'.\n";
                            //strInnerAlert += "Be sure to refesh the window after you change the settings."
                            strInnerAlert += "\n\n"
                            alert(strInnerAlert);
                            return false;
                        }
                        else {
                            if (strPath == null) {
                                alert("Select a Folder to Copy");
                                return false;
                            }
                        }
                    }
                    iXCopy = 0;
                    for (iXCopy = 0; iXCopy < arrFiles.length; iXCopy++) {
                        strDestFileName = arrFiles[iXCopy].substring(arrFiles[iXCopy].lastIndexOf("/") + 1, arrFiles[iXCopy].length);
                        strDestFileName = strPath + "\\" + strDestFileName;
                        try {
                            CopyRemoteFileToLocalFolder(arrFiles[iXCopy], strDestFileName)
                        } catch (e) {
                            if (e.description == "Permission denied") {
                                strInnerAlert = "Could not copy the specified files.";
                                strInnerAlert += "It is necessary that, you set the 'Access Data Sources across domains'\n";
                                strInnerAlert += "option to 'Enable'.You cannot copy files if this option is set to 'Disable'.\n";
                                //strInnerAlert += "Be sure to refesh the window after you change the settings."
                                alert(strInnerAlert);
                                return false;
                            }
                        }
                    }
                    if (gbool == true) {
                        alert(strFilesNotFound + "\nProcess Completed.");
                    }
                    else {
                        alert("Process Completed.");
                    }
                }
            }
        }

        function CopyUploadDesigns(index) {
            var frmOrderFulfillmentList = document.forms['frmOrderFulfillmentList'];
            var arrFiles = new Array();
            if (eval("frmOrderFulfillmentList.hidFrontImageURL" + index + ".value") != "") {
                var strImgUrl = eval("frmOrderFulfillmentList.hidFrontImageURL" + index + ".value");
                arrFiles[0] = strImgUrl;
            }
            if (eval("frmOrderFulfillmentList.hidBackImageURL" + index + ".value") != "") {
                var strImgUrl = eval("frmOrderFulfillmentList.hidBackImageURL" + index + ".value");
                arrFiles[1] = strImgUrl;
            }

            var strAlert = "This WebPage uses some ActiveX Controls,\n";
            strAlert += "To continue with the 'X-Copy' process, it is necessary\n";
            strAlert += "that you change the browser settings as specified.\n";
            strAlert += "Go to 'Tools -> Internet Options...' Under 'Security' Tab click on 'Custom Level...'\n";
            strAlert += "and Set the 'Initialize and script ActiveX controls not marked as safe' to 'Enable'.";
            strAlert += "\n\n";
            strAlert += "This page may request a data source on another domain.\n"
            strAlert += "Hence,It is also necessary that, you set the 'Access Data Sources across domains'\n"
            strAlert += "option to 'Enable'.If this option is set to 'Prompt',you will be prompted for\n";
            strAlert += "each file being copied,you can choose whether to copy a particular file or not."
            strAlert += "\n\n";
            strAlert += "If you want to continue with the 'Copy',click 'OK',or click 'Cancel' if you want to\n";
            strAlert += "check the browser settings."

            if (confirm(strAlert)) {
                var strInnerAlert;
                var strDestFileName;
                try {
                    var Shell = new ActiveXObject("Shell.Application");
                    var Folder = new Object;
                    Folder = Shell.BrowseForFolder(0, "Choose a folder to copy", 0);
                    var FolderItem = new Object;
                    FolderItem = Folder.Items().Item();
                    var strPath = FolderItem.Path;
                } catch (e) {
                    if (e.description == "Permission denied") {
                        strInnerAlert = "Cannot continue with the 'X-Copy' process\n";
                        strInnerAlert += "A Security violation has occured,please change\n";
                        strInnerAlert += "browser settings as specified.\n";
                        strInnerAlert += "Go to 'Tools -> Internet Options...' Under 'Security' Tab click on 'Custom Level...'\n";
                        strInnerAlert += "and Set the 'Initialize and script ActiveX controls not marked as safe' to 'Enable'.\n";
                        //strInnerAlert += "Be sure to refesh the window after you change the settings."
                        strInnerAlert += "\n\n"
                        alert(strInnerAlert);
                        return false;
                    }
                    else {
                        if (strPath == null) {
                            alert("Select a Folder to Copy");
                            return false;
                        }
                    }
                }
                var iXCopyUD = 0;
                for (iXCopyUD = 0; iXCopyUD < arrFiles.length; iXCopyUD++) {
                    strDestFileName = arrFiles[iXCopyUD].substring(arrFiles[iXCopyUD].lastIndexOf("/") + 1, arrFiles[iXCopyUD].length);
                    strDestFileName = strPath + "\\" + strDestFileName;
                    try {
                        CopyRemoteFileToLocalFolder(arrFiles[iXCopyUD], strDestFileName)
                    } catch (e) {
                        if (e.description == "Permission denied") {
                            strInnerAlert = "Could not copy the specified files.";
                            strInnerAlert += "It is necessary that, you set the 'Access Data Sources across domains'\n";
                            strInnerAlert += "option to 'Enable'.You cannot copy files if this option is set to 'Disable'.\n";
                            //strInnerAlert += "Be sure to refesh the window after you change the settings."
                            alert(strInnerAlert);
                            return false;
                        }
                    }
                }
                if (gbool == true) {
                    alert(strFilesNotFound + "\nProcess Completed.");
                }
                else {
                    alert("Process Completed.");
                }
            }
        }

        function copyExcel(xlUrl, photoUrl, logoUrl) {
            var frmOrderFulfillmentList = document.forms['frmOrderFulfillmentList'];
            var arrFiles = new Array();
            arrFiles[0] = xlUrl;

            if (photoUrl != "")
                arrFiles[1] = photoUrl;
            if (logoUrl != "")
                arrFiles[2] = logoUrl;
            var strAlert = "This WebPage uses some ActiveX Controls,\n";
            strAlert += "To continue with the 'X-Copy' process, it is necessary\n";
            strAlert += "that you change the browser settings as specified.\n";
            strAlert += "Go to 'Tools -> Internet Options...' Under 'Security' Tab click on 'Custom Level...'\n";
            strAlert += "and Set the 'Initialize and script ActiveX controls not marked as safe' to 'Enable'.";
            strAlert += "\n\n";
            strAlert += "This page may request a data source on another domain.\n"
            strAlert += "Hence,It is also necessary that, you set the 'Access Data Sources across domains'\n"
            strAlert += "option to 'Enable'.If this option is set to 'Prompt',you will be prompted for\n";
            strAlert += "each file being copied,you can choose whether to copy a particular file or not."
            strAlert += "\n\n";
            strAlert += "If you want to continue with the 'Copy',click 'OK',or click 'Cancel' if you want to\n";
            strAlert += "check the browser settings."

            if (confirm(strAlert)) {
                var strInnerAlert;
                var strDestFileName;
                try {
                    var Shell = new ActiveXObject("Shell.Application");
                    var Folder = new Object;
                    Folder = Shell.BrowseForFolder(0, "Choose a folder to copy", 0);
                    var FolderItem = new Object;
                    FolderItem = Folder.Items().Item();
                    var strPath = FolderItem.Path;
                } catch (e) {
                    if (e.description == "Permission denied") {
                        strInnerAlert = "Cannot continue with the 'X-Copy' process\n";
                        strInnerAlert += "A Security violation has occured,please change\n";
                        strInnerAlert += "browser settings as specified.\n";
                        strInnerAlert += "Go to 'Tools -> Internet Options...' Under 'Security' Tab click on 'Custom Level...'\n";
                        strInnerAlert += "and Set the 'Initialize and script ActiveX controls not marked as safe' to 'Enable'.\n";
                        //strInnerAlert += "Be sure to refesh the window after you change the settings."
                        strInnerAlert += "\n\n"
                        alert(strInnerAlert);
                        return false;
                    }
                    else {
                        if (strPath == null) {
                            alert("Select a Folder to Copy");
                            return false;
                        }
                    }
                }
                var iCopyEx = 0;
                for (iCopyEx = 0; iCopyEx < arrFiles.length; iCopyEx++) {
                    strDestFileName = arrFiles[iCopyEx].substring(arrFiles[iCopyEx].lastIndexOf("/") + 1, arrFiles[iCopyEx].length);
                    strDestFileName = strPath + "\\" + strDestFileName;
                    try {
                        CopyRemoteFileToLocalFolder(arrFiles[iCopyEx], strDestFileName)
                    } catch (e) {
                        if (e.description == "Permission denied") {
                            strInnerAlert = "Could not copy the specified files.";
                            strInnerAlert += "It is necessary that, you set the 'Access Data Sources across domains'\n";
                            strInnerAlert += "option to 'Enable'.You cannot copy files if this option is set to 'Disable'.\n";
                            //strInnerAlert += "Be sure to refesh the window after you change the settings."
                            alert(strInnerAlert);
                            return false;
                        }
                    }
                }
                if (gbool == true) {
                    alert(strFilesNotFound + "\nProcess Completed.");
                }
                else {
                    alert("Process Completed.");
                }
            }
        }

        function CopyRemoteFileToLocalFolder(From, To) {
            var frmOrderFulfillmentList = document.forms['frmOrderFulfillmentList'];
            var oXmlHttp = new ActiveXObject("Msxml2.XmlHttp.3.0");
            oXmlHttp.open("GET", From, false);
            oXmlHttp.send();
            if (oXmlHttp.status == 200) {
                var oStream = new ActiveXObject("Adodb.Stream");
                oStream.type = 1; //Binary
                oStream.open();
                oStream.write(oXmlHttp.responseBody);
                oStream.saveToFile(To, 1 && 2); //Create if needed and overwrite if necessary
                oStream.close();
            }
            else
                if (oXmlHttp.status == 404) {
                    gbool = true;
                    strFilesNotFound += From + "\n";
                    throw new Error(oXmlHttp.statusText);
                }
                else {
                    alert(oXmlHttp.status);
                    throw new Error(oXmlHttp.statusText);
                }
        }

        function XPrintManifest() {
            $.fancybox({
                'width': '60%',
                'height': '80%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': 'XPrintInvoice.aspx?ManifestPrint=true',
                'type': 'iframe'
            });
            return false;
        }

        function XPrintValue() {
            var frmOrderFulfillmentList = document.forms['frmOrderFulfillmentList'];
            var bool = false;
            var InvoiceValues;
            var EditKeys, PoDtlKey, customerKey, brpoKey;
            frmOrderFulfillmentList.hidXPrintValue.value = "";
            var iXPVal = 0;
            for (iXPVal = 0; iXPVal < frmOrderFulfillmentList.elements.length; iXPVal++) {
                var elementName = frmOrderFulfillmentList.elements[iXPVal].name;
                if (frmOrderFulfillmentList.elements[iXPVal].type == "checkbox" &&
                    elementName.indexOf("chkPrinted") != -1) {
                    var index = elementName.substring("chkPrinted".length, elementName.length);
                    var chkprint = eval("frmOrderFulfillmentList.chkPrinted" + index);
                    if (chkprint.checked == true) {
                        EditKeys = eval("frmOrderFulfillmentList.POKey" + index);
                        PoDtlKey = eval("frmOrderFulfillmentList.PODtlKey" + index);
                        customerKey = eval("frmOrderFulfillmentList.CustKey" + index);
                        brpoKey = eval("frmOrderFulfillmentList.BRPODtl" + index);

                        InvoiceValues = EditKeys.value + "," + PoDtlKey.value + "," + customerKey.value + "," + brpoKey.value;
                        frmOrderFulfillmentList.hidXPrintValue.value += InvoiceValues + "^";
                        bool = true;
                    }
                }
            }
            if (bool == false) {
                alert("Select atleast one Order to print.");
                return false;
            }
            else {
                $.fancybox({
                    'width': '60%',
                    'height': '80%',
                    'autoScale': true,
                    'transitionIn': 'fade',
                    'transitionOut': 'fade',
                    'href': 'XPrintInvoice.aspx?Keys=' + frmOrderFulfillmentList.hidXPrintValue.value,
                    'type': 'iframe'
                });
                return false;
            }
        }

        function SendNotificationMailToCustomer() {
            var bool = false;
            var PoDtlKey;
            var PoDtlKeyValues = "";
            var checkedOrders;
            $("#ContentPlaceHolder1_hdnPOKeys").val("");
            var iXPVal = 0;
            for (iXPVal = 0; iXPVal < frmOrderFulfillmentList.elements.length; iXPVal++) {
                var elementName = frmOrderFulfillmentList.elements[iXPVal].name;
                if (frmOrderFulfillmentList.elements[iXPVal].type == "checkbox" &&
                    elementName.indexOf("chkPrinted") != -1) {
                    var index = elementName.substring("chkPrinted".length, elementName.length);
                    var chkprint = eval("frmOrderFulfillmentList.chkPrinted" + index);
                    if (chkprint.checked == true) {
                        PoDtlKey = eval("frmOrderFulfillmentList.PODtlKey" + index);

                        PoDtlKeyValues += PoDtlKey.value + ",";
                        bool = true;
                    }
                }
            }
            if (bool == false) {
                alert("Select atleast one Order to Send Notification Mail to Customer.");
                return false;
            }
            $("#ContentPlaceHolder1_hdnPOKeys").val(PoDtlKeyValues);
            return true;
        }

        function SelectOrderStatus(cbostatus) {
            var frmOrderFulfillmentList = document.forms['frmOrderFulfillmentList'];
            var val = cbostatus.options[cbostatus.selectedIndex].value;
            var text = cbostatus.options[cbostatus.selectedIndex].text;
            var iDFP = 0;
            //alert(val);
            for (iDFP = 0; iDFP < frmOrderFulfillmentList.elements.length; iDFP++) {
                var elementName = frmOrderFulfillmentList.elements[iDFP].name;
                var id = frmOrderFulfillmentList.elements[iDFP].id;
                if (elementName.indexOf("cboOrderStatus") != -1) {

                    //alert(id + ":" + val + ":" + text);
                    //frmOrderFulfillmentList.elements[iDFP][cbostatus.selectedIndex].attr('selected', 'selected');
                    $("#" + id + " option:contains(" + text + ")").attr('selected', 'selected');
                    $("#" + id + '_chzn span').each(function () {
                        //alert(this.innerHTML);
                        this.innerHTML = text;
                    });
                }
            }
        }

        function SelectAllDFP(Check) {
            var frmOrderFulfillmentList = document.forms['frmOrderFulfillmentList'];
            var iDFP = 0;
            //alert(frmOrderFulfillmentList.elements.length);
            for (iDFP = 0; iDFP < frmOrderFulfillmentList.elements.length; iDFP++) {
                var elementName = frmOrderFulfillmentList.elements[iDFP].name;
                //alert(elementName);
                if (frmOrderFulfillmentList.elements[iDFP].type == "checkbox" &&
                    elementName.indexOf("chkPrinted") != -1) {
                    var index = elementName.substring("chkPrinted".length, elementName.length)
                    var chkprint = eval("frmOrderFulfillmentList.chkPrinted" + index);

                    chkprint.checked = (Check.checked);
                    //alert(chkprint.checked);
                }
            }
        }

        function UpdateKeysForSave(obj, key) {

            var frmOrderFulfillmentList = document.forms['frmOrderFulfillmentList'];
            if (frmOrderFulfillmentList.OrderKeys.value == "")
                frmOrderFulfillmentList.OrderKeys.value = obj.id.substring(obj.id.length - 2) + "," + key;
            else if (frmOrderFulfillmentList.OrderKeys.value.indexOf(obj.value) == -1) {
                frmOrderFulfillmentList.OrderKeys.value = "$" + obj.id.substring(obj.id.length - 2) + "," + key;
            }
        }

        function ReleaseHoldOrders() {
            var frmOrderFulfillmentList = document.forms['frmOrderFulfillmentList'];
            var recs = parseInt($('#ContentPlaceHolder1_hdnRecordCount').val());
            if (recs == "0") {
                alert("No Records Available for holding.");
                return;
            }
            var iRelease = 0;
            var hasRelease = false;
            var frm = frmOrderFulfillmentList;
            for (iRelease = 0; iRelease < frm.elements.length; iRelease++) {
                var elementName = frm.elements[iRelease].name;
                if (frm.elements[iRelease].type == "select-one" &&
                    elementName.indexOf("cboOrderStatus") != -1) {
                    if (frm.elements[iRelease].options[frm.elements[iRelease].selectedIndex].value != -1) {
                        hasRelease = true;
                        break
                    }
                }
            }
            if (hasRelease) {
                return true;
            } else {
                alert("Please select queue into which you want to move the order to.");
                return false;
            }
        }

        function HoldOrders() {
            var frmOrderFulfillmentList = document.forms['frmOrderFulfillmentList'];
            var recs = parseInt($('#ContentPlaceHolder1_hdnRecordCount').val());
            if (recs == "0") {
                alert("No Records Available for holding.");
                return;
            }
            frmOrderFulfillmentList.TargetAction.value = "hold";
            var iHold = 0;
            for (iHold = 0; iHold < frmOrderFulfillmentList.elements.length; iHold++) {
                if (frmOrderFulfillmentList.elements[iHold].type == "checkbox") {
                    frmOrderFulfillmentList.elements[iHold].disabled = false;
                }
            }
            frmOrderFulfillmentList.submit();
        }

        var sendEmailClicked = false;
        function SaveOrders() {
            var frmOrderFulfillmentList = document.forms['frmOrderFulfillmentList'];
            var recs = parseInt($('#ContentPlaceHolder1_hdnRecordCount').val());
            if (IsAnyCanceledOrder(frmOrderFulfillmentList)) {
                if (!confirm("This will cancel the order and you cannot revert it! ")) {
                    return false;
                }
            }

            if (IsAnyAllowedModify(frmOrderFulfillmentList)) {
                if (!confirm("It allows user to modify design!Do you want to continue?")) {
                    return false;
                }
            }

            if (recs == "0") {
                if (sendEmailClicked == false)
                    alert("No Records Available for saving.");
                else
                    alert("No Records Available for sending mails.");

                return;
            }
            if (!CheckALLTrackingExists(frmOrderFulfillmentList)) {
                return false;
            }

            var iSave = 0;
            for (iSave = 0; iSave < frmOrderFulfillmentList.elements.length; iSave++) {
                if (frmOrderFulfillmentList.elements[iSave].type == "checkbox") {
                    frmOrderFulfillmentList.elements[iSave].disabled = false;
                }
            }

            /*if (sendEmailClicked == false) {
                frmOrderFulfillmentList.submit();
            }
            else {
                return true;
            }*/

            return true;
        }

        function SendEmails() {
            var frmOrderFulfillmentList = document.forms['frmOrderFulfillmentList'];
            sendEmailClicked = true;
            if (SaveOrders()) {
                return true;
            }
            return false;
        }

        function ResetOrders(theform, cboOrders) {
            var frmOrderFulfillmentList = document.forms['frmOrderFulfillmentList'];
            theform.PageNo.value = 1;
            //Modified 
            theform.TargetAction.value = " ";
            theform.submit();
        }

        function ResetAllOrders(theform, cboOrders) {
            var theform = document.forms['frmOrderFulfillmentList'];
            theform.PageNo.value = 1;
            if (theform.cboOrderProductsFilter.type == "select-one") {
                theform.cboOrderProductsFilter.options[0].selected = true;
            }
            theform.TargetAction.value = " ";
            theform.submit();
        }

        function CheckTrackingExists(chkBox, frm, index) {
            var frmOrderFulfillmentList = document.forms['frmOrderFulfillmentList'];
            if (eval("frmOrderFulfillmentList.txtTrackingNumber" + index)) {
                var trackFld = eval("frmOrderFulfillmentList.txtTrackingNumber" + index);
                var trackNo = trimData(trackFld.value)
                if (chkBox.checked == true && trackNo == "") {
                    alert("Please enter the tracking number.")
                    chkBox.checked = false;
                    trackFld.focus();
                }
            }
        }


        function CheckIsOrderCanceled(lindex) {
            var frmOrderFulfillmentList = document.forms['frmOrderFulfillmentList'];
            var lchkBox = eval("frmOrderFulfillmentList.chkIsOrderCanceled" + lindex);
            if (lchkBox.checked == true) {	//Chk for Tracking Number
                var trackFld = eval("frmOrderFulfillmentList.txtTrackingNumber" + lindex);
                var trackNo = trimData(trackFld.value)
                var trackchk = eval("frmOrderFulfillmentList.chkIsTrackingReq" + lindex);
                var printchk = eval("frmOrderFulfillmentList.chkPrinted" + lindex);
                //var ddcShipCompanyFld = eval("frmOrderFulfillmentList.ddcShipCompany"+lindex);

                if (trackchk.checked == true) {
                    alert("This order is canceled.")
                    trackchk.checked = false;
                    trackchk.focus();
                }
                else if (trackNo != "") {
                    alert("This order is canceled.")
                    trackFld.focus();
                }
                else if (printchk.checked == true) {
                    alert("This order is canceled.")
                    printchk.checked = false;
                    printchk.focus();
                }
            }
        }

        function CheckOrderCanceled(lchkBox, lfrm, lindex) {
            var frmOrderFulfillmentList = document.forms['frmOrderFulfillmentList'];
            if (lchkBox.checked == true) {	//Chk for Tracking Number
                var trackFld = eval("frmOrderFulfillmentList.txtTrackingNumber" + lindex);
                var trackNo = trimData(trackFld.value)
                var trackchk = eval("frmOrderFulfillmentList.chkIsTrackingReq" + lindex);
                var printchk = eval("frmOrderFulfillmentList.chkPrinted" + lindex);
                //var ddcShipCompanyFld = eval("frmOrderFulfillmentList.ddcShipCompany"+lindex);
                var IsEmailfld = eval("frmOrderFulfillmentList.IsEmail" + lindex);

                if (IsEmailfld.value != "") {
                    lchkBox.checked = false;
                    alert("Mail has been send.");
                    lchkBox.focus();
                    return false;
                }
                //if(trackchk.checked==true || trackNo != "" || printchk.checked==true || ddcShipCompanyFld.value >= 0)
                if (trackchk.checked == true || trackNo != "" || printchk.checked == true) {

                    if (confirm("This will make Tracking # Blank? Do you want to continue ? ")) {
                        if (trackNo != "") {
                            trackFld.value = "";
                        }
                        if (printchk.checked == true) {
                            printchk.checked = false;
                        }
                        //	ddcShipCompanyFld.value = -1									
                        trackchk.checked = false;
                    }
                    else {
                        lchkBox.checked = false;
                    }
                }
            }
        }

        function IsAnyCanceled(frm) {
            var frmOrderFulfillmentList = document.forms['frmOrderFulfillmentList'];
            var isVerified = false;
            var iCancel = 0;
            for (iCancel = 0; iCancel < frm.elements.length; iCancel++) {
                var elementName = frm.elements[iCancel].name;
                if (frm.elements[iCancel].type == "checkbox" &&
                    elementName.indexOf("chkIsOrderCanceled") != -1) {
                    var index = elementName.substring("chkIsOrderCanceled".length, elementName.length)

                    if (frm.elements[iCancel].checked == true && frm.elements[iCancel].disabled == false) {
                        isVerified = true;
                        break
                    }
                }
            }
            return isVerified;
        }

        function IsAnyCanceledOrder(frm) {
            var isVerified = false;
            var iCancel = 0;
            for (iCancel = 0; iCancel < frm.elements.length; iCancel++) {
                var elementName = frm.elements[iCancel].name;
                //alert(frm.elements[iCancel].type);
                if (frm.elements[iCancel].type == "select-one" &&
                    elementName.indexOf("cboOrderStatus") != -1) {
                    if (frm.elements[iCancel].options[frm.elements[iCancel].selectedIndex].value == 8) {
                        isVerified = true;
                        break
                    }
                }
            }
            return isVerified;
        }

        function IsAnyAllowedModify(frm) {
            var isVerified = false;
            var iallowM = 0;
            for (iallowM = 0; iallowM < frm.elements.length; iallowM++) {
                var elementName = frm.elements[iallowM].name;
                if (frm.elements[iallowM].type == "checkbox" &&
                    elementName.indexOf("chkCanUserModify") != -1) {
                    var index = elementName.substring("chkCanUserModify".length, elementName.length)

                    if (frm.elements[iallowM].checked == true && frm.elements[iallowM].disabled == false) {
                        isVerified = true;
                        break
                    }
                }
            }
            return isVerified;
        }
        function CheckALLTrackingExists_Old(frm) {
            var isVerified = true;
            var iCATE = 0;
            for (iCATE = 0; iCATE < frm.elements.length; iCATE++) {
                var elementName = frm.elements[iCATE].name;
                if (frm.elements[iCATE].type == "checkbox" &&
                    elementName.indexOf("chkIsTrackingReq") != -1) {
                    var index = elementName.substring("chkIsTrackingReq".length, elementName.length)
                    var trackFld = eval("frmOrderFulfillmentList.txtTrackingNumber" + index);
                    var trackNo = trimData(trackFld.value)
                    if (frm.elements[iCATE].checked == true && trackNo == "") {
                        alert("Please enter the tracking number.")
                        //frm.elements[iCATE].checked = false;
                        trackFld.focus();
                        isVerified = false;
                        break
                    }
                }
            }
            return isVerified;
        }

        function CheckALLTrackingExists(frm) {
            var isVerified = true;
            var iCATE = 0;
            for (iCATE = 0; iCATE < frm.elements.length; iCATE++) {
                var elementName = frm.elements[iCATE].name;
                if (frm.elements[iCATE].type == "select-one" &&
                    elementName.indexOf("chkIsTrackingReq") != -1) {
                    //var index = elementName.substring("ddcShipCompany".length, elementName.length)
                    var index = elementName.substring("chkIsTrackingReq".length, elementName.length)
                    var trackFld = eval("frmOrderFulfillmentList.txtTrackingNumber" + index);

                    //alert(index)

                    var trackNo = trimData(trackFld.value)

                    //var shipVal = frm.elements[iCATE].options[frm.elements[iCATE].options.selectedIndex].value;
                    if (trackNo == "") {
                        alert("Please enter the Tracking Number.")
                        trackFld.focus();
                        isVerified = false;
                        break
                    }
                }
            }
            return isVerified;
        }

        function DesignCenter(pkey_cust_po_master, pkey_cust_po_dtl, pkey_customer, cardsFor, DsgnMode) {
            alert('this feature removed');
            return false;
            /*var frm = document.forms['frmOrderFulfillmentList'];;
            if (DsgnMode == 'UploadDesign') {
                frmOrderFulfillmentList.DesignMode.value = "UploadDesign";
            }
            frmOrderFulfillmentList.TargetAction.value = "ModifyDesign";
            frmOrderFulfillmentList.pkey_cust_po_master.value = pkey_cust_po_master;
            frmOrderFulfillmentList.pkey_cust_po_dtl.value = pkey_cust_po_dtl;
            frmOrderFulfillmentList.pkey_customer.value = pkey_customer;
            frmOrderFulfillmentList.CardsFor.value = cardsFor;
            frmOrderFulfillmentList.submit();*/
        }

        //var notesWindow = null;
        //function DisplayNotes(pkey_cust_po_dtl, pkey_br_po_dtl) {
        //    //closeNotesWindow();
        //    var tsUrl = (new Date()).getTime();
        //    //var props = "width=500,height=420,resizable=1,scrollbars=1,toolbar=0,menubar=0";
        //    var url;
        //    url = "CustOrderNotes.aspx?pkey_cust_po_dtl=" + pkey_cust_po_dtl + "&pkey_br_po_dtl=" + pkey_br_po_dtl + "&ts=" + tsUrl;
        //    //window.location.href("CustOrderNotes.aspx?pkey_cust_po_dtl=" + pkey_cust_po_dtl + "&pkey_br_po_dtl=" + pkey_br_po_dtl + "&ts=" + tsUrl);
        //    $("#divID").dialog("open");
        //    $("#summaryframe").attr("src", url);
        //    //return false;
        //}

        //var linksWindow = null;
        //function DisplayLinks(orderindex, pkey_cust_po_dtl, pkey_br_po_dtl) {
        //    //closeNotesWindow();
        //    var tsUrl = (new Date()).getTime();
        //    var url;
        //    url = "OrderLinks.aspx?orderindex=" + orderindex + "&pkey_cust_po_dtl=" + pkey_cust_po_dtl + "&pkey_br_po_dtl=" + pkey_br_po_dtl + "&ts=" + tsUrl;
        //    $("#divID").dialog("open");
        //    $("#summaryframe").attr("src", url);
        //    //return false;
        //    //linksWindow = window.open(url, "OrderLinksWindow", props);
        //}

        function DisplayCustomerComments(pkey_cust_po_master) {
            $.fancybox({
                'width': '50%',
                'height': '80%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': 'CustomerComments.aspx?pkey_cust_po_master=' + pkey_cust_po_master + '&ts=+<%= System.DateTime.Now.Ticks.ToString()%>',
                'type': 'iframe'
            });
        }

        function DisplayAdminComments(pkey_cust_po_dtl) {

            $.fancybox({
                'width': '50%',
                'height': '80%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': 'AdminComments.aspx?pkey_cust_po_dtl=' + pkey_cust_po_dtl + '&ts=+<%= System.DateTime.Now.Ticks.ToString()%>',
                'type': 'iframe'
            });
        }

        //var pdfUploadWindow = null;
        //function DisplayPDFUpload(pkey_cust_po_dtl, pkey_br_po_dtl) {
        //    //closeNotesWindow();
        //    var tsUrl = (new Date()).getTime();
        //    //var props = "width=500,height=420,resizable=1,scrollbars=1,toolbar=0,menubar=0";
        //    var url;
        //    url = "CustOrderNotes.aspx?IsUpload=True&pkey_cust_po_dtl=" + pkey_cust_po_dtl + "&pkey_br_po_dtl=" + pkey_br_po_dtl + "&ts=" + tsUrl;
        //    //pdfUploadWindow = window.open(url, "PDFUploadWindow", props);
        //    $("#divID").dialog("open");
        //    $("#summaryframe").attr("src", url);
        //    //return false;
        //}

        function OpenSummary() {
            $.fancybox({
                'width': '80%',
                'height': '80%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': 'OrderFulfillmentSummary.aspx',
                'type': 'iframe'
            });
            return false;
        }

        function OpenCustomerProof(url) {
            $.fancybox({
                'width': '80%',
                'height': '80%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': url,
                'type': 'iframe'
            });
            return false;
        }

        function OpenPOMailLink(url) {
            $.fancybox({
                'width': '40%',
                'height': '80%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': url,
                'type': 'iframe'
            });
            return false;
        }
        

        function OpenUSDataStatusLink(url) {
            $("#divID").dialog("open");
            $("#summaryframe").attr("src", url);
            return false;
        }

        function OpenInvoice(url) {
            $.fancybox({
                'width': '60%',
                'height': '80%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': url,
                'type': 'iframe'
            });
            return false;
        }

        function OpenSOPopup(url) {
            $.fancybox({
                'width': '60%',
                'height': '80%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': url,
                'type': 'iframe'
            });
            return false;
        }

        //function closeNotesWindow() {
        //    if (notesWindow != null) {
        //        if (isNav) {
        //            if (notesWindow.window)
        //                notesWindow.close();
        //        } else {
        //            notesWindow.close();
        //            notesWindow = null;
        //        }
        //    }
        //}

    </script>
    <script>
        $(function () {
            $("#ContentPlaceHolder1_txtFromDate").datepicker();
            $("#ContentPlaceHolder1_txtSFromDate").datepicker();
            $("#ContentPlaceHolder1_txtToDate").datepicker();
            $("#ContentPlaceHolder1_txtSToDate").datepicker();
        });

        function validateSearch() {
            if ($("#ContentPlaceHolder1_txtPattern").val() == ""
                && $("#ContentPlaceHolder1_txtFromDate").val() == "" && $("#ContentPlaceHolder1_txtSFromDate").val() == ""
                && $("#ContentPlaceHolder1_txtSToDate").val() == "" && $("#ContentPlaceHolder1_txtToDate").val() == "") {
                alert('Please define the criteria for applying search.');
                return false;
            }
            if ($("#ContentPlaceHolder1_txtFromDate").val() != "" && $("#ContentPlaceHolder1_txtToDate").val() == "") {
                alert('Please enter the To Date for filtering the records.');
                return false;
            }
            if ($("#ContentPlaceHolder1_txtFromDate").val() == "" && $("#ContentPlaceHolder1_txtToDate").val() != "") {
                alert('Please enter the From Date for filtering the records.');
                return false;
            }

            if ($("#ContentPlaceHolder1_txtSFromDate").val() != "" && $("#ContentPlaceHolder1_txtSToDate").val() == "") {
                alert('Please enter the To Date for filtering the records.');
                return false;
            }
            if ($("#ContentPlaceHolder1_txtSFromDate").val() == "" && $("#ContentPlaceHolder1_txtSToDate").val() != "") {
                alert('Please enter the From Date for filtering the records.');
                return false;
            }
            if (Date.parse($("#ContentPlaceHolder1_txtFromDate").val()) > Date.parse($("#ContentPlaceHolder1_txtToDate").val())) {
                alert("Invalid date range!");
                return false;
            }
            if (Date.parse($("#ContentPlaceHolder1_txtSFromDate").val()) > Date.parse($("#ContentPlaceHolder1_txtSToDate").val())) {
                alert("Invalid date range!");
                return false;
            }
            return true;
        }

        function QuantityAnchorClick(value)
        {
            if (value == 'Pre Press')            
                $("#ContentPlaceHolder1_ddlOrderFilter option:selected").val(1);
            if (value == 'Print Queue')
                $("#ContentPlaceHolder1_ddlOrderFilter option:selected").val(2);
            if (value == 'Supplier Queue')
                $("#ContentPlaceHolder1_ddlOrderFilter option:selected").val(3);
            if (value == 'Customer Feedback')
                $("#ContentPlaceHolder1_ddlOrderFilter option:selected").val(6);
            if (value == 'Special Processing Queue')
                $("#ContentPlaceHolder1_ddlOrderFilter option:selected").val(10);
            if (value == 'Customer Proof Queue')
                $("#ContentPlaceHolder1_ddlOrderFilter option:selected").val(11);
            __doPostBack('ctl00$ContentPlaceHolder1$ddlOrderFilter', '');
        }

        //function ApplyFilter() {
        //    $('#ContentPlaceHolder1_OrderFulfillmentGrid tr').each(function () {
        //        if ($(this).attr('data-po') === undefined) return;
        //        var poNumber = ($(this).attr('data-po')).toLowerCase();
        //        var textPattern = $("#ContentPlaceHolder1_txtPattern").val().toLowerCase();
        //        var Criteria = $("#ContentPlaceHolder1_ddlOperator option:selected").val();

        //        if ($("#ContentPlaceHolder1_txtPattern").val() != "") {
        //            if (Criteria == '=') {
        //                if (poNumber != textPattern) {
        //                    $(this).hide();
        //                    return;
        //                }
        //                $(this).show();
        //            }
        //            else if (Criteria == '<>') {
        //                if (poNumber == textPattern) {
        //                    $(this).hide();
        //                    return;
        //                }
        //                $(this).show();
        //            }
        //            else if (Criteria == 'LIKE%') {
        //                if (!isStartsWith(textPattern, poNumber)) {
        //                    $(this).hide();
        //                    return;
        //                }
        //                $(this).show();
        //            }
        //            else if (Criteria == '%LIKE') {
        //                if (!isEndsWith(textPattern, poNumber)) {
        //                    $(this).hide();
        //                    return;
        //                }
        //                $(this).show();
        //            }
        //            else {
        //                if (poNumber.indexOf(textPattern) == -1) {
        //                    $(this).hide();
        //                    return;
        //                }
        //                $(this).show();
        //            }
        //        }
        //    });
        //}

        //function isEndsWith(keyword, value) {
        //    var d = value.length - keyword.length;
        //    return d >= 0 && value.lastIndexOf(keyword) === d;
        //}

        //function isStartsWith(keyword, value) {
        //    return value.lastIndexOf(keyword) == 0;
        //}

        //$(function () {
        //    setTimeout(function () {
        //        alert($("#ContentPlaceHolder1_ddlFilterFor option:selected").index());
        //        $("#ContentPlaceHolder1_txtPattern").jSuggest({
        //            default_text: 'Enter Value',
        //            terms_url: 'SearchSuggestion.aspx?input=%input%&sid=orderfulfillment&Field=' + $("#ContentPlaceHolder1_ddlFilterFor option:selected").index(),
        //            limit: 10,
        //            li_onclick: 'searchSuggestion'
        //        });
        //    }, 1000);
        //});

        //function SetSuggestion() {
        //    $("#ContentPlaceHolder1_txtPattern").jSuggest({
        //        default_text: '',
        //        terms_url: 'SearchSuggestion.aspx?input=%input%&sid=orderfulfillment&Field=' + $("#ContentPlaceHolder1_ddlFilterFor option:selected").index(),
        //        limit: 10,
        //        li_onclick: 'searchSuggestion'
        //    });
        //}

        //function searchSuggestion() {
        //    __doPostBack('ctl00$ContentPlaceHolder1$btnApply', '');
        //}

        function SearchText() {
            $(".autosuggest").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "OrderFulfillment.aspx/GetAutoCompleteData",
                        data: "{'input':'" + $("#ContentPlaceHolder1_txtPattern").val() + "', field: '" + $("#ContentPlaceHolder1_ddlFilterFor option:selected").index() + "'}",
                        dataType: "json",
                        success: function (data) {
                            response(data.d);
                        },
                        error: function (result) {
                            alert("Error");
                        }
                    });
                },
                select: function (e, ui) {
                    $("#ContentPlaceHolder1_txtPattern").val(ui.item.value);
                    __doPostBack('ctl00$ContentPlaceHolder1$btnApply', '');
                    return true;
                }
            });
        }

        function ChangeIndex() {
            $("#ContentPlaceHolder1_txtPattern").val("");
        }
    </script>

    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Customer Service</li>
            <li>Order Fulfillment</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Order Fulfillment</h2>
        <div>
            <% if (UnprintedOrdersCount > 0)
               { %>
            <p>
                <span>&nbsp;You have <b><%= UnprintedOrdersCount%></b> orders in your print queue. To see a summary of your orders run <a href="#" onclick="OpenSummary();">click here</a><br />
                    <br />
                    &nbsp;<%= UnPrintedOrderSummary%></span>
            </p>
            <%}%>
        </div>
        <form class="form" runat="server" id="frmOrderFulfillmentList">
            <div class="form_sep_left" style="float: left;">
                <dl>
                    <dt>Site</dt>
                    <dd>
                        <asp:DropDownList name="ddlSite" ID="ddlSite" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSite_SelectedIndexChanged">
                            <asp:ListItem Value="-1">All Sites</asp:ListItem>
                            <asp:ListItem Value="1">BestPrintBuy</asp:ListItem>
                            <asp:ListItem Value="2">SparkPrints</asp:ListItem>
                        </asp:DropDownList>
                    </dd>
                </dl>
                <dl>
                    <dt>Products </dt>
                    <dd>
                        <asp:DropDownList name="ddlProducts" ID="ddlProducts" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlProducts_SelectedIndexChanged">
                        </asp:DropDownList>
                    </dd>
                </dl>
                <dl>
                    <dt>Filter For orders  </dt>
                    <dd>
                        <asp:DropDownList name="ddlOrderFilter" ID="ddlOrderFilter" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlOrderFilter_SelectedIndexChanged">
                        </asp:DropDownList>
                    </dd>
                </dl>
                <dl>
                    <dt>From Date  </dt>
                    <dd>
                        <asp:TextBox CssClass="text" name="txtFromDate" ID="txtFromDate" runat="server"></asp:TextBox>
                    </dd>
                </dl>
                <dl>
                    <dt>Schedule From Date  </dt>
                    <dd>
                        <asp:TextBox CssClass="text" name="txtSFromDate" ID="txtSFromDate" runat="server"></asp:TextBox>
                    </dd>
                </dl>
                <dl>
                    <dt>Filter List for  </dt>
                    <dd>
                        <asp:DropDownList name="ddlFilterFor" ID="ddlFilterFor" runat="server" onchange="ChangeIndex();">
                            <asp:ListItem Value="pps_customers.email_id">Email Address</asp:ListItem>
                            <asp:ListItem Value="pps_customers.first_name + ' ' + pps_customers.last_name">Customer First & Last Name</asp:ListItem>
                            <asp:ListItem Value="pps_cust_po_master.po_number">PO Number</asp:ListItem>
                            <asp:ListItem Value="pps_cust_po_dtl.po_dtl_number">PO Dtl Number</asp:ListItem>
                        </asp:DropDownList>
                    </dd>
                </dl>
            </div>
            <div class="form_sep_right mrg_top33" style="float: right;">
                <dl>
                    <dt>Product Categories</dt>
                    <dd>
                        <asp:DropDownList name="ddlProductCat" ID="ddlProductCat" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlProductCat_SelectedIndexChanged">
                        </asp:DropDownList>
                    </dd>
                </dl>
                <dl>
                    <dt>Promotion </dt>
                    <dd>
                        <asp:DropDownList name="ddlPromotion" ID="ddlPromotion" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPromotion_SelectedIndexChanged">
                        </asp:DropDownList>
                    </dd>
                </dl>
                <dl>
                    <dt>To Date  </dt>
                    <dd>
                        <asp:TextBox CssClass="text" name="txtToDate" ID="txtToDate" runat="server"></asp:TextBox>
                    </dd>
                </dl>
                <dl>
                    <dt>Schedule to Date  </dt>
                    <dd>
                        <asp:TextBox CssClass="text" name="txtSToDate" ID="txtSToDate" runat="server"></asp:TextBox>
                    </dd>
                </dl>
                <dl>
                    <dt>
                        <asp:DropDownList name="ddlOperator" ID="ddlOperator" CssClass="search" data-placeholder="Choose a Name" runat="server">
                            <asp:ListItem Value="=">Equals</asp:ListItem>
                            <asp:ListItem Value="<>">Not Equals</asp:ListItem>
                            <asp:ListItem Value="LIKE%">Starts With</asp:ListItem>
                            <asp:ListItem Value="%LIKE">Ends with</asp:ListItem>
                            <asp:ListItem Value="%LIKE%">Contains</asp:ListItem>
                        </asp:DropDownList>
                    </dt>
                    <dd>
                        <asp:TextBox CssClass="text autosuggest" name="txtPattern" ID="txtPattern" runat="server"></asp:TextBox>
                    </dd>
                </dl>
                <div class="btn_lst rgt">
                    <asp:Button runat="server" ID="btnReset" Text="Reset" CssClass="button rounded" OnClick="btnReset_Click" Visible="false" UseSubmitBehavior="false"></asp:Button>
                    <asp:Button runat="server" ID="btnApply" Text="Apply" CssClass="button rounded" OnClick="btnApply_Click" OnClientClick="return validateSearch();"></asp:Button>                    
                </div>
            </div>
            <div class="clr"></div>
            <asp:GridView ID="OrderFulfillmentGrid" runat="server" AutoGenerateColumns="false" EnableViewState="true"
                ForeColor="Black" Width="100%" PageSize="50" AllowPaging="true" AllowSorting="true"
                class="table" name="OrderFulfillmentGrid" OnPageIndexChanging="OrderFulfillmentGrid_PageIndexChanging" OnSorting="OrderFulfillmentGrid_Sorting">
                <PagerStyle HorizontalAlign="Right" CssClass="dtPage"></PagerStyle>
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />
                <Columns>
                    <asp:TemplateField HeaderText="#SL">
                        <HeaderStyle Width="6%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <span><%# GetRecordCountForPage()%></span>
                            <input type="hidden" name="POKey<%# GetRowIndex()%>" value="<%# DataBinder.Eval(Container, "DataItem.pkey_cust_po_master") %>">
                            <input type="hidden" name="PODtlKey<%# GetRowIndex()%>" value="<%# DataBinder.Eval(Container, "DataItem.pkey_cust_po_dtl") %>">
                            <input type="hidden" name="IsEmail<%# GetRowIndex()%>" value="<%# DataBinder.Eval(Container, "DataItem.email_date") %>">

                            <input type="hidden" name="CustKey<%# GetRowIndex()%>" value="<%# DataBinder.Eval(Container, "DataItem.pkey_customer") %>">
                            <input type="hidden" name="BRPODtl<%# GetRowIndex()%>" value="<%# DataBinder.Eval(Container, "DataItem.pkey_br_po_dtl") %>">
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Name" SortExpression="CustName">
                        <HeaderStyle Width="14%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate><%# DataBinder.Eval(Container, "DataItem.CustName") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="PO #" SortExpression="po_dtl_number">
                        <HeaderStyle Width="17%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <% if (FilterOrdersFor == 1 || FilterOrdersFor == 2 || FilterOrdersFor == 4 || FilterOrdersFor == 3 || FilterOrdersFor == 10)
                               { %>
                            <a class="a1b" href="#" onclick="OpenSOPopup('<%# GetPrintShopFloorInvoiceUrl(DataBinder.Eval(Container, "DataItem.pkey_cust_po_master"),DataBinder.Eval(Container, "DataItem.pkey_cust_po_dtl"),DataBinder.Eval(Container, "DataItem.pkey_customer"),DataBinder.Eval(Container, "DataItem.pkey_br_po_dtl")) %>')">
                                <%# DataBinder.Eval(Container, "DataItem.po_dtl_number") %>
                            </a>
                            <% }

                               else if (FilterOrdersFor == 11 || FilterOrdersFor == 5 || FilterOrdersFor == 8)
                               { %>
                            <font color='<%# GetShipTypeColor(DataBinder.Eval(Container, "DataItem.ship_type")) %>'>
														<%# GetPOMailLink(DataBinder.Eval(Container, "DataItem.po_dtl_number"),DataBinder.Eval(Container, "DataItem.pkey_cust_po_dtl")) %>
													</font>
                            <% }
                               else
                               { %>
                            <span class="GridItems"><%# DataBinder.Eval(Container, "DataItem.po_dtl_number") %></span>
                            <% } %>
                            <asp:HiddenField ID="hdnPO" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.po_dtl_number") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="SO #" Visible="false">
                        <HeaderStyle Width="15%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Date" SortExpression="orderdate">
                        <HeaderStyle Width="10%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <%# Convert.ToDateTime(DataBinder.Eval(Container, "DataItem.orderdate")).ToString("MM/dd/yyyy") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Back">
                        <HeaderStyle Width="6%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container, "DataItem.reverse_color") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Product" SortExpression="cards_for">
                        <HeaderStyle Width="6%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <%# Eval("cards_for").ToString() == "HH" ? "HHODF" : GetUSDataStatusLink(DataBinder.Eval(Container, "DataItem.cards_for"),DataBinder.Eval(Container, "DataItem.usleadstoken").ToString(),DataBinder.Eval(Container, "DataItem.pkey_cust_po_dtl"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Actions">
                        <HeaderStyle Width="14%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <div class="doc_ico">
                                <span id="pdfAction" runat="server">
                                    <%# GetXLPDFSURLLink(Eval("hi_res_proof_url"),Eval("photo_source"),Eval("logo_source"), Eval("cards_for").ToString(),Eval("design_mode").ToString())%>                                      
                                </span>
                                <span id="invoiceAction" runat="server">
                                    <a href="#" onclick="OpenInvoice('<%# GetPrintInvoiceUrl(DataBinder.Eval(Container, "DataItem.pkey_cust_po_master"),DataBinder.Eval(Container, "DataItem.pkey_cust_po_dtl"),DataBinder.Eval(Container, "DataItem.pkey_customer")) %>&inv=true')" class="print" title="Print Invoice"></a>
                                </span>
                                <span id="labelAction" runat="server">
                                    <a href="#" onclick="OpenInvoice('<%# GetPrintInvoiceUrl(DataBinder.Eval(Container, "DataItem.pkey_cust_po_master"),DataBinder.Eval(Container, "DataItem.pkey_cust_po_dtl"),DataBinder.Eval(Container, "DataItem.pkey_customer")) %>&inv=false')" class="box" title="Print Label"></a>
                                </span>
                                <span id="notesAction" runat="server"><a href="CustOrderNotes.aspx?pkey_cust_po_dtl=<%# DataBinder.Eval(Container, "DataItem.pkey_cust_po_dtl") %>&pkey_br_po_dtl=<%# DataBinder.Eval(Container, "DataItem.pkey_br_po_dtl") %>&ts=<%=DateTime.Now.Ticks%>" class="<%# GetNotesImageType(DataBinder.Eval(Container, "DataItem.pfnotes")) %> iframe" title="QA Notes"></a></span>
                                <span id="serviceNotes" runat="server"><a href="CustServiceNotes.aspx?pkey_cust_po_dtl=<%# DataBinder.Eval(Container, "DataItem.pkey_cust_po_dtl") %>&pkey_customer=<%# DataBinder.Eval(Container, "DataItem.pkey_customer") %>&ts=<%=DateTime.Now.Ticks%>" class="<%# GetServiceNotesImageType(DataBinder.Eval(Container, "DataItem.pkey_cust_po_dtl")) %> iframe" title="Customer Service Notes"></a></span>
                                <div <%# CheckInventoryProduct(Eval("fkey_inventory_item"))%>><span id="assetAction" runat="server"><a href="CustOrderNotes.aspx?IsUpload=True&pkey_cust_po_dtl=<%# DataBinder.Eval(Container, "DataItem.pkey_cust_po_dtl") %>&pkey_br_po_dtl=<%# DataBinder.Eval(Container, "DataItem.pkey_br_po_dtl") %>&ts=<%=DateTime.Now.Ticks%>" class="pdf_up iframe" title="Upload Asset"></a></span></div>
                            </div>
                            <br />
                            <div>
                                <a class="iframe" href="OrderLinks.aspx?orderindex=<%# GetRowIndex()%>&pkey_cust_po_dtl=<%# DataBinder.Eval(Container, "DataItem.pkey_cust_po_dtl") %>&pkey_br_po_dtl=<%# DataBinder.Eval(Container, "DataItem.pkey_br_po_dtl") %>&ts=<%=DateTime.Now.Ticks%>">All Assets</a>
                            </div>
                            <input type="hidden" name="hidPDF<%# GetRowIndex()%>" value="<%# GetHiresProofURL(Eval("hi_res_proof_url"), Eval("cards_for").ToString()) %>">
                            <input type="hidden" name="hidcsvUrl<%# GetRowIndex()%>" value="<%# GetCsvURL(Eval("csv_FileNM"), Eval("cards_for").ToString()) %>">
                            <input type="hidden" name="is_direct_mail<%# GetRowIndex()%>" value="<%# DataBinder.Eval(Container, "DataItem.is_direct_mail") %>">
                            <input type="hidden" name="hidLogoUrl<%# GetRowIndex()%>" value="<%#GetHiResImage(DataBinder.Eval(Container, "DataItem.logo_source")) %>">
                            <input type="hidden" name="hidPhotoUrl<%# GetRowIndex()%>" value="<%# GetHiResImage(DataBinder.Eval(Container, "DataItem.photo_source")) %>">
                            <input type="hidden" name="hidFrontImageURL<%# GetRowIndex()%>" value="<%# DataBinder.Eval(Container, "DataItem.front_image_url") %>">
                            <input type="hidden" name="hidBackImageURL<%# GetRowIndex()%>" value="<%# DataBinder.Eval(Container, "DataItem.reverse_image_url") %>">
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Invoice" Visible="false">
                        <ItemTemplate></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Label" Visible="false">
                        <ItemTemplate></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Ship" SortExpression="ship_type">
                        <HeaderStyle Width="5%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <%# GetShipType(DataBinder.Eval(Container, "DataItem.ship_type")) %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Shipper">
                        <HeaderStyle Width="5%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <%# GetShipper(DataBinder.Eval(Container, "DataItem.carrier_service_name"), DataBinder.Eval(Container, "DataItem.ship_type")) %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Track #">
                        <ItemTemplate>
                            <div style="width: 120px; padding-top: 2px"></div>
                            <input style="width: 110px;" name="txtTrackingNumber<%# GetRowIndex()%>" id="txtTrackingNumber<%# GetRowIndex()%>" <%# GetIsCanceledForOther(Eval("is_order_canceled"))%> maxlength="50" <%# IsDBNull(Eval("email_date")) ? "" : "disabled" %> value="<%# DataBinder.Eval(Container, "DataItem.tracking_number") %>" type="text">
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="<a href='#' class='whiteHeader' title='Send Shipped Mail Confirmation'>Send Mail</a>">
                        <HeaderStyle Width="7%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <input name="chkPrinted<%# GetRowIndex()%>" type="checkbox" class="chk" value='<%# DataBinder.Eval(Container, "DataItem.pkey_cust_po_dtl") %>' <%# GetPrintStatus(Convert.ToBoolean(Eval("printstatus")), Eval("automated_ship").ToString())%>>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="<a href='#' class='whiteHeader' title='Cancel Order'>Cancel</a>">
                        <ItemTemplate>
                            <input name="chkIsOrderCanceled<%# GetRowIndex()%>" <%= GetIsCanceledForOther(Eval("is_order_canceled"))%> class="chk" type="checkbox" <%= Convert.ToBoolean(Eval(".is_order_canceled"))?"checked":""%>>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="<a href='#' class='whiteHeader' title='Hold Queue'>Hold</a>">
                        <ItemTemplate>
                            <input name="chkIsQueueHeld<%# GetRowIndex()%>" value='<%# DataBinder.Eval(Container, "DataItem.pkey_cust_po_dtl") %>' <%= FilterOrdersFor == 10 ? "checked" : ""%> class="chk" type="checkbox">
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="<a href='#' class='whiteHeader' title='Edit Designs'>Edit</a>">
                        <ItemTemplate>
                            <a href="javascript:DesignCenter('<%# DataBinder.Eval(Container, "DataItem.pkey_cust_po_master") %>','<%# DataBinder.Eval(Container, "DataItem.pkey_cust_po_dtl") %>','<%# DataBinder.Eval(Container, "DataItem.pkey_customer")%>','<%# DataBinder.Eval(Container, "DataItem.cards_for") %>','<%# DataBinder.Eval(Container, "DataItem.design_mode") %>')" class="a1b">Modify</a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="<a href='#' class='whiteHeader' title='Allow User to Modify'>AUMD</a>">
                        <ItemTemplate>
                            <input name="chkCanUserModify<%# GetRowIndex()%>" class="chk" type="checkbox" value='<%# Eval("pkey_cust_po_dtl") %>' <%# Convert.ToBoolean(Eval("can_customer_modify_design")) ? "checked" : ""%> <%# GetPrintStatus(Convert.ToBoolean(Eval("printstatus")), Eval("automated_ship").ToString())%>>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="<a href='#' class='whiteHeader' title='Print Associate Notes'>Notes</a>" Visible="false">
                        <ItemTemplate>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="<a href='#' class='whiteHeader' title='Upload Asset'>Upload<br/>Asset</a>" Visible="false">
                        <HeaderStyle Width="5%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate><%# GetStatusDropdown(true) %></HeaderTemplate>
                        <HeaderStyle Width="100%" ForeColor="White" />
                        <ItemTemplate>
                            <%# GetStatusDropdown(false) %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Supplier">
                        <ItemTemplate>
                            <%# GetSupplierDropdown(DataBinder.Eval(Container, "DataItem.fkey_br_supplier")) %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Proof Status">
                        <HeaderStyle/>
                        <ItemTemplate>
                            <%# GetProofStatusByImage(DataBinder.Eval(Container, "DataItem.proofstatus")) %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="<a href='#' class='whiteHeader' title='Customer Comments'>CC</a>">
                        <ItemTemplate>
                            <a href="javascript:DisplayCustomerComments('<%# DataBinder.Eval(Container, "DataItem.pkey_cust_po_master") %>')" class="a1b">
                                <img src="../images/Admin/<%# GetCommetImageType(DataBinder.Eval(Container, "DataItem.HasCustComments")) %>" border="0"></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="<a href='#' class='whiteHeader' title='Admin Comments'>AC</a>">
                        <ItemTemplate>
                            <a href="javascript:DisplayAdminComments('<%# DataBinder.Eval(Container, "DataItem.pkey_cust_po_dtl") %>')" class="a1b">
                                <img src="../images/Admin/<%# GetCommetImageType(DataBinder.Eval(Container, "DataItem.HasAdminComments")) %>" border="0"></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <div class="ttl"><b>Total Number of Orders : </b><%=OrderCount%></div>
            <div class="btn_lst">
                <div class="lft">
                    <%if (isProcessInitiated)
                      { %>
                    <asp:Button runat="server" ID="btnSendMail" CssClass="button rounded" Text="Confirmed List" OnClientClick="return SendEmails()" OnClick="btnSendMail_Click"></asp:Button>
                    <% } %>&nbsp;&nbsp;
                        <input type="button" class="button rounded" value="Shipping Recon" name="cmdShipRecon" id="cmdShipRecon" onclick="ShipRecon();" />&nbsp;&nbsp;
                        <input type="button" style="visibility: hidden" class="button rounded" value="Copy Assets to FTP" name="cmdCopyAssetstoFTP" id="cmdCopyAssetstoFTP" onclick="CopyAssetstoFTP();" />
                    <input type="hidden" name="recordCounter" value="<%= GetRowIndex()%>" />&nbsp;
                        <input type="hidden" name="OrderKeys" id="OrderKeys" />
                    <input type="hidden" name="hidXPrintValue" id="hidXPrintValue" />
                    <input type="hidden" name="hdnPOKeys" id="hdnPOKeys" runat="server" />
                </div>
                <div class="rgt">
                    <iframe id="ifrmUSLeadsStatus" src="CheckUSLeadsList.aspx?ts=<%# System.DateTime.Now.ToString()%>" width="0" height="0" frameborder="no"></iframe>
                    <%if (FilterOrdersFor < -1)
                      { %>
                    <asp:Button runat="server" ID="btnHold" Text="Hold" CssClass="button rounded"></asp:Button>
                    <% }
                      else if (FilterOrdersFor == 10)
                      { %>
                    <asp:Button runat="server" ID="btnUnHold" Text="Save" CssClass="button rounded" OnClientClick="return ReleaseHoldOrders()" OnClick="btnHold_Click"></asp:Button>
                    <% } %>
                    <asp:Button runat="server" ID="btnClose" Text="Close" CssClass="button rounded" OnClick="btnClose_Click"></asp:Button>&nbsp;&nbsp;
                        <input type="button" class="button rounded" value="Export Orders" name="cmdExport" id="cmdExport" onclick="ExportOrders();" />&nbsp;&nbsp;
                        <asp:HiddenField ID="hdnRecordCount" runat="server" />
                    <%if (isPendingOrdersFilter)
                      { %>
                    <asp:Button runat="server" ID="btnSave" Text="Save" CssClass="button rounded" OnClick="btnSave_Click" OnClientClick="return SaveOrders();"></asp:Button>
                    <% } %>
                    <% if (isDisplayXsell && (FilterOrdersFor == 1 || FilterOrdersFor == 2 || FilterOrdersFor == 3 || FilterOrdersFor == 10))
                       { %>
                    <input type="button" class="button rounded" value="X-Copy" name="cmdXCopy" id="cmdXCopy" onclick="return XCopy();" />&nbsp;&nbsp;
						<input type="button" class="button rounded" value="X-Print" name="cmdXPrint" id="cmdXPrint" onclick="return XPrintValue();" />&nbsp;&nbsp;
                        <input type="button" class="button rounded" value="X-Print Manifest" name="cmdXPrintManifest" id="cmdXPrintManifest" onclick="return XPrintManifest();" />&nbsp;&nbsp;
						<% } %>
                    <% if (FilterOrdersFor == 11) { %>
						<asp:Button CssClass="button rounded" Text="Send Notification Mail to Customer" ID="btnSendNotification" OnClientClick="return SendNotificationMailToCustomer();"
								   OnClick="btnSendNotification_Click"	name="btnSendNotification" runat="server" ></asp:Button>&nbsp;&nbsp;
						<%}%>
                </div>
            </div>
            <div id="divID" title="Notes" style="display: none;">
                <iframe id="summaryframe" style="width: 850px; height: 530px; border: none;"></iframe>
            </div>
        </form>
    </div>
</asp:Content>
