﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="CatalogReqList.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Customers.CatalogReqList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function savecatalog() {
            if ('<%=HasAccess("CatalogReqList", "edit")%>' == 'False') {
                alert('User does not have access.');
                return false;
            }
            return true;
        }

        function SetOptSent(value) {
            $("#ContentPlaceHolder1_OptSentVal").val(value);
        }

        function OpenCatalogReqDialog(pkeyCatalogReq) {
            if ('<%=HasAccess("CatelogReqPopup", "view")%>' == 'False') {
                alert('User does not have access.');
                return false;
            }
            else {
                var url = "../Customers/CatelogReqPopup.aspx?pkeyCatalogReq=" + pkeyCatalogReq;
                $.fancybox({
                    'width': '40%',
                    'height': '80%',
                    'autoScale': true,
                    'transitionIn': 'fade',
                    'transitionOut': 'fade',
                    'href': url,
                    'type': 'iframe'
                });
                return false;
            }
        }

        $(function () {
            $("#ContentPlaceHolder1_txtFromDate").datepicker();
            $("#ContentPlaceHolder1_txtToDate").datepicker();

            $("#frmCatelogRequest").validate();
        });
        function validate_twoDates() {
            var dateStart = $("#ContentPlaceHolder1_txtFromDate").val();
            var dateEnd = $("#ContentPlaceHolder1_txtToDate").val();
            if (Date.parse(dateStart) > Date.parse(dateEnd)) {
                alert("Invalid date range!");
                return false;
            }
        }
    </script>
    <div class="contant_hldr">
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Customer Service</li>
            <li>Catalog Request</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Catalog Request List</h2>
        <form  runat="server" id="frmCatelogRequest">
   
            <table cellpadding="0" cellspacing="2" border="0" width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="15%" >From Date</td>
                        <td width="15%" >
                            <asp:TextBox CssClass="text-box date" Width="250px" name="txtFromDate" ID="txtFromDate" runat="server"></asp:TextBox></td>
                        <td width="15%" class="a_cnt" style="padding-left: 15px">To Date</td>
                        <td width="10%">
                            <asp:TextBox CssClass="text-box date" Width="250px" name="txtToDate" ID="txtToDate" runat="server"></asp:TextBox></td>
                        <td width="9%" style="padding-left: 15px"></td>
                        <td width="15%"></td>
                        <td width="25%"></td>
                    </tr>
                    <tr>
                        <td><asp:RadioButton runat="server"  ID="rbtnReqUnsent" onclick="SetOptSent(0);" value="" GroupName="OptSent" ></asp:RadioButton>
                           Request Unsent
                                </td>
                        <td style="padding-left: 15px">
                           <asp:RadioButton runat="server" ID="rbtnSenttoCustomer" onclick="SetOptSent(1);" value="" GroupName="OptSent"></asp:RadioButton>
                            Sent to Customer
                        </td>
                        <td class="a_rgt" colspan="2">
                            <asp:Button runat="server" CssClass="button rounded" ID="btnGo" Text="GO" OnClientClick="return validate_twoDates();" OnClick="btnGo_Click"></asp:Button>
                             <asp:Button runat="server" CssClass="button rounded" ID="btnReset" Text="Reset" OnClick="btnReset_Click" UseSubmitBehavior="false"></asp:Button></td>              
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>           
            <div class="clr"></div>
            <asp:GridView ID="CatelogReqGrid" runat="server" AutoGenerateColumns="false" EnableViewState="true"
                ForeColor="Black" Width="100%" PageSize="20" AllowPaging="true" AllowSorting="true"
                class="table" name="CatelogReqGrid" OnPageIndexChanging="CatelogReqGrid_PageIndexChanging" OnSorting="CatelogReqGrid_Sorting">
                <PagerStyle HorizontalAlign="Right" CssClass="dtPage"></PagerStyle>
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />
                <Columns>
                    <asp:TemplateField HeaderText="#SL">
                        <HeaderStyle Width="10%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <span><%# GetRecordCountForPage()%></span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Date" HeaderStyle-ForeColor="White" ItemStyle-Width="20%" SortExpression="ReqDate" DataField="ReqDate" DataFormatString="{0:MM/dd/yyyy}"></asp:BoundField>
                    <asp:TemplateField HeaderText="Name" SortExpression="Customer">
                        <HeaderStyle Width="30%" ForeColor="White" />
                        <ItemTemplate>
                            <a href="#" class="a1b" title="Edit" onclick='javascript:OpenCatalogReqDialog(<%#Eval("pkey_catalogReq") %>)'><%#Eval("Customer") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Site" SortExpression="Site">
                        <HeaderStyle Width="30%" ForeColor="White" />
                        <ItemTemplate>
                            <asp:Label ID="lblSite" runat="server" Text='<%#Eval("Site") %>'></asp:Label>
                            <asp:HiddenField ID="StrCatSentKeys" runat="server" Value='<%#Eval("pkey_catalogReq") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Sent">
                        <HeaderStyle Width="10%" ForeColor="White" />
                        <ItemTemplate>
                                <asp:CheckBox ID="chkSent" CssClass="chk" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
             <asp:Label ID="lblTotalRecords" runat="server" Font-Bold="true"></asp:Label>
            <div class="btn_lst">
                <div class="rgt">
                    <asp:HiddenField ID="OptSentVal" runat="server" /> 
                    <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" UseSubmitBehavior="false" OnClick="btnClose_Click"></asp:Button>
                    <asp:Button runat="server" ID="btnSave" CssClass="button rounded" Text="Save" OnClientClick="return savecatalog();" OnClick="btnSave_Click"></asp:Button>
                     <asp:Button runat="server" ID="btnExportTocsv" CssClass="button rounded" Text="Export to CSV" OnClick="btnExportTocsv_Click"></asp:Button>
                </div>
            </div>
        </form>
    </div>
</asp:Content>
