﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="USLeadsStatus.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Customers.USLeadsStatus" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Best Print Buy</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" media="all" href="../css/bestprintbuy_style.css" />
    <link rel="stylesheet" media="all" href="../css/jquery.fancybox.css" />
    <script src="../Scripts/jquery.js"></script>
    <script src="../Scripts/common.js"></script>
    <script src="../Scripts/ddsmoothmenu.js"></script>
    <script src="../Scripts/plugin/fancybox/jquery.fancybox.js"></script>
    <!--[if lt IE 9]>
			<script src="../Scripts/html5.js"></script>
		<![endif]-->
    <!--[if lte IE 7]> <html class="ie7"> <![endif]-->
    <!--[if IE 8]>     <html class="ie8"> <![endif]-->
    <!--[if IE 9]>     <html class="ie9"> <![endif]-->
    <!--[if IE 10]>     <html class="ie10"> <![endif]-->
    <!--[if !IE]><!-->
    <!--<![endif]-->
         <script>
             function UploadConfirm() {
                 //alert($("#uploadPdf").val());
                 if ($("#uploadPdf").val() == "") {
                     alert("Please select a file of PDF Type to Upload.");
                     $("#uploadPdf").focus();
                     return false;
                 }
                 return true;
             }
    </script>
</head>
    
<body id="Body" runat="server">
   <!-- Wrapper starts -->
    <div class="popup-wrapper" runat="server">
        <div class="popup-title">
           US Leads Status
        </div>
        <!-- Header Ends -->
        <div class="popup-contnt">
            <!-- Menu starts -->
            <form id="Form1" runat="server">
                <div>
                    <asp:Literal ID="ltrlLeadsData" runat="server"></asp:Literal>
                </div>
                <table>
                    <tr>
                        <td style="float:right;">
                            <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClientClick="parent.$.fancybox.close(); return false;"></asp:Button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</body>
</html>
