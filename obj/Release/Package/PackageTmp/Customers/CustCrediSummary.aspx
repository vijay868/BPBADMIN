﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustCrediSummary.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Customers.CustCrediSummary" %>

<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->

<!-- Consider adding a manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<head id="Head1" runat="server">
    <title>Best Print Buy</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" media="all" href="../css/bestprintbuy_style.css" />
    <link rel="stylesheet" media="all" href="../css/jquery.fancybox.css" />
    <link rel="stylesheet" href="../css/jquery-ui.css" />
    <script src="../Scripts/jquery.js"></script>
    <script src="../Scripts/jquery-ui.js"></script>
    <script src="../Scripts/common.js"></script>
    <script src="../Scripts/ddsmoothmenu.js"></script>
    <script src="../Scripts/plugin/fancybox/jquery.fancybox.js"></script>
    <script src="../Scripts/jquery.validate.js"></script>
    <!--[if lt IE 9]>
			<script src="../Scripts/html5.js"></script>
		<![endif]-->
    <!--[if lte IE 7]> <html class="ie7"> <![endif]-->
    <!--[if IE 8]>     <html class="ie8"> <![endif]-->
    <!--[if IE 9]>     <html class="ie9"> <![endif]-->
    <!--[if IE 10]>     <html class="ie10"> <![endif]-->
    <!--[if !IE]><!-->

    <!--<![endif]-->
    <script>
        function ExportData() {
            var url = "ExportOrders.aspx?CustomerCredit=true&ts=<%# DateTime.Now.Ticks%>";
            window.open(url, "ExportData", "resizable=1,scrollbars=1,toolbar=0,menubar=0");
        }
    </script>
</head>

<body id="Body" runat="server">
    <!-- Wrapper starts -->
    <div id="Div1" class="popup-wrapper" runat="server">
        <div class="popup-title">
            Customer Credit Summary
        </div>
        <!-- Header Ends -->
        <div class="popup-contnt" style="padding-bottom: 40px">
            <!-- Menu starts -->
            <form id="frmCreditSummary" runat="server">
                <table class="tablep">
                    <thead>
                        <tr>
                            <th width="3%"></th>
                            <th>CustomerEmail</th>
                            <th>Product Name</th>
                            <th>Credit Amount</th>
                            <th>Availed Amount</th>
                            <th>Balance Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rptrCustomerCreditSummary" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td></td>
                                    <td>
                                        <%#Eval("email_id") %>
                                    </td>
                                    <td>
                                        <%#Eval("customer_name") %>
                                    </td>
                                    <td>
                                        <%#Eval("credit_amount") %>
                                    </td>
                                    <td>
                                        <%#Eval("availed_amount") %>
                                    </td>
                                    <td>
                                        <%# Convert.ToDecimal(Eval("credit_amount")) - Convert.ToDecimal(Eval("availed_amount")) %>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
                <div class="btn_lst">
                    <div class="rgt">
                        <asp:Button runat="server" CssClass="button rounded" ID="btnClose" Text="Close" OnClientClick="parent.$.fancybox.close(); return false;"></asp:Button>
                        <asp:Button runat="server" ID="btnExportExcel" CssClass="button rounded" Text="Export Excel" OnClientClick="javascript:ExportData();"></asp:Button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</body>
</html>
