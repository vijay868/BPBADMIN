﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomerComments.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Customers.CustomerComments" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Best Print Buy</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" media="all" href="../css/bestprintbuy_style.css" />
    <link rel="stylesheet" media="all" href="../css/jquery.fancybox.css" />
    <script src="../Scripts/jquery.js"></script>
    <script src="../Scripts/common.js"></script>
    <script src="../Scripts/ddsmoothmenu.js"></script>
    <script src="../Scripts/plugin/fancybox/jquery.fancybox.js"></script>
    <!--[if lt IE 9]>
			<script src="../Scripts/html5.js"></script>
		<![endif]-->
    <!--[if lte IE 7]> <html class="ie7"> <![endif]-->
    <!--[if IE 8]>     <html class="ie8"> <![endif]-->
    <!--[if IE 9]>     <html class="ie9"> <![endif]-->
    <!--[if IE 10]>     <html class="ie10"> <![endif]-->
    <!--[if !IE]><!-->
    <!--<![endif]-->
    <script>
        function validateform() {
            if (document.getElementById("txtComments").value == '') {
                alert('Please Enter Comments');
                document.getElementById("txaComments").focus();
                return false;;
            }
            return true;
        }
    </script>
</head>
<body id="Body" runat="server">
    <!-- Wrapper starts -->
    <div id="Div1" class="popup-wrapper" runat="server">
        <div class="popup-title">
            Customer Comments
        </div>
        <!-- Header Ends -->
        <div class="popup-contnt">
            <!-- Menu starts -->
            <form id="Form1" runat="server">
                <table>
                    <tr>
                        <td width="40%">
                            <label>Comments</label></td>
                        <td>
                            <asp:TextBox runat="server" ID="txtComments" style="resize:none;" TextMode="MultiLine" Rows="8" Columns="50" disabled="true" Text='<%# GetValue("ac_message") %>'></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Image1</label></td>
                        <td>
                            <%# GetImageUrl("ac_image1") %>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Image2</label></td>
                        <td>
                            <%# GetImageUrl("ac_image2") %>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Image3</label></td>
                        <td>
                            <%# GetImageUrl("ac_image3") %>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="2">
                            <asp:Button runat="server" ID="Button1" CssClass="button rounded" Text="Close" OnClientClick="parent.$.fancybox.close(); return false;"></asp:Button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</body>
</html>
