﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExportOrderOptions.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Customers.ExportOrderOptions" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Best Print Buy</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" media="all" href="../css/bestprintbuy_style.css" />    
	<link rel="stylesheet" media="all" href="../css/jquery.fancybox.css" />
	<script src="../Scripts/jquery.js"></script>
	<script src="../Scripts/common.js"></script>
    <script src="../Scripts/ddsmoothmenu.js"></script>
	<script src="../Scripts/plugin/fancybox/jquery.fancybox.js"></script>
    <!--[if lt IE 9]>
			<script src="../Scripts/html5.js"></script>
		<![endif]-->
    <!--[if lte IE 7]> <html class="ie7"> <![endif]-->
    <!--[if IE 8]>     <html class="ie8"> <![endif]-->
    <!--[if IE 9]>     <html class="ie9"> <![endif]-->
    <!--[if IE 10]>     <html class="ie10"> <![endif]-->
    <!--[if !IE]><!-->
    <!--<![endif]-->

        <script>
            function ExportData(action) {
                //frm = document.forms['frmExportList'];

                //alert('1');
                //alert(trimData(frm.txtFromDate.value));
                if (document.getElementById("txtFromDate").value == '' && document.getElementById('txtToDate').value == '') {
                    alert("Please enter fromdate and todate");
                    return;
                }

                //var arr = new Array("txtFromDate,n,date,MSG0010_0,01/01/1900,12/31/9999,From Date",
                //  "txtToDate,n,date,MSG0010_0,01/01/1900,12/31/9999,To Date");
                //if (!validForm(frm, arr)) {
                //    return;
                //}

                if (document.getElementById('txtToDate').value == '' && document.getElementById("txtFromDate").value != '') {
                    alert("Please enter to date");
                    frm.txtToDate.focus();
                    return;
                }
                if (document.getElementById('txtToDate').value != '' && document.getElementById("txtFromDate").value == '') {
                    alert("Please enter from date");
                    frm.txtFromDate.focus();
                    return;
                }

                //if (!dateRange(frm.txtToDate, frm.txtFromDate)) {
                //    alert(MSG0011_0);
                //    frm.txtFromDate.focus();
                //    return;
                //}

                //document.frmExportList.TargetAct.value="Export"		    
                //document.frmExportList.submit();
                var ship = -1;
                if (document.getElementById("cboShip1").checked) ship = "1";
                if (document.getElementById("cboShip2").checked) ship = "2";
                if (document.getElementById("cboShip7").checked) ship = "7";
                var url = "ExportOrders.aspx?fromDate=" + document.getElementById("txtFromDate").value;
                /*if(action != ''){
                  url = "ExportManifests.aspx?fromDate="+trimData(frm.txtFromDate.value);
                }*/

                url = url + "&toDate=" + document.getElementById('txtToDate').value;
                url = url + "&ship=" + ship;
                url = url + "&Action=" + action;

                window.open(url, "ExportData", "resizable=1,scrollbars=1,toolbar=0,menubar=0");
                return false;
            }
    </script>

</head>
<body id="Body" runat="server">
    <div id="Div1" class="popup-wrapper" runat="server">
        <div class="popup-title">	
           Export Orders
			</div>
        <!-- Header Ends -->
        <div class="popup-contnt">
            <!-- Menu starts -->
            <form id="Form1" runat="server">
                    <table>
                        <tr>
                            <td><b>Shipping Option :</b></td>
                            <td>
                                <input type="radio" name="cboShip" id="cboShip1" value="1" /><label for="cboShip1">One Day</label></td>
                            <td>
                                <input type="radio" name="cboShip" id="cboShip2" value="2" /><label for="cboShip2">Two Days</label></td>
                            <td>
                                <input type="radio" name="cboShip" id="cboShip7" value="7" /><label for="cboShip7">Seven Days</label></td>
                            <td>
                                <input type="radio" name="cboShip" id="cboShipall" value="-1" checked="checked" /><label for="cboShipall">All</label></td>
                         </tr>
                        <tr>
                            <td colspan="5" style="text-align:center;">
                                <asp:Button runat="server" CssClass="button rounded" ID="btnSupplierManifest" Text="Export Supplier Manifest" OnClientClick="return ExportData('SupplierManifest')"></asp:Button>
                        <asp:Button runat="server" CssClass="button rounded" ID="btnShippingManifest" Text="Export Shipping Manifest" OnClientClick="return ExportData('ShipManifest')"></asp:Button>
                        <asp:Button runat="server" CssClass="button rounded" ID="btnExcel" Text="Export to Excel" OnClientClick="return ExportData('')"></asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5" style="text-align:center;">
                                <asp:Button runat="server" CssClass="button rounded" ID="btnNewSupplierManifest" Text="Export New Supplier Manifest" OnClientClick="return ExportData('SupManifestPCode')"></asp:Button>
                        <asp:Button runat="server" CssClass="button rounded" ID="btnAddressAuditedSupplierManfiest" Text="Export Address Audited Supplier Manfiest" OnClientClick="return ExportData('SupManifestPCodeAudit')"></asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5" style="text-align:center;">
                                 <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClientClick="parent.$.fancybox.close(); return false;"></asp:Button>
                            </td>
                        </tr>
                    </table>
                    <asp:HiddenField ID="txtFromDate" runat="server" Value="" />
                    <asp:HiddenField ID="txtToDate" runat="server" Value="" />
            </form>
        </div>
    </div>
</body>
</html>
