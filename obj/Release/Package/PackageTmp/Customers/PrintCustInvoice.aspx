﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintCustInvoice.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Customers.PrintCustInvoice" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">
        function printWindow() {
            bV = parseInt(navigator.appVersion)
            if (bV >= 4) window.print()
        }
		</script>
</head>
<body bgcolor="#e5e5e5" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0">
    <!-- Master Table -->
    <%       
        bool isInvoice = true;
        //if Request("inv") <> "" Then
        //    isInvoice = Convert.ToBoolean(Request("inv"))
        //End If
        isInvoice = Convert.ToBoolean(Request.QueryString["inv"]);
        if (!isInvoice)
        {
    %>
    <table cellspacing="0" cellpadding="1" width="750" bgcolor="#00526e" border="0">
        <tbody>
            <tr>
                <td>
                    <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#ffffff" border="0">
                        <tbody>
                            <tr>
                                <td>
                                    <!-- Header -->
                                    <table cellspacing="0" cellpadding="10" width="98%" border="0">
                                        <tbody>
                                            <tr>
                                                <td align="right" width="485"><a href="../Account/DashBoard.aspx">
                                                    <img height="30" alt="BestPrint Buy.com" src="../Images/bpb_logo_Black.png" width="210"
                                                        border="0"></a><br>
                                                    <table width="210" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td align="left" class="head">1506 Providence Highway,#29<br>
                                                                Norwood, MA 02062.<br>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td class="bodytext" valign="top" align="right"><a href="javascript:printWindow()">
                                                    <img height="29" alt="Print this Label" src="../Images/print1.gif" border="0"></a><br>
                                                    <br>
                                                    <input class="buttons grey" type="button" value="Close" onclick="parent.$.fancybox.close(); return false;"
                                                        name="Home"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- /Header -->
                                    <!-- Invoice -->
                                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <!-- address & order ID & Print Button -->
                                                                    <table cellspacing="0" cellpadding="5" width="98%" border="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="bodytext" align="center" colspan="3">
                                                                                    <hr size="1">
                                                                                    S H I P P I N G &nbsp; &nbsp; L A B E L
																						<hr size="1">
                                                                                </td>
                                                                                <tr>
                                                                                    <td align="center" width="45%" class="bodytext" valign="top">&nbsp;</td>
                                                                                    <td width="10%" nowrap class="bodytext" valign="top">
                                                                                        <asp:Label ID="lblShipName" runat="server"></asp:Label><br>
                                                                                        <asp:Label ID="lblShipCompanyName" runat="server"></asp:Label><br>
                                                                                        <asp:Label ID="lblShipAddress1" runat="server"></asp:Label><br>
                                                                                        <asp:Label ID="lblShipAddress2" runat="server"></asp:Label><br>
                                                                                        <asp:Label ID="lblShipCityZip" runat="server"></asp:Label><br>
                                                                                        <asp:Label ID="lblShipStateCountry" runat="server"></asp:Label><br>
                                                                                    </td>
                                                                                    <td align="center" width="45%" class="bodytext" valign="top">&nbsp;</td>
                                                                                </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    <!-- address & order ID & Print Button -->
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <p></p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- /Master Table -->
                </td>
            </tr>
        </tbody>
    </table>
    <%}
        else
        {%>
    <table cellspacing="0" cellpadding="1" width="750" bgcolor="#00526e" border="0">
        <tbody>
            <tr>
                <td>
                    <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#ffffff" border="0">
                        <tbody>
                            <tr>
                                <td>
                                    <!-- Header -->
                                    <table cellspacing="0" cellpadding="10" width="98%" border="0">
                                        <tbody>
                                            <tr>
                                                <td><a href="../Account/DashBoard.aspx">
                                                    <img height="30" alt="BestPrint Buy.com" src="../Images/bpb_logo_Black.png" width="210"
                                                        border="0"></a></td>
                                                <td class="bodytext" valign="top" align="right"><a href="javascript:printWindow()">
                                                    <img height="29" alt="Print this Invoice" src="../Images/print.gif" width="113"
                                                        border="0"></a><br>
                                                    <br>
                                                    <input class="buttons grey" type="button" value="Close" onclick="parent.$.fancybox.close(); return false;"
                                                        name="Home"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- /Header -->
                                    <!-- Invoice -->
                                    <table cellspacing="10" cellpadding="0" width="100%" border="0">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <!-- address & order ID & Print Button -->
                                                                    <table cellspacing="0" cellpadding="0" width="98%" border="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="bodytext" align="center" colspan="3">
                                                                                    <hr size="1">
                                                                                    Y O U R &nbsp; &nbsp; I N V O I C E
																						<hr size="1">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="bodytext" valign="top"><b>Bill to:</b><br>
                                                                                    <asp:Label ID="lblBillName" runat="server"></asp:Label><br>
                                                                                    <asp:Label ID="lblBillCompanyName" runat="server"></asp:Label><br>
                                                                                    <asp:Label ID="lblBillAddress1" runat="server"></asp:Label><br>
                                                                                    <asp:Label ID="lblBillAddress2" runat="server"></asp:Label><br>
                                                                                    <asp:Label ID="lblBillCityZip" runat="server"></asp:Label><br>
                                                                                    <asp:Label ID="lblBillStateCountry" runat="server"></asp:Label><br>
                                                                                </td>
                                                                                <td class="bodytext" valign="top"><b>Ship to:</b><br>
                                                                                    <asp:Label ID="lblShipName2" runat="server"></asp:Label><br>
                                                                                    <asp:Label ID="lblShipCompanyName2" runat="server"></asp:Label><br>
                                                                                    <asp:Label ID="lblShipAddress21" runat="server"></asp:Label><br>
                                                                                    <asp:Label ID="lblShipAddress22" runat="server"></asp:Label><br>
                                                                                    <asp:Label ID="lblShipCityZip2" runat="server"></asp:Label><br>
                                                                                    <asp:Label ID="lblShipStateCountry2" runat="server"></asp:Label><br>
                                                                                </td>
                                                                                <td class="bodytext" align="right">Date: <b>
                                                                                    <asp:Label ID="lblPOdate" runat="server"></asp:Label>
                                                                                </b>
                                                                                    <br>
                                                                                    Your Order ID: <b>
                                                                                        <asp:Label ID="lblPONumber" runat="server"></asp:Label>
                                                                                    </b>
                                                                                    <br>
                                                                                    <%= promotioncode %>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <!-- address & order ID & Print Button -->
                                                            <tr>
                                                                <td class="bodytext" valign="top">
                                                                    <table cellspacing="0" cellpadding="0" width="98%" bgcolor="#333333" border="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                    <table cellspacing="1" cellpadding="5" width="100%" border="0">
                                                                                        <tbody>
                                                                                            <tr bgcolor="#ffffff">
                                                                                                <td class="bodytext"><b>#.</b></td>
                                                                                                <td class="bodytext"><b>Date</b></td>
                                                                                                <td class="bodytext"><b>Product</b></td>
                                                                                                <td class="bodytext"><b>Quantity</b></td>
                                                                                                <td class="bodytext"><b>Value</b></td>
                                                                                            </tr>
                                                                                            <%= writeSelectedProducts() %>
                                                                                            <tr bgcolor="#ffffff">
                                                                                                <td class="bodytext" align="right" colspan="4"><b>Total for All Items </b></td>
                                                                                                <td class="bodytext" align="right"><b><%= ConvertToMoneyFormat(m_cartCost)%></b></td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    <p>
                                                                        <!-- /invoice -->
                                                                        <!-- Bottom Navigation -->
                                                                    </p>
                                                                    <%= GetShipInfoIsUSPSOrder()%>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <%--<PPSOL:ADMINFOOTER id="AdminFooter1" runat="server"></PPSOL:ADMINFOOTER><!-- /Bottom Navigation -->--%>
                                                    <p></p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- /Master Table -->
                </td>
            </tr>
        </tbody>
    </table>
    <%}%>
</body>
</html>

