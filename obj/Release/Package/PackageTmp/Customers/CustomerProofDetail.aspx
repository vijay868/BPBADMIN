﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomerProofDetail.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Customers.CustomerProofDetail" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Best Print Buy</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" media="all" href="../css/bestprintbuy_style.css" />
    <link rel="stylesheet" media="all" href="../css/jquery.fancybox.css" />
    <script src="../Scripts/jquery.js"></script>
    <script src="../Scripts/common.js"></script>
    <script src="../Scripts/ddsmoothmenu.js"></script>
    <script src="../Scripts/plugin/fancybox/jquery.fancybox.js"></script>
    <!--[if lt IE 9]>
			<script src="../Scripts/html5.js"></script>
		<![endif]-->
    <!--[if lte IE 7]> <html class="ie7"> <![endif]-->
    <!--[if IE 8]>     <html class="ie8"> <![endif]-->
    <!--[if IE 9]>     <html class="ie9"> <![endif]-->
    <!--[if IE 10]>     <html class="ie10"> <![endif]-->
    <!--[if !IE]><!-->
    <!--<![endif]-->

    <script>
        function NavigateBack() {
            var pokey = document.getElementById("PoDtlKey").value
            //var url = 
            //alert(url);
            document.location.href = "CustomerProofs.aspx?poDtlKey=" + pokey;
            return false;
        }

        function PostReply() {
            try {
                document.getElementById("txtAdminComments").value = document.getElementById("txtAdminComments").value.trim();
            }
            catch (eee) { }
            if (document.getElementById("txtAdminComments").value == "") {
                alert("Please enter Comments");
                return false;
            }
            return true;
        }

        function ShowImgPreview(url) {
            $.fancybox({
                'width': '50%',
                'height': '40%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': url,
                'type': 'iframe'
            });
            return false;
        }
    </script>
</head>
<body id="Body" runat="server">
    <!-- Wrapper starts -->
    <div id="Div1" class="popup-wrapper" runat="server">
        <div class="popup-title">
            Customer Proof Details
        </div>
        <!-- Header Ends -->
        <div class="popup-contnt">
            <!-- Menu starts -->
            <form id="Form1" runat="server">
                <div>
                    <table class="">
                        <tr>
                            <td><label>PO Number</label></td>
                            <td>
                                <asp:Label ID="lblPONumber" runat="server" Font-Size="12px" Font-Bold="true"></asp:Label></td>
                        </tr>
                        <tr>
                            <td><label>Proof Version</label></td>
                            <td><%# GetValue("proof_name") %>
                                <input id="PoDtlKey" value='<%# GetValue("fkey_cust_po_dtl") %>' type="hidden" name="PoDtlKey" runat="server" />
                                <input id="PkeyProof" value='<%# GetValue("pkey_proof") %>' type="hidden" name="PkeyProof" runat="server" />
                                <input id="email" value='<%# GetValue("EMAIL_ID") %>' type="hidden" name="email" runat="server" />
                                <input id="customername" value='<%# GetValue("CustomerName") %>' type="hidden" name="customername" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td><label>Proof Date</label></td>
                            <td><%# GetDateValue("proof_date") %></td>
                        </tr>
                        <tr>
                            <td><label>Customer Comments</label></td>
                            <td>
                                <textarea id="txtCustomerComments" disabled rows="10" cols="40"><%# GetValue("customer_comments") %></textarea></td>
                        </tr>
                        <tr>
                            <td><label>Front Proof</label></td>
                            <td><%# GetImageUrl("front_image_url") %></td>
                        </tr>
                        <tr>
                            <td><label>Back Proof</label></td>
                            <td><%# GetImageUrl("reverse_image_url") %></td>
                        </tr>
                        <tr>
                            <td><label>Image1</label></td>
                            <td><%# GetImageUrl("Image1") %></td>
                        </tr>
                        <tr>
                            <td><label>Image2</label></td>
                            <td><%# GetImageUrl("Image2") %></td>
                        </tr>
                        <tr>
                            <td><label>Image3</label></td>
                            <td><%# GetImageUrl("Image3") %></td>
                        </tr>
                    </table>
                    <div class="clr"></div>
                    <table>
                        <tr>
                            <th style="text-align: left;">COMMENTS</th>
                        </tr>
                        <asp:Repeater ID="rptrComments" runat="server">
                            <ItemTemplate>
                                <tr style="height: 25px">
                                    <td style="text-align: left; padding-left: 20px;">
                                        <b>
                                            <%# DataBinder.Eval(Container, "DataItem.commentedby") %>
																commented on
																<%# DataBinder.Eval(Container, "DataItem.datecreated") %>
                                        </b>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; padding-left: 20px;">
                                        <%# DataBinder.Eval(Container, "DataItem.comments") %>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                        <tr>
                            <td style="text-align: left;">
                                <div style="float: left; width: 70%; padding: 15px 0 0 20px;">
                                    <textarea id="txtAdminComments" runat="server" rows="6" cols="320" style="width:90%"></textarea>
                                </div>
                                <div style="float: left; width: 20%; padding-top: 35px;" class="btn_lst">
                                    <asp:Button runat="server" CssClass="button rounded" ID="btnReply" Text="Reply" OnClick="btnReply_Click" OnClientClick="return PostReply();"></asp:Button>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <table>
                    <tr>
                        <td style="float: right">
                            <asp:Button runat="server" ID="btnSendNotification" CssClass="button rounded" Text="Send Notification Mail to Customer" OnClick="btnSendNotification_Click"></asp:Button>
                            <asp:Button runat="server" ID="btnApprove" CssClass="button rounded" Text="Approve" OnClick="btnApprove_Click"></asp:Button>
                            <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close"></asp:Button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</body>
</html>
