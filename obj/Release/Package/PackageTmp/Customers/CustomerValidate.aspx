﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="CustomerValidate.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Customers.CustomerValidate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <script type="text/javascript">
         $().ready(function () {
             $("#frmCustomerCreditDetails").validate();
         });
         </script>


    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Customer Service</li>
             <li><a href="CustomerCreditList.aspx">Customer Credits</a></li>
            <li>Validate Customer</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Validate Customer</h2>
        <form class="form" runat="server" id="frmCustomerCreditDetails">
                    <table  class="accounts-table" width="100%">
                        <tr>
                            <td width="15%"><label>Validate By</label></td>
                            <td width="20%">
                                <asp:DropDownList name="ddlValidateType" ID="ddlValidateType" CssClass="slect-box" data-placeholder="Email Address" runat="server">
                                    <asp:ListItem Value="1">Email Address</asp:ListItem>
                                    <asp:ListItem Value="2">Purchase Order Number</asp:ListItem>
                                </asp:DropDownList>

                            </td>
                            <td width="50%"></td>
                        </tr>
                        <tr>
                            <td><label>PO Number/Email ID*</label></td>
                            <td>
                                <asp:TextBox ID="txtEmailID" runat="server" CssClass="text-box" required></asp:TextBox></td>                            
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                             <td class="a_rgt">
                               <asp:Button ID="btnClose" runat="server" CssClass="button rounded" Text="Close" OnClick="btnClose_Click" UseSubmitBehavior="false" />
                        <asp:Button ID="lbtnValidate" CssClass="button rounded" runat="server" Text="Validate" OnClick="lbtnValidate_Click" /></td>                            
                            <td></td>

                        </tr>
                    </table>
        </form>
    </div>
</asp:Content>
