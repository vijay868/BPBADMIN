﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustCreditStatus.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Customers.CustCreditStatus" %>

<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->

<!-- Consider adding a manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<head id="Head1" runat="server">
    <title>Best Print Buy</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" media="all" href="../css/bestprintbuy_style.css" />
    <link rel="stylesheet" media="all" href="../css/jquery.fancybox.css" />
    <link rel="stylesheet" href="../css/jquery-ui.css" />
    <script src="../Scripts/jquery.js"></script>
    <script src="../Scripts/jquery-ui.js"></script>
    <script src="../Scripts/common.js"></script>
    <script src="../Scripts/ddsmoothmenu.js"></script>
    <script src="../Scripts/plugin/fancybox/jquery.fancybox.js"></script>
    <script src="../Scripts/jquery.validate.js"></script>
    <!--[if lt IE 9]>
			<script src="../Scripts/html5.js"></script>
		<![endif]-->
    <!--[if lte IE 7]> <html class="ie7"> <![endif]-->
    <!--[if IE 8]>     <html class="ie8"> <![endif]-->
    <!--[if IE 9]>     <html class="ie9"> <![endif]-->
    <!--[if IE 10]>     <html class="ie10"> <![endif]-->
    <!--[if !IE]><!-->

    <!--<![endif]-->
</head>

<body id="Body" runat="server">
    <!-- Wrapper starts -->
    <div id="Div1" class="popup-wrapper" runat="server">
        <div class="popup-title">
            Customer Credit - PO Information
        </div>
        <!-- Header Ends -->
        <div class="popup-contnt">
            <!-- Menu starts -->
            <form id="Form1" runat="server">
                <table>
                    <tr>
                        <td >
                            <label>Customer Name</label>
                        </td>
                        <td width="30px"></td>
                        <td>
                            <asp:Label ID="lblCustName" runat="server" Font-Size="12px" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            <label>Customer Email</label>
                        </td>
                        <td></td>
                        <td>
                            <asp:Label ID="lblCustEmail" runat="server" Font-Size="12px" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            <label>PO Number</label>
                        </td>
                         <td></td>
                        <td>
                            <asp:Label ID="lblPONumber" runat="server" Font-Size="12px" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            <label>Credit Type</label>
                        </td>
                         <td></td>
                        <td>
                            <asp:Label ID="lblCreditType" runat="server" Font-Size="12px" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            <label>Date</label>
                        </td>
                         <td></td>
                        <td>
                            <asp:Label ID="lblDt" runat="server" Font-Size="12px" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            <label>Amount</label>
                        </td>
                         <td></td>
                        <td>
                            <asp:Label ID="lblAmount" runat="server" Font-Size="12px" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Availed Amount</label>
                        </td>
                         <td></td>
                        <td>
                            <asp:Label ID="lblAvailedAmt" runat="server" Font-Size="12px" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                </table>           
            <div class="clr"></div>
                <table class="tablep">
                    <thead>
                        <tr>
                            <th>PO Date</th>
                            <th>PO Number</th>
                            <th>Amount Applied</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rptrCustomerCreditStatus" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <%#Eval("po_date","{0:MM/dd/yyyy}") %>
                                    </td>
                                    <td>
                                        <%#Eval("po_number") %>
                                    </td>
                                    <td>
                                        <%#Eval("amount") %>
                                    </td>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
</body>
</html>
