﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="AdminCustLogin.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Customers.AdminCustLogin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"><script type="text/javascript" src="../Scripts/jquery.zclip.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    
    <script type="text/javascript">

        $(document).ready(function () {
            $("#ContentPlaceHolder1_txtEmailorPO").focus();
        });

        function checkRequiredFields() {
            var logid = $("#ContentPlaceHolder1_txtEmailorPO").val();
            var logOption = $("#ContentPlaceHolder1_ddlLoginID option:selected").val();
            var passowrd = $("#ContentPlaceHolder1_txtPassword").val();
            var isPwdChecked = $("#ContentPlaceHolder1_chksuperuser").is(':checked');
            
            try {
                logid = logid.trim();
            }
            catch (eee) { }

            if (!logid || logid == '') {
                alert("Email Address / P.O.Numer is required, please enter.");
                $("#ContentPlaceHolder1_txtEmailorPO").select();
                $("#ContentPlaceHolder1_txtEmailorPO").focus();
                return false;
            }

            if (isPwdChecked && (!passowrd || passowrd == '')) {
                alert("Password is required, Please enter.");
                $("#ContentPlaceHolder1_txtPassword").select();
                $("#ContentPlaceHolder1_txtPassword").focus();
                return false;
            }
            return true;
            $("#ContentPlaceHolder1_txtPassword").submit();
        }

        function OnChkClick(chkItem) {
            if (chkItem.checked) {
                $("#ContentPlaceHolder1_txtPassword").value = "";
                $("#ContentPlaceHolder1_txtPassword").readOnly = false;
            }
            else {
                $("#ContentPlaceHolder1_txtPassword").value = "";
                $("#ContentPlaceHolder1_txtPassword").readOnly = true;
            }
        }

        function OpenCustomerAccount() {
            var custUrl = $("#ContentPlaceHolder1_txtCustUrl").val();
            var newDate = new Date();
            var TimeStampLink = newDate.getMonth() + "" + newDate.getDate() + "" + newDate.getYear() + "" + newDate.getHours() + "" + newDate.getMinutes() + "" + newDate.getSeconds() + "" + newDate.getMilliseconds()
            var newwindow = window.open(custUrl, "BPBCustomer" + TimeStampLink);
            if (newwindow) newwindow.focus();
        }
    </script>
    <style type="text/css">
        #ContentPlaceHolder1_chksuperuser {
            width: 12px;
            line-height: 21px;
            height: 12px;
        }
    </style>
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Customer Service</li>
            <li>User Information Access</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Admin - Customer login</h2>
        <form class="form" runat="server" id="frmCustomerLogin">
            <table cellpadding="0" cellspacing="0" border="0" width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="22%">Login By*</td>
                        <td width="28%">
                            <asp:DropDownList name="ddlLoginID" ID="ddlLoginID" runat="server">
                                <asp:ListItem Value="3">PO Dtl Number</asp:ListItem>
                                <asp:ListItem Value="2" Selected>Purchase Order Number</asp:ListItem>
                                <asp:ListItem Value="1">Email Address</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td width="50%"></td>
                    </tr>
                    <tr>
                        <td>Email Address/Po Number*</td>
                        <td>
                            <asp:TextBox ID="txtEmailorPO" CssClass="text" runat="server"></asp:TextBox>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Customer Of</td>
                        <td>
                            <asp:DropDownList name="ddlKiosk" ID="ddlKiosk" data-placeholder="Choose a Name" runat="server" CssClass="search required">
                                <asp:ListItem Value=""></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:CheckBox ID="chksuperuser" runat="server" onclick="OnChkClick(this)" /><asp:Label Text="   Access Customer info as super user" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>Super user password*</td>
                        <td>
                            <asp:TextBox ID="txtPassword" CssClass="text" runat="server" TextMode="Password"></asp:TextBox>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div id="pnlcustaccessinfo" runat="server" visible="false" style="text-align: center">
                                <div>
                                    <span style="color: red; FONT-SIZE: 11px; FONT-WEIGHT: bold">Open a new browser, 
				            copy this url into address bar and click go.</span><br />
                                </div>
                                <div>
                                    <asp:TextBox Width="100%" name="txtCustUrl" ID="txtCustUrl" runat="server" style="resize:none;" TextMode="MultiLine" Rows="4"></asp:TextBox>
                                    <asp:Literal ID="ltrlCustomerUrlScript" runat="server"></asp:Literal>
                                </div>
                                <div style="position:relative; text-align: right" class="clipboard" id="btnCopyUrl">
                                    Copy URL to Clipboard
                                </div>
                            </div>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align:right">
                            <asp:Button runat="server" ID="btnClose" Text="Close" CssClass="button rounded" OnClick="btnClose_Click" UseSubmitBehavior="false"></asp:Button>
                            <asp:Button ID="btnLogin" runat="server" Text="Login" UseSubmitBehavior="false" OnClientClick="if(!checkRequiredFields()){return false;}" CssClass="button rounded" />
                        </td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </form>
        <script lang="JavaScript">
            $('#btnCopyUrl').zclip({
                path: '../Scripts/ZeroClipboard.swf',
                copy: function () { return $("#ContentPlaceHolder1_txtCustUrl").val(); }
            });
        </script>
    </div>
</asp:Content>
