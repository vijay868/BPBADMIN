﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderFulfillmentMail.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Customers.OrderFulfillmentMail" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Best Print Buy</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" media="all" href="../css/bestprintbuy_style.css" />
    <link rel="stylesheet" media="all" href="../css/jquery.fancybox.css" />
    <script src="../Scripts/jquery.js"></script>
    <script src="../Scripts/common.js"></script>
    <script src="../Scripts/ddsmoothmenu.js"></script>
    <script src="../Scripts/plugin/fancybox/jquery.fancybox.js"></script>
    <!--[if lt IE 9]>
			<script src="../Scripts/html5.js"></script>
		<![endif]-->
    <!--[if lte IE 7]> <html class="ie7"> <![endif]-->
    <!--[if IE 8]>     <html class="ie8"> <![endif]-->
    <!--[if IE 9]>     <html class="ie9"> <![endif]-->
    <!--[if IE 10]>     <html class="ie10"> <![endif]-->
    <!--[if !IE]><!-->
    <!--<![endif]-->
    <script type="text/javascript">
        function MailsSent() {
            alert("Mails sent");
            window.top.ClosePopup();
        }
    </script>
</head>
<body id="Body" runat="server">
    <!-- Wrapper starts -->
    <div class="popup-wrapper" runat="server">
        <div class="popup-title">
            Order Links
        </div>
        <!-- Header Ends -->
        <div class="popup-contnt">
            <!-- Menu starts -->
            <form id="frmOrderFulfillmentMail" name="frmOrderFulfillmentMail" runat="server">
                <div>
                    <table>
                        <tr>
                            <td><label>Products</label></td>
                            <td>
                                <asp:DropDownList ID="ddlProduct" name="ddlProduct" runat="server" CssClass="search" data-placeholder="Choose a category" AutoPostBack="true" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged"></asp:DropDownList>
                            </td>
                        </tr>
                    </table>
            <div class="clr"></div>
                    <table class="tablep">
                        <thead>
                            <tr>
                                <th>PO #</th>
                                <th>Customer Name</th>
                                <th>Product</th>
                                <th>Amount</th>
                                <th>Select</th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater ID="rptrOrderFulfillmentList" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <%#Eval("ordernumber") %>
                                        </td>
                                        <td>
                                            <%#Eval("customer_name") %>
                                        </td>
                                        <td>
                                            <%#Eval("cardtype") %>
                                        </td>
                                        <td>
                                            <%#Eval("price_per_unit") %>
                                        </td>
                                        <td class="center">
                                            <asp:CheckBox runat="server" ID="chkSelect" Checked="true" />
                                            <asp:HiddenField ID="POKey" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.pkey_cust_po_master") %>' />
                                            <asp:HiddenField ID="PODtlKey" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.pkey_cust_po_dtl") %>' />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </div>
                <table>
                        <tr>
                            <td style="float:left;">
                                <asp:Button runat="server" ID="btnSendMail" CssClass="button rounded" Text="Send Mail" OnClick="btnSendMail_Click" OnClientClick="return UploadConfirm();"></asp:Button>
                            </td>
                            <td style="float:right;">
                                <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClientClick="parent.$.fancybox.close(); return false;"></asp:Button>
                            </td>
                        </tr>
                    </table>
            </form>
        </div>
    </div>
</body>
</html>
