﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderLinks.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Customers.OrderLinks" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Best Print Buy</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" media="all" href="../css/bestprintbuy_style.css" />
    <link rel="stylesheet" media="all" href="../css/jquery.fancybox.css" />
    <script src="../Scripts/jquery.js"></script>
    <script src="../Scripts/common.js"></script>
    <script src="../Scripts/ddsmoothmenu.js"></script>
    <script src="../Scripts/plugin/fancybox/jquery.fancybox.js"></script>
    <!--[if lt IE 9]>
			<script src="../Scripts/html5.js"></script>
		<![endif]-->
    <!--[if lte IE 7]> <html class="ie7"> <![endif]-->
    <!--[if IE 8]>     <html class="ie8"> <![endif]-->
    <!--[if IE 9]>     <html class="ie9"> <![endif]-->
    <!--[if IE 10]>     <html class="ie10"> <![endif]-->
    <!--[if !IE]><!-->
    <!--<![endif]-->
</head>

<body id="Body" runat="server">
    <!-- Wrapper starts -->
    <div id="Div1" class="popup-wrapper" runat="server">
        <div class="popup-title">
            Order Links
        </div>
        <!-- Header Ends -->
        <div class="popup-contnt">
            <!-- Menu starts -->
            <form id="Form1" runat="server">
                <table>
                    <tr>
                        <td width="30%"><label>PO Number</label></td>
                        <td>
                            <asp:Label ID="lblPONumber" runat="server" Font-Size="12px" Font-Bold="true"></asp:Label></td>
                    </tr>
                    <tr>
                        <td><label>SO Number</label></td>
                        <td>
                            <asp:Label ID="lblSONumber" runat="server" Font-Size="12px" Font-Bold="true"></asp:Label>
                            <input id="hdnErrMessage" type="hidden" name="hdnErrMessage" runat="server" />
                            <input id="txtPkeyCustPoDtl" type="hidden" name="txtPkeyCustPoDtl" runat="server" />
                            <input id="txtPkeyBrPoDtl" type="hidden" name="txtPkeyBrPoDtl" runat="server" />
                            <input id="hdnOrderIndex" type="hidden" name="hdnOrderIndex" runat="server" />
                        </td>
                    </tr>
                    <tr id="trPDFLink" runat="server">
                        <td><label>PDF Asset</label></td>
                        <td>
                            <asp:HyperLink CssClass="a1b" NavigateUrl="#" ID="anchPdfUrl" runat="server" Target="_blank">PDF</asp:HyperLink>
                        </td>
                    </tr>
                    <tr id="trFrontImg" runat="server">
                        <td><label>Upload Asset 1</label></td>
                        <td>
                            <asp:HyperLink CssClass="a1b" NavigateUrl="#" ID="anchFrontImageUrl" runat="server" Target="_blank">Upload Asset</asp:HyperLink>
                        </td>
                    </tr>
                    <tr id="trBackImage" runat="server">
                        <td><label>Upload Asset 2</label></td>
                        <td>
                            <asp:HyperLink CssClass="a1b" NavigateUrl="#" ID="anchRevImageUrl" runat="server" Target="_blank">Upload Asset</asp:HyperLink>
                        </td>
                    </tr>
                    <tr id="trCsvUrl" runat="server">
                        <td><label>CSV Asset</label></td>
                        <td>
                            <asp:HyperLink CssClass="a1b" NavigateUrl="#" ID="anchCsvUrl" runat="server" Target="_blank">CSV</asp:HyperLink>
                        </td>
                    </tr>
                </table>
                <table>                    
                    <tr style="float: right;">
                        <td>
                            <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClientClick="parent.$.fancybox.close(); return false;"></asp:Button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</body>
</html>
