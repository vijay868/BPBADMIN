﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="CustomerValidateAndAssign.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Customers.CustomerValidateAndAssign" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        $().ready(function () {
            $("#frmCustomerLogin").validate();
        });
    </script>
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Customer Service</li>
            <li><a href="MCACustomerList.aspx">MCA Customers List</a></li>
            <li>Validate User</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Validate User</h2>
        <form class="form" runat="server" id="frmCustomerLogin">
            <div class="form_sep_left" style="float: left;">
                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="accounts-table">
                    <tbody>
                        <tr>
                            <td width="10%">Login By:*</td>
                            <td width="20%">
                                <asp:DropDownList name="ddlValidateType" ID="ddlValidateType" runat="server" required>
                                    <asp:ListItem Value="Email Address">Email Address</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>Email Address*</td>
                            <td>
                                <asp:TextBox ID="txtEmailID" required CssClass="text email" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:Button runat="server" ID="btnClose" Text="Close" CssClass="button rounded" OnClick="btnClose_Click" UseSubmitBehavior="false"></asp:Button>
                                <asp:Button ID="lbtnValidate" runat="server" Text="Validate" OnClick="lbtnValidate_Click" CssClass="button rounded" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>
</asp:Content>
