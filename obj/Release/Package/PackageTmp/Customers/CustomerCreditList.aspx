﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="CustomerCreditList.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Customers.CustomerCreditList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">

        function SearchText() {
            $(".autosuggest").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "OrderFulfillment.aspx/GetAutoCompleteData",
                        data: "{'input':'" + $("#ContentPlaceHolder1_txtPattern").val() + "', field: '" + $("#ContentPlaceHolder1_ddlFilterFor option:selected").index() + "'}",
                        dataType: "json",
                        success: function (data) {
                            response(data.d);
                        },
                        error: function (result) {
                            alert("Error");
                        }
                    });
                },
                select: function (e, ui) {
                    $("#ContentPlaceHolder1_txtPattern").val(ui.item.value);
                    __doPostBack('ctl00$ContentPlaceHolder1$btnGo', '');
                    return true;
                }
            });
        }

        function ChangeIndex() {
            $("#ContentPlaceHolder1_txtPattern").val("");
        }

        $(document).ready(function () {
            $("#ContentPlaceHolder1_btnApplyCredit").click(function () {
             
                if ($("#ContentPlaceHolder1_txtPattern").val() == "") {
                    $("#ContentPlaceHolder1_txtPattern").removeAttr('required');
                }
            });

        });
        $().ready(function () {
            $("#CusromerCreditList").validate();
            SearchText();
        });
            </script>
            <script type="text/javascript">
        function viewCreditSummary() {
            if ('<%=HasAccess("CustCrediSummary", "view")%>' == 'False') {
                alert('User does not have access.');
                return false;
            }
            else {
                var url = "CustCrediSummary.aspx";
                $.fancybox({
                    'width': '80%',
                    'height': '100%',
                    'autoScale': true,
                    'transitionIn': 'fade',
                    'transitionOut': 'fade',
                    'href': url,
                    'type': 'iframe'
                });
                return false;
            }
            return true;
        }
        function viewCredit() {
            if ('<%=HasAccess("CustomerCreditPreview", "view")%>' == 'False') {
             alert('User does not have access.');
             return false;
         }
         return true;
     }

     function addCredit() {
         if ('<%=HasAccess("CustomerCreditDetails", "add")%>' == 'False') {
                alert('User does not have access.');
                return false;
            }
            return true;
        }

        function OpenCreditStetusDialog(pkey_credit) {
            if ('<%=HasAccess("CustomerCreditPreview", "view")%>' == 'False') {
                alert('User does not have access.');
                return false;
            }
            else {
                var url = "CustCreditStatus.aspx?pkey_credit=" + pkey_credit;
                $.fancybox({
                    'width': '80%',
                    'height': '100%',
                    'autoScale': true,
                    'transitionIn': 'fade',
                    'transitionOut': 'fade',
                    'href': url,
                    'type': 'iframe'
                });
            }
        }
    </script>
    <div class="contant_hldr">
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Customer Service</li>
            <li>Customer Credits</li>
        </ul>
        <h2>Customer Credits</h2>
        <form class="form" runat="server" id="CusromerCreditList" name="CusromerCreditList">
            <table class="accounts-table">
                <tbody>
                    <tr>
                        <td width="7%">Filter List for :</td>
                        <td width="15%">
                            <asp:DropDownList name="ddlFilterFor" ID="ddlFilterFor" runat="server" onchange="ChangeIndex();">
                                <asp:ListItem Value="pps_customers.email_id">Email Address</asp:ListItem>
                                <asp:ListItem Value="pps_customers.first_name + ' ' + pps_customers.last_name">Customer First & Last Name</asp:ListItem>
                                <asp:ListItem Value="pps_cust_service_credits.po_number">PO Number</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td width="1%"></td>
                        <td width="15%">
                            <asp:DropDownList name="ddlOperator" ID="ddlOperator" runat="server">
                                <asp:ListItem Value="=">Equals</asp:ListItem>
                                <asp:ListItem Value="<>">Not Equals</asp:ListItem>
                                <asp:ListItem Value="LIKE%">Starts With</asp:ListItem>
                                <asp:ListItem Value="%LIKE">Ends with</asp:ListItem>
                                <asp:ListItem Value="%LIKE%">Contains</asp:ListItem>
                            </asp:DropDownList></td>
                        <td width="1%"></td>
                        <td width="15%">
                            <asp:TextBox name="txtPattern" CssClass="text autosuggest" ID="txtPattern" runat="server" required ></asp:TextBox>
                        </td>
                        <td width="10%" style="padding-left: 5px">
                            <asp:Button runat="server" ID="btnGo" CssClass="button rounded" Text="Apply"  OnClick="btnGo_Click"></asp:Button>
                        </td>
                        <td width="5%">
                            <asp:Button runat="server" ID="btnReset" CssClass="button rounded" Text="Reset" UseSubmitBehavior="false" OnClick="btnReset_Click" Visible="false"></asp:Button>
                        </td>
                        <td width="35%"></td>
                    </tr>
                </tbody>
            </table>           
            <div class="clr"></div>
            <asp:GridView ID="CustomercreditGrid" runat="server" AutoGenerateColumns="false" EnableViewState="true"
                ForeColor="Black" Width="100%" PageSize="20" AllowPaging="true" AllowSorting="true"
                class="table" name="CustomercreditGrid" OnPageIndexChanging="CustomercreditGrid_PageIndexChanging" OnSorting="CustomercreditGrid_Sorting">
                <PagerStyle HorizontalAlign="Right" CssClass="dtPage"></PagerStyle>
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />
                <Columns>
                    <asp:TemplateField HeaderText="#SL">
                        <HeaderStyle Width="3%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <span><%# GetRecordCountForPage()%></span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Email" SortExpression="email_id" ItemStyle-Width="10%">
                        <HeaderStyle Width="10%" ForeColor="White" />
                        <ItemTemplate>
                            <a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("CustomerCreditDetails", "view")%>','CustomerCreditDetails.aspx?Pkey=<%#Eval("pkey_service_credit") %>&fkeycustomer=<%#Eval("fkey_customer") %>&ponumber=<%#Eval("po_number") %>&ispo=false')" title="Edit"><%#Eval("email_id") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Name" SortExpression="customer_name" ItemStyle-Width="10%">
                        <HeaderStyle Width="10%" ForeColor="White" />
                        <ItemTemplate>
                            <asp:Label ID="lblName" runat="server" Text='<%# Eval("customer_name")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Credit Type" ItemStyle-Width="5%" SortExpression="service_credit_abbr">
                        <HeaderStyle Width="5%" ForeColor="White" />
                        <ItemTemplate>
                            <asp:Label ID="lblCredit" runat="server" Text='<%# Eval("service_credit_abbr")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Date" HeaderStyle-ForeColor="White" SortExpression="date_created" ItemStyle-Width="5%" DataField="date_created" DataFormatString="{0:MM/dd/yyyy}"></asp:BoundField>
                    <asp:TemplateField HeaderText="Amount" SortExpression="credit_amount" ItemStyle-Width="5%">
                        <HeaderStyle Width="5%" ForeColor="White" />
                        <ItemTemplate>
                            <asp:Label ID="lblAmount" runat="server" Text=' <%# Eval("credit_amount")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Active" SortExpression="is_active" ItemStyle-Width="3%">
                        <HeaderStyle Width="3%" ForeColor="White" />
                        <ItemTemplate>
                            <asp:Label ID="lblActive" runat="server" Text='<%#Eval("is_active").ToString() == "True" ? "Y" : "N"%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Availed Amount" SortExpression="availed_amount" ItemStyle-Width="5%">
                        <HeaderStyle Width="5%" ForeColor="White" />
                        <ItemTemplate>
                            <asp:Label ID="lblAvailedAmount" runat="server" Text='<%#GetAnchor(Eval("availed_amount"),Eval("pkey_service_credit")) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <div class="ttl"><b>Total Number of Customer Credits : </b><%=creditcount%></div>
            <div class="btn_lst">
                <div class="lft">
                    <asp:Button runat="server" ID="btnSummary" Text="Summary" CssClass="button rounded" UseSubmitBehavior="false" OnClientClick="return viewCreditSummary();"></asp:Button>
                </div>
                <div class="rgt">
                    <asp:Button runat="server" ID="btnClose" CssClass="button rounded" UseSubmitBehavior="false" Text="Close" OnClick="btnClose_Click"></asp:Button>
                    <asp:Button runat="server" ID="btnApplyCredit" CssClass="button rounded" Text="Apply Credit" OnClientClick="return addCredit();"  OnClick="btnApplyCredit_Click"></asp:Button>
                </div>
            </div>
        </form>
    </div>
</asp:Content>
