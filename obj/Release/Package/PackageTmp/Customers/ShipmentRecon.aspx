﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShipmentRecon.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Customers.ShipmentRecon" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Best Print Buy</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" media="all" href="../css/bestprintbuy_style.css" />
    <link rel="stylesheet" media="all" href="../css/jquery.fancybox.css" />
    <script src="../Scripts/jquery.js"></script>
    <script src="../Scripts/common.js"></script>
    <script src="../Scripts/ddsmoothmenu.js"></script>
    <script src="../Scripts/plugin/fancybox/jquery.fancybox.js"></script>
    <!--[if lt IE 9]>
			<script src="../Scripts/html5.js"></script>
		<![endif]-->
    <!--[if lte IE 7]> <html class="ie7"> <![endif]-->
    <!--[if IE 8]>     <html class="ie8"> <![endif]-->
    <!--[if IE 9]>     <html class="ie9"> <![endif]-->
    <!--[if IE 10]>     <html class="ie10"> <![endif]-->
    <!--[if !IE]><!-->
    <!--<![endif]-->

    <script>
        function UploadConfirm() {
            if ($("#uploadExcelFile").val() == "") {
                alert("Please select an Excel file and Upload.");
                $("#uploadExcelFile").focus();
                return false;
            }
            return true;
        }
    </script>
</head>

<body id="Body" runat="server">
    <!-- Wrapper starts -->
    <div class="popup-wrapper" runat="server">
        <div class="popup-title">
            Shipping Recon
        </div>
        <!-- Header Ends -->
        <div class="popup-contnt">
            <!-- Menu starts -->
            <form id="Form1" runat="server">
                    <div id="trUpload" runat="server">
                        <table>
                            <tr>
                                <td><label>Select Excel File</label></td>
                                <td>
                                    <asp:FileUpload ID="uploadExcelFile" runat="server" /></td>
                            </tr>
                        </table>
                    </div>
            <div class="clr"></div>
                    <div id="tdShipmentRecon" runat="server" visible="false">
                        <table class="tablep">
                            <thead>
                                <tr>
                                    <th>PO#</th>
                                    <th>Customer Name</th>
                                    <th>Shipper</th>
                                    <th>Tracking Number</th>
                                    <th id="thShipDate" runat="server">Ship Date</th>
                                    <th>Prev Tracking #</th>
                                    <th>Order Status</th>
                                    <th>Select</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rptrShipmentList" runat="server" OnItemDataBound="rptrShipmentList_ItemDataBound">
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <%# Eval("podtlnumber")%>
                                                <asp:HiddenField ID="hdnpkeycustpodtl" Value='<%# Eval("pkey_cust_po_dtl") %>' runat="server" />
                                            </td>
                                            <td>
                                                <%# Eval("customer_name")%>
                                            </td>
                                            <td>
                                                <asp:DropDownList runat="server" ID="ddlShipper">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="txtTrackingNo" Text='<%# Eval("podtltrackingnumber") %>' />
                                            </td>
                                            <td id="tdShipDate" runat="server">
                                                <asp:TextBox runat="server" ID="txtShipDate" Text='<%# Eval("po_ship_date") %>' />
                                            </td>
                                            <td>
                                                <%#Eval("tracking_number")%>
                                            </td>
                                            <td>
                                                <%# GetPOStatus(Eval("status_type")) %>
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chkSelect" runat="server" />
                                                <asp:HiddenField ID="hdnBrPoDtl" Value='<%# Eval("pkey_br_po_dtl") %>' runat="server" />
                                                <asp:HiddenField ID="hdnPoMaster" Value='<%# Eval("pkey_cust_po_master") %>' runat="server" />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>                                
                            </tbody>
                        </table>
                    </div>
                    <table>
                        <tr>
                            <td style="float:right;">
                                <asp:Button runat="server" ID="btnSave" CssClass="button rounded" Text="Save Shipment Recon" OnClick="btnSave_Click"></asp:Button>
                                <asp:Button runat="server" ID="btnUplad" CssClass="button rounded" Text="Upload File" OnClick="btnUplad_Click" OnClientClick="return UploadConfirm();"></asp:Button>
                                <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClientClick="parent.$.fancybox.close(); return false;"></asp:Button>
                            </td>
                        </tr>
                    </table>
            </form>
        </div>
    </div>
</body>
</html>
