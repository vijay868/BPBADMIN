﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="TemplateSearch.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Customers.TemplateSearch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            SearchText();
            $("#srchDetails").validate();
        });

        function SearchText() {
            $(".autosuggest").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "TemplateSearch.aspx/GetAutoCompleteData",
                        data: "{'templatename':'" + $("#ContentPlaceHolder1_txtTemplateName").val() + "'}",
                        dataType: "json",
                        success: function (data) {
                            response(data.d);
                        },
                        error: function (result) {
                            alert("Error");
                        }
                    });
                },
                select: function (e, ui) {
                    $("#ContentPlaceHolder1_txtTemplateName").val(ui.item.value);
                    __doPostBack('ctl00$ContentPlaceHolder1$btnGo', '');
                    return true;
                }
            });
        }
    </script>
    <div class="clr"></div>
    <!-- Container Starts -->
    <div class="contant_hldr">
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Customer Service</li>
            <li>Template Search</li>
        </ul>
         <h2>Template Search</h2>
        <form class="form validate" runat="server" name="srchDetails" id="srchDetails">
            <table cellpadding="0" cellspacing="0" border="0" width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="20%">
                            <asp:TextBox name="txtTemplateName" ID="txtTemplateName" CssClass="logincontrols autosuggest" runat="server" required> </asp:TextBox></td>
                        <td width="2%"></td>
                        <td width="5%">
                            <asp:Button runat="server" ID="btnGo" CssClass="button" Text="Search" OnClick="btnGo_Click"></asp:Button>
                        </td>
                        <td width="88%"></td>
                    </tr>
                </tbody>
            </table>           
            <div class="clr"></div>

            <asp:GridView ID="TmpltSrchGrid" runat="server" AutoGenerateColumns="false" EnableViewState="true"
                ForeColor="Black" Width="100%" PageSize="20" AllowPaging="true" AllowSorting="true"
                class="table" name="TmpltSrchGrid" OnPageIndexChanging="TmpltSrchGrid_PageIndexChanging" OnSorting="TmpltSrchGrid_Sorting">
                <PagerStyle HorizontalAlign="Right" CssClass="dtPage"></PagerStyle>
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />
                <Columns>
                    <asp:TemplateField HeaderText="#SL">
                        <HeaderStyle Width="10%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <span><%# GetRecordCountForPage()%></span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Name">
                        <HeaderStyle Width="30%" ForeColor="White" />
                        <ItemTemplate>
                            <asp:Label ID="lblName" runat="server" Text='<%#Eval("template_name") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Display Name">
                        <HeaderStyle Width="30%" ForeColor="White" />
                        <ItemTemplate>
                            <asp:Label ID="lblDpname" runat="server" Text='<%#Eval("template_display_name") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Thumbnail">
                        <ItemTemplate>
                            <img id="imgThumbnail" runat="server" src='<%# mCustomerPortalURL+Eval("thumbnail_url") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <div class="ttl"><b>Total Number of Templates: </b><%=recordCount%></div>
            <div class="btn_lst">
                <div class="rgt">
                    <asp:Button runat="server" CssClass="button rounded" ID="btnClose" Text="Close" OnClick="btnClose_Click" UseSubmitBehavior="false"></asp:Button>
                </div>
            </div>
        </form>
    </div>
</asp:Content>
