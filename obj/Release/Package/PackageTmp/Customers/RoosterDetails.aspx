﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RoosterDetails.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Customers.RoosterDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Best Print Buy</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" media="all" href="../css/bestprintbuy_style.css" />
    <link rel="stylesheet" media="all" href="../css/jquery.fancybox.css" />
    <script src="../Scripts/jquery.js"></script>
    <script src="../Scripts/common.js"></script>
    <script src="../Scripts/ddsmoothmenu.js"></script>
    <script src="../Scripts/plugin/fancybox/jquery.fancybox.js"></script>
    <!--[if lt IE 9]>
			<script src="../Scripts/html5.js"></script>
		<![endif]-->
    <!--[if lte IE 7]> <html class="ie7"> <![endif]-->
    <!--[if IE 8]>     <html class="ie8"> <![endif]-->
    <!--[if IE 9]>     <html class="ie9"> <![endif]-->
    <!--[if IE 10]>     <html class="ie10"> <![endif]-->
    <!--[if !IE]><!-->
    <!--<![endif]-->
</head>
<body id="Body" runat="server">
    <!-- Wrapper starts -->
    <div id="Div1" class="popup-wrapper" runat="server">
        <div class="popup-title">
            Rooster Details
        </div>
        <!-- Header Ends -->
        <div class="popup-contnt">
            <!-- Menu starts -->
            <form id="Form1" runat="server">
                <table>
                    <tr>
                        <td width="40%"><label>ID</label></td>
                        <td>
                            <asp:Label ID="lblMCID" runat="server" Font-Size="12px" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td><label>Office Name</label></td>
                        <td>
                            <asp:Label ID="lblOffice" runat="server" Font-Size="12px" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr runat="server">
                        <td><label>Region</label></td>
                        <td>
                            <asp:Label ID="lblRegion" runat="server" Font-Size="12px" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td><label>Address</label></td>
                        <td>
                            <asp:Label ID="lblAddress" runat="server" Font-Size="12px" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td><label>City</label></td>
                        <td>
                            <asp:Label ID="lblCity" runat="server" Font-Size="12px" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td><label>State</label></td>
                        <td>
                            <asp:Label ID="lblState" runat="server" Font-Size="12px" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td><label>Zip</label></td>
                        <td>
                            <asp:Label ID="lblZip" runat="server" Font-Size="12px" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td><label>Phone</label></td>
                        <td>
                            <asp:Label ID="lblPhone" runat="server" Font-Size="12px" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td><label>Fax</label></td>
                        <td>
                            <asp:Label ID="lblFax" runat="server" Font-Size="12px" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td><label>Operating Principal</label></td>
                        <td>
                            <asp:Label ID="lblOP" runat="server" Font-Size="12px" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td><label>Tech Lead</label></td>
                        <td>
                            <asp:Label ID="lblTL" runat="server" Font-Size="12px" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td><label>MCA</label></td>
                        <td>
                            <asp:Label ID="lblMCA" runat="server" Font-Size="12px" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td><label>Tech Coordinator</label></td>
                        <td>
                            <asp:Label ID="lblTC" runat="server" Font-Size="12px" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="2">
                            <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClientClick="parent.$.fancybox.close(); return false;"></asp:Button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</body>
</html>
