﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShopFloorInvoice.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Customers.ShopFloorInvoice" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script>
        function printWindow() {
            bV = parseInt(navigator.appVersion)
            if (bV >= 4) window.print()
        }
    </script>
</head>
<body ms_positioning="GridLayout" bgcolor="#e5e5e5" leftmargin="0" topmargin="0" marginheight="0"
    marginwidth="0">
   <%-- <customcontrol:scriptcontrol id="Scriptcontrol1" runat="server"></customcontrol:scriptcontrol>
    <customcontrol:messagescontrol id="Messagescontrol1" runat="server"></customcontrol:messagescontrol>--%>
   <%
        
        bool isInvoice = true;
        //if Request("inv") <> "" Then
        //    isInvoice = Convert.ToBoolean(Request("inv"))
        //End If
        isInvoice = Convert.ToBoolean(Request.QueryString["inv"]);
        if (!true)
        {
    %>
    <table cellspacing="0" cellpadding="1" width="750" bgcolor="#00526e" border="0">
        <tbody>
            <tr>
                <td>
                    <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#ffffff" border="0">
                        <tbody>
                            <tr>
                                <td>
                                    <!-- Header -->
                                    <table cellspacing="0" cellpadding="10" width="98%" border="0">
                                        <tbody>
                                            <tr>
                                                <td align="right" width="485"><a href="../Account/DashBoard.aspx">
                                                    <img height="30" alt="BestPrint Buy.com" src="../Images/bpb_logo_Black.png" width="210"
                                                        border="0"></a><br>
                                                    <table width="210" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td align="left" class="head">1506 Providence Highway,#29<br>
                                                                Norwood, MA 02062.<br>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td class="bodytext" valign="top" align="right"><a href="javascript:printWindow()">
                                                    <img height="29" alt="Print this Label" src="../Images/print1.gif" border="0"></a><br>
                                                    <br>
                                                    <input class="buttons grey" type="button" value="Close" onclick="parent.$.fancybox.close(); return false;"
                                                        name="Home"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- /Header -->
                                    <!-- Invoice -->
                                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <!-- address & order ID & Print Button -->
                                                                    <table cellspacing="0" cellpadding="5" width="100%" border="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="bodytext" align="center" colspan="3">
                                                                                    <hr size="1">
                                                                                    S H I P P I N G &nbsp; &nbsp; L A B E L
																						<hr size="1">
                                                                                </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="center" width="45%" class="bodytext" valign="top">&nbsp;</td>
                                                                                    <td width="10%" nowrap class="bodytext" valign="top">
                                                                                       <asp:Label ID="lblShipName1" runat="server" /><br />
                                                                                        <asp:Label ID="lblCompany1" runat="server" /><br />
                                                                                        <asp:Label ID="lblAddress11" runat="server" /><br />
                                                                                        <asp:Label ID="lblAddress21" runat="server" /><br />
                                                                                        <asp:Label ID="lblCity1" runat="server" />&nbsp;<asp:Label ID="lblZip1" runat="server" />
                                                                                        <asp:Label ID="lblState1" runat="server" />&nbsp;<asp:Label ID="lblCountry1" runat="server" />
                                                                                    </td>
                                                                                    <td align="center" width="45%" class="bodytext" valign="top">&nbsp;</td>
                                                                                </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    <!-- address & order ID & Print Button -->
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <%--<ppsol:adminfooter id="Adminfooter2" runat="server"></ppsol:adminfooter>--%>
                                                    
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- /Master Table -->
                </td>
            </tr>
        </tbody>
    </table>
     <%}
            else
            {%>
    <table cellspacing="0" cellpadding="1" width="750" bgcolor="#00526e" border="0">
        <tbody>
            <tr>
                <td>
                    <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#ffffff" border="0">
                        <tbody>
                            <tr>
                                <td>
                                    <!-- Header -->
                                    <table cellspacing="0" cellpadding="10" width="98%" border="0">
                                        <tbody>
                                            <tr>
                                                <td><a href="../Account/DashBoard.aspx">
                                                    <img height="30" alt="BestPrint Buy.com" src="../Images/bpb_logo_Black.png" width="210"
                                                        border="0"></a></td>
                                                <td class="bodytext" valign="top" align="right"><a href="javascript:printWindow()">
                                                    <img height="29" alt="Print this Invoice" src="../Images/print.gif" width="113"
                                                        border="0"></a><br>
                                                    <br>
                                                    <input class="buttons grey" type="button" value="Close" onclick="parent.$.fancybox.close(); return false;"
                                                        name="Home"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- /Header -->
                                    <!-- Invoice -->
                                    <table cellspacing="10" cellpadding="0" width="100%" border="0">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <!-- address & order ID & Print Button -->
                                                                    <table cellspacing="0" cellpadding="5" width="100%" border="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="bodytext" align="center" colspan="3">
                                                                                    <hr size="1"/>
                                                                                    S H O P &nbsp; &nbsp; F L O O R &nbsp; &nbsp; I N V O I C E
																						<hr size="1"/>
                                                                                </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="bodytext" valign="top"><b>Ship to:</b><br />
                                                                                        <%-- <%# GetShipInfo("ship_first_name") %>
																						&nbsp;<%# GetShipInfo("ship_last_name") %><br>
                                                                                        <%# GetShipInfo("ship_company_name") %>
                                                                                        <br>
                                                                                        <%# GetShipInfo("shipment_address1") %>
                                                                                        <br>
                                                                                        <%# GetShipInfo("shipment_address2") %>
                                                                                        <br>
                                                                                        <%# GetShipInfo("city") %>
																						&nbsp;<%# GetShipInfo("zipcode") %><br>
                                                                                        <%# GetStateName("fkey_state",true) %>
																						&nbsp;<%# GetCountryName("fkey_country",true) %>--%>
                                                                                        <asp:Label ID="lblShipName" runat="server" /><br />
                                                                                        <asp:Label ID="lblCompany" runat="server" /><br />
                                                                                        <asp:Label ID="lblAddress1" runat="server" /><br />
                                                                                        <asp:Label ID="lblAddress2" runat="server" /><br />
                                                                                        <asp:Label ID="lblCity" runat="server" />&nbsp;<asp:Label ID="lblZip" runat="server" />
                                                                                        <asp:Label ID="lblState" runat="server" />&nbsp;<asp:Label ID="lblCountry" runat="server" />
                                                                                    </td>
                                                                                    <td align="right">
                                                                                        <%-- <%# GetData("po_date") %></span></B>
																						<br>
                                                                                        <span class="bodytext">Shop Floor Invoice ID:</span><br>
                                                                                        <span id="barCodeId" style="FONT-SIZE: 8pt; COLOR: #000000; FONT-FAMILY: IDAutomationHC39M">*<%# GetData("po_number") + "-" + Convert.ToString(brpoKey) %>*</span>
                                                                                        <br>
                                                                                        <span class="bodytext">
                                                                                            <%= promotioncode %></span>--%>
                                                                                        <p>
                                                                                            Date: <b>
                                                                                                <asp:Label ID="lblpodate" runat="server"></asp:Label></b>
                                                                                        </p>
                                                                                        <p>
                                                                                            Shop Floor Invoice ID:<br />
                                                                                            <asp:Label ID="lblShipNumber" runat="server"></asp:Label>
                                                                                        </p>
                                                                                        <p>
                                                                                            <%= promotioncode %>
                                                                                        </p>
                                                                                    </td>
                                                                                </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    </td>
                                                                </tr>
                                                                    <!-- address & order ID & Print Button -->
                                                                    <tr>
                                                                        <td class="bodytext" valign="top">
                                                                            <table cellspacing="0" cellpadding="0" width="100%" bgcolor="#333333" border="0">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table cellspacing="1" cellpadding="5" width="100%" border="0">
                                                                                                <tbody>
                                                                                                    <tr bgcolor="#ffffff">
                                                                                                        <td class="bodytext"><b>#.</b></td>
                                                                                                        <td class="bodytext"><b>Date</b></td>
                                                                                                        <td class="bodytext"><b>Product</b></td>
                                                                                                        <td class="bodytext"><b>Quantity</b></td>
                                                                                                        <td class="bodytext"><b>Value</b></td>
                                                                                                    </tr>
                                                                                                    <%= writeSelectedProducts() %>
                                                                                                    <tr bgcolor="#ffffff">
                                                                                                        <td class="bodytext" align="right" colspan="4"><b>Total for All Items :</b></td>
                                                                                                        <td class="bodytext" align="right"><b>$<%= m_cartCost%></b></td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>                                                                          
                                                                            <%= GetShipInfoIsUSPSOrder()%>
                                                                        </td>
                                                                    </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- /Master Table -->
                </td>
            </tr>
        </tbody>
    </table>
    <%}%>
</body>
</html>
