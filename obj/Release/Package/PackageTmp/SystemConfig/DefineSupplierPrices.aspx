﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DefineSupplierPrices.aspx.cs" Inherits="Bestprintbuy.Admin.Web.SystemConfig.DefineSupplierPrices" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Best Print Buy</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" media="all" href="../css/bestprintbuy_style.css" />
    <link rel="stylesheet" media="all" href="../css/jquery.fancybox.css" />
    <link rel="stylesheet" href="../css/jquery-ui.css" />
    <script src="../Scripts/jquery.js"></script>
    <script src="../Scripts/jquery-ui.js"></script>
    <script src="../Scripts/common.js"></script>
    <script src="../Scripts/ddsmoothmenu.js"></script>
    <script src="../Scripts/plugin/fancybox/jquery.fancybox.js"></script>
    <script src="../Scripts/jquery.validate.js"></script>
    <!--[if lt IE 9]>
			<script src="../Scripts/html5.js"></script>
		<![endif]-->
    <!--[if lte IE 7]> <html class="ie7"> <![endif]-->
    <!--[if IE 8]>     <html class="ie8"> <![endif]-->
    <!--[if IE 9]>     <html class="ie9"> <![endif]-->
    <!--[if IE 10]>     <html class="ie10"> <![endif]-->
    <!--[if !IE]><!-->
    <!--<![endif]-->
    <script type="text/javascript">

        function SetSupplierData() {
            var count = parseInt($("#hdnSupplierCount").val(), 10);
            var parentID = $("#hdnParentID").val();
            //alert(count);
            //alert(parentID);
            window.top.SetSupplierData(count, parentID);
            return false;
        }

    </script>
</head>

<body id="Body" runat="server">
    <!-- Wrapper starts -->
    <div id="Div1" class="popup-wrapper" runat="server">
        <div class="popup-title">
            Define Supplier Price for Product
        </div>
        <!-- Header Ends -->
        <div class="popup-contnt">
            <!-- Menu starts -->
            <form id="Form1" runat="server">
                <table class="tablep">
                    <thead>
                        <tr>
                            <th width="2%"></th>
                            <th width="15%">Supplier ID</th>
                            <th width="25%">Supplier Name</th>
                            <th width="10%">Select</th>
                            <th width="10%">Competitive Index</th>
                            <th width="20%">Max. Capacity / Day</th>
                            <th width="15%">Price($)</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rptrSupplierTypes" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td></td>
                                    <td>
                                        <%#Eval("supplier_id") %>
                                    </td>
                                    <td>
                                        <%#Eval("supplier_name") %>
                                    </td>
                                    <td>
                                        <asp:HiddenField ID="hdnSupplier" runat="server" Value='<%#Eval("fkey_supplier") %>' />
                                        <asp:HiddenField ID="hdnSelect" runat="server" Value='<%#Eval("pkey_supplier_price") %>' />
                                        <asp:CheckBox ID="chkSelect" runat="server" Checked='<%#Eval("pkey_supplier_price").ToString() != "" ? true : false %>' Enabled='<%#Eval("pkey_supplier_price").ToString() != "" ? false : true %>' />
                                    </td>
                                    <td>
                                        <asp:HiddenField ID="hdncompeIndex" runat="server" Value='<%#Eval("competitive_index") %>' />
                                        <asp:DropDownList ID="ddlcompeIndex" runat="server" Width="90%"></asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCapacityPerDay" Width="90%" CssClass="textbox" runat="server" Text='<%#Eval("max_capacity_per_day") %>'></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtPrice" runat="server" Width="90%"  CssClass="textbox" Text='<%# ConvertToMoneyFormat(Eval("price")) %>'></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="btnDelete" runat="server" CommandName="Delete" CausesValidation="true" Visible='<%#Eval("pkey_supplier_price").ToString() != "" ? true : false %>'
                                            ToolTip="Delete" CommandArgument='<%#Bind("pkey_supplier_price")%>' OnClientClick="return confirm('Results in deletion of selected record(s).Do you want to continue?')"
                                            OnClick="btnDelete_Click"><img src="../images/cross.png" /></asp:LinkButton>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
                <table>
                    <tr>
                        <td style="float: right;">
                            <asp:HiddenField ID="hdnParentID" runat="server" />
                            <asp:HiddenField ID="hdnSupplierCount" runat="server" />
                            <asp:Button runat="server" ID="btnSave" CssClass="button rounded" Text="Save" OnClick="btnSave_Click"></asp:Button>
                            <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClientClick="SetSupplierData(); return false;"></asp:Button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</body>
</html>
