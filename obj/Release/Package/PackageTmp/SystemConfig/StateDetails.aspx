﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="StateDetails.aspx.cs" Inherits="Bestprintbuy.Admin.Web.SystemConfig.StateDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript">
        function saveState() {
            if ('<%=HasAccess("VariantList", pkeystate != -2 ? "edit" : "add")%>' == 'False') {
                alert('User does not have access.');
                return false;
            }
            return true;
        }
        $().ready(function () {
            $("#frmStateDetail").validate();
        });
    </script>
    <div class="contant_hldr">
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">System Configuration</li>
            <li><a href="StateList.aspx">States List</a></li>
            <li>State Details</li>
        </ul>
        <h2>State Details</h2>
        <form class="" runat="server" id="frmStateDetail">

            <table cellpadding="0" cellspacing="0" border="0" class="accounts-table">
                <tr>
                    <td width="15%">
                        <label>State  Abbreviation*</label></td>
                    <td width="20%">
                        <asp:TextBox name="txtStateID" CssClass="text-box" ID="txtStateID" runat="server" Width="220px" MaxLength="3" required></asp:TextBox></td>
                    <td width="50%"></td>
                </tr>
                <tr>
                    <td>
                        <label>State Name*</label></td>
                    <td>
                        <asp:TextBox name="txtStateName" CssClass="text-box" ID="txtStateName" runat="server" Width="220px" required></asp:TextBox></td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <label>Country Name</label></td>
                    <td>
                        <asp:Label ID="lblCountry" runat="server" Font-Bold="true"></asp:Label>
                        <asp:HiddenField runat="server" ID="hdnCountryKey" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td style="text-align: right; padding-right: 3%">
                        <asp:Button ID="btnSave" CssClass="button rounded" runat="server" OnClientClick="return saveState();" Text="Save" OnClick="btnSave_Click" />
                        <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClick="btnClose_Click" UseSubmitBehavior="false"></asp:Button>
                    </td>
                    <td></td>
                </tr>
            </table>
        </form>
    </div>
</asp:Content>
