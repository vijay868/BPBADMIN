﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="VariantList.aspx.cs" Inherits="Bestprintbuy.Admin.Web.SystemConfig.VariantList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function deleteVariant() {
            if ('<%=HasAccess("VariantList", "delete")%>' != 'False') {
                return confirm('All attached attributes will be deleted if the products are not defined, 	Are you sure?');
            }
            else {
                alert('User does not have access.');
                return false;
            }
        }
        function addVariant() {
            if ('<%=HasAccess("VariantList", "add")%>' == 'False') {
                alert('User does not have access.');
                return false;
            }
            return true;
        }
    </script>
     <div class="clr"></div>
    <!-- Container Starts -->
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">System Configuration</li>
            <li>Variants</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Variants</h2>
        <form id="frmVariantsList" name="frmVariantsList" runat="server">           
            <div class="clr"></div>
            <asp:GridView ID="VariantslistGrid" runat="server" AutoGenerateColumns="false" EnableViewState="true"
                ForeColor="Black" Width="100%" PageSize="20" AllowPaging="true" AllowSorting="true" OnRowDeleting="VariantslistGrid_RowDeleting"
                class="table" name="VariantslistGrid" OnPageIndexChanging="VariantslistGrid_PageIndexChanging" OnSorting="VariantslistGrid_Sorting">
                <PagerStyle HorizontalAlign="Right" CssClass="dtPage"></PagerStyle>
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />
                <Columns>
                    <asp:TemplateField HeaderText="#SL">
                            <ItemTemplate>
                                <span><%# GetRecordCountForPage()%></span>
                            </ItemTemplate>
                        </asp:TemplateField>
                    <asp:TemplateField HeaderText="Variant Code" SortExpression="variant_code">
                        <HeaderStyle Width="25%" ForeColor="White" />
                        <ItemTemplate>
                            <a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("VariantDetail", "view")%>','VariantDetails.aspx?variantKey=<%# DataBinder.Eval(Container,"DataItem.pkey_variant") %>')" title="Edit"><%# DataBinder.Eval(Container,"DataItem.variant_code") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Variant Name" SortExpression="variant_name">
                        <HeaderStyle Width="25%" ForeColor="White" />
                        <ItemTemplate>
                            <a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("VariantDetail", "view")%>','VariantDetails.aspx?variantKey=<%# DataBinder.Eval(Container,"DataItem.pkey_variant") %>')" title="Edit"><%# DataBinder.Eval(Container,"DataItem.variant_name") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Comments">
                        <HeaderStyle Width="25%" />
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container,"DataItem.comments") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Delete">
                        <HeaderStyle Width="15%" />
                        <ItemTemplate>
                            <asp:LinkButton ID="btnDelete" runat="server" CommandName="Delete" CausesValidation="true"
                                ToolTip="Delete" CommandArgument='<%#Bind("pkey_variant")%>' OnClientClick="return deleteVariant();"
                                OnClick="btnDelete_Click"><img src="../images/trashcan.png" /></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <div class="ttl"><b>Total Number of Variants: </b><%=recordCount%></div>
            <!-- table Ends -->
            <div class="btn_lst">
                <asp:Button ID="btnAddnewVariant" Text="Add New Variant" CssClass="button rounded" runat="server" OnClick="btnAddnewVariant_Click" OnClientClick="return addVariant();"></asp:Button>
                <div class="rgt">
                    <asp:Button ID="btnClose" Text="Close" CssClass="button rounded" runat="server" OnClick="btnClose_Click"></asp:Button>
                </div>
            </div>
        </form>
    </div>
</asp:Content>
