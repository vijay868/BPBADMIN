﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="MasterCategoryList.aspx.cs" Inherits="Bestprintbuy.Admin.Web.SystemConfig.MasterCategoryList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

       <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
             <li class="liColor">System Configuration</li>
            <li>Master Category List</li>
     
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Master Category List</h2> 
        <form runat="server" name="frmVariantsDetails" id="frmVariantsDetails">           
            <div class="clr"></div>
      <asp:GridView ID="MasterCategoryGrid" runat="server" AutoGenerateColumns="false" EnableViewState="true"
                ForeColor="Black" Width="100%" PageSize="20" AllowPaging="true" AllowSorting="true" OnPageIndexChanging="MasterCategoryGrid_PageIndexChanging"
                class="table" name="MasterCategoryGrid" OnSorting="MasterCategoryGrid_Sorting">
                <PagerStyle HorizontalAlign="Right" CssClass="dtPage"></PagerStyle>
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />
                <Columns>
                    <asp:TemplateField HeaderText="#SL" ItemStyle-Width="5%">
                        <HeaderStyle Width="5%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <span><%# GetRecordCountForPage()%></span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Display Name" ItemStyle-Width="12%" SortExpression="mc_name">
                        <HeaderStyle Width="12%" ForeColor="White" />
                        <ItemTemplate>
                            <a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("CategoryList", "list")%>','CategoryList.aspx?mastercatKey=<%#Eval("pkey_mastercategory") %>&edit=true')"   title="Edit"><%#Eval("mc_name") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Display Description" ItemStyle-Width="15%" SortExpression="comments">
                        <HeaderStyle Width="15%" ForeColor="White" />
                        <ItemTemplate>
                         <%#GetSelectedText(Eval("comments")) %>
                        </ItemTemplate>
                    </asp:TemplateField>
                      <asp:TemplateField HeaderText="# of Screens" ItemStyle-Width="10%">
                        <HeaderStyle Width="10%" ForeColor="White" />
                        <ItemTemplate>
                       <%#Eval("screens") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Edit" ItemStyle-Width="12%">
                        <HeaderStyle Width="12%" ForeColor="White" />
                        <ItemTemplate>
                        <a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("MasterCategoryList", "view")%>','MasterCategoryDetail.aspx?mastercatKey=<%#Eval("pkey_mastercategory") %>')" title="Edit" class="button rounded" style="background-color:#a8a8a8;color:white">Edit</a>
                        </ItemTemplate>
                    </asp:TemplateField>
                 </Columns>
            </asp:GridView>
            <asp:Label ID="lblTotalRecords" runat="server" Font-Bold="true"></asp:Label>
               <div class="btn_lst" style="padding-top:20px">
                <div class="rgt">
                    <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClick="btnClose_Click"></asp:Button>
                </div>
                 
            </div>
            </form>
           </div>
</asp:Content>
