﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="ProductDetails.aspx.cs" Inherits="Bestprintbuy.Admin.Web.SystemConfig.ProductDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
        $().ready(function () {
            $("#fmProductDetails").validate();
        });

        var hasDefaultProduct = '<%=hasDefaultProduct%>';
        var isADefaultProduct = '<%=isADefaultProduct%>';

        function deleteProduct() {
            if ('<%=HasAccess("ProductList", "delete")%>' != 'False') {
                //function DoDeleteCheck() {
                if (isADefaultProduct == 0) {
                    return confirm('This action will result in deleting the free product. Do you want to continue?');
                    return true;
                }
                else {
                    alert("Default Setting on this product can not be changed.");
                    return false;
                }
                //}
            }
            else {
                alert('User does not have access.');
                return false;
            }
        }
        function saveProduct() {

            if ($("#ContentPlaceHolder1_txtPrice").val() == '') {
                alert("Product price is required and numeric, please re-enter the value between '1.00' and '99999.99'.");
                return false;
            }
            if ('<%=HasAccess("ProductList", productkey != -2 ? "edit" :"add")%>' == 'False') {
                alert('User does not have access.');
                return false;
            }
            return true;
        }

        function checkDefault(checkObject) {
            if (hasDefaultProduct == 1 && isADefaultProduct == 1) {
                alert("Default Setting on this product can not be changed.");
                checkObject.checked = true;
            }
            else if (hasDefaultProduct == 1 && isADefaultProduct == 0) {
                if (confirm("Currently another product is defined as default ,this action results in setting the current product as default. Confirm.")) {
                    isADefaultProduct = 1;
                    checkObject.checked = true;
                    return false;
                }
                else {
                    checkObject.checked = false;
                    return false;
                }
            }
            else if (hasDefaultProduct == 0 && isADefaultProduct == 0) {
                isADefaultProduct = 1;
            }
        }
    </script>
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
             <li class="liColor">System Configuration</li>
            <li><a href="ProductList.aspx">Products List</a></li>
            <li>Product Details</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Product Details</h2>
        <form id="fmProductDetails" name="fmProductDetails" runat="server">
            <table width="100%" class="accounts-table">
                <thead>
                    <tr>
                        <th colspan="4" width="100%">Product Definition</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="15%">
                            <label>Product ID*</label></td>
                        <td width="20%">
                            <asp:TextBox CssClass="text-box" name="txtProductID" ID="txtProductID" runat="server" required></asp:TextBox>
                        </td>
                        <td width="15%">
                            <label>Default Product </label>
                        </td>
                        <td width="35%">
                            <asp:CheckBox ID="chkDefaultProduct" runat="server" CssClass="check-box" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Product Category</label></td>
                        <td>
                            <asp:Label ID="lblProductCategory" runat="server" Visible="false" Font-Size="16px"></asp:Label>
                            <asp:HiddenField ID="cboProdCategory" runat="server" />
                            <asp:DropDownList name="ddlProductCategory" ID="ddlProductCategory" Visible="false" CssClass="slect-box" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlProductCategory_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <label>Has Reverse</label>
                        </td>
                        <td>
                            <asp:CheckBox ID="chkHasReverse" runat="server" CssClass="check-box" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Product Type</label></td>
                        <td>
                            <asp:Label ID="lblProductType" runat="server" Visible="false" Font-Size="16px"></asp:Label>
                            <asp:HiddenField ID="cboProductType" runat="server" />
                            <asp:DropDownList name="ddlProductType" ID="ddlProductType" Visible="false" CssClass="slect-box" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlProductType_SelectedIndexChanged">
                                <asp:ListItem Value="-1"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <label>Is Direct Mail</label></td>
                        <td>
                            <asp:CheckBox ID="chkIsDirectMail" runat="server" CssClass="check-box" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Product Name </label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtProductName" ID="txtProductName" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <label>Product Display Name </label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtProductDisplayName" ID="txtProductDisplayName" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top;">
                            <label>Comments </label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtComments" ID="txtComments" runat="server" Rows="5" Width="295px" style="resize:none;" TextMode="MultiLine"></asp:TextBox>
                        </td>
                        <td style="vertical-align:top;">
                            <label>Product Display Description</label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtProdDisplayDiscription" ID="txtProdDisplayDiscription" runat="server" Rows="5" Width="295px" style="resize:none;" TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                </tbody>
            </table>

            <table width="100%" class="accounts-table">
                <thead>
                    <tr>
                        <th colspan="4" width="100%">Product Attribute Definitions</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="4">
                            <div class="clr"></div>
                            <table class="tabled" width="100%">
                                <thead>
                                    <tr>
                                        <th>Variant</th>
                                        <th>Is Applicable</th>
                                        <th>Select Attribute</th>
                                        <th>Comments</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rptrPriceMatrix" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <%# Eval("variant_name")%>
                                                    <asp:HiddenField ID="hdnVariantID" runat="server" Value='<%# Eval("pkey_variant")%>' />
                                                    <asp:HiddenField ID="hdnPkeyPriceMatrix" runat="server" Value="" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="chkIsApplicable" runat="server" />
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlAttribute" runat="server"></asp:DropDownList><asp:TextBox ID="txtAttribute" runat="server" Visible="false" CssClass="textbox" Width="275px"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" ID="txtComments" Text='' CssClass="textbox" Width="275px"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <tr>
                                        <td colspan="4">
                                            <table class="tabled" width="100%">
                                                <tr>
                                                    <td width="30%">Price&nbsp;&nbsp;<asp:TextBox runat="server" ID="txtPrice" Text='' CssClass="textbox number" Width="275px"></asp:TextBox></td>
                                                    <td width="26%">
                                                        <asp:CheckBox ID="chkIsActive" runat="server" />&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label Text="Is Active" runat="server"></asp:Label><asp:HiddenField ID="hdnPkeyPrice" runat="server" />
                                                    </td>
                                                    <td>Price comments&nbsp;&nbsp;<asp:TextBox runat="server" CssClass="textbox" ID="txtPriceComments" Width="275px"></asp:TextBox></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>

            <table width="100%" class="accounts-table">
                <thead>
                    <tr>
                        <th width="100%">Shipping & Handling Details</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="100%">
                            <table class="tabled" width="100%">
                                <thead>
                                    <tr>
                                        <th>Shippling Period</th>
                                        <th>Price/Quantity</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblOneDay" Text="Express Shipping & Handling - (one day)" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtOneDay" CssClass="textbox" Text='<%#ConvertToMoneyFormat(Eval("price_1")) %>' Width="275px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblTwoDays" Text="Speed Shipping & Handling - (two days)" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtTwoDays" CssClass="textbox" Text='<%#ConvertToMoneyFormat(Eval("price_2")) %>' Width="275px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblSevenDays" Text="Standard Shipping & Handling - (7 days)" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtSevenDays" CssClass="textbox" Text='<%#ConvertToMoneyFormat(Eval("price_7")) %>' Width="275px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblTenDays" Text="Standard Shipping & Handling - (10 days)" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtTenDays" CssClass="textbox" Text='<%#ConvertToMoneyFormat(Eval("price_14")) %>' Width="275px"></asp:TextBox>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                   
                </tbody>
            </table>
               <div class="btn_lst" style="padding-top:20px">
                <div class="rgt">
                    <asp:Button runat="server" CssClass="button rounded close-btn" ID="btnClose" Text="Close" UseSubmitBehavior="false" OnClick="btnClose_Click"></asp:Button>
                    <asp:Button runat="server" ID="btnDelete" CssClass="button rounded" Text="Delete" Visible="false" UseSubmitBehavior="false" OnClientClick="if(!deleteProduct()) return false;" OnClick="btnDelete_Click"></asp:Button>
                     <asp:Button runat="server" ID="btnSave" CssClass="button rounded" Text="Save" OnClientClick="return saveProduct();" OnClick="btnSave_Click"></asp:Button>
                </div>
                 
            </div>
            <!-- table Ends -->
        </form>
    </div>
</asp:Content>
