﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="CountryList.aspx.cs" Inherits="Bestprintbuy.Admin.Web.SystemConfig.CountryList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <script type="text/javascript">
          function deleteCountry() {
              if ('<%=HasAccess("CountryList", "delete")%>' != 'False') {
                return confirm('Results in deletion of selected record(s).Do you want to continue?');
            }
            else {
                alert('User does not have access.');
                return false;
            }
        }
        function addCountry() {
            if ('<%=HasAccess("CountryList", "add")%>' == 'False') {
                alert('User does not have access.');
                return false;
            }
            return true;
        }
    </script>
    <div class="contant_hldr">
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">System Configuration</li>
            <li>Countries</li>
        </ul>
        <h2>Countries</h2>
        <form class="" runat="server" id="frmOrderFulfillmentList">           
            <div class="clr"></div>           
            <asp:GridView ID="CountryGrid" runat="server" AutoGenerateColumns="false" EnableViewCountry="true"
                ForeColor="Black" Width="100%" PageSize="20" AllowPaging="true" OnPageIndexChanging="CountryGrid_PageIndexChanging"
                class="table" name="CountryGrid" OnRowDeleting="CountryGrid_RowDeleting">
                <PagerStyle HorizontalAlign="Right" CssClass="dtPage"></PagerStyle>
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />
                <Columns>
                    <asp:TemplateField HeaderText="#SL" ItemStyle-Width="5%">
                        <HeaderStyle Width="5%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <span><%# GetRecordCountForPage()%></span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Country Abbrevation" ItemStyle-Width="9%">
                        <HeaderStyle Width="9%" ForeColor="White" />
                        <ItemTemplate>
                            <a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("CountryDetail", "view")%>','CountryDetail.aspx?pkeyCountry=<%#Eval("pkey_country") %>')"  data-gravity="s" title="Edit"><%#Eval("country_abbr") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Country Name" ItemStyle-Width="10%">
                        <HeaderStyle Width="10%" ForeColor="White" />
                        <ItemTemplate>
                          <a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("CountryDetail", "view")%>','CountryDetail.aspx?pkeyCountry=<%#Eval("pkey_country") %>')"  data-gravity="s" title="Edit"><%#Eval("country_name") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Isdefault" ItemStyle-Width="12%">
                        <HeaderStyle Width="12%" ForeColor="White" />
                        <ItemTemplate>
                          <%#Eval("is_default") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Delete" ItemStyle-Width="12%">
                        <HeaderStyle Width="12%" ForeColor="White" />
                        <ItemTemplate>
                          <asp:LinkButton ID="btnDelete" runat="server" CommandName="Delete" CausesValidation="true" 
                                                    ToolTip="Delete" CommandArgument='<%#Bind("pkey_country")%>' OnClientClick="return deleteCountry();"
                                                     OnClick="btnDelete_Click"><img src="../images/trashcan.png" /></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>

                    
                </Columns>
            </asp:GridView>
            <asp:Label ID="lblTotalRecords" runat="server" Font-Bold="true"></asp:Label>
            <div class="btn_lst" style="padding-top:20px">
                <div class="rgt">
                    <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClick="btnClose_Click"></asp:Button>
                </div>
                 <div class="lft">
                     <asp:Button runat="server" ID="btnAddCountry" CssClass="button rounded"  Text="Add a Country" OnClientClick="return addCountry();" OnClick="btnAddCountry_Click"></asp:Button>
                </div>
            </div>
        </form>
    </div>
</asp:Content>
