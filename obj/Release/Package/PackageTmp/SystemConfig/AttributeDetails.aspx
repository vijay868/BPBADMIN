﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="AttributeDetails.aspx.cs" Inherits="Bestprintbuy.Admin.Web.SystemConfig.AttributeDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function addAttribute() {
            $("#ContentPlaceHolder1_txtAttributeCode").removeAttr("required");
            $("#ContentPlaceHolder1_txtAttributeName").removeAttr("required");
            if ('<%=HasAccess("AttributeList", "add")%>' == 'False') {
                alert('User does not have access.');
                return false;
            }
            return true;
        }
        function deleteAttribute() {
            if ('<%=HasAccess("AttributeList", "delete")%>' != 'False') {
                return confirm('All attached attributes will be deleted if the products are not defined, 	Are you sure?');
            }
            else {
                alert('User does not have access.');
                return false;
            }
        }
        function saveAttribute() {
            if ('<%=HasAccess("AttributeList", attributekey != -2 ? "edit" : "add")%>' == 'False') {
                alert('User does not have access.');
                return false;
            }
            return true;
        }

        $().ready(function () {
            $("#frmAttributesDetails").validate();
        });
    </script>
    <div class="clr"></div>
    <!-- Container Starts -->
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">System Configuration</li>
            <li><a href="AttributeList.aspx">Attribute List</a></li>
            <li>Attribute Details</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Attribute Details</h2>
        <form runat="server" name="frmAttributesDetails" id="frmAttributesDetails">
            <table width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="15%" style="vertical-align: top;">
                            <label>Attribute Code*</label>
                        </td>
                        <td width="20%" style="vertical-align: top;">
                            <asp:TextBox CssClass="text-box" name="txtAttributeCode" ID="txtAttributeCode" runat="server" required></asp:TextBox>
                        </td>
                        <td width="15%" style="vertical-align: top;" rowspan="3">
                            <label>Comments</label>
                        </td>
                        <td width="35%" style="vertical-align: top;" rowspan="3">
                            <asp:TextBox CssClass="text-box" name="txtComments" ID="txtComments" runat="server" Rows="8" Width="295px" style="resize:none;" TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">
                            <label>Attribute Name*</label>
                        </td>
                        <td style="vertical-align: top;">
                            <asp:TextBox CssClass="text-box" name="txtAttributeName" ID="txtAttributeName" runat="server" required></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">
                            <label>Variants*</label>
                        </td>
                        <td style="vertical-align: top;">
                            <asp:Label ID="lblVariants" runat="server" Visible="false" Font-Size="14px"></asp:Label>
                            <asp:DropDownList name="ddlCategory" ID="ddlCategory" Visible="false" runat="server" CssClass="slect-box">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </tbody>
            </table>
          <%-- <table width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="35%" style="float:left;">
                            <asp:Button runat="server" ID="btnAdd" Text="Add" CssClass="button rounded" Visible="false" OnClientClick="return addAttribute();" OnClick="btnAdd_Click"></asp:Button>
                            <asp:Button runat="server" ID="btnDelete" Text="Delete" Visible="false" CssClass="button rounded" OnClientClick="if(!deleteAttribute()) return false;" UseSubmitBehavior="false" OnClick="btnDelete_Click"></asp:Button>                           
                        </td>
                          <td width="17%">&nbsp;</td>
                        <td width="15%">&nbsp;</td>
                        <td width="26%" style="float:right;">
                            <asp:Button runat="server" ID="btnSave" Text="Save" OnClick="btnSave_Click" CssClass="button rounded" OnClientClick="return saveAttribute();"></asp:Button>
                            <asp:Button runat="server" ID="btnClose" Text="Close" OnClick="btnClose_Click" CssClass="button rounded" UseSubmitBehavior="false"></asp:Button>
                        </td>
                    </tr>
                </tbody>
            </table>--%>
             <div class="btn_lst" style="padding-top:20px">
                <div class="lft">
                      <asp:Button runat="server" ID="btnAdd" Text="Add" CssClass="button rounded" Visible="false" OnClientClick="return addAttribute();" OnClick="btnAdd_Click"></asp:Button>
                      <asp:Button runat="server" ID="btnDelete" Text="Delete" Visible="false" CssClass="button rounded" OnClientClick="if(!deleteAttribute()) return false;" UseSubmitBehavior="false" OnClick="btnDelete_Click"></asp:Button>                           
                </div>
                 <div class="rgt" style="padding-right:7%">
                     <asp:Button runat="server" ID="btnClose" Text="Close" OnClick="btnClose_Click" CssClass="button rounded" UseSubmitBehavior="false"></asp:Button>
                     <asp:Button runat="server" ID="btnSave" Text="Save" OnClick="btnSave_Click" CssClass="button rounded" OnClientClick="return saveAttribute();"></asp:Button>
                </div>
            </div>
        </form>
    </div>
</asp:Content>
