﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="TypeList.aspx.cs" Inherits="Bestprintbuy.Admin.Web.SystemConfig.TypeList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function deleteType() {
            if ('<%=HasAccess("TypeList", "delete")%>' != 'False') {
            return confirm('Results in deletion of selected record(s).Do you want to continue?');
        }
        else {
            alert('User does not have access.');
            return false;
        }
    }
    function addType() {
        if ('<%=HasAccess("TypeList", "add")%>' == 'False') {
                alert('User does not have access.');
                return false;
            }
            return true;
        }
        function viewType() {
            if ('<%=HasAccess("TypeDetail", "view")%>' == 'False') {
                alert('User does not have access.');
                return false;
            }
            return true;
        }
    </script>
    <div class="contant_hldr">
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
             <li class="liColor">System Configuration</li>
            <li>Types</li>
        </ul>
        <h2>Types</h2>
        <form class="" runat="server" id="frmOrderFulfillmentList">
            <div class="form_sep_left" style="padding-right: 20%; padding-left: 7%">
                <table class="accounts-table">
                    <tbody>
                        <tr>
                            <td width="6%">Master Category </td>
                            <td width="10%">
                                <asp:DropDownList name="ddlMasterCategory" ID="ddlMasterCategory" CssClass="slect-box" Width="220px" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlMasterCategory_SelectedIndexChanged">
                                    <asp:ListItem Value=""></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td width="6%">Category</td>
                            <td width="10%">
                                <asp:DropDownList name="ddlCategory" ID="ddlCategory" CssClass="slect-box" Width="220px" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged">
                                    <asp:ListItem Value=""></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>           
            <div class="clr"></div>
            <asp:GridView ID="TypesGrid" runat="server" AutoGenerateColumns="false" EnableViewState="true"
                ForeColor="Black" Width="100%" PageSize="20" AllowPaging="true" OnPageIndexChanging="TypesGrid_PageIndexChanging"
                class="table" name="TypesGrid" OnRowDeleting="TypesGrid_RowDeleting">
                <PagerStyle HorizontalAlign="Right" CssClass="dtPage"></PagerStyle>
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />
                <Columns>
                    <asp:TemplateField HeaderText="#SL" ItemStyle-Width="5%">
                        <HeaderStyle Width="5%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <span><%# GetRecordCountForPage()%></span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Type ID" ItemStyle-Width="9%">
                        <HeaderStyle Width="9%" ForeColor="White" />
                        <ItemTemplate>
                            <asp:LinkButton ID="btnCtAbbr" runat="server" CommandName="Delete" CausesValidation="true" OnClientClick="return viewType();"
                                ToolTip="Delete" CommandArgument='<%#Bind("pkey_category_type")%>' OnClick="btnCtAbbr_Click"><%#Eval("ct_abbr") %></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Type Name" ItemStyle-Width="10%">
                        <HeaderStyle Width="10%" ForeColor="White" />
                        <ItemTemplate>
                            <asp:LinkButton ID="btnCtName" runat="server" CommandName="Delete" CausesValidation="true" OnClientClick="return viewType();"
                                ToolTip="Delete" CommandArgument='<%#Bind("pkey_category_type")%>' OnClick="btnCtName_Click"><%#Eval("ct_name") %></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="# of Screens" ItemStyle-Width="10%">
                        <HeaderStyle Width="10%" ForeColor="White" />
                        <ItemTemplate>
                            <%#Eval("no_of_screens") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Delete" ItemStyle-Width="12%">
                        <HeaderStyle Width="12%" ForeColor="White" />
                        <ItemTemplate>
                            <asp:LinkButton ID="btnDelete" runat="server" CommandName="Delete" CausesValidation="true"
                                ToolTip="Delete" CommandArgument='<%#Bind("pkey_category_type")%>' OnClientClick="return deleteType();"
                                OnClick="btnDelete_Click"><img src="../images/trashcan.png" /></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <div class="clr"></div>
            <asp:Label ID="lblTotalRecords" runat="server" Font-Bold="true"></asp:Label>
            <div class="btn_lst" style="padding-top: 30px">
                <div class="rgt">
                    <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClick="btnClose_Click"></asp:Button>
                </div>
                <div class="lft">
                    <asp:Button runat="server" ID="btnAddState" CssClass="button rounded" Text="Add Type" OnClientClick="return addType();" OnClick="btnAdd_Click"></asp:Button>
                </div>
            </div>
        </form>
    </div>
</asp:Content>
