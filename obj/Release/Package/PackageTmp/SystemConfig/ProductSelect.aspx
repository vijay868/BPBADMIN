﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProductSelect.aspx.cs" Inherits="Bestprintbuy.Admin.Web.SystemConfig.ProductSelect" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head id="Head1" runat="server">
    <title>Best Print Buy</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" media="all" href="../css/bestprintbuy_style.css" />
    <link rel="stylesheet" media="all" href="../css/jquery.fancybox.css" />
    <script src="../Scripts/jquery.js"></script>
    <script src="../Scripts/common.js"></script>
    <script src="../Scripts/ddsmoothmenu.js"></script>
    <script src="../Scripts/plugin/fancybox/jquery.fancybox.js"></script>
    <!--[if lt IE 9]>
			<script src="../Scripts/html5.js"></script>
		<![endif]-->
    <!--[if lte IE 7]> <html class="ie7"> <![endif]-->
    <!--[if IE 8]>     <html class="ie8"> <![endif]-->
    <!--[if IE 9]>     <html class="ie9"> <![endif]-->
    <!--[if IE 10]>     <html class="ie10"> <![endif]-->
    <!--[if !IE]><!-->
    <!--<![endif]-->
        <script type="text/javascript">

            function SetDataAndClose(isNodata, ProdIds, ProdNames, ProdCat) {
                /*if (isNodata) {
                    alert("Plaese select");
                    return false;
                }*/
                window.top.SetProductData(ProdIds, ProdNames, ProdCat);
                return false;
            }
            
            function checkSelect() {
                var ischecked = false;
                $('.select input:checkbox').each(function () {
                    if (this.checked) {
                        ischecked = true;
                        return true;
                    }
                });

                if (!ischecked) {
                    alert("Please select atleast one product.");
                    return false;
                }
                return true;
            }
    </script>

</head>
<body>
    <!-- Wrapper starts -->
    <div id="Div1" class="popup-wrapper" runat="server">
        <div class="popup-title">
            Products List
        </div>
        <!-- Header Ends -->
        <div class="popup-contnt">
            <!-- Menu starts -->
            <form id="frmProductSelect" runat="server" name="frmProductSelect">                    
                        <table class="tablep">
                            <thead>
                                <tr>
                                    <th>Product Id</th>
                                    <th>Product Name</th>
                                    <%--<th>Product description</th>--%>
                                    <th>Active</th>
                                    <th># of Template</th>
                                    <th>Select</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rptrAvailableProductTypes" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <%#Eval("product_id") %>
                                            </td>
                                            <td>
                                                <%#Eval("product_name") %>
                                            </td>
                                           <%-- <td>
                                                <%#Eval("comments") %>
                                            </td>--%>
                                            <td>
                                                <%#Eval("is_active") %>
                                            </td>
                                            <td>
                                                <%#Eval("order_value") %>
                                            </td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkSelect" CssClass="select" />
                                                <asp:HiddenField ID="hdnProductName" runat="server" Value='<%#Eval("product_id") %>' />
                                                <asp:HiddenField ID="hdnProductType" runat="server" Value='<%#Eval("pkey_product") %>' />
                                                <asp:HiddenField ID="hdnProductCat" runat="server" Value='<%#Eval("pkey_template_product_xref") %>' />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>                                
                            </tbody>
                        </table>
                    <table>
                        <tr>
                            <td style="float:right;">
                                <asp:Button runat="server" ID="btnSelect" CssClass="button rounded" Text="Select" OnClientClick="if(!checkSelect()) return false;" OnClick="btnSelect_Click"></asp:Button>
                                <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClientClick="parent.$.fancybox.close(); return false;"></asp:Button>
                            </td>
                        </tr>
                    </table>
            </form>
        </div>
    </div>
</body>
</html>
