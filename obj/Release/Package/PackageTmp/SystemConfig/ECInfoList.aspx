﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="ECInfoList.aspx.cs" Inherits="Bestprintbuy.Admin.Web.SystemConfig.ECInfoList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="contant_hldr">
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">System Configuration</li>
            <li>Kiosks List</li>
        </ul>
        <h2>Kiosks List</h2>
        <form class="" runat="server" id="frmECInfoList">           
            <div class="clr"></div>           
            <asp:GridView ID="ECInfoListGrid" runat="server" AutoGenerateColumns="false" EnableViewCountry="true" AllowSorting="true"
                ForeColor="Black" Width="100%" PageSize="20" AllowPaging="true" OnPageIndexChanging="ECInfoListGrid_PageIndexChanging"
                class="table" name="ECInfoListGrid" OnSorting="ECInfoListGrid_Sorting">
                <PagerStyle HorizontalAlign="Right" CssClass="dtPage"></PagerStyle>
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />
                <Columns>
                    <asp:TemplateField HeaderText="#SL">
                        <HeaderStyle ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <span><%# GetRecordCountForPage()%></span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Attribute Code" SortExpression="attribute_code">
                        <HeaderStyle Width="20%" ForeColor="White" />
                        <ItemTemplate>
                            <a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("AttributeDetail", "view")%>','EcInfo.aspx?ECattributeKey=<%#Eval("pkey_attribute") %>')"  data-gravity="s" title="Edit"><%#Eval("attribute_code") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Attribute Name" SortExpression="attribute_value">
                        <HeaderStyle Width="40%" ForeColor="White" />
                        <ItemTemplate>
                          <a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("AttributeDetail", "view")%>','EcInfo.aspx?ECattributeKey=<%#Eval("pkey_attribute") %>')"  data-gravity="s" title="Edit"><%#Eval("attribute_value") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Comments">
                        <HeaderStyle Width="35%" ForeColor="White" />
                        <ItemTemplate>
                          <%#Eval("comments") %>
                        </ItemTemplate>
                    </asp:TemplateField>                    
                </Columns>
            </asp:GridView>
            <div class="ttl"><b>Total Number of Attributes: </b><%=recordCount%></div>
            <div class="btn_lst">
                <div class="rgt">
                    <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClick="btnClose_Click"></asp:Button>
                </div>
            </div>
        </form>
    </div>
</asp:Content>
