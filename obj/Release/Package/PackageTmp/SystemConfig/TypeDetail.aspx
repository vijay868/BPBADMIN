﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="TypeDetail.aspx.cs" Inherits="Bestprintbuy.Admin.Web.SystemConfig.TypeDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function saveType() {
            if ('<%=HasAccess("TypeList", pkeyType != -2 ? "edit" : "add")%>' == 'False') {
                alert('User does not have access.');
                return false;
            }
           if(!selectMemberRoles()) return false;
            return true;
        }

        $().ready(function () {
            $("#frmTypeDetail").validate();
        });

        function moveProductOptions(option, moveAll) {
            fromObject = document.getElementById("ContentPlaceHolder1_lbAvailableProductTypes");
            toObject = document.getElementById("ContentPlaceHolder1_lbAssignedProductTypes");

            if (option == "right") {
                from = eval(fromObject);
                to = eval(toObject);
            }
            else {
                from = eval(toObject);
                to = eval(fromObject);
            }

            if (from && to) {
                var fromSize = from.options.length;
                var toSize = to.options.length;
                var fromArray = new Array();
                var fromSelectedArray = new Array();
                var fromSelectedIndex = 0;
                var fromIndex = 0;
                for (i = fromSize - 1; i > -1; i--) {
                    var isSelected = false
                    if (from.options[i].selected)
                        isSelected = from.options[i].selected;
                    if ((isSelected && isSelected == true) || moveAll == true) {
                        fromSelectedArray[fromSelectedIndex] = new Array(from.options[i].value, from.options[i].text);
                        from.options[i] = null;
                        fromSelectedIndex++;
                    }
                    else {
                        fromArray[fromIndex] = new Array(from.options[i].value, from.options[i].text);
                        from.options[i] = null;
                        fromIndex++;
                    }
                }
                fromSize = fromArray.length;
                for (i = 0; i < fromSize; i++) {
                    from.options[i] = new Option(fromArray[fromSize - 1 - i][1], fromArray[fromSize - 1 - i][0]);
                }
                var newToSize = fromSelectedArray.length;
                var strData = "";
                for (i = 0; i < newToSize; i++) {
                    toSize = to.options.length;
                    to.options[toSize] = new Option(fromSelectedArray[i][1], fromSelectedArray[i][0]);
                    strData = strData + to.options[toSize].value;
                    if (i != newToSize - 1) strData = strData + ",";
                }
            }
        }

        function selectMemberRoles() {
            if (document.getElementById('ContentPlaceHolder1_lbAssignedProductTypes')) {
                var leng = document.getElementById('ContentPlaceHolder1_lbAssignedProductTypes').options.length;
                for (i = leng - 1; i > -1; i--) {
                    document.getElementById('ContentPlaceHolder1_lbAssignedProductTypes').options[i].selected = true;
                }
                if (leng == 0) {
                    alert("Atleast one product type is required for saving the data. Please select.");
                    return false;
                }
                return true;
            }
        }
    </script>
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
             <li class="liColor">System Configuration</li>
            <li><a href="TypeList.aspx">Type List</a></li>
            <li>Type Details</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Type Details</h2>
        <form id="frmTypeDetail" name="frmTypeDetail" runat="server">
            <table width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="15%">
                            <label>Type ID*</label></td>
                        <td width="20%">
                            <asp:TextBox CssClass="text-box" name="txtTypeID" ID="txtTypeID" runat="server" MaxLength="3" required></asp:TextBox>
                        </td>
                        <td width="15%">
                            <label>Type Name*</label></td>
                        <td width="35%">
                            <asp:TextBox CssClass="text-box" name="txtTypeName" ID="txtTypeName" runat="server" required></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Master Category</label></td>
                        <td>
                            <asp:Label ID="lblMasterCategory" runat="server" />
                        </td>
                        <td style="vertical-align: top">
                            <label>Comments</label></td>
                        <td rowspan="3">
                            <asp:TextBox CssClass="text-box" name="txtComments" ID="txtComments" runat="server" style="resize:none;" TextMode="MultiLine" Rows="8" Width="295px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>
                            <label>Category</label>
                        </td>
                        <td>
                            <asp:Label ID="lblCategory" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label></label>
                        </td>
                        <td>
                            <asp:CheckBox ID="chkBackOrdered" runat="server" CssClass="check-box" /><asp:Label Text="   Back Ordered." runat="server" />
                        </td>
                    </tr>
                </tbody>
            </table>

            <table width="100%" class="accounts-table" id="pnlProductSelection" runat="server" visible="false">
                <tr>
                    <td width="15%">
                        <label></label>
                    </td>
                    <td width="20%">
                        <asp:CheckBox ID="chkParentType" runat="server" CssClass="check-box" />
                        <asp:Label Text="   Has Parent Type." runat="server" />
                    </td>
                    <td width="15%">
                        <label></label>
                    </td>
                    <td width="35%">
                        <asp:CheckBox ID="chkRealestste" runat="server" CssClass="check-box" /><asp:Label Text="    Is RealEstate." runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Parent Type</label>
                    </td>
                    <td>
                        <asp:DropDownList name="ddlParentType" ID="ddlParentType" CssClass="slect-box" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <table width="100%" class="accounts-table">
                            <thead>
                                <tr>
                                    <th colspan="4" width="100%">Apply Eligible Product Types</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td width="15%">
                                        <label></label>
                                    </td>
                                    <td width="50%">
                                        <div style="width: 40%; float: left">
                                            <label style="padding-left: 0px;"><b>Available Product Types</b></label>
                                            <asp:ListBox ID="lbAvailableProductTypes" CssClass="list-box" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                        </div>
                                        <div style="width: 10%; float: left; padding: 40px 10px 0 10px;">
                                            <button id="to2" type="button" class="button rounded width53" onclick="javascript:moveProductOptions('right',false)">
                                                &nbsp;&gt;&nbsp;</button><br />
                                            <button id="allTo2" type="button" class="button rounded width53" onclick="javascript:moveProductOptions('right',true)">
                                                &nbsp;&gt;&gt;&nbsp;</button><br />
                                            <button id="to1" type="button" class="button rounded width53" onclick="javascript:moveProductOptions('left',false)">
                                                &nbsp;&lt;&nbsp;</button><br />
                                            <button id="allTo1" type="button" class="button rounded width53" onclick="javascript:moveProductOptions('left',true)">
                                                &nbsp;&lt;&lt;&nbsp;</button>
                                        </div>
                                        <div style="width: 35%; float: left; padding-right: 10px;">
                                            <label style="padding-left: 0px;"><b>Eligible Product Types</b></label>
                                            <asp:ListBox ID="lbAssignedProductTypes" runat="server" CssClass="list-box" SelectionMode="Multiple"></asp:ListBox>
                                        </div>
                                    </td>
                                    <td width="35%"></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>
            <table width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="15%">&nbsp;</td>
                        <td width="17%">&nbsp;</td>
                        <td width="15%">&nbsp;</td>
                        <td width="35%" style="float: right;">
                            <asp:Button runat="server" CssClass="button rounded" ID="btnSave" Text="Save" OnClientClick="return saveType();" OnClick="btnSave_Click"></asp:Button>
                            <asp:Button runat="server" CssClass="button rounded" ID="btnClose" Text="Close" UseSubmitBehavior="false" OnClick="btnClose_Click"></asp:Button>
                        </td>
                    </tr>
                </tbody>
            </table>
            <!-- table Ends -->
        </form>
    </div>
</asp:Content>

