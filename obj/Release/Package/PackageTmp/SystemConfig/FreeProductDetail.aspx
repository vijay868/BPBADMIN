﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="FreeProductDetail.aspx.cs" Inherits="Bestprintbuy.Admin.Web.SystemConfig.FreeProductDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function deleteProduct() {
            if ('<%=HasAccess("FreeProductsList", "delete")%>' != 'False') {
                return confirm('This action will result in deleting the free product. Do you want to continue?');
            }
            else {
                alert('User does not have access.');
                return false;
            }
        }
        function saveProduct() {
            if ('<%=HasAccess("FreeProductsList", productKey != -2 ? "edit" :"add")%>' == 'False') {
                alert('User does not have access.');
                return false;
            }
            else {
                var dateStart = $("#ContentPlaceHolder1_txtFromDate").val();
                var dateEnd = $("#ContentPlaceHolder1_txtToDate").val();
                if (Date.parse(dateStart) > Date.parse(dateEnd)) {
                    alert("Invalid date range!");
                    return false;
                }
            }
            return true;
        }

        $(function () {
            $("#ContentPlaceHolder1_txtFromDate").datepicker();
            $("#ContentPlaceHolder1_txtToDate").datepicker();
        });

        $().ready(function () {
            $("#frmfreeProductsDetails").validate();
        });

    </script>

    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">System Configuration</li>
            <li><a href="FreeProductsList.aspx">Free Products List</a></li>
            <li>Free Product Detail</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Free Product Detail</h2>
        <form id="frmfreeProductsDetails" name="frmfreeProductsDetails" runat="server">
            <table width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="15%">
                            <label>Product ID*</label></td>
                        <td width="20%">
                            <asp:TextBox CssClass="text-box" name="txtProductID" ID="txtProductID" runat="server" required></asp:TextBox>
                        </td>
                        <td width="15%">
                            <label>Product Category* </label>
                        </td>
                        <td width="35%">
                            <asp:DropDownList name="ddlProductCat" ID="ddlProductCat" CssClass="slect-box" runat="server" OnSelectedIndexChanged="ddlProductCat_SelectedIndexChanged" AutoPostBack="true" required>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Product Name*</label></td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtProductName" ID="txtProductName" runat="server" required></asp:TextBox>
                        </td>
                        <td>
                            <label>Product Type* </label>
                        </td>
                        <td>
                            <asp:DropDownList name="ddlProductType" ID="ddlProductType" CssClass="slect-box" runat="server" required>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Valid From*</label></td>
                        <td>
                            <asp:TextBox CssClass="text-box date" name="txtFromDate" ID="txtFromDate" runat="server" required></asp:TextBox>
                        </td>
                        <td>
                            <label>Valid To*</label></td>
                        <td>
                            <asp:TextBox CssClass="text-box date" name="txtToDate" ID="txtToDate" runat="server" required></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label></label>
                        </td>
                        <td>
                            <asp:CheckBox ID="chkActive" runat="server" CssClass="check-box" /><asp:Label runat="server" Text="    Active"></asp:Label>
                        </td>
                        <td>
                            <label>Quantity*</label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box digits" name="txtQuatity" ID="txtQuatity" runat="server" required></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Fore Color* </label>
                        </td>
                        <td>
                            <asp:DropDownList name="ddlForeColor" ID="ddlForeColor" CssClass="slect-box" runat="server" required>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <label>Reverse Color*</label>
                        </td>
                        <td>
                            <asp:DropDownList name="ddlReverseColor" ID="ddlReverseColor" CssClass="slect-box" runat="server" required>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Paper Type*</label>
                        </td>
                        <td>
                            <asp:DropDownList name="ddlPaperType" ID="ddlPaperType" CssClass="slect-box" runat="server" required>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <label>Size*</label>
                        </td>
                        <td>
                            <asp:DropDownList name="ddlSize" ID="ddlSize" CssClass="slect-box" runat="server" required>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Shipping & Handling Cost*</label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box number" name="txtShippingCost" ID="txtShippingCost" runat="server" required></asp:TextBox>
                        </td>
                        <td>
                            <label>Shipping & Handling Desc*</label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtShippingDescr" ID="txtShippingDescr" runat="server" required></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Thumbnail Url*</label>
                        </td>
                        <td>
                            <asp:HiddenField ID="hdnThumbNail" runat="server" />
                            <asp:TextBox CssClass="text-box" name="txtThumbnailUrl" ID="txtThumbnailUrl" runat="server" required></asp:TextBox>
                        </td>
                        <td colspan="2" align="right">
                            <asp:Button runat="server" ID="btnDelete" CssClass="button rounded" Text="Delete" Visible="false" OnClientClick="if(!deleteProduct()) return false;" OnClick="btnDelete_Click" UseSubmitBehavior="false"></asp:Button>
                            <asp:Button runat="server" ID="btnSave" CssClass="button rounded" Text="Save" OnClientClick="return saveProduct();" OnClick="btnSave_Click"></asp:Button>
                            <asp:Button runat="server" CssClass="button rounded close-btn" ID="btnClose" Text="Close" UseSubmitBehavior="false" OnClick="btnClose_Click"></asp:Button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
</asp:Content>
