﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="EcInfo.aspx.cs" Inherits="Bestprintbuy.Admin.Web.SystemConfig.EcInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
        function SetMessageTypeData(msgID, fields) {
            $("#ContentPlaceHolder1_hdnMsg").val(msgID);
            $("#ContentPlaceHolder1_hdnfields").val(fields);
            $("#divID").dialog("close");
        }

        function OpenMessageDialog() {
            var fkey_ec_attribute = "<%= fkeyECAttribute%>";

            var url = "../SystemConfig/SelectMessages.aspx?fkey_ec_attribute=" + fkey_ec_attribute + "&timestamp=<%= System.DateTime.Now.Ticks.ToString()%>&fields=Disclaimer";
            $.fancybox({
                'width': '70%',
                'height': '40%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': url,
                'type': 'iframe'
            });
            return false;
        }

        function ClosePopup() {
            parent.$.fancybox.close();
        }

        function SetMessageTypeData(msgID, fields) {
            $("#ContentPlaceHolder1_hdnMsg").val(msgID);
            $("#ContentPlaceHolder1_hdnfields").val(fields);
            parent.$.fancybox.close();
        }

        $().ready(function () {
            $("#frmECInfo").validate();
        });

        function saveKiosk() {
            if ('<%=HasAccess("AttributeList", ECkioskKey != -2 ? "edit" : "add")%>' == 'False') {
                alert('User does not have access.');
                return false;
            }
            return true;
        }
    </script>
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">System Configuration</li>
            <li><a href="ECInfoList.aspx">Kiosk List</a></li>
            <li>Exclusive Customer</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Exclusive Customer</h2>
        <form id="frmECInfo" name="frmECInfo" runat="server">
            <table width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="10%">
                            <label>Company Name*</label></td>
                        <td width="10%">
                            <asp:TextBox CssClass="text-box" name="txtCompanyName" ID="txtCompanyName" runat="server" required></asp:TextBox>
                        </td> 
                        <td width="60%"></td>                      
                    </tr>
                    <tr>
                         <td>
                            <label>Company Id*</label></td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtCompanyID" ID="txtCompanyID" runat="server" required></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Kiosk Id*</label></td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtKioskID" ID="txtKioskID" runat="server" required></asp:TextBox>
                        </td>                        
                    </tr>
                    <tr>
                        <td style="vertical-align: top">
                            <label>Images Directory*</label></td>
                        <td>
                            <asp:Label ID="lblImageDir" runat="server" required /></td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top">
                            <label>Banner Location*</label>
                        </td>
                        <td>
                            <asp:FileUpload ID="BannerImgFile" runat="server" Enabled="false" Width="315px" Height="22px" required/><br />
                            Banner file will be uploaded to
                            <asp:Label ID="lblBannerLoc" runat="server" Font-Size="14px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top">
                            <label>masthead Location*</label>
                        </td>
                        <td>
                            <asp:FileUpload ID="mastheadImgFile" runat="server" Width="315px" Height="22px" Enabled="false" required /><br />
                            masthead file will be uploaded to
                            <asp:Label ID="lblMastheadLoc" runat="server" Font-Size="14px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Logo Location*</label>
                        </td>
                        <td>
                            <asp:Label ID="lblLogoLoc" runat="server" required />
                        </td>
                    </tr>
                    <tr>
                        <td width="15%" style="float: left;">
                            <asp:HiddenField ID="hdnMsg" runat="server" />
                            <asp:HiddenField ID="hdnfields" runat="server" />
                            <asp:HiddenField runat="server" ID="hdnDirloc" />
                            <asp:HiddenField ID="hdnbannerLoc" runat="server" />
                            <asp:HiddenField ID="hdnmastLoc" runat="server" />
                            <asp:HiddenField runat="server" ID="hdnLogoLoc" />
                            <asp:Button runat="server" ID="btnUpdateKioskMsg" Text="Update Kiosk Message List" CssClass="button rounded" UseSubmitBehavior="false" OnClientClick="return OpenMessageDialog();"></asp:Button>

                        </td>
                        <td class="rgt" align="right">
                            <asp:Button runat="server" CssClass="button rounded" ID="btnSave" Text="Save" OnClientClick="return saveKiosk();" OnClick="btnSave_Click"></asp:Button>
                            <asp:Button runat="server" CssClass="button rounded" ID="btnClose" Text="Close" UseSubmitBehavior="false" OnClick="btnClose_Click"></asp:Button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
</asp:Content>
