﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="MasterCategoryDetail.aspx.cs" Inherits="Bestprintbuy.Admin.Web.SystemConfig.MasterCategoryDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function saveMasterCategoryDtls() {
            if ('<%=HasAccess("MasterCategoryList", "edit")%>' == 'False') {
                alert('User does not have access.');
                return false;
            }
            return true;
        }
    </script>
    <div class="contant_hldr">
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
             <li class="liColor">System Configuration</li>
            <li><a href="MasterCategoryList.aspx"> Master Category Lists</a></li>
            <li>Master Category Details</li>
        </ul>
        <h2>Master Category Details</h2>
        <form runat="server" id="frmMasteCateDetails">
            <table class="accounts-table" width="100%">
                <tbody>
                    <tr>
                        <td width="15%"><label>Default Name</label></td>
                        <td width="20%">
                            <asp:Label ID="lblDefaultName" runat="server" Font-Size="14px"></asp:Label></td>
                        <td width="15%"><label>Display Name</label></td>
                        <td width="35%">
                            <asp:TextBox CssClass="text-box" name="txtDisplayName" ID="txtDisplayName" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td width="15%" style="vertical-align:top"><label>Default Description</label></td>
                        <td width="35%">
                            <asp:TextBox name="txtDefaultDesc" ID="txtDefaultDesc" CssClass="text-box" runat="server" style="resize:none;" TextMode="MultiLine" Rows="7" Width="300px"></asp:TextBox>
                        </td>
                        <td style="vertical-align:top"><label>Display Description</label></td>
                        <td>
                            <asp:TextBox name="txtDisplayDesc" ID="txtDisplayDesc" CssClass="text-box" runat="server" style="resize:none;" TextMode="MultiLine" Rows="7" Width="300px"></asp:TextBox>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="clr"></div>
            <table class="table" width="100%">
                <thead>
                    <tr>
                        <th width="2%"></th>
                        <th>Screen Name - Module Name</th>
                        <th>Comments</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="rptrCategorylist" runat="server">
                        <ItemTemplate>
                            <tr>
                                <td width="2%"></td>
                                <td>
                                    <%#Eval("screen_name") %>
                                </td>
                                <td>
                                    <%#Eval("used_for_comments") %>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
            <div class="btn_lst">
                <div class="rgt">

                    <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClick="btnClose_Click"></asp:Button>
                    <asp:Button ID="btnSave" CssClass="button rounded" runat="server" OnClientClick="return saveMasterCategoryDtls();" Text="Save" OnClick="btnSave_Click" />

                </div>
            </div>

        </form>
    </div>


</asp:Content>
