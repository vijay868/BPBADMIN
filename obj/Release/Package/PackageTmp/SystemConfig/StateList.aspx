﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="StateList.aspx.cs" Inherits="Bestprintbuy.Admin.Web.SystemConfig.StateList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <script type="text/javascript">
         function deleteState() {
             if ('<%=HasAccess("StateList", "delete")%>' != 'False') {
                return confirm('Results in deletion of selected record(s).Do you want to continue?');
            }
            else {
                alert('User does not have access.');
                return false;
            }
        }
        function addState() {
            if ('<%=HasAccess("StateList", "add")%>' == 'False') {
                alert('User does not have access.');
                return false;
            }
            return true;
        }
        function viewState() { 
            if ('<%=HasAccess("StateDetails", "view")%>' == 'False') {
                alert('User does not have access.');
                return false;
            }
            return true;
        }
    </script>
     <div class="contant_hldr">
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">System Configuration</li>
            <li>States</li>
        </ul>
        <h2>States</h2>
        <form class="form" runat="server" id="frmStateList">            
            <table class="accounts-table">
                <tbody>
                    <tr>
                        <td width="10%">View States for </td>
                        <td width="10%">
                            <asp:DropDownList name="ddlState" ID="ddlState" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                                <asp:ListItem Value=""></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td width="70%"></td>
                    </tr>
                </tbody>
            </table>           
            <div class="clr"></div>
            <asp:GridView ID="StatesGrid" runat="server" AutoGenerateColumns="false" EnableViewState="true"
                ForeColor="Black" Width="100%" PageSize="20" AllowPaging="true" AllowSorting="true" OnSorting="StatesGrid_Sorting" OnPageIndexChanging="StatesGrid_PageIndexChanging"
                class="table" name="StatesGrid">
                <PagerStyle HorizontalAlign="Right" CssClass="dtPage"></PagerStyle>
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />
                <Columns>
                    <asp:TemplateField HeaderText="#SL" ItemStyle-Width="5%">
                        <HeaderStyle Width="5%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <span><%# GetRecordCountForPage()%></span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="State Abbrevation" ItemStyle-Width="9%" SortExpression="state_abbr">
                        <HeaderStyle Width="9%" ForeColor="White" />
                        <ItemTemplate>
                           <asp:LinkButton ID="btnCtAbbr" runat="server" CommandName="Delete" CausesValidation="true" OnClientClick="return viewState();"
                                                    ToolTip="Delete" CommandArgument='<%#Bind("pkey_state")%>' OnClick="btnStateAbbr_Click"><%#Eval("state_abbr") %></asp:LinkButton>                                            
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="State Name" ItemStyle-Width="10%" SortExpression="state_name">
                        <HeaderStyle Width="10%" ForeColor="White" />
                        <ItemTemplate>
                         <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Delete" CausesValidation="true" OnClientClick="return viewState();"
                                                    ToolTip="Delete" CommandArgument='<%#Bind("pkey_state")%>' OnClick="btnStateName_Click"><%#Eval("state_name") %></asp:LinkButton>  
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Delete" ItemStyle-Width="12%">
                        <HeaderStyle Width="12%" ForeColor="White" />
                        <ItemTemplate>
                            <asp:LinkButton ID="btnDelete" runat="server" CommandName="Delete" CausesValidation="true" 
                                                    ToolTip="Delete" CommandArgument='<%#Bind("pkey_state")%>' OnClientClick="return deleteState();"
                                                    OnClick="btnDelete_Click"><img src="../images/trashcan.png" /></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <asp:Label ID="lblTotalRecords" runat="server" Font-Bold="true"></asp:Label>
            <div class="btn_lst" style="padding-top:20px">
                <div class="rgt">
                    <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClick="btnClose_Click"></asp:Button>
                </div>
                 <div class="lft">
                    <asp:Button runat="server" ID="btnAddState" CssClass="button rounded" Text="Add State" OnClick="btnAddState_Click"></asp:Button>
                </div>
            </div>
        </form>
    </div>
</asp:Content>
