﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="AttributeList.aspx.cs" Inherits="Bestprintbuy.Admin.Web.SystemConfig.AttributeList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function deleteAttribute() {
            if ('<%=HasAccess("AttributeList", "delete")%>' != 'False') {
                return confirm('All attached attributes will be deleted if the products are not defined, 	Are you sure?');
            }
            else {
                alert('User does not have access.');
                return false;
            }
        }
        function addAttribute() {
            if ('<%=HasAccess("AttributeList", "add")%>' == 'False') {
                alert('User does not have access.');
                return false;
            }
            return true;
        }
    </script>
    <div class="clr"></div>
    <!-- Container Starts -->
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
           <li class="liColor">System Configuration</li>
            <li>Attributes</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Attributes</h2>
        <form id="frmAttributesList" name="frmAttributesList" runat="server" class="form">
            <table cellpadding="0" cellspacing="0" border="0" width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="7%">Variants </td>
                        <td width="15%">
                            <asp:DropDownList name="ddlvariants" ID="ddlvariants" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlvariants_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td width="70%"></td>
                    </tr>
                </tbody>
            </table>           
            <div class="clr"></div>
            <asp:GridView ID="AttributeslistGrid" runat="server" AutoGenerateColumns="false" EnableViewState="true"
                ForeColor="Black" Width="100%" PageSize="20" AllowPaging="true" AllowSorting="true"
                class="table" name="AttributeslistGridGrid" OnPageIndexChanging="AttributeslistGrid_PageIndexChanging" OnSorting="AttributeslistGrid_Sorting">
                <PagerStyle HorizontalAlign="Right" CssClass="dtPage"></PagerStyle>
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />
                <Columns>
                    <asp:TemplateField HeaderText="#SL">
                        <ItemTemplate>
                            <span><%# GetRecordCountForPage()%></span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Attribute Code" SortExpression="attribute_code">
                        <HeaderStyle Width="25%" ForeColor="White" />
                        <ItemTemplate>
                            <a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("AttributeDetail", "view")%>','AttributeDetails.aspx?attributeKey=<%# DataBinder.Eval(Container,"DataItem.pkey_attribute") %>&variantKey=<%#DataBinder.Eval(Container,"DataItem.fkey_variant") %>')" title="Edit"><%# DataBinder.Eval(Container,"DataItem.attribute_code") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Attribute Name" SortExpression="attribute_value">
                        <HeaderStyle Width="25%" ForeColor="White" />
                        <ItemTemplate>
                            <a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("AttributeDetail", "view")%>','AttributeDetails.aspx?attributeKey=<%# DataBinder.Eval(Container,"DataItem.pkey_attribute") %>&variantKey=<%#DataBinder.Eval(Container,"DataItem.fkey_variant") %>')" title="Edit"><%# DataBinder.Eval(Container,"DataItem.attribute_value") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Comments">
                        <HeaderStyle Width="25%" />
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container,"DataItem.comments") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Delete">
                        <HeaderStyle Width="15%" />
                        <ItemTemplate>
                            <asp:LinkButton ID="btnDelete" runat="server" CommandName="Delete" CausesValidation="true"
                                ToolTip="Delete" CommandArgument='<%#Bind("pkey_attribute")%>' OnClientClick="return deleteAttribute();"
                                OnClick="btnDelete_Click"><img src="../images/trashcan.png" /></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
             <div class="ttl"><b>Total Number of Attributes: </b><%=recordCount%></div>
            <!-- table Ends -->
            <div class="btn_lst">
                <div class="lft">
                    <asp:Button ID="btnAdd" Text="Add" CssClass="button rounded" runat="server" OnClick="btnAdd_Click" OnClientClick="return addAttribute();"></asp:Button>
                </div>
                <div class="rgt">
                    <asp:Button ID="btnClose" Text="Close" CssClass="button rounded" runat="server" OnClick="btnClose_Click"></asp:Button>
                </div>
            </div>
        </form>
    </div>
</asp:Content>
