﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="RealtorDetails.aspx.cs" Inherits="Bestprintbuy.Admin.Web.SystemConfig.RealtorDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="javascript">
        function CheckExclusiveCustomer() {
            if (eval(document.getElementById("ContentPlaceHolder1_chkExclusiveCustomer"))) {
                var parentKey = document.getElementById("ContentPlaceHolder1_ddlCustomer").options[document.getElementById("ContentPlaceHolder1_ddlCustomer").selectedIndex].value;
                //alert(parentKey);
                if (document.getElementById("ContentPlaceHolder1_chkExclusiveCustomer").checked == true && parentKey == -1) {
                    alert("Exclusive Customer is required. Please select a customer.");
                    document.getElementById("ContentPlaceHolder1_ddlCustomer").focus();
                    return false;
                }
                if (document.getElementById("ContentPlaceHolder1_chkExclusiveCustomer").checked == false && parentKey > 0) {
                    alert("Customer is selected. Please select the Is Exclusive Customer Check box");
                    return false;
                }
            }
            return true;
        }

        $().ready(function () {
            $("#frmRealtorDetail").validate();
        });

        function ChechCondition() {
            if (document.frmRealtorDetail.chkIsExclusiveCustomer.checked == false)
                document.frmRealtorDetail.cboExclusiveCustomer.options[0].selected = true;
        }
		</script>
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
             <li class="liColor">System Configuration</li>
            <li><a href="RealtorsList.aspx">Realtors List</a></li>
            <li>Realtor Details</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Realtor Details</h2>
        <form id="frmRealtorDetail" name="frmRealtorDetail" runat="server">
            <table cellpadding="0" cellspacing="0" border="0" width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="15%">
                            <label>Realtor ID*</label></td>
                        <td width="20%">
                            <asp:TextBox CssClass="text-box" name="txtRealtorID" ID="txtRealtorID" runat="server" MaxLength="10" required></asp:TextBox>
                        </td>
                        <td width="15%">
                            <label>Realtor Name* </label>
                        </td>
                        <td width="35%">
                            <asp:TextBox CssClass="text-box" name="txtRealtorName" ID="txtRealtorName" runat="server" required></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>IsActive</label></td>
                        <td>
                            <asp:CheckBox ID="chkActive" runat="server" CssClass="check-box" />
                        </td>
                        <td>
                            <label>IsExclusive Customer</label>
                        </td>
                        <td>
                            <asp:CheckBox ID="chkExclusiveCustomer" runat="server" CssClass="check-box" />
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">
                            <label>Exclusive Customer</label></td>
                        <td>
                            <asp:DropDownList name="ddlCustomer" ID="ddlCustomer" CssClass="slect-box" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td colspan="2" class="rgt" align="right">
                            <asp:Button runat="server" ID="btnSave" CssClass="button rounded" Text="Save" OnClientClick="if(!CheckExclusiveCustomer()) return false;" OnClick="btnSave_Click"></asp:Button>
                            <asp:Button runat="server" CssClass="button rounded close-btn" ID="btnClose" Text="Close" UseSubmitBehavior="false" OnClick="btnClose_Click"></asp:Button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
</asp:Content>
