﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="StatementofAccount.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Account.StatementofAccount" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <script type="text/javascript">
         function SupplierList() {
             url = "../BusinessEntites/SelectBusinessEntity.aspx?bid=sup";
             $.fancybox({
                 'width': '70%',
                 'height': '40%',
                 'autoScale': true,
                 'transitionIn': 'fade',
                 'transitionOut': 'fade',
                 'href': url,
                 'type': 'iframe'
             });
             return false;
         }
         function SetSupplierData(parentID, SupplierName) {
             $("#ContentPlaceHolder1_hdnSupplierkey").val(parentID);
             __doPostBack("ctl00$ContentPlaceHolder1$lbtnSupplier", "");
             $("#divSupplierList").dialog("close");
         }

         function SupplierView() {
             if ($("#ContentPlaceHolder1_lblSupplier").text() == "") {
                 alert("Please select the supplier.");
                 return false;
             }
             var RegisterKey = $("#ContentPlaceHolder1_hdnSupplierkey").val() != "" ? $("#ContentPlaceHolder1_hdnSupplierkey").val() : "-1";
             var url = "../BusinessEntites/BusinessRegisterPopUp.aspx?bid=bur&pkey=" + RegisterKey;
             
             $.fancybox({
                 'width': '70%',
                 'height': '40%',
                 'autoScale': true,
                 'transitionIn': 'fade',
                 'transitionOut': 'fade',
                 'href': url,
                 'type': 'iframe'
             });
             return false;
         }

         function NavigateToAdvances() {
             if ($("#ContentPlaceHolder1_lblSupplier").text() == "") {
                 alert("Please select the supplier.");
                 return false;
             }
             var recExist = '<%=rowexists%>';
            if (recExist == 'True') {
                //alert("In progress...");
                document.location.href = 'AdjustAdvancePayments.aspx?fkeyregister=<%=pkey_register%>';
                return false;
            } else {
                alert("There are no transactions to adjust. Not a valid operation.");
                return false;
            }
             
        }
    </script>
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Accounts</li>            
            <li>Statement of Account</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Statement of Account</h2>
        <form id="frmStatementofAccount" name="frmStatementofAccount" runat="server">
            <table width="100%" class="accounts-table">
                <tbody>
                    <tr>
                         <td width="5%"></td>
                        <td width="6%">
                            <asp:Button runat="server" CssClass="button rounded" ID="btnSupplier" Text="Supplier" OnClientClick="return SupplierView();" UseSubmitBehavior="false"></asp:Button>
                            <asp:LinkButton ID="lbtnSupplier" runat="server" OnClick="lbtnSupplier_Click" />
                        </td>
                        <td width="5%">
                            <asp:Button runat="server" CssClass="button rounded" ID="btnSupplierDetail" Text="..." OnClientClick="return SupplierList();" UseSubmitBehavior="false"></asp:Button>
                        </td>
                        <td width="10%">
                            <asp:Label runat="server" ID="lblSupplier" />
                        </td>
                        <td width="60%"></td>
                    </tr>
                </tbody>
            </table>
            <div class="clr"></div>
            <asp:GridView ID="StatementofAccountGrid" runat="server" AutoGenerateColumns="false" EnableViewCountry="true"
                ForeColor="Black" Width="100%" PageSize="20" AllowPaging="true" OnPageIndexChanging="StatementofAccountGrid_PageIndexChanging"
                class="table" name="StatementofAccountGrid" ShowHeaderWhenEmpty="true">
                <PagerStyle HorizontalAlign="Right" CssClass="dtPage"></PagerStyle>
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />
                <Columns>
                    <asp:TemplateField HeaderText="#SL">
                        <HeaderStyle ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <span><%# GetRecordCountForPage()%></span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Date">
                        <HeaderStyle Width="15%" ForeColor="White" />
                        <ItemTemplate>
                            <%#Eval("transdate") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Transaction Details">
                        <HeaderStyle Width="40%" ForeColor="White" />
                        <ItemTemplate>
                            <%#Eval("tran_detail") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Debit">
                        <HeaderStyle Width="13%" ForeColor="White" />
                        <ItemTemplate>
                            <%#Eval("debit") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Credit">
                        <HeaderStyle Width="13%" ForeColor="White" />
                        <ItemTemplate>
                            <%#Eval("credit") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Balance">
                        <HeaderStyle Width="13%" ForeColor="White" />
                        <ItemTemplate>
                            <%# GetBalance(Eval("credit"), Eval("debit"))%>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <table width="100%">
                <tr>
                    <td>
                        <b>Total Number of Transactions</b> :
                        <asp:Label ID="lblCount" runat="server" />
                    </td>
                    <td style="padding-left: 50px"><b>Total Debit ($)</b>:
                                    <%=totalDebit%>
                    </td>
                    <td style="padding-left: 50px"><b>Total Credit ($)</b>:
                                    <%=totalCredit%>
                    </td>
                    <td style="padding-left: 50px"><b>Total Balance ($)</b> :
                                    <%=balance%>
                    </td>
                </tr>
            </table>
             <div class="btn_lst">
                <asp:Button ID="btnAdd" Text="Adjust Advance" CssClass="button rounded" runat="server" OnClientClick="return NavigateToAdvances();"></asp:Button>
                 <asp:HiddenField ID="hdnSupplierkey" runat="server" />
                <div class="rgt">
                    <asp:Button ID="btnClose" Text="Close" CssClass="button rounded" runat="server" OnClick="btnClose_Click" UseSubmitBehavior="false"></asp:Button>
                </div>
            </div>
        </form>
    </div>
</asp:Content>
