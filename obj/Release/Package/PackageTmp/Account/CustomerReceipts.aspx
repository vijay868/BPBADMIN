﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="CustomerReceipts.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Account.CustomerReceipts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">

        $().ready(function () {
            $("#frmReceipts").validate();
        });
        function validate_twoDates() {
            var dateStart = $("#ContentPlaceHolder1_txtFromDate").val();
            var dateEnd = $("#ContentPlaceHolder1_txtToDate").val();
            if (Date.parse(dateStart) > Date.parse(dateEnd)) {
                alert("Invalid date range!");
                return false;
            }
        }
        $(function () {
            $("#ContentPlaceHolder1_txtFromDate").datepicker();
            $("#ContentPlaceHolder1_txtToDate").datepicker();
        });
        function CustomerReceiptView(receiptKey) {
            url = "CustomerReceiptView.aspx?receiptKey=" + receiptKey;
            $.fancybox({
                'width': '40%',
                'height': '80%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': url,
                'type': 'iframe'
            });
            return false;
        }
    </script>
    <div class="contant_hldr">
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Accounts</a></li>
            <li>Receipts</li>
        </ul>
        <h2>Receipts</h2>
        <form class="form" runat="server" id="frmReceipts">

            <table class="accounts-table" width="100%">
                <tbody>
                    <tr>
                        <td width="5%">From Date </td>
                        <td>
                            <asp:TextBox CssClass="text-box date" Height="18px" Width="200px" name="txtFromDate" ID="txtFromDate" runat="server" required ></asp:TextBox>

                        </td>
                        <td width="5%">To Date </td>
                        <td>
                            <asp:TextBox CssClass="text-box date" Height="18px" Width="200px" name="txtToDate" ID="txtToDate" runat="server" required ></asp:TextBox></td>

                        <td width="10%">Filter List For</td>
                        <td width="10%">
                            <asp:DropDownList name="ddlFilter" ID="ddlFilter" runat="server">
                                <asp:ListItem Value="-1">All Receipts</asp:ListItem>
                                <asp:ListItem Value="1">Only Invoiced</asp:ListItem>
                                <asp:ListItem Value="2">Only not Invoiced</asp:ListItem>
                                <asp:ListItem Value="3">Entered Manually</asp:ListItem>
                                <asp:ListItem Value="4">Automatic via Orders</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td width="5%">
                            <asp:Button runat="server" ID="btnGo" CssClass="button rounded" Text="Go" OnClientClick="return validate_twoDates();" OnClick="btnGo_Click"></asp:Button>
                        </td>
                        <td style="padding-right: 250px">
                            <asp:Button runat="server" ID="btnReset" CssClass="button rounded" Text="Reset" OnClick="btnReset_Click" Visible="false"></asp:Button>
                        </td>
                        <td></td>
                    </tr>
                </tbody>
            </table>            
            <div class="clr"></div>

            <asp:GridView ID="CustomereceiptGrid" runat="server" AutoGenerateColumns="false" EnableViewState="true"
                ForeColor="Black" Width="100%" PageSize="20" AllowPaging="true" AllowSorting="true"
                class="table" name="CustomereceiptGrid"  OnPageIndexChanging="CustomereceiptGrid_PageIndexChanging" OnSorting="CustomereceiptGrid_Sorting">
                <PagerStyle HorizontalAlign="Right" CssClass="dtPage"></PagerStyle>
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />
                <Columns>
                    <asp:TemplateField HeaderText="SL# " ItemStyle-Width="10%">
                        <HeaderStyle Width="5%" ForeColor="White" />
                        <ItemTemplate>
                        <span><%# GetRecordCountForPage()%></span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Receipt # " ItemStyle-Width="14%" SortExpression="receipt_number">
                        <HeaderStyle Width="14%" ForeColor="White" />
                        <ItemTemplate>
                            <a href="#" onclick='return CustomerReceiptView("<%#Eval("pkey_cust_manual_receipt") %>");'><%#Eval("receipt_number") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Receipt Date" ItemStyle-Width="14%" SortExpression="receipt_date">
                        <HeaderStyle Width="14%" ForeColor="White" />
                        <ItemTemplate>
                            <a href="#" onclick='return CustomerReceiptView("<%#Eval("pkey_cust_manual_receipt") %>");'><%#Eval("receipt_date") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Customer ID" ItemStyle-Width="14%" SortExpression="customer_id">
                        <HeaderStyle Width="14%" ForeColor="White" />
                        <ItemTemplate>
                            <asp:Label ID="lblCustID" runat="server" Text='<%#Eval("customer_id") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Customer Name" ItemStyle-Width="14%" SortExpression="customer_name">
                        <HeaderStyle Width="14%" ForeColor="White" />
                        <ItemTemplate>
                            <asp:Label ID="lblCustName" runat="server" Text=' <%#Eval("customer_name") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Receipt Amount" ItemStyle-Width="14%" ItemStyle-HorizontalAlign="Justify" SortExpression="receipt_amount">
                        <HeaderStyle Width="14%" ForeColor="White" HorizontalAlign="Center" />
                        <ItemTemplate>
                            <a href="#" onclick='return CustomerReceiptView("<%#Eval("pkey_cust_manual_receipt") %>");'><%#Eval("receipt_amount") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Mode of Payment" ItemStyle-Width="10%" SortExpression="payment_mode">
                        <HeaderStyle Width="10%" ForeColor="White" />
                        <ItemTemplate>
                            <asp:Label ID="lblpaymode" runat="server" Text='<%#Eval("payment_mode") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <asp:Label ID="lblTotalRecords" runat="server" Font-Bold="true"></asp:Label>
            <div class="btn_lst">
                <div class="lft">
                    <asp:Button runat="server" ID="btnAdd" CssClass="button rounded" Text="Add" UseSubmitBehavior="false" OnClick="btnAdd_Click"></asp:Button>
                </div>
                <div class="rgt">
                    <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" UseSubmitBehavior="false"  OnClick="btnClose_Click"></asp:Button>
                </div>
            </div>
        </form>
         <div id="divCustomerView" title="Customer List" style="display: none;">
        <iframe id="CustomerView" style="width: 500px; height: 400px;"></iframe>
    </div>
    </div>
</asp:Content>
