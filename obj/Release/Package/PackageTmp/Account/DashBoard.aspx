﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="DashBoard.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Account.DashBoard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="hom_hldr">
        <div class="list_hldr">
            <span class="customer"></span>
            <h2>Customer Service</h2>
            <ul>
                <li id="liOrderFulfillment" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("OrderFulfillment", "list")%>','../Customers/OrderFulfillment.aspx?timeStamp=<%= m_timeStamp%>');">Order Fulfillment</a></li>
                <li id="liAdminCustLogin" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("AdminCustLogin", "list")%>','../Customers/AdminCustLogin.aspx?timeStamp=<%= m_timeStamp%>');">User Information Access</a></li>
                <li id="liCustomersList" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("CustomersList", "list")%>','../Customers/CustomersList.aspx?timeStamp=<%= m_timeStamp%>');">Customer Accounts</a></li>
                <li id="liPromotionsList" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("PromoList", "list")%>','../Customers/PromoList.aspx?timeStamp=<%= m_timeStamp%>');">Promotions</a></li>
                <li id="liCustomerCredits" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("CustomerCredits", "list")%>','../Customers/CustomerCreditList.aspx?timeStamp=<%= m_timeStamp%>');">Customer Credits</a></li>
                <li id="liMCACustomersList" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("MCACustomerList", "list")%>','../Customers/MCACustomerList.aspx?timeStamp=<%= m_timeStamp%>');">MCA Accounts</a></li>
                <li id="liTemplateSearch" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("TemplateSearch", "list")%>','../Customers/TemplateSearch.aspx?timeStamp=<%= m_timeStamp%>');">Template Search</a></li>
                <li id="liUserStatistics" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("UserStatistics", "list")%>','../Account/RegisteredUsersList.aspx?timeStamp=<%= m_timeStamp%>');">User Statistics</a></li>
                <li id="liCatalogRequest" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("CatalogRequest", "list")%>','../Customers/CatalogReqList.aspx?timeStamp=<%= m_timeStamp%>');">Catalog Request</a></li>

            </ul>
        </div>
        <div class="list_hldr">
            <span class="product"></span>
            <h2>Product Setup &amp; Configuration</h2>
            <ul>
                <li id="liHomeMessages" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("HomeScrollMessages", "list")%>','../Products/HomeScrollMessages.aspx?timeStamp=<%= m_timeStamp%>');">Home Page Scrolling</a></li>
                <li id="liXSellProducts" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("XSellProducts", "edit")%>','../Products/XSellProducts.aspx?timeStamp=<%= m_timeStamp%>');">Cross Sell Product Setup</a></li>
                <li id="liXSellDiscountList" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("XSellDiscounts", "edit")%>','../Products/XSellDiscountList.aspx?timeStamp=<%= m_timeStamp%>');">Cross Sell Discount List</a></li>
                <li id="liShippingandTurnaround" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("ShippingandTurnaround", "list")%>','../Products/SetShipOptions.aspx?timeStamp=<%= m_timeStamp%>');">Shipping and Turnaround</a></li>
                <li id="liagentPackageList" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("ProductDetail", "list")%>','../Products/NewAgentPackageList.aspx?timeStamp=<%= m_timeStamp%>');">New Agent Package List</a></li>
                <li id="liManifest" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("ProductDetail", "list")%>','../Products/SupplierManifestSettings.aspx?timeStamp=<%= m_timeStamp%>');">Manifest - Product Code Translation</a></li>
            </ul>
        </div>
        <div class="list_hldr">
            <span class="accounts"></span>
            <h2>Accounts</h2>
            <ul>
                <li id="liInvoices" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("CustomerInvoiceList", "list")%>','../Account/CustomerInvoiceList.aspx?timeStamp=<%= m_timeStamp%>');">Invoices</a></li>
                <li id="liPayments" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("PaymentList", "list")%>','../Account/PaymentList.aspx?timeStamp=<%= m_timeStamp%>');">Payments</a></li>
                <li id="liReceipts" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("CustomerReceipts", "list")%>','../Account/CustomerReceipts.aspx?timeStamp=<%= m_timeStamp%>');">Receipts</a></li>  
                <li id="liCustomerReciepts" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("CustomerReceipts", "list")%>','../Account/CustomerPayments.aspx?timeStamp=<%= m_timeStamp%>');">Customer Reciepts</a></li>
                <li id="liStatementofAccount" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("StatementofAccount", "list")%>','../Account/StatementofAccount.aspx?timeStamp=<%= m_timeStamp%>');">Statement of Account</a></li>
            </ul>
        </div>
        <div class="list_hldr">
            <span class="admini"></span>
            <h2>Administrative Reports</h2>
            <ul>
                <li id="liMCAReport" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("GroupList", "list")%>','../AdminReports/MCAReport.aspx?timeStamp=<%= m_timeStamp%>');">MCA Report</a></li>
                <li id="liCatalogReport" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("GroupList", "list")%>','../AdminReports/CatalogReport.aspx?timeStamp=<%= m_timeStamp%>');">Catalog Report</a></li>
                <li id="liCatalogReportStatus" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("GroupList", "list")%>','../AdminReports/CatalogReportStatus.aspx?timeStamp=<%= m_timeStamp%>');">Catalog Report Status</a></li>
                <li id="liSprocketOrders" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("GroupList", "list")%>','../AdminReports/PendingSprocketOrders.aspx?timeStamp=<%= m_timeStamp%>');">Sproket Orders</a></li>
                <li id="liTransactionReport" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("GroupList", "list")%>','../AdminReports/TransactionReport.aspx?timeStamp=<%= m_timeStamp%>');">Transaction Report</a></li>
            </ul>
        </div>
        <div class="list_hldr">
            <span class="marketing"></span>
            <h2>Marketing Reports</h2>
            <ul>
                <li id="liCustomersWithCart" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("GroupList", "list")%>','../AdminReports/CustomersWithCart.aspx?timeStamp=<%= m_timeStamp%>');">Customers With Cart</a></li>
                <li id="liCustomersWithSavedDesign" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("GroupList", "list")%>','../AdminReports/CustomersWithSaveDesign.aspx?timeStamp=<%= m_timeStamp%>');">Customers With Saved Design</a></li>
                <li id="liEmailMarketingProgram" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("GroupList", "list")%>','../AdminReports/EMCustomerStats.aspx?timeStamp=<%= m_timeStamp%>');">Email Marketing Programs</a></li>
                <li id="liExcludeEmail" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("GroupList", "list")%>','../AdminReports/ExcludeEmailFromReports.aspx?timeStamp=<%= m_timeStamp%>');">Excluded Emails</a></li>
            </ul>
        </div>
        <div class="list_hldr">
            <span class="business"></span>
            <h2>Business Partners Setup</h2>
            <ul>
                <li id="liSupplierVenders" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("BusinessRegisterList", "list")%>','../BusinessEntites/BusinessRegisterList.aspx?bid=sup&timeStamp=<%= m_timeStamp%>');">Supplier / Vendors</a></li>
                <li id="liFreightShipping" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("BusinessRegisterList", "list")%>','../BusinessEntites/BusinessRegisterList.aspx?bid=ship&timeStamp=<%= m_timeStamp%>');">Freight / Shipping Companies</a></li>
                <li id="liSupplierPayment" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("ASupplierPayment", "list")%>','../BusinessEntites/ASupplierPayment.aspx?timeStamp=<%= m_timeStamp%>');">Supplier Payment Information</a></li>
                <li id="liSupplierModification" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("SupplierModRemList", "list")%>','../BusinessEntites/SupplierModRemList.aspx?bid=sup&timeStamp=<%= m_timeStamp%>');">Supplier Modification Reminders</a></li>
                <li id="liCobrandedBusinessPartner" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("BusinessRegisterList", "list")%>','../BusinessEntites/BusinessRegisterList.aspx?bid=pat&timeStamp=<%= m_timeStamp%>');">Co Branded Business Partners</a></li>
                <li id="liB2BAffiliates" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("BusinessRegisterList", "list")%>','../BusinessEntites/BusinessRegisterList.aspx?bid=afl&timeStamp=<%= m_timeStamp%>');">B2B Affiliates</a></li>
                <li id="liBusinessRegEntry" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("BusinessRegisterList", "list")%>','../BusinessEntites/BusinessRegisterList.aspx?bid=bur&timeStamp=<%= m_timeStamp%>');">Business Register Entity</a></li>
            </ul>
        </div>
        <div class="list_hldr">
            <span class="security"></span>
            <h2>Security</h2>
            <ul>
                <li id="liGroupList" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("GroupList", "list")%>','../Account/GroupList.aspx?timeStamp=<%= m_timeStamp%>');">Group Roles</a></li>
                <li id="liUserList" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("UserList", "list")%>','../Account/UserList.aspx?timeStamp=<%= m_timeStamp%>');">Users</a></li>
                <li id="liCngPwd" runat="server"><a  href="../Account/ChangePassword.aspx">Change Password</a></li>
            </ul>
        </div>
        <div class="list_hldr">
            <span class="system_config"></span>
            <h2>System Configuration</h2>
            <ul>
                <li id="liVariantsList" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("VariantList", "list")%>','../SystemConfig/VariantList.aspx?timeStamp=<%= m_timeStamp%>');">Variants</a></li>
                <li id="liAttributesList" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("AttributeList", "list")%>','../SystemConfig/AttributeList.aspx?timeStamp=<%= m_timeStamp%>');">Attributes</a></li>
                <li id="liProductsList" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("ProductList", "list")%>','../SystemConfig/ProductList.aspx?timeStamp=<%= m_timeStamp%>');">Products</a></li>
                <li id="liFreeProducts" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("FreeProductsList", "list")%>','../SystemConfig/FreeProductsList.aspx?timeStamp=<%= m_timeStamp%>');">Free Products</a></li>
                <li id="liFreePromotionsList" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("FreeProductsList", "list")%>','../SystemConfig/FreePromoList.aspx?timeStamp=<%= m_timeStamp%>');">Free Promotions</a></li>
                <li id="litemplateList" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("TemplateList", "list")%>','../SystemConfig/TemplateList.aspx?timeStamp=<%= m_timeStamp%>');">Templates</a></li>
                <li id="lirevtemplateList" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("RevTemplateList", "list")%>','../SystemConfig/RevTemplateList.aspx?timeStamp=<%= m_timeStamp%>');">Reverse Templates</a></li>
                <li id="liRealtors" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("RealtorsList", "list")%>','../SystemConfig/RealtorsList.aspx?timeStamp=<%= m_timeStamp%>');">Realtors</a></li>
            </ul>
        </div>
        <div class="list_hldr none">
            <h2>&nbsp;</h2>
            <ul>
                <li id="liMessages" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("CustMessagesList", "list")%>','../SystemConfig/CustMessagesList.aspx?timeStamp=<%= m_timeStamp%>');">Messages</a></li>
                <li id="likiosklist" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("AttributeList", "list")%>','../SystemConfig/ECInfoList.aspx?timeStamp=<%= m_timeStamp%>');">Kiosks</a></li>
                <li id="likioskCompanylist" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("KiosksCompanyList", "edit")%>','../SystemConfig/KiosksCompanyList.aspx?timeStamp=<%= m_timeStamp%>');">Kiosk Company List</a></li>
                <li id="liMasterCategoriesList" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("MasterCategoryList", "list")%>','../SystemConfig/MasterCategoryList.aspx?timeStamp=<%= m_timeStamp%>');">Master Categories</a></li>
                <li id="liCategories" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("CategoryList", "list")%>','../SystemConfig/CategoryList.aspx?timeStamp=<%= m_timeStamp%>');">Categories</a></li>
                <li id="litypesList" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("TypeList", "list")%>','../SystemConfig/TypeList.aspx?timeStamp=<%= m_timeStamp%>');">Types</a></li>
                <li id="liCountries" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("CountryList", "list")%>','../SystemConfig/CountryList.aspx?timeStamp=<%= m_timeStamp%>');">Countries</a></li>
                <li id="liStates" runat="server"><a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("StateList", "list")%>','../SystemConfig/StateList.aspx?timeStamp=<%= m_timeStamp%>');">States</a></li>
            </ul>
        </div>
        <div class="list_hldr">
            <span class="reserved"></span>
            <h2>Reserved</h2>
        </div>
    </div>
</asp:Content>
