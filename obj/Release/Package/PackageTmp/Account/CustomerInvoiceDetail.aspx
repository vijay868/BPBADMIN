﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="CustomerInvoiceDetail.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Account.CustomerInvoiceDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        #divCustomerPreview {
            overflow: hidden;
        }
    </style>
    <script type="text/javascript">
        $().ready(function () {
            $("#frmNewAgentPackageDetail").validate();
        });

        $(function () {
            $("#ContentPlaceHolder1_txtInvoiceDate").datepicker();
            $("#ContentPlaceHolder1_txtOrderDate").datepicker();
            $("#ContentPlaceHolder1_txtDCDate").datepicker();
        });

        function CustomerList() {
            $.fancybox({
                'width': '80%',
                'height': '80%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': 'CustomerList.aspx',
                'type': 'iframe'
            });
            return false;
        }

        function CustomerPreview() {
            if ($("#ContentPlaceHolder1_lblCustEmail").text() == "") {
                alert("Please select the Customer.");
                return false;
            }
            $.fancybox({
                'width': '100%',
                'height': '100%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': '#divCustomerPreview',
                'type': 'inline'
            });
            return false;
        }

        function SupplierList() {
            $.fancybox({
                'width': '80%',
                'height': '80%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': '../BusinessEntites/SelectBusinessEntity.aspx?bid=bur',
                'type': 'iframe'
            });
            return false;
        }

        function CustomerPOList() {
            if ($("#ContentPlaceHolder1_lblCustEmail").text() == "") {
                alert("Customer is required, Please select a value and try again.");
                return false;
            }
            var listKeys = $("#ContentPlaceHolder1_hdnpkey_customer").val();
            url = "SelectCustomerPO.aspx?ListKeys=" + listKeys;
            $.fancybox({
                'width': '80%',
                'height': '80%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': url,
                'type': 'iframe'
            });
            return false;
        }

        function ProductList() {
            $.fancybox({
                'width': '80%',
                'height': '80%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': 'SelectProducts.aspx',
                'type': 'iframe'
            });
            return false;
        }

        function validateSupplier() {
            if ($("#ContentPlaceHolder1_lblCustEmail").text() == "") {
                alert("Customer is required, Please select a value and try again.");
                return false;
            }
            if ('<%=strRec%>' == "0") {
                alert("Item is required to save the data. Please select the Item.");
                return false;
            }
            return true;
        }

        function SetCustomerData(pkey_customer, customer_id, cus_name, address1, address2, city, state, postalcode) {
            $("#ContentPlaceHolder1_hdnpkey_customer").val(pkey_customer);
            $("#ContentPlaceHolder1_hdncustomer_id").val(customer_id);
            $("#ContentPlaceHolder1_hdncus_name").val(cus_name);
            $("#ContentPlaceHolder1_hdnaddress1").val(address1);
            $("#ContentPlaceHolder1_hdnaddress2").val(address2);
            $("#ContentPlaceHolder1_hdncity").val(city);
            $("#ContentPlaceHolder1_hdnstate").val(state);
            $("#ContentPlaceHolder1_hdnpostalcode").val(postalcode);

            __doPostBack("ctl00$ContentPlaceHolder1$lbtnUpdate", "");
        }

        //function SetSupplierData(parentID, SupplierName) {
        //    $("#ContentPlaceHolder1_hdnSupplierkey").val(parentID);
        //    __doPostBack("ctl00$ContentPlaceHolder1$lbtnSupplier", "");
        //}

        function SetCustomerPOData(pkey_cust_po_master, po_number, po_date) {
            $("#ContentPlaceHolder1_hdnpkey_cust_po_master").val(pkey_cust_po_master);
            $("#ContentPlaceHolder1_txtOrderNumber").val(po_number);
            $("#ContentPlaceHolder1_txtOrderDate").val(po_date);
        }

        function SetProductData(pkey_product, product_name) {
            $("#ContentPlaceHolder1_hdnpkey_product").val(pkey_product);
            $("#ContentPlaceHolder1_hdnproduct_name").val(product_name);
            __doPostBack("ctl00$ContentPlaceHolder1$lbtnUpdate1", "");
        }
    </script>

    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Accounts</li>
            <li><a href="CustomerInvoiceList.aspx">Invoices</a></li>
            <li>Customer Invoice Details</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Customer Invoice Details</h2>
        <form id="frmNewAgentPackageDetail" name="frmNewAgentPackageDetail" runat="server">
            <table width="100%" class="accounts-table">
                <thead>
                    <tr>
                        <th colspan="4" width="100%">Invoice Details</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="15%" rowspan="4" style="vertical-align: top; text-align: right;">
                            <asp:Button ID="btnCustomer" Text="Customer" runat="server" UseSubmitBehavior="false" OnClientClick="return CustomerPreview();" CssClass="button rounded" />
                            <asp:Button ID="btnCustomerDetail" Text="..." runat="server" UseSubmitBehavior="false" OnClientClick="return CustomerList();" CssClass="button rounded" />
                            <asp:LinkButton ID="lbtnUpdate" runat="server" OnClick="lbtnUpdate_Click" />
                            <asp:LinkButton ID="lbtnUpdate1" runat="server" OnClick="lbtnUpdate1_Click" />
                        </td>
                        <td width="20%" rowspan="4">
                            <asp:Label ID="lblCustEmail" runat="server" /><br />
                            <asp:Label ID="lblName" runat="server" /><br />
                            <asp:Label ID="lblAdd1" runat="server" /><br />
                            <asp:Label ID="lblAdd2" runat="server" /><br />
                            <asp:Label ID="lblSate" runat="server" />
                        </td>
                        <td width="15%">
                            <label>Invoice Number*</label></td>
                        <td width="35%">
                            <asp:TextBox CssClass="text-box" name="txtInvoiceNumber" ID="txtInvoiceNumber" runat="server" required></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Invoice Date*</label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box date" name="txtInvoiceDate" ID="txtInvoiceDate" runat="server" required></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Order Number</label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtOrderNumber" ID="txtOrderNumber" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Order Date</label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box date" name="txtOrderDate" ID="txtOrderDate" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" width="100%"></td>
                    </tr>
                    <tr>
                        <td width="15%">
                            <label>D.C. Number*</label></td>
                        <td width="20%">
                            <asp:TextBox CssClass="text-box" name="txtDCNumber" ID="txtDCNumber" runat="server" required></asp:TextBox>
                        </td>
                        <td width="15%" rowspan="3" style="vertical-align: top; text-align: right;">
                            <asp:Button ID="btnFrieght" Text="Frieght" runat="server" UseSubmitBehavior="false" CssClass="button rounded" />
                            <asp:Button ID="btnFrieghtList" Text="..." runat="server" UseSubmitBehavior="false" CssClass="button rounded" OnClientClick="return SupplierList();" />
                        </td>
                        <td width="35%" rowspan="3">
                            <asp:Label ID="Label2" runat="server" /><br />
                            <br />
                            <asp:Label ID="Label3" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>D.C. Date*</label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box date" name="txtDCDate" ID="txtDCDate" runat="server" required></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Package Description</label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtTrackingNo" ID="txtTrackingNo" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" width="100%"></td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                        </td>
                        <td>&nbsp;
                        </td>
                        <td>&nbsp;
                        </td>
                        <td style="vertical-align: top;" align="right">
                            <asp:Button ID="btnPopulatePO" Text="Populate from PO" runat="server" UseSubmitBehavior="false" CssClass="button rounded" OnClientClick="return CustomerPOList();" />
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="clr"></div>
            <div class="form">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Item Code</th>
                            <th>Item Description</th>
                            <th>Qty</th>
                            <th>Price/Unit</th>
                            <th>Amount</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rptrProductList" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <%#Eval("product_id") %>
                                        <asp:HiddenField ID="hdnProductid" runat="server" Value='<%#Eval("product_id") %>' />
                                    </td>
                                    <td>
                                        <%#Eval("product_details") %>
                                        <asp:HiddenField ID="hdnProductname" runat="server" Value='<%#Eval("product_name") %>' />
                                    </td>
                                    <td>
                                        <%#Eval("quantity") %>
                                        <asp:HiddenField ID="hdnquantity" runat="server" Value='<%#Eval("quantity") %>' />
                                    </td>
                                    <td>
                                        <%#Eval("price") %>
                                        <asp:HiddenField ID="hdnprice" runat="server" Value='<%#Eval("quantity") %>' />
                                    </td>
                                    <td>
                                        <%#Eval("amount") %>
                                        <asp:HiddenField ID="hdnamount" runat="server" Value='<%#Eval("amount") %>' />
                                        <asp:HiddenField ID="hdnfkey_cust_po_dtl" runat="server" Value='<%# Eval("fkey_cust_po_dtl") %>' />
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="btnDelete" runat="server" CommandName="Delete" CausesValidation="true" OnClick="btnDelete_Click"
                                            ToolTip="Delete" CommandArgument='<%#Eval("pkey_product") %>' OnClientClick="return confirm('Results in deletion of selected record(s). Do you want to continue?')"><img src="../images/trashcan.png" /></asp:LinkButton>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                        <tr>
                            <td style="float: left; background-color: white;">
                                <asp:Button ID="btnAdd" Text="Add" CssClass="button rounded" runat="server" UseSubmitBehavior="false" OnClientClick="return ProductList();" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <table style="text-align: right; width: 100%;">
                <tr>
                    <td width="92%"><b>Shipping Cost </b></td>
                    <td>
                        <asp:Label ID="lblShippingPrice" runat="server"></asp:Label>
                        <asp:HiddenField ID="hdnAddOn1" runat="server" />
                        <asp:HiddenField ID="hdnAddOn2" runat="server" />
                    </td>
                </tr>
                <tr id="trPromDiscout" runat="server" visible="false">
                    <td><b>Less : promotional discount on products  </b>
                    </td>
                    <td>
                        <asp:Label ID="lblPromoDiscount" runat="server"></asp:Label></td>
                </tr>
                <tr id="trSHipDiscout" runat="server" visible="false">
                    <td><b>Less : promotional discount on shipment  </b>
                    </td>
                    <td>
                        <asp:Label ID="lblShipDiscount" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td><b>Total </b></td>
                    <td>
                        <asp:Label ID="lblTotal" runat="server"></asp:Label></td>
                </tr>
            </table>
            <table width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="50%">&nbsp;</td>
                        <td style="float: right;">
                            <asp:Button runat="server" ID="btnSave" Text="Save" CssClass="button rounded" OnClientClick="if(!validateSupplier()) return false;" OnClick="btnSave_Click"></asp:Button>
                            <asp:Button runat="server" ID="btnClose" Text="Close" CssClass="button rounded" UseSubmitBehavior="false" OnClick="btnClose_Click"></asp:Button>
                            <asp:HiddenField ID="hdnpkey_customer" runat="server" />
                            <asp:HiddenField ID="hdncustomer_id" runat="server" />
                            <asp:HiddenField ID="hdncus_name" runat="server" />
                            <asp:HiddenField ID="hdnaddress1" runat="server" />
                            <asp:HiddenField ID="hdnaddress2" runat="server" />
                            <asp:HiddenField ID="hdncity" runat="server" />
                            <asp:HiddenField ID="hdnstate" runat="server" />
                            <asp:HiddenField ID="hdnpostalcode" runat="server" />
                            <asp:HiddenField ID="hdnpkey_cust_po_master" runat="server" />
                            <asp:HiddenField ID="hdnInvoiceKey" runat="server" />
                            <asp:HiddenField ID="hdntotalOutStands" runat="server" />
                            <asp:HiddenField ID="hdnpkey_product" runat="server" />
                            <asp:HiddenField ID="hdnproduct_name" runat="server" />
                        </td>
                    </tr>
                </tbody>
            </table>
            <div id="divCustomerPreview" style="display: none;">
                <div id="Div1" class="popup-wrapper" runat="server">
                    <div class="popup-title">
                        Customer Details
                    </div>
                    <!-- Header Ends -->
                    <div class="popup-contnt">
                        <table width="100%">
                            <tr>
                                <td width="60%">
                                    <label for="lblCustomerId">Customer Id</label>
                                </td>
                                <td>
                                    <asp:Label ID="lblCustomerId" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="lblFirstName">First Name</label>
                                </td>
                                <td>
                                    <asp:Label ID="lblFirstName" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="lblLastName">Last Name</label>
                                </td>
                                <td>
                                    <asp:Label ID="lblLastName" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="lblTitle">Title</label>
                                </td>
                                <td>
                                    <asp:Label ID="lblTitle" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="lblCompanyName">Company Name</label>
                                </td>
                                <td>
                                    <asp:Label ID="lblCompanyName" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="lblAddress1">Address1</label>
                                </td>
                                <td>
                                    <asp:Label ID="lblAddress1" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="lblAddress2">Address2</label>
                                </td>
                                <td>
                                    <asp:Label ID="lblAddress2" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="lblCity">City</label>
                                </td>
                                <td>
                                    <asp:Label ID="lblCity" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="lblState">State</label>
                                </td>
                                <td>
                                    <asp:Label ID="lblState" runat="server"></asp:Label>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="lblZip">Zip</label>
                                </td>
                                <td>
                                    <asp:Label ID="lblZip" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </form>
    </div>
</asp:Content>
