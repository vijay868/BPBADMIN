﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ACustomerInvoiceView.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Account.ACustomerInvoiceView" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Best Print Buy</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" media="all" href="../css/bestprintbuy_style.css" />
    <link rel="stylesheet" media="all" href="../css/jquery.fancybox.css" />
    <script src="../Scripts/jquery.js"></script>
    <script src="../Scripts/common.js"></script>
    <script src="../Scripts/ddsmoothmenu.js"></script>
    <script src="../Scripts/plugin/fancybox/jquery.fancybox.js"></script>
    <!--[if lt IE 9]>
			<script src="../Scripts/html5.js"></script>
		<![endif]-->
    <!--[if lte IE 7]> <html class="ie7"> <![endif]-->
    <!--[if IE 8]>     <html class="ie8"> <![endif]-->
    <!--[if IE 9]>     <html class="ie9"> <![endif]-->
    <!--[if IE 10]>     <html class="ie10"> <![endif]-->
    <!--[if !IE]><!-->
    <!--<![endif]-->
</head>
<body id="Body" runat="server">
    <div id="Div1" class="popup-wrapper" runat="server">
        <div class="popup-title">
            Export Orders
        </div>
        <!-- Header Ends -->
        <div class="popup-contnt">
            <!-- Menu starts -->
            <form id="Form1" runat="server">
                <table>
                    <tr>
                        <td width="30%">
                            <label>Customer</label>
                        </td>
                        <td>
                            <asp:Label ID="lblCustEmail" runat="server"/><br />
                            <asp:Label ID="lblName" runat="server"/><br />
                            <asp:Label ID="lblAdd1" runat="server"/><br />
                            <asp:Label ID="lblAdd2" runat="server"/><br />
                            <asp:Label ID="lblSate" runat="server"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Invoice Number</label>
                        </td>
                        <td>
                            <asp:Label ID="lblinvoicenumber" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Invoice Date</label>
                        </td>
                        <td>
                            <asp:Label ID="lblInvoiceDate" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Order Number</label>
                        </td>
                        <td>
                            <asp:Label ID="lblordernumber" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Order Date</label>
                        </td>
                        <td>
                            <asp:Label ID="lblorderdate" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>D.C. Number</label>
                        </td>
                        <td>
                            <asp:Label ID="lblDCNumber" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>D.C. Date</label>
                        </td>
                        <td>
                            <asp:Label ID="lblDCDate" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Freight Agency</label>
                        </td>
                        <td>
                            <asp:Label ID="lblFreightAgency" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Tracking Number</label>
                        </td>
                        <td>
                            <asp:Label ID="lblTrackingNumber" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>           
            <div class="clr"></div>
                <table class="tablep">
                    <thead>
                        <tr>
                            <th>Item Code</th>
                            <th>Item Description</th>
                            <th>Qty</th>
                            <th>Price/Unit</th>
                            <th>Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rptrProductList" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td width="25%">
                                        <%#Eval("product_id") %>
                                    </td>
                                    <td width="45%">
                                        <%#Eval("product_details") %>
                                    </td>
                                   <td width="10%">
                                        <%#Eval("quantity") %>
                                    </td>
                                   <td width="10%">
                                        <%#Eval("price") %>
                                    </td>
                                    <td width="10%">
                                        <%#Eval("amount") %>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
                <table style="text-align: right; width: 100%">
                    <tr>
                        <td>
                            <label>Shipping Cost ($)</label>
                            <asp:Label ID="lblShippingCost" runat="server" Text=''></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Promotional discount on products ($)</label>
                            <asp:Label ID="lblPromotionaldiscount" runat="server" Text=''></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Promotional discount on shipment ($)</label>
                            <asp:Label ID="lblPromotionalshipment" runat="server" Text=''></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Total ($)</label>
                            <asp:Label ID="lblTotal" runat="server" Text=''></asp:Label>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</body>
</html>
