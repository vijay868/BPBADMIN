﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomerReceiptView.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Account.CustomerReceiptView" %>

<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->

<!-- Consider adding a manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<head id="Head1" runat="server">
    <title>Best Print Buy</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" media="all" href="../css/bestprintbuy_style.css" />
    <link rel="stylesheet" media="all" href="../css/jquery.fancybox.css" />
    <link rel="stylesheet" href="../css/jquery-ui.css" />
    <script src="../Scripts/jquery.js"></script>
    <script src="../Scripts/jquery-ui.js"></script>
    <script src="../Scripts/common.js"></script>
    <script src="../Scripts/ddsmoothmenu.js"></script>
    <script src="../Scripts/plugin/fancybox/jquery.fancybox.js"></script>
    <script src="../Scripts/jquery.validate.js"></script>
    <!--[if lt IE 9]>
			<script src="../Scripts/html5.js"></script>
		<![endif]-->
    <!--[if lte IE 7]> <html class="ie7"> <![endif]-->
    <!--[if IE 8]>     <html class="ie8"> <![endif]-->
    <!--[if IE 9]>     <html class="ie9"> <![endif]-->
    <!--[if IE 10]>     <html class="ie10"> <![endif]-->
    <!--[if !IE]><!-->

    <!--<![endif]-->
</head>

<body id="Body" runat="server">
    <!-- Wrapper starts -->
    <div id="Div1" class="popup-wrapper" runat="server">
        <div class="popup-title">
            Customer Receipt View
        </div>
        <!-- Header Ends -->
        <div class="popup-contnt">
            <!-- Menu starts -->
            <form id="frmCustomerReceiptView" name="frmCustomerReceiptView" runat="server" class="full">
                <table>
                    <tr>
                        <td style="width: 30%;">
                            <label for="lblReceiptNumbr">Receipt Number</label>
                        </td>
                        <td>
                            <asp:Label ID="lblReceiptNumbr" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="lblCustomer">Customer</label>
                        </td>
                        <td>
                            <asp:Label ID="lblCustomer" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="lblReceiptDate">Receipt Date</label>
                        </td>
                        <td>
                            <asp:Label ID="lblReceiptDate" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 10px 20px 10px 0;">
                            <label for="lblModeOfPayment">Mode Of Payment</label>
                        </td>
                        <td>
                            <asp:Label ID="lblModeOfPayment" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="lblInstrumentNumber">Instrument Number</label>
                        </td>
                        <td>
                            <asp:Label ID="lblInstrumentNumber" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="lblReceiptType">Receipt Type</label>
                        </td>
                        <td>
                            <asp:Label ID="lblReceiptType" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="lblReceiptAmount">Receipt Amount($)</label>
                        </td>
                        <td>
                            <asp:Label ID="lblReceiptAmount" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="border: 1px solid #ff6a00" id="tdAdvance" runat="server" visible="false" colspan="2">
                            <table>
                                <tr>
                                    <td>
                                        <label for="lblTotalAdvanceReceipts">Total Advance Receipts ($)</label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblTotalAdvanceReceipts" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="lblTotalInvoiced">Total Invoiced ($)</label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblTotalInvoiced" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="lblOtherRctAmount">Other Rct. Amount ($)</label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblOtherRctAmount" runat="server"></asp:Label>

                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="lblRctAmount">Rct. Amount ($)</label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblRctAmount" runat="server"></asp:Label>

                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="lblTotalOutstanding">Total Outstanding ($)</label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblTotalOutstanding" runat="server"></asp:Label>

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="box" id="divInvoice" runat="server" visible="false">
                                <div class="header">
                                    <h2>Pending Invoices / Orders</h2>
                                </div>
                                <div class="content">
                                    <div class="clr"></div>
                                    <table class="styled dynamic nosort" data-filter-bar='none' data-sort="false" data-table-tools='{"display":true}' data-max-items-per-page="-1">
                                        <thead>
                                            <tr>
                                                <th>Invoice # / Date</th>
                                                <th>Order Number / Date</th>
                                                <th>Amount</th>
                                                <th>Balance</th>
                                                <th>Paid</th>
                                                <th>Pay</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:Repeater ID="rptrInvoiceList" runat="server">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <%#Eval("invoice_number") %><br />
                                                            <%#Eval("invoice_date") %>
                                                        </td>
                                                        <td>
                                                            <%#Eval("po_number") %><br />
                                                            <%#Eval("po_date") %>
                                                        </td>
                                                        <td>
                                                            <%#Eval("invoice_amount") %>
                                                        </td>
                                                        <td>
                                                            <%#Eval("customer_name") %>
                                                        </td>
                                                        <td>
                                                            <%#Eval("balance_amount") %>
                                                        </td>
                                                        <td>
                                                            <%#Eval("paid_amount") %>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</body>
</html>
