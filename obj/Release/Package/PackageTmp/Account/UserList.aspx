﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="UserList.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Account.UserList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function deleteUser() {
            if ('<%=HasAccess("UserList", "delete")%>' != 'False') {
                return confirm('This action results in removal of the user(s) and associated functional items will be detached from the user, Are you sure?');
            }
            else {
                alert('User does not have access.');
                return false;
            }
        }
        function addUser() {
            if ('<%=HasAccess("UserList", "add")%>' == 'False') {
                 alert('User does not have access.');
                 return false;
             }
             return true;
         }
    </script>
    <div class="clr"></div>
    <!-- Container Starts -->
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="DashBoard.aspx">Home</a></li>
            <li class="liColor">Security</li>
            <li>User List</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>User List</h2>
        <form id="frmUserList" name="frmUserList" runat="server">           
            <div class="clr"></div>
            <asp:GridView ID="UserlistGrid" runat="server" AutoGenerateColumns="false" EnableViewState="true"
                ForeColor="Black" Width="100%" PageSize="20" AllowPaging="true" AllowSorting="true"
                class="table" name="UserlistGrid" OnPageIndexChanging="UserlistGrid_PageIndexChanging" OnSorting="UserlistGrid_Sorting"
                OnRowDataBound="UserlistGrid_RowDataBound">
                <PagerStyle HorizontalAlign="Right" CssClass="dtPage"></PagerStyle>
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />
                <Columns>
                    <asp:BoundField HeaderText="#SL"></asp:BoundField>

                    <asp:TemplateField HeaderText="First Name" SortExpression="first_name">
                        <HeaderStyle Width="20%" ForeColor="White" />
                        <ItemTemplate>
                            <a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("UserDetails", "view")%>','UserDetails.aspx?userID=<%# DataBinder.Eval(Container,"DataItem.pkey_user") %>')" title="Edit"><%# DataBinder.Eval(Container,"DataItem.first_name") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Last Name" SortExpression="last_name">
                        <HeaderStyle Width="20%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("UserDetails", "view")%>','UserDetails.aspx?userID=<%# DataBinder.Eval(Container,"DataItem.pkey_user") %>')" title="Edit"><%# DataBinder.Eval(Container,"DataItem.last_name") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Login ID" SortExpression="login_id">
                        <HeaderStyle Width="20%" ForeColor="White" />
                        <ItemTemplate>
                            <a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("UserDetails", "view")%>','UserDetails.aspx?userID=<%# DataBinder.Eval(Container,"DataItem.pkey_user") %>')" title="Edit"><%# DataBinder.Eval(Container,"DataItem.login_id") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Groups">
                        <HeaderStyle Width="7%" />
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container,"DataItem.groups") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Role">
                        <HeaderStyle Width="20%" />
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container,"DataItem.user_role") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Delete">
                        <HeaderStyle Width="7%" />
                        <ItemTemplate>
                            <asp:LinkButton ID="btnDelete" runat="server" CommandName="Delete" CausesValidation="true"
                                ToolTip="Delete" CommandArgument='<%#Bind("pkey_user")%>' OnClientClick="return deleteUser();"
                                OnClick="btnDelete_Click"><img src="../images/trashcan.png" /></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
             <asp:Label ID="lblTotalRecords" runat="server" Font-Bold="true"></asp:Label>
            <!-- table Ends -->
            <div class="btn_lst">
                <asp:Button ID="btnAddNewUser" Text="Add New User" CssClass="button rounded" runat="server" OnClick="btnAddNewUser_Click" OnClientClick="return addUser();"></asp:Button>
                <div class="rgt">
                    <asp:Button ID="btnClose" type="button" Text="Close" CssClass="button rounded" runat="server" OnClick="btnClose_Click"></asp:Button>
                </div>
            </div>
        </form>
    </div>
</asp:Content>
