﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserAccessList.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Account.UserAccessList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Best Print Buy</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" media="all" href="../css/bestprintbuy_style.css" />
    <link rel="stylesheet" media="all" href="../css/jquery.fancybox.css" />
    <script src="../Scripts/jquery.js"></script>
    <script src="../Scripts/common.js"></script>
    <script src="../Scripts/ddsmoothmenu.js"></script>
    <script src="../Scripts/plugin/fancybox/jquery.fancybox.js"></script>
    <!--[if lt IE 9]>
			<script src="../Scripts/html5.js"></script>
		<![endif]-->
    <!--[if lte IE 7]> <html class="ie7"> <![endif]-->
    <!--[if IE 8]>     <html class="ie8"> <![endif]-->
    <!--[if IE 9]>     <html class="ie9"> <![endif]-->
    <!--[if IE 10]>     <html class="ie10"> <![endif]-->
    <!--[if !IE]><!-->
    <!--<![endif]-->
</head>
<body id="Body" runat="server">
    <!-- Wrapper starts -->
    <div id="Div1" class="popup-wrapper" runat="server">
        <div class="popup-title">
            User Access List
        </div>
        <!-- Header Ends -->
        <div class="popup-contnt">
            <!-- Menu starts -->
            <form id="Form1" runat="server">
                <table class="tablep">
                    <thead>
                        <tr>
                            <th width="3%"></th>
                            <th style="width: 40%;">Screen Name</th>
                            <th>List</th>
                            <th>View</th>
                            <th>Add</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rptrUserAccesslist" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td></td>
                                    <td>
                                        <%#Eval("screen_name") %>
                                    </td>
                                    <td class="center">
                                        <%#Eval("listaccess") %>
                                    </td>
                                    <td class="center">
                                        <%#Eval("viewaccess") %>
                                    </td>
                                    <td class="center">
                                        <%#Eval("addaccess") %>
                                    </td>
                                    <td class="center">
                                        <%#Eval("editaccess") %>
                                    </td>
                                    <td class="center">
                                        <%#Eval("deleteaccess") %>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
                <table>
                    <tr>
                        <td style="float: right;">
                            <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClientClick="parent.$.fancybox.close(); return false;"></asp:Button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</body>
</html>
