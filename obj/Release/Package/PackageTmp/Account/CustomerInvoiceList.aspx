﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="CustomerInvoiceList.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Account.CustomerInvoiceList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        $(function () {
            $("#ContentPlaceHolder1_txtFromDate").datepicker();
            $("#ContentPlaceHolder1_txtToDate").datepicker();
        });

        function showSelectPopUp(pkeyCustInvoice) {
            url = "ACustomerInvoiceView.aspx?pkeyCustInvoice=" + pkeyCustInvoice;
            $.fancybox({
                'width': '80%',
                'height': '80%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': url,
                'type': 'iframe'
            });
            return false;
        }

    </script>
    <script>
        $().ready(function () {
            $("#frmInvoiceList").validate();
        });
</script>
    <div class="contant_hldr">
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Accounts</li>
            <li>Invoices</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Customer Invoice List</h2>
        <form class="form" runat="server" id="frmInvoiceList">
            <table width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="7%">From Date</td>
                        <td width="20%">
                            <asp:TextBox CssClass="text date" name="txtFromDate" ID="txtFromDate" runat="server"></asp:TextBox></td>
                        <td width="7%" style="padding-left: 15px">To Date</td>
                        <td width="20%">
                            <asp:TextBox CssClass="text date" name="txtToDate" ID="txtToDate" runat="server"></asp:TextBox></td>
                        <td width="40%"></td>
                    </tr>
                </tbody>
            </table>
            <table width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="8%">Filter List for</td>
                        <td width="15%">
                            <asp:DropDownList name="ddlFilterFor" ID="ddlFilterFor" runat="server">
                                <asp:ListItem Text="Customer" Value="PPS_CUSTOMERS.CUSTOMER_ID" />
                            </asp:DropDownList>
                        </td>
                        <td width="1%"></td>
                        <td width="15%">
                            <asp:DropDownList name="ddlOperator" ID="ddlOperator" runat="server" CssClass="select">
                                <asp:ListItem Value="=">Equals</asp:ListItem>
                                <asp:ListItem Value="<>">Not Equals</asp:ListItem>
                                <asp:ListItem Value="LIKE%">Starts With</asp:ListItem>
                                <asp:ListItem Value="%LIKE">Ends with</asp:ListItem>
                                <asp:ListItem Value="%LIKE%">Contains</asp:ListItem>
                            </asp:DropDownList></td>
                        <td width="1%"></td>
                        <td width="20%">
                            <asp:TextBox name="txtValue" CssClass="text" ID="txtValue" runat="server" Width="" required></asp:TextBox>
                        </td>
                        <td width="10%" style="padding-left: 5px">
                            <asp:Button runat="server" ID="btnGo" CssClass="button rounded" Text="Apply" OnClick="btnGo_Click"></asp:Button>
                        </td>
                        <td width="5%">
                            <asp:Button runat="server" ID="btnReset" CssClass="button rounded" Text="Reset" OnClick="btnReset_Click" UseSubmitBehavior="false" Visible="false"></asp:Button>
                        </td>
                        <td width="35%"></td>
                    </tr>
                </tbody>
            </table>            
            <div class="clr"></div>
            <asp:GridView ID="InvoiceListGrid" runat="server" AutoGenerateColumns="false" EnableViewState="true"
                ForeColor="Black" Width="100%" class="table" name="InvoiceListGrid" OnRowDeleting="InvoiceListGrid_RowDeleting">
                <PagerStyle HorizontalAlign="Right" CssClass="dtPage"></PagerStyle>
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />
                <Columns>
                    <asp:TemplateField HeaderText="#SL">
                        <HeaderStyle Width="10%" ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <span><%# GetRecordCountForPage()%></span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Invoice Number">
                        <HeaderStyle Width="15%" ForeColor="White" />
                        <ItemTemplate>
                            <a href="#" onclick='return showSelectPopUp("<%#Eval("pkey_customer_invoice_master") %>");'><%#Eval("invoice_number") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Invoice Date">
                        <HeaderStyle Width="10%" ForeColor="White" />
                        <ItemTemplate>
                            <%#Eval("invoice_date","{0:MM/dd/yyyy}") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Order Number">
                        <HeaderStyle Width="10%" ForeColor="White" />
                        <ItemTemplate>
                            <%#Eval("order_number") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Customer">
                        <HeaderStyle Width="30%" ForeColor="White" />
                        <ItemTemplate>
                            <%#Eval("customer_id") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Invoice Value">
                        <HeaderStyle Width="10%" ForeColor="White" />
                        <ItemTemplate>
                            <%#Eval("invoice_amount") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Order Value">
                        <HeaderStyle Width="10%" ForeColor="White" />
                        <ItemTemplate>
                            <%#Eval("po_net_amount") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Delete">
                        <HeaderStyle Width="5%" ForeColor="White" />
                        <ItemTemplate>
                            <asp:LinkButton ID="btnDelete" runat="server" CommandName="Delete" CausesValidation="true"
                                ToolTip="Delete" CommandArgument='<%#Bind("pkey_customer_invoice_master")%>' OnClientClick="return confirm('Results in deletion of selected record(s). Do you want to continue?')"
                                OnClick="btnDelete_Click"><img src="../images/trashcan.png" /></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
             <asp:Label ID="lblTotalRecords" runat="server" Font-Bold="true"></asp:Label>
            <div class="btn_lst">
                <div class="lft">
                    <asp:Button runat="server" ID="btnAdd" CssClass="button rounded" Text="Add an Invoice" OnClick="btnAdd_Click" UseSubmitBehavior="false"></asp:Button>
                </div>
                <div class="rgt">
                    <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClick="btnClose_Click" UseSubmitBehavior="false"></asp:Button>
                </div>
            </div>
        </form>
    </div>
</asp:Content>
