﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="PaymentDetails.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Account.PaymentDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">

        function SupplierList() {
            var RegisterKey = $("#ContentPlaceHolder1_hdnSupplierkey").val() != "" ? $("#ContentPlaceHolder1_hdnSupplierkey").val() : "-1";
            url = "../BusinessEntites/SelectBusinessEntity.aspx?bid=sup";
            $.fancybox({
                'width': '70%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': url,
                'type': 'iframe'
            });

            return false;
        }
        function SetSupplierData(parentID, SupplierName) {
            $("#ContentPlaceHolder1_hdnSupplierkey").val(parentID);
            $("#ContentPlaceHolder1_txtSupplier").val(SupplierName);
            //$("#ContentPlaceHolder1_txtPayName").val(SupplierName);
            __doPostBack("ctl00$ContentPlaceHolder1$lbtnSupplier", "");
            parent.$.fancybox.close();
        }

        function SupplierView(value) {
            if ($("#ContentPlaceHolder1_txtSupplier").val() == "") {
                alert("Please select the supplier.");
                return false;
            }
            var url = "";
            var RegisterKey = $("#ContentPlaceHolder1_hdnSupplierkey").val() != "" ? $("#ContentPlaceHolder1_hdnSupplierkey").val() : "-1";
            if (value == "Contact") {
                if ($("#ContentPlaceHolder1_hdnpkey_customer").val() != "-1")
                    url = "../BusinessEntites/ContactPopUp.aspx?pkeyContact=" + $("#ContentPlaceHolder1_hdnpkey_customer").val();
                else
                    url = "../BusinessEntites/BusinessRegisterPopUp.aspx?bid=bur&pkey=" + RegisterKey;
            }
            else
                url = "../BusinessEntites/BusinessRegisterPopUp.aspx?bid=bur&pkey=" + RegisterKey;
            $.fancybox({
                'width': '70%',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': url,
                'type': 'iframe'
            });
            return false;
        }

        $().ready(function () {
            $("#frmPaymentDetails").validate();
        });

        $(function () {
            $("#ContentPlaceHolder1_txtPaymentDate").datepicker();
        });
        var totalBalance = 0.00;
        var adjAmount = 0.00;
        function checkInvoice() {
            var sOption = '<%=paymentType%>';
            var key = $("#ContentPlaceHolder1_txtSupplier").val();  // window.document.frmStatementofAccount.txtSupplier.value;
            if (key == '') {
                alert("Supplier is required, Please select a value and try again.")
                return false;
            }

            if (sOption == "0" && !checkInvoiceAmount()) return false;
            else if (sOption == "2" && !checkOnAccountAmount()) return false;
            return true;

        }

        function checkOnAccountAmount() {
            var totalOutStands = $("#ContentPlaceHolder1_hdntotalOutStands").val();

            var pmttempAmount = '';
            if (eval($("#ContentPlaceHolder1_txtPaymentAmount").val()))
                pmttempAmount = eval($("#ContentPlaceHolder1_txtPaymentAmount").val());
            var pmtAmount = 0.00;

            if (!isNaN(pmttempAmount)) {
                pmtAmount = parseFloat(pmttempAmount);
            }
            if (pmtAmount != totalOutStands) {
                alert("Amount should be exactly equal to the entered Payment amount.");
                return false;
            }
            return true;
        }

        function checkInvoiceAmount() {
            var pmttempAmount = '';
            if (eval($("#ContentPlaceHolder1_txtPaymentAmount").val()))
                pmttempAmount = $("#ContentPlaceHolder1_txtPaymentAmount").val();
            var pmtAmount = 0.00;

            if (!isNaN(pmttempAmount)) {
                pmtAmount = parseFloat(pmttempAmount);
            }

            if (pmtAmount != totalBalance && (pmtAmount - totalBalance) < 0.01) {
                alert("Amount should be exactly equal to the entered payment amount.");
                return false;
            }
            else if (totalBalance <= 0.00) {
                alert("Amount should be exactly equal to the entered payment amount.");
                return false;
            }
            return true;

        }

        function UpdateTotalBalance(amount) {
            var textName = amount.name;
            var payAmount = 0.00;
            var tempAmount = amount.value;
            try {
                tempAmount = tempAmount.trim();
            } catch (e) {

            }

            if (tempAmount && '' != tempAmount && !isNaN(tempAmount)) {
                payAmount = parseFloat(tempAmount);
            }

            var splitArray = textName.split("txtPayAmount")
            var sizeArray = splitArray.length;
            var invoiceMaster = 0;
            var balanceAmt = 0.00;
            if (sizeArray > 0)
                invoiceMaster = splitArray[sizeArray - 1];
            if (invoiceMaster && '' != invoiceMaster && !isNaN(invoiceMaster))
                invoiceMaster = parseInt(invoiceMaster);


            if (invoiceMaster > 0) {
                var tempBalAmount = $(".classBalanceAmount" + invoiceMaster).val();
                if (!isNaN(tempBalAmount))
                    balanceAmt = parseFloat(tempBalAmount);

                if (payAmount != 0.00 && payAmount > balanceAmt) {
                    alert("Entered amount in pay amount column is more than the Invoce Balance.");
                    if (adjAmount == 0 || adjAmount == 0.00)
                        amount.value = '';
                    else
                        amount.value = adjAmount;
                    amount.focus();
                }
                else if (payAmount != 0.00) {
                    var tempTotalBalance = totalBalance;
                    totalBalance = totalBalance - (adjAmount - payAmount);
                    adjAmount = 0.00;
                }
                else if (payAmount == 0.00 || payAmount == 0) {
                    totalBalance = totalBalance - adjAmount;
                    if (totalBalance < 0.01) totalBalance = 0.00;
                    adjAmount = 0.00;

                }
            }
            else {
                alert("invoice key lost");
            }
            try {
                amount.value = amount.value.trim();
            } catch (e) {

            }
            amount.value = Math.round(amount.value, 2);

        }

        function UpdateAdjAmount(amount) {
            if (amount) {
                var tempAmount = amount.value;
                try {
                    tempAmount = tempAmount.trim()
                } catch (e) {

                }
                if (tempAmount && '' != tempAmount && !isNaN(tempAmount))
                    adjAmount = parseFloat(tempAmount);
            }
            if (amount.value == '0.00') this.select();
        }
    </script>


    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Accounts</li>
            <li><a href="PaymentList.aspx">Payment List</a></li>
            <li>Payment Details</li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Payment Details</h2>
        <form runat="server" id="frmPaymentDetails">
            <table class="accounts-table" width="100%">
                <tbody>
                    <tr>
                        <td width="15%">
                            <label>Payment Number*</label></td>
                        <td width="20%">
                            <asp:Label runat="server" ID="lblPaymentNumbr" /></td>
                        <td width="15%" align="right">
                            <asp:Button ID="btnSupplier" Text="Supplier" CssClass="button rounded" runat="server" UseSubmitBehavior="false" OnClientClick="return SupplierView('Supplier');" />
                            <asp:LinkButton ID="lbtnSupplier" runat="server" OnClick="lbtnSupplier_Click" />
                            <asp:Button ID="btnSupplierDetail" Text="..." CssClass="button rounded" Font-Bold="true" runat="server" UseSubmitBehavior="false" OnClientClick="return SupplierList();" />
                        </td>
                        <td width="35%"  style="padding-left:10px;">
                            <asp:TextBox runat="server" CssClass="text-box" ID="txtSupplier"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Payment Date*</label></td>
                        <td>
                            <asp:TextBox name="txtPaymentDate" ID="txtPaymentDate" CssClass="text-box date" runat="server" required></asp:TextBox></td>
                        <td align="right">
                            <asp:Button ID="btnPayTo" Text="PayTo" runat="server" CssClass="button rounded" UseSubmitBehavior="false" OnClientClick="return SupplierView('Contact');" />
                        </td>

                        <td style="padding-left:10px;">
                            <asp:TextBox ID="txtPayName" CssClass="text-box" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Mode Of Payment</label></td>
                        <td>
                            <asp:DropDownList name="ddlPaymentMode" ID="ddlPaymentMode" CssClass="slect-box" data-placeholder="Choose a Name" runat="server">
                                <asp:ListItem Value="CA" Selected="True">Cash</asp:ListItem>
                                <asp:ListItem Value="CH">Check</asp:ListItem>
                                <asp:ListItem Value="FT">Funds Transfer</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <label></label>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <label>Instrument Number</label>
                        </td>
                        <td>
                            <asp:TextBox name="txtInstrumentNumber" ID="txtInstrumentNumber" CssClass="text-box" runat="server">
                            </asp:TextBox></td>
                        <td class="a_cnt" colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;<asp:RadioButton runat="server" ID="rbtnAdvance" CssClass="check-box" GroupName="advance" OnCheckedChanged="rbtnAdvance_CheckedChanged" AutoPostBack="true" />Advance
                        &nbsp;&nbsp;&nbsp;&nbsp;<asp:RadioButton CssClass="check-box" runat="server" ID="rbtnAgainstInvoice" GroupName="advance" Checked="true" OnCheckedChanged="rbtnAdvance_CheckedChanged" AutoPostBack="true" />Against Invoice 
                         &nbsp;&nbsp;&nbsp;&nbsp; 
                            <asp:RadioButton runat="server" ID="rbtnOnAccount" CssClass="check-box" GroupName="advance" OnCheckedChanged="rbtnAdvance_CheckedChanged" AutoPostBack="true" />On Account                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Payment Amount($)*</label>
                        </td>
                        <td>
                            <asp:TextBox name="txtPaymentAmount" CssClass="text-box" ID="txtPaymentAmount" required runat="server"></asp:TextBox></td>

                        <td colspan="2" style="text-align: center;">
                            <asp:Label ID="lblRoutingnumber" runat="server" Font-Bold="true"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Label ID="lblBankname" runat="server" Font-Bold="true"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Label ID="lblAccountnumber" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td width="15%"></td>
                        <div id="divAdvance" runat="server" visible="false">
                            <td colspan="2" style="border: 1px solid #ff6a00">
                                <table width="100%" id="amountstable">
                                    <tbody>
                                        <tr>
                                            <td width="55%">
                                                <label>Total Advance Payments($)</label></td>
                                            <td width="35%">
                                                <asp:Label ID="lblAdvancePatyment" runat="server"></asp:Label>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td>
                                                <label>Total Invoiced ($)</label></td>
                                            <td>
                                                <asp:Label ID="lblTotalInvoice" runat="server"></asp:Label>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td>
                                                <label>Total Paid ($)</label></td>
                                            <td>
                                                <asp:Label ID="lblTotalPaid" runat="server"></asp:Label>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label>Total Outstanding ($)</label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblTotalOutstanding" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </div>
                        <td></td>
                    </tr>
                </tbody>
            </table>           
            <div class="clr"></div>

            <div id="divInvoice" runat="server" style="padding-top: 4%;">
                <h2>Pending Invoices</h2>
                <table class="tablep" width="100%" style="padding-bottom: 100px">
                    <thead>
                        <tr>
                            <th>#SL</th>
                            <th width="20%">Invoice # </th>
                            <th width="15%">Date</th>
                            <th width="15%">Amount</th>
                            <th width="15%">Balance</th>
                            <th width="15%">Paid</th>
                            <th width="15%">Pay</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rptrInvoiceList" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <%#Container.ItemIndex + 1 %>
                                    </td>
                                    <td>
                                        <%#Eval("invoice_number") %>
                                    </td>
                                    <td>
                                        <%#Eval("invoice_date") %>
                                    </td>
                                    <td>
                                        <%#Eval("invoice_amount") %>
                                    </td>
                                    <td>
                                        <%#Eval("balance_amount") %>
                                        <input class='classBalanceAmount<%# DataBinder.Eval(Container, "DataItem.pkey_br_invoice_master") %>' name='txtBalanceAmount<%# DataBinder.Eval(Container, "DataItem.pkey_br_invoice_master") %>' value='<%# DataBinder.Eval(Container, "DataItem.balance_amount") %>' type="hidden">
                                    </td>
                                    <td>
                                        <%#Eval("paid_amount") %>
                                    </td>
                                    <td>
                                        <input class="textbox" name='txtPayAmount<%# DataBinder.Eval(Container, "DataItem.pkey_br_invoice_master") %>' type="text" value='' dir="rtl" onblur="UpdateTotalBalance(this);" onfocus="UpdateAdjAmount(this)">$
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
                <div id="Div2" class="ttl" runat="server">
                    <b>Total Number of Invoices: </b><%=recordCount%>
                    </br>
                    <b>Totals:</b>
                    <asp:Label ID="lblTotalAmt" runat="server"></asp:Label>(Amount) &nbsp;<asp:Label ID="lblTotalBalance" runat="server"></asp:Label>(Balance)&nbsp;<asp:Label ID="lblTotalPaidAmt" runat="server"></asp:Label>(Paid)
                </div>
            </div>
            <div class="btn_lst">
                <div class="rgt" style="padding-top: 2%">
                    <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" UseSubmitBehavior="false" OnClick="btnClose_Click"></asp:Button>
                    <asp:Button runat="server" ID="btnSave" CssClass="button rounded" Text="Save" OnClick="btnSave_Click" OnClientClick="return checkInvoice();"></asp:Button>
                    <asp:HiddenField ID="hdnpkey_customer" runat="server" />
                    <asp:HiddenField ID="hdnpkey_cust_po_master" runat="server" />
                    <asp:HiddenField ID="hdnInvoiceKey" runat="server" />
                    <asp:HiddenField ID="hdntotalOutStands" runat="server" />
                    <asp:HiddenField ID="hdnSupplierkey" runat="server" />
                </div>
            </div>
        </form>
    </div>
</asp:Content>
