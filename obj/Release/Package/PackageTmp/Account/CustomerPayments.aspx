﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="CustomerPayments.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Account.CustomerPayments" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="contant_hldr">
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Accounts</li>
            <li>Customer Reciepts</li>
        </ul>
        <h2>Customer Reciepts</h2>
        <form class="" runat="server" id="frmCustomerPayment">            
            <div class="clr"></div>
            <asp:GridView ID="CustomerPaymentListGrid" runat="server" AutoGenerateColumns="false" EnableViewCountry="true" AllowSorting="true"
                ForeColor="Black" Width="100%" PageSize="20" AllowPaging="true" OnPageIndexChanging="CustomerPaymentListGrid_PageIndexChanging"
                class="table" name="CustomerPaymentListGrid" OnSorting="CustomerPaymentListGrid_Sorting">
                <PagerStyle HorizontalAlign="Right" CssClass="dtPage"></PagerStyle>
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />
                <Columns>
                    <asp:TemplateField HeaderText="#SL">
                        <HeaderStyle ForeColor="White"></HeaderStyle>
                        <ItemTemplate>
                            <span><%# GetRecordCountForPage()%></span>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Date" SortExpression="date_created">
                        <HeaderStyle Width="10%" ForeColor="White" />
                        <ItemTemplate>
                            <%#Eval("date_created","{0:MM/dd/yyyy}") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Email" SortExpression="email_id">
                        <HeaderStyle Width="25%" ForeColor="White" />
                        <ItemTemplate>
                            <a href="javascript:RedirectAfterCheckingRights('<%= HasAccess("CustomerPayment", "view")%>','CustomerPayment.aspx?pkeyReceipt=<%#Eval("pkey_cust_admin_receipts") %>')" data-gravity="s" title="Edit"><%#Eval("email_id") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Name">
                        <HeaderStyle Width="15%" ForeColor="White" />
                        <ItemTemplate>
                            <%#Eval("first_name") %> <%#Eval("last_name") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="PO #">
                        <HeaderStyle Width="15%" ForeColor="White" />
                        <ItemTemplate>
                            <%#Eval("po_number") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Amount">
                        <HeaderStyle Width="15%" ForeColor="White" />
                        <ItemTemplate>
                             <%#Eval("amount","{0:###,###.#0}") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="PN Ref#">
                        <HeaderStyle Width="15%" ForeColor="White" />
                        <ItemTemplate>
                            <%#Eval("pnref_number") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <div class="ttl"><b>Total Number of Attributes: </b><%=recordCount%></div>
            <div class="btn_lst">
                <div class="rgt">
                    <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClick="btnClose_Click"></asp:Button>
                    <asp:Button runat="server" ID="btnChangeCustomer" CssClass="button rounded" Text="Charge Customer" OnClick="btnChangeCustomer_Click"></asp:Button>
                </div>
            </div>
        </form>
    </div>
</asp:Content>
