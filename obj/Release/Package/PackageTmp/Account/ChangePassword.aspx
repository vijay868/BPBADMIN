﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="Bestprintbuy.Admin.Web.Account.ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
        $(document).ready(function () {
            $("#frmChangePassword").validate();
        });
    </script>
    <div class="contant_hldr">
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Security</li>
            <li>Change Password</li>
        </ul>
        <h2>Change Password</h2>
        <form class="" runat="server" id="frmChangePassword">
            <table cellpadding="0" cellspacing="0" border="0" class="accounts-table">
                <tr>
                    <td width="15%">
                        <label>Login ID</label></td>
                    <td width="20%">
                        <asp:Label ID="lblLoginID" runat="server"></asp:Label></td>
                    <td width="50%"></td>
                </tr>
                <tr>
                    <td>
                        <label>Old Password*</label></td>
                    <td>
                        <asp:TextBox ID="txtOldPassword" CssClass="text-box" runat="server" TextMode="Password" required></asp:TextBox></td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <label>New Password*</label></td>
                    <td>
                        <asp:TextBox ID="txtNewPassword" CssClass="text-box" runat="server" TextMode="Password" minlength="6" required></asp:TextBox></td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <label>Confirm New Password*</label></td>
                    <td>
                        <asp:TextBox ID="txtConfirmPassword" CssClass="text-box" runat="server" equalTo="#ContentPlaceHolder1_txtNewPassword" TextMode="Password" required></asp:TextBox></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td align="right">
                        <asp:Button ID="btnSave" CssClass="button rounded" runat="server" Text="Save" OnClick="btnSave_Click" />
                        <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClick="btnClose_Click" UseSubmitBehavior="false"></asp:Button>                        
                    </td>
                </tr>
            </table>
        </form>
    </div>
</asp:Content>
