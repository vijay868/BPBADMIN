﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;

namespace Bestprintbuy.Admin.Web.BusinessEntites
{
    public partial class SelectBusinessContact : PageBase
    {
        protected int pkeyregister = -1;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                GetData();
            }
        }

        private void GetData()
        {
            try
            {
                int contactKey = -1;
                pkeyregister = Convert.ToInt32(Request.QueryString["pkeyregister"] != "" ? Request.QueryString["pkeyregister"] : "-1");
                BusinessEntityModel model = BusinessEntitiesService.GetData(contactKey, pkeyregister, ConnString);
                if (model.BusinessContacts.Rows.Count > 0)
                {
                    rptrBusinessContactDetails.DataSource = model.BusinessContacts;
                    rptrBusinessContactDetails.DataBind();
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (RepeaterItem item in rptrBusinessContactDetails.Items)
                {
                    HiddenField hdnPkeyContact = (HiddenField)item.FindControl("hdnPkeyContact");
                    HiddenField hdnFirstName = (HiddenField)item.FindControl("hdnFirstName");
                    HiddenField hdnLastName = (HiddenField)item.FindControl("hdnLastName");
                    string cus_name = hdnFirstName.Value + " " + hdnLastName.Value;

                    CheckBox chkbox = (CheckBox)item.FindControl("chkSelect");
                    if (chkbox.Checked == true)
                    {
                        string script = string.Format("SetDataAndClose('{0}','{1}','{2}');", hdnPkeyContact.Value, hdnFirstName.Value, hdnLastName.Value);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "jscript", script, true);
                    }
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }
    }
}