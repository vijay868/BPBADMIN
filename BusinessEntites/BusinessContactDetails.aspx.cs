﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;

namespace Bestprintbuy.Admin.Web.BusinessEntites
{
    public partial class BusinessContactDetails : PageBase
    {
        protected int pkeyRegister = -1;
        protected int pkeyContact = -2;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    pkeyRegister = Convert.ToInt32(Request.QueryString["fkeyRegister"]);
                    pkeyContact = Convert.ToInt32(Request.QueryString["pkeyContact"]);
                    ViewState["pkeyRegister"] = pkeyRegister;
                    BusinessEntityModel model = BusinessEntitiesService.GetData(pkeyContact, pkeyRegister, ConnString);
                    ViewState["model"] = model;
                    if (model.Countries.Rows.Count > 0)
                    {
                        ddlCountry.DataSource = model.Countries;
                        ddlCountry.DataTextField = "country_name";
                        ddlCountry.DataValueField = "pkey_country";
                        ddlCountry.DataBind();
                    }
                    if (model.States.Rows.Count > 0)
                    {
                        ddlState.DataSource = model.States;
                        ddlState.DataTextField = "state_name";
                        ddlState.DataValueField = "pkey_state";
                        ddlState.DataBind();
                    }
                    if (pkeyContact != -2)
                    {
                        if (model.Phones.Rows.Count > 0)
                        {
                            rptrPhones.DataSource = model.Phones;
                            rptrPhones.DataBind();
                        }
                        if (model.BusinessContacts.Rows.Count > 0)
                        {
                            BusinessEntityModel.BusinessContactsRow row = (BusinessEntityModel.BusinessContactsRow)model.BusinessContacts.Rows[0];
                            txtContactID.Text = row.IsCONTACT_IDNull() ? "" : row.CONTACT_ID;
                            txtFirstName.Text = row.IsFIRST_NAMENull() ? "" : row.FIRST_NAME;
                            txtLastName.Text = row.IsLAST_NAMENull() ? "" : row.LAST_NAME;
                            txtTitle.Text = row.IsTITLENull() ? "" : row.TITLE;
                            chkPrinContact.Checked = Convert.ToBoolean(row.IS_PRINCIPAL_CONTACT);
                            txtAddress1.Text = row.IsADDRESS1Null() ? "" : row.ADDRESS1;
                            txtAddress2.Text = row.IsADDRESS2Null() ? "" : row.ADDRESS2;
                            txtCity.Text = row.IsCITYNull() ? "" : row.CITY;
                            txtPostalCode.Text = row.IsPOSTAL_CODENull() ? "" : row.POSTAL_CODE;
                            ddlCountry.SelectedValue = row.IsFKEY_COUNTRYNull() ? "-1" : row.FKEY_COUNTRY.ToString();
                            ddlState.SelectedValue = row.IsFKEY_STATENull() ? "-1" : row.FKEY_STATE.ToString();
                            ddlSalutation.SelectedValue = row.IsSALUTATIONNull() ? "-1" : row.SALUTATION.ToString();
                        }
                        btnDelete.Visible = true;
                        fSetPhone.Visible = true;
                    }
                    else
                    {
                        chkPrinContact.Checked = true;
                    }
                }
                catch (Exception ex)
                {
                    FileLogger.WriteException(ex);
                    AlertScript(ex.Message);
                }
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            pkeyRegister = Convert.ToInt32(Request.QueryString["fkeyRegister"]);
            Response.Redirect("BusinessContactsList.aspx?pkeyRegister=" + pkeyRegister);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                pkeyContact = Convert.ToInt32(Request.QueryString["pkeyContact"]);
                BusinessEntityModel model = new BusinessEntityModel();
                BusinessEntityModel.BusinessContactsRow row = model.BusinessContacts.NewBusinessContactsRow();
                row.PKEY_BR_CONTACT = pkeyContact;
                row.CONTACT_ID = txtContactID.Text;
                row.FIRST_NAME = txtFirstName.Text;
                row.LAST_NAME = txtLastName.Text;
                row.TITLE = txtTitle.Text;
                row.SALUTATION = ddlSalutation.SelectedValue == "" ? "-1" : ddlSalutation.SelectedValue;
                row.IS_PRINCIPAL_CONTACT = chkPrinContact.Checked ? "1" : "0";
                row.ADDRESS1 = txtAddress1.Text;
                row.ADDRESS2 = txtAddress2.Text;
                row.CITY = txtCity.Text;
                row.POSTAL_CODE = txtPostalCode.Text;
                row.fkey_user = Identity.UserPK;
                row.FKEY_STATE = ddlState.SelectedValue == "" ? -1 : Convert.ToInt32(ddlState.SelectedValue);
                row.FKEY_COUNTRY = ddlCountry.SelectedValue == "" ? -1 : Convert.ToInt32(ddlCountry.SelectedValue);
                row.FKEY_BUSINESS_REGISTER = Convert.ToInt32(ViewState["pkeyRegister"]);
                model.BusinessContacts.AddBusinessContactsRow(row);

                pkeyRegister = Convert.ToInt32(Request.QueryString["fkeyRegister"]);
                pkeyContact = BusinessEntitiesService.UpdateBusinessContact(model, ConnString);
                Response.Redirect("BusinessContactsList.aspx?pkeyRegister=" + pkeyRegister);
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                BusinessEntitiesService.DeleteBusinessContact(pkeyContact, ConnString);
                pkeyRegister = Convert.ToInt32(Request.QueryString["fkeyRegister"]);
                Response.Redirect("BusinessContactsList.aspx?pkeyRegister=" + pkeyRegister);
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }           
        }

        protected void lbtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                pkeyContact = Convert.ToInt32(Request.QueryString["pkeyContact"]);
                pkeyRegister = Convert.ToInt32(Request.QueryString["fkeyRegister"]);
                LinkButton lbtn = (LinkButton)sender;
                int pkeyPhones = Convert.ToInt32(lbtn.CommandArgument);
                BusinessEntityModel model = new BusinessEntityModel();
                BusinessEntityModel.PhonesRow phonesRow = model.Phones.NewPhonesRow();
                phonesRow.deletekeys = pkeyPhones.ToString();
                phonesRow.phone_number = "";
                phonesRow.extension = "";
                phonesRow.contact_type = "";
                phonesRow.type_key = "-1";
                phonesRow.pkey_phones = -1;
                model.Phones.AddPhonesRow(phonesRow);
                model = BusinessEntitiesService.UpdatePhones(model, pkeyContact, ConnString);
                Response.Redirect("BusinessContactDetails.aspx?pkeyContact=" + pkeyContact + "&pkeyRegister=" + pkeyRegister);
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }            
        }

        protected void btnUpdatePhone_Click(object sender, EventArgs e)
        {
            try
            {
                BusinessEntityModel model = new BusinessEntityModel();
                BusinessEntityModel.PhonesRow phonesRow = model.Phones.NewPhonesRow();
                phonesRow.deletekeys = "-1";
                phonesRow.phone_number = hdnNumber.Value;
                phonesRow.extension = hdnExt.Value;
                phonesRow.contact_type = hdnType.Value;
                phonesRow.type_key = hdnValue.Value;
                phonesRow.pkey_phones = Convert.ToInt32(pkeyphones.Value);
                pkeyContact = Convert.ToInt32(Request.QueryString["pkeyContact"]);
                pkeyRegister = Convert.ToInt32(Request.QueryString["fkeyRegister"]);
                model.Phones.AddPhonesRow(phonesRow);
           
                model = BusinessEntitiesService.UpdatePhones(model, pkeyContact, ConnString);
                Response.Redirect("BusinessContactDetails.aspx?pkeyContact=" + pkeyContact + "&pkeyRegister=" + pkeyRegister);
            }
            catch (Exception ex)
            {
                AlertScript(ex.Message);
            }
        }
    }
}