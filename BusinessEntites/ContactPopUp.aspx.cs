﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;

namespace Bestprintbuy.Admin.Web.BusinessEntites
{
    public partial class ContactPopUp : PageBase
    {
        protected int pkeyContact = -1;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                GetData();
            }
        }

        private void GetData()
        {
            try
            {
                pkeyContact = Convert.ToInt32(Request.QueryString["pkeyContact"].Trim() == "" ? "-1" : Request.QueryString["pkeyContact"]);
                BusinessEntityModel model = BusinessEntitiesService.GetContactSummary(pkeyContact, ConnString);
                if (model.Phones.Rows.Count > 0)
                {
                    rptrContactSummary.DataSource = model.Phones;
                    rptrContactSummary.DataBind();
                }
                if (model.BusinessContacts.Rows.Count > 0)
                {
                    BusinessEntityModel.BusinessContactsRow row = (BusinessEntityModel.BusinessContactsRow)model.BusinessContacts.Rows[0];
                    lblContactID.Text = row.IsCONTACT_IDNull() ? "" : row.CONTACT_ID;
                    lblFirstName.Text = row.IsFIRST_NAMENull() ? "" : row.FIRST_NAME;
                    lblLastName.Text = row.IsLAST_NAMENull() ? "" : row.LAST_NAME;
                    lblTitle.Text = row.IsTITLENull() ? "" : row.TITLE;
                    lblSalutation.Text = row.IsSALUTATIONNull() ? "" : row.SALUTATION == "-1" ? "" : row.SALUTATION;
                    chkPrincContact.Checked = Convert.ToBoolean(row.IS_PRINCIPAL_CONTACT);
                    lblAddress1.Text = row.IsADDRESS1Null() ? "" : row.ADDRESS1;
                    lblAddress2.Text = row.IsADDRESS2Null() ? "" : row.ADDRESS2;
                    lblCity.Text = row.IsCITYNull() ? "" : row.CITY;
                    lblState.Text = row.IsSTATE_NAMENull() ? "" : row.STATE_NAME;
                    lblPostalCode.Text = row.IsPOSTAL_CODENull() ? "" : row.POSTAL_CODE;
                    lblCountry.Text = row.IsCOUNTRY_NAMENull() ? "" : row.COUNTRY_NAME;
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }
    }
}