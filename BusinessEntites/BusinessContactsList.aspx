﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="BusinessContactsList.aspx.cs" Inherits="Bestprintbuy.Admin.Web.BusinessEntites.BusinessContactsList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="clr"></div>
    <!-- Container Starts -->
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Business Partners Setup</li>
            <li>Business Contacts List </li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Business Contacts List </h2>
        <form id="frmBusinessContactsList" name="frmBusinessContactsList" runat="server" class="form">           
            <div class="clr"></div>
            <table class="table">
                <thead>
                    <tr>
                        <th>#SL</th>
                        <th>Contact ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Title</th>
                        <th>Salutation</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="rptrBusinessContacts" runat="server">
                        <ItemTemplate>
                            <tr>
                                 <td>
                                    <%#Container.ItemIndex+1 %>
                                </td>
                                <td>
                                    <a href="BusinessContactDetails.aspx?pkeyContact=<%#Eval("PKEY_BR_CONTACT") %>&fkeyRegister=<%= fkeyRegister %>" data-gravity="s" title="Edit"><%#Eval("CONTACT_ID") %></a>
                                </td>
                                <td>
                                    <a href="BusinessContactDetails.aspx?pkeyContact=<%#Eval("PKEY_BR_CONTACT") %>&fkeyRegister=<%= fkeyRegister %>" data-gravity="s" title="Edit"><%#Eval("FIRST_NAME") %></a>
                                </td>
                                <td>
                                    <%#Eval("LAST_NAME") %>
                                </td>
                                <td>
                                    <%#Eval("TITLE") %>
                                </td>
                                <td>
                                    <%# Eval("SALUTATION") %>
                                </td>
                                <td style="text-align: center">
                                    <asp:LinkButton ID="lbtnDelete" runat="server" CommandName="Delete" CausesValidation="true" OnClientClick="return confirm('This results in permanent removal of Contact, are you sure you want to delete the contact?')"
                                        ToolTip="Delete" CommandArgument='<%#Bind("PKEY_BR_CONTACT")%>' OnClick="lbtnDelete_Click"><img src="../images/trashcan.png" /></asp:LinkButton>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
            <div class="btn_lst">
                <div class="lft">
                    <asp:Button runat="server" ID="btnAddNewContact" CssClass="button rounded" Text="Add A New Contact" OnClick="btnAddNewContact_Click"></asp:Button>
                </div>
                <div>
                    <asp:Button ID="btnClose" Text="Close" CssClass="button rounded" runat="server" OnClick="btnClose_Click"></asp:Button>
                </div>
            </div>
        </form>
    </div>

</asp:Content>
