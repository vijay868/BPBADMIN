﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.BusinessEntites
{
    public partial class BusinessAttributes : PageBase
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    BindAttributes();
                }
                catch (Exception ex)
                {
                    FileLogger.WriteException(ex);
                    AlertScript(ex.Message);
                }
            }

        }

        private void BindAttributes()
        {
            try
            {
                int l_pkeyMasterCategory = -1;
                int l_pkeyRegister = Convert.ToInt32(Request.QueryString["pkeyRegister"] != null ? Request.QueryString["pkeyRegister"] : "-1");
                l_pkeyMasterCategory = GetMasterCatKey("sup");
                BusinessEntityModel model = CartService.GetList(l_pkeyRegister, l_pkeyMasterCategory, ConnString);
                ViewState["BusinessEntity"] = model;
                AttributeGrid.DataSource = model.BusAttributes;
                AttributeGrid.DataBind();

                foreach (GridViewRow row in AttributeGrid.Rows)
                {
                    DropDownList ddlCategory = (DropDownList)row.FindControl("ddlCategory");
                    DropDownList ddlType = (DropDownList)row.FindControl("ddlType");
                    BindCategory(ddlCategory);
                    BindType(ddlType);
                    ddlCategory.SelectedValue = model.BusAttributes[0].pkeycategory.ToString();
                    ddlType.SelectedValue = model.BusAttributes[0].pkeytype.ToString();
                }
            }
            catch (Exception ex)
            {
                AlertScript(ex.Message);
            }
        }

        private void addNewRow()
        {
            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt.Columns.Add("Category");
                dt.Columns.Add("Type");
                dt.Columns.Add("PKEY_ATTRIBUTE");

                List<KeyValuePair<string, string>> category = null;
                List<KeyValuePair<string, string>> type = null;
                string strCategory = "";
                string strType = "";
                for (int i = 0; i < AttributeGrid.Rows.Count; i++)
                {
                    DropDownList ddlCategory = (DropDownList)AttributeGrid.Rows[i].FindControl("ddlCategory");
                    DropDownList ddlType = (DropDownList)AttributeGrid.Rows[i].FindControl("ddlType");
                    strCategory = ddlCategory.SelectedValue;
                    strType = ddlType.SelectedValue;
                    dt.Rows.Add(new string[] { ddlCategory.SelectedValue, ddlType.SelectedValue, "" });
                    if (category == null)
                        category = GetItems(ddlCategory);
                    if (type == null)
                        type = GetItems(ddlType);
                }

                System.Data.DataRow dr = dt.NewRow();
                dr[0] = dr[1] = 0;
                dr[2] = -2;
                dt.Rows.Add(dr);
                AttributeGrid.DataSource = dt;
                AttributeGrid.DataBind();

                for (int i = 0; i < AttributeGrid.Rows.Count; i++)
                {
                    DropDownList ddlCategory = (DropDownList)AttributeGrid.Rows[i].FindControl("ddlCategory");
                    DropDownList ddlType = (DropDownList)AttributeGrid.Rows[i].FindControl("ddlType");

                    if (category == null)
                        BindCategory(ddlCategory);
                    else
                        BindDropdown(ddlCategory, category, "");

                    if (dt.Rows[i]["Category"].ToString() != "")
                        ddlCategory.SelectedValue = dt.Rows[i]["Category"].ToString();
                    else
                        ddlCategory.SelectedIndex = 0;

                    if (type != null && ddlCategory.SelectedValue != "-1")
                        BindDropdown(ddlType, type, "");
                    else { ddlType.Items.Insert(0, new ListItem("No Option", "-1")); }

                    if (dt.Rows[i]["Type"].ToString() != "")
                        ddlType.SelectedValue = dt.Rows[i]["Type"].ToString();
                    else
                        ddlType.SelectedIndex = 0;
                }
            }
            catch (Exception ex) 
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message); 
            }
        }

        protected void btnAddAttribute_Click(object sender, EventArgs e)
        {
            try
            {
                addNewRow();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            BusinessEntityModel model = new BusinessEntityModel();
            try
            {
                for (int i = 0; i < AttributeGrid.Rows.Count; i++)
                {
                    BusinessEntityModel.BusAttributesRow row = model.BusAttributes.NewBusAttributesRow();

                    DropDownList ddlCategory = (DropDownList)AttributeGrid.Rows[i].FindControl("ddlCategory");
                    DropDownList ddlType = (DropDownList)AttributeGrid.Rows[i].FindControl("ddlType");
                    HiddenField hdnPKEYATTRIBUTE = (HiddenField)AttributeGrid.Rows[i].FindControl("hdnPKEYATTRIBUTE");

                    row.pkey_attribute = Convert.ToInt32(hdnPKEYATTRIBUTE.Value == "" ? "-2" : hdnPKEYATTRIBUTE.Value);
                    row.fkey_register = Convert.ToInt32(Request.QueryString["pkeyRegister"] != null ? Request.QueryString["pkeyRegister"] : "-1");
                    row.pkeytype = Convert.ToInt32(ddlType.SelectedValue);
                    row.fkey_user = Identity.UserPK;
                    model.BusAttributes.AddBusAttributesRow(row);
                }

                int key = CartService.Update(model, ConnString);
            }
            catch (Exception ex)
            {
                AlertScript(ex.Message);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)sender;
                string strcomm = btn.CommandArgument;
                if (strcomm != "-2")
                {
                    CartService.Delete(strcomm, ConnString);
                    addNewRow();
                }
                else { AlertScript("please select valid item."); }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        private void BindType(DropDownList ddlType)
        {
            BusinessEntityModel model = (BusinessEntityModel)ViewState["BusinessEntity"];
            ddlType.DataSource = model.AttributeTypes;
            ddlType.DataTextField = "ct_name";
            ddlType.DataValueField = "pkey_category_type";
            ddlType.DataBind();
            ddlType.Items.Insert(0, new ListItem("No Option", "-1"));
        }

        private void BindCategory(DropDownList ddlCategory)
        {
            BusinessEntityModel model = (BusinessEntityModel)ViewState["BusinessEntity"];
            ddlCategory.DataSource = model.AttributeCategories;
            ddlCategory.DataTextField = "ct_name";
            ddlCategory.DataValueField = "pkey_category_type";
            ddlCategory.DataBind();
            ddlCategory.Items.Insert(0, new ListItem("No Option", "-1"));
        }

        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow gvr = (GridViewRow)((DropDownList)sender).Parent.Parent;
                DropDownList ddl = (DropDownList)sender;
                DropDownList ddlType = gvr.FindControl("ddlType") as DropDownList;
                BindType(ddlType);
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }           
        }

        protected void AttributeGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }
    }
}