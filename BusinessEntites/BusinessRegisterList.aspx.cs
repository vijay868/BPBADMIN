﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;

namespace Bestprintbuy.Admin.Web.BusinessEntites
{
    public partial class BusinessRegisterList : PageBase
    {
        protected int pkeyType = -1;
        protected int pkeyCategory = -1;
        protected int pkeyMasterCat = 1;
        protected int isSupplier = 0;
        protected string searchfor = "-1";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                try
                {
                    Session.Remove("pkeyMasterCat");
                    pkeyMasterCat = GetMasterCatKey(Request.QueryString["bid"]);
                    if (pkeyMasterCat == 2)
                        lblbreadcrumb.Text = lblheader.Text = "Supplier / Vendors List";
                    else if (pkeyMasterCat == 13)
                        lblbreadcrumb.Text = lblheader.Text = "Freight / Shipping Companies";
                    else if (pkeyMasterCat == 3)
                        lblbreadcrumb.Text = lblheader.Text = "Co Branded Business Partners";
                    else if (pkeyMasterCat == 10)
                        lblbreadcrumb.Text = lblheader.Text = "B2B Affiliates";
                    else if (pkeyMasterCat == 1)
                        lblbreadcrumb.Text = lblheader.Text = "Business Register Entity";

                    GetData();
                }
                catch (Exception ex)
                {
                    FileLogger.WriteException(ex);
                    AlertScript(ex.Message);
                }
            }
        }

        private void GetData()
        {
            pkeyMasterCat = GetMasterCatKey(Request.QueryString["bid"]);
            pkeyType = Convert.ToInt32(ddltypes.SelectedValue == "" ? "-1" : ddltypes.SelectedValue);
            BusinessEntityModel model = BusinessEntitiesService.GetList(-1, pkeyType, pkeyCategory, searchfor, pkeyMasterCat, isSupplier, ConnString);
            lblTotalRecords.Text = string.Format(" Total Number Business Entity Register Entries : {0}", model.BusinessEntity.Rows.Count);
            if (model.BusinessTypes.Rows.Count > 0)
            {
                ddltypes.DataSource = model.BusinessTypes;
                ddltypes.DataTextField = "ct_name";
                ddltypes.DataValueField = "pkey_category_type";
                ddltypes.DataBind();
                ddltypes.Items.Insert(0, new ListItem("No Filter", "-1"));
            }
            if (model.BusinessEntity.Rows.Count > 0)
            {
                rptrBusinessRegList.DataSource = model.BusinessEntity;
                rptrBusinessRegList.DataBind();
            }
        }

        private int GetMasterCatKey(string strID)
        {
            switch (strID)
            {
                case "sup":
                    //Suppliers
                    return 2;
                case "mer":
                    //Merchant Association - now not being used
                    return 4;
                case "pat":
                    //Co-Branded Business Partners  
                    return 3;
                case "afl":
                    //B2B Affiliates   
                    return 10;
                case "bur":
                    //Business Category Register 
                    return 1;
                case "ship":
                    //Freight Classification and Delivery
                    return 13;
                case "usr":
                    //incase of admin user
                    return -1;
                default:
                    //Business Category Register 
                    return 1;
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/DashBoard.aspx");
        }

        protected void ddltypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GetData();
                ddltypes.SelectedValue = pkeyType.ToString();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
            
        }

        protected void btnAddNewBusiness_Click(object sender, EventArgs e)
        {
            pkeyMasterCat = GetMasterCatKey(Request.QueryString["bid"]);
            Response.Redirect("BusinessRegisterDetail.aspx?pkey=-2&bid=" + pkeyMasterCat);
        }

    }
}