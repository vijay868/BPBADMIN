﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;

namespace Bestprintbuy.Admin.Web.BusinessEntites
{
    public partial class BusinessContactsList : PageBase
    {
        protected int fkeyRegister = -1;
        protected int ContactKey = -1;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                try
                {
                    GetData();
                }
                catch (Exception ex)
                {
                    FileLogger.WriteException(ex);
                    AlertScript(ex.Message);
                }               
            }
        }

        private void GetData()
        {
            fkeyRegister = Convert.ToInt32(Request.QueryString["pkeyRegister"]);
            BusinessEntityModel model = BusinessEntitiesService.GetData(ContactKey, fkeyRegister, ConnString);
            rptrBusinessContacts.DataSource = model.BusinessContacts;
            rptrBusinessContacts.DataBind();
        }

        protected void btnAddNewContact_Click(object sender, EventArgs e)
        {
            fkeyRegister = Convert.ToInt32(Request.QueryString["pkeyRegister"]);
            Response.Redirect("BusinessContactDetails.aspx?pkeyContact=-2&fkeyRegister=" + fkeyRegister);
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            fkeyRegister = Convert.ToInt32(Request.QueryString["pkeyRegister"]);
            Response.Redirect("BusinessRegisterView.aspx?pkey=" + fkeyRegister);
        }

        protected void lbtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbtn = (LinkButton)sender;
                int pkeyContact = Convert.ToInt32(lbtn.CommandArgument);
                BusinessEntitiesService.DeleteBusinessContact(pkeyContact, ConnString);
                GetData();
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }            
        }
    }
}