﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="BusinessRegisterDetail.aspx.cs" Inherits="Bestprintbuy.Admin.Web.BusinessEntites.BusinessRegisterDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
        $().ready(function () {
            $("#frmBusinessView").validate();
        });

        function PrincContactDailog() {
            var pkeyregister = $("#ContentPlaceHolder1_hdnPrincContact").val();
            var url = "../BusinessEntites/SelectBusinessContact.aspx?pkeyregister=" + pkeyregister;
            $.fancybox({
                'width': '70%',
                'height': 'auto',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': url,
                'type': 'iframe'
            });
            return false;
        }

        function PrincContactSummaryDailog() {
            if ($("#ContentPlaceHolder1_lblPrincContact").text() == "") {
                alert("Please select a Principal Contact.");
                return false;
            }

            var pkeyContact = $("#ContentPlaceHolder1_hdnPkeyContact").val() != "" ? $("#ContentPlaceHolder1_hdnPkeyContact").val() : "<%= PrincContact%>";
            var url = "../BusinessEntites/ContactPopUp.aspx?pkeyContact=" + pkeyContact;
            $.fancybox({
                'width': '60%',
                'height': 'auto',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': url,
                'type': 'iframe'
            });
            return false;
        }

        function contactInfo(email, ext, type, pkay) {
            $("#ContentPlaceHolder1_txtNumberID").val(email);
            $("#ContentPlaceHolder1_txtExtension").val(ext);
            $("#ContentPlaceHolder1_pkeyphones").val(pkay);
            if (type != '')
                $("#ContentPlaceHolder1_ddlFormat option:contains(" + type + ")").attr('selected', 'selected');
            else
                $("#ContentPlaceHolder1_ddlFormat").val("OP");

            $.fancybox({
                'width': '100%',
                'height': 'auto',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': '#dialog_Contact_info',
                'type': 'inline'
            });
            return false;
        }

        function SetContactData(PkeyContact, FirstName, LastName) {
            $("#ContentPlaceHolder1_hdnPkeyContact").val(PkeyContact);
            $("#ContentPlaceHolder1_hdnFirstName").val(FirstName);
            $("#ContentPlaceHolder1_hdnLastName").val(LastName);
            $("#ContentPlaceHolder1_lblPrincContact").text(FirstName + " " + LastName);
            parent.$.fancybox.close();
        }

        function setValues() {
            $("#ContentPlaceHolder1_hdnNumber").val($("#ContentPlaceHolder1_txtNumberID").val());
            $("#ContentPlaceHolder1_hdnExt").val($("#ContentPlaceHolder1_txtExtension").val());
            $("#ContentPlaceHolder1_hdnType").val($("#ContentPlaceHolder1_ddlFormat").val());
            $("#ContentPlaceHolder1_hdnValue").val($("#ContentPlaceHolder1_ddlFormat option:selected").text());
            return true;
        }

    </script>
    <style type="text/css">
        #dialog_Contact_info {
            overflow: hidden;
        }
    </style>
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Business Partners Setup</li>
            <li>
                <asp:Label ID="lblbreadcrumb" runat="server"></asp:Label></li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>
            <asp:Label ID="lblheader" runat="server"></asp:Label></h2>
        <form id="frmBusinessView" name="frmBusinessView" runat="server">
            <table width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="15%">
                            <label>Business ID*</label></td>
                        <td width="20%">
                            <asp:TextBox CssClass="text-box" name="txtBusinessID" ID="txtBusinessID" runat="server" required></asp:TextBox>
                        </td>
                        <td width="15%">
                            <label>Prefix </label>
                        </td>
                        <td width="35%">
                            <asp:TextBox CssClass="text-box" name="txtPrefix" ID="txtPrefix" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Name*</label></td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtName" ID="txtName" runat="server" required></asp:TextBox>
                        </td>
                        <td>
                            <label>Other Names </label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtOtherNames" ID="txtOtherNames" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label></label>
                        </td>
                        <td>
                            <asp:RadioButton ID="rbtnBusiness" runat="server" value="optBusiness" GroupName="Business" /><asp:Label ID="Label3" runat="server" Text="  Business"></asp:Label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:RadioButton ID="rbtnIndividual" runat="server" value="optIndividual" GroupName="Business" /><asp:Label ID="Label4" runat="server" Text="  Individual"></asp:Label>
                        </td>
                        <td style="text-align: center;">
                            <asp:Button runat="server" ID="btnPrimcContactDetails" CssClass="button rounded" Text="Prin Contact" OnClientClick="PrincContactSummaryDailog();  return false;"></asp:Button>
                            <asp:HiddenField ID="hdnPrincContact" runat="server" />
                            <asp:Button runat="server" ID="btnPrincContact" CssClass="button rounded" Text="..." OnClientClick="PrincContactDailog();  return false;"></asp:Button>
                        </td>
                        <td>
                            <asp:Label ID="lblPrincContact" runat="server" Font-Size="14px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">
                            <label>Address </label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtAddress1" ID="txtAddress1" runat="server"></asp:TextBox>
                            <asp:TextBox CssClass="text-box" name="txtAddress2" ID="txtAddress2" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <label>City </label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtCity" ID="txtCity" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Country </label>
                        </td>
                        <td>
                            <asp:DropDownList name="ddlCountry" ID="ddlCountry" CssClass="slect-box" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <label>State </label>
                        </td>
                        <td>
                            <asp:DropDownList name="ddlState" ID="ddlState" CssClass="slect-box" runat="server">
                                <asp:ListItem Text="No Option" Value="-1" />
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Postal Code </label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtPostalCode" ID="txtPostalCode" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <label>Category*</label></td>
                        <td>
                            <asp:DropDownList name="ddlCategory" ID="ddlCategory" CssClass="slect-box" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged" required>
                                <asp:ListItem Text="No Option" Value="" />
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Type*</label>
                        </td>
                        <td>
                            <asp:DropDownList name="ddlType" ID="ddlType" CssClass="slect-box" runat="server" required>
                                <asp:ListItem Text="No Option" Value="" />
                            </asp:DropDownList>
                        </td>
                        <td>
                            <label>Tax ID </label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtTaxID" ID="txtTaxID" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Tax Type</label>
                        </td>
                        <td>
                            <asp:RadioButton ID="rbtnBusinessTaxType" runat="server" value="optBusinessTax" GroupName="TaxType" /><asp:Label ID="Label1" runat="server" Text="  Business"></asp:Label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:RadioButton ID="rbtnIndividualTaxType" runat="server" value="optIndividualTax" GroupName="TaxType" /><asp:Label ID="Label2" runat="server" Text="  Individual"></asp:Label>
                        </td>
                        <td>
                            <label></label>
                        </td>
                        <td></td>
                    </tr>
                </tbody>
            </table>


            <table width="100%" class="accounts-table" id="fSetPhone" visible="false" runat="server">
                <thead>
                    <tr>
                        <th colspan="4">Phones And Emails
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>           
            <div class="clr"></div>
                            <table class="tabled" width="100%">
                                <thead>
                                    <tr>
                                        <th>Number / ID</th>
                                        <th>Extension</th>
                                        <th>Type</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rptrPhones" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <%#Eval("phone_number") %>
                                                </td>
                                                <td>
                                                    <%#Eval("extension") %>
                                                </td>
                                                <td>
                                                    <%#Eval("contact_type") %>
                                                </td>
                                                <td>
                                                    <a href="#" onclick="contactInfo('<%#Eval("phone_number") %>', '<%#Eval("extension") %>', '<%#Eval("contact_type") %>', '<%#Eval("pkey_phones") %>');" title="Edit">
                                                        <img width="16" height="16" src="../images/pencil.png" /></a>
                                                    <asp:LinkButton ID="lbtnDelete" runat="server" CommandName="Delete" CausesValidation="true"
                                                        ToolTip="Delete" CommandArgument='<%#Bind("pkey_phones")%>'
                                                        OnClick="lbtnDelete_Click"><img src="../images/trashcan.png" /></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                            <table class="accounts-table">
                                <tbody>
                                    <tr>
                                        <td style="float: right;">
                                            <asp:HiddenField ID="pkeyphones" runat="server" />
                                            <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="button rounded" OnClientClick="javascript:contactInfo('','','','-2'); return false;" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>

            <table width="100%" class="accounts-table">
                <thead>
                    <tr>
                        <th colspan="4">Transaction Limits</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="15%">
                            <label>Credit Per Transaction($)</label></td>
                        <td width="20%">
                            <asp:TextBox CssClass="text-box number" name="txtCreditPerTransaction" ID="txtCreditPerTransaction" runat="server"></asp:TextBox>
                        </td>
                        <td width="15%">
                            <label>Credit Period(Days) ($) </label>
                        </td>
                        <td width="35%">
                            <asp:TextBox CssClass="text-box number" name="txtCreditPeriod" ID="txtCreditPeriod" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Cut off Period </label>
                        </td>
                        <td>
                            <asp:DropDownList name="ddlCutoffPeriod" ID="ddlCutoffPeriod" CssClass="slect-box" runat="server">
                                <asp:ListItem Value="-1" Selected="True">Select Period</asp:ListItem>
                                <asp:ListItem Value="7">Week</asp:ListItem>
                                <asp:ListItem Value="15">Fortnight</asp:ListItem>
                                <asp:ListItem Value="30">Month</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <label>Cummulative Credit($) </label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box number" name="txtCumilativeCredit" ID="txtCumilativeCredit" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Currency </label>
                        </td>
                        <td>
                            <asp:DropDownList name="ddlCurrency" ID="ddlCurrency" CssClass="slect-box" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <label></label>
                        </td>
                        <td>
                            <asp:CheckBox ID="chkWarnonCredit" runat="server" CssClass="check-box" /><asp:Label runat="server" Text="   Warn on credit violation"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button runat="server" ID="btnDelete" Text="Delete" CssClass="button rounded" OnClick="btnDelete_Click" Visible="false" OnClientClick="return confirm('The action results in permenant removal of business entry, are you sure to remove?')" ></asp:Button></td>
                        <td></td>
                        <td></td>
                        <td style="float: right;">
                            <asp:Button runat="server" ID="btnSave" Text="Save" OnClick="btnSave_Click" CssClass="button rounded"></asp:Button>
                            <asp:Button runat="server" ID="btnClose" Text="Close" OnClick="btnClose_Click" CssClass="button rounded" UseSubmitBehavior="false"></asp:Button>
                            <asp:HiddenField ID="hdnPkeyContact" runat="server" />
                            <asp:HiddenField ID="hdnFirstName" runat="server" />
                            <asp:HiddenField ID="hdnLastName" runat="server" />
                            <asp:HiddenField ID="hdnfkey_reg_prn_contact" runat="server" />
                            <asp:HiddenField ID="hdnNumber" runat="server" />
                            <asp:HiddenField ID="hdnExt" runat="server" />
                            <asp:HiddenField ID="hdnType" runat="server" />
                            <asp:HiddenField ID="hdnValue" runat="server" />
                        </td>
                    </tr>
                </tbody>
            </table>
            <div style="display: none;" id="dialog_Contact_info">
                <div id="Div1" class="popup-wrapper" runat="server">
                    <div class="popup-title">
                        Phones And Emails
                    </div>
                    <!-- Header Ends -->
                    <div class="popup-contnt">
                        <!-- Menu starts -->
                        <table>
                            <tr>
                                <td width="40%">
                                    <label>Number / ID</label></td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtNumberID" CssClass="text"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Ext / Cc</label></td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtExtension" CssClass="text"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Type</label></td>
                                <td>
                                    <asp:DropDownList name="ddlFormat" ID="ddlFormat" CssClass="select-box" runat="server">
                                        <asp:ListItem Value="OP" Selected>Office Phone</asp:ListItem>
                                        <asp:ListItem Value="HP">Home Phone</asp:ListItem>
                                        <asp:ListItem Value="MP">Mobile Phone</asp:ListItem>
                                        <asp:ListItem Value="FA">Fax</asp:ListItem>
                                        <asp:ListItem Value="EM">Email</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <asp:Button runat="server" CssClass="button rounded" ID="btnUpdatePhone" Text="Save" UseSubmitBehavior="false" OnClick="btnUpdatePhone_Click" OnClientClick="setValues();"></asp:Button></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </form>
    </div>
</asp:Content>
