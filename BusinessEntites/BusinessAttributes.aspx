﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BusinessAttributes.aspx.cs" Inherits="Bestprintbuy.Admin.Web.BusinessEntites.BusinessAttributes" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Best Print Buy</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" media="all" href="../css/bestprintbuy_style.css" />
    <link rel="stylesheet" media="all" href="../css/jquery.fancybox.css" />
    <link rel="stylesheet" href="../css/jquery-ui.css" />
    <link rel="stylesheet" href="../css/Style.css" />
    <script src="../Scripts/jquery.js"></script>
    <script src="../Scripts/jquery-ui.js"></script>
    <script src="../Scripts/common.js"></script>
    <script src="../Scripts/ddsmoothmenu.js"></script>
    <script src="../Scripts/plugin/fancybox/jquery.fancybox.js"></script>
    <script src="../Scripts/jquery.validate.js"></script>
    <!--[if lt IE 9]>
			<script src="../Scripts/html5.js"></script>
		<![endif]-->
    <!--[if lte IE 7]> <html class="ie7"> <![endif]-->
    <!--[if IE 8]>     <html class="ie8"> <![endif]-->
    <!--[if IE 9]>     <html class="ie9"> <![endif]-->
    <!--[if IE 10]>     <html class="ie10"> <![endif]-->
    <!--[if !IE]><!-->
    <!--<![endif]-->
    <script>
        function actionSave() {
            var flag = true;
            var dropDownControls = $('#<%=AttributeGrid.ClientID %> select option:selected');
            for (index = 0; index < dropDownControls.length; index++) {
                if (dropDownControls[index].value == -1) {
                    flag = false;
                    break;
                }
            }
            if (!flag) {
                alert("Please select option");
            }
            return flag;
        }
    </script>
</head>
<body id="Body" runat="server">
    <!-- Wrapper starts -->
    <div id="Div1" class="popup-wrapper" runat="server">
        <div class="popup-title">
            Business Register Attributes for : Print Portal
        </div>
        <!-- Header Ends -->
        <div class="popup-contnt">
            <!-- Menu starts -->
            <form id="Form1" runat="server">           
            <div class="clr"></div>
                <asp:GridView ID="AttributeGrid" runat="server" AutoGenerateColumns="false" EnableViewState="true"
                    ForeColor="Black" Width="100%" 
                    CssClass="tablep" name="TemplateListGrid" OnRowDeleting="AttributeGrid_RowDeleting">
                    <Columns>
                        <asp:TemplateField HeaderText="Category">
                            <ItemTemplate>
                                <asp:DropDownList runat="server" ID="ddlCategory" CssClass="select-box" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                                <asp:HiddenField ID="hdnPKEYATTRIBUTE" runat="server" Value='<%# Eval("PKEY_ATTRIBUTE") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Type">
                            <ItemTemplate>
                                <asp:DropDownList runat="server" ID="ddlType" CssClass="select-box">
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Action">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnDelete" runat="server" CommandName="Delete" CausesValidation="true"
                                    ToolTip="Delete" CommandArgument='<%#Bind("PKEY_ATTRIBUTE")%>' OnClientClick="return confirm('Results in deletion of selected record(s). Do you want to continue?')"
                                    OnClick="btnDelete_Click"><img src="../images/trashcan.png" /></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <table>
                    <tr>
                        <td style="float: left;">
                            <asp:Button runat="server" ID="btnAddAttribute" CssClass="button rounded" Text="Add Attribute" OnClick="btnAddAttribute_Click"></asp:Button>
                        </td>
                        <td style="float: right;">
                            <asp:Button runat="server" ID="btnSelect" CssClass="button rounded" Text="Save" OnClick="btnSave_Click" OnClientClick="return actionSave();"></asp:Button>
                            <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClientClick="parent.$.fancybox.close(); return false;"></asp:Button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</body>
</html>
