﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;

namespace Bestprintbuy.Admin.Web.BusinessEntites
{
    public partial class BusinessRegisterView : PageBase
    {
        protected int pkey_register = -2;
        protected int pkeyType = -1;
        protected int pkeyCategory = -1;
        protected int pkeyMasterCat = 1;
        protected int isSupplier = 0;
        protected string searchfor = "-1";
        protected string pkeyMastertext = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                try
                {
                    if (Session["pkeyMasterCat"] == null)
                    {
                        pkeyMasterCat = Convert.ToInt32(Request.QueryString["bid"]);
                        Session["pkeyMasterCat"] = pkeyMasterCat;
                    }
                    else
                        pkeyMasterCat = Convert.ToInt32(Session["pkeyMasterCat"]);
                    if (pkeyMasterCat == 2)
                        lblbreadcrumb.Text = lblheader.Text = tbheader.Text = "Supplier / Vendors View";
                    else if (pkeyMasterCat == 13)
                        lblbreadcrumb.Text = lblheader.Text = tbheader.Text = "Freight / Shipping Company View";
                    else if (pkeyMasterCat == 3)
                        lblbreadcrumb.Text = lblheader.Text = tbheader.Text = "Co Branded Business Partner View";
                    else if (pkeyMasterCat == 10)
                        lblbreadcrumb.Text = lblheader.Text = tbheader.Text = "B2B Affiliate View";
                    else if (pkeyMasterCat == 1)
                        lblbreadcrumb.Text = lblheader.Text = tbheader.Text = "Business Register Entity View";
                    GetData();
                }
                catch (Exception ex)
                {
                    FileLogger.WriteException(ex);
                    AlertScript(ex.Message);
                }                
            }
        }

        private void GetData()
        {
            pkey_register = Convert.ToInt32(Request.QueryString["pkey"]);
            BusinessEntityModel model = BusinessEntitiesService.GetList(pkey_register, pkeyType, pkeyCategory, searchfor, pkeyMasterCat, isSupplier, ConnString);

            if (model.ChildScreens.Rows.Count > 0)
            {
                rptrChildScreens.DataSource = model.ChildScreens;
                rptrChildScreens.DataBind();
            }
            if (model.Phones.Rows.Count > 0)
            {
                rptrPhones.DataSource = model.Phones;
                rptrPhones.DataBind();
            }
            if (model.BusinessEntity.Rows.Count > 0)
            {
                BusinessEntityModel.BusinessEntityRow row = (BusinessEntityModel.BusinessEntityRow)model.BusinessEntity.Rows[0];
                lblBusinessID.Text = row.Isbusiness_idNull() ? "" : row.business_id;
                lblPrefix.Text = row.IsprefixNull() ? "" : row.prefix;
                lblName.Text = row.Isbusiness_nameNull() ? "" : row.business_name;
                lblOtherNames.Text = row.Isother_namesNull() ? "" : row.other_names;
                lblAddress1.Text = row.Isaddress1Null() ? "" : row.address1;
                lblAddress2.Text = row.Isaddress2Null() ? "" : row.address2;
                lblCity.Text = row.IscityNull() ? "" : row.city;
                lblState.Text = row.Isstate_nameNull() ? "" : row.state_name;
                lblPostalCode.Text = row.IszipcodeNull() ? "" : row.zipcode;
                lblCountry.Text = row.Iscountry_nameNull() ? "" : row.country_name;
                lblCategory.Text = row.IsbusinesscategoryNull() ? "" : row.businesscategory;
                lblType.Text = row.IsbusinesstypeNull() ? "" : row.businesstype;
                lblTaxID.Text = row.Istax_idNull() ? "" : row.tax_id;
                if (row.is_business)
                    rbtnBusiness.Checked = true;
                else
                    rbtnIndividual.Checked = true;
                if (row.tax_type)
                    rbtnBusinessTaxType.Checked = true;
                else
                    rbtnIndividualTaxType.Checked = true;
                lblCreditPerTransaction.Text = row.Iscredit_per_transactionNull() ? "" : row.credit_per_transaction.ToString();
                lblCumilativeCredit.Text = row.Iscumulative_creditNull() ? "" : row.cumulative_credit.ToString();
                lblCurrency.Text = row.Iscurrency_nameNull() ? "" : row.currency_name;
            }
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            Response.Redirect("BusinessRegisterViewPrint.aspx?pkey=" + Request.QueryString["pkey"] + "&bid=" + Request.QueryString["bid"]);
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            pkeyMasterCat = Convert.ToInt32(Request.QueryString["bid"]);
            pkey_register = Convert.ToInt32(Request.QueryString["pkey"]);
            Response.Redirect("BusinessRegisterDetail.aspx?pkey=" + pkey_register + "&bid=" + pkeyMasterCat);
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            pkeyMastertext = GetMasterCatText(Session["pkeyMasterCat"].ToString());
            Session.Remove("pkeyMasterCat");
            Response.Redirect("BusinessRegisterList.aspx?bid=" + pkeyMastertext);
        }

        private string GetMasterCatText(string strID)
        {
            switch (strID)
            {
                case "2":
                    return "sup";
                case "4":
                    return "mer";
                case "3":
                    return "pat";
                case "10":
                    return "afl";
                case "1":
                    return "bur";
                case "13":
                    return "ship";
                case "-1":
                    return "usr";
                default:
                    return "bur";
            }
        }
    }
}