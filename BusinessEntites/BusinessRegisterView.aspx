﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="BusinessRegisterView.aspx.cs" Inherits="Bestprintbuy.Admin.Web.BusinessEntites.BusinessRegisterView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function OpenContactDialog(childscreen, pkeyRegister) {
            var url = "";
            if (childscreen == "Attributes")
                url = "BusinessAttributes.aspx?pkeyRegister=" + pkeyRegister;
            else if (childscreen == "Contacts") {
                window.location.assign("BusinessContactsList.aspx?pkeyRegister=" + pkeyRegister);
                return false;
            }
            else if (childscreen == "Transactions")
                url = "BusinessTransactionsList.aspx?pkeyRegister=" + pkeyRegister;
            $.fancybox({
                'width': '70%',
                'height': 'auto',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': url,
                'type': 'iframe'
            });
            return false;
        }
    </script>
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Business Partners Setup</li>
            <li>
                <asp:Label ID="lblbreadcrumb" runat="server"></asp:Label></li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>
            <asp:Label ID="lblheader" runat="server"></asp:Label></h2>
        <form id="frmBusinessView" name="frmBusinessView" runat="server">
            <table width="100%" class="accounts-table">
                <thead>
                    <tr>
                        <th colspan="4">
                            <asp:Label ID="tbheader" runat="server"></asp:Label>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="15%">
                            <label>Business ID</label></td>
                        <td width="20%">
                            <asp:Label ID="lblBusinessID" runat="server" Font-Size="14px"></asp:Label>
                        </td>
                        <td width="15%">
                            <label>Prefix </label>
                        </td>
                        <td width="35%">
                            <asp:Label ID="lblPrefix" runat="server" Font-Size="14px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Name</label></td>
                        <td>
                            <asp:Label ID="lblName" runat="server" Font-Size="14px"></asp:Label></td>
                        <td>
                            <label>Other Names </label>
                        </td>
                        <td>
                            <asp:Label ID="lblOtherNames" runat="server" Font-Size="14px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <label></label>
                        </td>
                        <td>
                            <asp:RadioButton ID="rbtnBusiness" runat="server" value="optBusiness" GroupName="Business" Enabled="false" /><asp:Label ID="Label3" runat="server" Text="  Business"></asp:Label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:RadioButton ID="rbtnIndividual" runat="server" value="optIndividual" GroupName="Business" Enabled="false" /><asp:Label ID="Label4" runat="server" Text="  Individual"></asp:Label>
                        </td>
                        <td>
                            <label>Address</label></td>
                        <td>
                            <asp:Label ID="lblAddress1" runat="server" Font-Size="14px"></asp:Label></br>
                        <asp:Label ID="lblAddress2" runat="server" Font-Size="14px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <label>City </label>
                        </td>
                        <td>
                            <asp:Label ID="lblCity" runat="server" Font-Size="14px"></asp:Label></td>
                        <td>
                            <label>State </label>
                        </td>
                        <td>
                            <asp:Label ID="lblState" runat="server" Font-Size="14px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Postal Code </label>
                        </td>
                        <td>
                            <asp:Label ID="lblPostalCode" runat="server" Font-Size="14px"></asp:Label></td>
                        <td>
                            <label>Country </label>
                        </td>
                        <td>
                            <asp:Label ID="lblCountry" runat="server" Font-Size="14px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <label>Category </label>
                        </td>
                        <td>
                            <asp:Label ID="lblCategory" runat="server" Font-Size="14px"></asp:Label>
                        </td>
                        <td>
                            <label>Type</label></td>
                        <td>
                            <asp:Label ID="lblType" runat="server" Font-Size="14px"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <label>Tax ID</label>
                        </td>
                        <td>
                            <asp:Label ID="lblTaxID" runat="server" Font-Size="14px"></asp:Label>
                        </td>
                        <td>
                            <label>Tax Type </label>
                        </td>
                        <td>
                            <asp:RadioButton ID="rbtnBusinessTaxType" runat="server" value="optBusinessTax" GroupName="TaxType" Enabled="false" /><asp:Label ID="Label1" runat="server" Text="  Business"></asp:Label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:RadioButton ID="rbtnIndividualTaxType" runat="server" value="optIndividualTax" GroupName="TaxType" Enabled="false" /><asp:Label ID="Label2" runat="server" Text="  Individual"></asp:Label>
                        </td>

                    </tr>
                </tbody>
            </table>
            <div class="clr"></div>

            <table width="100%" class="accounts-table" style="float: left;">
                <tbody>
                    <tr>
                        <td>
                            <table class="tabled" width="100%">
                                <thead>
                                    <tr>
                                        <th>Details</th>
                                        <th>Number of Records</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rptrChildScreens" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <a href="#" data-gravity="s" title="Edit" onclick="javascript:return OpenContactDialog('<%#Eval("childscreen") %>' , '<%= pkey_register%>');"><%#Eval("childscreen") %></a>
                                                </td>
                                                <td>
                                                    <%#Eval("screencount") %>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </td>
                        <td>
                            <table width="80%" class="tabled" style="float: right;">
                                <thead>
                                    <tr>
                                        <th colspan="2">Transaction and Credit Limits
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <label>Credit Per Transaction($)</label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCreditPerTransaction" runat="server" Font-Size="14px"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Cummulative Credit($)</label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCumilativeCredit" runat="server" Font-Size="14px"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label>Currency</label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCurrency" runat="server" Font-Size="14px"></asp:Label>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="clr"></div>
            <table width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td>
                            <table class="tabled" width="100%">
                                <thead>
                                    <tr>
                                        <th>Number / ID</th>
                                        <th>Extension</th>
                                        <th>Type</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rptrPhones" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <%#Eval("phone_number") %>
                                                </td>
                                                <td>
                                                    <%#Eval("extension") %>
                                                </td>
                                                <td>
                                                    <%#Eval("contact_type") %>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td>
                            <asp:Button runat="server" CssClass="button rounded" ID="btnEdit" Text="Edit" OnClick="btnEdit_Click"></asp:Button>
                            <asp:Button runat="server" CssClass="button rounded" ID="btnPrint" Text="Print" OnClick="btnPrint_Click"></asp:Button>
                        </td>
                        <td style="float: right;">
                            <asp:Button runat="server" CssClass="button rounded" ID="btnClose" Text="Close" UseSubmitBehavior="false" OnClick="btnClose_Click"></asp:Button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
</asp:Content>
