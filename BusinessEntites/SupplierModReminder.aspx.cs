﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;

namespace Bestprintbuy.Admin.Web.BusinessEntites
{
    public partial class SupplierModReminder : PageBase
    {
        protected string targetAction = "";
        protected int registerKey = -2;
        protected int pkeyType = -1;
        protected int pkeyCategory = -1;
        protected int pkeyMasterCat = 1;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                GetData();
            }
        }

        private void GetData()
        {
            try
            {
                pkeyMasterCat = GetMasterCatKey(Request.QueryString["bid"]);
                registerKey = Convert.ToInt32(Request.QueryString["pkey"]);
                BusinessEntityModel model = BusinessEntitiesService.GetSupplierRemData(registerKey, pkeyType, pkeyCategory, pkeyMasterCat, ConnString);
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }            
        }

        protected void btnAccept_Click(object sender, EventArgs e)
        {
            try
            {
                targetAction = "accept";
                pkeyMasterCat = BusinessEntitiesService.UpdateSupplierChanges(registerKey, targetAction, ConnString);
                AlertScript("The modifications requested by the supplier are alredy accepted/rejected.");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            try
            {                
                targetAction = "reject";
                pkeyMasterCat = BusinessEntitiesService.UpdateSupplierChanges(registerKey, targetAction, ConnString);
                AlertScript("The modifications requested by the supplier are alredy accepted/rejected.");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("SupplierModRemList.aspx");
        }

        private int GetMasterCatKey(string strID)
        {
            switch (strID)
            {
                case "sup":
                    //Suppliers
                    return 2;
                case "mer":
                    //Merchant Association - now not being used
                    return 4;
                case "pat":
                    //Co-Branded Business Partners  
                    return 3;
                case "afl":
                    //B2B Affiliates   
                    return 10;
                case "bur":
                    //Business Category Register 
                    return 1;
                case "ship":
                    //Freight Classification and Delivery
                    return 13;
                case "usr":
                    //incase of admin user
                    return -1;
                default:
                    //Business Category Register 
                    return 1;
            }
        }
    }
}