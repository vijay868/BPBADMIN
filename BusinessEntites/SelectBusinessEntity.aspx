﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SelectBusinessEntity.aspx.cs" Inherits="Bestprintbuy.Admin.Web.BusinessEntites.SelectBusinessEntity" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Best Print Buy</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" media="all" href="../css/bestprintbuy_style.css" />
    <link rel="stylesheet" media="all" href="../css/jquery.fancybox.css" />
    <link rel="stylesheet" href="../css/jquery-ui.css" />
    <script src="../Scripts/jquery.js"></script>
    <script src="../Scripts/jquery-ui.js"></script>
    <script src="../Scripts/common.js"></script>
    <script src="../Scripts/ddsmoothmenu.js"></script>
    <script src="../Scripts/plugin/fancybox/jquery.fancybox.js"></script>
    <script src="../Scripts/jquery.validate.js"></script>
    <!--[if lt IE 9]>
			<script src="../Scripts/html5.js"></script>
		<![endif]-->
    <!--[if lte IE 7]> <html class="ie7"> <![endif]-->
    <!--[if IE 8]>     <html class="ie8"> <![endif]-->
    <!--[if IE 9]>     <html class="ie9"> <![endif]-->
    <!--[if IE 10]>     <html class="ie10"> <![endif]-->
    <!--[if !IE]><!-->
    <!--<![endif]-->
    <script type="text/javascript">
        function customerSelect(obj) {
            var grid = obj.parentNode.parentNode.parentNode.parentNode;
            var inputs = grid.getElementsByTagName("input");
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].type == "checkbox") {
                    if (obj.checked && inputs[i] != obj && inputs[i].checked) {
                        inputs[i].checked = false;
                    }
                }
            }
        }

        function SetDataAndClose(RegisterKey, Lastname) {
            window.top.SetSupplierData(RegisterKey, Lastname);
            return false;
        }

        function checkSelect() {
            var ischecked = false;
            $('.select input:checkbox').each(function () {
                if (this.checked) {
                    ischecked = true;
                    return true;
                }
            });

            if (!ischecked) {
                alert("Please select atleast one bussiness register.");
                return false;
            }
            return true;
        }

    </script>
</head>
<body id="Body" runat="server">
    <!-- Wrapper starts -->
    <div id="Div1" class="popup-wrapper" runat="server">
        <div class="popup-title">
            Business Register List
        </div>
        <!-- Header Ends -->
        <div class="popup-contnt">
            <!-- Menu starts -->
            <form id="Form1" runat="server">
                <table class="tablep">
                    <thead>
                        <tr>
                            <th>Business ID</th>
                            <th>Business Name</th>
                            <th>Type Of Business Register</th>
                            <th>City</th>
                            <th>State</th>
                            <th>Country	</th>
                            <th>Select</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rptrSelectBusinessEntity" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <%#Eval("business_id") %>
                                    </td>
                                    <td>
                                        <%#Eval("business_name") %>
                                    </td>
                                    <td>
                                        <%#Eval("businesstype") %>
                                    </td>
                                    <td>
                                        <%#Eval("city") %>
                                    </td>
                                    <td>
                                        <%#Eval("state_name") %>
                                    </td>
                                    <td>
                                        <%#Eval("country_name") %>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkSelect" runat="server" onclick="customerSelect(this);" CssClass="select" />
                                        <asp:HiddenField ID="hdnRegisterKey" runat="server" Value='<%#Eval("pkey_business_register") %>' />
                                        <asp:HiddenField ID="hdnBusinessName" runat="server" Value='<%# Eval("business_name") %>' />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
                <table>
                    <tr>
                        <td style="float: right;">
                            <asp:Button runat="server" ID="btnSelect" CssClass="button rounded" Text="Select" OnClientClick="if(!checkSelect()) return false;" OnClick="btnSelect_Click"></asp:Button>
                            <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClientClick="parent.$.fancybox.close(); return false;"></asp:Button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</body>
</html>
