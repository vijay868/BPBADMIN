﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;

namespace Bestprintbuy.Admin.Web.BusinessEntites
{
    public partial class BusinessTransactionsList : PageBase
    {
        protected int pkeyregister = -1;
        protected decimal totalCredit = 0;
        protected decimal totalDebit = 0;
        protected string balance = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                try
                {
                    GetData();
                }
                catch (Exception ex)
                {
                    FileLogger.WriteException(ex);
                    AlertScript(ex.Message);
                }               
            }
        }

        private void GetData()
        {
            pkeyregister = Convert.ToInt32(Request.QueryString["pkeyRegister"] == null ? "-1" : Request.QueryString["pkeyRegister"]);
            BusinessEntityModel model = BusinessEntitiesService.GetStatementOfAccounts(pkeyregister, ConnString);

            rptrBusinessTransactions.DataSource = model.BusinessTransactions;
            rptrBusinessTransactions.DataBind();

            foreach (BusinessEntityModel.BusinessTransactionsRow row in model.BusinessTransactions.Rows)
            {
                totalCredit = row.credit;
                totalDebit = row.debit;
            }
            balance = (totalCredit - totalDebit).ToString();
        }
    }
}