﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;

namespace Bestprintbuy.Admin.Web.BusinessEntites
{
    public partial class SelectBusinessEntity : PageBase
    {
        protected int pkeyType = -1;
        protected int pkeyCategory = -1;
        protected int pkeyMasterCat = 1;
        protected int isSupplier = 0;
        protected string searchfor = "-1";
        protected int pkey_register = -1;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                GetData();
            }
        }

        private void GetData()
        {
            try
            {
                pkeyMasterCat = GetMasterCatKey(Request.QueryString["bid"]);
                pkeyType = Convert.ToInt32(Request.QueryString["cboType"] == null ? "-1" : Request.QueryString["cboType"]);
                pkey_register = Convert.ToInt32(Request.QueryString["pkeyregister"] == null ? "-1" : Request.QueryString["pkeyregister"]);
                BusinessEntityModel model = BusinessEntitiesService.GetList(pkey_register, pkeyType, pkeyCategory, searchfor, pkeyMasterCat, isSupplier, ConnString);
                if (model.BusinessEntity.Rows.Count > 0)
                {
                    rptrSelectBusinessEntity.DataSource = model.BusinessEntity;
                    rptrSelectBusinessEntity.DataBind();
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        private int GetMasterCatKey(string strID)
        {
            switch (strID)
            {
                case "sup":
                    //Suppliers
                    return 2;
                case "mer":
                    //Merchant Association - now not being used
                    return 4;
                case "pat":
                    //Co-Branded Business Partners  
                    return 3;
                case "afl":
                    //B2B Affiliates   
                    return 10;
                case "bur":
                    //Business Category Register 
                    return 1;
                case "ship":
                    //Freight Classification and Delivery
                    return 13;
                case "usr":
                    //incase of admin user
                    return -1;
                default:
                    //Business Category Register 
                    return 1;
            }
        }

        protected void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (RepeaterItem item in rptrSelectBusinessEntity.Items)
                {
                    HiddenField hdnRegisterKey = (HiddenField)item.FindControl("hdnRegisterKey");
                    HiddenField hdnBusinessName = (HiddenField)item.FindControl("hdnBusinessName");
                    CheckBox chkbox = (CheckBox)item.FindControl("chkSelect");
                    if (chkbox.Checked == true)
                    {
                        string script = string.Format("SetDataAndClose('{0}','{1}');", hdnRegisterKey.Value, hdnBusinessName.Value);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "jscript", script, true);
                    }
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }            
        }
    }
}