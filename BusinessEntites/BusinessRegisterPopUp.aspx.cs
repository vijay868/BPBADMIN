﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;

namespace Bestprintbuy.Admin.Web.BusinessEntites
{
    public partial class BusinessRegisterPopUp : PageBase
    {
        protected int pkey_register = -2;
        protected int pkeyType = -1;
        protected int pkeyCategory = -1;
        protected int pkeyMasterCat = 1;
        protected int isSupplier = 0;
        protected string searchfor = "-1";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                GetData();
            }
        }

        private void GetData()
        {
            try
            {
                pkeyMasterCat = GetMasterCatKey(Request.QueryString["bid"]);
                pkey_register = Convert.ToInt32(Request.QueryString["pkey"]);
                BusinessEntityModel model = BusinessEntitiesService.GetList(pkey_register, pkeyType, pkeyCategory, searchfor, pkeyMasterCat, isSupplier, ConnString);

                if (model.Phones.Rows.Count > 0)
                {
                    rptrPhones.DataSource = model.Phones;
                    rptrPhones.DataBind();
                }
                if (model.BusinessEntity.Rows.Count > 0)
                {
                    BusinessEntityModel.BusinessEntityRow row = (BusinessEntityModel.BusinessEntityRow)model.BusinessEntity.Rows[0];
                    lblBusinessID.Text = row.Isbusiness_idNull() ? "" : row.business_id;
                    lblPrefix.Text = row.IsprefixNull() ? "" : row.prefix;
                    lblName.Text = row.Isbusiness_nameNull() ? "" : row.business_name;
                    lblOtherNames.Text = row.Isother_namesNull() ? "" : row.other_names;
                    lblAddress1.Text = row.Isaddress1Null() ? "" : row.address1;
                    lblAddress2.Text = row.Isaddress2Null() ? "" : row.address2;
                    lblCity.Text = row.IscityNull() ? "" : row.city;
                    lblState.Text = row.Isstate_nameNull() ? "" : row.state_name;
                    lblPostalCode.Text = row.IszipcodeNull() ? "" : row.zipcode;
                    lblCountry.Text = row.Iscountry_nameNull() ? "" : row.country_name;
                    lblCategory.Text = row.IsbusinesscategoryNull() ? "" : row.businesscategory;
                    lblType.Text = row.IsbusinesstypeNull() ? "" : row.businesstype;
                    lblTaxID.Text = row.Istax_idNull() ? "" : row.tax_id;
                    if (row.is_business)
                        rbtnBusiness.Checked = true;
                    else
                        rbtnIndividual.Checked = true;
                    if (row.tax_type)
                        rbtnBusinessTaxType.Checked = true;
                    else
                        rbtnIndividualTaxType.Checked = true;
                    lblCreditPerTransaction.Text = row.Iscredit_per_transactionNull() ? "" : row.credit_per_transaction.ToString();
                    lblCumilativeCredit.Text = row.Iscumulative_creditNull() ? "" : row.cumulative_credit.ToString();
                    lblCurrency.Text = row.Iscurrency_nameNull() ? "" : row.currency_name;
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        private int GetMasterCatKey(string strID)
        {
            switch (strID)
            {
                case "sup":
                    //Suppliers
                    return 2;
                case "mer":
                    //Merchant Association - now not being used
                    return 4;
                case "pat":
                    //Co-Branded Business Partners  
                    return 3;
                case "afl":
                    //B2B Affiliates   
                    return 10;
                case "bur":
                    //Business Category Register 
                    return 1;
                case "ship":
                    //Freight Classification and Delivery
                    return 13;
                case "usr":
                    //incase of admin user
                    return -1;
                default:
                    //Business Category Register 
                    return 1;
            }
        }

    }
}