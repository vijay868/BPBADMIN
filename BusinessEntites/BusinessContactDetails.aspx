﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="BusinessContactDetails.aspx.cs" Inherits="Bestprintbuy.Admin.Web.BusinessEntites.BusinessContactDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
        $().ready(function () {
            $("#frmBusinessDetail").validate();
        });
        function contactInfo(email, ext, type, pkay) {
            $("#ContentPlaceHolder1_txtNumberID").val(email);
            $("#ContentPlaceHolder1_txtExtension").val(ext);
            $("#ContentPlaceHolder1_pkeyphones").val(pkay);
            $.fancybox({
                'width': '100%',
                'height': 'auto',
                'autoScale': true,
                'transitionIn': 'fade',
                'transitionOut': 'fade',
                'href': '#dialog_Contact_info',
                'type': 'inline'
            });
        }

        function validatePhones() {
            if ($("#ContentPlaceHolder1_txtNumberID").val() == "") {
                alert("Please enter Number / ID for saving the information.");
                return false;
            }
        }

        function setValues() {
            $("#ContentPlaceHolder1_hdnNumber").val($("#ContentPlaceHolder1_txtNumberID").val());
            $("#ContentPlaceHolder1_hdnExt").val($("#ContentPlaceHolder1_txtExtension").val());
            $("#ContentPlaceHolder1_hdnType").val($("#ContentPlaceHolder1_ddlFormat").val());
            $("#ContentPlaceHolder1_hdnValue").val($("#ContentPlaceHolder1_ddlFormat option:selected").text());
            parent.$.fancybox.close();
            return true;
        }
    </script>
     <style type="text/css">
        #dialog_Contact_info {
        overflow:hidden;        
        }
    </style>
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Business Partners Setup</li>
            <li>Contacts Detail </li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2>Contacts Detail </h2>
        <form id="frmBusinessDetail" name="frmBusinessDetail" runat="server">
            <table width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="15%">
                            <label>Contact ID*</label></td>
                        <td width="20%">
                            <asp:TextBox CssClass="text-box" name="txtContactID" ID="txtContactID" runat="server" required></asp:TextBox>
                        </td>
                        <td width="15%">
                            <label>First Name* </label>
                        </td>
                        <td width="35%">
                            <asp:TextBox CssClass="text-box" name="txtFirstName" ID="txtFirstName" runat="server" required></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Last Name*</label></td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtLastName" ID="txtLastName" runat="server" required></asp:TextBox>
                        </td>
                        <td>
                            <label>Title</label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtTitle" ID="txtTitle" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Salutation</label>
                        </td>
                        <td>
                            <asp:DropDownList name="ddlSalutation" ID="ddlSalutation" CssClass="slect-box" runat="server">
                                <asp:ListItem Value="Mr.">Mr.</asp:ListItem>
                                <asp:ListItem Value="Mrs.">Mrs.</asp:ListItem>
                                <asp:ListItem Value="Ms.">Ms.</asp:ListItem>
                                <asp:ListItem Value="Dr.">Dr.</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            <label>Principal contact</label>
                        </td>
                        <td>
                            <asp:CheckBox ID="chkPrinContact" runat="server" CssClass="check-box" />
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">
                            <label>Address </label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtAddress1" ID="txtAddress1" runat="server"></asp:TextBox>                            
                        </td>
                        <td>
                            <label>City </label>
                        </td>
                        <td>
                            <asp:TextBox CssClass="text-box" name="txtCity" ID="txtCity" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label></label>
                        </td>
                        <td>
                           <asp:TextBox CssClass="text-box" name="txtAddress2" ID="txtAddress2" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <label>Country </label>
                        </td>
                        <td>
                             <asp:DropDownList name="ddlCountry" ID="ddlCountry" CssClass="slect-box" runat="server" >
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <label>Postal Code </label>
                        </td>
                        <td>
                           
                             <asp:TextBox CssClass="text-box" name="txtPostalCode" ID="txtPostalCode" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <label>State</label>                           
                        </td>
                        <td>
                            <asp:DropDownList name="ddlState" ID="ddlState" CssClass="slect-box" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </tbody>
            </table>

            <table width="100%" class="accounts-table" id="fSetPhone" visible="false" runat="server">
                <thead>
                    <tr>
                        <th colspan="4">Phones And Emails
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>           
            <div class="clr"></div>
                            <table class="tabled" width="100%">
                                <thead>
                                    <tr>
                                        <th>Number / ID</th>
                                        <th>Extension</th>
                                        <th>Type</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rptrPhones" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <%#Eval("phone_number") %>
                                                </td>
                                                <td>
                                                    <%#Eval("extension") %>
                                                </td>
                                                <td>
                                                    <%#Eval("contact_type") %>
                                                </td>
                                                <td>
                                                    <a href="#" onclick="contactInfo('<%#Eval("phone_number") %>', '<%#Eval("extension") %>', '<%#Eval("contact_type") %>', '<%#Eval("pkey_phones") %>');" title="Edit">
                                                        <img width="16" height="16" src="../images/pencil.png" /></a>
                                                    <asp:LinkButton ID="lbtnDelete" runat="server" CommandName="Delete" CausesValidation="true"
                                                        ToolTip="Delete" CommandArgument='<%#Bind("pkey_phones")%>'
                                                        OnClick="lbtnDelete_Click"><img src="../images/trashcan.png" /></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                            <table class="accounts-table">
                                <tbody>
                                    <tr>
                                        <td style="float: right;">
                                            <asp:HiddenField ID="pkeyphones" runat="server" />
                                            <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="button rounded" OnClientClick="javascript:contactInfo('','','','-2'); return false;" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table class="accounts-table">
                <tbody>
                    <tr>
                        <td>
                            <asp:Button runat="server" ID="btnDelete" Text="Delete" CssClass="button rounded" OnClick="btnDelete_Click" Visible="false" UseSubmitBehavior="false"></asp:Button></td>
                        <td></td>
                        <td></td>
                        <td style="float: right;">
                            <asp:Button runat="server" ID="btnSave" Text="Save" OnClick="btnSave_Click" CssClass="button rounded"></asp:Button>
                            <asp:Button runat="server" ID="btnClose" Text="Close" OnClick="btnClose_Click" CssClass="button rounded" UseSubmitBehavior="false"></asp:Button>
                            <asp:HiddenField ID="hdnPkeyContact" runat="server" />
                            <asp:HiddenField ID="hdnFirstName" runat="server" />
                            <asp:HiddenField ID="hdnLastName" runat="server" />
                            <asp:HiddenField ID="hdnNumber" runat="server" />
                            <asp:HiddenField ID="hdnExt" runat="server" />
                            <asp:HiddenField ID="hdnType" runat="server" />
                            <asp:HiddenField ID="hdnValue" runat="server" />
                        </td>
                    </tr>
                </tbody>
            </table>
            <div style="display: none;" id="dialog_Contact_info">
                <div id="Div1" class="popup-wrapper" runat="server">
                    <div class="popup-title">
                        Phones And Emails
                    </div>
                    <!-- Header Ends -->
                    <div class="popup-contnt">
                        <!-- Menu starts -->
                        <table>
                            <tr>
                                <td width="40%">
                                    <label>Number / ID</label></td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtNumberID" CssClass="text"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Ext / Cc</label></td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtExtension" CssClass="text"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Type</label></td>
                                <td>
                                    <asp:DropDownList name="ddlFormat" ID="ddlFormat" CssClass="select-box" runat="server">
                                        <asp:ListItem Value="OP" Selected>Office Phone</asp:ListItem>
                                        <asp:ListItem Value="HP">Home Phone</asp:ListItem>
                                        <asp:ListItem Value="MP">Mobile Phone</asp:ListItem>
                                        <asp:ListItem Value="FA">Fax</asp:ListItem>
                                        <asp:ListItem Value="EM">Email</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <asp:Button runat="server" CssClass="button rounded" ID="btnUpdatePhone" Text="Save" UseSubmitBehavior="false" OnClick="btnUpdatePhone_Click" OnClientClick="setValues();"></asp:Button></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </form>
    </div>
</asp:Content>
