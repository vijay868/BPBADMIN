﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;

namespace Bestprintbuy.Admin.Web.BusinessEntites
{
    public partial class ASupplierPayment : PageBase
    {
        protected int pkey_br_payment_info = -2;
        protected int pkey_register = -1;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                try
                {
                    GetData();
                }
                catch (Exception ex)
                {
                    FileLogger.WriteException(ex);
                    AlertScript(ex.Message);
                }
            }
        }

        private void GetData()
        {
            pkey_register = hdnfkeyRegister.Value != "" ? Convert.ToInt32(hdnfkeyRegister.Value) : -1;
            PaymentSetupModel model = PaymentsSetupService.GetData(pkey_register, ConnString);
            ViewState["model"] = model;
            if (model.SupplierPaymentInfo.Rows.Count > 0)
            {
                if (model.PaymentCategories.Rows.Count > 0)
                {
                    ddlPaymentCategory.DataSource = model.PaymentCategories;
                    ddlPaymentCategory.DataTextField = "ct_name";
                    ddlPaymentCategory.DataValueField = "pkey_category_type";
                    ddlPaymentCategory.DataBind();
                }
                if (model.Countries.Rows.Count > 0)
                {
                    ddlCountry.DataSource = model.Countries;
                    ddlCountry.DataTextField = "country_name";
                    ddlCountry.DataValueField = "pkey_country";
                    ddlCountry.DataBind();
                }
                if (model.PaymentTypes.Rows.Count > 0)
                {
                    ddlPaymentType.DataSource = model.PaymentTypes;
                    ddlPaymentType.DataTextField = "ct_name";
                    ddlPaymentType.DataValueField = "pkey_category_type";
                    ddlPaymentType.DataBind();
                }

                if (model.States.Rows.Count > 0)
                {
                    ddlState.DataSource = model.States;
                    ddlState.DataTextField = "state_name";
                    ddlState.DataValueField = "pkey_state";
                    ddlState.DataBind();
                }
                ddlCountry.Items.Insert(0, new ListItem("No Option", ""));
                ddlPaymentCategory.Items.Insert(0, new ListItem("No Option", ""));
                ddlPaymentType.Items.Insert(0, new ListItem("No Option", ""));
                ddlState.Items.Insert(0, new ListItem("No Option", ""));

                PaymentSetupModel.SupplierPaymentInfoRow row = (PaymentSetupModel.SupplierPaymentInfoRow)model.SupplierPaymentInfo.Rows[0];
                lblSupplier.Text = row.IssuppliernameNull() ? "" : row.suppliername;
                lblPayto.Text = row.IspayeenameNull() ? "" : row.payeename;
                hdnfkeyRegister.Value = row.Isfkey_business_registerNull() ? "" : row.fkey_business_register.ToString();
                hdnfkeyContact.Value = row.Isfkey_payee_contactNull() ? "" : row.fkey_payee_contact.ToString();
                hdnpkey_br_payment_info.Value = row.Ispkey_br_payment_infoNull() ? "-2" : row.pkey_br_payment_info.ToString();

                txtAttentionof.Text = row.Isattention_ofNull() ? "" : row.attention_of;
                txtAddress1.Text = row.Isaddress1Null() ? "" : row.address1;
                txtAddress2.Text = row.Isaddress2Null() ? "" : row.address2;
                txtCity.Text = row.IscityNull() ? "" : row.city;
                txtPostalCode.Text = row.IszipcodeNull() ? "" : row.zipcode;
                txtSupplierAccNumber.Text = row.Issupplier_ac_numberNull() ? "" : row.supplier_ac_number;
                txtBankName.Text = row.Isbank_nameNull() ? "" : row.bank_name;
                txtRoutingtNumber.Text = row.Isrouting_numberNull() ? "" : row.routing_number;
                txtAccnumber.Text = row.Isaccount_numberNull() ? "" : row.account_number;
                ddlPaymentCategory.SelectedValue = row.Isfkey_payment_categoryNull() ? "" : row.fkey_payment_category.ToString();
                ddlPaymentType.SelectedValue = row.Isfkey_payment_typeNull() ? "" : row.fkey_payment_type.ToString();
                ddlCountry.SelectedValue = row.Isfkey_countryNull() ? "" : row.fkey_country.ToString();
                ddlState.SelectedValue = row.Isfkey_stateNull() ? "" : row.fkey_state.ToString();
            }
            else
            {
                if (model.PaymentCategories.Rows.Count > 0)
                {
                    ddlPaymentCategory.DataSource = model.PaymentCategories;
                    ddlPaymentCategory.DataTextField = "ct_name";
                    ddlPaymentCategory.DataValueField = "pkey_category_type";
                    ddlPaymentCategory.DataBind();
                }

                if (model.Countries.Rows.Count > 0)
                {
                    ddlCountry.DataSource = model.Countries;
                    ddlCountry.DataTextField = "country_name";
                    ddlCountry.DataValueField = "pkey_country";
                    ddlCountry.DataBind();
                }
                ddlCountry.Items.Insert(0, new ListItem("No Option", ""));
                ddlPaymentCategory.Items.Insert(0, new ListItem("No Option", ""));
                ddlPaymentType.Items.Insert(0, new ListItem("No Option", ""));
                ddlState.Items.Insert(0, new ListItem("No Option", ""));
                hdnfkeyRegister.Value = "";
                hdnfkeyContact.Value = "";
            }
            if (model.SupplierCount.Rows.Count > 0)
            {
                if (hdnfkeyRegister.Value == "")
                {
                    hdnfkeyRegister.Value = model.SupplierCount.Rows[0]["fkey_business_register"].ToString();
                    lblSupplier.Text = model.SupplierCount.Rows[0]["suppliername"].ToString();
                    if (Convert.ToInt32(model.SupplierCount.Rows[0]["noofcontacts"]) == 0)
                        AlertScript("The Selected supplier has no contacts defined. Please define contacts for the supplier.");
                }
            }
        }

        protected void lbtnSupplier_Click(object sender, EventArgs e)
        {
            lblPayto.Text = "";
            lblSupplier.Text = "";
            txtAttentionof.Text = "";
            txtAddress1.Text = "";
            txtAddress2.Text = "";
            txtCity.Text = "";
            txtPostalCode.Text = "";
            txtSupplierAccNumber.Text = "";
            txtBankName.Text = "";
            txtRoutingtNumber.Text = "";
            txtAccnumber.Text = "";
            GetData();
        }

        protected void ddlPaymentCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                PaymentSetupModel model = (PaymentSetupModel)ViewState["model"];
                var list = from collect in model.PaymentTypes
                           where collect.fkey_self == Convert.ToInt32(ddlPaymentCategory.SelectedValue)
                           select collect;

                ddlPaymentType.DataSource = list.ToList();
                ddlPaymentType.DataTextField = "ct_name";
                ddlPaymentType.DataValueField = "pkey_category_type";
                ddlPaymentType.DataBind();
                ddlPaymentType.Items.Insert(0, new ListItem("No Option", ""));
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                PaymentSetupModel model = (PaymentSetupModel)ViewState["model"];

                var list = from collect in model.States
                           where collect.fkey_country == Convert.ToInt32(ddlCountry.SelectedValue)
                           select collect;

                ddlState.DataSource = list.ToList();
                ddlState.DataTextField = "state_name";
                ddlState.DataValueField = "pkey_state";
                ddlState.DataBind();
                ddlState.Items.Insert(0, new ListItem("No Option", ""));
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                PaymentSetupModel model = new PaymentSetupModel();
                PaymentSetupModel.SupplierPaymentInfoRow row = (PaymentSetupModel.SupplierPaymentInfoRow)model.SupplierPaymentInfo.NewSupplierPaymentInfoRow();
                row.pkey_br_payment_info = hdnpkey_br_payment_info.Value == "" ? -2 : Convert.ToInt32(hdnpkey_br_payment_info.Value);
                row.fkey_business_register = Convert.ToInt32(hdnfkeyRegister.Value);
                row.fkey_payee_contact = Convert.ToInt32(hdnfkeyContact.Value);
                row.attention_of = txtAttentionof.Text;
                row.address1 = txtAddress1.Text;
                row.address2 = txtAddress2.Text;
                row.city = txtCity.Text;
                row.fkey_state = Convert.ToInt32(ddlState.SelectedValue == "" ? "-1" : ddlState.SelectedValue);
                row.zipcode = txtPostalCode.Text;
                row.supplier_ac_number = txtSupplierAccNumber.Text;
                row.bank_name = txtBankName.Text;
                row.routing_number = txtRoutingtNumber.Text;
                row.account_number = txtAccnumber.Text;
                row.fkey_user = Identity.UserPK;
                row.fkey_payment_type = Convert.ToInt32(ddlPaymentType.SelectedValue == "" ? "-1" : ddlPaymentType.SelectedValue);
                model.SupplierPaymentInfo.AddSupplierPaymentInfoRow(row);

                pkey_br_payment_info = PaymentsSetupService.Update(model, ConnString);
                AlertScript("Successfully Saved.");
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript("Insert/Update Failed. Please try Again.");
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/DashBoard.aspx");
        }
    }
}