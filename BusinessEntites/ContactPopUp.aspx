﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ContactPopUp.aspx.cs" Inherits="Bestprintbuy.Admin.Web.BusinessEntites.ContactPopUp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Best Print Buy</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" media="all" href="../css/bestprintbuy_style.css" />
    <link rel="stylesheet" media="all" href="../css/jquery.fancybox.css" />
    <script src="../Scripts/jquery.js"></script>
    <script src="../Scripts/common.js"></script>
    <script src="../Scripts/ddsmoothmenu.js"></script>
    <script src="../Scripts/plugin/fancybox/jquery.fancybox.js"></script>
    <!--[if lt IE 9]>
			<script src="../Scripts/html5.js"></script>
		<![endif]-->
    <!--[if lte IE 7]> <html class="ie7"> <![endif]-->
    <!--[if IE 8]>     <html class="ie8"> <![endif]-->
    <!--[if IE 9]>     <html class="ie9"> <![endif]-->
    <!--[if IE 10]>     <html class="ie10"> <![endif]-->
    <!--[if !IE]><!-->
    <!--<![endif]-->
</head>
<body id="Body" runat="server">
    <!-- Wrapper starts -->
    <div id="Div1" class="popup-wrapper" runat="server">
        <div class="popup-title">
            Business Contact Details
        </div>
        <!-- Header Ends -->
        <div class="popup-contnt">
            <!-- Menu starts -->
            <form id="Form1" runat="server">
                <table>
                    <tr>
                        <td width="40%">
                            <label for="lblCustName">Contact ID</label>
                        </td>
                        <td>
                            <asp:Label ID="lblContactID" runat="server" Font-Size="12px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="lblCustEmail">First Name</label>
                        </td>
                        <td>
                            <asp:Label ID="lblFirstName" runat="server" Font-Size="12px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Last Name</label>
                        </td>
                        <td>
                            <asp:Label ID="lblLastName" runat="server" Font-Size="12px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Title</label>
                        </td>
                        <td>
                            <asp:Label ID="lblTitle" runat="server" Font-Size="12px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Salutation</label>
                        </td>
                        <td>
                            <asp:Label ID="lblSalutation" runat="server" Font-Size="12px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label></label>
                        </td>
                        <td>
                            <asp:CheckBox ID="chkPrincContact" runat="server" Text=" Principal Contact" Enabled="false" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Address</label>
                        </td>
                        <td>
                            <asp:Label ID="lblAddress1" runat="server" Font-Size="12px"></asp:Label><br />
                            <asp:Label ID="lblAddress2" runat="server" Font-Size="12px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>City</label>
                        </td>
                        <td>
                            <asp:Label ID="lblCity" runat="server" Font-Size="12px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>State</label>
                        </td>
                        <td>
                            <asp:Label ID="lblState" runat="server" Font-Size="12px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Postal Code</label>
                        </td>
                        <td>
                            <asp:Label ID="lblPostalCode" runat="server" Font-Size="12px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Country</label>
                        </td>
                        <td>
                            <asp:Label ID="lblCountry" runat="server" Font-Size="12px"></asp:Label>
                        </td>
                    </tr>
                </table>           
            <div class="clr"></div>

                <table class="tablep">
                    <thead>
                        <tr>
                            <th>Number /ID</th>
                            <th>Extension</th>
                            <th>Type</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rptrContactSummary" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <%# Eval("phone_number")%>
                                    </td>
                                    <td>
                                        <%# Eval("extension")%>
                                    </td>
                                    <td>
                                        <%# Eval("contact_type") %>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
                <table>
                    <tr>
                        <td style="float: right;">
                            <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClientClick="parent.$.fancybox.close(); return false;"></asp:Button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</body>
</html>
