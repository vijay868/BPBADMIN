﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;

namespace Bestprintbuy.Admin.Web.BusinessEntites
{
    public partial class SupplierModRemList : PageBase
    {
        protected int pkeyType = -1;
        protected int pkeyCategory = -1;
        protected int pkeyMasterCat = 1;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                GetData();
            }
        }

        private void GetData()
        {
            try
            {
                pkeyMasterCat = GetMasterCatKeys(Request.QueryString["bid"]);
                pkeyType = Convert.ToInt32(ddltypes.SelectedValue == "" ? "-1" : ddltypes.SelectedValue);
                BusinessEntityModel model = BusinessEntitiesService.GetSupplierRemData(-1, pkeyType, pkeyCategory, pkeyMasterCat, ConnString);
                lblTotalRecords.Text = string.Format(" Total Number Supplier / Vendors Modification Reminders  : {0}", model.BusinessEntity.Rows.Count);
                if (model.BusinessEntity.Rows.Count > 0)
                {
                    SuppliermodGrid.DataSource = model.BusinessEntity;
                    SuppliermodGrid.DataBind();
                }
                if (model.BusinessTypes.Rows.Count > 0)
                {
                    ddltypes.DataSource = model.BusinessTypes;
                    ddltypes.DataTextField = "ct_name";
                    ddltypes.DataValueField = "pkey_category_type";
                    ddltypes.DataBind();
                    ddltypes.Items.Insert(0, new ListItem("No Filter", "-1"));
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }            
        }

        private int GetMasterCatKeys(string strID)
        {
            switch (strID)
            {
                case "sup":
                    //Suppliers
                    return 2;
                case "mer":
                    //Merchant Association - now not being used
                    return 4;
                case "pat":
                    //Co-Branded Business Partners  
                    return 3;
                case "afl":
                    //B2B Affiliates   
                    return 10;
                case "bur":
                    //Business Category Register 
                    return 1;
                case "ship":
                    //Freight Classification and Delivery
                    return 13;
                case "usr":
                    //incase of admin user
                    return -1;
                default:
                    //Business Category Register 
                    return 1;
            }
        }
        protected int GetRecordCountForPage()
        {
            return GetRecordCountForPageSize(SuppliermodGrid.PageSize, SuppliermodGrid.PageIndex + 1);
        }

        protected void CustomerslistGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            SortGridView(SuppliermodGrid, e);
            SuppliermodGrid.PageIndex = 0;

            GetData();
        }


        protected void SuppliermodGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            SuppliermodGrid.PageIndex = e.NewPageIndex;
            GetData();
        }
        protected void ddltypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetData();
            ddltypes.SelectedValue = pkeyType.ToString();
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Account/DashBoard.aspx");
        }
    }
}