﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BusinessTransactionsList.aspx.cs" Inherits="Bestprintbuy.Admin.Web.BusinessEntites.BusinessTransactionsList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Best Print Buy</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" media="all" href="../css/bestprintbuy_style.css" />
    <link rel="stylesheet" media="all" href="../css/jquery.fancybox.css" />
    <link rel="stylesheet" href="../css/jquery-ui.css" />
    <script src="../Scripts/jquery.js"></script>
    <script src="../Scripts/jquery-ui.js"></script>
    <script src="../Scripts/common.js"></script>
    <script src="../Scripts/ddsmoothmenu.js"></script>
    <script src="../Scripts/plugin/fancybox/jquery.fancybox.js"></script>
    <script src="../Scripts/jquery.validate.js"></script>
    <!--[if lt IE 9]>
			<script src="../Scripts/html5.js"></script>
		<![endif]-->
    <!--[if lte IE 7]> <html class="ie7"> <![endif]-->
    <!--[if IE 8]>     <html class="ie8"> <![endif]-->
    <!--[if IE 9]>     <html class="ie9"> <![endif]-->
    <!--[if IE 10]>     <html class="ie10"> <![endif]-->
    <!--[if !IE]><!-->
    <!--<![endif]-->
</head>
<body id="Body" runat="server">
    <!-- Wrapper starts -->
    <div id="Div1" class="popup-wrapper" runat="server">
        <div class="popup-title">
            Business Register List
        </div>
        <!-- Header Ends -->
        <div class="popup-contnt">
            <!-- Menu starts -->
            <form id="Form1" runat="server">           
            <div class="clr"></div>
                <table class="tablep">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Transaction Details</th>
                            <th>Debit</th>
                            <th>Credit</th>
                            <th>Balance</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rptrBusinessTransactions" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <%#Eval("transdate","{0:MM/dd/yyyy}") %>
                                    </td>
                                    <td>
                                        <%#Eval("tran_detail") %>
                                    </td>
                                    <td>
                                        <%#Eval("debit") %>
                                    </td>
                                    <td>
                                        <%#Eval("credit") %>
                                    </td>
                                    <td>
                                        <%#Convert.ToDecimal(Eval("credit"))-Convert.ToDecimal(Eval("debit")) %>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
                <table>
                    <tr>
                        <td >
                            <label>Total Debit($) </label>
                            <%= totalDebit %></td>
                        <td >
                            <label>Total Credit($)</label>
                            <%= totalCredit %></td>
                        <td >
                            <label>Total Balance($)</label>
                            <%= balance%></td>
                    </tr>
                    <tr>
                        <td></td>
                            <td></td>
                        <td style="float: right;" >
                            <asp:Button runat="server" ID="btnClose" CssClass="button rounded" Text="Close" OnClientClick="parent.$.fancybox.close(); return false;"></asp:Button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</body>
</html>
