﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.Master" AutoEventWireup="true" CodeBehind="BusinessRegisterList.aspx.cs" Inherits="Bestprintbuy.Admin.Web.BusinessEntites.BusinessRegisterList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="clr"></div>
    <!-- Container Starts -->
    <div class="contant_hldr">
        <!-- Breadcrumbs Starts -->
        <ul class="breadcrumbs">
            <li><a href="../Account/DashBoard.aspx">Home</a></li>
            <li class="liColor">Business Partners Setup</li>
            <li><asp:Label ID="lblbreadcrumb" runat="server"></asp:Label></li>
        </ul>
        <!-- Breadcrumbs Ends -->
        <h2><asp:Label ID="lblheader" runat="server"></asp:Label></h2>
        <form id="frmBusinessRegisterList" name="frmBusinessRegisterList" runat="server" class="form">
            <table cellpadding="0" cellspacing="0" border="0" width="100%" class="accounts-table">
                <tbody>
                    <tr>
                        <td width="15%">
                            <asp:DropDownList name="ddltypes" ID="ddltypes" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddltypes_SelectedIndexChanged">
                                <asp:ListItem Value="-1" Selected>No Filter</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td width="70%"></td>
                    </tr>
                </tbody>
            </table>           
            <div class="clr"></div>

            <table class="table">
                <thead>
                    <tr>
                        <th>#SL</th>
                        <th>Business ID</th>
                        <th>Business Name</th>
                        <th>Type Of Business Register</th>
                        <th>City</th>
                        <th>State</th>
                        <th>Country</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="rptrBusinessRegList" runat="server">
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <%#Container.ItemIndex+1 %>
                                </td>
                                <td>
                                    <a href="BusinessRegisterView.aspx?pkey=<%#Eval("pkey_business_register") %>&bid=<%= pkeyMasterCat %>" data-gravity="s" title="Edit"><%#Eval("business_id") %></a>
                                </td>
                                <td>
                                    <a href="BusinessRegisterView.aspx?pkey=<%#Eval("pkey_business_register") %>&bid=<%= pkeyMasterCat %>" data-gravity="s" title="Edit"><%#Eval("business_name") %></a>
                                </td>
                                <td>
                                    <%#Eval("businesstype") %>
                                </td>
                                <td>
                                    <%#Eval("city") %>
                                </td>
                                <td>
                                    <%# Eval("state_name") %>
                                </td>
                                <td>
                                    <%# Eval("country_name") %>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
            <asp:Label ID="lblTotalRecords" runat="server" Font-Bold="true"></asp:Label>
            <div class="btn_lst">
                <div class="lft">
                    <asp:Button runat="server" ID="btnAddNewBusiness" CssClass="button rounded" Text="Add A New Business" OnClick="btnAddNewBusiness_Click"></asp:Button>
                </div>
                <div>
                    <asp:Button ID="btnClose" Text="Close" CssClass="button rounded" runat="server" OnClick="btnClose_Click"></asp:Button>
                </div>
            </div>
        </form>
    </div>
</asp:Content>
