﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Bestprintbuy.Admin.DataModel;
using Bestprintbuy.Admin.Services;
using Bestprintbuy.Admin.Web.Common;
using Bestprintbuy.Admin.Utilities;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;

namespace Bestprintbuy.Admin.Web.BusinessEntites
{
    public partial class BusinessRegisterDetail : PageBase
    {
        protected int pkey_register = -2;
        protected int pkeyType = -1;
        protected int pkeyCategory = -1;
        protected int pkeyMasterCat = 1;
        protected int isSupplier = 0;
        protected string searchfor = "-1";
        protected int PrincContact = -1;
        protected string pkeyMastertext = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                pkeyMasterCat = Convert.ToInt32(Request.QueryString["bid"]);
                if (pkeyMasterCat == 2)
                    lblbreadcrumb.Text = lblheader.Text = "Supplier / Vendors Details";
                else if (pkeyMasterCat == 13)
                    lblbreadcrumb.Text = lblheader.Text = "Freight / Shipping Company Details";
                else if (pkeyMasterCat == 3)
                    lblbreadcrumb.Text = lblheader.Text = "Co Branded Business Partner Details";
                else if (pkeyMasterCat == 10)
                    lblbreadcrumb.Text = lblheader.Text = "B2B Affiliate Details";
                else if (pkeyMasterCat == 1)
                    lblbreadcrumb.Text = lblheader.Text = "Business Register Entity Details";
                try
                {
                    GetData();
                }
                catch (Exception ex)
                {
                    FileLogger.WriteException(ex);
                    AlertScript(ex.Message);
                }
            }
        }

        private void GetData()
        {
            pkeyMasterCat = Convert.ToInt32(Request.QueryString["bid"]);
            pkey_register = Convert.ToInt32(Request.QueryString["pkey"]);
            BusinessEntityModel model = BusinessEntitiesService.GetList(pkey_register, pkeyType, pkeyCategory, searchfor, pkeyMasterCat, isSupplier, ConnString);

            ViewState["model"] = model;
            if (pkey_register != -2)
            {
                if (model.Countries.Rows.Count > 0)
                {
                    ddlCountry.DataSource = model.Countries;
                    ddlCountry.DataTextField = "country_name";
                    ddlCountry.DataValueField = "pkey_country";
                    ddlCountry.DataBind();
                    ddlCountry.Items.Insert(0, new ListItem("No Option", "-1"));
                }

                if (model.States.Rows.Count > 0)
                {
                    ddlState.DataSource = model.States;
                    ddlState.DataTextField = "state_name";
                    ddlState.DataValueField = "pkey_state";
                    ddlState.DataBind();
                    ddlState.Items.Insert(0, new ListItem("No Option", "-1"));
                }

                if (model.BusinessCategories.Rows.Count > 0)
                {
                    ddlCategory.DataSource = model.BusinessCategories;
                    ddlCategory.DataTextField = "ct_name";
                    ddlCategory.DataValueField = "pkey_category_type";
                    ddlCategory.DataBind();
                    ddlCategory.Items.Insert(0, new ListItem("No Option", ""));
                }

                if (model.BusinessTypes.Rows.Count > 0)
                {
                    ddlType.DataSource = model.BusinessTypes;
                    ddlType.DataTextField = "ct_name";
                    ddlType.DataValueField = "pkey_category_type";
                    ddlType.DataBind();
                    ddlType.Items.Insert(0, new ListItem("No Option", ""));
                }

                if (model.Currencies.Rows.Count > 0)
                {
                    ddlCurrency.DataSource = model.Currencies;
                    ddlCurrency.DataTextField = "Currency_name";
                    ddlCurrency.DataValueField = "pkey_currency";
                    ddlCurrency.DataBind();
                    ddlCurrency.Items.Insert(0, new ListItem("No Option", "-1"));
                }

                if (model.Phones.Rows.Count > 0)
                {
                    rptrPhones.DataSource = model.Phones;
                    rptrPhones.DataBind();
                }

                if (model.BusinessEntity.Rows.Count > 0)
                {
                    BusinessEntityModel.BusinessEntityRow row = (BusinessEntityModel.BusinessEntityRow)model.BusinessEntity.Rows[0];
                    txtBusinessID.Text = row.Isbusiness_idNull() ? "" : row.business_id;
                    txtPrefix.Text = row.IsprefixNull() ? "" : row.prefix;
                    txtName.Text = row.Isbusiness_nameNull() ? "" : row.business_name;
                    txtOtherNames.Text = row.Isother_namesNull() ? "" : row.other_names;
                    txtAddress1.Text = row.Isaddress1Null() ? "" : row.address1;
                    txtAddress2.Text = row.Isaddress2Null() ? "" : row.address2;
                    txtCity.Text = row.IscityNull() ? "" : row.city;
                    txtPostalCode.Text = row.IszipcodeNull() ? "" : row.zipcode;
                    txtTaxID.Text = row.Istax_idNull() ? "" : row.tax_id;
                    if (row.is_business)
                        rbtnBusiness.Checked = true;
                    else
                        rbtnIndividual.Checked = true;
                    if (row.tax_type)
                        rbtnBusinessTaxType.Checked = true;
                    else
                        rbtnIndividualTaxType.Checked = true;
                    txtCreditPerTransaction.Text = row.Iscredit_per_transactionNull() ? "" : row.credit_per_transaction.ToString();
                    txtCumilativeCredit.Text = row.Iscumulative_creditNull() ? "" : row.cumulative_credit.ToString();
                    txtCreditPeriod.Text = row.Iscredit_periodNull() ? "" : row.credit_period.ToString();
                    ddlCountry.SelectedValue = row.Isfkey_countryNull() ? "-1" : row.fkey_country.ToString();
                    ddlState.SelectedValue = row.Isfkey_stateNull() ? "-1" : row.fkey_state.ToString();
                    ddlCategory.SelectedValue = row.Isfkey_categoryNull() ? "-1" : row.fkey_category.ToString();
                    ddlType.SelectedValue = row.Isfkey_typeNull() ? "-1" : row.fkey_type.ToString();
                    ddlCurrency.SelectedValue = row.Isfkey_currencyNull() ? "-1" : row.fkey_currency.ToString();
                    chkWarnonCredit.Checked = row.warn_on_credit_violation;
                    hdnPrincContact.Value = row.Ispkey_business_registerNull() ? "-1" : row.pkey_business_register.ToString();
                    lblPrincContact.Text = row.IsprincipalcontactNull() ? "" : row.principalcontact;
                    PrincContact = row.Isfkey_reg_prn_contactNull() ? -1 : row.fkey_reg_prn_contact;
                    hdnPkeyContact.Value = row.Isfkey_reg_prn_contactNull() ? "-1" : row.fkey_reg_prn_contact.ToString();
                    ddlCutoffPeriod.SelectedValue = row.Iscut_of_periodNull() ? "-1" : row.cut_of_period.ToString();
                    btnDelete.Visible = true;
                }
                fSetPhone.Visible = true;
            }
            else
            {
                hdnPkeyContact.Value = "-1";
                rbtnBusiness.Checked = true;
                rbtnBusinessTaxType.Checked = true;
                chkWarnonCredit.Checked = true;
                if (model.Countries.Rows.Count > 0)
                {
                    ddlCountry.DataSource = model.Countries;
                    ddlCountry.DataTextField = "country_name";
                    ddlCountry.DataValueField = "pkey_country";
                    ddlCountry.DataBind();
                    ddlCountry.Items.Insert(0, new ListItem("No Option", "-1"));
                }
                if (model.BusinessCategories.Rows.Count > 0)
                {
                    ddlCategory.DataSource = model.BusinessCategories;
                    ddlCategory.DataTextField = "ct_name";
                    ddlCategory.DataValueField = "pkey_category_type";
                    ddlCategory.DataBind();
                    ddlCategory.Items.Insert(0, new ListItem("No Option", ""));
                }
                if (model.Currencies.Rows.Count > 0)
                {
                    ddlCurrency.DataSource = model.Currencies;
                    ddlCurrency.DataTextField = "Currency_name";
                    ddlCurrency.DataValueField = "pkey_currency";
                    ddlCurrency.DataBind();
                    ddlCurrency.Items.Insert(0, new ListItem("No Option", "-1"));
                }
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                pkey_register = Convert.ToInt32(Request.QueryString["pkey"]);
                BusinessEntitiesService.DeleteBusinessEntity(pkey_register, ConnString);
                pkeyMastertext = GetMasterCatText(Request.QueryString["bid"]);
                Response.Redirect("BusinessRegisterList.aspx?bid=" + pkeyMastertext);
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                pkey_register = Convert.ToInt32(Request.QueryString["pkey"]);
                BusinessEntityModel model = new BusinessEntityModel();
                BusinessEntityModel.BusinessEntityRow row = model.BusinessEntity.NewBusinessEntityRow();
                row.pkey_business_register = pkey_register;
                row.business_id = txtBusinessID.Text;
                row.business_name = txtName.Text;
                row.prefix = txtPrefix.Text;
                row.other_names = txtOtherNames.Text;
                row.is_business = rbtnBusiness.Checked ? true : false;
                row.tax_type = rbtnBusinessTaxType.Checked ? true : false;
                row.fkey_reg_prn_contact = Convert.ToInt32(hdnPkeyContact.Value);
                row.address1 = txtAddress1.Text;
                row.address2 = txtAddress2.Text;
                row.city = txtCity.Text;
                row.fkey_state = ddlState.SelectedValue == "" ? -1 : Convert.ToInt32(ddlState.SelectedValue);
                row.zipcode = txtPostalCode.Text;
                row.fkey_country = ddlCountry.SelectedValue == "" ? -1 : Convert.ToInt32(ddlCountry.SelectedValue);
                row.fkey_category = ddlCategory.SelectedValue == "" ? -1 : Convert.ToInt32(ddlCategory.SelectedValue);
                row.fkey_type = ddlType.SelectedValue == "" ? -1 : Convert.ToInt32(ddlType.SelectedValue);
                row.tax_id = txtTaxID.Text;
                row.cut_of_period = ddlCutoffPeriod.SelectedValue == "" ? -1 : Convert.ToInt32(ddlCutoffPeriod.SelectedValue);
                row.credit_per_transaction = Convert.ToInt32(txtCreditPerTransaction.Text == "" ? "0" : txtCreditPerTransaction.Text);
                row.credit_period = Convert.ToInt32(txtCreditPeriod.Text == "" ? "0" : txtCreditPeriod.Text);
                row.cumulative_credit = Convert.ToInt32(txtCumilativeCredit.Text == "" ? "0" : txtCumilativeCredit.Text);
                row.fkey_currency = ddlCurrency.SelectedValue == "" ? -1 : Convert.ToInt32(ddlCurrency.SelectedValue);
                row.warn_on_credit_violation = chkWarnonCredit.Checked;
                row.fkey_user = Identity.UserPK;
                model.BusinessEntity.AddBusinessEntityRow(row);

                pkey_register = BusinessEntitiesService.UpdateBusinessEntity(model, ConnString);
                pkeyMastertext = GetMasterCatText(Request.QueryString["bid"]);
                Response.Redirect("BusinessRegisterList.aspx?bid=" + pkeyMastertext);
            }
            catch (Exception ex)
            {
                AlertScript(ex.Message);
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            pkeyMastertext = GetMasterCatText(Request.QueryString["bid"]);
            Response.Redirect("BusinessRegisterList.aspx?bid=" + pkeyMastertext);
        }

        private string GetMasterCatText(string strID)
        {
            switch (strID)
            {
                case "2":
                    return "sup";
                case "4":
                    return "mer";
                case "3":
                    return "pat";
                case "10":
                    return "afl";
                case "1":
                    return "bur";
                case "13":
                    return "ship";
                case "-1":
                    return "usr";
                default:
                    return "bur";
            }
        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BusinessEntityModel model = (BusinessEntityModel)ViewState["model"];

                var list = from collect in model.States
                           where collect.fkey_country == Convert.ToInt32(ddlCountry.SelectedValue)
                           select collect;
                ddlState.DataSource = list.ToList();
                ddlState.DataTextField = "state_name";
                ddlState.DataValueField = "pkey_state";
                ddlState.DataBind();
                ddlState.Items.Insert(0, new ListItem("No Option", "-1"));
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }            
        }

        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BusinessEntityModel model = (BusinessEntityModel)ViewState["model"];
                var list = from collect in model.BusinessTypes
                           where collect.fkey_self == Convert.ToInt32(ddlCategory.SelectedValue == "" ? "0" : ddlCategory.SelectedValue)
                           select collect;

                ddlType.DataSource = list.ToList();
                ddlType.DataTextField = "ct_name";
                ddlType.DataValueField = "pkey_category_type";
                ddlType.DataBind();
                ddlType.Items.Insert(0, new ListItem("No Option", "-1"));
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }            
        }

        protected void lbtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbtn = (LinkButton)sender;
                int pkeyPhones = Convert.ToInt32(lbtn.CommandArgument);
                BusinessEntityModel model = new BusinessEntityModel();
                BusinessEntityModel.PhonesRow phonesRow = model.Phones.NewPhonesRow();
                phonesRow.deletekeys = pkeyPhones.ToString();
                phonesRow.phone_number = "";
                phonesRow.extension = "";
                phonesRow.contact_type = "";
                phonesRow.type_key = "-1";
                phonesRow.pkey_phones = -1;
                model.Phones.AddPhonesRow(phonesRow);
                model = BusinessEntitiesService.UpdatePhones(model, Convert.ToInt32(hdnPrincContact.Value), ConnString);
                pkeyMasterCat = Convert.ToInt32(Request.QueryString["bid"]);
                pkey_register = Convert.ToInt32(Request.QueryString["pkey"]);
                Response.Redirect("BusinessRegisterDetail.aspx?pkey=" + pkey_register + "&bid=" + pkeyMasterCat);
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }           
        }

        protected void btnUpdatePhone_Click(object sender, EventArgs e)
        {
            try
            {
                BusinessEntityModel model = new BusinessEntityModel();
                BusinessEntityModel.PhonesRow phonesRow = model.Phones.NewPhonesRow();
                phonesRow.deletekeys = "-1";
                phonesRow.phone_number = hdnNumber.Value;
                phonesRow.extension = hdnExt.Value;
                phonesRow.contact_type = hdnType.Value;
                phonesRow.type_key = hdnValue.Value;
                phonesRow.pkey_phones = Convert.ToInt32(pkeyphones.Value);
                model.Phones.AddPhonesRow(phonesRow);
            
                model = BusinessEntitiesService.UpdatePhones1(model, Convert.ToInt32(hdnPrincContact.Value), ConnString);
                pkeyMasterCat = Convert.ToInt32(Request.QueryString["bid"]);
                pkey_register = Convert.ToInt32(Request.QueryString["pkey"]);
                Response.Redirect("BusinessRegisterDetail.aspx?pkey=" + pkey_register + "&bid=" + pkeyMasterCat);
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                AlertScript(ex.Message);
            }
        }
    }
}